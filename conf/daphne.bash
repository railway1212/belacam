#!/bin/bash
set -e  # exit on error
source /home/osboxes/Documents/AmbientFund/AmbientFund/.env/bin/activate
exec daphne -b localhost -p 8080 belacam.asgi:channel_layer

