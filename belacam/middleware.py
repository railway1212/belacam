import datetime

from django.contrib.auth import logout

from belacam.apps.thoughts.models import UserRequestStat
from belacam.apps.thoughts.mail_composer import reset_current_request, thread_locals


class TrackUserRequest(object):
    def process_request(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        if request.user.is_staff:
            user = request.user
            stats = UserRequestStat.objects.filter(user=user, date=datetime.date.today())
            if stats:
                stats[0].requests += 1
                stats[0].save()
            else:
                stats = UserRequestStat()
                stats.user = user
                stats.requests = 1
                stats.save()

        # Code to be executed for each request/response after
        # the view is called.
        return None


class RequestMiddleware(object):
    def _https_referer_replace(self, request):
        """
        When https is enabled, and request from mobile app is sent, add Referer header to the request to fix CSRF Token issue
        """
        origin = request.META.get('HTTP_ORIGIN')

        if request.is_secure() and origin == "file://" and 'HTTP_REFERER' not in request.META:

            try:
                http_host = "https://%s/" % request.META['HTTP_HOST']
                request.META = request.META.copy()
                request.META['HTTP_REFERER'] = http_host
            except KeyError:
                pass

    def process_request(self, request):
        if request._cors_enabled:
            self._https_referer_replace(request)
        # get_token(request) # force CSRFTOKEN setup
        thread_locals.request = request

    def process_response(self, request, response):
        reset_current_request()
        return response


class SuspendSessionMiddleware(object):
    def process_request(self, request):
        try:
            user = request.user

            # Just pass if anonymous user
            if not user.id or not user.user1:
                return None

            user_profile = user.user1

            if not user.is_active:
                # TODO: destroy session
                logout(request)
                return

            if user_profile.suspend_until:
                date_now = datetime.datetime.now(user_profile.suspend_until.tzinfo)
                date_diff = user_profile.suspend_until - date_now
                if date_diff.total_seconds() > 0:
                    # TODO: destroy session
                    logout(request)

            return None
        except Exception as e:
            return None
