# -*- coding: utf-8 -*-
import os

import dj_database_url

from django.conf import global_settings
from django.utils.translation import gettext_noop
import django.conf.locale

PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
PACKAGE_ROOT = os.path.abspath(os.path.dirname(__file__))
NEW_BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_DIR = PACKAGE_ROOT

if os.environ.get("ENVIRONMENT", "DEVELOPMENT") == "PRODUCTION":
    PRODUCTION = True
else:
    PRODUCTION = False

if PRODUCTION:
    SECURE_SSL_REDIRECT = True

    DATABASES = {
        "default": dj_database_url.config()
    }

else:
    if os.environ.get('DEV_DB_TYPE', 'SQLITE') == "POSTGRES":
        print('Postgres')
        DATABASES = {
            "default": {
                'ENGINE': 'django.db.backends.postgresql_psycopg2',
                'NAME': os.environ.get("DB_NAME", "belacam_dev"),
                'USER': os.environ.get("PG_USER", ""),
                'PASSWORD': os.environ.get("PG_PASSWORD", ""),
                'HOST': os.environ.get("DB_HOST", "localhost"),
                'PORT': '5432',
            }
        }
    else:
        print('Spatialite')
        SPATIALITE_LIBRARY_PATH = os.environ.get('SPATIALITE_LIBRARY_PATH', '')
        DATABASES = {
            "default": {
                "ENGINE": "django.contrib.gis.db.backends.spatialite",
                "NAME": "dev.db",
            }
        }

SECURE_PROXY_SSL_HEADER = ('HTTP_CF_VISITOR', '{"scheme":"https"}')

EMAIL_HOST = os.environ.get("EMAIL_HOST", "")
EMAIL_HOST_USER = os.environ.get("EMAIL_HOST_USER", "")
EMAIL_HOST_PASSWORD = os.environ.get("EMAIL_HOST_PASSWORD", "")
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_BACKEND = os.environ.get("EMAIL_BACKEND", "")
DEFAULT_FROM_EMAIL = os.environ.get("DEFAULT_FROM_EMAIL", "")
SENDGRID_API_KEY = os.environ.get("SENDGRID_API_KEY", "")


DEBUG = os.environ.get("DEFAULT_DEBUG", "TRUE") == "TRUE"

GOOGLE_RECAPTCHA_SITE_KEY = os.environ.get("GOOGLE_RECAPTCHA_SITE_KEY", "")
GOOGLE_RECAPTCHA_SECRET_KEY = os.environ.get("GOOGLE_RECAPTCHA_SECRET_KEY", "")

NEUTRINO_API_USER_ID = os.environ.get("NEUTRINO_API_USER_ID", "")
NEUTRINO_API_KEY = os.environ.get("NEUTRINO_API_KEY", "")

AVATAR_AUTO_GENERATE_SIZES = (80, 30, 40, 50, 60, 70, )

# REST_FRAMEWORK = {
#     'DEFAULT_RENDERER_CLASSES': (
#                                  'rest_framework.renderers.JSONRenderer',
#                                  )
# }
REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ],
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 8
}

AWS_ACCESS_KEY_ID = os.environ.get("AWS_ACCESS_KEY_ID", "")
AWS_SECRET_ACCESS_KEY = os.environ.get("AWS_SECRET_ACCESS_KEY", "")
AWS_STORAGE_BUCKET_NAME = os.environ.get("AWS_STORAGE_BUCKET_NAME", "")
AWS_S3_FILE_OVERWRITE = False
AWS_QUERYSTRING_EXPIRE = 157784630

DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
STATICFILES_STORAGE = os.environ.get("STATICFILES_STORAGE", 'django.contrib.staticfiles.storage.StaticFilesStorage')
# STATIC_URL = 'http://s3.amazonaws.com/%s' % AWS_STORAGE_BUCKET_NAME + '/'
STATIC_URL = "/static/"

ALLOWED_HOSTS = ['*']

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = "UTC"

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = "en-us"

SITE_ID = int(os.environ.get("SITE_ID", 1))

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(NEW_BASE_DIR, "media")


# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = "/site_media/media/"

FILE_SAVE_PATH = os.path.join(MEDIA_ROOT, "images", "profiles")
FILE_URL_PATH = MEDIA_URL+'images/profiles/'

# Absolute path to the directory static files should be collected to.
# Don"t put anything in this directory yourself; store your static files
# in apps" "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"

STATIC_ROOT = os.path.join(NEW_BASE_DIR, "staticfiles")

SELECT2_CSS = 'assets/css/select2.css'

# Additional locations of static files
STATICFILES_DIRS = [
    os.path.join(PROJECT_ROOT, "static"),
]

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]

# Make this unique, and don't share it with anybody.
# SECRET_KEY = "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"
SECRET_KEY = os.environ.get("SECRET_KEY", "")

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            # todo: fix this and use only one.
            os.path.join(NEW_BASE_DIR, "templates"),
            os.path.join(NEW_BASE_DIR, "apps", "thoughts", "templates"),
            os.path.join(NEW_BASE_DIR, "static/dist"),
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "debug": DEBUG,
            "context_processors": [
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.template.context_processors.request",
                "django.contrib.messages.context_processors.messages",
                "account.context_processors.account",
                "pinax_theme_bootstrap.context_processors.theme",
                # "social.apps.django_app.context_processors.backends",
                # "social.apps.django_app.context_processors.login_redirect",
            ],
        },
    },
]

MIDDLEWARE_CLASSES = [
    'corsheaders.middleware.CorsMiddleware',
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.auth.middleware.SessionAuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    'pagination.middleware.PaginationMiddleware',
    # 'django_otp.middleware.OTPMiddleware',
    'belacam.middleware.TrackUserRequest',
    'belacam.middleware.RequestMiddleware',
    'belacam.middleware.SuspendSessionMiddleware',
    'account.middleware.LocaleMiddleware',
    'belacam.apps.referral.middleware.ReferrerMiddleware',
    'belacam.apps.session_tracking.middleware.SessionTrackingMiddleware'
    # 'two_factor.middleware.threadlocals.ThreadLocals',
]

ENABLE_TWO_FACTOR_AUTH = os.environ.get("ENABLE_TWO_FACTOR_AUTH", "FALSE") == "TRUE"
TWO_FACTOR_PATCH_ADMIN = True
TWO_FACTOR_CALL_GATEWAY = os.environ.get("TWO_FACTOR_CALL_GATEWAY", "")
TWO_FACTOR_SMS_GATEWAY = os.environ.get("TWO_FACTOR_SMS_GATEWAY", "")
TWILIO_ACCOUNT_SID = os.environ.get("TWILIO_ACCOUNT_SID", "")
TWILIO_AUTH_TOKEN = os.environ.get("TWILIO_AUTH_TOKEN", "")
TWILIO_CALLER_ID = os.environ.get("TWILIO_CALLER_ID", "")

AVATAR_CACHE_ENABLED = False

CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': 'localhost:6379',
    },
}

ROOT_URLCONF = "belacam.urls"

# Python dotted path to the WSGI application used by Django's runserver.
RAVEN_CONFIG = {
    'dsn': os.environ.get('SENTRY_DSN', ""),
}
WSGI_APPLICATION = "belacam.wsgi.application"

INSTALLED_APPS = [
    # django
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.messages",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.staticfiles",
    'django.contrib.humanize',
    #################
    # 3rd party     #
    #################
    # 'ajax_select',
    'avatar',
    "account",
    'crispy_forms',
    'dal',
    'dal_select2',
    "django_countries",
    'django_extensions',
    'django_select2.urls',
    'el_pagination',
    "friendship",
    'import_export',
    'notifications',
    "pinax.eventlog",
    "pinax.webanalytics",
    'postman',
    'raven.contrib.django.raven_compat',
    'rest_framework',
    'rosetta',

    # "social.apps.django_app.default",
    'storages',
    'taggit',
    "taggit_autosuggest",
    'taggit_templatetags2',
    'widget_tweaks',
    'corsheaders',
    'channels',

    # theme
    "bootstrapform",
    "pinax_theme_bootstrap",

    # project apps
    'belacam.apps.core',
    'belacam.apps.bonus',
    'belacam.apps.stats',
    'belacam.apps.user',
    'belacam.apps.referral',
    'belacam.apps.reward',
    'belacam.apps.session_tracking',
    'belacam.apps.thoughts',
]

if (not PRODUCTION) and DEBUG:
    INSTALLED_APPS = INSTALLED_APPS + [
        'debug_toolbar',
    ]
    MIDDLEWARE_CLASSES = MIDDLEWARE_CLASSES + [
            'debug_toolbar.middleware.DebugToolbarMiddleware',
    ]

if ENABLE_TWO_FACTOR_AUTH:
    INSTALLED_APPS = INSTALLED_APPS + [
        'django_otp',
        'django_otp.plugins.otp_static',
        'django_otp.plugins.otp_totp',
        'two_factor',
    ]
    MIDDLEWARE_CLASSES = MIDDLEWARE_CLASSES + [
        'django_otp.middleware.OTPMiddleware',
        'two_factor.middleware.threadlocals.ThreadLocals',
    ]

# EL_PAGINATION_LOADING = """<img src="http://belacoin.org/assets/img/loading.gif" alt="loading" />"""
AUTH_PROFILE_MODULE = 'thoughts.UserProfile'

AJAX_LOOKUP_CHANNELS = {
    'postman_users': dict(model='auth.user', search_field='username'),
}


#############
# Celery    #
#############
CELERY_BROKER_URL = os.environ.get("REDIS_URL", "redis://localhost:6379/1")
CELERY_RESULT_BACKEND = os.environ.get("REDIS_URL", "redis://localhost:6379/1")

#: Only add pickle to this list if your broker is secured
#: from unwanted access (see userguide/security.html)
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'UTC'
CELERY_ENABLE_UTC = True

SERVER_URL_PREFIX = os.environ.get('SERVER_URL_PREFIX', 'https://localhost:8000')
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "require_debug_false": {
            "()": "django.utils.log.RequireDebugFalse"
        }
    },
    "handlers": {
        "mail_admins": {
            "level": "ERROR",
            "filters": ["require_debug_false"],
            "class": "django.utils.log.AdminEmailHandler"
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
    },
    "loggers": {
        'two_factor': {
            'handlers': ['console'],
            'level': 'INFO',
        },
        "django.request": {
            "handlers": ["mail_admins"],
            "level": "ERROR",
            "propagate": True,
        },
    }
}

ANONYMOUS_USER_ID = -1
POSTMAN_AUTO_MODERATE_AS = True  # default is None
POSTMAN_SHOW_USER_AS = 'get_full_name'  # default is None
POSTMAN_QUICKREPLY_QUOTE_BODY = True  # default is False
POSTMAN_AUTOCOMPLETER_APP = {
    'name': '',  # default is 'ajax_select'
    'field': '',  # default is 'AutoCompleteField'
    'arg_name': '',  # default is 'channel'
    'arg_default': 'postman_friends',  # no default, mandatory to enable the feature
}

AVATAR_GRAVATAR_DEFAULT = os.environ.get("AVATAR_GRAVATAR_DEFAULT", "")
AVATAR_STORAGE_DIR = os.path.join(MEDIA_ROOT, "images", "profiles")
AVATAR_MAX_SIZE = 5*1024*1024
AVATAR_MAX_AVATARS_PER_USER = 1

FIXTURE_DIRS = [
    os.path.join(PROJECT_ROOT, "../../fixtures"),
]


# EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

ACCOUNT_OPEN_SIGNUP = True
ACCOUNT_EMAIL_UNIQUE = True
ACCOUNT_EMAIL_CONFIRMATION_REQUIRED = False
ACCOUNT_LOGIN_URL = ""
ACCOUNT_LOGIN_REDIRECT_URL = LOGIN_REDIRECT_URL = ""
ACCOUNT_LOGOUT_REDIRECT_URL = ""
ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = 4
ACCOUNT_USE_AUTH_AUTHENTICATE = True
ACCOUNT_EMAIL_CONFIRMATION_ANONYMOUS_REDIRECT_URL = ACCOUNT_EMAIL_CONFIRMATION_AUTHENTICATED_REDIRECT_URL = "/"

AUTHENTICATION_BACKENDS = [
    # "social.backends.twitter.TwitterOAuth",
    "account.auth_backends.UsernameAuthenticationBackend",
    'guardian.backends.ObjectPermissionBackend',
    'django.contrib.auth.backends.ModelBackend',
    'belacam.backends.EmailBackend'
]

# SOCIAL_AUTH_TWITTER_KEY = os.environ.get("SOCIAL_AUTH_TWITTER_KEY", "")
# SOCIAL_AUTH_TWITTER_SECRET = os.environ.get("SOCIAL_AUTH_TWITTER_SECRET", "")

EXTRA_LANG_INFO = {
    'tl': {
        'bidi': False,  # right-to-left
        'code': 'tl',
        'name': 'Tagalog',
        'name_local': 'Tagalog',  # unicode codepoints here
    },
    'ny': {
        'bidi': False,  # right-to-left
        'code': 'ny',
        'name': 'Chichewa',
        'name_local': 'Chichewa',  # unicode codepoints here
    },
    'yo': {
        'bidi': False, # right-to-left
        'code': 'yo',
        'name': 'Yoruba',
        'name_local': 'Yoruba',  # unicode codepoints here
    }
}

LANG_INFO = dict(django.conf.locale.LANG_INFO.items() + EXTRA_LANG_INFO.items())
django.conf.locale.LANG_INFO = LANG_INFO

ACCOUNT_LANGUAGES = [
    ("en", "English"),
    ("de", "Deutsch"),
    ("es", "español"),
    ("fr", "français"),
    ("hi", "Hindi"),
    ("it", "italiano"),
    ("ja", "日本語"),
    ("ko", "한국어"),
    ("ny", gettext_noop("Chichewa")),
    ("pt", "Português"),
    ("ru", "Русский"),
    ("zh-hans", "简体中文"),
    ("id", "Bahasa Indonesia"),
    ("tl", gettext_noop("Tatalog")),
    ("tr", "Türkçe"),
    ("yo", gettext_noop("Yoruba")),
    ("vi", "tiếng Việt"),
]

LANGUAGES = [
    ("en", gettext_noop('English')),
    ("de", gettext_noop('German')),
    ("es", gettext_noop('Spanish')),
    ("fr", gettext_noop('French')),
    ("hi", gettext_noop('Hindi')),
    ("it", gettext_noop('Italian')),
    ("ja", gettext_noop('Japanese')),
    ("ko", gettext_noop('Korean')),
    ("ny", gettext_noop("Chichewa")),
    ("pt", gettext_noop('Portuguese')),
    ("ru", gettext_noop('Russian')),
    ("zh-hans", gettext_noop('Simplified Chinese')),
    ("id", gettext_noop('Indonesian')),
    ("tl", gettext_noop("Tatalog")),
    ("tr", gettext_noop('Turkish')),
    ("vi", gettext_noop("Vietnamese")),
    ("yo", gettext_noop("Yoruba"))
]

LOCALE_PATHS = [
    os.path.join(PROJECT_ROOT, "../../locale"),
    os.path.join(PROJECT_ROOT, "..", "static", "assets", "locale")
]


LANGUAGE_COUNTRY_MAPPER = {
    'id': ['ID'],
    'ru': ['RU'],
    'hi': ['IN'],
    'vi': ['VN']
}

ROSETTA_POFILENAMES = ['django.po', 'djangojs.po', 'js.po']
ROSETTA_AUTO_COMPILE = False


INTERNAL_IPS = (
    os.environ.get("INTERNAL_IP", "127.0.0.1")
)

UPLOAD_S3_MAX_FILE_SIZE = 10485760
DEPOSIT_SERVER = os.environ.get("DEPOSIT_SERVER", "localhost:3000")
INSTA_BELLA_SERVER = os.environ.get("INSTA_BELLA_SERVER", "localhost")
SECRET_RECV_PASSWORD = os.environ.get("SECRET_RECV_PASSWORD", "password")
SECRET_SEND_PASSWORD = os.environ.get("SECRET_SEND_PASSWORD", "password")
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True
CORS_REPLACE_HTTPS_REFERER = True

TAGGIT_AUTOSUGGEST_STATIC_BASE_URL = "https://s3.amazonaws.com/bellassets/jquery-autosuggest"

##################
# Session Tracking
##################
SESSION_TRACKING_EXPIRE_AFTER = 120
SESSION_TRACKING_WARN_AFTER = 90
SESSION_TRACKING_PASSIVE_URL_NAMESPACES = ['admin']

#########################
# Django Channel Settings
#########################
CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "asgi_redis.RedisChannelLayer",
        "CONFIG": {
            "hosts": [("localhost", 6379)],
            "capacity": 500
        },
        "ROUTING": "belacam.routing.channel_routing",
    },
}

#########################
# OneSignal Settings
#########################
ONE_SIGNAL_USER_AUTH_KEY = os.environ.get("ONE_SIGNAL_USER_AUTH_KEY", "")
ONE_SIGNAL_APP_AUTH_KEY = os.environ.get("ONE_SIGNAL_APP_AUTH_KEY", "")
ONE_SIGNAL_APP_ID = os.environ.get("ONE_SIGNAL_APP_ID", "")

#  REFERAL SETTINGS

# Name of the referrer's get parameter name
GET_PARAMETER = 'ref'
# Name of the session variable storing the detected referrer
SESSION_KEY = 'referrer'
# Should unknown referrers be auto created? Default: True
AUTO_CREATE = False
# If this is set auto created referrers will be associated to a campaign that defines a matching pattern. Default: True
AUTO_ASSOCIATE = 'REFERRAL_AUTO_ASSOCIATE'
# If this is set to True, referral names will be case-sensitive.
CASE_SENSITIVE = True


#  SESSION TRACKING SETTINGS
EXPIRE_AFTER = 600
WARN_AFTER = 540
PASSIVE_URLS = []
PASSIVE_URL_NAMES = []
PASSIVE_URL_NAMESPACES = []
