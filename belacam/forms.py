from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from belacam.apps.thoughts.models import UserProfile
from django import forms
import re
from django.core.exceptions import ObjectDoesNotExist
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse
from collections import OrderedDict

import account.forms


class SettingsForm(account.forms.SettingsForm):
    username = forms.CharField(label=_("Username"), max_length=30)
    send_weekly_email = forms.BooleanField(label=_("Send weekly email?"), initial=True, required=False)

    def __init__(self, *args, **kwargs):
        super(SettingsForm, self).__init__(*args, **kwargs)
        field_order = ["username", "email", "language", "send_weekly_email"]
        if not OrderedDict or hasattr(self.fields, "keyOrder"):
            self.fields.keyOrder = field_order
        else:
            self.fields = OrderedDict((k, self.fields[k]) for k in field_order)

    def clean_username(self):
        alnum_re = re.compile(r"^\w+$")
        username = self.cleaned_data["username"]

        if username == self.initial.get('username'):
            return username

        if not alnum_re.search(username):
            raise forms.ValidationError(_("Usernames can only contain letters, numbers and underscores."))
        try:
            User.objects.get(username=username)
        except ObjectDoesNotExist:
            return username.lower()
        raise forms.ValidationError('Username is already taken.')


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        exclude = []


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['accepted_terms_of_service']

    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)
        self.fields['accepted_terms_of_service'].required = True


class RegistrationForm(forms.Form):
    username = forms.CharField(required=True, label='Username', min_length=1, max_length=20)
    email = forms.EmailField(required=True)
    password = forms.CharField(label='Password', widget=forms.PasswordInput())
    password_confirm = forms.CharField(label='Password', widget=forms.PasswordInput())
    # password2 = forms.CharField(label='Password (Again)', widget=forms.PasswordInput())
    # password2 is a 'confirmation' password, to make sure the user typed password1 in correctly

    def clean_password2(self):
        if 'password1' in self.cleaned_data:
            password1 = self.cleaned_data['password1']
            return password1
            # password2 = self.cleaned_data['password2']
            # if password1 == password2:
            #     return password2
        raise forms.ValidationError('Passwords do not match.')

    def clean_username(self):
        username = self.cleaned_data['username'].lower()
        if not re.search(r'^\w+$', username):
            raise forms.ValidationError('Username can only contain alphanumeric characters and the underscore.')
        try:
            User.objects.get(username=username)
        except ObjectDoesNotExist:
            return username.lower()
        raise forms.ValidationError('Username is already taken.')

    def clean_email(self):
        email = self.cleaned_data.get('email')
        username = self.cleaned_data.get('username')
        if email and User.objects.filter(email=email).exclude(username=username).count():
            raise forms.ValidationError(
                mark_safe(('A user with that email already exists, click this <a href="{0}">Password Reset</a> link'
                           ' to recover your account.').format(reverse('account_password_reset')))
            )
        return email



