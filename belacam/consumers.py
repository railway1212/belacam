import json

from channels.auth import channel_session_user_from_http, channel_session_user
from channels.security.websockets import allowed_hosts_only
from django.http import HttpResponse
from channels.handler import AsgiHandler
from channels import Group


def http_consumer(message):
    # Make standard HTTP response - access ASGI path attribute directly
    response = HttpResponse("Hello world! You asked for %s" % message.content['path'])
    # Encode that response into message format (ASGI)
    for chunk in AsgiHandler.encode_response(response):
        message.reply_channel.send(chunk)


@allowed_hosts_only
@channel_session_user_from_http
def ws_add(message):
    message.reply_channel.send({"accept": True})
    user = message.user
    # Add them to the chat group
    Group(user.username).add(message.reply_channel)


@channel_session_user
def ws_disconnect(message):
    Group(message.user.username).discard(message.reply_channel)


def notify_user(message):
    username = message.content['username']
    Group(username).send({
        'text': json.dumps({
            'type': 'NEW_NOTIFICATION'
        })
    })

