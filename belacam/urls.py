from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views, get_user_model
from django.contrib import admin

from rest_framework import routers

import belacam.views


if settings.ENABLE_TWO_FACTOR_AUTH:
    from two_factor.admin import AdminSiteOTPRequired
    from two_factor.urls import urlpatterns as tf_urls
    from two_factor.gateways.twilio.urls import urlpatterns as tf_twilio_urls

    admin.site.__class__ = AdminSiteOTPRequired

# todo: why is this User model in this place? it looks not being used.
User = get_user_model()

router = routers.DefaultRouter()
# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

urlpatterns = [

    url(r'', include('taggit_live.urls')),

    url(r'^wahoo285/', admin.site.urls),

    # todo: why was this deactivated? a small explanation with reason would be awesome!
    # url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
    #     auth_views.password_reset_confirm, name='password_reset_confirm'),

    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),

    url(r"^account/confirm_email/(?P<key>\w+)/$", belacam.views.ConfirmEmailView.as_view(),
        name="account_confirm_email"),

    url(r"^api/", include("belacam.apps.thoughts.urls")),
    url(r"^rosetta/", include("rosetta.urls")),

    url(r'^taggit_autosuggest/', include('taggit_autosuggest.urls')),
    url(r'^avatar/', include('avatar.urls')),
    url(r'^friendship/', include('friendship.urls')),

    url(
        r'^ref/(?P<referrer>[0-9A-Za-z_\-]+)$',
        belacam.views.referrer_view,
        name='referrer_view'
    ),
    url(r"^n/(?P<path>.*)", belacam.views.redirect_prefix),
    url(r"^b/(?P<path>.*)", belacam.views.redirect_prefix),
    url(
        r'^send_weekly_email_to_all$',
        belacam.views.SendWeeklyEmailToAllView.as_view(),
        name='send_weekly_email_to_all'
    ),
    url(
        r'^send_weekly_email/(?P<user_id>\d+)/$',
        belacam.views.SendWeeklyEmailView.as_view(),
        name='send_weekly_email'
    ),
    url(
        r'^send_earning_email/(?P<option>\w+)/$',
        belacam.views.SendEarningEmailView.as_view(),
        name='send_earning_email'
    ),
    url(
        'user-autocomplete/$',
        belacam.views.UserAutocomplete.as_view(),
        name='user-autocomplete',
    ),
    url(
        'bela-autocomplete/$',
        belacam.views.BelaAutocomplete.as_view(),
        name='bela-autocomplete',
    ),

    url(r'', include("belacam.apps.core.urls")),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += [
        url(r'^rosetta/', include('rosetta.urls'))
    ]

if 'belacam.apps.session_tracking' in settings.INSTALLED_APPS:
    urlpatterns += [
        url(r'^session_tracking/', include('belacam.apps.session_tracking.urls'))
    ]

if settings.ENABLE_TWO_FACTOR_AUTH:
    urlpatterns = [
                      url(r'', include(tf_urls + tf_twilio_urls, 'two_factor')),
                  ] + urlpatterns

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
                      url(r'^__debug__/', include(debug_toolbar.urls)),
                  ] + urlpatterns

# todo: what was the reason to deactivate this?
# handler404 = 'belacam.views.error_404'
# handler500 = 'belacam.views.error_500'
