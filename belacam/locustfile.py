from locust import HttpLocust, TaskSet, task

csrftoken = "zhcszfijdAW3Cc2Nb4hAHnCoTi4GPwVd"  # Replace with valid csrftoken


class UserBehavior(TaskSet):
    def on_start(self):
        """ on_start is called when a Locust start before any task is scheduled """
        # self.login()
        # self.loadMainPage()

    # def login(self):
    #     response = self.client.get("/")
    #     csrftoken = response.cookies['csrftoken']
    #     self.client.post("/",
    #                         {"username": "sokhom",
    #                          "password": "abcdefg"},
    #                         headers={"X-CSRFToken": csrftoken})
    # def loadMainPage(self):
    #     self.client.get("b/feed")
    @task
    class HomeFeedPageTaskSet(TaskSet):
        @task(2)
        def growth_api(self):
            response = self.client.get("/feed/api/growth/", headers={"X-CSRFToken": csrftoken})
            print "Response(growth_api): ", response.content

        @task(2)
        def discover_api(self):
            response = self.client.get("/feed/api/discover/", headers={"X-CSRFToken": csrftoken})
            print "Response(discover_api): ", response.content

        @task(1)
        def live_api(self):
            response = self.client.get("/feed/api/notifications/live/", headers={"X-CSRFToken": csrftoken})
            print "Response(live_api): ", response.content

        @task(1)
        def homefeeds_api(self):
            response = self.client.get("/feed/api/homefeeds/", headers={"X-CSRFToken": csrftoken})
            print "Response(homefeeds_api): ", response.content

    @task
    class DiscoverPageTaskSet(TaskSet):
        @task(1)
        def sort_by_hot_api(self):
            response = self.client.get("/feed/api/feed/list/?sort_by=hot", headers={"X-CSRFToken": csrftoken})
            print "Response(sort_by_hot_api): ", response.content

        @task(1)
        def sort_by_new_api(self):
            response = self.client.get("/feed/api/feed/list/?sort_by=new", headers={"X-CSRFToken": csrftoken})
            print "Response(sort_by_new_api): ", response.content

        @task(1)
        def sort_by_likes_today_api(self):
            response = self.client.get("/feed/api/feed/list/?sort_by=time_today", headers={"X-CSRFToken": csrftoken})
            print "Response(sort_by_likes_today_api): ", response.content

        @task(1)
        def sort_by_likes_week_api(self):
            response = self.client.get("/feed/api/feed/list/?sort_by=time_week", headers={"X-CSRFToken": csrftoken})
            print "Response(sort_by_likes_week_api): ", response.content

        @task(1)
        def sort_by_likes_month_api(self):
            response = self.client.get("/feed/api/feed/list/?sort_by=time_month", headers={"X-CSRFToken": csrftoken})
            print "Response(sort_by_likes_today_api): ", response.content

        @task(1)
        def sort_by_likes_all_api(self):
            response = self.client.get("/feed/api/feed/list/?sort_by=time_all", headers={"X-CSRFToken": csrftoken})
            print "Response(sort_by_likes_today_api): ", response.content

        @task(1)
        def people_api(self):
            response = self.client.get("/feed/api/people/", headers={"X-CSRFToken": csrftoken})
            print "Response(people_api): ", response.content

        @task(1)
        def tags_api(self):
            response = self.client.get("/feed/api/tags/", headers={"X-CSRFToken": csrftoken})
            print "Response(tags_api): ", response.content


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 5000
    max_wait = 15000
