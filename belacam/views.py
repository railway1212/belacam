from dal import autocomplete
from django.shortcuts import render_to_response, redirect
from django.views.decorators.http import require_http_methods
from django.views.generic import TemplateView

from belacam.apps.core.utils import custom_redirect
from belacam.apps.thoughts.tasks import send_weekly_email
import account.views
from django.contrib.auth import authenticate, login, logout
from belacam.apps.thoughts.models import Bella
from django.contrib.auth import get_user_model
User = get_user_model()

class ConfirmEmailView(account.views.ConfirmEmailView):
    def get_template_names(self):
        if self.request.user.is_authenticated():
            logout(self.request)
        return {
            "GET": ["account/email_confirm.html"],
            "POST": ["account/email_confirmed.html"],
        }[self.request.method]

class SendWeeklyEmailToAllView(TemplateView):
    template_name = "send_weekly_email.html"

    def get_context_data(self, **kwargs):
        send_weekly_email({})
        return {}


class SendWeeklyEmailView(TemplateView):
    template_name = "send_weekly_email.html"

    def get_context_data(self, **kwargs):
        user_id = kwargs.get('user_id')

        send_weekly_email({'user': [user_id]})
        return {}


class SendEarningEmailView(TemplateView):
    template_name = "send_weekly_email.html"

    def get_context_data(self, **kwargs):
        option = kwargs.get('option')

        send_weekly_email({'duration': option})
        return {}


@require_http_methods(["GET"])
def referrer_view(request, referrer):
    if referrer is None:
        return error_404(request)
    return custom_redirect('/', ref=referrer)


def error_404(request):
    return render_to_response('errors/404.html')


def error_500(request):
    return render_to_response('errors/500.html')


@require_http_methods(["GET"])
def redirect_prefix(request, path):
    return redirect("/" + path)

class UserAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated():
            return User.objects.none()
        qs = User.objects.all()
        if self.q:
            qs = qs.filter(username__istartswith=self.q)
        return qs

class BelaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated():
            return Bella.objects.none()
        qs = Bella.objects.all()
        if self.q:
            qs = qs.filter(content__icontains=self.q)
        return qs