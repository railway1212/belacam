from django.contrib.auth import get_user_model
User = get_user_model()


class EmailBackend(object):
    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

    def authenticate(self, username=None, password=None, **kwargs):

        try:
            user = User.objects.get(email=username)
        except User.DoesNotExist:
            return None
        except User.MultipleObjectsReturned:
            user = User.objects.filter(email=username)[0]

        if getattr(user, 'is_active', False) and  user.check_password(password):
            return user

        return None
