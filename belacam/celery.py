from __future__ import absolute_import, unicode_literals
import dotenv
import os
from celery import Celery
from celery.schedules import crontab

# set the default Django settings module for the 'celery' program.
local_env_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), ".env")
if os.path.exists(local_env_path):
    dotenv.read_dotenv(local_env_path)
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'belacam.settings')
app = Celery('belacam')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'send-weekly-email': {
        'task': 'belacam.apps.thoughts.tasks.send_weekly_email',
        'schedule': crontab(hour='9', minute='0', day_of_week='1'),
        'args': {},
    },
    'get-belacam-price': {
        'task': 'belacam.apps.thoughts.tasks.get_belacam_price',
        'schedule': 600.0,
    },
    'update-likes': {
        'task': 'belacam.apps.thoughts.tasks.update_likes',
        'schedule': 600.0,
    },
    'update-post-rank': {
        'task': 'belacam.apps.thoughts.tasks.update_post_rank',
        'schedule': 1800.0,
    },
}
