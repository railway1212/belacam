from channels.routing import route
from belacam.consumers import ws_add, ws_disconnect, notify_user

channel_routing = [
    route("websocket.connect", ws_add),
    route("websocket.disconnect", ws_disconnect),
    route("notify_user", notify_user)
]