var email_run = false;
var username_run = false;
var password_run = false;
var balance = 0;
var balance_counter = document.getElementById("balance_counter");

$( "#signup_email" ).focusout(function() {
    if( $(this).val() && email_run == false ) {
        setTimeout(function(){ $('#sack').addClass("playit"); 
$('#sack_tag_balance').addClass("show");          $('.prelim_sack_tag').addClass("hide");              
balance = balance + .19;
balance = balance.toFixed(2);
balance_counter.innerHTML = balance;
                              
balance = Number(balance);


                             
                             
                             }, 10);
        $('#sack').removeClass("playit");
        email_run = true;
    }
});

$( "#signup_username" ).focusout(function() {
    if( $(this).val() && username_run == false ) {
        setTimeout(function(){ $('#sack').addClass("playit");

balance = balance + .09;
balance = balance.toFixed(2);
balance_counter.innerHTML = balance;
balance = Number(balance);
                             
                             }, 10);
        $('#sack').removeClass("playit");
        username_run = true;
    }
});

$( "#signup_password" ).focusout(function() {
    if( $(this).val() && password_run == false ) {
        setTimeout(function(){ $('#sack').addClass("playit"); 
                             
balance = balance + .08;
balance = balance.toFixed(2);
balance_counter.innerHTML = balance;
balance = Number(balance);
                              
                             }, 10);
        $('#sack').removeClass("playit");
        password_run = true;
    }
});

/*jslint browser: true*/

/*global $, jQuery, alert*/

function copyLink() {
    var linkValue = document.getElementById("repoFolder").getAttribute('value');
    var link = $('<input>').val(linkValue).appendTo('body').select()
    document.execCommand('copy')
}


function loginFirst() {
    var desktoplogin = document.getElementById("desktoplogin");
    var mobilelogin = document.getElementById("mobilelogin");

    function flicker() {


        desktoplogin.style.backgroundColor = "#F87433";
        desktoplogin.style.color = "#FFF";
        mobilelogin.style.backgroundColor = "#F87433";
        mobilelogin.style.color = "#FFF";


        setTimeout(function () {
            desktoplogin.style.backgroundColor = "#FFF";

            desktoplogin.style.color = "#F87433";
            mobilelogin.style.backgroundColor = "#FFF";
            mobilelogin.style.color = "#F87433";
        }, 150);

        setTimeout(function () {
            desktoplogin.style.backgroundColor = "#F87433";
            desktoplogin.style.color = "#FFF";
            mobilelogin.style.backgroundColor = "#F87433";
            mobilelogin.style.color = "#FFF";
        }, 300);


        setTimeout(function () {
            desktoplogin.style.backgroundColor = "#FFF";

            desktoplogin.style.color = "#F87433";
            mobilelogin.style.backgroundColor = "#FFF";
            mobilelogin.style.color = "#F87433";
        }, 450);


    }


    flicker();


}


function inputFocus() {
    
    var camera = document.getElementById("posting-form-camera");
        var video = document.getElementById("posting-form-video");
    var location = document.getElementById("posting-form-location");

    camera.classList.add("colortransition");
    video.classList.add("colortransition");
    location.classList.add("colortransition");
}

function inputFocusOut() {
    var camera = document.getElementById("posting-form-camera");
    var video = document.getElementById("posting-form-video");
    var location = document.getElementById("posting-form-location");

    camera.classList.remove("colortransition");
    video.classList.remove("colortransition");
    location.classList.remove("colortransition");
}


function toTop() {
    $("html, body").animate({scrollTop: 0}, "slow");
    if ($(window).width() < 767) {
        closeMobileMenu();
    }
}


function closeMobileMenu() {
    var mobileInfo = document.getElementById("mobileinfo").classList;
    mobileInfo.remove("slidein");
    mobileInfo.add("slideout");
    bodyLocker.unlockPage();
}


// Close the dropdown menu if the user clicks outside of it
window.onclick = function (event) {
    if ($('.dropbtn').has($(event.target)).length <= 0 && !$('.dropbtn').is($(event.target))) {

        $(".dropdown-content").removeClass("show");
    }
}


function dropdownSort() {
    $("#dropdownsort").toggleClass("show");
    $("#dropdowntime").removeClass("show");
}


function dropdownTime() {
    $("#dropdowntime").toggleClass("show");
    $("#dropdownsort").removeClass("show");
}

function select_sort_btn(event) {
    $(event).parents('.dropdown').find('span').html($(event).html());
    $(event).parents('.dropdown').find('.dropdown-content a').removeClass('hide');
    $(event).addClass('hide');
}

$(document).ready(function () {

    /******************************
     ADD ANIMATION TO THE TEXT
     ******************************/
    animation();


    /*********************
     SET ROW'S COLOR
     *********************/
    $.each($('.row'), function () {
        var c = $(this).attr("data-color");
        $(this).css("background", c);
    });


    /******************
     SEARCH BOX
     ******************/
    var input = $('#search-wrap input'),
        sw = $('#search-wrap'),
        tw = $('.tags-wrap'),
        sr = $('#search-results');

    //show the search box
    $('#search-btn').click(function () {
        sw.addClass('active');
        input.focus();
    });

    //close the search box
    $('#content-wrap, #li-4').click(function () {
        //collapse bars
        fcollapse(sw, tw);

        //reset search
        input.val("");
        nsearch(sr);

    });

    //key up function
    input.keyup(function (e) {
        tw.addClass('active');

        //if input is 'EMPTY'
        if ($(this).val().length === 0) {
            tw.removeClass('active');
            nsearch(sr);
        }

        //if we press 'ENTER'
        if (e.keyCode === 13) {
            fsearch(input, sr, tw);
        }

        //if we press 'BACKSPACE'
        if (e.keyCode === 8) {
            nsearch(sr);
        }
    });


    /*TAGS CLICK*/
    $('.tags-wrap ul li').click(function () {
        var txt = $(this).text();
        input.val(txt);
        input.focus();
    });


    /*SEARCH CLICK*/
    $('#li-3').click(function () {
        fsearch(input, sr, tw);
    });


    /*************************
     ON WINDOW SCROLL FUNCTION
     *************************/
    var count = 0;
    $(window).scroll(function (event) {

        var scrolled = $(this).scrollTop();

        if (scrolled > count) {
            //scroll down
            count++;
            $('#nav-wrap').addClass('active');
        }
        else {
            //scroll up
            count--;
            $('#nav-wrap').removeClass('active');
        }

        //Check if we've reached the top
        count = scrolled;

        if (count === 0) {
            animation();
            $('#first-row i').removeClass('active');
        }
        else {
            $('h1 ,h2').removeClass('animationActive');
            $('#first-row i').addClass('active');
        }

        if (count === 200) {
            //collapse
            fcollapse(sw, tw);
        }

    });


    /*****************
     MOBILE
     *****************/
    var mob = $('#menu-mob');
    mob.click(function () {
        $(this).toggleClass('active');
        $('#nav-wrap ul li').toggleClass('mob');
    });


});//DOCUMENT READY


function mobileSearch() {

    var mobileImage = document.getElementById("mobileimageupload").classList;
    var mobileInfo = document.getElementById("mobileinfo").classList;
    var mobileNotifications = document.getElementById("mobilenotifications").classList;
    var mobileSearch = document.getElementById("mobilesearch").classList;
    var mobileProfile = document.getElementById("mobileprofile").classList;
    var body = document.getElementsByTagName("body")[0].classList;

    if (mobileNotifications.contains("displayblock")) {
        mobileNotifications.remove("displayblock");
        mobileNotifications.add("hiddentransition");

    }
    if (mobileImage.contains("displayblock")) {
        mobileImage.remove("displayblock");
        mobileImage.add("hiddentransition");
    }
    if (mobileProfile.contains("displayblock")) {
        mobileProfile.remove("displayblock");
        mobileProfile.add("hiddentransition");
    }

    if (mobileSearch.contains("displayblock")) {

        mobileSearch.remove("displayblock");
        mobileSearch.add("hiddentransition");
        mobileInfo.remove("slidein");
        mobileInfo.add("slideout");
        bodyLocker.unlockPage();
    } else {

        mobileSearch.add("displayblock");
        mobileSearch.remove("hiddentransition");
        mobileInfo.add("slidein");
        mobileInfo.remove("slideout");
        bodyLocker.lockPage();
    }

}


function uploadMobileImage() {
    var mobileImage = document.getElementById("mobileimageupload").classList;
    var mobileInfo = document.getElementById("mobileinfo").classList;
    var mobileNotifications = document.getElementById("mobilenotifications").classList;
    var mobileSearch = document.getElementById("mobilesearch").classList;
    var mobileProfile = document.getElementById("mobileprofile").classList;
    var isSafari = !!navigator.userAgent.match(/Version\/[\d.]+.*Safari/);
    var hideFixedItems = function () {
        $('.mobileheader').hide();
        $('.mobilemenu').hide();
    };

    var showFixedItems = function () {
        $('.mobileheader').show();
        $('.mobilemenu').show();
    };
    var textarea = $('#mobileimageupload textarea');

    if (mobileNotifications.contains("displayblock")) {
        mobileNotifications.remove("displayblock");
        mobileNotifications.add("hiddentransition");
    }

    if (mobileSearch.contains("displayblock")) {
        mobileSearch.remove("displayblock");
        mobileSearch.add("hiddentransition");
    }
    if (mobileProfile.contains("displayblock")) {
        mobileProfile.remove("displayblock");
        mobileProfile.add("hiddentransition");
    }


    if (mobileImage.contains("displayblock")) {

        mobileImage.remove("displayblock");
        mobileImage.add("hiddentransition");
        mobileInfo.remove("slidein");
        mobileInfo.add("slideout");
        bodyLocker.unlockPage();
        disableHideOnFocus();
    } else {
        mobileImage.add("displayblock");
        mobileImage.remove("hiddentransition");
        mobileInfo.add("slidein");
        mobileInfo.remove("slideout");
        bodyLocker.lockPage();
        enableHideOnFocus();
        window.dispatchEvent(new Event('resize'));
    }

    function enableHideOnFocus() {
        if (isSafari) {
            textarea.bind('focus', hideFixedItems);
            textarea.bind('focusout', showFixedItems);
        }
    }

    function disableHideOnFocus() {
        if (isSafari) {
            textarea.unbind('focus', hideFixedItems);
            textarea.unbind('focusout', showFixedItems);
        }
    }
}


function mobileNotifications() {

    var mobileNotifications = document.getElementById("mobilenotifications").classList;
    var mobileInfo = document.getElementById("mobileinfo").classList;
    var mobileImage = document.getElementById("mobileimageupload").classList;
    var mobileSearch = document.getElementById("mobilesearch").classList;
    var mobileProfile = document.getElementById("mobileprofile").classList;


    if (mobileImage.contains("displayblock")) {
        mobileImage.remove("displayblock");
        mobileImage.add("hiddentransition");
    }
    if (mobileSearch.contains("displayblock")) {
        mobileSearch.remove("displayblock");
        mobileSearch.add("hiddentransition");
    }
    if (mobileProfile.contains("displayblock")) {
        mobileProfile.remove("displayblock");
        mobileProfile.add("hiddentransition");
    }


    if (mobileNotifications.contains("displayblock")) {
        mobileNotifications.remove("displayblock");
        mobileNotifications.add("hiddentransition");
        mobileInfo.remove("slidein");
        mobileInfo.add("slideout");
        bodyLocker.unlockPage();

    } else {
        mobileNotifications.add("displayblock");
        mobileNotifications.remove("hiddentransition");
        mobileInfo.add("slidein");
        mobileInfo.remove("slideout");
        bodyLocker.lockPage();
    }
}


function closeMobileProfile() {
    if ($(window).width() < 767) {
        mobileProfile();
    }
}


function mobileProfile() {

    var mobileNotifications = document.getElementById("mobilenotifications").classList;
    var mobileInfo = document.getElementById("mobileinfo").classList;
    var mobileImage = document.getElementById("mobileimageupload").classList;
    var mobileSearch = document.getElementById("mobilesearch").classList;
    var mobileProfile = document.getElementById("mobileprofile").classList;


    if (mobileImage.contains("displayblock")) {
        mobileImage.remove("displayblock");
        mobileImage.add("hiddentransition");
    }
    if (mobileSearch.contains("displayblock")) {
        mobileSearch.remove("displayblock");
        mobileSearch.add("hiddentransition");
    }
    if (mobileNotifications.contains("displayblock")) {
        mobileNotifications.remove("displayblock");
        mobileNotifications.add("hiddentransition");
    }


    if (mobileProfile.contains("displayblock")) {
        mobileProfile.remove("displayblock");
        mobileProfile.add("hiddentransition");
        mobileInfo.remove("slidein");
        mobileInfo.add("slideout");
        bodyLocker.unlockPage();
    } else {
        mobileProfile.add("displayblock");
        mobileProfile.remove("hiddentransition");
        mobileInfo.add("slidein");
        mobileInfo.remove("slideout");
        bodyLocker.lockPage();
    }
}


$(function () {

    $("body").on("click", ".user-profile-contain", function () {
        $(".msearchresults").addClass("hidden");
    });
});

$(function () {
    $("body").on("focusin", "#mobilesearch1", function () {
        $(".msearchresults").removeClass("hidden");
    });
});


//resize
// $(window).on('resize', function(){
//
// });


/*Animation Text*/
function animation() {
    $('h1').addClass('animationActive');

    var count = 0;
    var limit = 10;

    var interval = setInterval(function () {
        count++;
        if (count === limit) {
            clearInterval(interval);
            $('h2').addClass('animationActive');
        }
    }, 50);
}

/*Collapse Bars Function*/
function fcollapse(sw, tw) {
    sw.removeClass('active');
    tw.removeClass('active');
}

/*Search Function*/
function fsearch(input, sr, tw) {
    var r = input.val();
    $('.tags-wrap ul').css('display', 'none');
    sr.html('Sorry, 0 results were found for: <span> "' + r + '"</span>');
    tw.addClass('active');
}

/*Clear/New function*/
function nsearch(sr) {
    sr.html('');
    $('.tags-wrap ul').css('display', 'inline-block');
}

var bodyLocker = {
    scrollTop: 0,
    body: document.getElementsByTagName("body")[0],
    lockPage: function () {
        this.scrollTop = window.pageYOffset;
        this.body.classList.add('modal-open');
        this.body.style.top = -this.scrollTop + "px";
    },
    unlockPage: function () {
        var self = this;

        this.body.classList.remove('modal-open');
        this.body.style.top = 0;

        window.scrollTo(0, this.scrollTop);
        window.setTimeout(function () {
            self.scrollTop = null;
        }, 0);
    }
};

$(window).scroll(function() {
  if ($(window).scrollTop() > 500) {
    $('#top_to_button').addClass('show');
  } else {
    $('#top_to_button').removeClass('show');
  }
});
