# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


class BelaBonus(models.Model):
    starter_balance = models.FloatField(
        blank=True,
        default=0.0,
        help_text='the balance of a new user will be filled with this balance'
    )
    deposit_bonus = models.IntegerField(
        default=5,
        blank=True,
        help_text='if user deposits 100 Bela, he gets 105'
    )

    # may be, we should offload these two fields into Base Model or so.
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(
        User, related_name='user_created_bela_bonuses', null=True, blank=True
    )
    updated_by = models.ForeignKey(
        User, related_name='user_updated_bela_bonuses', null=True, blank=True
    )

    def __str__(self):
        return 'starter balance: %s, deposit_bonus: %s' % (
            self.starter_balance, self.deposit_bonus
        )

    class Meta:
        verbose_name_plural = 'Bela Bonus'


def seed_starter_balance(self):
    bonus = BelaBonus.objects.last()
    if bonus:
        self.user1.belacoin = bonus.starter_balance
        self.user1.save()


def update_deposit_bonus(self, deposit_amount=None):
    bonus = BelaBonus.objects.last()
    if bonus and deposit_amount:

        bonus_percentage = bonus.deposit_bonus
        bonus_amount = (deposit_amount * bonus_percentage / 100) + deposit_amount

        self.user1.belacoin += float(bonus_amount)
        self.user1.save()


User.add_to_class('seed_starter_balance', seed_starter_balance)
User.add_to_class('update_deposit_bonus', update_deposit_bonus)
