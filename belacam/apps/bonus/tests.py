# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

from .models import BelaBonus


class TestBelaBonus(TestCase):

    def setUp(self):
        self.bonus_instance = BelaBonus.objects.create(
            starter_balance=2.0, deposit_bonus=12
        )

    def test_starter_balance(self):
        # user signs up
        pass

    def test_deposit_bonus(self):
        # user deposits
        pass
