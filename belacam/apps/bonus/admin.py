# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import BelaBonus
from .admin_forms import BelaBonusForm

@admin.register(BelaBonus)
class BelaBonusAdmin(admin.ModelAdmin):
    form = BelaBonusForm
    list_display = ('id', 'starter_balance', 'deposit_bonus', 'created_at', 'updated_at', 'created_by', 'updated_by')

    def save_model(self, request, obj, form, change):
        if not obj.created_by:
            obj.created_by = request.user

        if form.changed_data:
            obj.updated_by = request.user

        return super(BelaBonusAdmin, self).save_model(request, obj, form, change)
