from dal import autocomplete
from django import forms
from belacam.apps.bonus.models import *

class BelaBonusForm(forms.ModelForm):
    class Meta:
        model = BelaBonus
        fields = ('__all__')
        widgets = {
            'created_by': autocomplete.ModelSelect2(url='user-autocomplete'),
            'updated_by': autocomplete.ModelSelect2(url='user-autocomplete'),
        }
