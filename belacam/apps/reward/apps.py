from __future__ import unicode_literals

from django.apps import AppConfig


class RewardTaskConfig(AppConfig):
    name = 'belacam.apps.reward'
