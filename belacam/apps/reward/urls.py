from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'reward/$', views.RewardTaskView.as_view(), name='get_reward_tasks'),
]