from __future__ import unicode_literals

# Create your models here.
from django.db import models
from django.utils.translation import ugettext_lazy as _

from .choices import RewardTaskType

from .compat import User


class Task(models.Model):
    type = models.CharField(_('Type'), max_length=50,
                            choices=RewardTaskType.choices(), db_index=True, default=None)
    description = models.CharField(max_length=200)
    reward = models.FloatField(default=0)
    users = models.ManyToManyField(User, related_name='reward_tasks', through='UserTask')


class UserTaskManager(models.Manager):
    def complete_task(self, user, task):
        user_task = UserTask.objects.filter(user=user, task=task).first()
        if user_task is not None:
            return None

        user_task = UserTask(user=user, task=task)
        user_task.save()
        return user_task


class UserTask(models.Model):
    objects = UserTaskManager()
    task = models.ForeignKey(Task)
    user = models.ForeignKey(User)
    created_at = models.DateTimeField(null=True, blank=True, auto_now_add=True, db_index=True)
