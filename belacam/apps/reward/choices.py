from belacam.apps.core.enum import ChoiceEnum


class RewardTaskType(ChoiceEnum):
    GET_PROFILE_PIC = 'Get Profile Pic'
    POST_PHOTO = 'Post a photo'
    GIVE_LIKE = 'Give a like'
