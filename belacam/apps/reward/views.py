from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from rest_framework import permissions
from rest_framework.generics import ListAPIView

from .models import Task
from .serializers import RewardTaskSerializer


class RewardTaskView(ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Task.objects.all()
    serializer_class = RewardTaskSerializer

    def get_serializer_context(self):
        user = self.request.user
        completed_tasks = user.reward_tasks.all().values_list('id', flat=True)
        return {
            'completed_tasks': completed_tasks
        }

