from rest_framework import serializers

from .models import Task


class RewardTaskSerializer(serializers.ModelSerializer):
    is_complete = serializers.SerializerMethodField()

    def get_is_complete(self, obj):
        completed_tasks = self.context.get('completed_tasks', [])
        return obj.id in completed_tasks

    class Meta:
        model = Task
        fields = '__all__'
