from django.contrib import admin

from belacam.apps.reward.models import Task, UserTask
from belacam.apps.reward.admin_forms import *

class UserTaskAdmin(admin.ModelAdmin):
    form = UserTaskForm


admin.site.register(Task)
admin.site.register(UserTask, UserTaskAdmin)
