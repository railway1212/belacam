# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2018-08-17 03:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reward', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='type',
            field=models.CharField(choices=[(b'GET_PROFILE_PIC', b'Get Profile Pic'), (b'GIVE_LIKE', b'Give a like'), (b'POST_PHOTO', b'Post a photo')], db_index=True, default=None, max_length=50, verbose_name='Type'),
        ),
    ]
