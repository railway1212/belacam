from dal import autocomplete
from django import forms
from belacam.apps.reward.models import *

class UserTaskForm(forms.ModelForm):
    class Meta:
        model = UserTask
        fields = ('__all__')
        widgets = {
            'user': autocomplete.ModelSelect2(url='user-autocomplete'),
        }
