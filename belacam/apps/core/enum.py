from __future__ import absolute_import

from enum import Enum, IntEnum


class ChoiceEnum(Enum):
    @classmethod
    def choices(cls):
        return tuple((x.name, x.value) for x in cls)


class IntChoiceEnum(IntEnum):
    @classmethod
    def choices(cls):
        return tuple((x.value, x.name) for x in cls)
