from django.conf.urls import url
from belacam.apps.core import views

urlpatterns = [
   url(r'', views.AngularView.as_view(), name='angular_main'),
]
