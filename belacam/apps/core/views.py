# -*- coding: utf-8 -*-
from django.views.generic import TemplateView
from django.conf import settings

from belacam.apps.thoughts.models import AppSetting
from belacam.apps.bonus.models import BelaBonus


class AngularView(TemplateView):
    template_name = "main.html"

    def get_context_data(self):
        app_settings = AppSetting.objects.last()
        bela_bonus = BelaBonus.objects.last()

        return {
            'requesting_user': self.request.user,
            'DEBUG': settings.DEBUG,
            'GOOGLE_RECAPTCHA_SITE_KEY': settings.GOOGLE_RECAPTCHA_SITE_KEY,
            'starter_balance': bela_bonus.starter_balance,
            'bella_average_price': app_settings.bella_average_price
        }

