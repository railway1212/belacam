import datetime

from account.signals import password_changed
from account.signals import user_sign_up_attempt, user_signed_up
from account.signals import user_login_attempt, user_logged_in
from account.signals import email_confirmed
from pinax.eventlog.models import log

from django.contrib.auth import get_user_model
from django.db.models.signals import pre_save
from django.dispatch import receiver

from belacam.apps.referral.choices import UserReferrerStatus
from belacam.apps.thoughts import mail_composer
from belacam.apps.thoughts.models import UserProfile


User = get_user_model()


@receiver(user_logged_in)
def handle_user_logged_in(sender, **kwargs):
    log(
        user=kwargs.get("user"),
        action="USER_LOGGED_IN",
        extra={}
    )


@receiver(password_changed)
def handle_password_changed(sender, **kwargs):
    log(
        user=kwargs.get("user"),
        action="PASSWORD_CHANGED",
        extra={}
    )


@receiver(user_login_attempt)
def handle_user_login_attempt(sender, **kwargs):
    log(
        user=None,
        action="LOGIN_ATTEMPTED",
        extra={
            "username": kwargs.get("username"),
            "result": kwargs.get("result")
        }
    )


@receiver(user_sign_up_attempt)
def handle_user_sign_up_attempt(sender, **kwargs):
    log(
        user=None,
        action="SIGNUP_ATTEMPTED",
        extra={
            "username": kwargs.get("username"),
            "email": kwargs.get("email"),
            "result": kwargs.get("result")
        }
    )


@receiver(user_signed_up)
def handle_user_signed_up(sender, **kwargs):
    log(
        user=kwargs.get("user"),
        action="USER_SIGNED_UP",
        extra={}
    )


@receiver(email_confirmed)
def handle_email_confirmed(sender, **kwargs):
    try:
        user = User.objects.filter(email=kwargs.get("email_address").email).first()
        up = UserProfile.objects.filter(user=user).first()
        up.is_bot = False
        up.save(update_fields=['is_bot'])

        user_referrer = user.user_referrer
        if user_referrer and user_referrer.status == UserReferrerStatus.PENDING.name:
            user_referrer.status = UserReferrerStatus.VERIFIED.name
            user_referrer.save()

            created_by = user_referrer.referrer.created_by
            if created_by:
                created_by.user1.belacoin += user_referrer.belas
                created_by.user1.save()

        log(
            user=user,
            action="USER_EMAIL_CONFIRMED",
            extra={}
        )
    except Exception as e:
        pass


@receiver(pre_save, sender=User)
def handle_user_save(sender, **kwargs):
    user = kwargs.get('instance')

    if not user.id:
        return

    old_user = User.objects.get(id=user.id)

    if not user.is_active and old_user.is_active:
        mail = mail_composer.compose_ban_email(user)
        mail_composer.send_html_mail('Your Belacam Account has been Banned', [user.email], mail)

    if user.is_active and not old_user.is_active:
        # TODO: Send active email
        pass


@receiver(pre_save, sender=UserProfile)
def handle_user_profile_save(sender, **kwargs):
    user_profile = kwargs.get('instance')

    try:
        old_user_profile = UserProfile.objects.get(user_id=user_profile.user_id)
    except UserProfile.DoesNotExist:
        return

    user = user_profile.user

    if user_profile.suspend_until and user_profile.suspend_until != old_user_profile.suspend_until:
        date_now = datetime.datetime.now(user_profile.suspend_until.tzinfo)
        time = user_profile.suspend_until - date_now

        if time.total_seconds() > 0:
            mail = mail_composer.compose_suspend_email(user_profile.user, time)
            mail_composer.send_html_mail('Your Belacam account has been temporarily suspended', [user.email], mail)

