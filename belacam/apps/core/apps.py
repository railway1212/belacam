# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from importlib import import_module

from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'belacam.apps.core'

    def ready(self):
        import_module("belacam.apps.core.receivers")
