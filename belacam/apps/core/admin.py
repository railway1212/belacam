from urllib import urlencode

from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.core import urlresolvers
from django.db.models import Count, Max


class CustomUserAdmin(UserAdmin):

    list_display = ('username', 'email', 'is_active', 'date_joined', 'get_user_ip', 'total_sessions_count',
                    'last_session_date')

    readonly_fields = ('date_joined',)

    def get_queryset(self, request):
        qs = super(CustomUserAdmin, self).get_queryset(request)
        qs = qs.annotate(num_sessions=Count('sessions')).annotate(last_session_date=Max('sessions__end_at'))
        return qs

    def get_user_ip(self, instance):
        return instance.user1.user_ip

    get_user_ip.short_description = 'Registration IP Address'
    get_user_ip.admin_order_field = 'user1__user_ip'

    def total_sessions_count(self, instance):
        querydict = {'user__id__exact': instance.id}
        session_url = '{}?{}'.format(urlresolvers.reverse('admin:session_tracking_faketrack_changelist'),
                                     urlencode(querydict))
        return '<a href="{}">{}</a>'.format(session_url, str(instance.num_sessions))

    total_sessions_count.admin_order_field = 'num_sessions'
    total_sessions_count.short_description = 'Total Session Count'
    total_sessions_count.allow_tags = True

    def last_session_date(self, instance):
        date = instance.last_session_date
        if date is not None:
            date = date.date()
        return date

    last_session_date.admin_order_field = 'last_session_date'
    last_session_date.short_description = 'Last Session Date'


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
