import json
import boto3
import requests
import urllib

from django.http import HttpResponseRedirect
from django.utils.html import strip_tags
from django.conf import settings
from django.template.loader import get_template


def get_native_language_via_country_code(country_code='US'):
    for lang, countries in settings.LANGUAGE_COUNTRY_MAPPER.items():
        if country_code in countries:
            return lang

    # default language
    return 'en'


def format_td(td_object, separator=", "):
    seconds = int(td_object.total_seconds())
    periods = [
        ('year', 60 * 60 * 24 * 365),
        ('month', 60 * 60 * 24 * 30),
        ('day', 60 * 60 * 24),
        ('hour', 60 * 60),
        ('minute', 60),
        ('second', 1)
    ]

    strings = []
    for period_name, period_seconds in periods:
        if seconds > period_seconds:
            period_value, seconds = divmod(seconds, period_seconds)
            has_s = 's' if period_value > 1 else ''
            strings.append("%s %s%s" % (period_value, period_name, has_s))

    return separator.join(strings)


def custom_redirect(url_name, *args, **kwargs):
    params = urllib.urlencode(kwargs)
    return HttpResponseRedirect(url_name + "?%s" % params)


def safe_cast(val, to_type, default=None):
    try:
        return to_type(val)
    except (ValueError, TypeError):
        return default


def get_upload_url(image_path):
    try:
        boto_client = boto3.client('s3',
                                   aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                                   aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)
        # User can't trick system by insert url of another client
        params = {
            'Bucket': settings.AWS_STORAGE_BUCKET_NAME,
            'Key': image_path
        }
        expiration = settings.AWS_QUERYSTRING_EXPIRE
        url = boto_client.generate_presigned_url('get_object', Params=params, ExpiresIn=expiration)
        return url
    except Exception:
        return ""


def upload_aws_photo(photo_data, image_type):
    data = dict(bucket=settings.AWS_STORAGE_BUCKET_NAME, key=photo_data, image_type=image_type)

    result = requests.post(
        'https://j91our9x8i.execute-api.us-east-1.amazonaws.com/dev/',
        headers={'x-api-key': 've1nfBJ4xl2qhNDVmDN7B96EtGqnfBed9pzt8bc1'},
        data=json.dumps(data))

    return result


def compose_improper_address_denied_mail(user):
    template = get_template('mails/improper_address_denied.html')

    html_str = template.render({
        'username': user.username,
    })

    text_str = strip_tags(html_str)

    return {
        'message': text_str,
        'html_message': html_str
    }


def is_aws_successful(response):
    return response.status_code == 200 and response.json().get('status', None) == 'success'