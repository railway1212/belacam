from belacam.apps.core.enum import ChoiceEnum, IntChoiceEnum

# todo: remove enum dependency


class WithdrawStatus(ChoiceEnum):
    PENDING = 'Pending Confirmation'
    CONFIRMED = 'Confirmed'
    FULFILLED = 'Fulfilled'
    SENT = 'Sent'
    BEING_PROCESSED = 'Being Processed'
    IMPROPER_ADDRESS = 'Improper Address'


class ActivityType(IntChoiceEnum):
    Like = 1
    UnLike = 2
    Comment = 3
    Post = 4
    Follow = 5
    UnFollow = 6
    Report = 7
    Block = 8
    InstagramPost = 9


class InstagramUserStatus(ChoiceEnum):
    CREATED = 'Created'
    DELETED = 'Deleted'
