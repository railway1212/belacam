from taggit.models import Tag
from friendship.models import Follow
from notifications.models import Notification
from rest_framework import serializers

from django.db.models.functions import Coalesce
from django.db.models import Count, Q, Case, Sum, When, IntegerField
from django.utils import timezone

from .models import *
from belacam.apps.core.utils import get_upload_url
from .serializer_fields import PointFieldSerializer


class TagSerializer(serializers.ModelSerializer):
    num_items = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()

    def get_type(self, obj):
        return self.context.get('type', 'tag')

    def get_num_items(self, obj):
        return obj.num_items

    class Meta:
        model = Tag
        exclude = ()


class UserProfileSerializer(serializers.ModelSerializer):
    account_balance = serializers.SerializerMethodField()
    cover_pic = serializers.SerializerMethodField()

    def get_account_balance(self, obj):
        return AppSetting.objects.values_list("bella_average_price", flat=True).first()*obj.belacoin

    def get_cover_pic(self, obj):
        if not obj.cover_pic:
            return get_upload_url('assets/images/c-pic.jpg')
        return get_upload_url(obj.cover_pic)

    class Meta:
        model = UserProfile
        exclude = ('blocked_people',)


class UserWithoutProfileSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField()
    full_name = serializers.SerializerMethodField()
    is_following = serializers.SerializerMethodField()

    def get_full_name(self, obj):
        full_name = obj.get_full_name()
        if not full_name:
            full_name = obj.username
        return full_name
    
    def get_avatar(self, obj):
        return "<img src='" + get_upload_url(obj.user1.profile_pic) + "'></img>"

    def get_is_following(self, obj):
        followings = self.context.get('cur_user_followings', [])
        try:
            return obj.id in followings
        except Exception:
            return False

    class Meta:
        model = User
        fields = ('id', 'full_name', 'username', 'avatar', 'is_following')


class UserWithSmallAvatarSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField()
    full_name = serializers.SerializerMethodField()
    is_following = serializers.SerializerMethodField()

    def get_full_name(self, obj):
        full_name = obj.get_full_name()
        if not full_name:
            full_name = obj.username
        return full_name

    def get_avatar(self, obj):
        return "<img src='" +get_upload_url(obj.user1.profile_pic) + "'></img>"

    def get_is_following(self, obj):
        try:
            return obj.id in self.context['cur_user_followings']
        except Exception:
            return False

    class Meta:
        model = User
        fields = ('id', 'full_name', 'username', 'avatar', 'is_following')


class UserProfileSerializerWithoutSensiveInfo(serializers.ModelSerializer):
    cover_pic = serializers.SerializerMethodField()

    def get_cover_pic(self, obj):
        if not obj.cover_pic:
            return get_upload_url('assets/images/c-pic.jpg')
        return get_upload_url(obj.cover_pic)

    class Meta:
        model = UserProfile
        exclude = ('blocked_people', 'belacoin', 'belacoin_address', 'accepted_terms_of_service', 'phone_number')


class UserSerializer(serializers.ModelSerializer):
    user1 = serializers.SerializerMethodField()
    avatar = serializers.SerializerMethodField()
    avatar_url = serializers.SerializerMethodField()
    full_name = serializers.SerializerMethodField()
    belas = serializers.SerializerMethodField()
    earnings = serializers.SerializerMethodField()
    thought_count = serializers.SerializerMethodField()
    bella_per_like = serializers.SerializerMethodField()
    verified = serializers.SerializerMethodField()
    is_following = serializers.SerializerMethodField()
    total_deposit = serializers.SerializerMethodField()

    def get_user1(self, obj):
        request = self.context.get('request', None)
        if request is not None:
            user = request.user
        else:
            user = None

        if user == obj:
            return UserProfileSerializer(
                obj.user1,
            ).data
        else:
            return UserProfileSerializerWithoutSensiveInfo(
                obj.user1,
            ).data

    def get_full_name(self, obj):
        full_name = obj.get_full_name()
        if not full_name:
            full_name = obj.username
        return full_name
    
    def get_avatar(self, obj):
        return "<img src='" + get_upload_url(obj.user1.profile_pic) + "'></img>"

    def get_avatar_url(self, obj):
        return get_upload_url(obj.user1.profile_pic)

    def get_belas(self, obj):
        try:
            return obj.total_bellasum
        except Exception:
            return 0

    def get_earnings(self, obj):
        try:
            return AppSetting.objects.values_list("bella_average_price", flat=True).first() * obj.total_bellasum
        except Exception:
            return 0

    def get_thought_count(self, obj):
        try:
            return obj.thought_count
        except Exception:
            return 0

    def get_bella_per_like(self, obj):
        return AppSetting.objects.values_list("bella_per_like", flat=True).first()

    def get_verified(self, obj):
        try:
            return EmailAddress.objects.filter(user=obj).first().verified
        except Exception:
            return False

    def get_is_following(self, obj):
        try:
            if self.context['request'].user.is_authenticated():
                return Follow.objects.filter(follower=self.context['request'].user, followee=obj).exists()
            else:
                return False
        except Exception:
            return False

    def get_total_deposit(self, obj):
        try:
            if self.context['request'].user.is_authenticated():
                result = BellaDeposit.objects.filter(user=obj).aggregate(total_deposit=Sum('deposit_amount'))
                return result['total_deposit']
            else:
                return 0
        except Exception:
            return 0

    @staticmethod
    def setup_eager_loading(queryset):
        queryset = queryset.select_related(
            'user1'
        )
        queryset = queryset.annotate(
            total_bellasum=Coalesce(Sum('bellas__bellalike__bella_per_like'), 0),
            thought_count=Count('bellas__id', distinct=True)
        )
        return queryset

    class Meta:
        model = User
        fields = ('id',
                  'full_name',
                  'user1',
                  'username',
                  'avatar',
                  'avatar_url',
                  'belas',
                  'earnings',
                  'thought_count',
                  'bella_per_like',
                  'is_superuser',
                  'verified',
                  'is_following',
                  'total_deposit')


class NotificationSerializer(serializers.ModelSerializer):
    actor_thumbnail = serializers.SerializerMethodField()
    actor = serializers.SerializerMethodField()
    actor_user = serializers.SerializerMethodField()
    action = serializers.SerializerMethodField()
    action_id = serializers.SerializerMethodField()
    username = serializers.SerializerMethodField()
    action_object = serializers.SerializerMethodField()

    def get_actor_thumbnail(self, obj):
        try:
            return "<img src='" + get_upload_url(obj.actor.user1.profile_pic) + "'></img>"
        except Exception:
            return "<img src='" + get_upload_url("assets/images/profile.png") + "'></img>"

    def get_actor(self, obj):
        try:
            return str(obj.actor)
        except Exception:
            return ""

    def get_actor_user(self, obj):
        if isinstance(obj.actor, User):
            return UserWithoutProfileSerializer(obj.actor, context=self.context).data
        else:
            return None

    def get_action(self, obj):
        try:
            return obj.action_object.__class__.__name__.lower()
        except Exception:
            return ""

    def get_action_id(self, obj):
        try:
            class_name = obj.action_object.__class__.__name__.lower()
            if class_name == "bella":
                return obj.action_object.id
            elif class_name == "comment":
                return obj.action_object.post.id
            elif class_name == "user":
                return obj.action_object.id
            else:
                return ""
        except Exception:
            return ""

    def get_username(self, obj):
        try:
            class_name = obj.action_object.__class__.__name__.lower()
            if class_name == "bella":
                return obj.action_object.user.username
            elif class_name == "comment":
                return obj.action_object.post.user.username
            elif class_name == "user":
                return obj.action_object.username
            else:
                return ""
        except Exception:
            return ""

    def get_action_object(self, obj):
        try:
            return unicode(obj.action_object)
        except Exception:
            return ""

    class Meta:
        model = Notification
        exclude = ()


class UserWithPreviewSerializer(UserSerializer):
    preview = serializers.SerializerMethodField()

    def get_preview(self, obj):
        custom_qs = Bella.objects.filter(user=obj).exclude(
                        reports__id__in=[self.context['request'].user.id]
                    ).order_by('-created_at')[:3]
        custom_qs = BellaSerializer.setup_eager_loading(custom_qs, self.context['request'].user)
        return BellaSerializer(
            custom_qs,
            many=True,
            context=self.context
        ).data

    class Meta:
        model = User
        fields = ('id', 'full_name', 'username', 'avatar', 'earnings', 'preview',)


class TagWithPreviewSerializer(serializers.ModelSerializer):
    preview = serializers.SerializerMethodField()
    tag = serializers.SerializerMethodField()
    posting_count = serializers.SerializerMethodField()

    def get_preview(self, obj):
        blocked_people_ids = list(self.context['request'].user.user1.blocked_people.all().values_list('id', flat=True))

        custom_qs = Bella.objects.filter(
            tags__name__in=[obj.name], created_at__gte=(timezone.now() - timezone.timedelta(hours=24))
        ).exclude(
            user_id__in=blocked_people_ids
        ).annotate(
            most_liked=Count('likes')
        ).order_by(
             '-most_liked'
        )[:4]

        custom_qs = BellaSerializer.setup_eager_loading(custom_qs, self.context['request'].user)

        return BellaSerializer(
            custom_qs,
            many=True,
            context=self.context
        ).data

    def get_tag(self, obj):
        return obj.name

    def get_posting_count(self, obj):
        return obj.number_of_tag

    class Meta:
        model = Tag
        fields = ('id', 'preview', 'tag', 'posting_count')


class CommentSerializer(serializers.ModelSerializer):
    author = UserWithoutProfileSerializer(read_only=True)

    class Meta:
        model = Comment
        exclude = ('post',)


class GeoPlaceSerializer(serializers.ModelSerializer):
    location = PointFieldSerializer()

    class Meta:
        model = GeoPlace
        fields = ('id', 'name', 'asciiname', 'location',)


class BellaSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    likes = serializers.SerializerMethodField()
    total_likes_count = serializers.SerializerMethodField()
    comments = CommentSerializer(many=True)
    belas = serializers.SerializerMethodField()
    earnings = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()
    video = serializers.SerializerMethodField()
    is_liked = serializers.SerializerMethodField()

    def get_user(self, obj):
        return UserWithoutProfileSerializer(obj.user, context=self.context).data

    def get_likes(self, obj):
        return UserWithSmallAvatarSerializer(
            obj.likes.select_related('user1').order_by('bellalike__created_at')[:20],
            many=True,
            context=self.context
        ).data

    def get_image(self, obj):
        try:
            if obj.user != self.context['request'].user:
                BellaView.objects.get_or_create(bella=obj, user=self.context['request'].user)
        except Exception:
            pass
        finally:
            return get_upload_url(obj.image_path)

    def get_video(self, obj):
        try:
            if obj.user != self.context['request'].user:
                BellaView.objects.get_or_create(bella=obj, user=self.context['request'].user)
        except Exception:
            pass
        finally:
            return get_upload_url(obj.video_path)

    def get_belas(self, obj):
        try:
            return obj.total_bellasum
        except Exception:
            return 0

    def get_total_likes_count(self, obj):
        try:
            return obj.total_likes_count
        except Exception:
            return 0

    def get_earnings(self, obj):
        try:
            return self.context['bella_average_price'] * obj.total_bellasum
        except Exception:
            return 0

    def get_is_liked(self, obj):
        try:
            return obj.is_liked > 0
        except Exception:
            return 0

    @staticmethod
    def setup_eager_loading(queryset, cur_user):
        if cur_user and cur_user.is_authenticated():
            cur_user_id = [cur_user.id]
            queryset = queryset.annotate(
                total_bellasum=Coalesce(Sum('bellalike__bella_per_like'), 0),
                total_likes_count=Count('likes'),
                is_liked=Sum(Case(
                    When(Q(bellalike__user_id__in=cur_user_id), then=1),
                    default=0,
                    output_field=IntegerField()
                ))
            )
        else:
            queryset = queryset.annotate(
                total_bellasum=Coalesce(Sum('bellalike__bella_per_like'), 0),
                total_likes_count=Count('likes'),
            )

        queryset = queryset.select_related('user', 'user__user1')

        queryset = queryset.prefetch_related(
            'likes',
            'likes__user1',
            'comments',
            'comments__author',
            'comments__author__user1',
            'views'
        )
        return queryset

    class Meta:
        model = Bella
        exclude = ('reports',)


class TagSearchSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()
    
    @staticmethod
    def get_type(obj):
        return 'tag'

    class Meta:
        model = Tag
        fields = ('type', 'name', 'id')


class UserSearchSerializer(serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()
    earnings = serializers.SerializerMethodField()
    is_follower = serializers.SerializerMethodField()
    is_followee = serializers.SerializerMethodField()

    def get_full_name(self, obj):
        full_name = obj.get_full_name()
        if not full_name:
            full_name = obj.username
        return full_name

    def get_earnings(self, obj):
        try:
            return self.context['bella_average_price'] * obj.total_bellasum
        except Exception:
            return 0

    def get_is_follower(self, obj):
        return obj.id in self.context['cur_user_followers_list']

    def get_is_followee(self, obj):
        return obj.id in self.context['cur_user_followings_list']

    def get_type(self, obj):
        return self.context.get('type', 'user')

    class Meta:
        model = User
        fields = ('id', 'full_name', 'username', 'type', 'earnings', 'is_follower', 'is_followee')


class WithdrawRequestSerializer(serializers.ModelSerializer):
    author = UserSerializer(read_only=True)

    class Meta:
        model = WithdrawRequest
        fields = '__all__'
        read_only_fields = ('status', 'author', 'fee', 'owed', 'created_at',)


class WithdrawFeeSettingSerializer(serializers.ModelSerializer):

    class Meta:
        model = WithdrawFeeSetting
        fields = '__all__'
