# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime

from taggit_autosuggest.managers import TaggableManager
from account.models import EmailAddress

from django.contrib.gis.db.models import PointField
from django.conf import settings
from django.contrib.auth.models import User
from django.utils import timezone
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinValueValidator
from django.utils.encoding import python_2_unicode_compatible
from django.db.models import Sum, F

from belacam.apps.thoughts.choices import ActivityType, WithdrawStatus, InstagramUserStatus
from belacam.apps.thoughts.mail_composer import send_html_mail
from belacam.apps.core.utils import get_upload_url, compose_improper_address_denied_mail


AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


@python_2_unicode_compatible
class Bella(models.Model):
    content = models.CharField(max_length=1500)
    user = models.ForeignKey(AUTH_USER_MODEL, related_name="bellas")
    created_at = models.DateTimeField(auto_now_add=True, blank=True, db_index=True)
    tags = TaggableManager(blank=True, related_name="bellas")
    likes = models.ManyToManyField(AUTH_USER_MODEL, related_name='likes', through='BellaLike')
    views = models.ManyToManyField(AUTH_USER_MODEL, related_name='views', through='BellaView')
    reports = models.ManyToManyField(AUTH_USER_MODEL, blank=True, related_name='reports')
    rank_number = models.FloatField(default=0)
    location = models.TextField(default="", blank=True, null=True, db_index=True)
    image_path = models.TextField(null=True)
    video_path = models.TextField(default='')
    processed = models.BooleanField(default=False)

    @property
    def total_likes(self):
        """
        Likes for the company
        :return: Integer: Likes for the company
        """
        return self.likes.count()

    @property
    def total_bella(self):
        """
        How many BELA this bella has earned
        :return: Float: Total BELA for all the likes of this bella object
        """
        try:
            return float(self.bellalike_set.aggregate(total_bella=Sum('bella_per_like'))["total_bella"])
        except Exception:
            return 0

    @property
    def total_reports(self):
        """
        How many times this bella has been reported
        :return: Integer: Number of times
        """
        return self.reports.count()

    @property
    def total_views(self):
        """
        How many users has viewed this bella
        :return: Integer: Number of users
        """
        return self.views.count()

    def __unicode__(self):
        text = '%s %s' % (self.user.username, self.content)
        return text

    def __str__(self):
        return '[%s]: %s' % (self.user.username, self.content[:30] + (self.content[30:] and '...'))


class BellaLike(models.Model):
    bella = models.ForeignKey(Bella)
    user = models.ForeignKey(AUTH_USER_MODEL)
    bella_per_like = models.FloatField(validators=[MinValueValidator(0)], default=0.5)
    created_at = models.DateTimeField(null=True, blank=True, auto_now_add=True, db_index=True)


class BellaView(models.Model):
    bella = models.ForeignKey(Bella)
    user = models.ForeignKey(AUTH_USER_MODEL)
    created_at = models.DateTimeField(null=True, blank=True, auto_now_add=True, db_index=True)

    class Meta:
        unique_together = (('bella', 'user'),)


class UserRequestStat(models.Model):
    user = models.ForeignKey(AUTH_USER_MODEL, related_name="stats")
    date = models.DateField(auto_now_add=True, db_index=True)
    requests = models.IntegerField(default=0)

    class Meta:
        verbose_name = "User Request Statistic"
        verbose_name_plural = "User Request Statistics"


class UserProfile(models.Model):
    gender = 5
    user = models.OneToOneField(AUTH_USER_MODEL, unique=True, primary_key=True, related_name="user1")
    belacoin = models.FloatField(default=0)
    bio = models.TextField(default="", blank=True, null=True)
    accepted_terms_of_service = models.BooleanField(default=True)
    belacoin_address = models.TextField(default="No Address, Contact Support")
    blocked_people = models.ManyToManyField(AUTH_USER_MODEL, blank=True, related_name='blocked')
    cover_pic = models.TextField(max_length=1500, default="assets/images/c-pic.jpg")
    profile_pic = models.TextField(max_length=1500, default="assets/images/profile.png")
    number_of_posts = models.IntegerField(default=0)
    likes_count = models.IntegerField(default=0, db_index=True)
    like_update_time = models.DateTimeField(auto_now_add=True, db_index=True)
    is_bot = models.BooleanField(default=True)
    suspend_until = models.DateTimeField(blank=True, null=True)
    user_ip = models.GenericIPAddressField(blank=True, null=True)
    send_weekly_email = models.BooleanField(default=True)
    phone_number = models.CharField(max_length=20, blank=True, null=True)

    @property
    def avatar_url(self):
        try:
            return get_upload_url(self.profile_pic)
        except Exception:
            return ""

    @property
    def belas(self):
        try:
            return float(Bella.objects.filter(user=self.user).aggregate(
                total_bella=Sum('bellalike__bella_per_like')
            )["total_bella"])
        except Exception:
            return 0

    @property
    def earnings(self):
        return AppSetting.objects.values_list("bella_average_price", flat=True).first() * self.belas

    @property
    def thought_count(self):
        return self.user.bellas.count()

    @property
    def bella_per_like(self):
        return AppSetting.objects.values_list("bella_per_like", flat=True).first()

    @property
    def full_name(self):
        full_name = self.user.get_full_name()
        if not full_name:
            full_name = self.user.username
        return full_name

    @property
    def total_deposit(self):
        result = BellaDeposit.objects.filter(user=self.user).aggregate(total_deposit=Sum('deposit_amount'))
        return result['total_deposit']

    @property
    def verified(self):
        try:
            return EmailAddress.objects.filter(user=self.user).first().verified
        except Exception:
            return False

    def __str__(self):
        return '{}'.format(self.user.username)


class PhoneNumberConfirmation(models.Model):
    user = models.ForeignKey(AUTH_USER_MODEL, related_name='phone_number_confirmation')
    phone_number = models.CharField(max_length=20)
    code = models.CharField(max_length=10)
    verified = models.BooleanField(_("verified"), default=False)
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)
    updated_at = models.DateTimeField(auto_now=True, db_index=True)


class ActivityHistory(models.Model):
    user = models.ForeignKey(AUTH_USER_MODEL)
    receiver = models.ForeignKey(AUTH_USER_MODEL, blank=True, null=True, related_name='recepient')
    activity_type = models.IntegerField(choices=ActivityType.choices(), db_index=True)
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)

    def get_total_likes_received(self, user):
        # fix: user as param is not needed
        return ActivityHistory.objects.filter(receiver=user, activity_type=ActivityType.Like).count()

    def likes_received_24_hrs(self, user):
        # fix: user as param is not needed
        return ActivityHistory.objects.filter(
            receiver=user,
            activity_type=ActivityType.Like,
            created_at__gt=datetime.datetime.now()-datetime.timedelta(days=1)
        ).count()

    def likes_given_24hrs(self, user):
        # fix: user as param is not needed
        return ActivityHistory.objects.filter(
            user=user,
            activity_type=ActivityType.Like,
            created_at__gt=datetime.datetime.now()-datetime.timedelta(days=1)
        ).count()


class Comment(models.Model):
    post = models.ForeignKey(Bella, related_name='comments')
    author = models.ForeignKey(AUTH_USER_MODEL, related_name="user3")
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)

    def __unicode__(self):
        return self.text

    class Meta:
        ordering = ('created_at',)


class WithdrawRequestManager(models.Manager):
    def create_request(self, **kwargs):
        request = WithdrawRequest(**kwargs)

        amount = request.amount

        objs = WithdrawFeeSetting.objects.all()
        for obj in objs:
            range_from = obj.range_from if obj.range_from else float('-inf')
            range_to = obj.range_to if obj.range_to else float('inf')

            if range_from <= amount <= range_to:
                request.fee = amount * obj.percentage / 100
                request.owed = amount - request.fee

        request.save()
        return request


class WithdrawRequest(models.Model):
    status = models.CharField(max_length=30, choices=WithdrawStatus.choices(), null=True)
    author = models.ForeignKey(AUTH_USER_MODEL, related_name="requesting_user")
    address = models.TextField()
    amount = models.FloatField(validators=[MinValueValidator(0)], null=False)
    fee = models.FloatField(default=0)
    owed = models.FloatField(default=0)
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)
    improper_address_denied = models.BooleanField(default=False)

    objects = WithdrawRequestManager()

    def set_improper_address_denied(self):
        # trello task: https://trello.com/c/7m0ZkM1h
        # This essentially cancels the withdrawal — it should return the Bela back to the user's balance, and
        # they should get a notification saying "Your withdrawal was denied due to providing an improper address."
        # The withdrawal request in the admin panel should then be removed from the main withdrawal table,
        # but we should keep a record elsewhere of the improper-address-denied withdrawals for our records.

        user_profile = self.author.user1

        # returning bela back to user
        user_profile.belacoin = F('belacoin') + self.amount
        user_profile.save()

        # notify user
        mail = compose_improper_address_denied_mail(user=self.author)
        send_html_mail('Your withdrawal was denied', [self.author.email], mail)

        # hide this withdrawal request from main panel - set the flag 'improper_address_denied'.
        self.improper_address_denied = True
        self.save()

    def __str__(self):
        return '%s: %s BELA' % (self.author.username, self.amount)


class WithdrawEmailConfirmation(models.Model):
    request = models.ForeignKey(WithdrawRequest, related_name='emails')
    key = models.CharField(max_length=64)
    created_at = models.DateTimeField(default=timezone.now)


class WithdrawFeeSetting(models.Model):
    range_from = models.IntegerField(default=0)
    range_to = models.IntegerField(default=0, null=True, blank=True)
    percentage = models.FloatField(default=0)

    class Meta:
        verbose_name = "Withdraw Fee Setting"
        verbose_name_plural = "Withdraw Fee Settings"

    def __str__(self):
        return '{} - {} : {}%'.format(self.range_from, self.range_to, self.percentage)


class PriceHistory(models.Model):
    price = models.FloatField(default=0)
    record_time = models.DateTimeField(auto_now_add=True, db_index=True)

    def get_average_price(self):
        # Calculate this by adding the past 12 prices and dividing that total by 12
        try:
            return PriceHistory.objects.all().order_by('-record_time')[:12].annotate(
                avg_price=models.Avg('price')
            )[0].avg_price
        except:
            return 0.0


class AppSetting(models.Model):
    bella_per_like = models.FloatField(validators=[MinValueValidator(0)], default=0.5)
    coefficient_hours_ago = models.FloatField(default=1.05)
    coefficient_likes_for_post = models.FloatField(default=-1)
    coefficient_likes_given_by_poster = models.FloatField(default=-1)
    bella_average_price = models.FloatField(default=0)
    start_balance = models.FloatField(default=50)
    deposit_bonus = models.FloatField(default=5)

    class Meta:
        db_table = 'app_setting'


class GeoPlace(models.Model):
    id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=200, db_index=True)
    asciiname = models.CharField(max_length=200, db_index=True)
    alternatenames = models.CharField(max_length=10000)
    location = PointField(null=True)
    feature_class = models.CharField(max_length=1)
    feature_code = models.CharField(max_length=100)
    country_code = models.CharField(max_length=20)
    country_name = models.CharField(max_length=800, db_index=True)
    cc2 = models.CharField(max_length=200)
    admin1_code = models.CharField(max_length=200)
    admin1_name = models.CharField(max_length=400)
    admin1_asciiname = models.CharField(max_length=400)
    admin2_code = models.CharField(max_length=200)
    admin2_name = models.CharField(max_length=400)
    admin2_asciiname = models.CharField(max_length=400)
    admin3_code = models.CharField(max_length=200)
    admin4_code = models.CharField(max_length=200)
    timezone = models.CharField(max_length=400)


class BellaDeposit(models.Model):
    user = models.ForeignKey(AUTH_USER_MODEL)
    deposit_address = models.TextField(blank=False, null=False)
    deposit_amount = models.FloatField(default=0)
    transactionHash = models.TextField(blank=False, null=False, unique=True)
    time = models.DateTimeField(blank=False, null=False)
    created_at = models.DateTimeField(null=True, blank=True, auto_now_add=True)


class ExcludedTag(models.Model):
    tags = TaggableManager(blank=False)

    def __str__(self):
        return ', '.join(self.tags.names())


class InstagramUser(models.Model):
    user = models.ForeignKey(AUTH_USER_MODEL, related_name="instagrams")
    username = models.TextField(blank=False, null=False, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, db_index=True)
    status = models.CharField(max_length=30, choices=InstagramUserStatus.choices(), null=False, blank=False, default='CREATED')

    def delete(self, *args, **kwargs):
        self.status = 'DELETED'
        return super(InstagramUser, self).save(*args, **kwargs)


class Statistics(models.Model):
    type = models.CharField(max_length=20)
    earnings = models.FloatField(default=0)
    likes_given = models.IntegerField(default=0)
    photos_posted = models.IntegerField(default=0)
    user_registered = models.IntegerField(default=0)
    bela_deposited = models.FloatField(default=0)
    payout_referral = models.FloatField(default=0)
    payout_reward_bonus = models.FloatField(default=0)
    free_bela_given = models.FloatField(default=0)

    class Meta:
        managed = False
        verbose_name = "Statistic"
        verbose_name_plural = "Statistics"


class BlackListIP(models.Model):
    ip = models.GenericIPAddressField(unique=True)
    failed_count = models.IntegerField(default=0)
    updated_at = models.DateTimeField(auto_now=True)


class OneSignalPlayer(models.Model):
    user = models.ForeignKey(AUTH_USER_MODEL, related_name="onesignal_players")
    player_id = models.TextField(blank=False, null=False, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


def user_can_comment_this_post(self, bella_id=None, comment_text=''):
    """
    trello task: MFh7mOyx
    If a user comments the exact same thing 5 times in a row, remove those comments and suspend him for 24 hours
    :param self: User object
    :param bella_id: Bella id
    :return: Boolean
    """
    if self.user1.suspend_until and self.user1.suspend_until >= timezone.now():
        # user is still suspended
        return False

    the_last_4_comments_of_this_bela = Comment.objects.filter(
        post_id=bella_id, created_at__gte=(timezone.now() - timezone.timedelta(minutes=5)), text__iexact=comment_text
    ).order_by('-created_at')[:4]

    if len(the_last_4_comments_of_this_bela) < 4:
        return True

    all_comments_is_his = [comment.author_id == self.id for comment in the_last_4_comments_of_this_bela]

    if all(all_comments_is_his):
        # suspend him for the next 24 hours
        self.user1.suspend_until = timezone.now() + timezone.timedelta(hours=24)
        self.user1.save()

        # delete these comments
        for comment in the_last_4_comments_of_this_bela:
            comment.delete()

        return False

    return True


User.add_to_class('user_can_comment_this_post', user_can_comment_this_post)
