# -*- coding: utf-8 -*-
import datetime

from import_export.resources import ModelResource
from import_export.fields import Field
from import_export.admin import ExportMixin

from django.contrib.auth.models import User
from django.db import connection
from django.contrib import admin
from django.db.models import Count, Sum
from django.template.response import TemplateResponse
from django.utils import timezone

# Register your models here.
from belacam.apps.referral.choices import UserReferrerStatus
from belacam.apps.referral.models import UserReferrer
from belacam.apps.reward.models import UserTask as RewardTask

from belacam.apps.thoughts.models import (
    Bella,
    UserProfile,
    Comment,
    WithdrawRequest,
    WithdrawFeeSetting,
    BellaLike,
    UserRequestStat,
    BellaView,
    AppSetting,
    BellaDeposit,
    ExcludedTag,
    Statistics,
    InstagramUser
)

from belacam.apps.thoughts.admin_forms import *

admin.ModelAdmin.list_per_page = 20


class BellaLikePage(admin.ModelAdmin):
    form = BellaLikeForm
    list_display = ('id', 'bella', 'user', 'bella_per_like', 'created_at')

    def changelist_view(self, request, extra_context=None):
        datetime_to_day = connection.ops.date_trunc_sql('day', 'created_at')

        extra_context = extra_context or {}
        extra_context['total_likes'] = BellaLike.objects.all().count()
        extra_context['day_likes'] = BellaLike.objects.extra(
            {'day': datetime_to_day}
        ).values(
            'day'
        ).annotate(
            likes=Count('pk')
        ).order_by('day')
        return super(BellaLikePage, self).changelist_view(
            request,
            extra_context=extra_context
        )


class UserStatPage(admin.ModelAdmin):
    form = UserStatForm
    list_display = ('user', 'date', 'requests')
    list_filter = ('date',)

    def __init__(self, model, admin_site):
        super(UserStatPage, self).__init__(model, admin_site)
        self.extra_context = {}

    def get_queryset(self, request):
        queryset = super(UserStatPage, self).get_queryset(request)
        users = list(set([record.user for record in queryset]))
        total_active = len(users)
        extra_context = {
            'active_users': users,
            'total_active': total_active
        }
        self.extra_context = extra_context

        return queryset

    def changelist_view(self, request, extra_context=None):
        return super(UserStatPage, self).changelist_view(request, extra_context=self.extra_context)


class BellaVisit(Bella):
    class Meta:
        proxy = True


class BellaVisitPage(admin.ModelAdmin):
    form = BellaVisitForm
    list_display = ('bella_name', 'view_count') 

    search_fields = ['content', ]

    def get_queryset(self, request):
        bella_list = BellaView.objects.values_list('bella_id', flat=True).distinct()
        return Bella.objects.filter(id__in=bella_list).annotate(total_count=Count('views'))

    def bella_name(self, obj):
        return '(@%s) %s' % (obj.user.username, obj.content[:20] + (obj.content[20:] and '...'))

    bella_name.short_description = 'Bella'
    bella_name.admin_order_field = '-content'

    def view_count(self, obj):
        return obj.total_views
    view_count.short_description = 'Total View Count'
    view_count.admin_order_field = 'total_count'
    

class WithdrawRequestResource(ModelResource):
    username = Field(attribute='author__username', column_name='Username')
    address = Field(attribute='address', column_name='Address')
    owed = Field(attribute='owed', column_name='Withdrawal Amount Owed')
    fee = Field(attribute='fee', column_name='Withdrawal Amount Fee')
    amount = Field(attribute='amount', column_name='Withdrawal Total')
    status = Field(attribute='status', column_name='Status')

    class Meta:
        model = WithdrawRequest
        fields = ('username', 'address', 'owed', 'fee', 'amount', 'status', )
        export_order = ('username', 'address', 'owed', 'fee', 'amount', 'status', )


def set_status_to_pending(modeladmin, request, queryset):
    queryset.update(status='PENDING')


def set_status_to_confirmed(modeladmin, request, queryset):
    queryset.update(status='CONFIRMED')


def set_status_to_fulfilled(modeladmin, request, queryset):
    queryset.update(status='FULFILLED')


def set_status_to_sent(modeladmin, request, queryset):
    queryset.update(status='SENT')


def set_status_to_being_processed(modeladmin, request, queryset):
    queryset.update(status='BEING_PROCESSED')


def set_status_to_improper_address(modeladmin, request, queryset):
    queryset.update(status='IMPROPER_ADDRESS')

    # set_improper_address_denied for each object
    for wr in queryset:
        wr.set_improper_address_denied()


set_status_to_pending.short_description = "Mark as PENDING"
set_status_to_confirmed.short_description = "Mark as CONFIRMED"
set_status_to_fulfilled.short_description = "Mark as FULFILLED"
set_status_to_sent.short_description = "Mark as SENT"
set_status_to_being_processed.short_description = "Mark as BEING_PROCESSED"
set_status_to_improper_address.short_description = "Mark as IMPROPER_ADDRESS"


class WithdrawRequestPage(ExportMixin, admin.ModelAdmin):
    form = WithdrawRequestForm
    actions = [
        set_status_to_pending, set_status_to_confirmed, set_status_to_fulfilled,
        set_status_to_sent, set_status_to_being_processed, set_status_to_improper_address
    ]
    list_display = ('__str__', 'address', 'created_at', 'status')
    resource_class = WithdrawRequestResource
    list_filter = (
        'status', 'improper_address_denied'
    )
    change_list_template = 'admin/thoughts/withdraw_requests/change_list.html'
    ordering = ('status', )
    fieldsets = [
        (None, {'fields': (
            'status', 'author', 'address', 'amount', 'fee', 'owed'
        )})
    ]

    def get_queryset(self, request):
        if 'change' in request.path:
            # allow change view of improper_address_denied object
            return super(WithdrawRequestPage, self).get_queryset(request)

        if request.GET.get('improper_address_denied__exact', '0') == '1':
            return WithdrawRequest.objects.filter(improper_address_denied=True)
        return WithdrawRequest.objects.filter(improper_address_denied=False)

    def save_model(self, request, obj, form, change):

        if 'status' in form.changed_data and obj.status == 'IMPROPER_ADDRESS':
            # trello task: https://trello.com/c/7m0ZkM1h
            obj.set_improper_address_denied()

        return super(WithdrawRequestPage, self).save_model(request, obj, form, change)


class AppSettingPage(admin.ModelAdmin):
    list_display = (
        'id',
        'bella_per_like',
        'coefficient_hours_ago',
        'coefficient_likes_for_post',
        'coefficient_likes_given_by_poster',
        'start_balance',
        'deposit_bonus'
    )


class ReportPost(Bella):
    class Meta:
        proxy = True


class ReportPostPage(admin.ModelAdmin):
    form = BellaForm
    list_display = ('bella_name', 'report_count')

    search_fields = ['content', ]

    def get_queryset(self, request):
        return Bella.objects.annotate(total_count=Count('reports')).filter(total_count__gte=1)

    def bella_name(self, obj):
        return '(@%s) %s' % (obj.user.username, obj.content[:20] + (obj.content[20:] and '...'))

    bella_name.short_description = 'Bella'
    bella_name.admin_order_field = '-content'

    def report_count(self, obj):
        return obj.total_reports
    report_count.short_description = 'Total Report Count'
    report_count.admin_order_field = 'total_count'


class BellaDepositPage(admin.ModelAdmin):
    form = BellaDepositForm
    list_display = ('id', 'user', 'deposit_address', 'deposit_amount', 'transactionHash', 'time', 'created_at')


class UserProfileAdmin(admin.ModelAdmin):
    form = UserProfileForm

    list_display = ('user', 'belacoin', 'number_of_posts', 'likes_count', 'is_bot', 'suspend_until', 'user_ip')
    search_fields = ('user__username',)


class InstagramUserPage(admin.ModelAdmin):
    form = InstagramUserForm
    list_display = ('user', 'username', 'status')
    search_fields = ('user__username',)

    def get_actions(self, request):
        actions = super(InstagramUserPage, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions


class StatisticAdmin(admin.ModelAdmin):
    actions = None
    list_display_links = None
    enable_change_view = False

    def _get_stat_list(self):
        now = timezone.now()
        today_start = datetime.datetime(year=now.year, month=now.month, day=now.day, tzinfo=now.tzinfo)
        today_end = today_start + datetime.timedelta(days=1)
        week_before = today_start - datetime.timedelta(days=7)

        stat_tasks = [{
            'type': 'Day',
            'from': today_start,
            'to': today_end
        }, {
            'type': 'Week',
            'from': week_before,
            'to': today_end
        }]

        result = []

        # Generate total statistic
        stats = dict()

        stats['type'] = 'Total'

        stats['earnings'] = BellaLike.objects.aggregate(earnings=Sum('bella_per_like'))['earnings'] or 0

        stats['likes_given'] = BellaLike.objects.aggregate(likes=Count('pk'))['likes']

        stats['photos_posted'] = Bella.objects.aggregate(photos=Count('pk'))['photos']

        stats['user_registered'] = User.objects.aggregate(register=Count('pk'))['register']

        stats['bela_deposited'] = BellaDeposit.objects.aggregate(deposit=Sum('deposit_amount'))['deposit'] or 0

        stats['payout_referral'] = UserReferrer.objects.filter(
            status=UserReferrerStatus.VERIFIED.name
        ).aggregate(payout_referral=Sum('belas'))['payout_referral']

        stats['payout_reward_bonus'] = RewardTask.objects.aggregate(
            payout_reward_bonus=Sum('task__reward')
        )['payout_reward_bonus'] or 0

        result.append(stats)

        # Generate periodic statistic
        for task in stat_tasks:
            stats = dict()

            stats['type'] = task['type']

            stats['earnings'] = BellaLike.objects.filter(
                created_at__gte=task['from'], created_at__lte=task['to']
            ).aggregate(earnings=Sum('bella_per_like'))['earnings'] or 0

            stats['likes_given'] = BellaLike.objects.filter(
                created_at__gte=task['from'], created_at__lte=task['to']
            ).aggregate(likes=Count('pk'))['likes']

            stats['photos_posted'] = Bella.objects.filter(
                created_at__gte=task['from'], created_at__lte=task['to']
            ).aggregate(photos=Count('pk'))['photos']

            stats['user_registered'] = User.objects.filter(
                date_joined__gte=task['from'], date_joined__lte=task['to']
            ).aggregate(register=Count('pk'))['register']

            stats['bela_deposited'] = BellaDeposit.objects.filter(
                created_at__gte=task['from'], created_at__lte=task['to']
            ).aggregate(deposit=Sum('deposit_amount'))['deposit'] or 0

            stats['payout_referral'] = UserReferrer.objects.filter(
                updated_at__gte=task['from'], updated_at__lte=task['to'],
                status=UserReferrerStatus.VERIFIED.name
            ).aggregate(payout_referral=Sum('belas'))['payout_referral'] or 0

            stats['payout_reward_bonus'] = RewardTask.objects.filter(
                created_at__gte=task['from'], created_at__lte=task['to']
            ).aggregate(payout_reward_bonus=Sum('task__reward'))['payout_reward_bonus'] or 0

            result.append(stats)
        return result

    def changelist_view(self, request, extra_context=None):
        return TemplateResponse(
            request,
            'admin/thoughts/statistics/templates/admin/thoughts/statistics/change_list.html', context={
                'opts': self.opts,
                'stats': self._get_stat_list()
            }
        )


class CommentAdmin(admin.ModelAdmin):
    form = CommentForm

    list_display = ('author', 'post', 'text')
    search_fields = ('text',)


class BellaAdmin(admin.ModelAdmin):
    form = BellaForm

    search_fields = ('content',)


admin.site.register(Bella, BellaAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(WithdrawRequest, WithdrawRequestPage)
admin.site.register(WithdrawFeeSetting)
admin.site.register(BellaLike, BellaLikePage)
admin.site.register(UserRequestStat, UserStatPage)
admin.site.register(BellaVisit, BellaVisitPage) 
admin.site.register(AppSetting, AppSettingPage)
admin.site.register(ReportPost, ReportPostPage)
admin.site.register(BellaDeposit, BellaDepositPage)
admin.site.register(ExcludedTag)
admin.site.register(Statistics, StatisticAdmin)
admin.site.register(InstagramUser, InstagramUserPage)
