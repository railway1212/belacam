import os

from dal import autocomplete
from taggit.models import Tag

from django import forms
from django.conf import settings
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.models import User
from django.template.defaultfilters import filesizeformat
from django.utils.html import strip_tags
from django.utils.translation import ugettext_lazy as _
import account.forms
from collections import OrderedDict
import re
from django.core.exceptions import ObjectDoesNotExist

from .models import Bella, GeoPlace, UserProfile, Comment


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['accepted_terms_of_service']

    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)
        self.fields['accepted_terms_of_service'].required = True


class UserCreateForm(UserCreationForm):
    email = forms.EmailField(required=True, widget=forms.widgets.TextInput(attrs={'placeholder': 'Email'}))
    first_name = forms.CharField(required=True, widget=forms.widgets.TextInput(attrs={'placeholder': 'First Name'}))
    last_name = forms.CharField(required=True, widget=forms.widgets.TextInput(attrs={'placeholder': 'Last Name'}))
    username = forms.CharField(widget=forms.widgets.TextInput(attrs={'placeholder': 'Username'}))
    password1 = forms.CharField(widget=forms.widgets.PasswordInput(attrs={'placeholder': 'Password'}))
    password2 = forms.CharField(widget=forms.widgets.PasswordInput(attrs={'placeholder': 'Password Confirmation'}))

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            User._default_manager.get(username=username)
        except User.DoesNotExist:
            return username

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()

            # userProfile = DoctorSeeker(user=user, name=name, email=email)
            # userProfile.save()

        return user

    def is_valid(self):
        form = super(UserCreateForm, self).is_valid()
        for f, error in self.errors.iteritems():
            if f != '__all_':
                self.fields[f].widget.attrs.update({'class': 'error', 'value': strip_tags(error)})
        return form

    class Meta:
        fields = ['email', 'username', 'first_name', 'last_name', 'password1',
                  'password2']
        model = User


class ThoughtForm(forms.ModelForm):
    image_path = forms.CharField(required=False)
    video_path = forms.CharField(required=False)
    content = forms.CharField(required=False, widget=forms.Textarea(attrs={'cols': 80, 'rows': 2}))
    location = forms.CharField(required=False)

    def is_valid(self):
        form = super(ThoughtForm, self).is_valid()
        for f in self.errors.iterkeys():
            if f != '__all__':
                self.fields[f].widget.attrs.update({'class': 'error bellaText'})
        return form

    class Meta:
        model = Bella
        fields = ('content', 'image_path', 'location', 'video_path')


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('text',)


class CommentForm2(forms.ModelForm):
    class Meta:
        model = Comment
        # exclude = ['author', 'updated', 'created', ]
        fields = ['text']
        widgets = {
            'text': forms.TextInput(
                attrs={'id': 'post-text', 'required': True, 'placeholder': 'Leave a comment', 'class': 'inlinecomment'}
            ),
        }


class CoverpicForm(forms.ModelForm):
    cover_pic = forms.CharField(required=True)

    class Meta:
        model = UserProfile
        fields = ('cover_pic',)


class AvatarForm(forms.ModelForm):
    profile_pic = forms.CharField(required=True)

    class Meta:
        model = UserProfile
        fields = ('profile_pic',)


class SettingsForm(account.forms.SettingsForm):
    username = forms.CharField(label=_("Username"), max_length=30)
    send_weekly_email = forms.BooleanField(label=_("Send weekly email?"), initial=True, required=False)

    def __init__(self, *args, **kwargs):
        super(SettingsForm, self).__init__(*args, **kwargs)
        field_order = ["username", "email", "language", "send_weekly_email"]
        if not OrderedDict or hasattr(self.fields, "keyOrder"):
            self.fields.keyOrder = field_order
        else:
            self.fields = OrderedDict((k, self.fields[k]) for k in field_order)

    def clean_username(self):
        alnum_re = re.compile(r"^\w+$")
        username = self.cleaned_data["username"]

        if username == self.initial.get('username'):
            return username

        if not alnum_re.search(username):
            raise forms.ValidationError(_("Usernames can only contain letters, numbers and underscores."))
        try:
            User.objects.get(username=username)
        except ObjectDoesNotExist:
            return username.lower()
        raise forms.ValidationError('Username is already taken.')
