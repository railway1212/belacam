from belacam.apps.reward.models import UserTask as UserRewardTask, Task as RewardTask
from belacam.apps.reward.serializers import RewardTaskSerializer


def complete_reward_task(user, task_type):
    if not user.user1.phone_number:
        return None

    task = RewardTask.objects.filter(type=task_type).first()
    user_task = UserRewardTask.objects.complete_task(user=user, task=task)
    if user_task is not None:
        user.user1.belacoin += task.reward
        user.user1.save()

        result = {
            "type": "complete_reward_task",
            "task": RewardTaskSerializer(task).data,
            "belacoin": user.user1.belacoin
        }
        return result
    return None
