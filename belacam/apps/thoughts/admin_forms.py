from dal import autocomplete
from django import forms
from belacam.apps.thoughts.models import *

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('__all__')
        widgets = {
            'user': autocomplete.ModelSelect2(url='user-autocomplete'),
            'blocked_people': autocomplete.ModelSelect2Multiple(url='user-autocomplete')
        }

class BellaLikeForm(forms.ModelForm):
    class Meta:
        model = BellaLike
        fields = ('__all__')
        widgets = {
            'user': autocomplete.ModelSelect2(url='user-autocomplete'),
            'bella': autocomplete.ModelSelect2(url='bela-autocomplete'),
        }

class BellaLikeForm(forms.ModelForm):
    class Meta:
        model = BellaLike
        fields = ('__all__')
        widgets = {
            'user': autocomplete.ModelSelect2(url='user-autocomplete'),
            'bella': autocomplete.ModelSelect2(url='bela-autocomplete'),
        }

class UserStatForm(forms.ModelForm):
    class Meta:
        model = UserRequestStat
        fields = ('__all__')
        widgets = {
            'user': autocomplete.ModelSelect2(url='user-autocomplete'),
        }

class BellaVisitForm(forms.ModelForm):
    class Meta:
        model = BellaView
        fields = ('__all__')
        widgets = {
            'user': autocomplete.ModelSelect2(url='user-autocomplete'),
            'bella': autocomplete.ModelSelect2(url='bela-autocomplete'),
        }

class WithdrawRequestForm(forms.ModelForm):
    class Meta:
        model = BellaView
        fields = ('__all__')
        widgets = {
            'author': autocomplete.ModelSelect2(url='user-autocomplete'),
            'bella': autocomplete.ModelSelect2(url='bela-autocomplete'),
        }

class BellaForm(forms.ModelForm):
    class Meta:
        model = Bella
        fields = ('__all__')
        widgets = {
            'user': autocomplete.ModelSelect2(url='user-autocomplete'),
            'reports': autocomplete.ModelSelect2Multiple(url='user-autocomplete'),
            'likes': autocomplete.ModelSelect2Multiple(url='user-autocomplete'),
            'views': autocomplete.ModelSelect2Multiple(url='user-autocomplete'),
        }

class BellaDepositForm(forms.ModelForm):
    class Meta:
        model = BellaDeposit
        fields = ('__all__')
        widgets = {
            'user': autocomplete.ModelSelect2(url='user-autocomplete'),
        }

class InstagramUserForm(forms.ModelForm):
    class Meta:
        model = InstagramUser
        fields = ('__all__')
        widgets = {
            'user': autocomplete.ModelSelect2(url='user-autocomplete'),
        }

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('__all__')
        widgets = {
            'author': autocomplete.ModelSelect2(url='user-autocomplete'),
            'post': autocomplete.ModelSelect2(url='bela-autocomplete'),
        }
