from __future__ import absolute_import, unicode_literals
import json
import requests
import datetime

from celery import shared_task

from django.utils import timezone
from django.conf import settings
from django.db.models import Sum, Count, F

from belacam.apps.thoughts.models import Bella, BellaLike, AppSetting, User, UserProfile, PriceHistory, ActivityType
from belacam.apps.thoughts import mail_composer as MailComposer


@shared_task
def process_image_with_lambda(key, post_id):

    if not key:
        return

    headers = {'x-api-key': 've1nfBJ4xl2qhNDVmDN7B96EtGqnfBed9pzt8bc1'}
    data = dict(bucket=settings.AWS_STORAGE_BUCKET_NAME, key=key)
    result = requests.post(
        'https://j91our9x8i.execute-api.us-east-1.amazonaws.com/dev/',
        headers=headers,
        data=json.dumps(data))

    if result.status_code == 200 and 'status' in result.json() and result.json()['status'] == 'success':
        bella = Bella.objects.get(id=post_id)
        bella.processed = True
        bella.save()

    return True


@shared_task
def send_weekly_email(arg):
    if not settings.DEBUG:
        print("Send weekly mail")

        now_date = datetime.date.today()
        week_end = timezone.make_aware(datetime.datetime.combine(now_date, datetime.time(0, 0, 0, 0)))
        week_start = week_end - datetime.timedelta(days=7)

        num_users = User.objects.filter(is_active=True).count()
        bela_average_price = AppSetting.objects.values_list("bella_average_price", flat=True).first()

        if arg.get('duration', 'weekly') == 'all':
            if arg.get('user'):
                queryset = BellaLike.objects.filter(
                    bella__user__id__in=arg.get('user')
                ).values(
                    'bella__user__id'
                ).annotate(
                    email=F('bella__user__email'),
                    user_name=F('bella__user__username'),
                    earnings=Sum('bella_per_like')
                )
            else:
                queryset = BellaLike.objects.values(
                    'bella__user__id'
                ).annotate(
                    email=F('bella__user__email'),
                    user_name=F('bella__user__username'),
                    earnings=Sum('bella_per_like')
                )
        else:
            if arg.get('user'):
                queryset = BellaLike.objects.filter(
                    created_at__gte=week_start,
                    created_at__lte=week_end,
                    bella__user__user1__send_weekly_email=True,
                    bella__user__id__in=arg.get('user')
                ).values(
                    'bella__user__id'
                ).annotate(
                    email=F('bella__user__email'),
                    user_name=F('bella__user__username'),
                    earnings=Sum('bella_per_like')
                ).filter(
                    earnings__gt=0
                )
            else:
                queryset = BellaLike.objects.filter(
                    created_at__gte=week_start,
                    created_at__lte=week_end,
                    bella__user__user1__send_weekly_email=True
                ).values(
                    'bella__user__id'
                ).annotate(
                    email=F('bella__user__email'),
                    user_name=F('bella__user__username'),
                    earnings=Sum('bella_per_like')
                ).filter(
                    earnings__gt=0
                )
        for record in queryset:

            dollar_earnings = round(record['earnings'] * bela_average_price, 2)
            if arg.get('duration', 'weekly') == 'all':
                mail = MailComposer.compose_total_earning_email({
                    'username': record['user_name'],
                    'dollar_earning': dollar_earnings,
                    'bela_earning': record['earnings'],
                    'num_users': "{:,}".format(num_users)
                })

                title = "You Earned ${} on Belacam.com since you have joined us".format(dollar_earnings)
            else:
                mail = MailComposer.compose_weekly_earning_email({
                    'username': record['user_name'],
                    'dollar_earning': dollar_earnings,
                    'bela_earning': record['earnings'],
                    'num_users': "{:,}".format(num_users)
                })

                title = "You Earned ${} on Belacam.com Last Week".format(dollar_earnings)

            MailComposer.send_html_mail(title, [record['email']], mail)


@shared_task
def get_belacam_price():
    resp = requests.get('https://api.coinmarketcap.com/v1/ticker/belacoin/')
    if resp.status_code == requests.codes.ok:
        resp_json = resp.json()
        if len(resp_json) > 0:
            price_in_dollors = float(resp_json[0].get('price_usd', 0))
            PriceHistory.objects.create(price=price_in_dollors)
            app_setting = AppSetting.objects.first()
            app_setting.bella_average_price = PriceHistory().get_average_price()
            app_setting.save(update_fields=['bella_average_price'])


@shared_task
def update_likes():
    current_time = datetime.datetime.now()
    UserProfile.objects.update(likes_count=0, like_update_time=current_time)
    for up in UserProfile.objects.filter(
            user__recepient__created_at__gt=datetime.datetime.now() - datetime.timedelta(days=1),
            user__recepient__activity_type=ActivityType.Like
    ).annotate(like_count=Count('user__recepient__id')):
        up.likes_count = up.like_count
        up.like_update_time = current_time
        up.save(update_fields=['likes_count', 'like_update_time', ])


@shared_task
def update_post_rank():
    coefficient_hours_ago = AppSetting.objects.first().coefficient_hours_ago
    coefficient_likes_for_post = AppSetting.objects.first().coefficient_likes_for_post
    coefficient_likes_given_by_poster = AppSetting.objects.first().coefficient_likes_given_by_poster

    query_set = Bella.objects.select_related('user__user1').filter(created_at__lte=timezone.now()).order_by(
        '-created_at').annotate(total_likes_count=Count("likes"))
    count = query_set.count()
    i = 0
    while i < count:

        for post in query_set[i: i + 1000]:
            post.rank_number = (
                    pow((timezone.now() - post.created_at).total_seconds() / 3600, coefficient_hours_ago)
                    + post.total_likes_count * coefficient_likes_for_post
                    + post.user.user1.likes_count * coefficient_likes_given_by_poster
            )

            post.save(update_fields=['rank_number'])
        i += 1000
