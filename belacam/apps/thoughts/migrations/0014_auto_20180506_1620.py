# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2018-05-06 16:20
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('thoughts', '0013_auto_20180501_1952'),
    ]

    operations = [
        migrations.AlterField(
            model_name='BellaLike',
            name='bella',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='thoughts.Bella'),
        ),
        migrations.AlterField(
            model_name='BellaView',
            name='bella',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='thoughts.Bella'),
        ),
        migrations.AlterField(
            model_name='Comment',
            name='post',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE,
                                    related_name='comments',
                                    to='thoughts.Bella'),
        ),
    ]
