# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models



class Migration(migrations.Migration):
    dependencies = [
        ('thoughts', '0038_merge')
    ]

    operations = [
        migrations.AddField(
            model_name='bella',
            name='image_path',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='bella',
            name='processed',
            field=models.BooleanField(default=False),
        ),
    ]
