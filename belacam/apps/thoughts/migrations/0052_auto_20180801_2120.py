# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2018-08-01 21:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('thoughts', '0051_auto_20180801_1937'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='cover_pic',
            field=models.TextField(default='assets/images/c-pic.jpg', max_length=1500),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='profile_pic',
            field=models.TextField(default='assets/images/profile.png', max_length=1500),
        ),
    ]
