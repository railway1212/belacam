# -*- coding: utf-8 -*-	position: relative;
# Generated by Django 1.9.5 on 2018-04-19 15:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('thoughts', '0010_auto_20180418_1049'),
    ]

    operations = [
        migrations.AlterField(
            model_name='withdraw_request',
            name='amount',
            field=models.FloatField(),
        ),
    ]
