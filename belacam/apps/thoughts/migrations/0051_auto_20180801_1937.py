# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2018-08-01 19:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('thoughts', '0050_auto_20180801_1923'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='profile_pic',
            field=models.URLField(default='assets/images/profile.png', max_length=1500),
        ),
    ]
