# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2018-07-11 02:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('thoughts', '0035_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='WithdrawFeeSetting',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('range_from', models.IntegerField(default=0)),
                ('range_to', models.IntegerField(blank=True, default=0, null=True)),
                ('percentage', models.FloatField(default=0)),
            ],
        ),
    ]
