# -*- coding: utf-8 -*-	position: relative;
# Generated by Django 1.9.5 on 2018-05-01 19:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('thoughts', '0012_appsetting'),
    ]

    operations = [
        migrations.AddField(
            model_name='appsetting',
            name='coefficient_hours_ago',
            field=models.FloatField(default=1.05),
        ),
        migrations.AddField(
            model_name='appsetting',
            name='coefficient_likes_for_post',
            field=models.FloatField(default=-1),
        ),
        migrations.AddField(
            model_name='appsetting',
            name='coefficient_likes_given_by_poster',
            field=models.FloatField(default=-1),
        ),
    ]
