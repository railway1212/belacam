from rest_framework import permissions


class IsStaffOrTargetUser(permissions.BasePermission):
    def has_permission(self, request, view):
        # allow user to list all users if logged in user is staff
        return not request.user.is_anonymous()

    def has_object_permission(self, request, view, obj):
        # allow logged in user to view own details, allows staff to view all records
        print obj == request.user.user1
        print obj == request.user.username
        print obj

        return request.user.is_staff or obj == request.user.user1 or obj == request.user.username
