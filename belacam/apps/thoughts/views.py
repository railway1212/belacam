import hashlib
import hmac
import json
import re
import uuid
import requests
import os
import boto3
from datetime import datetime as c_datetime, timedelta
from dateutil.parser import parse

from channels import Channel
from notifications.signals import notify
from rest_framework import permissions
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.generics import (
    ListAPIView,
    UpdateAPIView,
    CreateAPIView,
    GenericAPIView
)
from rest_framework.mixins import UpdateModelMixin, DestroyModelMixin
from rest_framework.response import Response
from rest_framework.views import APIView
from neutrino_api.neutrino_api_client import NeutrinoApiClient
import account.views
from account.models import AccountDeletion
from ipware import get_client_ip
import onesignal as onesignal_sdk
from onesignal.error import OneSignalError

from django.utils.crypto import get_random_string
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from django.http import HttpResponseNotAllowed
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.views.decorators.cache import never_cache
from django.core.cache import cache
from django.views.decorators.debug import sensitive_post_parameters
from django.middleware.csrf import get_token
from django.contrib.auth.forms import PasswordResetForm, SetPasswordForm
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode

from belacam.apps.reward.choices import RewardTaskType
from belacam.apps.core import utils
from . import helpers
from belacam.apps.core.utils import format_td, get_native_language_via_country_code
from belacam.apps.referral.models import UserReferrer
from belacam.apps.thoughts.tasks import process_image_with_lambda
from .mail_composer import compose_withdrawal_email, send_html_mail
from .error_codes import BelaLikeError
from .pagination import LargeResultsSetPagination, MediumResultsSetPagination
from .serializers import *
from .permissions import IsStaffOrTargetUser
from .forms import ThoughtForm, AvatarForm, CoverpicForm, UserProfileForm, SettingsForm

neutrino_api_client = NeutrinoApiClient(settings.NEUTRINO_API_USER_ID, settings.NEUTRINO_API_KEY)

try:
    onesignal_client = onesignal_sdk.Client(
        user_auth_key=settings.ONE_SIGNAL_USER_AUTH_KEY,
        app={
            "app_auth_key": settings.ONE_SIGNAL_APP_AUTH_KEY,
            "app_id": settings.ONE_SIGNAL_APP_ID
        }
    )
except OneSignalError as e:
    print(e)


class CommentCreateAPIView(CreateAPIView):
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        bella_id = self.kwargs.get('post_id')
        post = Bella.objects.filter(id=bella_id)

        if not post:
            raise ValidationError('Thought data not provided')

        comment = serializer.save(author=self.request.user, post=post[0])

        can_comment = self.request.user.user_can_comment_this_post(bella_id=bella_id, comment_text=comment.text)
        if not can_comment:
            raise ValidationError('You commented too often - like a boss')

        matches = re.findall(r'@\w+', comment.text)
        for username in set(matches):
            users = User.objects.filter(username=username[1:])
            if users and users[0] != self.request.user:
                notify.send(self.request.user, recipient=users[0], verb=" tagged you in a post", action_object=comment)

                # ========= Sending OneSignal Notification =======
                if users[0].onesignal_players.count() > 0:
                    player_ids = list(users[0].onesignal_players.values_list('player_id', flat=True))
                    content = "%s tagged you in a post" % self.request.user.username
                    send_onesignal_push_notification(self.request.user, content, player_ids)

                # ================================================

                Channel("notify_user").send({
                    'username': users[0].username
                })

        ActivityHistory.objects.create(
            user=self.request.user,
            receiver=post[0].user,
            activity_type=ActivityType.Comment
        )
        if self.request.user != post[0].user:
            notify.send(
                self.request.user,
                recipient=post[0].user,
                verb=" has commented on your post!",
                action_object=comment
            )
            # ========= Sending OneSignal Notification =======
            if post[0].user.onesignal_players.count() > 0:
                player_ids = list(post[0].user.onesignal_players.values_list('player_id', flat=True))
                content = "%s has commented on your post!" % self.request.user.username
                send_onesignal_push_notification(self.request.user, content, player_ids)

            # ================================================

            Channel("notify_user").send({
                'username': post[0].user.username
            })


class CommentUpdateDestroyAPIView(GenericAPIView, UpdateModelMixin, DestroyModelMixin):
    serializer_class = CommentSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        user = self.request.user
        if self.request.method == 'PATCH':
            comment = Comment.objects.filter(author=user)
        elif self.request.method == 'DELETE':
            comment = Comment.objects.filter(Q(post__user=user) | Q(author=user))
        else:
            raise ValidationError('Invalid Method')

        if not comment:
            raise ValidationError('Comment not provided')
        return comment

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class UserRetrieveView(APIView):
    permission_classes = []

    def get(self, request, **kwargs):
        current_user = request.user
        queryset = User.objects.filter(username=self.kwargs['username'])

        # Exclude blocked users
        if current_user.is_authenticated():
            queryset = queryset.exclude(
                id__in=list(current_user.blocked.values_list('user_id', flat=True))
            )

        selected_user = UserSerializer.setup_eager_loading(queryset).first()
        if selected_user is None:
            return Response({}, status=404)

        resp = UserSerializer(selected_user, context={'request': request}).data
        return Response(resp)


class UserProfileUpdateView(UpdateAPIView):
    serializer_class = UserProfileSerializer
    queryset = UserProfile.objects.all()
    lookup_field = 'user'
    lookup_url_kwarg = 'pk'
    allowed_methods = ['PATCH']

    def get_permissions(self):
        return [IsStaffOrTargetUser()]


class FollowerListView(ListAPIView):
    serializer_class = UserWithSmallAvatarSerializer
    pagination_class = LargeResultsSetPagination

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        username = self.kwargs.get('username')
        followers_list = Follow.objects.filter(followee__username__in=[username]).values_list('follower_id', flat=True)
        queryset = User.objects.filter(id__in=followers_list).select_related('user1')
        return queryset

    def get_serializer_context(self):
        """Use this to add extra context."""
        context = super(FollowerListView, self).get_serializer_context()
        cur_user_followings_list = []
        if self.request.user.is_authenticated():
            cur_user_followings_list = Follow.objects.filter(
                follower=self.request.user
            ).values_list('followee_id', flat=True)
        context['cur_user_followings'] = cur_user_followings_list
        return context


class FollowingListView(ListAPIView):
    serializer_class = UserWithSmallAvatarSerializer
    pagination_class = LargeResultsSetPagination

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        username = self.kwargs.get('username')
        followings_list = Follow.objects.filter(follower__username__in=[username]).values_list('followee_id', flat=True)
        queryset = User.objects.filter(id__in=followings_list).select_related('user1')
        return queryset

    def get_serializer_context(self):
        """Use this to add extra context."""
        context = super(FollowingListView, self).get_serializer_context()
        cur_user_followings_list = []
        if self.request.user.is_authenticated():
            cur_user_followings_list = Follow.objects.filter(
                follower=self.request.user
            ).values_list('followee_id', flat=True)
        context['cur_user_followings'] = cur_user_followings_list
        return context


class SuperThotListView(ListAPIView):
    model = Bella
    serializer_class = BellaSerializer

    def dispatch(self, request, **kwargs):
        resp = super(SuperThotListView, self).dispatch(request, **kwargs)

        return resp

    def get_serializer_context(self):
        """Use this to add extra context."""
        context = super(SuperThotListView, self).get_serializer_context()
        context['bella_average_price'] = AppSetting.objects.values_list("bella_average_price", flat=True).first()
        cur_user_followings_list = []
        if self.request.user.is_authenticated():
            cur_user_followings_list = Follow.objects.filter(
                follower=self.request.user
            ).values_list(
                'followee_id',
                flat=True
            )
        context['cur_user_followings'] = cur_user_followings_list
        return context


class HomeFeedListView(SuperThotListView):
    def get_queryset(self):
        current_user = self.request.user

        if current_user.is_authenticated():

            cached_fg_users = cache.get('hl_fg_%s' % current_user.id)
            if not cached_fg_users:
                cached_fg_users = list(Follow.objects.filter(follower=current_user).values_list(
                    "followee_id", flat=True
                )) + [current_user.id]
                cache.set('hl_fg_%s' % current_user.id, cached_fg_users, 60 * 2)

            cached_blocked_users = cache.get('hl_bu_%s' % current_user.id)
            if not cached_blocked_users:
                cached_blocked_users = list(current_user.user1.blocked_people.all().values_list('id', flat=True))
                cache.set('hl_bu_%s' % current_user.id, cached_blocked_users, 60 * 30)

            queryset = Bella.objects.filter(
                user_id__in=cached_fg_users
            ).order_by(
                '-created_at'
            ).exclude(user_id__in=cached_blocked_users).exclude(reports__id__in=[current_user.id])

            queryset = self.get_serializer_class().setup_eager_loading(queryset, current_user)

            return queryset
        else:
            return Bella.objects.none()


class NotificationListView(ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = NotificationSerializer
    pagination_class = LargeResultsSetPagination

    def get_queryset(self):
        return self.request.user.notifications.all().order_by('-timestamp')

    def filter_queryset(self, queryset):
        from_date = self.request.query_params.get('from')
        queryset = super(NotificationListView, self).filter_queryset(queryset)
        if from_date:
            queryset = queryset.filter(timestamp__lte=from_date)
        return queryset

    def get_serializer_context(self):
        context = super(NotificationListView, self).get_serializer_context()
        cur_user_followings_list = []
        if self.request.user.is_authenticated():
            cur_user_followings_list = Follow.objects.filter(follower=self.request.user).values_list('followee_id',
                                                                                                     flat=True)
        context['cur_user_followings'] = cur_user_followings_list
        return context


class ThoughtListView(SuperThotListView):

    @staticmethod
    def get_exclude_list(current_user_):
        blocked_users = list(current_user_.user1.blocked_people.all().values_list('id', flat=True))
        blocking_users = list(current_user_.blocked.all().values_list('user_id', flat=True))
        following_users = (
                list(Follow.objects.filter(follower=current_user_).values_list("followee_id", flat=True)) +
                [current_user_.id]
        )

        # cached_exclude_list = cache.get('usr_excl_list_%s' % current_user_.id)
        # if not cached_exclude_list:
        #    cached_exclude_list = list(set(blocked_users + following_users + blocking_users))
        # cache.set('usr_excl_list_%s' % current_user_.id, cached_exclude_list, 20)  # cache for 20 secs

        return list(set(blocked_users + following_users + blocking_users))

    def get_queryset(self):
        sorting_param = self.request.GET.get('sort_by', 'hot')
        current_user = self.request.user

        if sorting_param == 'new':
            if current_user.is_authenticated():

                exclude_list = self.get_exclude_list(current_user_=current_user)

                queryset = Bella.objects.filter(
                    created_at__lte=timezone.now()
                ).order_by('-created_at').exclude(
                    user_id__in=exclude_list
                ).exclude(reports__id__in=[current_user.id])
            else:
                queryset = Bella.objects.filter(
                    created_at__lte=timezone.now()
                ).order_by('-created_at')

        elif sorting_param == 'time_today':
            if current_user.is_authenticated():

                exclude_list = self.get_exclude_list(current_user_=current_user)

                queryset = Bella.objects.filter(
                    bellalike__created_at__gte=timezone.now() - timedelta(days=1)
                ).annotate(
                    today_likes_count=Count('bellalike__id')
                ).order_by(
                    '-today_likes_count', '-created_at'
                ).exclude(
                    user_id__in=exclude_list
                ).exclude(reports__id__in=[current_user.id])
            else:
                queryset = Bella.objects.filter(
                    bellalike__created_at__gt=timezone.now() - timedelta(days=1)
                ).annotate(
                    today_likes_count=Count('bellalike__id')
                ).order_by(
                    '-today_likes_count', '-created_at'
                )

        elif sorting_param == 'time_week':
            if current_user.is_authenticated():

                exclude_list = self.get_exclude_list(current_user_=current_user)

                queryset = Bella.objects.filter(
                    bellalike__created_at__gte=timezone.now() - timedelta(days=7)
                ).annotate(
                    week_likes_count=Count('bellalike__id')
                ).order_by(
                    '-week_likes_count', '-created_at'
                ).exclude(
                    user_id__in=exclude_list
                ).exclude(reports__id__in=[current_user.id])
            else:
                queryset = Bella.objects.filter(
                    bellalike__created_at__gt=timezone.now() - timedelta(days=7)
                ).annotate(
                    week_likes_count=Count('bellalike__id')
                ).order_by(
                    '-week_likes_count', '-created_at'
                )

        elif sorting_param == 'time_month':
            if current_user.is_authenticated():

                exclude_list = self.get_exclude_list(current_user_=current_user)

                queryset = Bella.objects.filter(
                    bellalike__created_at__gte=timezone.now() - timedelta(days=31)
                ).annotate(
                    month_likes_count=Count('bellalike__id')
                ).order_by(
                    '-month_likes_count', '-created_at'
                ).exclude(
                    user_id__in=exclude_list
                ).exclude(reports__id__in=[current_user.id])
            else:
                queryset = Bella.objects.filter(
                    bellalike__created_at__gt=timezone.now() - timedelta(days=31)
                ).annotate(
                    month_likes_count=Count('bellalike__id')
                ).order_by(
                    '-month_likes_count', '-created_at'
                )

        elif sorting_param == 'time_all':
            if current_user.is_authenticated():

                exclude_list = self.get_exclude_list(current_user_=current_user)

                queryset = Bella.objects.filter(
                    created_at__lte=timezone.now()
                ).annotate(
                    all_likes_count=Count("likes")
                ).order_by(
                    '-all_likes_count', '-created_at'
                ).exclude(
                    user_id__in=exclude_list
                ).exclude(
                    reports__id__in=[current_user.id]
                )
            else:
                queryset = Bella.objects.filter(
                    created_at__lte=timezone.now()
                ).annotate(
                    all_likes_count=Count("likes")
                ).order_by(
                    '-all_likes_count', '-created_at'
                )
        else:
            if current_user.is_authenticated():

                exclude_list = self.get_exclude_list(current_user_=current_user)

                queryset = Bella.objects.filter(
                    created_at__lte=timezone.now()
                ).order_by('rank_number', '-created_at').exclude(
                    user_id__in=exclude_list).exclude(reports__id__in=[current_user.id])
            else:
                queryset = Bella.objects.filter(
                    created_at__lte=timezone.now()
                ).order_by('rank_number', '-created_at')

        queryset = self.get_serializer_class().setup_eager_loading(queryset, current_user)

        # cached_query_set = cache.get('thoughtslist_%s' % current_user.id)
        # if not cached_query_set:
        # evaluate queryset
        #    cached_query_set = list(queryset)
        # cache.set('thoughtslist_%s' % current_user.id, cached_query_set, 60)

        return queryset


class SelfListView(SuperThotListView):
    def get_queryset(self):
        if self.request.user.is_authenticated():
            username = self.kwargs.get('username')
            queryset = Bella.objects.filter(
                user__username__in=[username]
            ).order_by('-created_at').exclude(reports__id__in=[self.request.user.id])
        else:
            username = self.kwargs.get('username')
            queryset = Bella.objects.filter(user__username__in=[username]).order_by('-created_at')
        queryset = self.get_serializer_class().setup_eager_loading(queryset, self.request.user)
        return queryset


class SinglePostListView(SuperThotListView):

    def get_queryset(self):
        post_id = self.kwargs.get('post_id')

        #cached_query_set = cache.get('snglpostlist_%s' % post_id)
        # if not cached_query_set:
        # evaluate queryset
        # queryset = Bella.objects.filter(id=post_id)
        # cached_query_set = list(self.get_serializer_class().setup_eager_loading(queryset, self.request.user))
        # cache.set('snglpostlist_%s' % post_id, cached_query_set, 60)
        queryset = Bella.objects.filter(id=post_id)

        return self.get_serializer_class().setup_eager_loading(queryset, self.request.user)


class PostOfTagsListView(SuperThotListView):
    pagination_class = LargeResultsSetPagination

    def get_queryset(self):
        tagname = self.kwargs.get('tagname', None)
        user = self.request.user
        queryset = Bella.objects.filter(tags__name__in=[tagname]).order_by('-created_at').exclude(
            user__in=user.user1.blocked_people.all() if user.is_authenticated() else []
        )
        queryset = self.get_serializer_class().setup_eager_loading(queryset, user)
        return queryset


class FeedOfTagsListView(SuperThotListView):

    @staticmethod
    def get_blocked_users(current_user_):

        cached_blocked_users = cache.get('blocked_usrs_%s' % current_user_.id)
        if not cached_blocked_users:
            cached_blocked_users = list(current_user_.user1.blocked_people.all().values_list('id', flat=True))
        cache.set('blocked_usrs_%s' % current_user_.id, cached_blocked_users, 60 * 20)

        return cached_blocked_users

    def get_queryset(self):
        tagname = self.kwargs.get('tagname', None)
        if not tagname:
            return Bella.objects.none()
        else:
            tag = Tag.objects.filter(name=tagname.lower())
            if not tag:
                return Bella.objects.none()
            else:
                tag = tag[0]

        sorting_param = self.request.GET.get('sort_by', 'hot')
        current_user = self.request.user
        if sorting_param == 'new':
            if self.request.user.is_authenticated():

                blocked_users = self.get_blocked_users(current_user_=current_user)

                queryset = Bella.objects.filter(
                    created_at__lte=timezone.now(),
                    tags__in=[tag]
                ).order_by(
                    '-created_at'
                ).exclude(user_id__in=blocked_users).exclude(reports__id__in=[current_user.id])
            else:
                queryset = Bella.objects.filter(
                    created_at__lte=timezone.now(),
                    tags__in=[tag]
                ).order_by('-created_at')

        elif sorting_param == 'time_today':
            if current_user.is_authenticated():

                blocked_users = self.get_blocked_users(current_user_=current_user)

                queryset = Bella.objects.filter(
                    bellalike__created_at__gte=timezone.now() - timedelta(days=1),
                    tags__in=[tag]
                ).annotate(
                    today_likes_count=Count('bellalike__id')
                ).order_by(
                    '-today_likes_count', '-created_at'
                ).exclude(
                    user_id__in=blocked_users
                ).exclude(
                    reports__id__in=[current_user.id]
                )
            else:
                queryset = Bella.objects.filter(
                    bellalike__created_at__gte=timezone.now() - timedelta(days=1),
                    tags__in=[tag]
                ).annotate(
                    today_likes_count=Count('bellalike__id')
                ).order_by(
                    '-today_likes_count', '-created_at'
                )

        elif sorting_param == 'time_week':
            if current_user.is_authenticated():

                blocked_users = self.get_blocked_users(current_user_=current_user)

                queryset = Bella.objects.filter(
                    bellalike__created_at__gte=timezone.now() - timedelta(days=7),
                    tags__in=[tag]
                ).annotate(
                    week_likes_count=Count('bellalike__id')
                ).order_by(
                    '-week_likes_count', '-created_at'
                ).exclude(
                    user_id__in=blocked_users
                ).exclude(
                    reports__id__in=[current_user.id]
                )
            else:
                queryset = Bella.objects.filter(
                    bellalike__created_at__gte=timezone.now() - timedelta(days=7),
                    tags__in=[tag]
                ).annotate(
                    week_likes_count=Count('bellalike__id')
                ).order_by(
                    '-week_likes_count', '-created_at'
                )

        elif sorting_param == 'time_month':
            if current_user.is_authenticated():

                blocked_users = self.get_blocked_users(current_user_=current_user)

                queryset = Bella.objects.filter(
                    bellalike__created_at__gte=timezone.now() - timedelta(days=31),
                    tags__in=[tag]
                ).annotate(
                    month_likes_count=Count('bellalike__id')
                ).order_by(
                    '-month_likes_count', '-created_at'
                ).exclude(
                    user_id__in=blocked_users
                ).exclude(
                    reports__id__in=[current_user.id]
                )
            else:
                queryset = Bella.objects.filter(
                    bellalike__created_at__gte=timezone.now() - timedelta(days=31),
                    tags__in=[tag]
                ).annotate(
                    month_likes_count=Count('bellalike__id')
                ).order_by(
                    '-month_likes_count', '-created_at'
                )

        elif sorting_param == 'time_all':
            if current_user.is_authenticated():

                blocked_users = self.get_blocked_users(current_user_=current_user)

                queryset = Bella.objects.filter(
                    created_at__lte=timezone.now(),
                    tags__in=[tag]
                ).annotate(
                    all_likes_count=Count("likes")
                ).order_by(
                    '-all_likes_count', '-created_at'
                ).exclude(user_id__in=blocked_users).exclude(reports__id__in=[current_user.id])
            else:
                queryset = Bella.objects.filter(
                    created_at__lte=timezone.now(),
                    tags__in=[tag]
                ).annotate(
                    all_likes_count=Count("likes")
                ).order_by('-all_likes_count', '-created_at')

        else:
            if current_user.is_authenticated():

                blocked_users = self.get_blocked_users(current_user_=current_user)

                queryset = Bella.objects.filter(
                    created_at__lte=timezone.now(),
                    tags__in=[tag]
                ).order_by(
                    'rank_number', '-created_at'
                ).exclude(user_id__in=blocked_users).exclude(reports__id__in=[current_user.id])
            else:
                queryset = Bella.objects.filter(
                    created_at__lte=timezone.now(),
                    tags__in=[tag]
                ).order_by('rank_number', '-created_at')

        queryset = self.get_serializer_class().setup_eager_loading(queryset, current_user)

        return queryset


class PeopleListView(ListAPIView):

    serializer_class = UserWithPreviewSerializer

    @staticmethod
    def get_exclude_list(current_user_):
        cached_exclude_list = cache.get('usr_excl_list_%s' % current_user_.id)
        if not cached_exclude_list:
            blocked_users = list(current_user_.user1.blocked_people.all().values_list('id', flat=True))
            blocking_users = list(current_user_.blocked.all().values_list('user_id', flat=True))
            following_users = (
                    list(Follow.objects.filter(follower=current_user_).values_list("followee_id", flat=True)) +
                    [current_user_.id]
            )
            cached_exclude_list = list(set(blocked_users + following_users + blocking_users))
        cache.set('usr_excl_list_%s' % current_user_.id, cached_exclude_list, 60 * 5)  # cache for 1 min

        return cached_exclude_list

    def get_queryset(self):
        current_user = self.request.user

        page = self.request.GET.get('page', 1)

        cached_query_set = cache.get('peoplelist_%s_%s' % (current_user.id, page))
        if not cached_query_set:
            # evaluate queryset
            queryset = User.objects.annotate(
                total_bellasum=Coalesce(Sum('bellas__bellalike__bella_per_like'), 0),
                thought_count=Count('bellas')
            ).filter(
                thought_count__gte=3
            )

            if current_user.is_authenticated():
                exclude_list = self.get_exclude_list(current_user_=current_user)

                queryset = queryset.exclude(
                    id__in=exclude_list
                )

            queryset = queryset.order_by(
                '-total_bellasum', '-user1__like_update_time', '-user1__likes_count', '-thought_count'
            )

            cached_query_set = queryset

            cache.set('peoplelist_%s_%s' % (current_user.id, page), cached_query_set, 60 * 5)

        return cached_query_set

    def get_serializer_context(self):
        """Use this to add extra context."""
        context = super(PeopleListView, self).get_serializer_context()
        context['bella_average_price'] = AppSetting.objects.values_list("bella_average_price", flat=True).first()
        cur_user_followings_list = []
        if self.request.user.is_authenticated():
            cur_user_followings_list = Follow.objects.filter(follower=self.request.user).values_list(
                'followee_id',
                flat=True
            )
        context['cur_user_followings'] = cur_user_followings_list
        return context


class TagListView(ListAPIView):
    serializer_class = TagWithPreviewSerializer

    def get_queryset(self):
        excluded_tags = ExcludedTag.objects.values_list('tags', flat=True)

        # Get top trends in last 24 hours
        return Tag.objects.filter(
            bella__created_at__gte=c_datetime.now() - timedelta(days=1)
        ).annotate(
            number_of_tag=Count('slug')
        ).exclude(
            id__in=excluded_tags
        ).order_by(
            '-number_of_tag'
        )

    def get_serializer_context(self):
        """Use this to add extra context."""
        context = super(TagListView, self).get_serializer_context()
        context['bella_average_price'] = AppSetting.objects.values_list("bella_average_price", flat=True).first()
        cur_user_followings_list = []
        if self.request.user.is_authenticated():
            cur_user_followings_list = Follow.objects.filter(follower=self.request.user).values_list(
                'followee_id',
                flat=True
            )
        context['cur_user_followings'] = cur_user_followings_list
        return context


class LikeListView(ListAPIView):
    serializer_class = UserWithSmallAvatarSerializer
    pagination_class = MediumResultsSetPagination

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        try:
            pk = self.kwargs.get('pk')
            bella = Bella.objects.filter(pk=pk)[0]
            queryset = bella.likes.select_related('user1').order_by('bellalike__created_at')
            return queryset
        except Exception:
            return User.objects.none()

    def get_serializer_context(self):
        """Use this to add extra context."""
        context = super(LikeListView, self).get_serializer_context()
        cur_user_followings_list = []
        if self.request.user.is_authenticated():
            cur_user_followings_list = Follow.objects.filter(follower=self.request.user).values_list('followee_id',
                                                                                                     flat=True)
        context['cur_user_followings'] = cur_user_followings_list
        return context


class DiscoverAPIView(APIView):
    permission_classes = []

    def get(self, request, **kwargs):

        # Get top trends in last 24 hours
        excluded_tags = ExcludedTag.objects.values_list('tags', flat=True)

        trending = Bella.objects.filter(
            created_at__gte=c_datetime.now() - timedelta(days=1),
            tags__isnull=False
        ).values('tags').annotate(
            num_items=Count('id'), id=F('tags__id'), name=F('tags__name')
        ).exclude(
            id__in=excluded_tags
        ).order_by('-num_items')[:10]

        # trending = []
        # for tag in top_ten_trends:
        #     if tag['tags']:
        #         tag_data = TagSerializer(Tag.objects.get(id=tag['tags'])).data
        #         tag_data.update({'num_items': tag['cnt']})
        #         trending.append(tag_data)
        suggested = []
        earners = []

        # Get likers/suggested person from ActivityHistory
        for activity in ActivityHistory.objects.filter(
                created_at__gt=c_datetime.now() - timedelta(days=1),
                activity_type=ActivityType.Like
        ).values('user').annotate(
            likes_24hrs=Count('id'), username=F('user__username'), avatar=F('user__user1__profile_pic')
        ).order_by('-likes_24hrs')[:5]:

            avatar = "<img src='" + get_upload_url(activity['avatar']) + "'></img>"
            activity.update({'avatar': avatar})
            suggested.append(activity)

        # Get top earners
        bella_average_price = AppSetting.objects.values_list("bella_average_price", flat=True).first()
        for activity in ActivityHistory.objects.filter(
                created_at__gt=c_datetime.now() - timedelta(days=1),
                activity_type=ActivityType.Like
        ).values('receiver').annotate(
            like_count=Count('id'), username=F('receiver__username'), avatar=F('receiver__user1__profile_pic')
        ).order_by('-like_count')[:5]:
            avatar = "<img src='" + get_upload_url(activity['avatar']) + "'></img>"
            activity.update({'avatar': avatar})
            activity.update({
                'earnings': activity['like_count'] * bella_average_price
            })
            earners.append(activity)

        context = {
            'trending': list(trending),
            'suggested': suggested,
            'earners': earners
        }
        return JsonResponse(context)


class SearchTop(APIView):
    permission_classes = []

    def get(self, request, **kwargs):

        query = request.GET.get('term', None)
        search_in = request.GET.get('in', None)

        result = []
        # Search relevant users and tags
        if query:
            if search_in != 'tag':
                bella_average_price = AppSetting.objects.values_list("bella_average_price", flat=True).first()
                blocked_people_ids = []
                if request.user.is_authenticated():
                    blocked_people_ids = request.user.blocked.all().values_list('user_id', flat=True)

                users = User.objects.filter(
                    username__istartswith=query, is_active=True, is_superuser=False
                ).exclude(
                    id__in=blocked_people_ids
                ).annotate(
                    total_bellasum=Coalesce(Sum('bellas__bellalike__bella_per_like'), 0)
                ).order_by('-total_bellasum', 'username')[:10]

                cur_user_followings_list = []
                cur_user_followers_list = []
                if request.user.is_authenticated():
                    cur_user_followings_list = Follow.objects.filter(follower=request.user).values_list(
                        'followee_id', flat=True)
                    cur_user_followers_list = Follow.objects.filter(followee=request.user).values_list(
                        'follower_id', flat=True)

                result.extend(
                    UserSearchSerializer(
                        users,
                        many=True,
                        context={
                            'bella_average_price': bella_average_price,
                            'user': request.user if request.user.is_authenticated() else None,
                            'cur_user_followings_list': cur_user_followings_list,
                            'cur_user_followers_list': cur_user_followers_list,
                            'type': 'relevant_user'
                        }
                    ).data
                )

            if search_in != 'user':
                if request.user.is_authenticated():
                    reported_bellas = list(request.user.reports.values_list("id", flat=True))
                    tags = Tag.objects.annotate(num_items=Sum(
                        Case(
                            When(Q(taggit_taggeditem_items__object_id__in=reported_bellas,
                                   taggit_taggeditem_items__content_type__model=Bella.__name__.lower()), then=0),
                            default=1,
                            output_field=IntegerField()
                        ))).filter(
                        name__istartswith=query.lower(),
                        num_items__gt=0
                    ).order_by('-num_items', 'name')[:10]
                else:
                    tags = Tag.objects.annotate(
                        num_items=Count('taggit_taggeditem_items__object_id')
                    ).filter(
                        name__istartswith=query.lower(),
                        num_items__gt=0
                    ).order_by('-num_items', 'name')[:10]

                tag_serializer_data = TagSerializer(
                    tags,
                    many=True,
                    context={'request': request, 'type': 'relevant_tag'}
                ).data
                result.extend(
                    tag_serializer_data
                )

        return JsonResponse(status=200, data=result, safe=False)


class SearchAPI(APIView):
    permission_classes = []

    def get(self, request, **kwargs):
        query = self.kwargs['query'].lower()
        search_in = request.GET.get('in', None)

        result = []
        if search_in != 'tag':
            bella_average_price = AppSetting.objects.values_list("bella_average_price", flat=True).first()
            blocked_people_ids = []
            if request.user.is_authenticated():
                blocked_people_ids = request.user.blocked.all().values_list('user_id', flat=True)

            users = User.objects.filter(
                username__istartswith=query, is_active=True, is_superuser=False
            ).exclude(
                id__in=blocked_people_ids
            ).annotate(
                total_bellasum=Coalesce(Sum('bellas__bellalike__bella_per_like'), 0)
            ).order_by('-total_bellasum', 'username')[:4]

            cur_user_followings_list = []
            cur_user_followers_list = []
            if request.user.is_authenticated():
                cur_user_followings_list = Follow.objects.filter(follower=request.user).values_list(
                    'followee_id', flat=True)
                cur_user_followers_list = Follow.objects.filter(followee=request.user).values_list(
                    'follower_id', flat=True)

            result.extend(
                UserSearchSerializer(
                    users,
                    many=True,
                    context={
                        'bella_average_price': bella_average_price,
                        'user': request.user if request.user.is_authenticated() else None,
                        'cur_user_followings_list': cur_user_followings_list,
                        'cur_user_followers_list': cur_user_followers_list,
                    }
                ).data
            )

        if search_in != 'user':
            if request.user.is_authenticated():
                reported_bellas = list(request.user.reports.values_list("id", flat=True))
                tags = Tag.objects.annotate(num_items=Sum(
                    Case(
                        When(
                            Q(
                                taggit_taggeditem_items__object_id__in=reported_bellas,
                                taggit_taggeditem_items__content_type__model=Bella.__name__.lower()
                            ),
                            then=0
                        ),
                        default=1,
                        output_field=IntegerField()
                    ))).filter(
                    name__istartswith=query.lower(),
                    num_items__gt=0
                ).order_by('-num_items', 'name')[:4]
            else:
                tags = Tag.objects.annotate(
                    num_items=Count('taggit_taggeditem_items__object_id')
                ).filter(
                    name__istartswith=query.lower(),
                    num_items__gt=0
                ).order_by('-num_items', 'name')[:4]

            tag_serializer_data = TagSerializer(tags, many=True, context={'request': request}).data
            result.extend(
                tag_serializer_data
            )

        return JsonResponse(status=200, data=result, safe=False)


@login_required
def report(request):
    if request.method == 'POST':
        deleted = False
        user = request.user
        pk = request.POST.get('pk', None)
        bela = get_object_or_404(Bella, pk=pk)
        if bela.reports.filter(id=user.id).exists():
            reported = False
        else:
            bela.reports.add(user)
            reported = True
            if bela.total_reports >= 3:
                bela.delete()
                deleted = True
    ctx = {'reported': reported, 'pk': pk, 'deleted': deleted}
    return HttpResponse(json.dumps(ctx), content_type='application/json')


@login_required
@require_POST
@csrf_exempt
def delete(request):
    user = request.user
    pk = request.POST.get('pk', None)
    bela = get_object_or_404(Bella, pk=pk, user=user)
    bela.delete()
    ctx = {'pk': pk}
    return HttpResponse(json.dumps(ctx), content_type='application/json')


@login_required
@csrf_exempt
def adminDelete(request):
    if request.method == 'POST':
        user = request.user
        if user.is_superuser:
            pk = request.POST.get('pk', None)
            bela = get_object_or_404(Bella, pk=pk)
            bela.delete()
            ctx = {'pk': pk}
            return HttpResponse(json.dumps(ctx), content_type='application/json')
        else:
            return HttpResponseNotAllowed(HttpResponse(None, content_type='application/json'))
    return HttpResponseNotAllowed(HttpResponse(None, content_type='application/json'))


@login_required
@csrf_exempt
def users(request, username):
    if request.method == 'POST' and username == request.user.username:
        form = ThoughtForm(request.POST, request.FILES)

        if form.is_valid():

            post = form.save(commit=False)
            post.user = request.user
            post.save()

            # add tag into the post
            matches = re.findall(r'#\w+', post.content, re.UNICODE)
            for tag in set(matches):
                post.content = post.content.replace(tag, tag.lower())
                post.tags.add(tag[1:].lower())
            post.save()

            request.user.user1.number_of_posts = request.user.user1.number_of_posts + 1
            request.user.user1.save(update_fields=['number_of_posts'])

            matches = re.findall(r'@\w+', post.content, re.UNICODE)
            for username in set(matches):
                users = User.objects.filter(username=username[1:])
                if users and users[0] != request.user:
                    notify.send(request.user, recipient=users[0], verb=" tagged you in a post", action_object=post)
                    # ========= Sending OneSignal Notification =======
                    if users[0].onesignal_players.count() > 0:
                        player_ids = list(users[0].onesignal_players.values_list('player_id', flat=True))
                        content = "%s tagged you in a post" % request.user.username
                        send_onesignal_push_notification(request.user, content, player_ids)

                    # ================================================

                    Channel("notify_user").send({
                        'username': users[0].username
                    })

            # call API gateway
            if post.image_path:
                process_image_with_lambda.delay(post.image_path, post.id)

            # TODO use django activity stream
            ActivityHistory.objects.create(user=request.user, activity_type=ActivityType.Post)
            thot = BellaSerializer(post, context={'request': request}).data

            # Reward user
            notifications = []

            user = request.user
            reward_update = helpers.complete_reward_task(user, RewardTaskType.POST_PHOTO.name)
            if reward_update is not None:
                notifications.append(reward_update)

            return HttpResponse(
                json.dumps(
                    {
                        "status": 'success',
                        "result": {"thot": thot},
                        "notifications": notifications
                    }
                ),
                content_type="application/json"
            )

        else:
            return HttpResponse(json.dumps({"status": 'fail', "error": form.errors}), content_type="application/json")


@login_required
@csrf_exempt
def ajax_follow(request):
    ctx = {}
    if request.method == "POST":
        user = request.user
        pk = request.POST.get('want_to_follow')
        if not pk:
            return HttpResponse(json.dumps(ctx), content_type='application/json', status=404)
        want_to_follow = User.objects.get(id=int(pk))
        if want_to_follow:
            Follow.objects.get_or_create(follower=user, followee=want_to_follow)

            yesterday = timezone.now() - datetime.timedelta(days=1)
            notify_history = Notification.objects.filter(
                recipient=want_to_follow,
                actor_object_id=user.pk,
                verb=" has followed you!",
                timestamp__gte=yesterday
            ).first()

            if notify_history is None:
                notify.send(user, recipient=want_to_follow, verb=" has followed you!", action_object=user)

                # ========= Sending OneSignal Notification =======
                if want_to_follow.onesignal_players.count() > 0:
                    player_ids = list(want_to_follow.onesignal_players.values_list('player_id', flat=True))
                    content = "%s has followed you!" % user.username
                    send_onesignal_push_notification(user, content, player_ids)

                # ================================================

                Channel("notify_user").send({
                    'username': want_to_follow.username
                })

            ActivityHistory.objects.create(user=user, receiver=want_to_follow, activity_type=ActivityType.Follow)
            ctx = {'followed_user_id': pk}

    return HttpResponse(json.dumps(ctx), content_type='application/json')


@login_required
@csrf_exempt
def ajaxUnfollow(request):
    ctx = {}
    if request.method == "POST":
        user = request.user
        pk = request.POST.get('want_to_unfollow')
        if not pk:
            return HttpResponse(json.dumps(ctx), content_type='application/json', status=404)
        want_to_unfollow = User.objects.get(id=pk)
        if want_to_unfollow:
            Follow.objects.remove_follower(user, want_to_unfollow)
            ActivityHistory.objects.create(user=user, receiver=want_to_unfollow, activity_type=ActivityType.UnFollow)
            ctx = {'unfollowed_user_id': pk}

    return HttpResponse(json.dumps(ctx), content_type='application/json')


@login_required
@require_POST
@csrf_exempt
def like(request):
    user = request.user
    pk = request.POST.get('pk', None)
    bela = get_object_or_404(Bella, pk=pk)
    liked = False
    error = ''
    bela_result = None
    user_result = None
    total_bela = 0
    total_earning = 0
    total_likes = 0
    notifications = []

    if bela.user != user:
        bella_per_like = AppSetting.objects.values_list("bella_per_like", flat=True).first()
        if bela.likes.filter(id=user.id).exists():
            # bela.likes.remove(user)
            # ActivityHistory.objects.create(user=user, activity_type=ActivityType.Like)
            liked = True
        elif user.user1.belacoin >= bella_per_like:
            user.user1.belacoin -= bella_per_like
            user.user1.save(update_fields=['belacoin'])

            bela.user.user1.belacoin += bella_per_like
            bela.user.user1.save(update_fields=['belacoin'])
            # add a new like for a company
            bella_like = BellaLike(user=user, bella=bela, bella_per_like=bella_per_like)
            bella_like.save()

            liked = True

            notify.send(request.user, recipient=bela.user, verb=" has liked your post!", action_object=bela)

            # ========= Sending OneSignal Notification =======
            if bela.user.onesignal_players.count() > 0:
                player_ids = list(bela.user.onesignal_players.values_list('player_id', flat=True))
                content = "%s has liked your post!" % request.user.username
                send_onesignal_push_notification(request.user, content, player_ids)

            # ================================================

            Channel("notify_user").send({
                'username': bela.user.username
            })

            ActivityHistory.objects.create(user=user, receiver=bela.user, activity_type=ActivityType.Like)

            bela_result = UserWithSmallAvatarSerializer(
                user, context={'request': request, 'cur_user_followings': []}
            ).data
            total_bela = bela.total_bella
            total_likes = bela.likes.count()
            total_earning = AppSetting.objects.values_list("bella_average_price", flat=True).first() * total_bela
            user_result = UserProfileSerializer(user.user1).data

            reward_update = helpers.complete_reward_task(user, RewardTaskType.GIVE_LIKE.name)
            if reward_update is not None:
                notifications.append(reward_update)

        else:
            error = BelaLikeError.E_INSUFFICIENT_COIN
    else:
        error = BelaLikeError.E_SAME_USER

    ctx = {
        'status': 'success' if not error else 'fail',
        'result': {
            'liked': liked,
            'bela': bela_result,
            'total_bela': total_bela,
            'total_likes': total_likes,
            'total_earning': total_earning,
            'user': user_result,
        },
        'notifications': notifications,
        'error': error
    }
    # use mimetype instead of content_type if django < 5
    return HttpResponse(json.dumps(ctx), content_type='application/json')


class WithdrawRequestView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request):
        user = request.user
        serializer = WithdrawRequestSerializer(data=request.data)
        if serializer.is_valid():
            amount = serializer.validated_data['amount']
            address = serializer.validated_data['address']

            if user.user1.belacoin >= amount:
                withdrawal = WithdrawRequest.objects.create_request(
                    status=WithdrawStatus.PENDING.name,
                    author=request.user,
                    address=address,
                    amount=amount
                )

                user.user1.belacoin -= amount
                user.user1.save()

                key_str = get_random_string(length=32)

                email_obj = WithdrawEmailConfirmation(request=withdrawal, key=key_str)
                email_obj.save()

                mail = compose_withdrawal_email(email_obj)
                send_html_mail('Your withdrawal request', [user.email], mail)

                notify.send(request.user, recipient=user, verb=", We have recieved your withdrawl request!")

                # ========= Sending OneSignal Notification =======
                if user.onesignal_players.count() > 0:
                    player_ids = list(user.onesignal_players.values_list('player_id', flat=True))
                    content = "%s, We have recieved your withdrawl request!" % user.username
                    send_onesignal_push_notification(user, content, player_ids)

                # ================================================

                Channel("notify_user").send({
                    'username': user.username
                })

                return Response({
                    'status': 'success',
                    'balance': request.user.user1.belacoin
                })
            else:
                return Response({
                    'status': 'failed',
                    'code': 'Not enough balance'
                }, status=400)
        else:
            return Response({
                'status': 'failed',
                'code': 'Bad request',
                'error': serializer.errors
            }, status=400)


class WithdrawFeeSettingsView(ListAPIView):
    pagination_class = None
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = WithdrawFeeSettingSerializer
    queryset = WithdrawFeeSetting.objects.all().order_by('range_from')


def withdraw_confirm(request, *args, **kwargs):
    key = kwargs.get('key', '')

    try:
        obj = WithdrawEmailConfirmation.objects.filter(key=key)[0]
        obj.request.status = WithdrawStatus.CONFIRMED.name
        obj.request.save()

        return render(request, 'withdraw/email_confirmed.html', {
            'message': 'Your withdrawal was confirmed'
        })
    except Exception:
        return render(request, 'withdraw/email_confirmed.html', {
            'message': 'This key is invalid'
        })


@login_required
@csrf_exempt
def update_caption(request):
    if request.method == 'POST':
        json_data = json.loads(request.body)
        thought_id = json_data['pk']
        thought = Bella.objects.filter(id=thought_id).first()
        new_caption = json_data['caption']
        user = request.user
        if thought and user.id == thought.user.id:
            thought.content = new_caption
            thought.save(update_fields=['content'])
            matches = re.findall(r'#\w+', new_caption)
            thought.tags.clear()

            for tag in set(matches):
                thought.tags.add(tag[1:].lower())
            return HttpResponse(json.dumps({'result': new_caption, 'status': 'success'}),
                                content_type='application/json')
        else:
            return HttpResponse(json.dumps({'status': 'non_permission'}),
                                content_type='application/json')


@login_required
@csrf_exempt
def update_location(request):
    if request.method == 'POST':
        json_data = json.loads(request.body)
        thought_id = json_data['pk']
        thought = Bella.objects.filter(id=thought_id).first()
        new_location = json_data['location']
        user = request.user
        if thought and user.id == thought.user.id:
            thought.location = new_location
            thought.save(update_fields=['location'])

            return HttpResponse(json.dumps({'result': new_location, 'status': 'success'}),
                                content_type='application/json')
        else:
            return HttpResponse(json.dumps({'status': 'non_permission'}),
                                content_type='application/json')


@login_required
@require_POST
@csrf_exempt
def block(request):
    pk = request.POST.get('pk', None)
    user = request.user
    to_block = User.objects.get(id=pk)
    if user and to_block and user != to_block:
        user.user1.blocked_people.add(to_block)
        user.user1.save()

        Follow.objects.remove_follower(user, to_block)
        ActivityHistory.objects.create(user=request.user, receiver=to_block, activity_type=ActivityType.Block)
    ctx = {'blocked': pk}
    return HttpResponse(json.dumps(ctx), content_type='application/json')


def live_notification_list(request):
    if not request.user.is_authenticated():
        data = {
            'unread_count': 0,
            'notification_list': []
        }
        return JsonResponse(data)

    try:
        num_to_fetch = request.GET.get('max', 20)  # If they don't specify, make it 20.
        num_to_fetch = int(num_to_fetch)
        num_to_fetch = max(1, num_to_fetch)  # if num_to_fetch is negative, force at least one fetched notifications
        num_to_fetch = min(num_to_fetch, 20)  # put a sane ceiling on the number retrievable
    except ValueError:
        num_to_fetch = 5  # If casting to an int fails, just make it 5.

    notifications = request.user.notifications.all()[0:num_to_fetch]

    cur_user_followings_list = []
    if request.user.is_authenticated():
        cur_user_followings_list = Follow.objects.filter(follower=request.user).values_list('followee_id',
                                                                                            flat=True)
    context = {
        'cur_user_followings': cur_user_followings_list
    }

    data = {
        'unread_count': request.user.notifications.unread().count(),
        'notifications_list': NotificationSerializer(notifications, many=True, context=context).data
    }
    return JsonResponse(data)


@login_required
def mark_all_as_read(request):
    request.user.notifications.mark_all_as_read()

    return JsonResponse({'success': True})


@login_required
def mark_it_as_read(request, not_id):
    notification = Notification.objects.get(id=not_id)
    notification.mark_as_read()
    return JsonResponse({'success': True})


@login_required
def search_place(request):
    lat = request.GET.get('latitude', default=None)
    lng = request.GET.get('longitude', default=None)
    keyword = request.GET.get('keyword')

    places = GeoPlace.objects.all()
    response_data = list()
    response_data.append({'location': ""})
    if lat is not None and lng is not None:
        result = neutrino_api_client.geolocation.geocode_reverse(lat, lng)
        if result.found:
            response_data = list()
            response_data.append({'location': "%s, %s" % (result.city, result.state)})

        # places = places.filter(location__dwithin=(Point(lat, lng, srid=4326), D(km=5)))

    # if keyword is not None:
    # places = places.filter(
    #     Q(name__istartswith=keyword) | Q(asciiname__istartswith=keyword) | Q(country_name__istartswith=keyword)
    # )

    # places = places[:5]
    # response_data = GeoPlaceSerializer(places, many=True).data
    return JsonResponse(status=200, data=response_data, safe=False)


def growth_statistics(request):
    total_users = User.objects.count()
    new_users_in_7 = User.objects.filter(date_joined__gte=c_datetime.now() - timedelta(days=7)).count()
    new_users_in_1 = User.objects.filter(date_joined__gte=c_datetime.now() - timedelta(days=1)).count()
    response_data = {
        'total_users': total_users,
        'new_users_in_1': new_users_in_1,
        'new_users_in_7': new_users_in_7
    }

    return JsonResponse(data=response_data, safe=False)


@csrf_exempt
def increase_deposit(request):
    if request.method == 'POST' and get_client_ip(request)[0] == settings.DEPOSIT_SERVER:
        json_data = json.loads(request.body)
        user_id = json_data.get('user_id', None)
        deposit_address = json_data.get('deposit_address', None)
        deposit_amount = json_data.get('deposit_amount', None)
        transaction_hash = json_data.get('transactionHash', None)
        time = json_data.get('time', None)
        token = json_data.get('token', None)
        try:
            if (user_id and
                    deposit_address and
                    deposit_amount and
                    token and
                    hmac.compare_digest(
                        hmac.new(
                            settings.SECRET_RECV_PASSWORD,
                            "{}, {}, {}, {}, {}".format(
                                str(user_id),
                                str(deposit_address),
                                str(deposit_amount),
                                str(transaction_hash),
                                str(time)
                            ),
                            hashlib.sha256
                        ).hexdigest(), str(token))):

                app_setting = AppSetting.objects.first()
                user = User.objects.filter(id=user_id).first()

                if user and user.user1.belacoin_address == deposit_address:

                    # deposit user belacoin balance
                    user.update_deposit_bonus(deposit_amount=deposit_amount)

                    notify.send(user, recipient=user, verb=", We have accepted your deposit request!")
                    # ========= Sending OneSignal Notification =======
                    if user.onesignal_players.count() > 0:
                        player_ids = list(user.onesignal_players.values_list('player_id', flat=True))
                        content = "%s, We have accepted your deposit request!" % user.username
                        send_onesignal_push_notification(user, content, player_ids)

                    # ================================================
                    Channel("notify_user").send({
                        'username': user.username
                    })

                    BellaDeposit.objects.create(
                        user=user,
                        deposit_address=deposit_address,
                        deposit_amount=float(deposit_amount),
                        transactionHash=transaction_hash,
                        time=parse(time)
                    )
                    return JsonResponse(
                        {'status': 'success', 'code': "Succesfully increase %s BELA!" % (str(deposit_amount))})
                else:
                    return JsonResponse({'status': 'failed', 'code': "Can't find valid user or deposit_address"})
            else:
                return JsonResponse({'status': 'failed', 'code': "Invalid Request"})
        except:
            return JsonResponse({'status': 'failed', 'code': "Invalid Format"})

    else:
        return JsonResponse({'status': 'failed', 'code': "Not POST Request"})


class UploadSignS3ViewSet(APIView):
    # This would receive file-name and file-type to create a signed request to directly upload to S3.

    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        s3_upload_bucket = settings.AWS_STORAGE_BUCKET_NAME
        max_file_size = settings.UPLOAD_S3_MAX_FILE_SIZE
        user_id = request.user.id
        try:
            boto_client = boto3.client('s3',
                                       aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                                       aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)
            file_name = 'avatars/%s/%s_%s' % (user_id, str(uuid.uuid4()), request.GET.get('file-name'))
            file_type = request.GET.get('file-type')

            file_path = 'https://%s.s3.amazonaws.com/%s' % (s3_upload_bucket, file_name)
            presigned_post = boto_client.generate_presigned_post(
                Bucket=s3_upload_bucket,
                Key=file_name,
                Fields={"Content-Type": file_type},
                Conditions=[
                    {"Content-Type": file_type},
                    ["content-length-range", 0, max_file_size]
                ],
                ExpiresIn=3600
            )

            return JsonResponse(
                {
                    'data': presigned_post,
                    'url': file_path
                })
        except Exception as e:
            response = {'status': 'error', 'message': str(e)}
            return JsonResponse(response,
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@login_required
def assign_deposit_address(request):
    try:
        if settings.PRODUCTION and request.user.is_authenticated():
            up = request.user.user1
            if not up.belacoin_address.startswith("0xbe1a"):
                resp = requests.post(
                    "http://%s/api/vanity_address" % settings.DEPOSIT_SERVER,
                    data={
                        'uid': up.user.id,
                        'uname': up.user.username,
                        'token': hmac.new(
                            settings.SECRET_SEND_PASSWORD,
                            "%s, %s" % (str(up.user.id), str(up.user.username)),
                            hashlib.sha256
                        ).hexdigest(),
                    })

                if resp.status_code == requests.codes.ok and resp.json().get("status", "failed") == "success":
                    up.belacoin_address = resp.json().get("address", "No Address, Contact Support")
                    up.save()
                    return JsonResponse(
                        status=200,
                        data={"status": "success", "content": up.belacoin_address},
                        safe=False
                    )
                else:
                    return JsonResponse(
                        status=200,
                        data={"status": "error", "content": "Failed for some reason. Please try again later!"},
                        safe=False
                    )
            else:
                return JsonResponse(
                    status=200,
                    data={"status": "exist", "content": up.belacoin_address},
                    safe=False
                )
        else:
            return JsonResponse(
                status=200,
                data={"status": "error", "content": "Failed for some reason. Please try again later!"},
                safe=False
            )
    except Exception as e:
        return JsonResponse(
            status=200,
            data={"status": "error", "content": "Failed for some reason. Please try again later!"},
            safe=False
        )


class UpdateCoverPicView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def patch(self, request):
        upload_cover_form = CoverpicForm(request.data or request.POST or None)

        response = {"status": "error"}

        try:
            if upload_cover_form.is_valid():
                cover_pic = upload_cover_form.cleaned_data['cover_pic']

                result = utils.upload_aws_photo(cover_pic, 'cover_pic')

                if utils.is_aws_successful(result):
                    user_profile = request.user.user1
                    user_profile.cover_pic = upload_cover_form.cleaned_data['cover_pic']
                    user_profile.save()
                    response.update({
                        "status": "success",
                        "result": {
                            "cover_url": get_upload_url(user_profile.cover_pic)
                        }
                    })
                else:
                    response.update({"status": "error"})
            else:
                response.update({"status": "error", "message": upload_cover_form.errors['cover_pic'][0]})
        except Exception as e:
            print(e)
            response.update({"status": "error"})

        return HttpResponse(json.dumps(response), content_type='application/json')


class UploadProfilePicView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        upload_avatar_form = AvatarForm(request.data or request.POST or None)

        response = {"status": "fail"}

        try:
            if upload_avatar_form.is_valid():
                profile_pic = upload_avatar_form.cleaned_data['profile_pic']

                result = utils.upload_aws_photo(profile_pic, 'profile_pic')

                if utils.is_aws_successful(result):
                    user_profile = request.user.user1
                    user_profile.profile_pic = profile_pic
                    user_profile.save()
                    signed_profile_pic_url = get_upload_url(profile_pic)

                    notifications = []

                    # Reward user
                    user = request.user
                    reward_update = helpers.complete_reward_task(user, task_type=RewardTaskType.GET_PROFILE_PIC.name)
                    if reward_update:
                        notifications.append(reward_update)

                    response.update({
                        "status": "success",
                        "result": {
                            "avatar": "<img src='" + signed_profile_pic_url + "'></img>",
                            "avatar_url": signed_profile_pic_url
                        },
                        'notifications': notifications
                    })

                else:
                    response.update({"status": "fail"})
            else:
                response.update({"status": "fail", "message": upload_avatar_form.errors['profile_pic'][0]})
        except Exception as e:
            print(e)
            response.update({"status": "fail"})

        return HttpResponse(json.dumps(response), content_type='application/json')


class VerifyPhoneView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, **kwargs):
        user = request.user

        phone_number = request.data.get('phone_number', None)
        if phone_number is not None:
            confirmation = PhoneNumberConfirmation.objects.filter(phone_number=phone_number, user=user).first()

            if confirmation and confirmation.verified:
                return Response({
                    'status': 'fail',
                    'code': 'BAD_REQUEST',
                    'message': 'This phone number was already used.'
                }
                )

            telephony = neutrino_api_client.telephony
            response = telephony.sms_verify(phone_number)
            if response.number_valid:
                security_code = response.security_code
                if confirmation is None:
                    confirmation = PhoneNumberConfirmation(user=user, phone_number=phone_number)

                confirmation.code = security_code
                confirmation.save()

                return Response({
                    'status': 'success',
                    'result': {
                        'phone_number': phone_number
                    }
                })
            else:
                return Response(
                    {
                        'status': 'fail',
                        'code': 'BAD_REQUEST',
                        'message': 'Phone number is not valid'
                    }
                )
        else:
            return Response(
                {
                    'status': 'fail',
                    'code': 'BAD_REQUEST',
                    'message': 'Phone number is required'
                }
            )


class VerifyPhoneConfirmView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, **kwargs):
        user = request.user
        phone_number = request.data.get('phone_number', None)
        code = request.data.get('code', None)

        if phone_number is None or code is None:
            return Response({
                'status': 'fail',
                'code': 'BAD_REQUEST',
                'message': 'Phone number or code is required.'
            })
        else:
            confirmation = PhoneNumberConfirmation.objects.filter(user=user, phone_number=phone_number).first()
            if confirmation is not None and confirmation.code == code:
                if confirmation.verified:
                    return Response({
                        'status': 'fail',
                        'code': 'BAD_REQUEST',
                        'message': 'This phone number was already verified'
                    })

                confirmation.verified = True
                confirmation.save()

                user.user1.phone_number = phone_number
                user.user1.save()

                return Response({
                    'status': 'success',
                    'message': 'Confirmation was successful'
                })
            else:
                return Response({
                    'status': 'fail',
                    'code': 'INVALID_CODE',
                    'message': 'Security code is not valid'
                })


@csrf_exempt
def sign_in(request):
    response_data = {"status": "error"}
    try:
        if request.method == 'POST':
            ip = get_client_ip(request)[0]
            current_time = c_datetime.now()
            time_diff = 0
            blacklist_ip = None
            try:
                blacklist_ip = BlackListIP.objects.get(ip=ip)
                last_failed_time = blacklist_ip.updated_at.replace(tzinfo=None)
                time_diff = (current_time - last_failed_time).total_seconds()
            except Exception as e:
                pass

            if blacklist_ip and blacklist_ip.failed_count >= 5 and time_diff < 600:
                response_data = {"status": "error",
                                 "message": "You are blocked from logging in for 10 minutes due to too many failed login attempt."}
                return HttpResponse(json.dumps(response_data), content_type='application/json')

            username = request.POST.get('username', default=None)
            password = request.POST.get('password', default=None)
            user = authenticate(username=username, password=password)
            if username is not None and password is not None and user is not None:
                if user.is_active:
                    # If the account is valid and active, we can log the user in.
                    # We'll send the user back to the homepage.
                    if user.user1.suspend_until:
                        datenow = datetime.datetime.now(user.user1.suspend_until.tzinfo)
                        suspend_td = user.user1.suspend_until - datenow
                    else:
                        suspend_td = None

                    if suspend_td is not None and suspend_td.total_seconds() > 0:
                        response_data = {"status": "error",
                                         "message": "Your Belacam account has been suspended for " + format_td(
                                             suspend_td) + "."}
                    else:
                        onesignal_player_id = request.POST.get('onesignal_player_id', default=None)
                        if onesignal_player_id is not None:
                            OneSignalPlayer.objects.get_or_create(user=user, player_id=onesignal_player_id)

                        if blacklist_ip:
                            blacklist_ip.delete()
                        login(request, user)
                        if "CSRF_COOKIE" not in request.META:
                            csrf_token = ""
                        else:
                            csrf_token = request.META["CSRF_COOKIE"]
                        response_data = {"status": "success", "csrf_token": csrf_token, "user_id": user.id,
                                         "user_name": user.username, "language": user.account.language}
                else:
                    # An inactive account was used - no logging in!
                    response_data = {"status": "error", "message": "Your Belacam account has not been activated yet."}
            else:
                if not blacklist_ip:
                    blacklist_ip = BlackListIP.objects.create(ip=ip)
                else:
                    if time_diff < 30:
                        blacklist_ip.failed_count += 1
                    else:
                        blacklist_ip.failed_count = 0
                    blacklist_ip.save()
                response_data = {"status": "error", "message": "Incorrect Username or Password"}

            return HttpResponse(json.dumps(response_data), content_type='application/json')
        else:
            response_data = {"status": "error", "message": "Only Allow Post Request!"}
            return HttpResponse(json.dumps(response_data), content_type='application/json')
    except Exception as e:
        response_data.update({"message": e.message})
        return HttpResponse(json.dumps(response_data), content_type='application/json')


class SignupView(account.views.SignupView):

    @method_decorator(sensitive_post_parameters())
    @method_decorator(csrf_exempt)
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        self.request = request
        self.args = args
        self.kwargs = kwargs
        self.setup_signup_code()
        return super(account.views.SignupView, self).dispatch(request, *args, **kwargs)

    def form_invalid(self, form):
        response_data = {"status": "error", "message": form.errors.as_text()}
        return HttpResponse(json.dumps(response_data), content_type='application/json')

    def form_valid(self, form):
        self.created_user = self.create_user(form, commit=False)
        # prevent User post_save signal from creating an Account instance
        # we want to handle that ourself.
        self.created_user._disable_account_creation = True
        self.created_user.save()
        self.use_signup_code(self.created_user)
        email_address = self.create_email_address(form)
        if settings.ACCOUNT_EMAIL_CONFIRMATION_REQUIRED and not email_address.verified:
            self.created_user.is_active = False
            self.created_user.save()
        self.create_account(form)
        self.after_signup(form)
        if settings.ACCOUNT_EMAIL_CONFIRMATION_EMAIL and not email_address.verified:
            self.send_email_confirmation(email_address)
        if settings.ACCOUNT_EMAIL_CONFIRMATION_REQUIRED and not email_address.verified:
            return self.email_confirmation_required_response()
        else:
            self.form = form

            onesignal_player_id = self.request.POST.get('onesignal_player_id', default=None)
            if onesignal_player_id is not None:
                OneSignalPlayer.objects.get_or_create(user=self.created_user, player_id=onesignal_player_id)

            self.login_user()
            if "CSRF_COOKIE" not in self.request.META:
                csrf_token = ""
            else:
                csrf_token = self.request.META["CSRF_COOKIE"]
            response_data = {
                "status": "success",
                "csrf_token": csrf_token,
                "user_id": self.created_user.id,
                "user_name": self.created_user.username,
                "language": self.created_user.account.language
            }
        return HttpResponse(json.dumps(response_data), content_type='application/json')

    def after_signup(self, form):
        app_setting = AppSetting.objects.first()
        user_profile_form = UserProfileForm()

        if app_setting:
            user_profile_form.initial['belacoin'] = app_setting.start_balance

        up = user_profile_form.save(commit=False)

        up.user = self.created_user

        resp = requests.post('https://www.google.com/recaptcha/api/siteverify',
                             data={
                                 'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                                 'response': self.request.POST.get('g-recaptcha-response')
                             })
        if resp.status_code == requests.codes.ok:
            up.is_bot = not resp.json().get("success", False)

        user_ip = get_client_ip(self.request)[0]
        up.user_ip = user_ip
        up.save()

        # ===============Get Predict Language==================
        security_client = neutrino_api_client.security_and_networking

        ip_probe_response = security_client.ip_probe(ip=user_ip)
        country_code = ip_probe_response.country_code

        detected_language = get_native_language_via_country_code(country_code=country_code)
        self.created_user.account.language = detected_language
        self.created_user.account.save()
        # =====================================================

        if not up.is_bot:
            UserReferrer.objects.apply_referrer(self.created_user, self.request)

            # seed with starter balance
            up.user.seed_starter_balance()

        super(SignupView, self).after_signup(form)


@csrf_exempt
def reset_password(request,
                   email_template_name='registration/password_reset_email.html',
                   subject_template_name='registration/password_reset_subject.txt',
                   password_reset_form=PasswordResetForm,
                   token_generator=default_token_generator,
                   from_email=None,
                   html_email_template_name=None,
                   extra_email_context=None):
    if request.method == "POST":
        form = password_reset_form(request.POST)
        if form.is_valid():
            opts = {
                'use_https': request.is_secure(),
                'token_generator': token_generator,
                'from_email': from_email,
                'email_template_name': email_template_name,
                'subject_template_name': subject_template_name,
                'request': request,
                'html_email_template_name': html_email_template_name,
                'extra_email_context': extra_email_context,
            }
            form.save(**opts)
            success_message = "We've emailed you instructions for setting your password, if an account exists with the email you entered.\
                                You should receive them shortly.\
                                If you don't receive an email, please make sure you've entered the address you registered with, and check your spam folder."
            response_data = {"status": "success", "message": success_message}
            return HttpResponse(json.dumps(response_data), content_type='application/json')
        else:
            response_data = {"status": "error", "message": form.errors.as_text()}
            return HttpResponse(json.dumps(response_data), content_type='application/json')
    else:
        response_data = {"status": "error", "message": "Only Allow Post Request!"}
        return HttpResponse(json.dumps(response_data), content_type='application/json')


@csrf_exempt
def check_reset_password_link(request):
    response_data = {"status": "incorrect"}
    try:
        if request.method == 'POST':
            uidb64 = request.POST.get('uidb64', default=None)
            token = request.POST.get('token', default=None)
            if uidb64 is not None and token is not None:
                uid = force_text(urlsafe_base64_decode(uidb64))
                user = User.objects.get(pk=uid)
                if user is not None and default_token_generator.check_token(user, token):
                    response_data = {"status": "success"}
                return HttpResponse(json.dumps(response_data), content_type='application/json')
            else:
                return HttpResponse(json.dumps(response_data), content_type='application/json')
        else:
            return HttpResponse(json.dumps(response_data), content_type='application/json')
    except Exception as e:
        return HttpResponse(json.dumps(response_data), content_type='application/json')


@csrf_exempt
def reset_password_confirm(request):
    response_data = {"status": "incorrect"}
    try:
        if request.method == 'POST':
            uidb64 = request.POST.get('uidb64', default=None)
            token = request.POST.get('token', default=None)
            if uidb64 is not None and token is not None:
                uid = force_text(urlsafe_base64_decode(uidb64))
                user = User.objects.get(pk=uid)
                if user is not None and default_token_generator.check_token(user, token):
                    form = SetPasswordForm(user, request.POST)
                    if form.is_valid():
                        form.save()
                        response_data = {"status": "done"}
                return HttpResponse(json.dumps(response_data), content_type='application/json')
            else:
                return HttpResponse(json.dumps(response_data), content_type='application/json')
        else:
            return HttpResponse(json.dumps(response_data), content_type='application/json')
    except Exception as e:
        return HttpResponse(json.dumps(response_data), content_type='application/json')


@csrf_exempt
def logout_me(request):
    response_data = {"status": "error"}
    try:
        if request.method == 'POST':
            if request.user.is_authenticated():
                onesignal_player_id = request.POST.get('onesignal_player_id', default=None)

                if onesignal_player_id is not None:
                    OneSignalPlayer.objects.filter(user=request.user, player_id=onesignal_player_id).delete()

                logout(request)
                response_data = {"status": "success"}
            return HttpResponse(json.dumps(response_data), content_type='application/json')
        else:
            response_data = {"status": "error", "message": "Only Allow Post Request!"}
            return HttpResponse(json.dumps(response_data), content_type='application/json')
    except Exception as e:
        response_data.update({"message": e.message})
        return HttpResponse(json.dumps(response_data), content_type='application/json')


@csrf_exempt
def get_instagram_users(request):
    response_data = {"status": "error"}
    try:
        if request.method == 'POST' and get_client_ip(request)[0] == settings.INSTA_BELLA_SERVER:
            last_update_time = request.POST.get('last_update_time', default=None)
            if last_update_time is not None:
                data = InstagramUser.objects.filter(updated_at__gt=last_update_time).values("user_id", "username",
                                                                                            "status")
                response_data = {"status": "success", "result": list(data),
                                 "last_update_time": timezone.now().__str__()}
            else:
                data = InstagramUser.objects.values("user_id", "username", "status")
                response_data = {"status": "success", "result": list(data),
                                 "last_update_time": timezone.now().__str__()}

            return HttpResponse(json.dumps(response_data), content_type='application/json')
        else:
            response_data = {"status": "error", "message": "Only Allow Post Request!"}
            return HttpResponse(json.dumps(response_data), content_type='application/json')
    except Exception as e:
        response_data.update({"message": e.message})
        return HttpResponse(json.dumps(response_data), content_type='application/json')


@csrf_exempt
def add_instagram_posts(request):
    response_data = {"status": "error"}
    try:
        if request.method == 'POST' and get_client_ip(request)[0] == settings.INSTA_BELLA_SERVER:
            image_list = json.loads(request.body)['data']
            for image in image_list:
                users_queryset = User.objects.filter(pk=image['user_id'])
                if users_queryset.count() > 0:
                    current_user = users_queryset[0]
                    data = {
                        "bucket": settings.AWS_STORAGE_BUCKET_NAME,
                        "image_type": "third_party",
                        "key": 'avatars/%s/%s_%s' % (
                            image['user_id'], str(uuid.uuid4()), os.path.basename(image['image_url'])),
                        "image_url": image['image_url'],
                    }

                    # call lambda
                    result = requests.post(
                        'https://j91our9x8i.execute-api.us-east-1.amazonaws.com/dev/',
                        headers={'x-api-key': 've1nfBJ4xl2qhNDVmDN7B96EtGqnfBed9pzt8bc1'},
                        data=json.dumps(data))

                    if result.status_code == 200 and 'status' in result.json() and result.json()['status'] == 'success':
                        post = Bella.objects.create(
                            content='' if image['image_caption'] is None else image['image_caption'].encode('utf-8'),
                            image_path=data['key'], user=current_user, processed=True)
                        # add tag into the post
                        if image['image_caption'] is not None:
                            matches = re.findall(r'#\w+', image['image_caption'], re.UNICODE)
                            for tag in set(matches):
                                post.tags.add(tag[1:].lower())
                            post.save()

                        current_user.user1.number_of_posts = current_user.user1.number_of_posts + 1
                        current_user.user1.save(update_fields=['number_of_posts'])

                        if image['image_caption'] is not None:
                            matches = re.findall(r'@\w+', image['image_caption'], re.UNICODE)
                            for username in set(matches):
                                users = User.objects.filter(username=username[1:])
                                if users and users[0] != current_user:
                                    notify.send(current_user, recipient=users[0], verb=" tagged you in a post",
                                                action_object=post)

                                    # ========= Sending OneSignal Notification =======
                                    if users[0].onesignal_players.count() > 0:
                                        player_ids = list(
                                            users[0].onesignal_players.values_list('player_id', flat=True))
                                        content = "%s tagged you in a post" % current_user.username
                                        send_onesignal_push_notification(current_user, content, player_ids)

                                    # ================================================

                                    Channel("notify_user").send({
                                        'username': users[0].username
                                    })

                        # TODO use django activity stream
                        ActivityHistory.objects.create(user=current_user, activity_type=ActivityType.InstagramPost)

            response_data.update({"status": "success"})

            return HttpResponse(json.dumps(response_data), content_type='application/json')
        else:
            response_data = {"status": "error", "message": "Only Allow Post Request!"}
            return HttpResponse(json.dumps(response_data), content_type='application/json')
    except Exception as e:
        response_data.update({"message": e.message})
        return HttpResponse(json.dumps(response_data), content_type='application/json')


class SettingListView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, **kwargs):
        user = request.user
        response_data = {
            "username": user.username,
            "email": user.email,
            "language": user.account.language,
            "language_list": [{"value": value, "label": label} for (value, label) in settings.ACCOUNT_LANGUAGES],
            "send_weekly_email": user.user1.send_weekly_email,
            "verified": EmailAddress.objects.get_primary(user).verified
        }
        return HttpResponse(json.dumps(response_data), content_type='application/json')


class ChangePasswordView(account.views.ChangePasswordView):

    def dispatch(self, request, *args, **kwargs):
        return super(ChangePasswordView, self).dispatch(request, *args, **kwargs)

    def form_invalid(self, form):
        response_data = {"status": "error", "message": form.errors.as_text()}
        return HttpResponse(json.dumps(response_data), content_type='application/json')

    def form_valid(self, form):
        self.change_password(form)
        self.after_change_password()

        response_data = {
            "status": "success",
            "message": "Password successfully changed."
        }
        return HttpResponse(json.dumps(response_data), content_type='application/json')


class SettingsView(account.views.SettingsView):
    form_class = SettingsForm

    def get_initial(self):
        initial = super(SettingsView, self).get_initial()
        initial["username"] = self.request.user.username
        initial["send_weekly_email"] = self.request.user.user1.send_weekly_email
        return initial

    def get_context_data(self, **kwargs):
        ctx = super(SettingsView, self).get_context_data(**kwargs)
        ctx['primary_email'] = self.primary_email_address
        return ctx

    def update_account(self, form):
        fields = {}
        cleaned = form.cleaned_data

        if "timezone" in cleaned:
            fields["timezone"] = cleaned["timezone"]

        if "language" in cleaned:
            fields["language"] = cleaned["language"]

        if "username" in cleaned or "email" in cleaned:
            if "username" in cleaned:
                setattr(self.request.user, "username", cleaned["username"])

            if "email" in cleaned:
                setattr(self.request.user, "email", cleaned["email"])

            self.request.user.save()

        if "send_weekly_email" in cleaned:
            setattr(self.request.user.user1, "send_weekly_email", cleaned["send_weekly_email"])
            self.request.user.user1.save()

        if fields:
            account_ = self.request.user.account
            for k, v in fields.items():
                setattr(account_, k, v)

            account_.save()

    def dispatch(self, request, *args, **kwargs):
        return super(SettingsView, self).dispatch(request, *args, **kwargs)

    def form_invalid(self, form):
        response_data = {"status": "error", "message": form.errors.as_text()}
        return HttpResponse(json.dumps(response_data), content_type='application/json')

    def form_valid(self, form):
        self.update_settings(form)
        user = self.request.user
        response_data = {
            "status": "success",
            "message": "Account settings updated.",
            "username": user.username,
            "email": user.email,
            "language": user.account.language,
            "language_list": [{"value": value, "label": label} for (value, label) in settings.ACCOUNT_LANGUAGES],
            "send_weekly_email": user.user1.send_weekly_email,
            "verified": EmailAddress.objects.get_primary(user).verified
        }

        return HttpResponse(json.dumps(response_data), content_type='application/json')


@login_required
def resend_verification_email(request):
    user = request.user
    primary_email = EmailAddress.objects.get_primary(user)
    if not primary_email.verified:
        primary_email.send_confirmation()
        response_data = {
            "status": "success",
            "message": "Verification email has been sent. Please confirm it.",
        }
        return HttpResponse(json.dumps(response_data), content_type='application/json')
    else:
        response_data = {
            "status": "success",
            "message": "Your account is already verified.",
        }
        return HttpResponse(status=200)


class DeleteView(account.views.DeleteView):

    def dispatch(self, request, *args, **kwargs):
        return super(DeleteView, self).dispatch(request, *args, **kwargs)

    def get(self, *args, **kwargs):
        response_data = {"status": "error", "message": "Only Allow Post Request!"}
        return HttpResponse(json.dumps(response_data), content_type='application/json')

    def post(self, *args, **kwargs):
        AccountDeletion.mark(self.request.user)
        logout(self.request)
        response_data = {
            "status": "success",
            "message": "Your account is now inactive and your data will be expunged in the next {expunge_hours} hours.".format(
                **{
                    "expunge_hours": settings.ACCOUNT_DELETION_EXPUNGE_HOURS,
                })
        }

        return HttpResponse(json.dumps(response_data), content_type='application/json')


def send_onesignal_push_notification(user, content, player_ids):
    try:
        new_notification = onesignal_sdk.Notification(contents={"en": content})
        new_notification.set_parameter("large_icon", get_upload_url(user.user1.profile_pic))
        # set filters
        new_notification.set_target_devices(player_ids)

        # send notification, it will return a response
        onesignal_response = onesignal_client.send_notification(new_notification)
        print(onesignal_response.status_code)
        print(onesignal_response.json())

    except Exception as e:
        print(e)
