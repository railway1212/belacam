import threading

from django.core.urlresolvers import reverse
from django.template.loader import get_template
from django.conf import settings
from django.utils.html import strip_tags
from django.core.mail import EmailMultiAlternatives

from belacam.apps.core.utils import format_td


thread_locals = threading.local()


def get_current_request():
    return getattr(thread_locals, 'request', None)


def reset_current_request():
    setattr(thread_locals, 'request', None)


class EmailThread(threading.Thread):
    def __init__(self, subject, recipient_list, mail_dict):
        self.subject = subject
        self.recipient_list = recipient_list
        self.html_content = mail_dict.get('html_message')
        self.text_content = mail_dict.get('message')
        threading.Thread.__init__(self)

    def run(self):
        msg = EmailMultiAlternatives(
            self.subject,
            self.text_content,
            'Belacam <no-reply@belacam.com>',
            self.recipient_list
        )
        msg.attach_alternative(self.html_content, "text/html")
        msg.send()


def send_html_mail(subject, recipient_list, mail_dict):
    EmailThread(subject, recipient_list, mail_dict).start()


def compose_withdrawal_email(email_obj):
    http_request = get_current_request()
    template = get_template('mails/email_confirmation_message.html')
    key = email_obj.key
    withdraw_request = email_obj.request

    confirm_link = http_request.build_absolute_uri(reverse('withdrawal_confirm_email', args=[key]))

    html_str = template.render({
        'username': withdraw_request.author.username,
        'withdraw_amount': withdraw_request.amount,
        'withdraw_address': withdraw_request.address,
        'confirm_link': confirm_link
    })

    text_str = strip_tags(html_str)

    return {
        'message': text_str,
        'html_message': html_str
    }


def compose_ban_email(user):
    http_request = get_current_request()
    url = http_request.build_absolute_uri('/b/terms')
    template = get_template('mails/ban_account.html')
    html_str = template.render({
        'username': user.username,
        'term_page_url': url,
    })

    text_str = strip_tags(html_str)

    return {
        'message': text_str,
        'html_message': html_str
    }


def compose_suspend_email(user, time):
    http_request = get_current_request()
    url = http_request.build_absolute_uri('/b/terms')
    template = get_template('mails/suspend_account.html')
    time_str = format_td(time)

    html_str = template.render({
        'username': user.username,
        'time': time_str,
        'term_page_url': url
    })

    text_str = strip_tags(html_str)

    return {
        'message': text_str,
        'html_message': html_str
    }


def compose_weekly_earning_email(context):
    http_request = get_current_request()
    # TODO: replace with url to account setting page.
    if http_request is not None:
        url = http_request.build_absolute_uri('/settings')
    else:
        prefix = settings.SERVER_URL_PREFIX
        url = prefix + '/settings'
        print url

    context['account_setting_url'] = url

    template = get_template('mails/weekly_earning.html')
    html_str = template.render(context)

    text_str = strip_tags(html_str)

    return {
        'message': text_str,
        'html_message': html_str
    }


def compose_total_earning_email(context):
    http_request = get_current_request()
    # TODO: replace with url to account setting page.
    if http_request is not None:
        url = http_request.build_absolute_uri('/settings')
    else:
        prefix = settings.SERVER_URL_PREFIX
        url = prefix + '/settings'
        print url

    context['account_setting_url'] = url

    template = get_template('mails/total_earning.html')
    html_str = template.render(context)

    text_str = strip_tags(html_str)

    return {
        'message': text_str,
        'html_message': html_str
    }
