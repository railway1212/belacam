from django.conf.urls import url
from belacam.apps.referral import urls as referral_urls
from belacam.apps.reward import urls as reward_task_urls
from . import views

urlpatterns = [
    url(r'^users/(?P<username>\w{0,30})/$', views.users, name='upload_post'),
    url(r'^ajaxFollow/', views.ajax_follow, name='ajaxFollow'),
    url(r'^ajaxUnfollow/', views.ajaxUnfollow, name='ajaxUnfollow'),
    url(r'^like/$', views.like, name='like'),
    url(r'^report/$', views.report, name='report'),
    url(r'^delete/$', views.delete, name='delete'),
    url(r'^adminDelete/$', views.adminDelete, name='admin_delete'),
    url(r'^block/$', views.block, name='block'),
    url(r'^withdrawal/confirm/(?P<key>\w+)/$', views.withdraw_confirm, name="withdrawal_confirm_email"),

    url(r'^notifications/live/$', views.live_notification_list, name='live_notifications_list'),
    url(r'^notifications/$', views.NotificationListView.as_view(), name='all_notifications_list'),
    url(r'^mark_it_read/(?P<not_id>\d+)/$', views.mark_it_as_read, name='mark_it_read'),
    url(r'^mark_all_read$', views.mark_all_as_read, name='mark_all_read'),

    url(r'^account/cover_pic/$', views.UpdateCoverPicView.as_view(), name='update_cover_pic'),
    url(r'^account/profile_pic/$', views.UploadProfilePicView.as_view(), name='upload_profile_pic'),
    url(r'^account/verify_phone/$', views.VerifyPhoneView.as_view(), name='verify_phone'),
    url(r'^account/verify_phone_confirm/$', views.VerifyPhoneConfirmView.as_view(), name='verify_phone_confirm'),

    url(r'^self/(?P<username>\w{0,30})/$', views.SelfListView.as_view(), name='self_api_view'),
    url(r'^user/(?P<pk>\d+)/profile/$', views.UserProfileUpdateView.as_view(), name='user_profile_api_view'),
    url(r'^user/(?P<username>\w{0,30})/$', views.UserRetrieveView.as_view(), name='user_api_view'),
    url(r'^followers/(?P<username>\w{1,30})/$', views.FollowerListView.as_view(), name='follower_api_view'),
    url(r'^followings/(?P<username>\w{1,30})/$', views.FollowingListView.as_view(), name='following_api_view'),
    url(r'^homefeeds/$', views.HomeFeedListView.as_view(), name='home_feed'),
    url(r'^discover/$', views.DiscoverAPIView.as_view(), name='discover_api'),
    url(r'^feed/list/$', views.ThoughtListView.as_view(), name='feed_list_api'),
    url(r'^people/$', views.PeopleListView.as_view(), name='people_api'),
    url(r'^tags/$', views.TagListView.as_view(), name='tag_api'),

    url(r'^tagfeeds/(?P<tagname>.{0,300})/$', views.FeedOfTagsListView.as_view(),
        name='feed_tags_api_view'),
    url(r'^tagposts/(?P<tagname>.{0,300})/$', views.PostOfTagsListView.as_view(),
        name='post_tags_api_view'),

    url(r'^likes/(?P<pk>\d+)/$', views.LikeListView.as_view(), name='like_api_view'),

    url(r'^searchtop/$', views.SearchTop.as_view(), name='search_top'),
    url(r'^search/(?P<query>\w{0,300})/$', views.SearchAPI.as_view(), name='search_api'),
    url(r'^post/(?P<post_id>\d+)/comment/$', views.CommentCreateAPIView.as_view(), name='add_comment'),
    url(r'^places/', views.search_place, name='search_place'),
    url(r'^comment/(?P<pk>\d+)/', views.CommentUpdateDestroyAPIView.as_view(), name='update_destroy_comment'),
    url(r'^update_caption/$', views.update_caption, name='update_caption'),
    url(r'^update_location/$', views.update_location, name='update_location'),
    url(r'^post/(?P<post_id>\d+)/$', views.SinglePostListView.as_view(), name='single_post_list_view'),
    url(r'^withdraw/$', views.WithdrawRequestView.as_view(), name='withdraw'),
    url(r'^withdraw/settings/fee/$', views.WithdrawFeeSettingsView.as_view(), name="withdrawal_fee_settings"),
    url(r'^growth/$', views.growth_statistics, name='growth_statistics'),
    url(r'^increase_deposit$', views.increase_deposit, name='increase_deposit'),
    url(r'^get_s3_post$', views.UploadSignS3ViewSet.as_view(), name='upload_sign_s3'),
    url(r'^request_deposit_address/$', views.assign_deposit_address, name='request_deposit_address'),

    url(r'^sign-in/$', views.sign_in, name='sign_in_api'),
    url(r'^sign-up/$', views.SignupView.as_view(), name='sign_up_api'),
    url(r'^reset-password$', views.reset_password, name='reset_password_api'),
    url(r'^check-reset-password-link$', views.check_reset_password_link, name='check_reset_password_link_api'),
    url(r'^reset-password-confirm$', views.reset_password_confirm, name='reset_password_confirm_api'),
    url(r'^logout/$', views.logout_me, name='logout_api'),
    url(r'^setting_details/$', views.SettingListView.as_view(), name='setting_details_api'),
    url(r'^settings/update_account$', views.SettingsView.as_view(), name='update_account_api'),
    url(r'^settings/update_password$', views.ChangePasswordView.as_view(), name='update_password_api'),
    url(r"^settings/resend_email$", views.resend_verification_email, name="resend_email_api"),
    url(r"^settings/deleteAccount$", views.DeleteView.as_view(), name="delete_account_api"),

    url(r'^get_instagram_users$', views.get_instagram_users, name='get_instagram_users_api'),
    url(r'^add_instagram_posts', views.add_instagram_posts, name='add_instagram_posts_api'),
]

urlpatterns = urlpatterns + referral_urls.urlpatterns
urlpatterns = urlpatterns + reward_task_urls.urlpatterns
