from django import template
from friendship.models import Follow
from django.conf import settings
from decimal import Decimal, ROUND_UP

BELA_PER_LIKE = PRIVATE_DIR = getattr(settings, "BELA_PER_LIKE", None)

register = template.Library()


#Amount a particular post has earned
@register.simple_tag(takes_context=True)
def Earnings_Per_Post(context, number):
    amount = Decimal(number*BELA_PER_LIKE).quantize(Decimal('.01'), rounding=ROUND_UP)
    return amount


