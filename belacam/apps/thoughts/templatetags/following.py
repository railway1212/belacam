from django import template
from friendship.models import Follow

register = template.Library()

@register.simple_tag(takes_context=True)
def isfollowing(context, who_to_follow):
    u = context['request'].user
    follow = "Unfollow"
    try:
        fg = (Follow.objects.following(u))
        follow = "Follow"
        if who_to_follow in fg:
            follow = "Unfollow"
    except ObjectDoesNotExist:
        follow = "Unfollow"
    return follow
