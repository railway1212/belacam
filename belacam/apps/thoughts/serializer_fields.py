from django.contrib.gis.geos import Point
from rest_framework import serializers


class PointFieldSerializer(serializers.Field):
    """
    Color objects are serialized into 'rgb(#, #, #)' notation.
    """
    def to_representation(self, obj):
        return {
            'longitude': obj.x,
            'latitude': obj.y
        }

    def to_internal_value(self, data):
        return Point(data.longitude, data.latitude, srid=4326)
