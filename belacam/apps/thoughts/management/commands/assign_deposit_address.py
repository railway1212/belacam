from django.core.management.base import BaseCommand
from belacam.apps.thoughts.models import *
import requests
import hashlib
import hmac

class Command(BaseCommand):
    """
    Dict file creation
    """
    help = 'Assign deposit address to all users'

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)

    def handle(self, *args, **options):
        if settings.PRODUCTION:
            for up in UserProfile.objects.exclude(belacoin_address__startswith="0xbe1a"):
                try:
                    resp = requests.post("http://%s/api/vanity_address" % (settings.DEPOSIT_SERVER),
                                         data={
                                             'uid': up.user.id,
                                             'uname': up.user.username,
                                             'token': hmac.new(settings.SECRET_SEND_PASSWORD,
                                                               "%s, %s" % (str(up.user.id), str(up.user.username)),
                                                               hashlib.sha256).hexdigest(),
                                         })
                    if resp.status_code == requests.codes.ok and resp.json().get("status", "failed") == "success":
                        up.belacoin_address = resp.json().get("address", "No Address, Contact Support")
                    up.save()
                except:
                    pass
