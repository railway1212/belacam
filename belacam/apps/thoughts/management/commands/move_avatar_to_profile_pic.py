from django.core.management.base import BaseCommand
from belacam.apps.thoughts.models import *

from django.db.models import *


class Command(BaseCommand):
    """
    Dict file creation
    """
    help = "Move user's avatar pic field to profile_pic field"

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)

    def handle(self, *args, **options):
        for up in UserProfile.objects.annotate(
            avatar_count=Count("user__avatar"),
            avatar_path=F("user__avatar__avatar")
        ).filter(
            avatar_count__gt=0
        ):
            up.profile_pic = up.avatar_path[1:]
            up.save(update_fields=['profile_pic'])


