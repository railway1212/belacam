import csv
from django.core.management.base import BaseCommand
from belacam.apps.thoughts.models import GeoPlace
from django.contrib.gis.geos import Point


class Command(BaseCommand):
    help = 'Seed geoplaces data from large text file'

    def add_arguments(self, parser):
        parser.add_argument('filename')

    def handle(self, *args, **options):
        filename = options['filename']

        with open(filename, "rbU") as csvfile:
            datareader = csv.reader(csvfile, quoting=csv.QUOTE_NONE, dialect=csv.excel_tab)

            for row in datareader:
                record = GeoPlace()
                record.id = row[0]
                record.name = row[1]
                record.asciiname = row[2]
                record.alternatenames = row[3]
                record.location = Point(float(row[4]), float(row[5]))
                record.feature_class = row[6]
                record.feature_code = row[7]
                record.country_code = row[8]
                record.cc2 = row[9]
                record.admin1_code = row[10]
                record.admin2_code = row[11]
                record.admin3_code = row[12]
                record.admin4_code = row[13]
                record.timezone = row[14]
                record.country_name = row[15]
                record.admin2_name = row[16]
                record.admin2_asciiname = row[17]
                record.admin1_name = row[18]
                record.admin1_asciiname = row[19]
                record.save()
