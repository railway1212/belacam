from django.core.management.base import BaseCommand
from account.models import EmailAddress


class Command(BaseCommand):
    """
    Dict file creation
    """
    help = 'Make all the emailaddresses comfirmed'

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)

    def handle(self, *args, **options):
        EmailAddress.objects.filter(verified=False).update(verified=True)

