import requests
from django.core.management.base import BaseCommand
from belacam.apps.thoughts.models import PriceHistory, AppSetting


class Command(BaseCommand):
    """
    Dict file creation
    """
    help = 'To save price after fetching them from market website'

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)

    def handle(self, *args, **options):
        resp = requests.get('https://api.coinmarketcap.com/v1/ticker/belacoin/')
        if resp.status_code == requests.codes.ok:
            resp_json = resp.json()
            if len(resp_json) > 0:
                price_in_dollors = float(resp_json[0].get('price_usd', 0))
                PriceHistory.objects.create(price=price_in_dollors)
                app_setting = AppSetting.objects.first()
                app_setting.bella_average_price = PriceHistory().get_average_price()
                app_setting.save(update_fields=['bella_average_price'])
