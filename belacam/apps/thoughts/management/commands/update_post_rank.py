from django.core.management.base import BaseCommand
from belacam.apps.thoughts.models import *
from django.db.models import Count


class Command(BaseCommand):
    """
    Dict file creation
    """
    help = 'Recalculate rank number for each post'

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)

    def handle(self, *args, **options):
        coefficient_hours_ago = AppSetting.objects.first().coefficient_hours_ago
        coefficient_likes_for_post = AppSetting.objects.first().coefficient_likes_for_post
        coefficient_likes_given_by_poster = AppSetting.objects.first().coefficient_likes_given_by_poster

        for post in Bella.objects.select_related('user__user1').filter(created_at__lte=timezone.now()).order_by('-created_at').annotate(total_likes_count=Count("likes")):
            post.rank_number = (
                    pow((timezone.now() - post.created_at).total_seconds() / 3600, coefficient_hours_ago)
                    + post.total_likes_count * coefficient_likes_for_post
                    + post.user.user1.likes_count * coefficient_likes_given_by_poster
            )

            post.save(update_fields=['rank_number'])



