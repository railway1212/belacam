from django.core.management.base import BaseCommand
from belacam.apps.thoughts.tasks import process_image_with_lambda
from belacam.apps.thoughts.models import Bella


class Command(BaseCommand):
    """
    Dict file creation
    """
    help = 'Get unprocessed images and try processing again'

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)

    def handle(self, *args, **options):
        for obj in Bella.objects.filter(processed=False):
            process_image_with_lambda(obj.image_path, obj.id)
