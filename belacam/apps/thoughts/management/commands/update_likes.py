from django.core.management.base import BaseCommand
from belacam.apps.thoughts.models import *
from datetime import datetime, timedelta

from django.db.models import Count


class Command(BaseCommand):
    """
    Dict file creation
    """
    help = 'Update likes count and time that user get in the past 24hrs'

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)

    def handle(self, *args, **options):

        current_time = datetime.now()
        UserProfile.objects.update(likes_count=0, like_update_time=current_time)
        for up in UserProfile.objects.filter(
            user__recepient__created_at__gt=datetime.now()-timedelta(days=1),
            user__recepient__activity_type=ActivityType.Like
        ).annotate(like_count=Count('user__recepient__id')):
            up.likes_count = up.like_count
            up.like_update_time = current_time
            up.save(update_fields=['likes_count', 'like_update_time',])

