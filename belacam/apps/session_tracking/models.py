from .compat import User

from django.db import models


class Track(models.Model):
    user = models.ForeignKey(User, related_name='sessions')
    start_at = models.DateTimeField()
    end_at = models.DateTimeField()
