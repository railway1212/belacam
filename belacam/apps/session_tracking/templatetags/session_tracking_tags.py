from django import template
from django.conf import settings

register = template.Library()


@register.filter
def expire_after(request):
    return settings.EXPIRE_AFTER


@register.filter
def warn_after(request):
    return settings.WARN_AFTER
