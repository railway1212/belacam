// Use 'yourlabs' as namespace.
if (window.yourlabs == undefined) window.yourlabs = {};

// Session tracking constructor. These are the required options:
//
// - pingUrl: url to ping with last activity in this tab to get global last
//   activity time,
// - warnAfter: number of seconds of inactivity before warning,
// - expireAfter: number of seconds of inactivity before expiring the session.
//
// Optional options:
//
// - confirmFormDiscard: message that will be shown when the user tries to
//   leave a page with unsaved form data. Setting this will enable an
//   onbeforeunload handler that doesn't block expire().
// - events: a list of event types to watch for activity updates.
// - returnToUrl: a url to redirect users to expired sessions to. If this is not defined we just reload the page
yourlabs.SessionTracking = function(options) {
    // **HTML element** that should show to warn the user that his session will
    // expire.

    // Last recorded activity datetime.
    this.lastActivity = new Date();

    // Events that would trigger an activity
    this.events = ['mousemove', 'scroll', 'keyup', 'click', 'touchstart', 'touchend', 'touchmove'];

    // Merge the options dict here.
    $.extend(this, options);

    // Bind activity events to update this.lastActivity.
    var $document = $(document);
    for(var i=0; i<this.events.length; i++) {
        if ($document[this.events[i]]) {
            $document[this.events[i]]($.proxy(this.activity, this));
        }
    }

    // Initialize timers.
    this.apply()
};

yourlabs.SessionTracking.prototype = {
    // Called when there has been no activity for more than expireAfter
    // seconds.

    warn: function() {
        this.warning = true;
    },

    stopWarn: function() {
        this.warning = false;
    },

    logout: function () {
        this.loggedout = true;
    },
    // Called by click, scroll, mousemove, keyup, touchstart, touchend, touchmove
    activity: function() {
        var now = new Date();
        if (this.loggedout) {
            return;
        }

        if (now - this.lastActivity < 1000)
            // Throttle these checks to once per second
            return;

        this.lastActivity = now;

        if (this.warning) {
            // Inform the server that the user came back manually, this should
            // block other browser tabs from expiring.
            this.ping();
        }
        console.log('new activity!');
        this.stopWarn();

    },

    // Hit the PingView with the number of seconds since last activity.
    ping: function() {
        var idleFor = Math.floor((new Date() - this.lastActivity) / 1000);

        $.ajax(this.pingUrl, {
            data: {idleFor: idleFor},
            cache: false,
            success: $.proxy(this.pong, this),
            // In case of network error, we still want to hide potentially
            // confidential data !!
            error: $.proxy(this.apply, this),
            dataType: 'json',
            type: 'get'
        });
    },

    // Callback to process PingView response.
    pong: function(data) {
        if (data === 'logout') return this.logout();

        this.lastActivity = new Date();
        this.lastActivity.setSeconds(this.lastActivity.getSeconds() - data);
        this.apply();
    },

    // Apply expiry, setup next ping
    apply: function() {
        // Cancel timeout if any, since we're going to make our own
        clearTimeout(this.timeout);
        console.log(this.lastActivity);
        console.log(this.warning);
        var idleFor = Math.floor((new Date() - this.lastActivity) / 1000);
        var nextPing;

        if (idleFor >= this.expireAfter) {
            this.warn();
            console.log('no timeout');
            return;
        } else if(idleFor >= this.warnAfter){
            this.warn();
            nextPing = this.expireAfter - idleFor;
        } else {
            this.stopWarn();
            nextPing = this.warnAfter - idleFor;
        }

        // setTimeout expects the timeout value not to exceed
        // a 32-bit unsigned int, so cap the value
        var milliseconds = Math.min(nextPing * 1000, 2147483647)
        this.timeout = setTimeout($.proxy(this.ping, this), milliseconds);
    }
};
