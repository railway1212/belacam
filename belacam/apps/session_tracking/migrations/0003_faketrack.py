# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('session_tracking', '0002_auto_20180820_0209'),
    ]

    operations = [
        migrations.CreateModel(
            name='FakeTrack',
            fields=[
            ],
            options={
                'verbose_name': 'Session Track',
                'proxy': True,
                'verbose_name_plural': 'Session Tracks',
            },
            bases=('session_tracking.track',),
        ),
    ]
