import datetime

from django.contrib import admin
from django.contrib.admin.views.main import ChangeList
from django.db import models
from django.db.models import Min

from belacam.apps.session_tracking.models import Track


def format_td(td_object):
    seconds = int(td_object.total_seconds())
    if seconds < 1:
        seconds = 1

    periods = [
        ('', 60 * 60),
        ('', 60),
        ('', 1)
    ]

    strings = []
    for period_name, period_seconds in periods:
        period_value, seconds = divmod(seconds, period_seconds)
        strings.append("%02d" % period_value)

    return ":".join(strings)


class FakeTrackManager(models.Manager):
    def get_queryset(self):
        return FakeTrackQueryset(self.model, using=self._db)


class FakeTrack(Track):
    objects = FakeTrackManager()

    def __init__(self, *args, **kwargs):
        super(FakeTrack, self).__init__(*args, **kwargs)
        self._date = kwargs.get('date', None)
        self._durations = kwargs.get('durations', None)
        self._freq = kwargs.get('freq', None)

    @property
    def durations(self):
        return self._durations

    @durations.setter
    def durations(self, value):
        self._durations = value

    @property
    def freq(self):
        return self._freq

    @freq.setter
    def freq(self, value):
        self._freq = value

    @property
    def date(self):
        return self._date

    @date.setter
    def date(self, value):
        self._date = value

    class Meta:
        verbose_name = "Session Track"
        verbose_name_plural = 'Session Tracks'  # This is the name used in the link text
        proxy = True


class UnorderedChangeList(ChangeList):
    list_filter = ('user', )

    def get_ordering(self, request, queryset):
        return ()


class FakeTrackQueryset(models.query.QuerySet):
    def _fetch_all(self):
        if self._result_cache is None:
            self._result_cache = list(self.iterator())

            max_date = self._get_max_date()
            from_date = self._get_min_date()

            # Generate date list
            date_dict = {}
            cur_date = max_date
            while cur_date >= from_date:
                date_dict[str(cur_date)] = {
                    'date': cur_date,
                    'durations': [],
                    'freq': 0
                }
                cur_date -= datetime.timedelta(days=1)

            for obj in self._result_cache:
                duration = format_td(obj.end_at - obj.start_at)
                start_date = obj.start_at.date()
                date_dict[str(start_date)]['durations'].append(duration)

            result = []

            for date, value in sorted(date_dict.iteritems(), reverse=True):
                value['freq'] = len(value['durations'])
                value['durations'] = ','.join(value['durations'])

                result.append(FakeTrack(freq=value['freq'], durations=value['durations'], date=value['date']))
            self._result_cache = result

        if self._prefetch_related_lookups and not self._prefetch_done:
            self._prefetch_related_objects()

    def _get_max_date(self):
        return datetime.datetime.now().date() + datetime.timedelta(days=1)

    def _get_min_date(self):
        result = self.aggregate(start_at_min=Min('start_at'))['start_at_min']
        if result:
            return result.date()
        else:
            return result

    def count(self):
        """
        Performs a SELECT COUNT() and returns the number of records as an
        integer.

        If the QuerySet is already fully cached this simply returns the length
        of the cached results set to avoid multiple SELECT COUNT(*) calls.
        """
        if self._result_cache is not None:
            return len(self._result_cache)

        max_date = self._get_max_date()
        min_date = self._get_min_date()

        return 0 if not min_date else (max_date - min_date).days


class CustomTrackAdmin(admin.ModelAdmin):
    actions = None
    list_display_links = None
    enable_change_view = False
    # paginator = CustomPaginator
    change_list_template = 'admin/session_tracking/session_list.html'

    def has_add_permission(self, request):
        return False

    def has_delete_permission(*args, **kwargs):
        return False

    def get_queryset(self, request):
        qs = FakeTrack.objects.all()
        ordering = self.get_ordering(request)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs

    def get_changelist(self, request, **kwargs):
        return UnorderedChangeList

    def changelist_view(self, request, extra_context=None):
        response = super(CustomTrackAdmin, self).changelist_view(request, extra_context=extra_context)
        changelist = response.context_data['cl']
        response.context_data.update({
            'num_total_sessions': changelist.queryset.count()
        })
        return response

    list_display = ('date', 'freq', 'durations',)
    list_filter = ('user', )


admin.site.register([FakeTrack], CustomTrackAdmin)
