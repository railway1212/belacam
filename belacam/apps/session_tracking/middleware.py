"""
SessionTrackingMiddleware is the heart of the tracking that this application
attemps to provide.

To install this middleware, add to your ``settings.MIDDLEWARE_CLASSES``::

    'session_tracking.middleware.SessionTrackingMiddleware'

Make sure that it is placed **after** authentication middlewares.
"""

from datetime import datetime, timedelta

import django
from django.conf import settings

from belacam.apps.session_tracking.models import Track

try:  # Django 2.0
    from django.urls import reverse, resolve, Resolver404
except:  # Django < 2.0
    from django.core.urlresolvers import reverse, resolve, Resolver404

try:
    from django.utils.deprecation import MiddlewareMixin
except ImportError:  # Django < 1.10
    # Works perfectly for everyone using MIDDLEWARE_CLASSES
    MiddlewareMixin = object

from .utils import (
    get_current_track, set_current_track,
    get_last_activity, set_last_activity, check_last_activity_exist,
    array_intersect
)


class SessionTrackingMiddleware(MiddlewareMixin):
    """
    In charge of maintaining the real 'last activity' time, and log out the
    user if appropriate.
    """

    def is_passive_request(self, request):
        """ Should we skip activity update on this URL/View. """
        if request.path in settings.PASSIVE_URLS:
            return True

        try:
            match = resolve(request.path)

            # TODO: check namespaces too
            if match.url_name in settings.PASSIVE_URL_NAMES:
                return True

            if len(array_intersect(match.namespaces, settings.PASSIVE_URL_NAMESPACES)) > 0:
                return True
        except Resolver404:
            pass

        return False

    def get_expire_seconds(self, request):
        """Return time (in seconds) before the user should be logged out."""
        return settings.EXPIRE_AFTER

    def process_request(self, request):
        """ Update last activity time or logout. """

        if django.VERSION < (1, 10):
            is_authenticated = request.user.is_authenticated()
        else:
            is_authenticated = request.user.is_authenticated

        if not is_authenticated:
            return

        now = datetime.now()

        if self.is_passive_request(request):
            return

        if not check_last_activity_exist(request.session):
            track = Track(user=request.user, start_at=now, end_at=now)
            track.save()

            set_last_activity(request.session, now)
            set_current_track(request.session, track)
            return

        was_expired = self.activity_expired(request, now, get_last_activity(request.session))

        if request.path == reverse('session_tracking_ping') and 'idleFor' in request.GET:
            self.update_last_activity(request, now)
        else:
            set_last_activity(request.session, now)

        self.update_track(request, now, was_expired)

    def activity_expired(self, request, now, activity):
        expire_seconds = self.get_expire_seconds(request)
        delta = now - activity
        return delta >= timedelta(seconds=expire_seconds)

    def update_track(self, request, now, already_expired):
        update_activity = get_last_activity(request.session)
        update_expired = self.activity_expired(request, now, update_activity)

        if not already_expired or update_expired:
            #  If the last activity wasn't expired or last activity and update activity are both expired,
            #  extend last track.

            track = get_current_track(request.session)
            track.end_at = update_activity
        else:
            track = Track(user=request.user, start_at=update_activity, end_at=update_activity)
        track.save()
        set_current_track(request.session, track)

    def update_last_activity(self, request, now):
        """
        If ``request.GET['idleFor']`` is set, check if it refers to a more
        recent activity than ``request.session['_session_tracking']`` and
        update it in this case.
        """
        last_activity = get_last_activity(request.session)
        server_idle_for = (now - last_activity).seconds

        # Gracefully ignore non-integer values
        try:
            client_idle_for = int(request.GET['idleFor'])
        except ValueError:
            return

        # Disallow negative values, causes problems with delta calculation
        if client_idle_for < 0:
            client_idle_for = 0

        if client_idle_for < server_idle_for:
            # Client has more recent activity than we have in the session
            last_activity = now - timedelta(seconds=client_idle_for)

        # Update the session
        set_last_activity(request.session, last_activity)
