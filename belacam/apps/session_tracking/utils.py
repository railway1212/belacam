""" Helpers to support json encoding of session data """

from datetime import datetime

from belacam.apps.session_tracking.models import Track


def get_current_track(session):
    if '_session_tracking_current' in session:
        try:
            return Track.objects.get(pk=session['_session_tracking_current'])
        except Track.DoesNotExist:
            return None
    else:
        return None


def set_current_track(session, track):
    session['_session_tracking_current'] = track.id


def set_last_activity(session, dt):
    """ Set the last activity datetime as a string in the session. """
    session['_session_tracking_last'] = dt.strftime('%Y-%m-%dT%H:%M:%S.%f')


def get_last_activity(session):
    """
    Get the last activity datetime string from the session and return the
    python datetime object.
    """
    try:
        return datetime.strptime(session['_session_tracking_last'],
                '%Y-%m-%dT%H:%M:%S.%f')
    except AttributeError:
        #################################################################
        # * this is an odd bug in python
        # bug report: http://bugs.python.org/issue7980
        # bug explained here:
        # http://code-trick.com/python-bug-attribute-error-_strptime/
        # * sometimes, in multithreaded enviroments, we get AttributeError
        #     in this case, we just return datetime.now(),
        #     so that we are not logged out
        #   "./session_tracking/middleware.py", in update_last_activity
        #     last_activity = get_last_activity(request.session)
        #   "./session_tracking/utils.py", in get_last_activity
        #     '%Y-%m-%dT%H:%M:%S.%f')
        #   AttributeError: _strptime
        #
        #################################################################

        return datetime.now()
    except TypeError:
        return datetime.now()


def check_last_activity_exist(session):
    return '_session_tracking_last' in session


def array_intersect(a, b):
    """ return the intersection of two lists """
    return list(set(a) & set(b))
