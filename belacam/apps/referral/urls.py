from django.conf.urls import url

from belacam.apps.referral import views

urlpatterns = [
    url(r'referrer/$', views.ReferrerView.as_view(), name='get_referrer'),
    url(r'referrer/application/$', views.ReferrerApplicationView.as_view(), name='referrer_application'),
    url(r'referrer/statistics/$', views.ReferrerStatisticView.as_view(), name='get_referrer_statistics'),
    url(r'referral/settings/$', views.ReferrerSettingsView.as_view(), name='get_referral_settings'),
]
