# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2018-08-15 17:24
from __future__ import unicode_literals

import datetime

from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('referral', '0011_userreferrer_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='referrerapplication',
            name='status',
            field=models.CharField(choices=[(b'APPROVED', b'Approved'), (b'DENIED', b'Denied'), (b'PENDING', b'Pending')], db_index=True, default=b'PENDING', max_length=10),
        ),
        migrations.AlterField(
            model_name='userreferrer',
            name='status',
            field=models.CharField(choices=[(b'PENDING', b'Pending'), (b'VERIFIED', b'Verified')], db_index=True, default=b'PENDING', max_length=10),
        ),
        migrations.AddField(
            model_name='userreferrer',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True,
                                       default=datetime.datetime(2018, 8, 22, 14, 4, 27, 117934, tzinfo=utc),
                                       verbose_name='Creation date'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userreferrer',
            name='updated_at',
            field=models.DateTimeField(auto_now=True,
                                       default=datetime.datetime(2018, 8, 22, 14, 4, 32, 211292, tzinfo=utc),
                                       verbose_name='Update date'),
            preserve_default=False,
        ),
    ]
