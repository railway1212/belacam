from importlib import import_module

from django.apps import AppConfig as BaseAppConfig


class AppConfig(BaseAppConfig):

    name = "belacam.apps.referral"

    def ready(self):
        import_module("belacam.apps.referral.receivers")
