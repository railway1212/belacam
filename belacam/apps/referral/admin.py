from django.contrib import admin
from django.db import models

from belacam.apps.referral.choices import ReferrerApplicationStatus
from belacam.apps.thoughts.models import BellaView
from .models import Campaign, Referrer, UserReferrer, ReferralPayoutSetting, ReferrerApplication

from .admin_forms import *

class ReferrerInline(admin.TabularInline):
    model = Referrer
    extra = 0


class CampaignAdmin(admin.ModelAdmin):
    form = CampaignForm
    list_display = ('name', 'count_users')
    inlines = (ReferrerInline,)


class ReferrerAdmin(admin.ModelAdmin):
    form = ReferrerForm
    list_display = ('name', 'campaign', 'created_at', 'number_of_users')
    ordering = ['-created_at']

    def get_queryset(self, request):
        qs = super(ReferrerAdmin, self).get_queryset(request)
        qs = qs.annotate(num_users=models.Count('users'))
        return qs

    def number_of_users(self, obj):
        return obj.num_users

    number_of_users.admin_order_field = 'num_users'


class UserReferrerAdmin(admin.ModelAdmin):
    form = UserReferrerForm
    def ip_address(self, instance):
        return instance.user.user1.user_ip

    def joined_at(self, instance):
        return instance.user.date_joined

    def number_of_posts(self, instance):
        return instance.user.user1.thought_count

    def lifetime_earnings(self, instance):
        return instance.user.user1.earnings

    def has_viewed_other_photos(self, instance):
        return BellaView.objects.filter(user=instance.user).exists()

    def get_queryset(self, request):
        qs = super(UserReferrerAdmin, self).get_queryset(request)
        qs = qs.select_related('user').select_related('user__user1')
        return qs

    ip_address.admin_order_field = 'user__user1__user_ip'
    joined_at.admin_order_field = 'user__date_joined'

    has_viewed_other_photos.short_description = "Has viewed other photos?"
    has_viewed_other_photos.boolean = True
    list_display = ('user', 'referrer', 'ip_address', 'joined_at',
                    'number_of_posts', 'lifetime_earnings', 'has_viewed_other_photos', 'status',)


class ReferralPayoutSettingAdmin(admin.ModelAdmin):
    list_display = ('range_from', 'range_to', 'payout')
    model = ReferralPayoutSetting


class ReferrerApplicationAdmin(admin.ModelAdmin):
    form = ReferrerApplicationForm
    def approve_selected(self, request, queryset):
        queryset.update(status=ReferrerApplicationStatus.APPROVED.name)
        for application in queryset:
            user = application.user
            referrer = Referrer.objects.filter(created_by=user).first()
            if referrer is None:
                Referrer.objects.create(name=user.username, created_by=user, application=application)

    def deny_selected(self, request, queryset):
        queryset.update(status=ReferrerApplicationStatus.DENIED.name)

    def make_pending_selected(self, request, queryset):
        queryset.update(status=ReferrerApplicationStatus.PENDING.name)

    def email_address(self, instance):
        return instance.user.email

    def number_of_posts(self, instance):
        return instance.user.user1.number_of_posts

    list_display = ('user', 'created_at', 'content', 'status')

    fieldsets = (
        (None, {'fields': ('user', 'status', 'content')}),
        ('User info', {'fields': ('email_address', 'number_of_posts')})
    )

    readonly_fields = ('email_address', 'number_of_posts')

    search_fields = ('user__username',)

    approve_selected.short_description = 'Approve selected Ref Link Applications'
    deny_selected.short_description = 'Deny selected Ref Link Applications'
    make_pending_selected.short_description = 'Make pending selected Ref Link Applications'
    actions = [
        approve_selected,
        deny_selected,
        make_pending_selected
    ]


admin.site.register(ReferrerApplication, ReferrerApplicationAdmin)
admin.site.register(Campaign, CampaignAdmin)
admin.site.register(Referrer, ReferrerAdmin)
admin.site.register(UserReferrer, UserReferrerAdmin)
admin.site.register(ReferralPayoutSetting, ReferralPayoutSettingAdmin)
