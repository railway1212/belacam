from dal import autocomplete
from django import forms
from belacam.apps.referral.models import *

class ReferrerApplicationForm(forms.ModelForm):
    class Meta:
        model = ReferrerApplication
        fields = ('__all__')
        widgets = {
            'user': autocomplete.ModelSelect2(url='user-autocomplete'),
        }

class CampaignForm(forms.ModelForm):
    class Meta:
        model = Campaign
        fields = ('__all__')
        widgets = {
            'created_by': autocomplete.ModelSelect2(url='user-autocomplete'),
        }

class ReferrerForm(forms.ModelForm):
    class Meta:
        model = Referrer
        fields = ('__all__')
        widgets = {
            'created_by': autocomplete.ModelSelect2(url='user-autocomplete'),
        }

class UserReferrerForm(forms.ModelForm):
    class Meta:
        model = UserReferrer
        fields = ('__all__')
        widgets = {
            'user': autocomplete.ModelSelect2(url='user-autocomplete'),
        }
