from django.dispatch import receiver
from django.db.models.signals import pre_save

from belacam.apps.referral.choices import ReferrerApplicationStatus
from belacam.apps.referral.models import ReferrerApplication, Referrer


@receiver(pre_save, sender=ReferrerApplication)
def handle_update_application(sender, **kwargs):
    instance = kwargs.get('instance')
    if instance.status == ReferrerApplicationStatus.APPROVED.name:
        # TODO: Add new referral link
        user = instance.user
        referrer = Referrer.objects.filter(created_by=user).first()

        if referrer is None:
            referrer = Referrer(
                name=user.username,
                created_by=user,
                application=instance
            )
            referrer.save()
