from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from .choices import ReferrerApplicationStatus, UserReferrerStatus
from .compat import User
from .utils import id_generator


class Campaign(models.Model):
    name = models.CharField(_("Name"), max_length=255, unique=True)
    description = models.TextField(_("Description"), blank=True, null=True)
    pattern = models.CharField(
        _("Referrer pattern"),
        blank=True,
        max_length=255,
        help_text="All auto created referrers containing this pattern will be associated with this campaign"
    )
    created_by = models.ForeignKey(
        User,
        verbose_name=_("Created User"),
        related_name="campaigns",
        null=True
    )

    class Meta:
        ordering = ['name']
        verbose_name = _("Campaign")
        verbose_name_plural = _("Campaigns")

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.__unicode__()

    def count_users(self):
        count = 0
        for referrer in self.referrers.all():
            count += referrer.count_users()
        return count

    count_users.short_description = _("User count")


class ReferrerApplication(models.Model):
    user = models.OneToOneField(
        User,
        verbose_name=_("User"),
        related_name='referrer_application'
    )
    content = models.CharField(_("Application Content"), max_length=2048, blank=True, default="")
    created_at = models.DateTimeField(_("Creation date"), auto_now_add=True)
    status = models.CharField(max_length=10,
                              choices=ReferrerApplicationStatus.choices(),
                              db_index=True,
                              default=ReferrerApplicationStatus.PENDING.name)

    class Meta:
        verbose_name = _("Ref Link Application")
        verbose_name_plural = _("Ref Link Applications")

    def __unicode__(self):
        return "[%s]: %s" % (self.user.username, self.content[:10] + (self.content[10:] and '...'))

    def __str__(self):
        return self.__unicode__()


class Referrer(models.Model):
    name = models.CharField(_("Name"), max_length=255, unique=True)
    description = models.TextField(_("Description"), blank=True, null=True)
    application = models.ForeignKey(ReferrerApplication, null=True)
    created_at = models.DateTimeField(_("Creation date"), auto_now_add=True)
    created_by = models.ForeignKey(
        User,
        verbose_name=_("Created User"),
        related_name="referrers",
        null=True
    )
    campaign = models.ForeignKey(
        Campaign,
        verbose_name=_("Campaign"),
        related_name='referrers',
        blank=True,
        null=True
    )

    class Meta:
        ordering = ['name']
        verbose_name = _("Referrer")
        verbose_name_plural = _("Referrers")

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.__unicode__()

    def count_users(self):
        return self.users.count()

    count_users.short_description = _("User count")

    def match_campaign(self):
        for campaign in Campaign.objects.exclude(pattern=""):
            if campaign.pattern in self.name:
                self.campaign = campaign
                self.save()
                break

    @staticmethod
    def generate_random_name():
        return id_generator(6)


class UserReferrerManager(models.Manager):
    def apply_referrer(self, user, request):
        try:
            referrer = Referrer.objects.get(pk=request.session[settings.SESSION_KEY])
        except KeyError:
            return
        else:
            referred_user_count = referrer.count_users() + 1
            belas = 0

            for payout_setting in ReferralPayoutSetting.objects.all():
                if referred_user_count >= payout_setting.range_from:
                    if payout_setting.range_to is None or referred_user_count <= payout_setting.range_to:
                        belas = payout_setting.payout
                        break

            user_referrer = UserReferrer(
                user=user,
                referrer=referrer,
                belas=belas,
                status=UserReferrerStatus.PENDING.name
            )
            user_referrer.save()


class UserReferrer(models.Model):
    user = models.OneToOneField(
        User,
        verbose_name=_("User"),
        related_name='user_referrer'
    )
    referrer = models.ForeignKey(
        Referrer,
        verbose_name=_("Referrer"),
        related_name='users'
    )
    belas = models.IntegerField(default=0)
    status = models.CharField(max_length=10,
                              choices=UserReferrerStatus.choices(),
                              db_index=True,
                              default=UserReferrerStatus.PENDING.name)
    created_at = models.DateTimeField(_("Creation date"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Update date"), auto_now=True)
    objects = UserReferrerManager()

    class Meta:
        ordering = ['referrer__name']
        verbose_name = _("User Referrer")
        verbose_name_plural = _("User Referrers")

    def __unicode__(self):
        return "%s -> %s" % (self.user.username, self.referrer.name)

    def __str__(self):
        return self.__unicode__()


class ReferralPayoutSetting(models.Model):
    range_from = models.IntegerField(default=0)
    range_to = models.IntegerField(default=0, null=True, blank=True)
    payout = models.FloatField(default=0)

    class Meta:
        db_table = 'referral_payout_setting'
        verbose_name = 'Referral Range and Payout'
        verbose_name_plural = 'Referral Payout Settings'

    def __unicode__(self):
        return '{} - {}: {} Bela'.format(self.range_from, self.range_to, self.payout)

    def __str__(self):
        return self.__unicode__()
