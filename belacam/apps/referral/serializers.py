from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from django.core.urlresolvers import reverse
from django.db import models

from belacam.apps.referral.choices import UserReferrerStatus
from belacam.apps.referral.models import Referrer, UserReferrer, ReferralPayoutSetting, ReferrerApplication


class UserReferrerListSerializer(serializers.ListSerializer):
    class Meta:
        model = UserReferrer

    def to_representation(self, data):
        """
        List of object instances -> List of dicts of primitive datatypes.
        """
        # Dealing with nested relationships, data can be a Manager,
        # so, first get a queryset from the Manager if needed
        iterable = data.filter(status=UserReferrerStatus.VERIFIED.name) if isinstance(data, models.Manager) else data

        return [
            self.child.to_representation(item) for item in iterable
        ]


class UserReferrerSerializer(serializers.ModelSerializer):
    username = serializers.SerializerMethodField()

    def get_username(self, obj):
        return obj.user.username

    class Meta:
        list_serializer_class = UserReferrerListSerializer
        model = UserReferrer
        fields = '__all__'


class ReferrerSerializer(serializers.ModelSerializer):
    users = UserReferrerSerializer(many=True)
    link = serializers.SerializerMethodField()

    class Meta:
        model = Referrer
        fields = '__all__'

    def get_link(self, obj):
        link = reverse('referrer_view', args=[obj.name])
        request = self.context.get('request', None)
        if request:
            link = request.build_absolute_uri(link)
        return link


class ReferralPayoutSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReferralPayoutSetting
        fields = '__all__'


class ReferrerApplicationSerializer(serializers.ModelSerializer):
    user_id = serializers.IntegerField(write_only=True,
                                       validators=[UniqueValidator(queryset=ReferrerApplication.objects.all())])

    def create(self, validated_data):
        obj = ReferrerApplication.objects.create(**validated_data)
        return obj

    class Meta:
        model = ReferrerApplication
        fields = '__all__'
        read_only_fields = ('created_at', 'user',)
