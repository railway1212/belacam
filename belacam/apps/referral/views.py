import copy

from rest_framework.views import APIView
from rest_framework.response import Response

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from .models import Referrer, ReferralPayoutSetting, ReferrerApplication
from belacam.apps.referral.choices import ReferrerApplicationStatus, UserReferrerStatus
from belacam.apps.referral.serializers import (
    ReferrerSerializer, ReferralPayoutSettingSerializer, ReferrerApplicationSerializer
)
from belacam.apps.thoughts.models import AppSetting


class ReferrerView(APIView):
    permission_classes = []

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ReferrerView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwrags):
        user = request.user
        referrer = Referrer.objects.filter(created_by=user).first()

        if referrer is None:
            return Response({}, status=404)

        return Response({
            'status': 'success',
            'result': {
                'referrer': ReferrerSerializer(referrer, context={'request': request}).data
            }
        })


class ReferrerStatisticView(APIView):
    permission_classes = []

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ReferrerStatisticView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        user = request.user
        referrer = Referrer.objects.filter(created_by=user).first()

        users = [user for user in referrer.users.filter(status=UserReferrerStatus.VERIFIED.name)] if referrer else []
        num_users = 0
        belas_income = 0
        for referred_user in users:
            num_users += 1
            belas_income += referred_user.belas

        dollar = AppSetting.objects.values_list("bella_average_price", flat=True).first() * belas_income

        result = {
            'num_users': num_users,
            'belas_income': belas_income,
            'dollar_income': dollar
        }
        return Response(result)


class ReferrerSettingsView(APIView):
    permission_classes = []

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ReferrerSettingsView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        payout_settings = ReferralPayoutSetting.objects.all().order_by('range_from')
        return Response(ReferralPayoutSettingSerializer(payout_settings, many=True).data)


class ReferrerApplicationView(APIView):
    permission_classes = []

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ReferrerApplicationView, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        user = request.user
        status = request.GET.get('status', None)

        query = ReferrerApplication.objects.filter(user=user)
        if status is not None:
            query = query.filter(status=status)

        application = query.first()

        if application is not None:
            serializer = ReferrerApplicationSerializer(application)
            response = {
                'status': 'success',
                'result': {
                    'application': serializer.data
                }
            }
            return Response(response)
        else:
            return Response({}, status=404)

    def post(self, request):
        response = {}
        request_data = copy.copy(request.POST)
        request_data.update({
            'status': ReferrerApplicationStatus.PENDING.name,
            'user_id': request.user.id
        })

        serializer = ReferrerApplicationSerializer(data=request_data)
        if serializer.is_valid():
            if self.check_referral_condition(request):
                serializer.save()
                response.update({
                    'status': 'success',
                    'result': {
                        'application': serializer.data
                    }
                })
                return Response(response)
            else:
                response.update({
                    'status': 'fail',
                    'message': 'Please complete conditions before submitting referral application.',
                    'error': {}
                })
                return Response(response)
        else:
            response.update({
                'status': 'fail',
                'message': 'Bad data format',
                'error': serializer.errors
            })

            return Response(response)

    def check_referral_condition(self, request):
        # Disable this checking temporary. (https://trello.com/c/YbKKwtV5/779-remove-some-barriers-to-referral-links)
        # return request.user.user1.verified and request.user.user1.total_deposit >= 50 and request.user.user1.earnings >= 1

        return request.user.user1.verified
