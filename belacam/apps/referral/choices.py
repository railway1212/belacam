from belacam.apps.core.enum import ChoiceEnum


class ReferrerApplicationStatus(ChoiceEnum):
    PENDING = 'Pending'
    APPROVED = 'Approved'
    DENIED = 'Denied'


class UserReferrerStatus(ChoiceEnum):
    PENDING = 'Pending'
    VERIFIED = 'Verified'
