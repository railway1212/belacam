# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import WAUStats, LoginTrace


@admin.register(WAUStats)
class WAUStatsAdmin(admin.ModelAdmin):
    change_form_template = 'admin/stats/change_form.html'

    def change_view(self, request, object_id, form_url='', extra_context=None):
        triggers = [
            'life_time_earnings_1',
            'have_made_5_posts',
            'have_made_1_withdrawal',
            'have_followed_20_users',
            'received_1_earnings_email'
        ]
        extra_context = {
            'triggers_by_aha_factor': {
                'triggers': triggers,
                'aha_factor': [
                    1, 2, 3, 4, 5
                ]
            },
            'triggers_by_wau': {
                'triggers': triggers,
                'wau': [12, 32, 23, 22, 22]
            },
            'triggers_by_non_wau': {
                'triggers': triggers,
                'non_wau': [12, 32, 23, 22, 22]
            }

        }
        return super(WAUStatsAdmin, self).change_view(request, object_id, form_url, extra_context)


@admin.register(LoginTrace)
class LoginTraceAdmin(admin.ModelAdmin):
    list_per_page = 20
