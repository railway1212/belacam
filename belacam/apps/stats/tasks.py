from celery import shared_task

from django.utils import timezone

from .models import WAUStats


@shared_task
def start_wau_calculations():
    today = timezone.now().date()

    # we calculate WAU Stats only once a day
    if not WAUStats.objects.filter(date=today).count():
        wau_stats = WAUStats.objects.create(
            date=today
        )
        wau_stats.calculate_wau_stats()

