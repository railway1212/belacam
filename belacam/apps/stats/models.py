# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_in
from django.utils import timezone


class WAUStats(models.Model):
    """
    WAU: Weekly Active Users:
      - someone who logs on at least once every 7 days for 3 weeks in a row.

    Non-WAU: Not a Weekly Active User:
      - someone who has not logged on in the past 7 days but made an account within the last 21 days.

    Goal:
    We want to identify our Weekly Active Users (WAU) and track what events help trigger their WAU status.
    If an event is highly associated with retained users and lowly associated with inactive users,
    then it may be an Aha moment.
    """

    EVENTS = [
        'life_time_earnings_1',
        'have_made_5_posts',
        'have_made_1_withdrawal',
        'have_followed_20_users',
        'received_1_earnings_email'
    ]
    total_wau = models.IntegerField(null=True)
    total_non_wau = models.IntegerField(null=True)
    date = models.DateField()

    def calculate_wau_stats(self):
        """
        1. calculates and saves "total_wau" and "total_non_wau"
        2. creates all event names.
        3. starts _calculate_aha_moments()
        """
        # 1
        three_weeks_ago = timezone.now() - timezone.timedelta(weeks=3)
        last_3_weeks_logged_users = User.objects.filter(last_login__gte=three_weeks_ago)

        # get everyday date into a list:
        today = timezone.now()
        date_list = [today - timezone.timedelta(days=x) for x in range(0, 21)]

        final_wau_list = []
        for dt in date_list:
            pass

        # todo: track everyday login.

        self._calculate_aha_moments()

    def _calculate_aha_moments(self):
        """
        triggers each event aha calculation.
        """
        events = self.wau_events.all()
        for event in events:
            event.calculate_aha()

    def __str__(self):
        return 'wau of day: %s' % self.date.strftime('%m.%d.%Y')


class WAUEvent(models.Model):
    wau_stats = models.ForeignKey(WAUStats, related_name='wau_events')
    event_name = models.CharField(max_length=400)
    aha_factor = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    wau_percent = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    non_wau_percent = models.DecimalField(max_digits=5, decimal_places=2, null=True)

    def calculate_aha(self):
        pass

    def __str__(self):
        return 'wau of event: %s' % self.event_name


class LoginTrace(models.Model):
    date = models.DateField(db_index=True)
    user = models.ForeignKey(User, related_name='user_login_traces')

    def __str__(self):
        return 'user: %s, date: %s' % (
            self.user.username, self.date.strftime('%m.%d.%Y')
        )


def track_user_login(sender, request, user, **kwargs):
    today = timezone.now().date()
    if not user.user_login_traces.filter(date=today).count():
        _ = user.user_login_traces.create(date=today)


user_logged_in.connect(track_user_login)
