import {Component, Input, OnInit} from '@angular/core';
import {User} from "../app.model";

@Component({
  selector: 'app-insufficient-bela',
  templateUrl: './insufficient-bela.component.html',
  styleUrls: []
})
export class InsufficientBelaComponent implements OnInit {
  @Input() user:User = new User();
  visible: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  close() {
    this.visible = false;
  }

  show() {
    this.visible = true;
  }
}
