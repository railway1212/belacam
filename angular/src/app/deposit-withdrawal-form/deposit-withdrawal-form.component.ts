import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Thought, User } from "../app.model";
import { BelaApiService } from "../core/bela-api.service";
import { Comment } from '../app.model';
import { Observable } from "rxjs/Observable";
import { EventService } from "../core/event.service";
import { Subscription } from "rxjs/Subscription";
import { AuthService } from "../core/auth.service";
import {BelaEvent, BelaEventType} from "../core/event.types";

@Component({
  selector: 'app-deposit-withdrawal-form',
  templateUrl: './deposit-withdrawal-form.component.html',
  styleUrls: ['./deposit-withdrawal-form.component.css'],
  providers: []
})
export class DepositWithdrawalFormComponent implements OnInit, OnDestroy {

  @ViewChild('withdrawalAddress') withdrawalAddress: any;
  @ViewChild('withdrawalAmount') withdrawalAmount: any;

  user = new User();
  withdrawal_address = "";
  withdrawal_amount = 0;
  loading = false;
  subscriptions: Subscription[] = [];

  constructor(private appService: BelaApiService,
              private authService: AuthService,
              private eventService: EventService) { }

  ngOnInit() {
    this.user = this.authService.user;
    this.subscriptions.push(this.eventService.subscribe({
      next: (event: BelaEvent) => {
        if(event.type == BelaEventType.LIKE_POST) {
          this.user = event.value;
          localStorage.setItem('user', JSON.stringify(event.value));
        } else if(event.type == BelaEventType.UPDATE_USER) {
          this.user = event.value;
        }
      }
    }));
  }

  ngOnDestroy() {
    for(let subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  withdrawal() {
    if(this.user.user1) {
      if(this.user.user1.belacoin < this.withdrawal_amount) {
        alert("Withdrawal amount must be smaller than current account capacity!");
      } else if(this.withdrawal_amount > 0 && this.withdrawal_address != "") {
        this.loading = true;
        this.appService.createWithdrawRequest(this.withdrawal_address, this.withdrawal_amount)
          .then(resp =>{
            this.user.user1.belacoin = resp['balance'];
            this.authService.updateUser(this.user);
            alert("A withdrawal confirmation message will arrive in your email inbox in the next 5 minutes. Please click the link there to confirm your withdrawal!");
            this.loading = false;
          });
      }
    }

  }
  assignAddress(event: any) {
    this.withdrawal_address = event.target.value;
  }
  assignAmount(event: any) {
    this.withdrawal_amount = parseFloat(event.target.value);
  }

}
