import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import {BelaApiService} from "../core/bela-api.service";

declare var jQuery:any;

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  @Input() unread_count:any = 0;
  @Input() notification_list:any = [];
  csrf="";

  constructor(private apiService:BelaApiService,private router: Router) { }

  ngOnInit() {
    this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
  }

  markAllRead(){
    this.apiService.markAllRead().then(result =>{
      this.apiService.getUnreadNotifications().then(result =>{
        this.unread_count = result.unread_count;
        this.notification_list = result.notifications_list;
      });
    })
  }
  goToAction(notification){
    this.apiService.markItRead(notification).then(result =>{
      if(notification.unread == true){
        notification.unread=false;
        this.unread_count = this.unread_count-1;
      }
    });
    if(notification.action == "comment"|| notification.action == "bella")
      this.router.navigate(['/post',notification.username, notification.action_id]);
    else if(notification.action == "user"){
      this.router.navigate(['/user', notification.username]);
    }
  }

  handleClickAvatar(event, notification) {
    event.stopPropagation();
    jQuery("#notification-dropdown").dropdown('toggle');
    this.router.navigate(['user/', notification.actor])
  }

  followUser(event, user) {
    event.preventDefault();
    event.stopPropagation();
    this.apiService.follow(user, this.csrf).then(listResp=>{
      window.location.reload();
    });
  }

  unfollowUser(event, user) {
    event.preventDefault();
    event.stopPropagation();
    this.apiService.unfollow(user, this.csrf).then(listResp=>{
      window.location.reload();
    });
  }
}
