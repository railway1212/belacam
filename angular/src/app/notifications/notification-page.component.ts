import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { BelaApiService } from "../core/bela-api.service";
import { Location } from "@angular/common";
import { Lightbox } from "../lightbox/lightbox.service";
import { Constants } from "../constants";
import { BelaNotification, User } from "../app.model";
import { AuthService } from "../core/auth.service";
import {WebSocketEvent, WebSocketService} from "../core/web-socket.service";
import {Subscription} from "rxjs/Subscription";
declare var jQuery:any;

@Component({
  selector: 'app-notification-page',
  templateUrl: './notification-page.component.html',
  styleUrls: ['./notification-page.component.css']
})
export class NotificationPageComponent implements OnInit, OnDestroy {

  csrf="";
  requesting_user = new User();

  subscriptions: Subscription[] = [];
  all_notifications_api_url = '/api/notifications/';

  next_page_url: string = '';

  new_notifications = 0;
  notification_list: BelaNotification[] = [];
  unread_count: number;
  public loading = true;

  constructor(private apiService:BelaApiService,
              private authService: AuthService,
              private webSocketSerivce: WebSocketService,
              private route:ActivatedRoute,
              private router: Router
              ) { }
  ngOnInit() {

    this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();

    this.requesting_user = this.authService.user;

    if(this.authService.isLoggedIn()) {
      this.loadNewNotifications();

      this.subscriptions.push(this.webSocketSerivce.subscribe({
        next: (event: WebSocketEvent) => {
          if(event.type === WebSocketEvent.EVENT_NEW_NOTIFICATION) {
            this.new_notifications ++;
          }
        }
      }))
    }
  }

  ngOnDestroy() {
    for(let subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  handleClickAvatar(event, notification) {
    event.stopPropagation();
    this.router.navigate(['user/', notification.actor])
  }

  loadNewNotifications() {
    this.loading = true;
    this.apiService.getNotificatons(this.all_notifications_api_url, {from: new Date().toISOString()})
      .then( listResp => {
        this.next_page_url = listResp.next;
        this.new_notifications = 0;
        this.notification_list = listResp.results;
        this.loading = false;
      });
  }

  goToAction(notification){
    this.apiService.markItRead(notification).then(result =>{
      if(notification.unread == true){
        notification.unread=false;
        this.unread_count = this.unread_count-1;
      }
    });
    if(notification.action == "comment"|| notification.action == "bella")
      this.router.navigate(['/post',notification.username, notification.action_id]);
    else if(notification.action == "user"){
      this.router.navigate(['/user', notification.username]);
    }
  }

  followUser(event, user) {
    event.preventDefault();
    event.stopPropagation();
    this.apiService.follow(user, this.csrf).then(listResp=>{
      window.location.reload();
    });
  }

  unfollowUser(event, user) {
    event.preventDefault();
    event.stopPropagation();
    this.apiService.unfollow(user, this.csrf).then(listResp=>{
      window.location.reload();
    });
  }

  onNotificationScroll() {
    let bottom_reached = this.apiService.isBottomReached();
    if(this.next_page_url != null && bottom_reached && !this.loading){
      this.loading = true;
      this.apiService.getNotificatons(this.next_page_url, {}).then(listResp =>{
        this.next_page_url = listResp.next;
        for(let result of listResp.results) {
          this.notification_list.push(result);
        }
        this.loading = false;
      });
    }
  }
}
