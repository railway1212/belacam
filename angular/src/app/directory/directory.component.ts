import { Component, OnInit, Input } from '@angular/core';
import { User, Tag } from '../app.model';
import { BelaApiService } from '../core/bela-api.service';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import { Lightbox } from '../lightbox/lightbox.service';
declare var jQuery:any;
import { Observable }       from 'rxjs/Observable';
import { HttpModule, JsonpModule } from '@angular/http';

@Component({
  template: `
    <h1>Search Demo</h1>
    <p>Search after each keystroke</p>
  `
})
export class DirectoryComponent {
  items: Array<string>;
  stuff = [1,2,3,4,5];

  constructor () { }
}
