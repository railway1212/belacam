import {
  AfterViewInit,
  Component,
  ElementRef,
  Inject,
  Input,
  OnInit,
  OnDestroy,
  Renderer,
  ViewChild,
} from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { LightboxEvent, LIGHTBOX_EVENT, IAlbum, IEvent, LightboxWindowRef } from './lightbox-event.service';
import { BelaApiService } from '../core/bela-api.service';
import {BelaResponse, ExThought, LikeBelaResult, User} from "../app.model";
import { EventService } from "../core/event.service";
import { AuthService } from "../core/auth.service";
import { InsufficientBelaComponent } from "../insufficient-bela/insufficient-bela.component";
import { BelaEventType, EvLoadedAlbum, EvRequestAlbum } from "../core/event.types";

declare var jQuery:any;
@Component({
  templateUrl: './lightbox.component.html',
  selector: '[lb-content]',
  host: {
    '(click)': 'close($event)',
    '[class]': 'ui.classList'
  },
  styleUrls: [],
  providers: [ BelaApiService ]
})
export class LightboxComponent implements AfterViewInit, OnDestroy, OnInit {
  @Input() album: Array<IAlbum>;
  @Input() currentImageIndex: number;
  @Input() options: any;
  @Input() cmpRef: any;

  @ViewChild('outerDiv') _outerDivElem: ElementRef;
  @ViewChild('outerContainer') _outerContainerElem: ElementRef;
  @ViewChild('container') _containerElem: ElementRef;
  @ViewChild('leftArrow') _leftArrowElem: ElementRef;
  @ViewChild('rightArrow') _rightArrowElem: ElementRef;
  @ViewChild('navArrow') _navArrowElem: ElementRef;
  @ViewChild('dataContainer') _dataContainerElem: ElementRef;
  @ViewChild('image') _imageElem: ElementRef;
  @ViewChild('caption') _captionElem: ElementRef;
  @ViewChild('number') _numberElem: ElementRef;
  @ViewChild('insufficientMessage') _insufficientMessage: InsufficientBelaComponent;

  public content: any;
  public ui: any;
  public curPageID: number = 0;
  private _cssValue: any;
  private _event: any;
  private _windowRef: any;

  csrf="";
  requesting_user = new User();
  videoSrc="";

  constructor(
    private _elemRef: ElementRef,
    private _rendererRef: Renderer,
    private _lightboxEvent: LightboxEvent,
    public _lightboxElem: ElementRef,
    private _lightboxWindowRef: LightboxWindowRef,
    @Inject(DOCUMENT) private _documentRef: any,
    private appService: BelaApiService,
    private eventService: EventService,
    private authService: AuthService
  ) {
    // initialize data
    this.options = this.options || {};
    this.album = this.album || [];
    this.currentImageIndex = this.currentImageIndex || null;
    this._windowRef = this._lightboxWindowRef.nativeWindow;

    // control the interactive of the directive
    this.ui = {
      // control the appear of the reloader
      // false: image has loaded completely and ready to be shown
      // true: image is still loading
      showReloader: true,
      showAlbumRequestLoader: false,
      // control the appear of the nav arrow
      // the arrowNav is the parent of both left and right arrow
      // in some cases, the parent shows but the child does not show
      showLeftArrow: false,
      showRightArrow: false,
      showArrowNav: false,

      // control whether to show the
      // page number or not
      showPageNumber: false,
      showCaption: false,
      classList: 'lightbox animation fadeIn'
    };

    this.content = {
      pageNumber: ''
    };

    this._event = {
      subscriptions: []
    };
    this._lightboxElem = this._elemRef;
  }

  public ngAfterViewInit(): void {
    // need to init css value here, after the view ready
    // actually these values are always 0
    this._cssValue = {
      containerTopPadding: Math.round(this._getCssStyleValue(this._containerElem, 'padding-top')),
      containerRightPadding: Math.round(this._getCssStyleValue(this._containerElem, 'padding-right')),
      containerBottomPadding: Math.round(this._getCssStyleValue(this._containerElem, 'padding-bottom')),
      containerLeftPadding: Math.round(this._getCssStyleValue(this._containerElem, 'padding-left')),
      imageBorderWidthTop: Math.round(this._getCssStyleValue(this._imageElem, 'border-top-width')),
      imageBorderWidthBottom: Math.round(this._getCssStyleValue(this._imageElem, 'border-bottom-width')),
      imageBorderWidthLeft: Math.round(this._getCssStyleValue(this._imageElem, 'border-left-width')),
      imageBorderWidthRight: Math.round(this._getCssStyleValue(this._imageElem, 'border-right-width'))
    };

    if (this._validateInputData()) {
      this._prepareComponent();
      this._registerImageLoadingEvent();
    }

    jQuery('#dropdownMenuLink3').dropdown();
  }

  ngOnInit() {
    this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
    this.requesting_user = JSON.parse(localStorage.getItem('user'));
    this._event.subscriptions.push(
      this._lightboxEvent.lightboxEvent$
        .subscribe((event: IEvent) => this._onReceivedEvent(event))
    );
    this._event.subscriptions.push(
      this.eventService.subscribe({
        next: (event) => {
          if(event.type === BelaEventType.LOADED_NEXT_ALBUM) {
            let evValue: EvLoadedAlbum = event.value;

            this.album = evValue.album;

            this.ui.showAlbumRequestLoader = false;

            this._changeImage(evValue.newImageIndex);

          } else if(event.type === BelaEventType.NO_NEXT_ALBUM) {
            this.ui.showAlbumRequestLoader = false;
          }
        }
      })
    )
  }

  public ngOnDestroy(): void {
    if (!this.options.disableKeyboardNav) {
      // unbind keyboard event
      this._disableKeyboardNav();
    }

    for(let subscription of this._event.subscriptions) {
      subscription.unsubscribe();
    }
  }

  public handleAvatarClick($event: any): void {
    $event.stopPropagation();
    this._lightboxEvent.broadcastLightboxEvent({ id: LIGHTBOX_EVENT.CLOSE, data: null });
  }

  public close($event: any): void {
    $event.stopPropagation();
    if (($event.target.classList.contains('lightbox') && this.isHome() ) ||
      $event.target.classList.contains('lb-loader') ||
      $event.target.classList.contains('lb-outerDiv') ||
      $event.target.classList.contains('lb-close')) {

      this._close();
    } else if (($event.target.classList.contains('lightbox') && !this.isHome() )){
      this.openHome();
    }
  }

  public nextImage(): void {
    if (this.currentImageIndex !== this.album.length - 1) {
      this._changeImage(this.currentImageIndex + 1);
    } else if (this.options.wrapAround && this.album.length > 1) {
      this._changeImage(0);
    } else if (this.options.allowRequestAlbum && !this.ui.showAlbumRequestLoader) {
      let evValue = new EvRequestAlbum();
      evValue.album = this.album;
      evValue.currentImageIndex = this.currentImageIndex;
      evValue.direction = EvRequestAlbum.DIRECTION_NEXT;
      evValue.requestID = this.options.requestID;

      this.eventService.fireEvent({
        type: BelaEventType.REQUEST_NEXT_ALBUM,
        value: evValue
      });

      this.ui.showAlbumRequestLoader = true;
    }
  }

  public prevImage(): void {
    if (this.currentImageIndex !== 0) {
      this._changeImage(this.currentImageIndex - 1);
    } else if (this.options.wrapAround && this.album.length > 1) {
      this._changeImage(this.album.length - 1);
    } else if (this.options.allowRequestAlbum && !this.ui.showAlbumRequestLoader) {
      let evValue = new EvRequestAlbum();
      evValue.album = this.album;
      evValue.currentImageIndex = this.currentImageIndex;
      evValue.direction = EvRequestAlbum.DIRECTION_PREV;
      evValue.requestID = this.options.requestID;

      this.eventService.fireEvent({
        type: BelaEventType.REQUEST_NEXT_ALBUM,
        value: evValue
      });

      this.ui.showAlbumRequestLoader = true;
    }
  }

  like(post: ExThought){
    if(!post.is_liking && !post.is_liked && this.requesting_user) {

      post.is_liking = true;
      post.is_liked = true;
      this.appService.like(post).then((res: BelaResponse) => {
        if (res.error === LikeBelaResult.E_INSUFFICIENT_COIN) {
          this._insufficientMessage.show();
        } else {
          let result = res.result;
          if(result.bela) {
            post.likes.push(result.bela);

            post.belas = result.total_bela;
            post.earnings = result.total_earning;
            post.is_liked = result.liked;
            post.is_liking = false;
            post.total_likes_count = result.total_likes;
          }

          if(result.user) {
            this.requesting_user.user1 = result.user;
            this.authService.updateUser(this.requesting_user);

            this.eventService.fireEvent({
              type: BelaEventType.LIKE_POST,
              value: this.requesting_user
            });
          }
        }
      }).catch(() => {
        post.is_liked = false;
        post.is_liking = false;
      });
    }
  }

  removeAlbumAtPos(idx) {
    this.eventService.fireEvent({
      type: BelaEventType.DELETED_POST,
      value: this.album[idx].thot
    });

    this.album.splice(this.currentImageIndex,1);

    if(this.album.length < 1) {
      this._lightboxEvent.broadcastLightboxEvent({ id: LIGHTBOX_EVENT.CLOSE, data: null });
    } else {
      this.prevImage();
    }
  }

  report(){
    jQuery('#dropdownMenuLink3').dropdown('toggle');

    this.appService.report(this.currentThot.id).then( result=>{
      this.removeAlbumAtPos(this.currentImageIndex);
    });
  }

  delete(){
    jQuery('#dropdownMenuLink3').dropdown('toggle');

    this.appService.deleteFeed(this.currentThot.id).then( ()=>{
      this.removeAlbumAtPos(this.currentImageIndex);
    });
  }

  deleteAdmin(thot, i){
    jQuery('#dropdownMenuLink3').dropdown('toggle');

    this.appService.deleteAdminFeed(this.currentThot.id).then( ()=>{
      this.removeAlbumAtPos(this.currentImageIndex);
    }).catch(()=>{
      alert("Can't delete the post. Try again later");
    });
  }

  block_user(){
    jQuery('#dropdownMenuLink3').dropdown('toggle');

    this.appService.blockUser(this.currentThot.user.id).then(listResp=>{
      window.location.reload();
    });
  }

  editable(state){
    jQuery('#dropdownMenuLink3').dropdown('toggle');

    this.currentThot.editable = state;
    if(state) {
      this.currentThot.tempContent = this.currentThot.content;
    } else {
      this.currentThot.tempContent = '';
    }
  }

  update() {
    this.appService.updateCaption(this.currentThot.id, this.currentThot.tempContent).then( result=>{
      if(result['status'] == "success") {
        this.currentThot.content = result['result'];
        this.currentThot.editable = false;
      } else {
        alert("Something went wrong. Please try again later!");
      }
    });
  }

  addCommentToAlbumPost(comment) {
    this.currentThot.comments.push(comment);
    this.currentThot.displayed_comments.push(comment);
  }

  follow(user){
    if(user && user.id) {
      this.appService.follow(user, this.csrf).then(result => {
        user.is_following = true;
      }).catch(err => {
      })
    }
  }

  unfollow(user){

    this.appService.unfollow(user, this.csrf).then( result=>{
      user.is_following=false;
    }).catch(err => {})
  }

  isHome() {
    return this.curPageID === 0;
  }

  openHome() {
    this.curPageID = 0;
  }

  openLikes() {
    if(this.currentThot.total_likes_count>0) {
      this.curPageID = 1;
    }
  }

  openViews() {
    if(this.requesting_user) {
      this.curPageID = 2;
    }
  }

  showVideo(album) {
    if (!album.src) {
      this.videoSrc = album.thot.video;
      return true;
    } else {
      this.videoSrc = '';
      return false;
    }
  }

  private _validateInputData(): boolean {
    if (this.album && this.album.length > 0) {
      for (let i = 0; i < this.album.length; i++) {
        // check whether each _nside
        // album has src data or not

        if (this.album[i].thot.video) {
          continue;
        } else if (this.album[i].src) {
          continue;
        }

        throw new Error('One of the album data does not have source data');
      }
    } else {
      throw new Error('No album data or album data is not correct in type');
    }

    // to prevent data understand as string
    // convert it to number
    if (isNaN(this.currentImageIndex)) {
      throw new Error('Current image index is not a number');
    } else {
      this.currentImageIndex = Number(this.currentImageIndex);
    }

    return true;
  }

  private _registerImageLoadingEvent(): void {
    const preloader = new Image();

    preloader.onload = preloader.onerror = () => {
      this._onLoadImageSuccess();
    };

    preloader.src = this.album[this.currentImageIndex].src;
  }

  /**
   * Fire when the image is loaded
   */
  private _onLoadImageSuccess(): void {
    if (!this.options.disableKeyboardNav) {
      // unbind keyboard event during transition
      this._disableKeyboardNav();
    }

    let imageHeight;
    let imageWidth;
    let maxImageHeight;
    let maxImageWidth;
    let windowHeight;
    let windowWidth;
    let naturalImageWidth;
    let naturalImageHeight;

    // set default width and height of image to be its natural
    imageWidth = naturalImageWidth = this._imageElem.nativeElement.naturalWidth;
    imageHeight = naturalImageHeight = this._imageElem.nativeElement.naturalHeight;
    if (this.options.fitImageInViewPort) {
      windowWidth = this._windowRef.innerWidth;
      windowHeight = this._windowRef.innerHeight;
      maxImageWidth = windowWidth - this._cssValue.containerLeftPadding -
        this._cssValue.containerRightPadding - this._cssValue.imageBorderWidthLeft -
        this._cssValue.imageBorderWidthRight - 20;
      maxImageHeight = windowHeight - this._cssValue.containerTopPadding -
        this._cssValue.containerTopPadding - this._cssValue.imageBorderWidthTop -
        this._cssValue.imageBorderWidthBottom - 120;
      if (naturalImageWidth > maxImageWidth || naturalImageHeight > maxImageHeight) {
        if ((naturalImageWidth / maxImageWidth) > (naturalImageHeight / maxImageHeight)) {
          imageWidth = maxImageWidth;
          imageHeight = Math.round(naturalImageHeight / (naturalImageWidth / imageWidth));
        } else {
          imageHeight = maxImageHeight;
          imageWidth = Math.round(naturalImageWidth / (naturalImageHeight / imageHeight));
        }
      }

      this._rendererRef.setElementStyle(this._imageElem.nativeElement, 'width', `${imageWidth}px`);
      this._rendererRef.setElementStyle(this._imageElem.nativeElement, 'height', `${imageHeight}px`);
    }

    this._sizeContainer(imageWidth, imageHeight);
    this._sizeNav(imageWidth, imageHeight);
    this._sizeOuterDiv();
  }

  private _sizeContainer(imageWidth: number, imageHeight: number): void {
    const oldWidth  = this._outerContainerElem.nativeElement.offsetWidth;
    const oldHeight = this._outerContainerElem.nativeElement.offsetHeight;
    const newWidth  = imageWidth + this._cssValue.containerRightPadding + this._cssValue.containerLeftPadding +
      this._cssValue.imageBorderWidthLeft + this._cssValue.imageBorderWidthRight;
    const newHeight = imageHeight + this._cssValue.containerTopPadding
      + this._cssValue.containerBottomPadding + this._cssValue.imageBorderWidthTop
      + this._cssValue.imageBorderWidthBottom;

    if (oldWidth !== newWidth /* || oldHeight !== newHeight */) {
      this._rendererRef.setElementStyle(this._outerContainerElem.nativeElement, 'max-width', `935px`);
      // this._rendererRef.setElementStyle(this._outerContainerElem.nativeElement, 'width', `${newWidth}px`);
      // this._rendererRef.setElementStyle(this._outerContainerElem.nativeElement, 'height', `${newHeight}px`);
      // bind resize event to outer container
      // this._event.transitions = [];
      // ['transitionend', 'webkitTransitionEnd', 'oTransitionEnd', 'MSTransitionEnd'].forEach(eventName => {
      //   this._event.transitions.push(
      //     this._rendererRef.listen(this._outerContainerElem.nativeElement, eventName, (event: any) => {
      //       if (event.target === event.currentTarget) {
      //         this._postResize(newWidth, newHeight);
      //       }
      //     })
      //   );
      // });
      this._postResize(newWidth, newHeight);
    } else {
      this._postResize(newWidth, newHeight);
    }
  }

  private _sizeNav(imageWidth: number, imageHeight: number): void {
    this._rendererRef.setElementStyle(this._navArrowElem.nativeElement, 'height', `${imageHeight}px`);
  }

  private _sizeOuterDiv(): void {
    const containerHeight = this._outerContainerElem.nativeElement.offsetHeight;
    const windowHeight = this._windowRef.innerHeight;
    if (containerHeight > windowHeight) {
      this._rendererRef.setElementStyle(this._outerDivElem.nativeElement, 'height', 'auto');
    } else {
      this._rendererRef.setElementStyle(this._outerDivElem.nativeElement, 'height', '100%');
    }
  }

  private _postResize(newWidth: number, newHeight: number): void {
    // unbind resize event
    if (Array.isArray(this._event.transitions)) {
      this._event.transitions.forEach((eventHandler: any) => {
        eventHandler();
      });

      this._event.transitions = [];
    }

    this._rendererRef.setElementStyle(this._dataContainerElem.nativeElement, 'width', `${newWidth}px`);
    this._showImage();
  }

  private _showImage(): void {
    this.ui.showReloader = false;
    this._updateNav();
    this._updateDetails();
    if (!this.options.disableKeyboardNav) {
      this._enableKeyboardNav();
    }
  }

  private _prepareComponent(): void {
    // add css3 animation
    this._addCssAnimation();

    // position the image according to user's option
    this._positionLightBox();
  }

  private _positionLightBox(): void {
    const top = this.options.positionFromTop;
    const bottom = this.options.positionFromBottom;
    const left = 0;

    this._rendererRef.setElementStyle(this._lightboxElem.nativeElement, 'top', `${top}px`);
    this._rendererRef.setElementStyle(this._lightboxElem.nativeElement, 'bottom', `${bottom}px`);
    this._rendererRef.setElementStyle(this._lightboxElem.nativeElement, 'left', `${left}px`);
    this._rendererRef.setElementStyle(this._lightboxElem.nativeElement, 'display', 'block');
  }

  /**
   * addCssAnimation add css3 classes for animate lightbox
   */
  private _addCssAnimation(): void {
    const resizeDuration = this.options.resizeDuration;
    const fadeDuration = this.options.fadeDuration;

    this._rendererRef.setElementStyle(this._lightboxElem.nativeElement,
      '-webkit-animation-duration', `${fadeDuration}s`);
    this._rendererRef.setElementStyle(this._lightboxElem.nativeElement,
      '-animation-duration', `${fadeDuration}s`);
    this._rendererRef.setElementStyle(this._outerContainerElem.nativeElement,
      '-webkit-transition-duration', `${resizeDuration}s`);
    this._rendererRef.setElementStyle(this._outerContainerElem.nativeElement,
      '-transition-duration', `${resizeDuration}s`);
    this._rendererRef.setElementStyle(this._dataContainerElem.nativeElement,
      '-webkit-animation-duration', `${fadeDuration}s`);
    this._rendererRef.setElementStyle(this._dataContainerElem.nativeElement,
      '-animation-duration', `${fadeDuration}s`);
    this._rendererRef.setElementStyle(this._imageElem.nativeElement,
      '-webkit-animation-duration', `${fadeDuration}s`);
    this._rendererRef.setElementStyle(this._imageElem.nativeElement,
      '-animation-duration', `${fadeDuration}s`);
    this._rendererRef.setElementStyle(this._captionElem.nativeElement,
      '-webkit-animation-duration', `${fadeDuration}s`);
    this._rendererRef.setElementStyle(this._captionElem.nativeElement,
      '-animation-duration', `${fadeDuration}s`);
    this._rendererRef.setElementStyle(this._numberElem.nativeElement,
      '-webkit-animation-duration', `${fadeDuration}s`);
    this._rendererRef.setElementStyle(this._numberElem.nativeElement,
      '-animation-duration', `${fadeDuration}s`);
  }

  private _end(): void {
    this.ui.classList = 'lightbox animation fadeOut';
    setTimeout(() => {
      this.cmpRef.destroy();
    }, this.options.fadeDuration * 1000);
  }

  private _updateDetails(): void {
    // update the caption
    if (typeof this.album[this.currentImageIndex].caption !== 'undefined' &&
      this.album[this.currentImageIndex].caption !== '') {
      this.ui.showCaption = true;
    }

    // update the page number if user choose to do so
    // does not perform numbering the page if the
    // array length in album <= 1
    if (this.album.length > 1 && this.options.showImageNumberLabel) {
      this.ui.showPageNumber = true;
      this.content.pageNumber = this._albumLabel();
    }
  }

  private _albumLabel(): string {
    // due to {this.currentImageIndex} is set from 0 to {this.album.length} - 1
    return `Image ${Number(this.currentImageIndex + 1)} of ${this.album.length}`;
  }

  private _changeImage(newIndex: number): void {
    this.currentImageIndex = newIndex;
    this._hideImage();
    this._registerImageLoadingEvent();
    this._lightboxEvent.broadcastLightboxEvent({ id: LIGHTBOX_EVENT.CHANGE_PAGE, data: newIndex });
  }

  private _hideImage(): void {
    this.ui.showReloader = true;
    this.ui.showArrowNav = false;
    this.ui.showLeftArrow = false;
    this.ui.showRightArrow = false;
    this.ui.showPageNumber = false;
    this.ui.showCaption = false;
  }

  private _updateNav(): void {
    let alwaysShowNav = false;

    // check to see the browser support touch event
    try {
      this._documentRef.createEvent('TouchEvent');
      alwaysShowNav = !!(this.options.alwaysShowNavOnTouchDevices);
    } catch (e) {
      // noop
    }

    // initially show the arrow nav
    // which is the parent of both left and right nav
    this._showArrowNav();
    if (this.album.length > 1) {
      if (this.options.wrapAround) {
        if (alwaysShowNav) {
          // alternatives this.$lightbox.find('.lb-prev, .lb-next').css('opacity', '1');
          this._rendererRef.setElementStyle(this._leftArrowElem.nativeElement, 'opacity', '1');
          this._rendererRef.setElementStyle(this._rightArrowElem.nativeElement, 'opacity', '1');
        }

        // alternatives this.$lightbox.find('.lb-prev, .lb-next').show();
        this._showLeftArrowNav();
        this._showRightArrowNav();
      } else {
        if (this.currentImageIndex > 0) {
          // alternatives this.$lightbox.find('.lb-prev').show();
          this._showLeftArrowNav();
          if (alwaysShowNav) {
            // alternatives this.$lightbox.find('.lb-prev').css('opacity', '1');
            this._rendererRef.setElementStyle(this._leftArrowElem.nativeElement, 'opacity', '1');
          }
        }

        if ((this.currentImageIndex < this.album.length - 1) || (this.currentImageIndex >= this.album.length - 1 && this.options.allowRequestAlbum)) {
          // alternatives this.$lightbox.find('.lb-next').show();
          this._showRightArrowNav();
          if (alwaysShowNav) {
            // alternatives this.$lightbox.find('.lb-next').css('opacity', '1');
            this._rendererRef.setElementStyle(this._rightArrowElem.nativeElement, 'opacity', '1');
          }
        }
      }
    }
  }

  private _showLeftArrowNav(): void {
    this.ui.showLeftArrow = true;
  }

  private _showRightArrowNav(): void {
    this.ui.showRightArrow = true;
  }

  private _showArrowNav(): void {
    this.ui.showArrowNav = (this.album.length !== 1);
  }

  private _enableKeyboardNav(): void {
    this._event.keyup = this._rendererRef.listenGlobal('document', 'keyup', (event: any) => {
      this._keyboardAction(event);
    });
  }

  private _disableKeyboardNav(): void {
    if (this._event.keyup) {
      this._event.keyup();
    }
  }

  private _keyboardAction($event: any): void {
    const KEYCODE_ESC = 27;
    const KEYCODE_LEFTARROW = 37;
    const KEYCODE_RIGHTARROW = 39;
    const keycode = $event.keyCode;
    const key = String.fromCharCode(keycode).toLowerCase();

    if (keycode === KEYCODE_ESC || key.match(/x|o|c/)) {
      this._lightboxEvent.broadcastLightboxEvent({ id: LIGHTBOX_EVENT.CLOSE, data: null });
    } else if (key === 'p' || keycode === KEYCODE_LEFTARROW) {
      this.prevImage();
    } else if (key === 'n' || keycode === KEYCODE_RIGHTARROW) {
      this.nextImage();
    }
  }

  private _getCssStyleValue(elem: any, propertyName: string): number {
    return parseFloat(this._windowRef
      .getComputedStyle(elem.nativeElement, null)
      .getPropertyValue(propertyName));
  }

  private _onReceivedEvent(event: IEvent): void {
    switch (event.id) {
      case LIGHTBOX_EVENT.CLOSE:
        this._end();
      break;
      default:
      break;
    }
  }

  private _close() {
    this._lightboxEvent.broadcastLightboxEvent({ id: LIGHTBOX_EVENT.CLOSE, data: null });
  }

  get currentThot(){
    if (this.album && this.currentImageIndex !== null) {
      return this.album[this.currentImageIndex].thot;
    }
    return null;
  }

}
