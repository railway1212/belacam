import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {BelaResponse, ExThought, GeoLocation, GeoPlace, LikeBelaResult, Thought, User} from "../app.model";
import {EventService} from "../core/event.service";
import { AuthService } from "../core/auth.service";
import {BelaApiService} from "../core/bela-api.service";
import {InsufficientBelaComponent} from "../insufficient-bela/insufficient-bela.component";
import {BelaEventType} from "../core/event.types";
import {bool} from "aws-sdk/clients/signer";

declare var jQuery:any;

@Component({
  selector: 'app-post-box',
  templateUrl: './post-box.component.html',
  styleUrls: ['./post-box.component.css']
})
export class PostBoxComponent implements OnInit {

  @Input() thot: Thought;
  @Input() user: User;
  @Input() index: number;
  @Output('delete') deleteEvent: EventEmitter<Thought> = new EventEmitter<Thought>();
  @Output('openThotBox') openThotBoxEvent: EventEmitter<number> = new EventEmitter<number>();
  @ViewChild('insufficientMessage') _insufficientMessage: InsufficientBelaComponent;

  csrf="";

  constructor(private apiService: BelaApiService,
              private eventService: EventService,
              private authService: AuthService,) { }

  ngOnInit() {
    this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
  }

  showImage() {
    return this.thot.image && !this.thot.video;
  }

  showVideo() {
    if (this.thot.video) {
      return this.thot.video + '#t=0.1';
    } else {
      return false;
    }

  }

  like(){
    let post: ExThought = this.thot as ExThought;

    if(!post.is_liking && !post.is_liked && post.user.id !== this.user.id) {

      post.is_liking = true;
      post.is_liked = true;

      this.apiService.like(post).then((res: BelaResponse) => {
        if(res.error === LikeBelaResult.E_INSUFFICIENT_COIN) {
          this._insufficientMessage.show();
        } else {
          let result = res.result;
          if(result.bela) {
            post.likes.push(result.bela);

            post.belas = result.total_bela;
            post.earnings = result.total_earning;
            post.is_liked = result.liked;
            post.is_liking = false;
            post.total_likes_count = result.total_likes;
          }

          if(result.user) {
            this.user.user1 = result.user;
            this.authService.updateUser(this.user);

            this.eventService.fireEvent({
              type: BelaEventType.LIKE_POST,
              value: this.user
            });
          }
        }
      }).catch(() => {
        post.is_liked = false;
        post.is_liking = false;
      });
    }
  }

  delete(){
    this.apiService.deleteFeed(this.thot.id).then( ()=>{
      this.deleteEvent.emit(this.thot);
    });
  }

  deleteAdmin(){
    this.apiService.deleteAdminFeed(this.thot.id).then( ()=>{
      this.deleteEvent.emit(this.thot);
    }).catch(()=>{
      alert("Can't delete the post. Try again later");
    });
  }

  report(){
    this.apiService.report(this.thot.id).then( ()=>{
      this.deleteEvent.emit(this.thot);
    });
  }

  blockUser(){
    this.apiService.blockUser(this.thot.user.id).then(listResp=>{
      window.location.reload();
    });
  }

  followUser() {
    this.apiService.follow(this.thot.user, this.csrf).then(listResp=>{
      window.location.reload();
    });
  }

  unfollowUser() {
    this.apiService.unfollow(this.thot.user, this.csrf).then(listResp => {
      window.location.reload();
    })
  }

  editable(state){
    this.thot.editable = state;
    if(state) {
      this.thot.tempContent = this.thot.content;
    } else {
      this.thot.tempContent = '';
    }
  }

  editableLocation(state){
    this.thot.editableLocation = state;
    if(state) {
      this.thot.tempLocation = this.thot.location;
    } else {
      this.thot.tempLocation = '';
    }
  }

  update() {
    this.apiService.updateCaption(this.thot.id, this.thot.tempContent).then( result=>{
      if(result['status'] == "success") {
        this.thot.content = result['result'];
        this.thot.editable = false;
      } else {
        alert("Something went wrong. Please try again later!");
      }
    });
  }

  updateLocation() {
    this.apiService.updateLocation(this.thot.id, this.thot.tempLocation).then( result=>{
      if(result['status'] == "success") {
        this.thot.location = result['result'];
        this.thot.editableLocation = false;
      } else {
        alert("Something went wrong. Please try again later!");
      }
    });
  }

  openThotBox(): void {
    this.openThotBoxEvent.emit(this.index);
  }

  get mapLink() {
    let place = this.thot.location;
    if (place) {
      return null;
    } else {
      return null;
    }
  }
}
