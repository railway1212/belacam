import { Component, OnInit, Input } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {GrowthStatistics} from "../app.model";

@Component({
  selector: 'app-trending',
  templateUrl: './trending.component.html',
  styleUrls: ['./trending.component.css']
})
export class TrendingComponent implements OnInit {
  @Input() trending = [];
  ngOnInit() {
  }

}

@Component({
  selector: 'app-suggested',
  templateUrl: './suggested.component.html'
})
export class SuggestedComponent implements OnInit {

  @Input() suggested = [];
  @Input() earners = [];

  ngOnInit() {
  }

}


@Component({
  selector: 'app-growth',
  templateUrl: './growth.component.html'
})
export class GrowthComponent implements OnInit {
  @Input() growth: GrowthStatistics = new GrowthStatistics();

  ngOnInit() {
  }

}
