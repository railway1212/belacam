import { Component, NgZone, OnInit, ElementRef, ViewChild, Renderer, Renderer2, OnDestroy } from '@angular/core';
import { BelaApiService } from './core/bela-api.service';
import { User, Thought } from './app.model';
import { NavigationEnd, NavigationStart, Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { EventService } from "./core/event.service";
import { Subscription } from "rxjs/Subscription";
import { BelaEvent, BelaEventType } from "./core/event.types";
import { LoaderService } from "./core/loader.service";

declare var jQuery:any;
declare var userLanguage:string;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [BelaApiService, LoaderService]
})
export class AppComponent implements OnInit, OnDestroy {
  subscription: Subscription;

  constructor(
    private loaderService: LoaderService,
    private router: Router,
    private renderer: Renderer2,
    private belaservice: BelaApiService,
    private eventService: EventService,
    translate: TranslateService) {

    if(userLanguage == ""){
      translate.use("en");
    } else {
      translate.use(userLanguage);
    }


    router.events.subscribe((val) => {
      if(val instanceof NavigationStart) {
        renderer.removeClass(document.body, 'modal-open');
        renderer.removeStyle(document.body, 'top');
      }
      if(val instanceof NavigationEnd) {
        jQuery('html, body').animate({scrollTop:0}, '400');
      }
    });
  }

  ngOnInit() {
    this.subscription = this.eventService.subscribe({
      next: (event: BelaEvent) => {
        if(event.type == BelaEventType.ADDED_POST) {
          if(this.needNavigateOnPost()) {
            this.router.navigate(['/feed']);
          }
        }
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  needNavigateOnPost() {
    return this.router.url !== '/feed' && this.router.url.indexOf('user/') === -1;
  }


}
