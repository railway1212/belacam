import { Component, OnInit, Input } from '@angular/core';
declare var jQuery:any;
import {User} from "../app.model";
import { BelaApiService } from '../core/bela-api.service';

@Component({
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.css'],
})
export class DepositComponent {


  user = new User();
  deposit_address = "";
  processing = false;

  constructor(private appService:BelaApiService) {
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    if(this.user.user1.belacoin_address) {
      this.deposit_address = this.user.user1.belacoin_address;
    }
  }

  request_deposit_address() {
    this.processing = true;
    this.appService.requestDepositAddress().then( (resp)=>{
      if(resp.status == "error") {
        alert(resp.content);
      } else {
        this.deposit_address = this.user.user1.belacoin_address = resp.content
        localStorage.setItem('user', JSON.stringify(this.user));
      }
      this.processing = false;
    }).catch(err => {
      console.error(err);
      alert("Server Error!");
      this.processing = false;
    });
  }


}
