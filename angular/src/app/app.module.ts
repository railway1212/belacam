import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule }   from '@angular/router';
import { HttpModule, XSRFStrategy, CookieXSRFStrategy, JsonpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { RECAPTCHA_SETTINGS, RecaptchaSettings } from 'ng-recaptcha';
import { ImageCompressService,ResizeOptions,ImageUtilityService } from 'ng2-image-compress';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AppComponent } from './app.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { HeaderComponent } from './core/header/header.component';
import { UserComponent } from './user/user.component';
import { AppModuleRouting }     from './app.routing';
import { DiscoverComponent, PeopleComponent, TagsComponent, PhotoComponent } from './discover/discover.component';
import { ImageModalComponent } from './discover/modalimage.component';
import { TagfeedsComponent } from './tagfeeds/tagfeeds.component';
import { TrendingComponent, SuggestedComponent, GrowthComponent } from './trending/trending.component';
import { CommentComponent, DynamicHtmlComponent } from './comment/comment.component';
import { TimesinceComponent } from './timesince/timesince.component';
import { LoadingModule } from 'ngx-loading';
import { NgSpinKitModule } from 'ng-spin-kit'
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { HomefeedComponent } from './homefeed/homefeed.component';
import { SearchComponent,MobileSearchComponent } from './search/search.component';
import { PostingFormComponent, HeaderPostingFormComponent } from './posting-form/posting-form.component';
import { AboutComponent } from './about/about.component';
import { TermsComponent } from './terms/terms.component';
import { DirectoryComponent } from './directory/directory.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { DepositComponent } from './deposit/deposit.component';
import { WithdrawalComponent } from './withdrawal/withdrawal.component';
import { CommentFormComponent } from './comment/comment-form.component';
import { TypeaheadComponent } from './typeahead/typeahead.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NotificationPageComponent } from './notifications/notification-page.component';
import { DepositWithdrawalFormComponent } from './deposit-withdrawal-form/deposit-withdrawal-form.component';
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { ProfileBoxComponent } from './profile-box/profile-box.component';
import { LikesModalComponent } from './modals/likes-modal/likes-modal.component';
import { MainLayoutComponent } from './core/main-layout/main-layout.component';
import { SidebarLayoutComponent } from './core/sidebar-layout/sidebar-layout.component';
import { ViewsModalComponent } from './modals/views-modal/views-modal.component';
import { LikesModalWrapperComponent } from './modals/likes-modal-wrapper/likes-modal-wrapper.component';
import { ViewsModalWrapperComponent } from './modals/views-modal-wrapper/views-modal-wrapper.component';
import { CoreModule } from "./core/core.module";
import { LightboxOverlayComponent } from "./lightbox/lightbox-overlay.component";
import { InsufficientBelaComponent } from "./insufficient-bela/insufficient-bela.component";
import { LightboxComponent } from "./lightbox/lightbox.component";
import { AlbumLightboxComponent } from "./lightbox/album-lightbox.component";
import { PostBoxComponent } from './post-box/post-box.component';
import { TopSearchComponent } from './top-search/top-search.component';
import { SharedModule } from "./shared/shared.module";
import { ConfirmedComponent } from './confirmed/confirmed.component';
import { HelpComponent } from './help/help.component';
import { TradeComponent } from './trade/trade.component';
import { EarnFreeBelaComponent } from './earn-free-bela/earn-free-bela.component';
import { VerifyAccountComponent } from './verify-account/verify-account.component';
import { LeaderboardsComponent } from './leaderboards/leaderboards.component';
import { LandingPageComponent } from "./landing-page/landing-page.component";
import { PasswordResetPageComponent, PasswordResetConfirmPageComponent } from "./password-reset-page/password-reset-page.component";
import { SettingsComponent } from "./settings/settings.component";
import { ConfirmComponent } from "./settings/settings.component";


@NgModule({
  declarations: [
    AppComponent,
    NotificationsComponent,
    HeaderComponent,
    UserComponent,
    DiscoverComponent,
    PhotoComponent,
    TagsComponent,
    PeopleComponent,
    TagfeedsComponent,
    TrendingComponent,
    SuggestedComponent,
    GrowthComponent,
    CommentComponent,
    DynamicHtmlComponent,
    TimesinceComponent,
    ImageModalComponent,
    HomefeedComponent,
    SearchComponent,
    PostingFormComponent,
    HeaderPostingFormComponent,
    MobileSearchComponent,
    AboutComponent,
    PrivacyComponent,
    DepositComponent,
    WithdrawalComponent,
    TermsComponent,
    DirectoryComponent,
    CommentFormComponent,
    TypeaheadComponent,
    NotFoundComponent,
    NotificationPageComponent,
    DepositWithdrawalFormComponent,
    ProfileBoxComponent,
    LikesModalComponent,
    MainLayoutComponent,
    SidebarLayoutComponent,
    ViewsModalComponent,
    LikesModalWrapperComponent,
    ViewsModalWrapperComponent,
    LightboxComponent,
    LightboxOverlayComponent,
    AlbumLightboxComponent,
    InsufficientBelaComponent,
    PostBoxComponent,
    TopSearchComponent,
    ConfirmedComponent,
    HelpComponent,
    TradeComponent,
    EarnFreeBelaComponent,
    VerifyAccountComponent,
    LeaderboardsComponent,
    LandingPageComponent,
    PasswordResetPageComponent,
    PasswordResetConfirmPageComponent,
    SettingsComponent,
    ConfirmComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    HttpModule,
    AppModuleRouting,
    LoadingModule,
    InfiniteScrollModule,
    NgSpinKitModule,
    BootstrapModalModule,
    JsonpModule,
    HttpClientModule,
    CoreModule,
    SharedModule,
    RecaptchaModule.forRoot(),
    RecaptchaFormsModule,
  ],
  exports: [CommentComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    TypeaheadComponent,
    LikesModalComponent,
    LightboxOverlayComponent,
    LightboxComponent,
    AlbumLightboxComponent,
    ConfirmComponent
  ],
  providers: [
    ImageCompressService,
    ResizeOptions,
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: { siteKey: "6LfCvVUUAAAAAFLF7zPTJ0iaWF_kSrGySSvSd7uW" } as RecaptchaSettings,
    },
  ],
})
export class AppModule { }

