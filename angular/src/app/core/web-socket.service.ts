import { Injectable } from '@angular/core';
import {$WebSocket} from 'angular2-websocket/angular2-websocket'
import {Subject} from "rxjs";
import {Subscription} from "rxjs/Subscription";
import {NextObserver} from "rxjs/Observer";


@Injectable()
export class WebSocketService {
  socket: any = null;
  eventEmitter = new Subject();

  constructor() {
    let wsProtocol = 'ws://';

    if (location.protocol.indexOf('https') !== -1) {
      wsProtocol = 'wss://';
    }

    console.log(wsProtocol + location.host);

    this.socket = new $WebSocket(wsProtocol + location.host);

    this.socket.onMessage(
      (msg: MessageEvent)=> {
        console.log("New message arrived");
        console.log(msg.data);
        this.fireEvent(JSON.parse(msg.data));
      },
      {autoApply: false}
    );
  }

  subscribe(observer: NextObserver<any>): Subscription {
    return this.eventEmitter.subscribe(observer);
  }

  fireEvent(event: WebSocketEvent) {
    this.eventEmitter.next(event);
  }
}

export class WebSocketEvent {
  static EVENT_NEW_NOTIFICATION = 'NEW_NOTIFICATION';

  type: string;
}
