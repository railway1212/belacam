import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BelaApiService } from "./bela-api.service";
import { EventService } from "./event.service";
import {LightboxEvent, LightboxWindowRef} from "../lightbox/lightbox-event.service";
import {LightboxConfig} from "../lightbox/lightbox-config.service";
import {Lightbox} from "../lightbox/lightbox.service";
import {AuthService} from "./auth.service";
import {FilterSub4Pipe} from "./filter-sub4.pipe";
import {MessageTimePipe} from "./message-time.pipe";
import {TagifyPipe} from "./tagify.pipe";
import {TranslateCut} from "./translate-cut.pipe";
import {CookieXSRFStrategy, XSRFStrategy} from "@angular/http";
import {TranslatePoHttpLoader} from "@biesbjerg/ngx-translate-po-http-loader";
import {Http} from "@angular/http";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {Constants} from "../constants";
import {AWSS3ApiService} from "./awss3-api/awss3-api.service";
import {BootstrapModalModule} from "ng2-bootstrap-modal";
import {AvatarEditModalComponent} from "../modals/avatar-edit-modal/avatar-edit-modal.component";
import {ImageCropperModule} from "ng2-img-cropper";
import {CoverEditModalComponent} from "../modals/cover-edit-modal/cover-edit-modal.component";
import {WebSocketService} from "./web-socket.service";

declare var appDebug:string;

export function createTranslateLoader(http: Http) {
  if(appDebug.toLowerCase() == "true") {
    return new TranslatePoHttpLoader(http, '/static/assets/locale', '/LC_MESSAGES/js.po');
  } else {
    return new TranslatePoHttpLoader(http, Constants.default_remote_asset_url_base + '/assets/locale', '/LC_MESSAGES/js.po');
  }
}


@NgModule({
  imports: [
    CommonModule,
    BootstrapModalModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [Http]
      }
    }),
    ImageCropperModule
  ],
  declarations: [
    FilterSub4Pipe,
    MessageTimePipe,
    TagifyPipe,
    TranslateCut,
    AvatarEditModalComponent,
    CoverEditModalComponent
  ],
  exports: [
    FilterSub4Pipe,
    MessageTimePipe,
    TagifyPipe,
    TranslateCut,
    ImageCropperModule
  ],
  providers: [
    {
      provide: XSRFStrategy,
      useFactory: xsrfFactory
    },
    Lightbox,
    LightboxConfig,
    LightboxEvent,
    LightboxWindowRef,
    AuthService,
    EventService,
    BelaApiService,
    AWSS3ApiService,
    WebSocketService
  ],
  entryComponents: [
    AvatarEditModalComponent,
    CoverEditModalComponent
  ]
})
export class CoreModule {
  constructor(webService: WebSocketService) {
  }
}

export function xsrfFactory() {
  return new CookieXSRFStrategy('csrftoken', 'X-CSRFToken');
}
