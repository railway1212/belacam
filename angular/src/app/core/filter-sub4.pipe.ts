import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterSub4'
})
export class FilterSub4Pipe implements PipeTransform
{
  transform(obj:any, args?:any[]):any
  {
    if(Array.isArray(obj)) {
      return obj.slice(0, 4);
    } else {
      return obj;
    }
  }

}
