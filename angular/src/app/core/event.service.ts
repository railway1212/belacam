import { Injectable } from '@angular/core';
import { Subject, Observer } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import { NextObserver } from "rxjs/Observer";
import { Subscription } from "rxjs/Subscription";
import { BelaEvent } from "./event.types"
import {RewardTask} from "../app.model";
declare var jQuery:any;

@Injectable()
export class EventService {
  eventEmitter = new Subject();

  subscribe(observer: NextObserver<any>): Subscription {
    return this.eventEmitter.subscribe(observer);
  }

  fireEvent(event: BelaEvent) {
    this.eventEmitter.next(event);
  }

}
