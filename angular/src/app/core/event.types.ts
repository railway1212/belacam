import {IAlbum} from "../lightbox/lightbox-event.service";

class EvValue {
}

export class EvRequestAlbum extends EvValue {
  static DIRECTION_NEXT = 0;
  static DIRECTION_PREV = 0;

  album: IAlbum[];
  currentImageIndex: number;
  direction: number;
  requestID: any;
}

export class EvLoadedAlbum extends EvValue {
  album: IAlbum[];
  newImageIndex: number;
}


export class BelaEvent {
  type: BelaEventType;
  value: any;
}

export enum BelaEventType {
  ADDED_POST,
  DELETED_POST,
  LIKE_POST,
  REQUEST_NEXT_ALBUM,
  LOADED_NEXT_ALBUM,
  NO_NEXT_ALBUM,
  UPDATE_USER,
  NO_AUTH
}

