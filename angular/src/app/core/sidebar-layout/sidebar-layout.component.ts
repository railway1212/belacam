import { Component, OnDestroy, OnInit } from '@angular/core';
import { BelaApiService } from "../bela-api.service";
import { GrowthStatistics, User } from "../../app.model";
import { AuthService } from "../auth.service";
import { Subscription } from "rxjs/Subscription";
import { BelaEvent, BelaEventType } from "../event.types";
import {EventService} from "../event.service";

declare var jQuery:any;

@Component({
  selector: 'app-sidebar-layout',
  templateUrl: './sidebar-layout.component.html',
  styleUrls: ['./sidebar-layout.component.css'],
  providers: []
})
export class SidebarLayoutComponent implements OnInit, OnDestroy {

  requesting_user = new User();
  trending = [];
  suggested = [];
  earners = [];
  growth: GrowthStatistics = new GrowthStatistics();
  subscriptions: Subscription[] = [];

  constructor(
    private authService: AuthService,
    private appService: BelaApiService,
    private eventService: EventService
  ) { }

  ngOnInit() {
    this.requesting_user = this.authService.user;
    this.subscriptions.push(this.eventService.subscribe({
      next: (event: BelaEvent) => {
      if(event.type == BelaEventType.UPDATE_USER) {
        this.requesting_user = event.value;
      }
    }}));

    this.appService.getDiscover().then(discoverResp =>{
      this.trending = discoverResp.trending;
      this.suggested = discoverResp.suggested;
      this.earners = discoverResp.earners;
      jQuery('html, body').animate({scrollTop:0}, '400');
    });

    this.appService.getGrowthStatistics().then(growthResp =>{
      this.growth = growthResp;
    });
  }

  ngOnDestroy() {
    for(let subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  goToTop(e) {
    e.stopPropagation();
    e.preventDefault();
    jQuery('html, body').animate({scrollTop:0}, '400');
  }

}
