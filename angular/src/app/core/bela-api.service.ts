import {Injectable} from '@angular/core';
import {Headers, Http, Response, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {
  User,
  UserProfile,
  Follow,
  ListResult,
  Comment,
  Thought,
  Referrer,
  ReferrerStatistics,
  LikeBelaResult,
  GrowthStatistics,
  ReferralSetting,
  S3Resource,
  WithdrawFeeSetting,
  BelaResponse,
  BelaNotificationResp, PaginatedBelaNotificationResp
} from '../app.model';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {DiscoverResult} from "../discover/discover.model";
import {URLSearchParams} from "@angular/http/src/url_search_params";
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";
import {EventService} from "./event.service";
import {AuthService} from "./auth.service";
import {BelaEvent, BelaEventType} from "./event.types";
import {Router} from '@angular/router';

var vrouter;

function handleErrorPromise(error: any): Promise<any> {
  handleError(error);
  return Promise.reject(error.message || error);
}

function handleError(error: any) {
  if (error.status === 403) {
    vrouter.navigate(['']);
  }
}

@Injectable()
export class BelaApiService {
  responseData = {success: false};

  constructor(private http: Http,
              private router: Router,
              private eventService: EventService,
              private authService: AuthService) {
    vrouter = router;
  }

  signin(username: string, password: string): Promise<Response> {
    const signinUrl = '/api/sign-in/';
    const form: FormData = new FormData();

    form.append('username', username);
    form.append('password', password);
    return this.http.post(
      signinUrl,
      form,
    )
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  signup(email: string, username: string, password: string, captchaResponse: string): Promise<Response> {
    const signupUrl = '/api/sign-up/';
    const form: FormData = new FormData();

    form.append('email', email);
    form.append('username', username);
    form.append('password', password);
    form.append('password_confirm', password);
    form.append('g-recaptcha-response', captchaResponse);
    return this.http.post(
      signupUrl,
      form,
    )
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  reset_password(email: string): Promise<Response> {
    const resetPasswordUrl = '/api/reset-password';
    const form: FormData = new FormData();

    form.append('email', email);
    return this.http.post(
      resetPasswordUrl,
      form,
    )
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  check_reset_password_link(uidb64: string, token: string): Promise<Response> {
    const resetPasswordLinkUrl = '/api/check-reset-password-link';
    const form: FormData = new FormData();

    form.append('uidb64', uidb64);
    form.append('token', token);
    return this.http.post(
      resetPasswordLinkUrl,
      form,
    )
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  reset_password_confirm(password_new: string, password_new_confirm: string, uidb64: string, token: string): Promise<Response> {
    const resetPasswordConfirmUrl = '/api/reset-password-confirm';
    const form: FormData = new FormData();

    form.append('new_password1', password_new);
    form.append('new_password2', password_new_confirm);
    form.append('uidb64', uidb64);
    form.append('token', token);
    return this.http.post(
      resetPasswordConfirmUrl,
      form,
    )
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  logout(): Promise<Response> {
    const signinUrl = '/api/logout/';
    return this.http.post(
      signinUrl,
      {},
    )
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  getSettingDetails(): Promise<Response> {
    const settingDetailsUrl = '/api/setting_details/';
    return this.http.post(
      settingDetailsUrl,
      {},
    )
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  updateAccount(username: string, email: string, language: string, send_weekly_email:boolean, csrf_token: string): Promise<Response> {
    const updateAccountUrl = '/api/settings/update_account';
    const form: FormData = new FormData();

    form.append('username', username);
    form.append('email', email);
    form.append('language', language);
    form.append('send_weekly_email', send_weekly_email.toString());
    form.append('csrfmiddlewaretoken', csrf_token);

    return this.http.post(
      updateAccountUrl,
      form,
    )
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  resend_verify_email(): Promise<Response> {
    const resendVerifyEmailUrl = '/api/settings/resend_email';

    return this.http.post(
      resendVerifyEmailUrl,
      {},
    )
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  updatePassword(password_current: string, password_new: string, password_new_confirm: string, csrf_token: string): Promise<Response> {
    const updatePasswordUrl = '/api/settings/update_password';
    const form: FormData = new FormData();

    form.append('password_current', password_current);
    form.append('password_new', password_new);
    form.append('password_new_confirm', password_new_confirm);
    form.append('csrfmiddlewaretoken', csrf_token);

    return this.http.post(
      updatePasswordUrl,
      form,
    )
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  deleteAccount(): Promise<Response> {
    const deleteAccountUrl = '/api/settings/deleteAccount';

    return this.http.post(
      deleteAccountUrl,
      {},
    )
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  getUserDetail(username: string): Promise<User> {
    const userDataUrl = '/api/user/' + username + '/';
    return this.http.get(userDataUrl)
      .toPromise()
      .then(response => response.json() as User)
      .catch(handleErrorPromise);
  }


  updateUsername(user: User, username: string): Promise<User> {
    const nameSaveUrl = '/api/usernames/' + user.id + '/';
    return this.http.patch(
      nameSaveUrl,
      JSON.stringify({username: username}),
      new RequestOptions({headers: new Headers({'Content-Type': 'application/json'})})
    )
      .toPromise()
      .then(response => response.json() as User)
      .catch(handleErrorPromise);
  }

  updateUserProfile(user: User, data: any): Promise<UserProfile> {
    const url = `/api/user/${user.id}/profile/`;
    return this.http.patch(
      url,
      JSON.stringify(data),
      new RequestOptions({headers: new Headers({'Content-Type': 'application/json'})})
    )
      .toPromise()
      .then(response => response.json() as UserProfile)
      .catch(handleErrorPromise);
  }

  updateProfileCoverPicture(picUrl: string): Promise<BelaResponse> {
    const url = `/api/account/cover_pic/`;
    const form: FormData = new FormData();

    form.append('cover_pic', picUrl);

    return this.http.patch(url, form)
      .toPromise()
      .then(response => response.json() as BelaResponse)
      .then(response => this._handleNotifications(response))
      .catch(handleErrorPromise);
  }

  updateProfileAvatar(picUrl: string): Promise<BelaResponse> {
    const url = `/api/account/profile_pic/`;
    const form: FormData = new FormData();

    form.append('profile_pic', picUrl);
    return this.http.post(url, form)
      .toPromise()
      .then(response => response.json() as BelaResponse)
      .then(response => this._handleNotifications(response))
      .catch(handleErrorPromise);
  }

  createComment(post_id: number, text: string): Promise<Comment> {
    const addCommentUrl = `/api/post/${post_id}/comment/`;
    return this.http.post(
      addCommentUrl,
      JSON.stringify({"text": text}),
      new RequestOptions({headers: new Headers({'Content-Type': 'application/json'})})
    )
      .toPromise()
      .then(response => response.json() as Comment)
      .catch(handleErrorPromise);
  }

  follow(user: User, csrf: string): Promise<User> {
    const followUrl = '/api/ajaxFollow/';
    return this.http.post(followUrl, "want_to_follow=" + user.id, new RequestOptions({headers: new Headers({'Content-Type': 'application/x-www-form-urlencoded'})}))
      .toPromise()
      .then(response => response.json() as User)
      .catch(handleErrorPromise);
  }

  unfollow(user: User, csrf: string): Promise<User> {
    const unfollowUrl = '/api/ajaxUnfollow/';
    return this.http.post(unfollowUrl, "want_to_unfollow=" + user.id, new RequestOptions({headers: new Headers({'Content-Type': 'application/x-www-form-urlencoded'})}))
      .toPromise()
      .then(response => response.json() as User)
      .catch(handleErrorPromise);
  }

  like(post: Thought): Promise<BelaResponse> {

    const likeUrl = '/api/like/';
    return this.http.post(likeUrl, "pk=" + post.id, new RequestOptions({headers: new Headers({'Content-Type': 'application/x-www-form-urlencoded'})}))
      .toPromise()
      .then(response => response.json() as BelaResponse)
      .then(response => this._handleNotifications(response))
      .catch(handleErrorPromise);
  }

  getList(apiUrl, params?): Observable<ListResult> {
    return Observable.create((observer: Subject<ListResult>) => {
      this.http.get(apiUrl, {params: params})
        .subscribe(
          (data: Response) => {
            observer.next(data.json() as ListResult);
            observer.complete()
          },
          (err) => {
            handleError(err);
            observer.error(err);
          }
        )
    });
  }

  getTopSearchList(apiUrl, search_term = "", search_in = 'all'): Promise<ListResult> {
    return this.http.get(apiUrl, {params: {term: search_term, in: search_in}})
      .toPromise()
      .then(response => response.json() as ListResult)
      .catch(handleErrorPromise);
  }

  getPhotoList(apiUrl, param = 'hot'): Observable<ListResult> {
    let params = param ? {sort_by: param} : {};
    return this.getList(apiUrl, params);
  }

  deleteFeed(pk: number): Promise<Response> {
    const deleteUrl = '/api/delete/';
    return this.http.post(
      deleteUrl,
      "pk=" + pk,
      new RequestOptions({headers: new Headers({'Content-Type': 'application/x-www-form-urlencoded'})})
    )
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  deleteAdminFeed(pk: number): Promise<Response> {
    const deleteUrl = '/api/adminDelete/';
    return this.http.post(
      deleteUrl,
      "pk=" + pk,
      new RequestOptions({headers: new Headers({'Content-Type': 'application/x-www-form-urlencoded'})})
    )
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  report(pk: number): Promise<Response> {
    const reportUrl = '/api/report/';
    return this.http.post(
      reportUrl,
      "pk=" + pk,
      new RequestOptions({headers: new Headers({'Content-Type': 'application/x-www-form-urlencoded'})})
    )
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  blockUser(pk: number): Promise<Response> {
    const reportUrl = '/api/block/';
    return this.http.post(
      reportUrl,
      "pk=" + pk,
      new RequestOptions({headers: new Headers({'Content-Type': 'application/x-www-form-urlencoded'})})
    )
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  isBottomReached(): boolean {
    let bottom_reached = false;
    let windowHeight = "innerHeight" in window ? window.innerHeight
      : document.documentElement.offsetHeight;
    let body = document.body, html = document.documentElement;
    let docHeight = Math.max(body.scrollHeight,
      body.offsetHeight, html.clientHeight,
      html.scrollHeight, html.offsetHeight);
    let windowBottom = windowHeight + window.pageYOffset;
    if (docHeight - windowBottom < 1500) {
      bottom_reached = true;
    }
    return bottom_reached;
  }

  createFeed(username: string, data: any): Promise<BelaResponse> {
    const url = '/api/users/' + username + '/';
    return this.postData(url, data, null)
      .then(response => this._handleNotifications(response));
  }

  postData(url: string, data: any, headers: any): Promise<any> {
    let form = new FormData();
    for (let key in data) {
      form.append(key, data[key]);
    }

    return this.http.post(url, form, {headers: headers})
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  postWithImage(url: string, postData: any, imageParamKey: string, imageData: any, imageName: string, imageType: string) {

    let headers = new Headers();
    let formData: FormData = new FormData();
    formData.append(imageParamKey, this.dataURItoBlob(imageData, imageType), imageName);

    // For multiple files
    // for (let i = 0; i < files.length; i++) {
    //     formData.append(`files[]`, files[i], files[i].name);
    // }

    if (postData !== "" && postData !== undefined && postData !== null) {
      for (let property in postData) {
        if (postData.hasOwnProperty(property)) {
          formData.append(property, postData[property]);
        }
      }
    }

    return new Promise((resolve, reject) => {
      this.http.post('/api/' + url, formData, {
        headers: headers
      }).subscribe(
        res => {
          this.responseData = res.json();
          resolve(this.responseData);
        },
        error => {
          reject(error);
        }
      );
    });
  }


  postWithFile(url: string, postData: any, files: File[]) {
    let headers = new Headers();
    let formData: FormData = new FormData();
    formData.append('image', files[0], files[0].name);

    // For multiple files
    // for (let i = 0; i < files.length; i++) {
    //     formData.append(`files[]`, files[i], files[i].name);
    // }

    if (postData !== "" && postData !== undefined && postData !== null) {
      for (let property in postData) {
        if (postData.hasOwnProperty(property)) {
          formData.append(property, postData[property]);
        }
      }
    }
    return new Promise((resolve, reject) => {
      this.http.post('/api/' + url, formData, {
        headers: headers
      }).subscribe(
        res => {
          this.responseData = res.json();
          resolve(this.responseData);
        },
        error => {

          reject(error);
        }
      );
    });
  }

  dataURItoBlob(dataURI, type) {
    let binary = atob(dataURI.split(',')[1]);
    let array = [];
    for (let i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {type: type});
  }

  updateCaption(pk: number, caption: string): Promise<any> {
    const updateCaptionUrl = '/api/update_caption/';

    return this.http.post(updateCaptionUrl, JSON.stringify({
      "pk": pk,
      "caption": caption
    }), new RequestOptions({headers: new Headers({'Content-Type': 'application/json'})}))
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  updateLocation(pk: number, location: string): Promise<any> {
    const updateLocationUrl = '/api/update_location/';

    return this.http.post(updateLocationUrl, JSON.stringify({
      "pk": pk,
      "location": location
    }), new RequestOptions({headers: new Headers({'Content-Type': 'application/json'})}))
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  updateComment(comment: Comment, newData): Promise<any> {
    const url = `/api/comment/${comment.id}/`;

    return this.http.patch(url, JSON.stringify(newData),
      new RequestOptions({headers: new Headers({'Content-Type': 'application/json'})})
    )
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  deleteComment(comment: Comment): Promise<any> {
    const url = `/api/comment/${comment.id}/`;

    return this.http.delete(url, new RequestOptions({headers: new Headers({'Content-Type': 'application/json'})}))
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  searchPlace(params) {
    const url = `/api/places/`;

    return this.http.get(url, new RequestOptions({
      headers: new Headers({'Content-Type': 'application/json'}),
      params: params
    }))
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  getDiscover(): Promise<DiscoverResult> {
    const discoverDataUrl = '/api/discover/';

    return this.http.get(discoverDataUrl)
      .toPromise()
      .then(response => response.json() as DiscoverResult)
      .catch(handleErrorPromise);
  }

  getReferrer(): Promise<BelaResponse> {
    const referrerUrl = '/api/referrer/';

    return this.http.get(referrerUrl)
      .toPromise()
      .then(response => response.json() as BelaResponse)
      .catch(handleErrorPromise);
  }

  getReferrerStatistics(): Promise<ReferrerStatistics> {
    const url = '/api/referrer/statistics/';
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as ReferrerStatistics)
      .catch(handleErrorPromise);
  }

  getReferralSettings(): Promise<ReferralSetting[]> {
    const url = '/api/referral/settings/';

    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as ReferralSetting[])
      .catch(handleErrorPromise);
  }

  getGrowthStatistics(): Promise<GrowthStatistics> {
    const url = '/api/growth/';

    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as GrowthStatistics)
      .catch(handleErrorPromise);
  }

  getImagePostLink(fileName: string, fileType: string): Promise<S3Resource> {
    const url = '/api/get_s3_post';
    const options = {
      params: {
        'file-name': fileName,
        'file-type': fileType
      }
    };
    return this.http.get(url, options)
      .toPromise()
      .then(response => S3Resource.fromJson(response.json()) as S3Resource)
      .catch(handleErrorPromise);
  }

  getReferrerApplication(status?): Promise<BelaResponse> {
    const url = '/api/referrer/application/';
    const params = {
      "status": status
    };

    return this.http.get(url, {params: params})
      .toPromise()
      .then(response => response.json() as BelaResponse)
      .catch(handleErrorPromise);
  }

  createReferralApplication(content: string): Promise<BelaResponse> {
    const url = '/api/referrer/application/';
    let form: FormData = new FormData();
    form.append('content', content);

    return this.http.post(url, form)
      .toPromise()
      .then(response => response.json() as BelaResponse)
      .catch(handleErrorPromise);
  }

  createWithdrawRequest(address: string, amount: number): Promise<any> {
    const url = '/api/withdraw/';

    return this.http.post(url, {address, amount})
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  requestDepositAddress(): Promise<any> {
    const url = '/api/request_deposit_address/';

    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as any)
      .catch(handleErrorPromise);
  }

  getUnreadNotifications():Promise<BelaNotificationResp>{
    const notificationsUrl = '/api/notifications/live/';
    return this.http.get(notificationsUrl)
      .toPromise()
      .then(response => response.json() as BelaNotificationResp)
      .catch(handleErrorPromise);
  }

  markAllRead(): Promise<any> {
    const markAllReadUrl = '/api/mark_all_read';
    return this.http.get(markAllReadUrl)
      .toPromise()
      .then(response => response.json())
      .catch(handleErrorPromise);
  }

  markItRead(notification): Promise<any> {
    const markItReadUrl = '/api/mark_it_read/' + notification.id + "/";
    return this.http.get(markItReadUrl)
      .toPromise()
      .then(response => response.json())
      .catch(handleErrorPromise);
  }

  getNotificatons(url: string, params: any):Promise<PaginatedBelaNotificationResp> {
    return this.http.get(url, new RequestOptions({
      params: params
    }))
      .toPromise()
      .then(response => response.json() as PaginatedBelaNotificationResp)
      .catch(handleErrorPromise);
  }

  getWithdrawFeeSettings(): Promise<WithdrawFeeSetting[]> {
    const url = '/api/withdraw/settings/fee/';

    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as WithdrawFeeSetting[])
      .catch(handleErrorPromise);
  }

  requestPhoneVerification(phoneNumber): Promise<BelaResponse> {
    const url = '/api/account/verify_phone/';
    return this.http.post(url, {phone_number: phoneNumber})
      .toPromise()
      .then(response => response.json() as BelaResponse)
      .catch(handleErrorPromise)
  }

  verifySecurityCode(phoneNumber, securityCode): Promise<BelaResponse> {
    const url = '/api/account/verify_phone_confirm/';
    return this.http.post(url, {phone_number: phoneNumber, code: securityCode})
      .toPromise()
      .then(response => response.json() as BelaResponse)
      .catch(handleErrorPromise)
  }

  getRewardTasks(): Promise<any> {
    const url = '/api/reward/';
    return this.http.get(url)
      .toPromise()
      .then(response => response.json())
      .catch(handleErrorPromise)
  }

  _handleNotifications(resp: BelaResponse) {
    if (resp.notifications) {
      for (let notification of resp.notifications) {
        if (notification.type === 'complete_reward_task') {
          let belacoin = notification.belacoin;

          let user = this.authService.getUser();
          if (user && user.id) {
            user.user1.belacoin = belacoin;
            this.authService.updateUser(user);
          }
        }
      }
    }
    return resp;
  }

}
