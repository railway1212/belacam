import { Injectable } from '@angular/core';
import { Subject, Observer } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import { BelaApiService } from "./bela-api.service";
import { User } from "../app.model";
import { NextObserver } from "rxjs/Observer";
import { Subscription } from "rxjs/Subscription";
import { BelaEvent, BelaEventType } from "./event.types";
import {Http} from "@angular/http";
import {EventService} from "./event.service";

declare var jQuery:any;

@Injectable()
export class AuthService {
  isReady: boolean = false;
  user: User = new User();
  eventEmitter = new Subject();

  constructor(private http: Http, private eventService: EventService ) {

    this.loadUserDetail();
  }

  isServiceReady() {
    return this.isReady;
  }

  isLoggedIn() {
    let user_id = jQuery('input[name="requesting_user_id"]').val();
    return !!user_id;
  }

  getUser() {
    return this.user;
  }

  updateUser(userData) {
    localStorage.setItem('user', JSON.stringify(userData));

    this.user = userData;

    let event = new BelaEvent();
    event.type = BelaEventType.UPDATE_USER;
    event.value = this.user;
    this.eventService.fireEvent(event);
  }

  getUserId() {
    return jQuery('input[name="requesting_user_id"]').val();
  }

  getUserName() {
    return jQuery('input[name="requesting_user_username"]').val();
  }

  loadUserDetail() {
    let userId = jQuery('input[name="requesting_user_id"]').val();
    let userName = jQuery('input[name="requesting_user_username"]').val();

    if(userName) {
      const userDataUrl = '/api/user/'+userName +'/';
      return this.http.get(userDataUrl)
        .toPromise()
        .then(response => response.json() as User)
        .then(res=> {
          this.updateUser(res);
          this.isReady = true;
        })
        .catch((err => {
        }));
    } else {
      let event = new BelaEvent();
      event.type = BelaEventType.NO_AUTH;

      this.isReady = true;
      this.eventService.fireEvent(event);
      localStorage.removeItem('user');
    }
  }

  subscribe(observer: NextObserver<any>): Subscription {
    return this.eventEmitter.subscribe(observer);
  }

  logout() {
    jQuery('input[name="requesting_user_id"]').val('');
    jQuery('input[name="requesting_user_username"]').val('');
    let event = new BelaEvent();
    event.type = BelaEventType.NO_AUTH;

    this.isReady = true;
    delete this.user;
    this.user = new User();
    this.eventEmitter.next(event);
    localStorage.removeItem('user');

  }
}
