import { TestBed, inject } from '@angular/core/testing';

import { AWSS3ApiService } from './awss3-api.service';

describe('AWSS3ApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AWSS3ApiService]
    });
  });

  it('should be created', inject([AWSS3ApiService], (service: AWSS3ApiService) => {
    expect(service).toBeTruthy();
  }));
});
