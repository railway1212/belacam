import { Injectable } from '@angular/core';
import {Headers, Http} from "@angular/http";
import { S3Resource } from "../../app.model";
import * as AWS from 'aws-sdk';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from "../../../environments/environment";

@Injectable()
export class AWSS3ApiService {

  constructor(
    private http: Http,
    private sanitizer:DomSanitizer,
  ) { }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

  uploadFileToS3(_resource: S3Resource, _file: File) {
    return this.uploadBlobToS3(_resource, _file, _file.name);
  }

  uploadBlobToS3(_resource: S3Resource, data: Blob, filename: string) {
    const url = _resource.data.url;

    let headers = new Headers();
    headers.append("x-amz-acl", "public-read");
    let formData:FormData = new FormData();

    formData.append('AWSAccessKeyId', _resource.data.fields.accessKeyId);
    formData.append('Content-Type', _resource.data.fields.contentType);
    formData.append('key', _resource.data.fields.key);
    formData.append('policy', _resource.data.fields.policy);
    formData.append('signature', _resource.data.fields.signature);
    formData.append('file', data, filename);

    return this.http.post(url, formData, {
      headers: headers
    }).toPromise()
      .then((res) => {
        if(res.status == 204) {
          return true
        }
      })
      .catch(this.handleError)
  }

  putVideoToS3(_resource: S3Resource, data: Blob) {

    AWS.config.update({
      credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: "us-east-1:e43cb9f0-a254-4001-9e54-339499aee9ba"
      }),
      'region': 'us-east-1'
    });

    const s3 = new AWS.S3({
      apiVersion: '2006-03-01',
      params: {Bucket: environment.bucket}
    });

    console.log('environment.bucket: ', environment.bucket);
    const params = {
      Body: data,
      Bucket: environment.bucket,
      Key: _resource.data.fields.key,
      ACL: 'public-read'
    };
    console.log('starting s3 putobject');
    console.log('params of s3 putobject: ', params);

    let putObjectPromise = s3.putObject(params).promise();

    return putObjectPromise.then((res) => {
      console.log('Success');
      return true
    }).catch(this.handleError);

  }
}
