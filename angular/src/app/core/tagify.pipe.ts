import {Pipe, PipeTransform, SecurityContext} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import * as _ from "lodash";
import '../string';
import * as OuterXRegExp from 'xregexp';

@Pipe({
  name: 'tagify'
})
export class TagifyPipe implements PipeTransform
{
  value = '';
  constructor(private _sanitizer:DomSanitizer){}

  convert_http(match, p1, p2, p3, offset, string){
    let s = p2;
    s = s.replace('https://', '').replace('http://', '');
    return p1 + '<a target="_blank" href="http://' + s + '">' + p2 + '</a>' + p3;
  }

  append_reverse(match, p1, offset, string) {
    return (' <a class="ng-link" href="/discover/' + p1.reverse() + '">#' + p1.reverse() + '</a>').reverse()
  }

  transform(obj:any, args?:any[]):any
  {
    if(!obj) {
      obj = "";
    }

    let result = _.escape(" " + obj); // add space character in the front to match such case : "#stunning
    let pat1: RegExp = OuterXRegExp("([\\p{L}0-9*-_]+)#(?!&)", 'g');
    let pat2: RegExp = OuterXRegExp("@([\\p{L}0-9*-_]+)",'g');
    let pat3: RegExp;                 // web url pattern
    pat3 = /(^|[ ,.!?()"\\\n-]|&quot;|&#39;)((?:https?:\/\/)?(?:[\w-]+\.)+(?:[a-z]{2,6}\.?)[\/\w.?=-]*\/?(?:\?[\w.=*&]+)?)($|[ ,.!?()"\\\n-]|&quot;|&#39;)/g;

    result = OuterXRegExp.replace(result.reverse(), pat1, this.append_reverse).reverse();
    result = OuterXRegExp.replace(result, pat2, ' <a class="ng-link" href="/user/$1">@$1</a>');
    result = result.replace(pat3, this.convert_http);
    return this._sanitizer.sanitize(SecurityContext.HTML, result);
  }

}

