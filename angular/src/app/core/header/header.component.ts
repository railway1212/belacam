import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import { BelaApiService } from '../bela-api.service';
import {GrowthStatistics, User} from '../../app.model';
import { SearchService } from '../../search/search.service';

import { Router } from '@angular/router';
import { AuthService } from "../auth.service";
import { Subscription } from "rxjs/Subscription";
import { BelaEvent, BelaEventType } from "../event.types";
import {WebSocketEvent, WebSocketService} from "../web-socket.service";
import {EventService} from "../event.service";
import { TranslateService } from '@ngx-translate/core';

declare var jQuery:any;
declare var userLanguage:string;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [SearchService]
})
export class HeaderComponent implements OnInit, OnDestroy {
  unread_count = 0;
  notification_list = [];
  requesting_user_id="";
  requesting_user_username="";
  requesting_user = new User();
  trending = [];
  suggested = [];
  earners = [];
  growth: GrowthStatistics = new GrowthStatistics();
  loading = true;
  subscriptions: Subscription[] = [];

  constructor(
    private authService: AuthService,
    private eventService: EventService,
    private webSocketService: WebSocketService,
    private apiService:BelaApiService,
    private router: Router,
    private translate: TranslateService) { }

  ngOnInit() {
    this.requesting_user_id = this.authService.getUserId();
    this.requesting_user_username = this.authService.getUserName();

    this.apiService.getDiscover().then(discoverResp =>{
      this.trending = discoverResp.trending;
      this.suggested = discoverResp.suggested;
      this.earners = discoverResp.earners;

    });

    this.requesting_user = this.authService.user;
    this.loading = !this.authService.isServiceReady();

    this.subscriptions.push(this.eventService.subscribe({
      next: (event: BelaEvent) => {
        if(event.type == BelaEventType.UPDATE_USER) {
          this.requesting_user = event.value;
          this.loading = false;
        }
      }
    }));

    this.subscriptions.push(this.webSocketService.subscribe( {
      next:(event: WebSocketEvent) => {
        if(event.type === WebSocketEvent.EVENT_NEW_NOTIFICATION) {
          this.getUnreadNotifications();
        }
      }
    }));

    this.getUnreadNotifications();
  }

  ngOnDestroy() {
    for(let subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  getUnreadNotifications() {
    this.apiService.getUnreadNotifications().then(result =>{
      this.unread_count = result.unread_count;
      this.notification_list = result.notifications_list;
    });
  }

  markAllRead(){
    this.apiService.markAllRead().then(result =>{
      this.getUnreadNotifications();
    })
  }
  goToAction(notification){
    this.apiService.markItRead(notification).then(result =>{
      if(notification.unread == true){
        notification.unread=false;
        this.unread_count = this.unread_count-1;
      }
    });
    if(notification.action == "comment"|| notification.action == "bella")
      this.router.navigate(['/post',notification.username, notification.action_id]);
    else if(notification.action == "user"){
      this.router.navigate(['/user', notification.username]);
    }
  }

  handleClickAvatar(event, notification) {
    this.router.navigate(['user/', notification.actor])
  }

  logout() {
    this.apiService.logout().then((result:any) => {
      if(result["status"] == "success") {
        this.authService.logout();

        if(userLanguage != "en") {
          this.translate.resetLang(userLanguage);
          this.translate.use("en");
          userLanguage = "en";
        }

        this.router.navigate(['']);
      } else {
        alert("Logout failed!");
      }
    });
  }
}
