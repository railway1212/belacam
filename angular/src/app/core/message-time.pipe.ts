import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'messageTime'
})
export class MessageTimePipe implements PipeTransform
{
  value = new Date();
  transform(obj:any, args?:any[]):any
  {
    let result = '';
    let from = obj;
    let to;

    if(args) {
      from = args;
      to = obj;
    } else {
      from = obj;
      to = new Date();
    }

    if (from instanceof Date)
    {
      this.value = from;
    }
    else{
      this.value = new Date(from);
    }

    let now = to.getTime();
    let delta = (now - this.value.getTime()) / 1000;


    if (delta < 10)
    {
      result = 'just now';
    }
    else if (delta < 60)
    { // sent in last minute
      result =  Math.floor(delta) + ' seconds ago';
    }
    else if (delta < 3600)
    { // sent in last hour
      result = Math.floor(delta / 60) + ' minutes ago';
    }
    else if (delta < 86400)
    { // sent on last day
      result = Math.floor(delta / 3600) + ' hours ago';
    }
    else
    { // sent more than one day ago
      result = Math.floor(delta / 86400) + ' days ago';
    }

    return result;
  }

}
