import {
  Component,
  OnInit,
  Input,
  Renderer2,
  ViewChild,
  ElementRef,
  ContentChildren,
  QueryList,
  Directive
} from '@angular/core';
import {BelaApiService} from '../core/bela-api.service';
import {User} from '../app.model';
import {Router} from "@angular/router";

import {Thought} from '../app.model';
import {Observable} from "rxjs/Observable";
import {Subscriber} from "rxjs/Subscriber";
import {Constants} from "../constants";

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css'],
  providers: []
})
export class CommentComponent implements OnInit {
  @Input() thot = new Thought();
  @Input() hideForm: boolean = false;
  public loading = false;
  requesting_user = new User();
  comment_text: string = "";
  comment_in_edit: Comment = null;
  timeService = Constants.timeService;

  constructor(private appService: BelaApiService) {
    this.comment_in_edit = null;
  }

  ngOnInit() {

    this.thot.displayed_comments = this.thot.comments.slice(-2);
    this.requesting_user = JSON.parse(localStorage.getItem('user'));
  }

  onAddCommentToPost(comment) {
    this.thot.displayed_comments.push(comment);
    this.thot.comments.push(comment);
  }

  viewAllComments() {
    Thought.updateDisplayedComments(this.thot, true);
  }

  hideComments() {
    Thought.updateDisplayedComments(this.thot, false);
    this.comment_in_edit = null;
  }

  enterEditMode(comment) {
    this.comment_in_edit = comment;
    this.comment_text = comment.text;
  }

  finishEdit(comment) {
    this.appService.updateComment(comment, {text: this.comment_text}).then(() => {
      this.comment_in_edit.text = this.comment_text;
      this.comment_in_edit = null;
    }).catch(() => {

    });
  }

  cancelEdit(comment) {
    this.comment_in_edit = null;
  }

  deleteComment(comment) {
    this.appService.deleteComment(comment).then(() => {
      this.comment_in_edit = null;

      // TODO: remove comment from displayed_comments list as well as thot comments

      let idxFrom = this.thot.comments.indexOf(comment);
      if(idxFrom != -1) {
        this.thot.comments.splice(idxFrom, 1);
      }

      Thought.updateDisplayedComments(this.thot, this.thot.show_all_comment);
    }).catch(() => {

    });
    this.comment_in_edit = null;
  }
}

@Component({
  selector: 'app-dynamic-html',
  template: '<ng-content></ng-content>'
})

export class DynamicHtmlComponent implements OnInit {

  private clickListeners: Function[] = [];

  constructor(private router: Router, private el: ElementRef, private renderer: Renderer2) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() { // for searching a components template
    const anchorNodes: NodeList = this.el.nativeElement.querySelectorAll('a.ng-link');

    const anchors: Node[] = Array.from(anchorNodes);

    anchors.forEach(anchor => {
      let listener = this.renderer.listen(anchor, 'click', e => {
        let href;

        e.preventDefault();

        if(e.srcElement) { // IE specific
          href = e.srcElement.getAttribute('href');
        } else {
          href = e.target.getAttribute('href');
        }

        this.router.navigateByUrl(href);
      });
      this.clickListeners.push(listener);

    });
  }

}
