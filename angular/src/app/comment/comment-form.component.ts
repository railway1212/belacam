import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Thought, User} from "../app.model";
import {BelaApiService} from "../core/bela-api.service";
import {SearchService} from "../search/search.service";
import {Comment} from '../app.model';
import {Observable} from "rxjs/Observable";
import {TypeaheadComponent} from "../typeahead/typeahead.component";

declare var jQuery: any;

@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.css'],
  providers: [ SearchService ]
})
export class CommentFormComponent implements OnInit {
  @Input() thot = new Thought();

  @Output() success: EventEmitter<Comment> = new EventEmitter<Comment>();

  @ViewChild('commentText') commentText: TypeaheadComponent;

  requesting_user = new User();
  comment_text = "";
  comment_target: any;
  alert_message = "";
  public loading = false;

  constructor(private appService: BelaApiService, private searchService: SearchService) { }

  ngOnInit() {
    this.requesting_user = JSON.parse(localStorage.getItem('user'));
  }

  addCommentToPost() {
    this.loading = true;
    if (this.comment_text.trim() != "") {
      this.appService.createComment(this.thot.id, this.comment_text.trim()).then((commentResp) => {
        this.loading = false;
        this.success.emit(commentResp);
        // this.thot.displayed_comments.unshift(commentResp);
        this.comment_text = "";
      }, (err) => {
        this.loading = false;
        this.alert_message = "Your account is now suspended for 24 hours because you commented too often.";
      });
    } else {
      this.loading = false;
    }
  }
  
}
