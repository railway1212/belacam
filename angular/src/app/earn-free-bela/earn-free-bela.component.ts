import { Component, OnInit } from '@angular/core';
import {RewardTask, User} from "../app.model";
import {AuthService} from "../core/auth.service";
import {BelaEvent, BelaEventType} from "../core/event.types";
import {Subscription} from "rxjs/Subscription";
import {BelaApiService} from "../core/bela-api.service";
import {EventService} from "../core/event.service";

@Component({
  selector: 'app-earn-free-bela',
  templateUrl: './earn-free-bela.component.html',
  styleUrls: ['./earn-free-bela.component.css']
})
export class EarnFreeBelaComponent implements OnInit {

  public tasks: RewardTask[];
  requesting_user = new User();
  subscriptions: Subscription[] = [];

  constructor(private authService: AuthService,
              private eventService: EventService,
              private belaApiService: BelaApiService) {
  }

  ngOnInit() {
    this.requesting_user = this.authService.user;
    this.assignTasks();

    this.subscriptions.push(this.eventService.subscribe({
      next: (event: BelaEvent) => {
        if(event.type == BelaEventType.UPDATE_USER) {
          this.requesting_user = event.value;
          this.assignTasks();
        }
      }
    }));
  }

  ngOnDestroy() {
    for(let subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  assignTasks() {
    if(!this.requesting_user.user1.phone_number) {
      this.tasks = []
    } else {
      this.belaApiService.getRewardTasks().then((res) => {
        this.tasks = res.results;
      })
    }
  }
}
