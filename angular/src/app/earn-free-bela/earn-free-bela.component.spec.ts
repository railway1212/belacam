import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EarnFreeBelaComponent } from './earn-free-bela.component';

describe('EarnFreeBelaComponent', () => {
  let component: EarnFreeBelaComponent;
  let fixture: ComponentFixture<EarnFreeBelaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EarnFreeBelaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EarnFreeBelaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
