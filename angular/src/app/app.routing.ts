import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserComponent }   from './user/user.component';
import { DiscoverComponent} from './discover/discover.component';
import { TagfeedsComponent } from './tagfeeds/tagfeeds.component';
import { TimesinceComponent } from './timesince/timesince.component';
import { HomefeedComponent } from './homefeed/homefeed.component';
import { SearchComponent } from './search/search.component';
import { AboutComponent } from './about/about.component';
import { TermsComponent } from './terms/terms.component';
import { DirectoryComponent } from './directory/directory.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { DepositComponent } from './deposit/deposit.component';
import { WithdrawalComponent } from './withdrawal/withdrawal.component';
import { NotFoundComponent } from "./not-found/not-found.component";
import { NotificationPageComponent } from "./notifications/notification-page.component";
import { TopSearchComponent } from "./top-search/top-search.component";
import { MainLayoutComponent } from "./core/main-layout/main-layout.component";
import { SidebarLayoutComponent } from "./core/sidebar-layout/sidebar-layout.component";
import { ConfirmedComponent } from "./confirmed/confirmed.component";
import { HelpComponent } from "./help/help.component";
import { TradeComponent } from "./trade/trade.component";
import { VerifyAccountComponent } from "./verify-account/verify-account.component";
import { LeaderboardsComponent } from "./leaderboards/leaderboards.component";
import { LandingPageComponent } from "./landing-page/landing-page.component";
import { SettingsComponent } from "./settings/settings.component";
import { PasswordResetPageComponent, PasswordResetConfirmPageComponent } from "./password-reset-page/password-reset-page.component";

const routes: Routes = [
  { path: '', component: LandingPageComponent },
  { path: '', component: MainLayoutComponent, children: [
      { path: '', component: SidebarLayoutComponent, children: [
          { path: 'feed', component: HomefeedComponent },
          { path: 'verify', component: VerifyAccountComponent },
          { path: 'confirmed', component: ConfirmedComponent },
          { path: 'deposit', component:DepositComponent },
          { path: 'discover', component:DiscoverComponent },
          { path: 'discover/:tagname', component:TagfeedsComponent },
          { path: 'feed', component: HomefeedComponent },
          { path: 'help', component: HelpComponent },
          { path: 'leaderboards', component: LeaderboardsComponent },
          { path: 'notifications', component:NotificationPageComponent },
          { path: 'post/:username/:post_id', component:TimesinceComponent },
          { path: 'referral', loadChildren: 'app/referral/referral.module#ReferralModule' },
          { path: 'top_search', component:TopSearchComponent },
          { path: 'top_search/:tagname', component:TopSearchComponent },
          { path: 'trade', component: TradeComponent },
          { path: 'withdrawal', component:WithdrawalComponent },
        ]
      },
      { path: 'password_reset', component:PasswordResetPageComponent },
      { path: 'reset/:uidb64/:token', component:PasswordResetConfirmPageComponent },
      { path: 'settings', component:SettingsComponent },
      { path: 'directory', component:DirectoryComponent },
      { path: 'privacy', component:PrivacyComponent },
      { path: 'about', component:AboutComponent },
      { path: 'terms', component:TermsComponent },
      { path: 'user/:username', component: UserComponent },

    ]},
  { path: '404', component:NotFoundComponent },
  { path: '**', redirectTo: '/404'}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppModuleRouting {}
