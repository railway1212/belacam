import { User, Tag } from '../app.model';


export class TagClass{
	id:number;
	tag:string;
}


export class People{
	person: User;
}

export class DiscoverResult{

	trending:Tag[];
	suggested:User[];
	earners:User[];
}