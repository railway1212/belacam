import {Component, OnDestroy, OnInit, ElementRef, ViewChild} from '@angular/core';
import {BelaApiService} from '../core/bela-api.service';
import {Router} from '@angular/router';
import {AuthService} from "../core/auth.service";
import { TranslateService } from '@ngx-translate/core';
import * as $ from "jquery";

declare var jQuery: any;
declare var userLanguage:string;

var email_run:boolean = false; 
var username_run:boolean = false; 
var password_run:boolean = false; 

var balance:number = 0;

declare var starter_balance:number;
declare var bela_average_price:number;

var signup_bonus_awarded: number = (starter_balance * bela_average_price)/3;



@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css'],
  providers: []

})
export class LandingPageComponent implements OnInit, OnDestroy {
  signin_username = "";
  signin_password = "";
  signup_email = "";
  signup_username = "";
  signup_password = "";
  alert_message = "";
  
  languageToggle:boolean = false;
  
  @ViewChild('captchaRef') captchaRef: ElementRef;

  constructor(private belaAPIService: BelaApiService,
              private router: Router,
              private authService: AuthService,
              private translate: TranslateService) {
    let userName = jQuery('input[name="requesting_user_username"]').val();
    if (userName) {
      this.router.navigate(['/feed']);
    }
  }

  ngOnInit() {

  }

  ngOnDestroy() {
  }

  ngAfterViewInit() {
     jQuery(".signup_form").validate({
        rules: {
            signup_email: {
                required: true,
                email: true
            },
            signup_username: "required",
            signup_password: "required",
        }
    });
  }

  switchLanguage(){
    this.languageToggle = !this.languageToggle;

    jQuery("#language_dropdown").removeClass("show");
  }

  showLanguage() {
    jQuery("#language_dropdown").toggleClass("show");
  }

  showLogin() {
    jQuery("#login").toggleClass("show");
  }

  focusOutEmail() {

    //Regular Expression for validating an email address
    var regEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ 

    
    if(this.signup_email.match(regEx)){
      if ($('#signup_email').val() && email_run == false) {
        setTimeout(function () {
          $('#sack').addClass("playit");
          $('#sack_tag_balance').addClass("show");
          $('.prelim_sack_tag').addClass("hide");
          balance = balance + signup_bonus_awarded;
          var newBalance = balance.toFixed(2);

          let balancestring: string = '' + newBalance;

          let greentext = <HTMLElement> document.querySelector("#balance_counter");
          greentext.innerHTML = balancestring;

        }, 10);
        $('#sack').removeClass("playit");
        email_run = true;
      }
    }
  }


  focusOutUsername() {

    if ($('#signup_username').val() && username_run == false) {
      setTimeout(function () {
        $('#sack').addClass("playit");
        $('#sack_tag_balance').addClass("show");
        let greentext = <HTMLElement> document.querySelector("#balance_counter");

          balance = balance + signup_bonus_awarded;
          var newBalance = balance.toFixed(2)
          let balancestring: string = '' + newBalance;
          greentext.innerHTML = balancestring;

      }, 10);
      $('#sack').removeClass("playit");
      username_run = true;
    }
  }

  focusOutPassword() {

    if ($('#signup_password').val() && password_run == false) {
      setTimeout(function () {
        $('#sack').addClass("playit");
        $('#sack_tag_balance').addClass("show");
        balance = balance + signup_bonus_awarded;
        var newBalance = balance.toFixed(2)

        let balancestring: string = '' + newBalance;

        let greentext = <HTMLElement> document.querySelector("#balance_counter");


        greentext.innerHTML = balancestring;

      }, 10);
      $('#sack').removeClass("playit");
      $('#signup_submit').removeClass("disabled_button");
      password_run = true;
    }
  }

  signin(event) {
    event.stopPropagation();
    if (this.signin_username.trim() != "" && this.signin_password.trim() != "") {
      this.alert_message = "";
      this.belaAPIService.signin(this.signin_username.trim(), this.signin_password.trim()).then((res: any) => {
        if (res["status"] == "success") {
          jQuery('input[name="requesting_user_id"]').val(res["user_id"]);
          jQuery('input[name="requesting_user_username"]').val(res["user_name"]);
          jQuery('input[name="csrfmiddlewaretoken"]').val(res["csrf_token"]);

          if(userLanguage != res["language"]) {
            this.translate.resetLang(userLanguage);
            this.translate.use(res["language"]);
            userLanguage = res["language"];
          }

          this.authService.loadUserDetail();
          this.router.navigate(['/feed']);
        } else {
          this.alert_message = res["message"];
          jQuery("html, body").animate({ scrollTop: 0 }, "slow");
        }
      });
    } else {
      alert("Username and Password is required!");
    }
  }
  

  is_signup_form_valid() {
    return jQuery(".signup_form").valid();
  }

  signup(captchaResponse: string) {
    grecaptcha.reset();

    if (jQuery(".signup_form").valid()) {
      this.alert_message = "";
      this.belaAPIService.signup(this.signup_email.trim(), this.signup_username.trim(), this.signup_password.trim(), captchaResponse).then((res: any) => {
        if (res["status"] == "success") {
          jQuery('input[name="requesting_user_id"]').val(res["user_id"]);
          jQuery('input[name="requesting_user_username"]').val(res["user_name"]);
          jQuery('input[name="csrfmiddlewaretoken"]').val(res["csrf_token"]);

          if(userLanguage != res["language"]) {
            this.translate.resetLang(userLanguage);
            this.translate.use(res["language"]);
            userLanguage = res["language"];
          }

          this.authService.loadUserDetail();
          this.router.navigate(['/feed']);
        } else {
          this.alert_message = res["message"];
          jQuery("html, body").animate({ scrollTop: 0 }, "slow");
        }
      });
    }
  }
  togglePassword($event: any) {
    $event.stopPropagation();
    let password_field = jQuery($event.target);
    if (password_field.text() == "Show") {
        password_field.text("Hide");
        password_field.next().attr('type', 'text');
    } 
    else if(password_field.text() == "Показать") { //show
      password_field.text("Скрыть"); //hide
      password_field.next().attr('type', 'text');
    }
    else if(password_field.text() == "Скрыть"){ //hide
      password_field.text("Показать"); //show
      password_field.next().attr('type', 'password');
    }
    else {
        password_field.text("Show");
        password_field.next().attr('type', 'password');
    }
  }

}

