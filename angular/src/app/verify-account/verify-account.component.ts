import {Component, OnDestroy, OnInit} from '@angular/core';
import {BelaApiService} from "../core/bela-api.service";
import {BelaResponse, User} from "../app.model";
import {AuthService} from "../core/auth.service";
import {BelaEvent, BelaEventType} from "../core/event.types";
import {Subscription} from "rxjs/Subscription";
import {EventService} from "../core/event.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-verify-account',
  templateUrl: './verify-account.component.html',
  styleUrls: ['./verify-account.component.css']
})
export class VerifyAccountComponent implements OnInit, OnDestroy {

  user: User = new User();
  phoneNumber: string;
  securityCode: string;
  isCodeSent: boolean = false;
  subscriptions: Subscription[] = [];

  constructor(private belaApiService: BelaApiService,
              private eventService: EventService,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.user = this.authService.user

    this.subscriptions.push(
      this.eventService.subscribe({
        next: (event: BelaEvent) => {
          if(event.type == BelaEventType.UPDATE_USER) {
            this.user = event.value;
          }
        }
      })
    );
  }

  ngOnDestroy() {
    for(let subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  assignPhoneNumber(event) {
    this.phoneNumber = event.target.value;
  }

  assignSecurityCode(event) {
    this.securityCode = event.target.value;
  }

  requestVerification() {
    this.belaApiService.requestPhoneVerification(this.phoneNumber)
      .then((response: BelaResponse) => {
        if(response.status === 'success') {
          this.isCodeSent = true;
        } else {
          alert(response.message);
        }
      })
      .catch((err) => {
        alert('Server error. Please try again later.')
      });
  }

  verifySecurityCode() {
    this.belaApiService.verifySecurityCode(this.phoneNumber, this.securityCode)
      .then((response: BelaResponse) => {
        if (response.status === 'success') {
          alert('Your phone number was successfully verified');

          this.user.user1.phone_number = this.phoneNumber;
          this.authService.updateUser(this.user);

          this.router.navigate(['/']);
        } else {
          alert(response.message);
        }
      })
      .catch((err) => {
        alert('Server error. Please try again later.')
      })
  }
}
