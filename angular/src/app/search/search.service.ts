// #docregion
import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class SearchService {
  constructor(private http: Http) {}
  a: Array<string>;

  search (term: string) {
    let wikiUrl = "";
    let params = new URLSearchParams();

    params.set('format', 'json');

    if(term.charAt(0)=="#") {
      term = term.slice(1,);
      // #docregion search-parameters
      params.set('in', 'tag')
    } else if(term.charAt(0)=="@") {
      term = term.slice(1,);
      // #docregion search-parameters
      params.set('in', 'user')
    }

    wikiUrl = '/api/search/' + term + '/';
    return this.http
      .get(wikiUrl, { params })
      .toPromise()
      .then((response) => response.json())
  }
}
