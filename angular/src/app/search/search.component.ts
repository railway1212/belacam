import {
  Component,
  OnInit,
  Input,
  ChangeDetectorRef,
  ViewChild,
  ElementRef,
  Renderer2,
  OnDestroy,
  AfterContentInit
} from '@angular/core';
import {User, Tag, GrowthStatistics} from '../app.model';
import { BelaApiService } from '../core/bela-api.service';
import { Observable }       from 'rxjs/Observable';
import { Subject }           from 'rxjs/Subject';
import { Router } from '@angular/router';

// Observable class extensions
import 'rxjs/add/observable/of';

// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { SearchService } from './search.service';
import { HttpModule, JsonpModule } from '@angular/http';
import {Subscription} from "rxjs/Subscription";
import {AfterViewInit} from "@angular/core/src/metadata/lifecycle_hooks";

declare var jQuery:any;

@Component({
  styles: [
    `
      .search-backdrop {
        bottom: 0;
        left: 0;
        position: fixed;
        right: 0;
        top: 0;
        z-index: 0;
      }
    `
  ],
  selector: 'my-search',
  templateUrl: './search.component.html',
  providers: [ SearchService ]
})
export class SearchComponent implements OnInit, OnDestroy{
  items: any[] = [];
  subscription: Subscription;
  showSuggestions: boolean = false;
  private searchTerms = new Subject<string>();
  term = "";

  @ViewChild('searchForm') searchForm: ElementRef;

  constructor (
    private searchService: SearchService,
    private cdr: ChangeDetectorRef,
    private renderer: Renderer2,
    private router: Router
  ) { }

  search(term: string): void {
    let blank = "";
    let hash = "#";

    console.log(this.searchForm);
    term = term.trim();
    if(term != blank || term !=hash) {
      this.searchTerms.next(term);
      this.term = term;
    }
  }

  ngOnInit(): void {
    this.subscription = this.searchTerms
      .debounceTime(300)        // wait 300ms after each keystroke before considering the term
      .distinctUntilChanged()   // ignore if next search term is same as previous
      .switchMap(term => {
        return term   // switch to new observable each time the term changes
        // return the http search observable
        ? this.searchService.search(term)
            .catch(err => [])
        : Observable.of<string[]>([])})
      .subscribe(results => {
        this.items = results;

        this._addSocialContext();

        if(this.items.length > 0) {
          this.showSuggestions = true;
        }
        else {
          this.hideSuggestions();
        }
        this.cdr.markForCheck();
      });

    jQuery(this.searchForm.nativeElement).tooltip({
      trigger: 'manual',
      placement: 'bottom'
    });
  }

  _addSocialContext() {
    for(let item of this.items) {
      let contextArr = [];

      if(item.is_followee) {
        contextArr.push('Following');
      }

      if(item.is_follower){
        contextArr.push('Follows you');
      }

      item['social_context'] = contextArr.join(', ');
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();

    jQuery(this.searchForm.nativeElement).tooltip('destroy');
  }

  hideSuggestions() {
    this.showSuggestions = false;
  }

  handleSelectItem() {
    this.hideSuggestions();
  }

  goToTopSearch() {
    if(this.term) {
      this.router.navigate(['/top_search/', this.term])
    } else {
      jQuery(this.searchForm.nativeElement).tooltip('show');
      setTimeout(() => {
        jQuery(this.searchForm.nativeElement).tooltip('hide');
      }, 2000);
    }

  }
}

@Component({
  selector: 'mobile-search',
  templateUrl: './mobile-search.component.html',
  providers: [ SearchService ]
})
export class MobileSearchComponent extends SearchComponent{
   @Input() trending = [] ;
   growth: GrowthStatistics = new GrowthStatistics();

   constructor( searchService: SearchService,
                cdr: ChangeDetectorRef,
                renderer: Renderer2,
                router: Router,
                private appService: BelaApiService,
                ) {
     super(searchService, cdr, renderer, router);
     this.appService.getGrowthStatistics().then((res) =>{
        this.growth = res;
     });
   }


}
