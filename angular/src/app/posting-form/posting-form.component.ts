import { Component, OnInit, Input, ElementRef, ViewChild} from '@angular/core';
import { ImageCompressService, ResizeOptions, ImageUtilityService, IImage, SourceImage } from  'ng2-image-compress';
import { BelaApiService } from '../core/bela-api.service';
import {BelaResponse, GeoPlace, S3Resource, User} from '../app.model';
import { TypeaheadComponent } from "../typeahead/typeahead.component";
import { Router } from "@angular/router";
import { ImageCropperComponent, CropperSettings, Bounds } from 'ng2-img-cropper';
import { EventService } from "../core/event.service";
import { AWSS3ApiService } from "../core/awss3-api/awss3-api.service";
import { BelaEventType } from "../core/event.types";
import { DomSanitizer } from '@angular/platform-browser';

declare var jQuery:any;
declare var window:any;


@Component({
  selector: 'app-posting-form',
  templateUrl: './posting-form.component.html',
  styleUrls: ['./posting-form.component.css']
})
export class PostingFormComponent implements OnInit {
  @ViewChild('thoughtFile') thoughtFile:ElementRef;
  @ViewChild('thoughtVideoFile') thoughtVideoFile:ElementRef;
  @ViewChild('content') content:TypeaheadComponent;
  @ViewChild('cropper', undefined) cropper:ImageCropperComponent;
  @Input() requesting_user = new User();
  data:any;
  cropperSettings: CropperSettings;

  public loading = false;
  previewImg:any;
  showPreview = false;
  showGifPreview = false;
  csrf = "";

  isLoadingLocation: boolean = false;
  isSearchingQuery: boolean = false;
  locations = {
    topSearches: [],
    searchResult: []
  };
  delayTimer = null;
  curLocation: GeoPlace = null;
  isPickerVisible: boolean = false;
  curKeyword: string;
  enableCrop = false;

  //FILTER VARIABLES
  enableFilter = false;
  filterImgPreview: any;
  saturatedPreview: any;
  appliedPreview = 0;

  //VIDEO VARIABLES
  videoData: any;
  videoPreview: any;
  showVideoPreview = false;

  duration = 0;
  videoTooLong = false;

  compressed_image: any;
  resizeOption = new ResizeOptions();

  constructor(
    private belaAPIService: BelaApiService,
    private sanitizer:DomSanitizer,
    private s3APIService: AWSS3ApiService,
    private eventService: EventService,
    private imgCompressService: ImageCompressService) {
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.canvasWidth = 200;
    this.cropperSettings.canvasHeight = 200;
    this.cropperSettings.croppedWidth = 1000;
    this.cropperSettings.minWidth = 200;
    this.cropperSettings.minHeight = 200;
    this.cropperSettings.noFileInput = true;
    this.cropperSettings.preserveSize = true;
    this.cropperSettings.keepAspect = false;
    this.cropperSettings.dynamicSizing = true;
    this.cropperSettings.cropperClass = "previewimage_canvas";
    this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,0,0,1)';
    this.cropperSettings.touchRadius = 50;
    this.data = {};
    this.resizeOption.Resize_Max_Height = 5000;
  }
  ngOnInit() {
    this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
  }

  ngAfterViewInit() {
    this.cropper.cropper.resizeCanvas(jQuery('#post_form .fild').width(), 400, true);

    jQuery(window).resize( () => {
      this.cropper.cropper.resizeCanvas(jQuery('#post_form .fild').width(), 400, true);
    })

  }

  clearPreview(){
    this.showPreview = false;
    this.showGifPreview = false;
    
    this.showVideoPreview = false;
    
    this.appliedPreview = 0;
    this.toggleFilterPreview();
    this.cropper.reset();
    this.thoughtFile.nativeElement.value="";
    this.thoughtVideoFile.nativeElement.value="";
  }

  // Geo Picker

  toogleGeoPicker() {
    this.isPickerVisible = !this.isPickerVisible;
    if(!this.curLocation && this.isPickerVisible) {
      navigator.geolocation.getCurrentPosition((position) => {
        // TODO: get nearest location from server
        this.searchNearest({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        });
      }, (err) => {
        this.searchNearest({});
      });
    }
  }


  onChangeKeyword() {
    this.belaAPIService.searchPlace({keyword: this.curKeyword}).then((res) => {
      if (this.delayTimer) {
        clearTimeout(this.delayTimer);
      }
      this.delayTimer = setTimeout(() => {
        this.searchPlaces(this.curKeyword);
      }, 200);
    });
  }

  searchNearest(params) {
    this.isLoadingLocation = true;
    this.belaAPIService.searchPlace(params).then((res) => {
      this.isLoadingLocation = false;
      this.locations.topSearches = <GeoPlace[]>res;
      this.curLocation = this.locations.topSearches[0];
    })
  }

  searchPlaces(keyword: string) {
    let params = {};

    if(keyword) {
      params = {keyword: keyword}
    } else {
      params = {};
    }

    this.isSearchingQuery = true;
    this.belaAPIService.searchPlace(params).then((res) => {
      this.isSearchingQuery = false;
      this.locations.searchResult = <GeoPlace[]>res;

    });
  }

  selectPlace(place: GeoPlace) {
    let placeIdx = this.locations.topSearches.indexOf(place);
    if(placeIdx !== -1) {
      this.locations.topSearches.splice(placeIdx, 1);
    }

    this.locations.topSearches.unshift(place);
    this.locations.topSearches = this.locations.topSearches.slice(0, 5);
    this.curLocation = place;

  }

  thoughtPicChange($event) {

    let images: Array<IImage> = [];
    this.enableCrop = false;
    let file:File = $event.target.files[0];
    this.showGifPreview = file.type == "image/gif";
    let that = this;

    if(this.showGifPreview) {

      let Reader:FileReader = new FileReader();
      let that = this;
      Reader.onloadend = (loadEvent:any) => {
        that.showPreview = true;
        that.previewImg = loadEvent.target.result;
        that.filterImgPreview = loadEvent.target.result;
        that.saturatedPreview = loadEvent.target.result;
      };

      Reader.readAsDataURL(file);
    } else {
      ImageCompressService.filesToCompressedImageSource($event.target.files).then(observableImages => {
        observableImages.subscribe((image) => {
          images.push(image);
        }, (error) => {
          console.log("Error while converting");
        }, () => {
          that.showPreview = true;
          that.previewImg = images[0].compressedImage.imageDataUrl;
          that.filterImgPreview = images[0].compressedImage.imageDataUrl;
          that.saturatedPreview = images[0].compressedImage.imageDataUrl;
        });
      });

    }
  }

  postThought(event){
    this.loading = true;
    event.stopPropagation();

    console.log('trying to post thought');

    let postData = {
      content:this.content.getHostElement().nativeElement.value,
      csrfmiddlewaretoken:this.csrf
    };

    let files = this.thoughtFile.nativeElement.files;
    let video_files = this.thoughtVideoFile.nativeElement.files;

    if(this.isPickerVisible) {
      postData['location'] = this.curLocation.location;
    }

    if(files.length > 0 || video_files.length > 0) {
      // this.appService.postWithFile('users/'+this.requesting_user.username+'/', postData, files).then(result => {
      let response;
      let fileData;
      let fileType;
      let fileName;
      let it_is_video = false;

      if (video_files.length > 0) {
        fileName = video_files[0].name;
        fileType = video_files[0].type;
        it_is_video = true;
      } else {
        fileName = files[0].name;
        fileType = files[0].type;
      }

      if(this.videoPreview){
        // it is already blob.
        fileData = this.videoData;
      } else {
        if(this.enableCrop) {
          fileData = this.data.image;
        } else {
          fileData = this.previewImg;
        }
        this.compressed_image = {
          fileName: fileName,
          imageObjectUrl: URL.createObjectURL(files[0]),
          imageDataUrl: fileData,
          type: fileType,
          compressedImage: null
        };
      }

      response = this.belaAPIService.getImagePostLink(fileName, fileType)
        .then((res: S3Resource) => {
          if (it_is_video) {
            postData['video_path'] = res.url.replace(res.data.url, '');
            return this.s3APIService.putVideoToS3(res, fileData);
          } else {
            postData['image_path'] = res.url.replace(res.data.url, '');
            if(this.showGifPreview){
              return this.s3APIService.uploadBlobToS3(res, fileData, fileName)
            } else {
              let that = this;
              this.resizeOption.Resize_Type = fileType;
              return ImageCompressService.IImageListToCompressedImageSourceEx([this.compressed_image], this.resizeOption).then(iImage => {
                let temp_file_data = that.belaAPIService.dataURItoBlob(that.compressed_image.compressedImage.imageDataUrl, fileType);
                return that.s3APIService.uploadBlobToS3(res, temp_file_data, fileName)
              });
          }}
        })
        .then((res) => {
          console.log('res: ', res);
          return this.belaAPIService.createFeed(this.requesting_user.username, postData)
        }).catch(err => {
          console.error(err);
        });

      response.then((res: BelaResponse) => {
        this.loading = false;
        if(res.status === 'success'){
          event.target.reset();
          this.showPreview = false;
          this.showGifPreview = false;
          this.isPickerVisible = false;
          this.showVideoPreview = false;

          this.eventService.fireEvent({
            type: BelaEventType.ADDED_POST,
            value: res.result['thot']
          });
        }

      });
    } else {
      this.loading = false;
      jQuery('#uploadlabel').tooltip("show");
      setTimeout(function(){ jQuery('#uploadlabel').tooltip('hide'); }, 4000);
    }

  }

  /**
   * Video Upload Method
   * @param $event 
   */
  thoughtVidUpload($event) {
    let file:File = $event.target.files[0];
    this.videoPreview = file.type == "video/mp4";
    let Reader:FileReader = new FileReader();
    let that = this;
    Reader.onloadend = (loadEvent: any) => {
      that.showVideoPreview = true;
      const arrayBuffer = loadEvent.target.result;
      const fileType = "video/mp4";
      const blob = new Blob([arrayBuffer], {type: fileType});
      this.videoData = blob;
      const src = URL.createObjectURL(blob);
      // URL.revokeObjectURL(src);
      that.videoPreview = this.sanitizer.bypassSecurityTrustResourceUrl(src);
    };
    // var chunk = file.slice(0, 1024 * 1024 * 10); // .5MB
    Reader.readAsArrayBuffer(file);
  }

  onMetadata(e, video) {
    console.log('metadata: ', e);
    this.duration = video.duration;
    console.log('duration: ', this.duration);

    if (this.duration > 60) {
      this.videoTooLong = true;
      this.clearPreview();
      console.log("Video is too long");
    }
    else {
      this.videoTooLong = false;
    }
    video.currentTime = 0.5;
  }

  clickCloseDialog() {
    this.videoTooLong = false;
  }

  setCrop() {
    console.log('calling: setCrop()');
    this.enableCrop = !this.enableCrop;
    if (this.enableCrop) {
      let image: any = new Image();
      image.src = this.previewImg;
      this.cropper.cropper.resizeCanvas(jQuery('#post_form .fild').width(), 400, true);
      this.cropper.setImage(image, new Bounds(image.width / 4, image.height / 4, image.width / 2, image.height / 2));
    } else {
      this.previewImg = this.cropper.image.original.src;
    }
  }

  rotate() {
    if (!this.showGifPreview) {

      let image: any = new Image();
      image.src = this.previewImg;

      let canvas = document.createElement('canvas');
      let ctx = canvas.getContext('2d');

      canvas.width = image.height;
      canvas.height = image.width;

      ctx.rotate(90 * Math.PI / 180);
      ctx.drawImage(image, 0, -image.height);
      this.previewImg = image.src = canvas.toDataURL('image/png');

      //TO ROTATE FILTER PREVIEWS
      let image2: any = new Image();
      image2.src = this.filterImgPreview;

      let canvas2 = document.createElement('canvas');
      let ctx2 = canvas2.getContext('2d');

      canvas2.width = image2.height;
      canvas2.height = image2.width;

      ctx2.rotate(90 * Math.PI / 180);
      ctx2.drawImage(image2, 0, -image2.height);

      this.filterImgPreview = image2.src = canvas2.toDataURL('image/png');
      this.saturatedPreview = this.filterImgPreview;
      this.filterImgPreview = this.filterImgPreview;

      if (this.enableCrop) {
        this.cropper.setImage(image, new Bounds(image.height / 4, image.width / 4, image.height / 2, image.width / 2));
      }

    }
  }

  /*
      START FILTER METHODS
  */
  toggleFilterPreview() {
    this.enableFilter = !this.enableFilter;
  }


  isPreviewApplied() {
    this.appliedPreview = this.appliedPreview + 1;
    this.saturatedPreviewApply();
  }

  resetApplied() {
    this.appliedPreview = 0;
  }

  saturatedPreviewApply() {
    if (this.appliedPreview == 1) {
      let image: any = new Image();
      image.src = this.saturatedPreview;

      let canvas = document.createElement('canvas');
      let ctx = canvas.getContext('2d');

      canvas.width = image.width;
      canvas.height = image.height;

      ctx.drawImage(image, 0, 0);

      image.style.display = "none";
      var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
      var data = imageData.data;

      for (var i = 0; i < data.length; i += 4) {
        var r = data[i] / 1.5;
        var g = data[i + 1] / 1.5;
        var b = data[i + 1] / 1.5;
        data[i] = data[i] + r; // red
        data[i + 1] = data[i + 1] + g; // green
        data[i + 2] = data[i + 2] + b; // blue
      }

      ctx.putImageData(imageData, 0, 0);

      this.saturatedPreview = image.src = canvas.toDataURL('image/jpeg', 0.5);
      this.appliedPreview = this.appliedPreview + 1;
    }
  }

  setOriginal() {
    this.previewImg = this.filterImgPreview;
  }

  setSaturated() {
    let image: any = new Image();
    image.src = this.filterImgPreview;


    let canvas = document.createElement('canvas');
    let ctx = canvas.getContext('2d');

    canvas.width = image.width;
    canvas.height = image.height;

    ctx.drawImage(image, 0, 0);
    image.style.display = "none";
    var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);


    var data = imageData.data;

    for (var i = 0; i < data.length; i += 4) {
      var r = data[i] / 1.5;
      var g = data[i + 1] / 1.5;
      var b = data[i + 1] / 1.5;
      data[i] = data[i] + r; // red
      data[i + 1] = data[i + 1] + g; // green
      data[i + 2] = data[i + 2] + b; // blue
    }

    ctx.putImageData(imageData, 0, 0);

    this.previewImg = image.src = canvas.toDataURL('image/jpeg', 0.5);
  }

  setGrayscale() {
    let image: any = new Image();
    image.src = this.filterImgPreview;

    let canvas = document.createElement('canvas');
    let ctx = canvas.getContext('2d');

    canvas.width = image.width;
    canvas.height = image.height;

    ctx.drawImage(image, 0, 0);
    image.style.display = "none";
    var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);

    var data = imageData.data;

    for (var i = 0; i < data.length; i += 4) {
      var avg = (data[i] + data[i + 1] + data[i + 2]) / 3;
      data[i] = avg; // red
      data[i + 1] = avg; // green
      data[i + 2] = avg; // blue
    }

    ctx.putImageData(imageData, 0, 0);

    this.previewImg = image.src = canvas.toDataURL('image/jpeg', 1);
  }

  /*
      END FILTER METHODS
   */
}

@Component({
  selector: 'app-header-posting-form',
  templateUrl: './header-posting-form.component.html'
})
export class HeaderPostingFormComponent implements OnInit {
  @ViewChild('headerThoughtFile') headerThoughtFile:ElementRef;
  @ViewChild('headerThoughtVideoFile') headerThoughtVideoFile:ElementRef;
  @ViewChild('headerContent') headerContent:TypeaheadComponent;
  @ViewChild('cropper', undefined) cropper:ImageCropperComponent;

  @Input() requesting_user = new User();
  @Input() thots:any;
  data:any;
  cropperSettings: CropperSettings;

  public loading = false;
  headerPreviewImg:any;
  showHeaderPreview = false;
  showHeaderGifPreview = false;
  showHeaderPreviewClose = false;
  csrf = "";
  enableCrop = false;
  curLocation: GeoPlace = null;
  isPickerVisible: boolean = false;
  isLoadingLocation: boolean = false;
  locations = {
    topSearches: [],
    searchResult: []
  };

  duration = 0;
  videoTooLong = false;

  //FILTER VARIABLES
  headerEnableFilter = false;
  headerFilterImgPreview: any;
  headerSaturatedPreview: any;
  headerAppliedPreview = 0;
  headerCompressed_image: any;
  headerResizeOption = new ResizeOptions();

  videoMobilePreview: any;
  videoData: any;
  showMobileVideoPreview = false;

  constructor(private belaAPIService: BelaApiService,
              private s3APIService: AWSS3ApiService,
              private sanitizer:DomSanitizer,
              private router: Router,
              private eventService: EventService,
              private imgCompressService: ImageCompressService) {
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.canvasWidth = 200;
    this.cropperSettings.canvasHeight = 200;
    this.cropperSettings.croppedWidth = 1000;
    this.cropperSettings.minWidth = 200;
    this.cropperSettings.minHeight = 200;
    this.cropperSettings.noFileInput = true;
    this.cropperSettings.preserveSize = true;
    this.cropperSettings.keepAspect = false;
    this.cropperSettings.dynamicSizing = true;
    this.cropperSettings.cropperClass = "previewimage_canvas";
    this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,0,0,1)';
    this.cropperSettings.touchRadius = 50;
    this.data = {};
    this.headerResizeOption.Resize_Max_Height = 5000;

  }

  ngOnInit() {
    this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();

  }

  ngAfterViewInit() {
    let elImagePreview = jQuery('#header_post_form .fild');

    this.cropper.cropper.resizeCanvas(elImagePreview.width(), 300, true);
    jQuery(window).resize(() => {
      this.cropper.cropper.resizeCanvas(elImagePreview.width(), 300, true);
    })

  }
  // Geo Picker

  toogleGeoPicker() {
    this.isPickerVisible = !this.isPickerVisible;
    if(!this.curLocation && this.isPickerVisible) {
      navigator.geolocation.getCurrentPosition((position) => {
        // TODO: get nearest location from server
        this.searchNearest({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        });
      }, (err) => {
        this.searchNearest({});
      });
    }
  }

  searchNearest(params) {
    this.isLoadingLocation = true;
    this.belaAPIService.searchPlace(params).then((res) => {
      this.isLoadingLocation = false;
      this.locations.topSearches = <GeoPlace[]>res;
      this.curLocation = this.locations.topSearches[0];
    })
  }

  clearHeaderPreview(){
    this.showMobileVideoPreview = false;
    this.showHeaderGifPreview = false;
    this.showHeaderPreviewClose = false;
    this.showHeaderPreview = false;
    this.headerAppliedPreview = 0;
    this.cropper.reset();
    this.headerThoughtFile.nativeElement.value="";
    this.headerThoughtVideoFile.nativeElement.value="";
  }

  headerThoughtPicChange($event) {

    let images: Array<IImage> = [];
    this.enableCrop = false;
    let file:File = $event.target.files[0];
    this.showHeaderGifPreview = file.type == "image/gif";
    let that = this;
    this.showHeaderPreviewClose = true;

    if(this.showHeaderGifPreview) {

      let Reader:FileReader = new FileReader();
      let that = this;
      Reader.onloadend = (loadEvent:any) => {
        that.showHeaderPreview = true;
        that.headerPreviewImg = loadEvent.target.result;
        that.headerFilterImgPreview = loadEvent.target.result;
        that.headerSaturatedPreview = loadEvent.target.result;
      };

      Reader.readAsDataURL(file);
    } else {
      ImageCompressService.filesToCompressedImageSource($event.target.files).then(observableImages => {
        observableImages.subscribe((image) => {
          images.push(image);
        }, (error) => {
          console.log("Error while converting");
        }, () => {
          that.showHeaderPreview = true;
          that.headerPreviewImg = images[0].compressedImage.imageDataUrl;
          that.headerFilterImgPreview = images[0].compressedImage.imageDataUrl;
          that.headerSaturatedPreview = images[0].compressedImage.imageDataUrl;
        });
      });

    }
  }

  headerThoughtVidUpload($event) {
    let file:File = $event.target.files[0];
    this.videoMobilePreview = file.type == "video/mp4";
    let Reader:FileReader = new FileReader();
    let that = this;
    this.showHeaderPreviewClose = true;
    Reader.onloadend = (loadEvent: any) => {
      that.showMobileVideoPreview = true;
      const arrayBuffer = loadEvent.target.result;
      const fileType = "video/mp4";
      const blob = new Blob([arrayBuffer], {type: fileType});
      this.videoData = blob;
      const src = URL.createObjectURL(blob);
      // URL.revokeObjectURL(src);
      that.videoMobilePreview = this.sanitizer.bypassSecurityTrustResourceUrl(src);

    };
    // var chunk = file.slice(0, 1024 * 1024 * 10); // .5MB
    Reader.readAsArrayBuffer(file);
  }

  clearPreview(){
    this.showHeaderPreview = false;
    this.showHeaderGifPreview = false;
    this.showMobileVideoPreview = false;
    this.toggleFilterPreview();
    this.cropper.reset();
    this.headerThoughtFile.nativeElement.value="";
    this.headerThoughtVideoFile.nativeElement.value="";
  }

  onMetadata(e, video) {
    console.log('metadata: ', e);
    this.duration = video.duration;
    console.log('duration: ', this.duration);

    if (this.duration > 60) {
      this.videoTooLong = true;
      this.clearPreview();
      console.log("Video is too long");
    }
    else {
      this.videoTooLong = false;
    }
    video.currentTime = .5;
  }

  postThought(event){
    this.loading = true;
    event.stopPropagation();

    let postData = {
      content:this.headerContent.getHostElement().nativeElement.value,
      csrfmiddlewaretoken:this.csrf
    };
    let files = this.headerThoughtFile.nativeElement.files;
    let video_files = this.headerThoughtVideoFile.nativeElement.files;

    if(this.isPickerVisible) {
      postData['location'] = this.curLocation.location;
    }

    if(files.length > 0 || video_files.length > 0) {
      let response;
      let fileData, fileType, fileName;
      let it_is_video = false;

      if (video_files.length > 0) {
        fileName = video_files[0].name;
        fileType = video_files[0].type;
        it_is_video = true;
      } else {
        fileName = files[0].name;
        fileType = files[0].type;
      }

      if(this.videoMobilePreview){
        fileData = this.videoData;
      } else {
        if(this.enableCrop) {
          fileData = this.data.image;
        } else {
          fileData = this.headerPreviewImg;
        }
        this.headerCompressed_image = {
          fileName: fileName,
          imageObjectUrl: URL.createObjectURL(files[0]),
          imageDataUrl: fileData,
          type: fileType,
          compressedImage: null
        };
      }

      response = this.belaAPIService.getImagePostLink(fileName, fileType)
        .then((res: S3Resource) => {
          if (it_is_video) {
            postData['video_path'] = res.url.replace(res.data.url, '');
            return this.s3APIService.putVideoToS3(res, fileData);
          } else {
            postData['image_path'] = res.url.replace(res.data.url, '');
            if(this.showHeaderGifPreview){
              return this.s3APIService.uploadBlobToS3(res, fileData, fileName)
            } else {
              let that = this;
              this.headerResizeOption.Resize_Type = fileType;
              return ImageCompressService.IImageListToCompressedImageSourceEx([this.headerCompressed_image], this.headerResizeOption).then(iImage => {
                let temp_file_data = that.belaAPIService.dataURItoBlob(that.headerCompressed_image.compressedImage.imageDataUrl, fileType);
                return that.s3APIService.uploadBlobToS3(res, temp_file_data, fileName)
              });
          }}
        })
        .then((res) => {
          return this.belaAPIService.createFeed(this.requesting_user.username, postData)
        }).catch(err => {
          console.error(err);
        });

      response.then((res:BelaResponse) => {

        this.loading = false;
        if(res.status === 'success'){
          //this.initiateFeed();
          event.target.reset();
          this.showHeaderPreview = false;
          this.showHeaderGifPreview = false;
          this.showMobileVideoPreview = false;

          this.eventService.fireEvent({
            type: BelaEventType.ADDED_POST,
            value: res.result['thot']
          });

          window.uploadMobileImage();
          jQuery('html, body').animate({
            scrollTop: jQuery(".middle-section .white-box").first().offset().top - 80
          }, 1000);
        }
        else{
          //Handle error in form
        }
      });
    } else {
      this.loading = false;
      jQuery('#uploadlabelHeader').tooltip("show");
      setTimeout(function(){ jQuery('#uploadlabelHeader').tooltip('hide'); }, 4000);
    }
  }

  setCrop() {
    this.enableCrop = !this.enableCrop;
    if(this.enableCrop) {
      let image: any = new Image();
      image.src = this.headerPreviewImg;
      this.cropper.cropper.resizeCanvas(jQuery('#header_post_form .fild').width(), 300, true);
      this.cropper.setImage(image, new Bounds(image.width/4, image.height/4, image.width/2, image.height/2));
    } else {
      this.headerPreviewImg = this.cropper.image.original.src;
    }
  }

  rotate() {
    if(!this.showHeaderGifPreview)
    {

      let image: any = new Image();
      image.src = this.headerPreviewImg;
      let canvas = document.createElement('canvas');
      let ctx = canvas.getContext('2d');
      canvas.width = image.height;
      canvas.height = image.width;
      ctx.rotate(90 * Math.PI / 180);
      ctx.drawImage(image, 0, -image.height);
      this.headerPreviewImg = image.src = canvas.toDataURL('image/png');

      //TO ROTATE FILTER PREVIEWS
      let image2: any = new Image();
      image2.src = this.headerFilterImgPreview;

      let canvas2 = document.createElement('canvas');
      let ctx2 = canvas2.getContext('2d');

      canvas2.width = image2.height;
      canvas2.height = image2.width;

      ctx2.rotate(90 * Math.PI / 180);
      ctx2.drawImage(image2, 0, -image2.height);

      this.headerFilterImgPreview = image2.src = canvas2.toDataURL('image/png');

      this.headerSaturatedPreview = this.headerFilterImgPreview;
      this.headerFilterImgPreview = this.headerFilterImgPreview;

      if(this.enableCrop) {
        this.cropper.setImage(image, new Bounds(image.height/4, image.width/4, image.height/2, image.width/2));
      }

    }
  }

  /*
     START FILTER METHODS
  */
  toggleFilterPreview() {
    this.headerEnableFilter = !this.headerEnableFilter;
  }

  isPreviewApplied() {
    this.headerAppliedPreview = this.headerAppliedPreview + 1;
    this.saturatedPreviewApply();
  }

  resetApplied() {
    this.headerAppliedPreview = 0;
  }

  saturatedPreviewApply() {
    if (this.headerAppliedPreview == 1) {
      let image: any = new Image();
      image.src = this.headerSaturatedPreview;

      let canvas = document.createElement('canvas');
      let ctx = canvas.getContext('2d');

      canvas.width = image.width;
      canvas.height = image.height;

      ctx.drawImage(image, 0, 0);

      image.style.display = "none";
      var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
      var data = imageData.data;

      for (var i = 0; i < data.length; i += 4) {
        var r = data[i] / 1.5;
        var g = data[i + 1] / 1.5;
        var b = data[i + 1] / 1.5;
        data[i] = data[i] + r; // red
        data[i + 1] = data[i + 1] + g; // green
        data[i + 2] = data[i + 2] + b; // blue
      }

      ctx.putImageData(imageData, 0, 0);

      this.headerSaturatedPreview = image.src = canvas.toDataURL('image/jpeg', 0.5);
      this.headerAppliedPreview = this.headerAppliedPreview + 1;
    }
  }

  setOriginal() {
    this.headerPreviewImg = this.headerFilterImgPreview;
  }

  setSaturated() {
    let image: any = new Image();
    image.src = this.headerFilterImgPreview;


    let canvas = document.createElement('canvas');
    let ctx = canvas.getContext('2d');

    canvas.width = image.width;
    canvas.height = image.height;

    ctx.drawImage(image, 0, 0);
    image.style.display = "none";
    var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);


    var data = imageData.data;

    for (var i = 0; i < data.length; i += 4) {
      var r = data[i] / 1.5;
      var g = data[i + 1] / 1.5;
      var b = data[i + 1] / 1.5;
      data[i] = data[i] + r; // red
      data[i + 1] = data[i + 1] + g; // green
      data[i + 2] = data[i + 2] + b; // blue
    }

    ctx.putImageData(imageData, 0, 0);

    this.headerPreviewImg = image.src = canvas.toDataURL('image/jpeg', 0.5);
  }

  setGrayscale() {
    let image: any = new Image();
    image.src = this.headerFilterImgPreview;

    let canvas = document.createElement('canvas');
    let ctx = canvas.getContext('2d');

    canvas.width = image.width;
    canvas.height = image.height;

    ctx.drawImage(image, 0, 0);
    image.style.display = "none";
    var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);

    var data = imageData.data;

    for (var i = 0; i < data.length; i += 4) {
      var avg = (data[i] + data[i + 1] + data[i + 2]) / 3;
      data[i] = avg; // red
      data[i + 1] = avg; // green
      data[i + 2] = avg; // blue
    }

    ctx.putImageData(imageData, 0, 0);

    this.headerPreviewImg = image.src = canvas.toDataURL('image/jpeg', 1);
  }

  /*
      END FILTER METHODS
   */
}
