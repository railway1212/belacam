import { Component, OnDestroy, OnInit } from '@angular/core';
import { User } from '../app.model';
import { BelaApiService } from '../core/bela-api.service';
import { Constants } from "../constants";
import { Lightbox } from "../lightbox/lightbox.service";
import { AuthService } from "../core/auth.service";
import { EventService } from "../core/event.service";
import { Subscription } from "rxjs/Subscription";
import { BelaEvent, BelaEventType, EvLoadedAlbum, EvRequestAlbum } from "../core/event.types";

declare var jQuery:any;

@Component({
  selector: 'app-homefeed',
  templateUrl: './homefeed.component.html',
  styleUrls: ['./homefeed.component.css'],
  providers: []

})
export class HomefeedComponent implements OnInit, OnDestroy {

  thoughts=[];
  album=[];
  requesting_user = new User();
  homefeeds_api_url = '/api/homefeeds/';
  cover_pic_url = Constants.default_cover_pic;

  subscriptions: Subscription[] = [];

  public loading = true;
  constructor(
    private appService:BelaApiService,
    private _lightbox: Lightbox,
    private authService: AuthService,
    private eventService: EventService) { }

  ngOnInit() {

    //this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();

    this.setRequestingUser(this.authService.user);

    this.subscriptions.push(
      this.eventService.subscribe({
        next: (val: BelaEvent) => {
          if(val.type === BelaEventType.ADDED_POST) {
            this.thoughts.unshift(val.value);
            this._buildAlbum();
          } else if(val.type === BelaEventType.DELETED_POST) {
            this.delete(val.value);
          } else if(val.type === BelaEventType.REQUEST_NEXT_ALBUM) {
            this._requestAlbum(val.value);
          } else if(val.type === BelaEventType.UPDATE_USER) {
            this.setRequestingUser(val.value);
          }
        }
      })
    );

    this.loading = true;
    this._loadNextPage().subscribe(() => {
      this.loading = false;
      jQuery('html, body').animate({scrollTop:0}, '400');
    });
  }

  setRequestingUser(user: User) {
    this.cover_pic_url =
      user.user1 && user.user1.cover_pic ? user.user1.cover_pic : Constants.default_cover_pic;
    this.requesting_user = user;
  }

  ngOnDestroy() {
    for(let subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  onFeedScroll(){
    let bottom_reached = this.appService.isBottomReached();
    if(this.homefeeds_api_url != null && bottom_reached && !this.loading){
      this.loading = true;
      this._loadNextPage().subscribe(() => {
        this.loading = false;
      });
    }
  }

  delete(thot) {
    let idx = this.thoughts.indexOf(thot);
    if(idx > -1) {
      this.thoughts.splice(idx, 1);
    }
    this._buildAlbum();
  }

  openThotBox(index) {
    if(index >= 0) {
      this._lightbox.openPost(this.album, index, {
        disableKeyboardNav: true,
        wrapAround: false,
        allowRequestAlbum: true
      });
    }
  }

  _buildAlbum() {
    this.album = [];
    for(let j=0; j<this.thoughts.length; j++){
      let _album = {
        src: this.thoughts[j].image,
        caption: this.thoughts[j].content,
        thumb: this.thoughts[j].image,
        thot:this.thoughts[j]
      };
      this.album.push(_album);
    }
  }

  _loadNextPage() {
    return this.appService.getList(this.homefeeds_api_url)
      .map((listResp: any) => {

          this.homefeeds_api_url = listResp.next;
          for (let i in listResp.results) {
            this.thoughts.push(listResp.results[i]);
          }
          this._buildAlbum();
        });
  }

  _requestAlbum(request: EvRequestAlbum) {
    if(this.homefeeds_api_url != null && request.direction === EvRequestAlbum.DIRECTION_NEXT) {
      this._loadNextPage().subscribe(() => {
        let evValue: EvLoadedAlbum = new EvLoadedAlbum();

        evValue.album = this.album;
        evValue.newImageIndex = request.currentImageIndex + 1;

        this.eventService.fireEvent({
          type: BelaEventType.LOADED_NEXT_ALBUM,
          value: evValue
        });
      })
    } else {
      this.eventService.fireEvent({
        type: BelaEventType.NO_NEXT_ALBUM,
        value: null
      });
    }

  }
}
