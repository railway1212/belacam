export class Comment {
  id: number;
  text: string;
  created_at: Date;
  author: User;
}

export class Tag{
  id: number;
  name: string;
  slug: string;
  num_items:number;
  preview: any[];

  /* Dynamic properties */
  preview_next: number;
  preview_page_size: number;
}

export class UserProfile{
  user:number;
  belacoin: number;
  bio: string;
  birthyear:number;
  gender:string;
  country:string;
  belacoin_address:string;
  cover_pic:string;
  account_balance:number;
  phone_number: string;
  is_bot:boolean;
}

export class User {
  id: number;
  user1: UserProfile;
  full_name:string;
  username:string;
  avatar:string;
  avatar_url?:string;
  is_following:boolean;
  earnings:number;
  thought_count:number;
  total_depsoit: number;
  album:any[];
  preview:any[];
  earning_24hrs:number;
  likes_24hrs:number;
  bella_per_like:number;
  is_superuser:boolean;
  verified:boolean;

  constructor(){
    this.user1 = new UserProfile();
    this.is_following=false;
    this.earnings=0;
    this.album=[];
    this.preview=[];
    this.earning_24hrs=0.0;
    this.likes_24hrs=0.0;
    this.is_superuser=false;
    this.verified=false;
  }
}

export class Follow {
  success:boolean;
  followers:User[];
  followers_count:number;
  followings:User[];
  followings_count:number;
}

export class Thought {
  id: number;
  user: User = new User();
  is_liked: boolean;
  content: string;
  created_at: Date;
  image: string;
  video: string;
  likes:User[] = [];
  location: string;
  views: number[] = [];
  earnings: number;
  belas: number;
  comments: Comment[] = [];
  displayed_comments:Comment[] = [];
  show_all_comment:boolean = false;
  editable: boolean;
  editableLocation: boolean;
  tempContent: string;
  tempLocation: string;
  total_likes_count: number;
  likes_api_url: string = "";

  static updateDisplayedComments(thot: Thought, bShowAll: boolean) {
    if(bShowAll) {
      thot.displayed_comments = thot.comments.slice();
    } else {
      thot.displayed_comments = thot.comments.slice(-2);
    }
    thot.show_all_comment = bShowAll;
  }
}

export class ExThought extends Thought {
    is_liking: boolean;
}

export class ThoughtListResult {
  count: number;
  next: string;
  previous: string;
  results: Thought[]
}

export class ListResult {
  count: number;
  next: string;
  previous: string;
  results: any[]
}

export class LikeBelaResult {
  liked: boolean;
  bela: Thought;
  user: User;
  error: string;

  static E_SAME_USER = 'BL_SAME_USER';
  static E_INSUFFICIENT_COIN = 'BL_INSUFFICIENT_COIN';
}

export class GeoLocation {
  latitude: number;
  longitude: number;

}

export class GeoPlace {
  location: string;
}

export class UserReferrer {
  user: User;
  belas: number;
  status: string;
}

export class Referrer {
  id: number;
  name: string;
  link: string;
  description: string;
  created_at: Date;
  users: UserReferrer[];

  constructor() {
    this.users = [];
  }
}

export class ReferrerStatistics {
  num_users: number;
  belas_income: number;
  dollar_income: number;
}

export class ReferrerApplication {
  user: User;
  content: string;
  status: string;
  created_at: Date;

  static fromJson(json: any): ReferrerApplication {
    let obj = new ReferrerApplication();
    Object.assign(obj, json);
    return obj;
  }

  get statusText() {
    let map = {
      'PENDING': 'Pending',
      'APPROVED': 'Approved',
      'DENIED': 'Deferred'
    };
    if(this.status && map[this.status]) {
      return map[this.status];
    } else {
      return this.status;
    }
  }
}

export class ReferralSetting {
  range_from: number;
  range_to: number;
  payout: number;
}

export class GrowthStatistics {
  total_users: number;
  new_users_in_1: number;
  new_users_in_7: number;

  constructor() {
    this.total_users = 0;
    this.new_users_in_1 = 0;
    this.new_users_in_7 = 0;
  }
}


export class S3ResourceFields {
  policy: string;
  accessKeyId: string;
  contentType: string;
  key: string;
  signature: string;

  static fromJson(json: any): S3ResourceFields {
    if(!json) {
      return null;
    }

    let obj = new S3ResourceFields();
    let propMaps = [
      ['policy', 'policy'],
      ['accessKeyId', 'AWSAccessKeyId'],
      ['contentType', 'Content-Type'],
      ['key', 'key'],
      ['signature', 'signature']
    ];

    for(let map of propMaps) {
      if(json[map[1]] !== undefined) {
        obj[map[0]] = json[map[1]]
      }
    }

    return obj;
  }
}

export class S3ResourceData {
  url: string;
  fields: S3ResourceFields;

  static fromJson(json: any): S3ResourceData {
    if(!json) {
      return null;
    }

    let obj = new S3ResourceData();
    obj.url = json.url;
    obj.fields = S3ResourceFields.fromJson(json.fields);

    return obj;
  }
}

export class S3Resource {
  url: string;
  data: S3ResourceData;

  static fromJson(json: any): S3Resource {
    let obj = new S3Resource();
    obj.url = json.url;
    obj.data = S3ResourceData.fromJson(json.data);

    return obj;
  }
}

export class WithdrawFeeSetting {
  range_from: number;
  range_to: number;
  percentage: number;
}

export class BelaResponse {
  status: string;
  result: any;
  message: string;
  code: string;
  error: any;
  notifications: any[];
}


export interface ImageEditModel {
  imageFile: any;
  type?: string;
  keepAspect?: boolean;
  title?: string;
}

export class RewardTask {
  type: string;
  descripiton: string;
  reward: number;
  is_complete: boolean;
}

export class BelaNotification {
  actor: string;
  action: string;
  action_id:number;
  username:string;
  action_object_object_id: number;
  emailed: boolean;
  description: string;
  level: string;
  deleted: boolean;
  timestamp: Date;
  action_object_content_type: number;
  data: any;
  public: boolean;
  actor_object_id: string;
  verb: string;
  target_object_id: string;
  actor_content_type: number;
  unread: boolean;
  recipient: number;
  id: number;
  target_content_type: any;
  actor_thumbnail:any;
  actor_user: User;
}

export class BelaNotificationResp
{
  unread_count:number;
  notifications_list:BelaNotification[];
  action:"";
}

export class PaginatedBelaNotificationResp {
  count: number;
  next: string;
  unread_count: number;
  previous: string;
  results: BelaNotification[];
}
