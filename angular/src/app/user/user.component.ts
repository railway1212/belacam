import {Component, NgZone, OnInit, ElementRef, ViewChild, OnDestroy} from '@angular/core';
import { BelaApiService } from '../core/bela-api.service';
import { User, Thought } from '../app.model';
import { ActivatedRoute, Params, Router }   from '@angular/router';
import { Location }                 from '@angular/common';
import { Lightbox } from "../lightbox/lightbox.service";
import 'rxjs/add/operator/switchMap';
import { Constants } from "../constants";
import { EventService } from "../core/event.service";
import { AuthService } from "../core/auth.service";
import { Subscription } from "rxjs/Subscription";
import {BelaEvent, BelaEventType, EvLoadedAlbum, EvRequestAlbum} from "../core/event.types";
import { LoaderService } from "../core/loader.service";
import {AWSS3ApiService} from "../core/awss3-api/awss3-api.service";
import {DialogService} from "ng2-bootstrap-modal";
import {AvatarEditModalComponent} from "../modals/avatar-edit-modal/avatar-edit-modal.component";
import {CoverEditModalComponent} from "../modals/cover-edit-modal/cover-edit-modal.component";

declare var jQuery:any;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers: [LoaderService]
})
export class UserComponent implements OnInit, OnDestroy {
  @ViewChild('profilefileInput') profilefileInput:ElementRef;
  @ViewChild('coverfileInput') coverfileInput:ElementRef;

  title = 'app';
  username = 'amitjaiswal';
  total_feeds=0;
  thoughts:Thought[] = [];
  albumList=[];

  user = new User();
  requesting_user = new User();
  requesting_user_id = 1;
  requesting_user_username = '';

  cover_pic_url = Constants.default_cover_pic;

  csrf = "";
  followers = [];
  followings = [];
  followers_count = 0;
  followings_count = 0;

  feed_api_url = '/api/self/';
  followers_api_url = '/api/followers/';
  followings_api_url = '/api/followings/';
  followers_api_base_url = '/api/followers/';
  followings_api_base_url = '/api/followings/';

  subscriptions: Subscription[] = [];

  viewMode = 'list';

  bio_in_edit: string;

  edit_clicked = false;
  edit_bio_clicked = false;
  edit_username_clicked = false;
  edit_bio_success = true;
  edit_username_success = true;

  requestID = 'user_page';
  public loading = true;
  public follow_loading = true;


  constructor(private belaApiService: BelaApiService,
              private awsApiService: AWSS3ApiService,
              private authService: AuthService,
              private eventService: EventService,
              public ngZone:NgZone,
              private loaderService: LoaderService,
              private route: ActivatedRoute,
              private router: Router,
              private location: Location,
              private _lightbox: Lightbox,
              private dialogService:DialogService) {

  }
  ngOnInit() {

    this.requesting_user_id = this.authService.getUserId();
    this.requesting_user_username = this.authService.getUserName();
    this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();

    this.requesting_user = this.authService.user;

    this.subscriptions.push(
      this.eventService.subscribe({
        next: (event) => {
          if (event.type === BelaEventType.DELETED_POST) {
            this.delete(event.value);
          } else if (event.type === BelaEventType.ADDED_POST) {
            this.add(event.value);
          } else if (event.type === BelaEventType.REQUEST_NEXT_ALBUM) {

            let request: EvRequestAlbum = event.value;

            if (request.requestID === this.requestID) {
              if (this.existNextFeed()) {
                this.loadNextFeed().subscribe(next => {
                  let evValue: EvLoadedAlbum = new EvLoadedAlbum();

                  evValue.album = this.albumList;
                  evValue.newImageIndex = request.currentImageIndex + 1;

                  let event = new BelaEvent();

                  event.type = BelaEventType.LOADED_NEXT_ALBUM;
                  event.value = evValue;

                  this.eventService.fireEvent(event);
                }, err => {

                  let event: BelaEvent = new BelaEvent();

                  event.type = BelaEventType.NO_NEXT_ALBUM;
                  event.value = null;

                  this.eventService.fireEvent(event);

                })
              } else {
                let event = new BelaEvent();
                event.type = BelaEventType.NO_NEXT_ALBUM;

                this.eventService.fireEvent(event);
              }
            }
          } else if(event.type == BelaEventType.UPDATE_USER) {
            this.requesting_user = event.value;
          }
        }
      })
    );

    this.route.params
      .subscribe(params => {
        this.loading = true;
        this.username = params['username'];
        if(!this.username || this.username == "undefined") {

          this.router.navigate(['/discover'])

        } else {

          this.feed_api_url = '/api/self/' + this.username + '/';
          this.followers_api_url = this.followers_api_base_url + this.username + '/';
          this.followings_api_url = this.followings_api_base_url + this.username + '/';

          jQuery('#mobileinfo').removeClass("slidein").addClass("slideout");
          jQuery('#search').removeClass('open');
          jQuery('.modal').modal('hide'); // Hide any opened modal.
          this._lightbox.close_all();

          this.followers = [];
          this.followings = [];
          this.total_feeds = 0;

          this.belaApiService.getUserDetail(this.username)
            .then((userData) => {
              this.user = userData;

              // trello issue: CHmi3Ghb
              /*
              if user has cover photo, then we will wait (by showing white image) and then show his cover photo.
              Otherwise (if user doesn't have his cover photo), the default cover color photo will be shown.
              The key is: the Constants.default_cover_pic is set first which is a white background image.
              This serves as "waiting image" in both cases.
               */
              const user_has_own_cover_photo = userData.user1.cover_pic.indexOf('assets/images/c-pic.jpg') < 0;

              if (userData.user1 && user_has_own_cover_photo) {
                this.cover_pic_url = userData.user1.cover_pic;
              } else {
                this.cover_pic_url = Constants.default_cover_color_pic;
              }

              jQuery('html, body').animate({scrollTop:0}, '400');
            })
            .catch((err) => {
              if(err.status == 404) {
                // User was not found
                this.router.navigate(['404']);
              }
            });

          this.initiateFeed();

          this.initiateEditStatus();

          this.initiateFollowerFollowing();

        }
        jQuery('html, body').animate({scrollTop:0}, '400');
      });

    jQuery('html, body').animate({scrollTop:0}, '400');
  }

  ngOnDestroy () {
    for(let subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  showImage(thot) {
    return thot.image && !thot.video;
  }

  showVideo(thot) {
    if (thot.video) {
      return thot.video + '#t=0.1';
    } else {
      return false;
    }

  }

  initiateEditStatus() {
    this.edit_clicked = false;
    this.edit_bio_clicked = false;
    this.edit_username_clicked = false;
    this.edit_bio_success = true;
    this.edit_username_success = true;
  }

  initiateFeed(){
    this.loading = true;
    this.thoughts = [];

    this.belaApiService.getList(this.feed_api_url).subscribe((thoughtListResp) => {
      this.total_feeds = thoughtListResp.count;
      this.thoughts = thoughtListResp.results;
      this.feed_api_url = thoughtListResp.next;
      jQuery('html, body').animate({scrollTop:0}, '400');
      this.loading = false;
      this.buildAlbum();
    });
    jQuery('html, body').animate({scrollTop:0}, '400');
  }

  initiateFollowerFollowing(){
    this.follow_loading = true
    this.followers = [];
    this.followings = [];
    this.followers_count = 0;
    this.followings_count = 0;

    this.belaApiService.getList(this.followers_api_url).subscribe((followListResp) => {
      this.followers_count = followListResp.count;
      this.followers = followListResp.results;
      this.followers_api_url = followListResp.next;
      this.follow_loading = false;
    });

    this.belaApiService.getList(this.followings_api_url).subscribe((followListResp) => {
      this.followings_count = followListResp.count;
      this.followings = followListResp.results;
      this.followings_api_url = followListResp.next;
      this.follow_loading = false;
    });
  }

  onFollowerScroll(){
    if(this.followers_api_url != null && !this.follow_loading) {
      this.follow_loading = true;
      this.belaApiService.getList(this.followers_api_url).subscribe((followListResp) => {
        this.followers_count = followListResp.count;
        for (const result of followListResp.results) {
          this.followers.push(result);
        }
        this.followers_api_url = followListResp.next;
        this.follow_loading = false;
      });
    }
  }

  onFollowingScroll(){
    if(this.followings_api_url != null && !this.follow_loading) {
      this.follow_loading = true;
      this.belaApiService.getList(this.followings_api_url).subscribe((followListResp) => {
        this.followings_count = followListResp.count;
        for (const result of followListResp.results) {
          this.followings.push(result);
        }
        this.followings_api_url = followListResp.next;
        this.follow_loading = false;
      });
    }
  }

  onScroll(){
    let bottom_reached = this.belaApiService.isBottomReached();

    if(this.existNextFeed() && bottom_reached && !this.loading){
      this.loadNextFeed().subscribe();
    }
  }

  existNextFeed() {
    return this.feed_api_url != null;
  }

  loadNextFeed() {
    this.loading = true;
    return this.belaApiService.getList(this.feed_api_url).do(listResp =>{
      this.feed_api_url = listResp.next;
      for(const result of listResp.results) {
        this.thoughts.push(result);
      }
      console.log("Loaded ok");
      this.loading = false;
      this.buildAlbum();
    });
  }

  saveBio(event){
    event.stopPropagation();
    this.belaApiService.updateUserProfile(this.requesting_user, {bio:this.bio_in_edit})
      .then((userProfileResp)=>{
        this.user.user1 = userProfileResp;
        this.edit_bio_clicked = false;
        this.edit_bio_success = true;
      });
  }

  startBioEdit(event) {
    event.stopPropagation();
    this.bio_in_edit = this.user.user1.bio;
    this.edit_bio_clicked = !this.edit_bio_clicked;
    return false;

  }

  cancelBioEdit(event) {
    event.stopPropagation();
    this.edit_bio_clicked = !this.edit_bio_clicked;
  }

  saveName(event) {
    event.stopPropagation();
    this.belaApiService.updateUsername(this.requesting_user, this.user.username)
      .then((userResp) => {
        this.user.username = userResp.username;
        this.edit_username_clicked = false;
        this.edit_username_success = true;
      });
  }

  selectProfilePic(){
    this.profilefileInput.nativeElement.value="";
    this.profilefileInput.nativeElement.click();
  }

  selectCoverPic(){
    this.coverfileInput.nativeElement.value = "";
    this.coverfileInput.nativeElement.click();
  }

  coverFileChangeEvent($event){
    let file:File = $event.target.files[0];
    let fileName = file.name;
    let fileType = file.type;
    let picUrl: string;

    this.dialogService.addDialog(CoverEditModalComponent, {
      imageFile: file
    }).subscribe((result) => {
      if(result) {
        let resizedData = this.belaApiService.dataURItoBlob(result, fileType);

        this.belaApiService.getImagePostLink(fileName, fileType)
          .then(res => {
            picUrl = res.url.replace(res.data.url, '');
            return this.awsApiService.uploadBlobToS3(res, resizedData, fileName);
          })
          .then(res => {
            return this.belaApiService.updateProfileCoverPicture(picUrl);
          })
          .then(res => {
            if(res.status === "success") {
              this.cover_pic_url = res.result['cover_url'];
            }
          })
          .catch(err => {
            console.log(err);
            alert("We got error while uploading photo. Please try again later.");
          })
      }
    });


  }

  profileFileChangeEvent($event){
    let file:File = $event.target.files[0];
    let fileName = file.name;
    let fileType = file.type;
    let picUrl: string;

    this.dialogService.addDialog(AvatarEditModalComponent, {
      imageFile: file
    }).subscribe((result) => {
      console.log('Dialog closed')
      console.log(result);
      if(result) {
        let resizedData = this.belaApiService.dataURItoBlob(result, fileType);

        this.belaApiService.getImagePostLink(fileName, fileType)
          .then(res => {
            picUrl = res.url.replace(res.data.url, '');
            return this.awsApiService.uploadBlobToS3(res, resizedData, fileName);
          })
          .then(res => {
            return this.belaApiService.updateProfileAvatar(picUrl);
          })
          .then(res => {
            if(res.status === 'success') {
              this.user.avatar_url = res.result['avatar_url'];
              this.user.avatar = res.result['avatar'];

              this.authService.updateUser(this.user);
              this.initiateFeed();
            }
          })
          .catch(err => {
            console.log(err);
            alert("We got error while uploading photo. Please try again later.");
          })
      }
    });
  }

  openCoverLightbox(imgUrl: string) {
    let options = {
      disableScrolling: true,
      fitImageInViewPort: true
    };

    let _album = {
      src: imgUrl,
      thumb: imgUrl,
      caption: "",
      thot: null
    };

    this._lightbox.openAlbum([_album], 0, options)
  }

  openAvatarLightbox() {
    let _album = {
      src: this.user.avatar_url,
      thumb: this.user.avatar_url,
      caption: "",
      thot: null
    };
    let options = {
      disableScrolling: true,
      circleImage: true,
      fitImageInViewPort: true
    };

    this._lightbox.openAlbum([_album], 0, options)
  }

  openThotBox(index) {
    if(index >= 0) {
      this._lightbox.openPost(this.albumList, index,
        {
          disableKeyboardNav: true,
          requestID: this.requestID,
          allowRequestAlbum: true
        });
    }
  }

  follow(user){
    if(user && user.id) {
      this.belaApiService.follow(user, this.csrf).then(result => {
        user.is_following=true;

        this.belaApiService.getList(this.followers_api_base_url + this.username + '/').subscribe((followListResp) => {
          this.followers_count = followListResp.count;
          this.followers = followListResp.results;
          this.followers_api_url = followListResp.next;
          this.follow_loading = false;
        });

        this.belaApiService.getList(this.followings_api_base_url + this.username + '/').subscribe((followListResp) => {
          this.followings_count = followListResp.count;
          this.followings = followListResp.results;
          this.followings_api_url = followListResp.next;
          this.follow_loading = false;
        });

      }).catch(() => {
      })
    }
  }

  unfollow(user){
    if(user && user.id) {
      this.belaApiService.unfollow(user, this.csrf).then(result => {
        user.is_following=false;

        this.belaApiService.getList(this.followers_api_base_url + this.username + '/').subscribe((followListResp) => {
          this.followers_count = followListResp.count;
          this.followers = followListResp.results;
          this.followers_api_url = followListResp.next;
          this.follow_loading = false;
        });

        this.belaApiService.getList(this.followings_api_base_url + this.username + '/').subscribe((followListResp) => {
          this.followings_count = followListResp.count;
          this.followings = followListResp.results;
          this.followings_api_url = followListResp.next;
          this.follow_loading = false;
        });

      }).catch(() => {
      })
    }
  }

  switchViewMode(mode: string) {
    if(this.viewMode !== mode) {
      this.viewMode = mode;
      if(mode == "thumbnail") {
        setTimeout(() => {
          this.onScroll();
        }, 1500)
      }
    }
  }

  add(thot) {
    this.thoughts.unshift(thot);
  }

  delete(thot) {
    let idx = this.thoughts.indexOf(thot);
    if(idx > -1) {
      this.thoughts.splice(idx, 1);
    }
  }

  buildAlbum() {
    this.albumList = [];
    for(let j=0; j<this.thoughts.length; j++){
      let _album = {
        src: this.thoughts[j].image,
        caption: this.thoughts[j].content,
        thumb: this.thoughts[j].image,
        thot:this.thoughts[j]
      };
      this.albumList.push(_album);
    }
  }
}
