import {RouterModule, Routes} from "@angular/router";
import {ReferralDashboardComponent} from "./referral-dashboard/referral-dashboard.component";
import {NgModule} from "@angular/core";
import {ReferralApplyComponent} from "./referral-apply/referral-apply.component";

const routes: Routes = [
  { path: 'dashboard', component:ReferralDashboardComponent },
  { path: 'apply', component:ReferralApplyComponent }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class ReferralModuleRouting {}
