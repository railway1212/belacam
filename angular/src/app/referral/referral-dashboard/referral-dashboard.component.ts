import {Component, OnDestroy, OnInit} from '@angular/core';
import {BelaResponse, ReferralSetting, Referrer, ReferrerApplication, ReferrerStatistics, User} from "../../app.model";
import { BelaApiService } from "../../core/bela-api.service";
import { AuthService } from "../../core/auth.service";
import {BelaEvent, BelaEventType} from "../../core/event.types";
import {Subscription} from "rxjs/Subscription";
import {EventService} from "../../core/event.service";

declare var jQuery:any;

@Component({
  selector: 'app-referral-dashboard',
  templateUrl: './referral-dashboard.component.html',
  styleUrls: ['./referral-dashboard.component.css']
})
export class ReferralDashboardComponent implements OnInit, OnDestroy {

  requesting_user = new User();
  referrer: Referrer = new Referrer();
  referrerApplication: ReferrerApplication;
  referrerStatistics: ReferrerStatistics = new ReferrerStatistics();
  referralSettings: ReferralSetting[] = [];
  subscriptions: Subscription[]  = [];

  constructor(private appService: BelaApiService,
              private authService: AuthService,
              private eventService: EventService) { }

  ngOnInit () {
    this.requesting_user = this.authService.user;

    this.subscriptions.push(
      this.eventService.subscribe({
        next: (event: BelaEvent) => {
          if(event.type == BelaEventType.UPDATE_USER) {
            this.requesting_user = event.value;
          }
        }
      })
    );

    if(this.authService.isLoggedIn()) {
      this.appService.getReferrer().then(obj => {
        if(obj.status == 'success') {
          this.referrer = obj.result['referrer'];
        }

      }).catch(err => {
        if(err.status === 404) {
          this.appService.getReferrerApplication()
            .then((obj: BelaResponse) => {

              if(obj.status == 'success') {
                this.referrerApplication = ReferrerApplication.fromJson(obj.result['application']);
              }

            });
        }

      });

      this.appService.getReferrerStatistics().then(obj => {
        this.referrerStatistics = obj;
      });
    }

    this.appService.getReferralSettings().then(obj => {
      this.referralSettings = obj;
    });
  }

  ngOnDestroy () {
    for(let subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }
}
