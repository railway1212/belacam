import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferralDashboardComponent } from "./referral-dashboard/referral-dashboard.component";
import { ReferralApplyComponent } from "./referral-apply/referral-apply.component";
import { ReferralModuleRouting }     from './referral.routing';
import { SharedModule } from "../shared/shared.module";
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ReferralModuleRouting
  ],

  declarations: [
    ReferralDashboardComponent,
    ReferralApplyComponent
  ]
})
export class ReferralModule { }
