import { Component, OnInit } from '@angular/core';
import { User } from "../../app.model";
import { AuthService } from "../../core/auth.service";
import { BelaApiService } from "../../core/bela-api.service";
import { BelaEvent, BelaEventType } from "../../core/event.types";
import { Router } from "@angular/router";
import {EventService} from "../../core/event.service";

@Component({
  selector: 'app-referral-apply',
  templateUrl: './referral-apply.component.html',
  styleUrls: ['./referral-apply.component.css']
})
export class ReferralApplyComponent implements OnInit {
  requesting_user = new User();
  applyText: string;

  constructor(
    private authService: AuthService,
    private eventService: EventService,
    private belaAPIService: BelaApiService,
    private router: Router
  ) { }

  ngOnInit() {
    this.requesting_user = this.authService.user;

    this.eventService.subscribe({
        next: (event: BelaEvent) => {
          if (event.type == BelaEventType.UPDATE_USER) {
            this.requesting_user = event.value;
          }
        }
      });
  }

  submit() {
    console.log("on submit");
    this.belaAPIService.createReferralApplication(this.applyText)
      .then((res) => {
        if(res.status == 'fail') {

          if(res.error['user_id'] && res.error['user_id'][0].indexOf('unique') !== -1) {
            alert('You already have sent application');
            this.router.navigate(['/feed']);
          } else {
            alert(res.message);
          }

        } else {
          alert('Your application was successfully sent');
          this.router.navigate(['/feed']);
        }

      })
      .catch(err => {
        alert('Server error. Please try again later');
      });
  }
}
