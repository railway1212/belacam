import {Component, OnInit, Input} from '@angular/core';
import {User, Thought} from '../app.model';
import {BelaApiService} from '../core/bela-api.service';
import {ActivatedRoute, Params} from '@angular/router';
import {Location} from '@angular/common';

declare var jQuery: any;

@Component({
  selector: 'app-top-search',
  templateUrl: './top-search.component.html',
  styleUrls: ['./top-search.component.css'],
  providers: []
})

export class TopSearchComponent implements OnInit {
  items: any = [];
  top_search_api_url = '/api/searchtop/';
  tagname = "";

  constructor(private appService: BelaApiService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params
      .subscribe(params => {
        this.tagname = params['tagname'];
        let search_in = "all";
        if(this.tagname == "" || this.tagname === undefined) {
          search_in = "all";
          this.tagname = "";
        }
        else if(this.tagname.charAt(0)=="#") {
          this.tagname = this.tagname.slice(1,);
          // #docregion search-parameters
          search_in = 'tag';
        } else if(this.tagname.charAt(0)=="@") {
          this.tagname = this.tagname.slice(1,);
          // #docregion search-parameters
          search_in = 'user';
        }

        this.appService.getTopSearchList(this.top_search_api_url, this.tagname, search_in).then(listResp => {
          this.items = listResp;
        });
      });
  }
}


