import {Component, OnDestroy, OnInit, ElementRef, ViewChild} from '@angular/core';
import {BelaApiService} from '../core/bela-api.service';
import {Router, ActivatedRoute} from '@angular/router';
import {AuthService} from "../core/auth.service";

declare var jQuery: any;

@Component({
  selector: 'app-password-reset-page',
  templateUrl: './password-reset-page.component.html',
  styleUrls: ['./password-reset-page.component.css'],
  providers: []

})
export class PasswordResetPageComponent implements OnInit, OnDestroy {

  email = "";
  alert_message = "";
  message_type = "";

  constructor(private belaAPIService: BelaApiService,
              private router: Router,
              private authService: AuthService,) {
    let userName = jQuery('input[name="requesting_user_username"]').val();
    if (userName) {
      this.router.navigate(['/feed']);
    }
  }

  ngOnInit() {
    jQuery("#reset_password_form").validate({
      rules: {
        email: {
          required: true,
          email: true
        }
      }
    });

  }

  ngOnDestroy() {
  }

  reset_password_form() {
    if (jQuery("#reset_password_form").valid()) {
      this.belaAPIService.reset_password(this.email).then((res: any) => {
        if (res["status"] == "success") {
          this.alert_message = res["message"];
          this.message_type = "alert-info";
          jQuery("html, body").animate({scrollTop: 0}, "slow");
        } else {
          this.alert_message = res["message"];
          this.message_type = "alert-danger";
          jQuery("html, body").animate({scrollTop: 0}, "slow");
        }
      });
    } else {

    }
  }

}

@Component({
  selector: 'app-password-reset-confirm-page',
  templateUrl: './password-reset-confirm-page.component.html',
  styleUrls: ['./password-reset-page.component.css'],
  providers: []

})
export class PasswordResetConfirmPageComponent implements OnInit, OnDestroy {

  password_new = "";
  password_new_confirm = "";
  status = "loading";
  uidb64 = "";
  token = "";

  constructor(private belaAPIService: BelaApiService,
              private router: Router,
              private route: ActivatedRoute,
              private authService: AuthService,) {
    let userName = jQuery('input[name="requesting_user_username"]').val();
    if (userName) {
      this.router.navigate(['/feed']);
    }
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.belaAPIService.check_reset_password_link(
        params['uidb64'],
        params['token']
      ).then((res: any) => {
        this.uidb64 = params['uidb64'];
        this.token = params['token'];
        this.status = res["status"];
        jQuery("html, body").animate({scrollTop: 0}, "slow");
      });

    });


  }

  ngOnDestroy() {
  }

  reset_password_confirm_form() {
    jQuery("#reset_password_confirm_form").validate({
      rules: {
        new_password: "required",
        new_password_confirm: {
          required: true,
          equalTo: "#id_new_password"
        }
      }
    });
    if (jQuery("#reset_password_confirm_form").valid()) {
      this.belaAPIService.reset_password_confirm(
        this.password_new,
        this.password_new_confirm,
        this.uidb64, this.token
      ).then((res: any) => {
        this.status = res["status"]
        jQuery("html, body").animate({scrollTop: 0}, "slow");
      });
    }
  }

}
