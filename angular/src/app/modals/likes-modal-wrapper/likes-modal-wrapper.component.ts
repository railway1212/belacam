import {Component, Input, OnInit} from '@angular/core';
import {Thought, User} from "../../app.model";

@Component({
  selector: 'app-likes-modal-wrapper',
  templateUrl: './likes-modal-wrapper.component.html',
  styleUrls: ['./likes-modal-wrapper.component.css']
})
export class LikesModalWrapperComponent implements OnInit {

  @Input() thot: Thought=null;
  @Input() id: string='likebox';
  @Input() user: User=null;

  constructor() { }

  ngOnInit() {
  }

}
