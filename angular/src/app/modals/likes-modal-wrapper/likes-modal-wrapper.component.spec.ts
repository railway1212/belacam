import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LikesModalWrapperComponent } from './likes-modal-wrapper.component';

describe('LikesModalWrapperComponent', () => {
  let component: LikesModalWrapperComponent;
  let fixture: ComponentFixture<LikesModalWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LikesModalWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LikesModalWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
