import {Component, Input, OnInit} from '@angular/core';
import {Thought, User} from "../../app.model";

@Component({
  selector: 'app-views-modal-wrapper',
  templateUrl: './views-modal-wrapper.component.html',
  styleUrls: ['./views-modal-wrapper.component.css']
})
export class ViewsModalWrapperComponent implements OnInit {

  @Input() thot: Thought=null;
  @Input() id: string='viewbox';
  @Input() user: User=null;

  constructor() { }

  ngOnInit() {
  }

}
