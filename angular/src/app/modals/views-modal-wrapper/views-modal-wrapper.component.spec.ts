import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewsModalWrapperComponent } from './views-modal-wrapper.component';

describe('ViewsModalWrapperComponent', () => {
  let component: ViewsModalWrapperComponent;
  let fixture: ComponentFixture<ViewsModalWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewsModalWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewsModalWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
