import {Component, Input, OnInit} from '@angular/core';
import {BelaApiService} from "../../core/bela-api.service";
import {Thought, User} from "../../app.model";

declare var jQuery:any;

@Component({
  selector: '[app-likes-modal]',
  templateUrl: './likes-modal.component.html',
  styleUrls: ['./likes-modal.component.css']
})
export class LikesModalComponent implements OnInit {

  @Input() thot: Thought=null;
  @Input() user: User=null;
  csrf="";
  like_loading = false;
  likes_api_url = '/api/likes/';
  constructor(private appService:BelaApiService) { }

  ngOnInit() {
    this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
    if(this.thot.total_likes_count > 20 && this.thot.likes.length <= 20){
      this.thot.likes_api_url = this.likes_api_url + this.thot.id + '/?page=2'
    } else if(this.thot.total_likes_count <= 20) {
      this.thot.likes_api_url = null
    }
  }

  follow(user){
    if(user && user.id) {
      this.appService.follow(user, this.csrf).then(() => {
        user.is_following = true;
      }).catch(err => {
      })
    }
  }

  unfollow(user){
    if(user && user.id) {
      this.appService.unfollow(user, this.csrf).then(() => {
        user.is_following = false;
      }).catch(err => {
      })
    }
  }


  onLikeScroll(){
    if(this.thot.likes_api_url != null && !this.like_loading) {
      this.like_loading = true;
      this.appService.getList(this.thot.likes_api_url).subscribe((likeListResp) => {
        for (const result of likeListResp.results) {
          this.thot.likes.push(result);
        }
        this.thot.likes_api_url = likeListResp.next;
        this.like_loading = false;
      });
    }
  }
}
