import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AvatarEditModalComponent } from "./avatar-edit-modal.component";

describe('LikesModalComponent', () => {
  let component: AvatarEditModalComponent;
  let fixture: ComponentFixture<AvatarEditModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvatarEditModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvatarEditModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
