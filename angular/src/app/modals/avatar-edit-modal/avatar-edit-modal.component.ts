import {AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { BelaApiService } from "../../core/bela-api.service";
import {ImageEditModel, S3Resource, Thought, User} from "../../app.model";
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';
import { randomString } from "../../../utils";
import { AWSS3ApiService } from "../../core/awss3-api/awss3-api.service";
import {DialogComponent, DialogService} from "ng2-bootstrap-modal";
import { ImageCompressService, ResizeOptions, ImageUtilityService, IImage, SourceImage } from  'ng2-image-compress';

declare var jQuery:any;
declare var window:any;

@Component({
  selector: 'app-avatar-edit-modal',
  templateUrl: './avatar-edit-modal.component.html',
  styleUrls: ['./avatar-edit-modal.component.css']
})
export class AvatarEditModalComponent extends DialogComponent<ImageEditModel, Blob>
  implements ImageEditModel, AfterViewInit, OnDestroy, OnInit {

  imageFile: any=null;
  type: string='circle';
  keepAspect: boolean = false;
  title: string='Edit Image';

  csrf="";

  data:any;
  cropperSettings: CropperSettings;
  id: string;
  resizeHandler: any;

  @ViewChild('cropper', undefined) cropper:ImageCropperComponent;
  @ViewChild('imageEditDiv') imageEditDiv:ElementRef;

  constructor(
    dialogService: DialogService) {
    super(dialogService);
  }

  ngOnInit() {
    this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.canvasWidth = 200;
    this.cropperSettings.canvasHeight = 200;
    this.cropperSettings.croppedWidth = 1000;
    this.cropperSettings.minWidth = 200;
    this.cropperSettings.minHeight = 200;
    this.cropperSettings.noFileInput = true;
    this.cropperSettings.preserveSize = true;
    this.cropperSettings.keepAspect = this.keepAspect;
    this.cropperSettings.dynamicSizing = true;
    this.cropperSettings.cropperClass = "previewimage_canvas";
    this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,0,0,1)';
    this.cropperSettings.touchRadius = 20;
    this.cropperSettings.rounded = this.type == "circle";
    this.data = {};

    this.drawImage();
  }

  ngAfterViewInit() {
    this.resizeCroppingCanvas();
  }

  resizeCroppingCanvas() {
    this.resizeHandler = () => {
      this.cropper.cropper.resizeCanvas(jQuery(this.imageEditDiv.nativeElement).width(), 400, true);
    }

    this.resizeHandler();

    jQuery(window).resize(this.resizeHandler)
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    jQuery(window).off('resize', this.resizeHandler)
  }

  drawImage() {
    let images: Array<IImage> = [];
    let image:any = new Image();

    ImageCompressService.filesArrayToCompressedImageSource([this.imageFile]).then(observableImages => {
        observableImages.subscribe((image) => {
          images.push(image);
        }, (error) => {
          console.log("Error while converting");
        }, () => {
          image.src = images[0].compressedImage.imageDataUrl;
          image.onload = () => {
            this.cropper.setImage(image);
          };
        });
      });
  }

  saveImage(event){
    event.stopPropagation();
    this.result = this.data.image;
    this.close();

    // let postData = {
    //   csrfmiddlewaretoken:this.csrf
    // };
    // let file = this.imageFile;
    // if(file) {
    //   let fileName = file.name;
    //   let fileType = file.type;
    //   let data = this.data.image;
    //
    //   this.belaAPIService.getImagePostLink(fileName, fileType)
    //     .then((res: S3Resource) => {
    //       postData['image_path'] = res.url.replace(res.data.url, '');
    //       return this.s3APIService.uploadBlobToS3(res, data, fileName)
    //     })
    //     .then(res =>{
    //
    //       window.location.reload();
    //     });
    //   // this.callback_url
    //   // this.appService.postWithImage(, postData, 'avatar', this.data.image, files[0].name, files[0].type).then(result => {
    //   //
    //   //
    //   // });
    // } else {
    //   alert("file tag is empty!")
    // }
  }

}
