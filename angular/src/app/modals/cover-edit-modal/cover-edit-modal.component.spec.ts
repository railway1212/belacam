import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoverEditModalComponent } from './cover-edit-modal.component';

describe('CoverEditModalComponent', () => {
  let component: CoverEditModalComponent;
  let fixture: ComponentFixture<CoverEditModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoverEditModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoverEditModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
