import {AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {randomString} from "../../../utils";
import {CropperSettings, ImageCropperComponent} from "ng2-img-cropper";
import {BelaApiService} from "../../core/bela-api.service";
import {ImageEditModel} from "../../app.model";
import {DialogComponent, DialogService} from "ng2-bootstrap-modal";
import { ImageCompressService, ResizeOptions, ImageUtilityService, IImage, SourceImage } from  'ng2-image-compress';

declare var jQuery:any;
declare var window:any;

@Component({
  selector: 'app-cover-edit-modal',
  templateUrl: './cover-edit-modal.component.html',
  styleUrls: ['./cover-edit-modal.component.css']
})
export class CoverEditModalComponent extends DialogComponent<ImageEditModel, Blob>
  implements ImageEditModel, OnInit, AfterViewInit, OnDestroy
{


  imageFile: any=null;
  type: string='rect';
  keepAspect: boolean = false;
  title: string='Edit Image';

  csrf="";

  @ViewChild('cropper', undefined) cropper:ImageCropperComponent;
  @ViewChild('imageEditDiv') imageEditDiv: ElementRef;

  resizeHandler: any;
  data:any;
  cropperSettings: CropperSettings;
  id: string;

  constructor(private appService:BelaApiService,
              dialogService: DialogService) {
    super(dialogService);
  }

  ngOnInit() {
    this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.canvasWidth = 200;
    this.cropperSettings.canvasHeight = 200;
    this.cropperSettings.croppedWidth = 1140;
    this.cropperSettings.width = 325;
    this.cropperSettings.height = 100;
    this.cropperSettings.noFileInput = true;
    this.cropperSettings.preserveSize = true;
    this.cropperSettings.keepAspect = true;
    this.cropperSettings.dynamicSizing = true;
    this.cropperSettings.cropperClass = "previewimage_canvas";
    this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,0,0,1)';
    this.cropperSettings.touchRadius = 20;
    this.cropperSettings.rounded = this.type == "circle";
    this.data = {};


    this.drawImage();
  }

  ngAfterViewInit() {
    this.resizeCropperCanvas();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    jQuery(window).off('resize', this.resizeHandler);
  }

  resizeCropperCanvas() {
    this.resizeHandler = () => {
      this.cropper.cropper.resizeCanvas(jQuery(this.imageEditDiv.nativeElement).width(), 400, true);
    };

    this.resizeHandler();
    jQuery(window).resize(this.resizeHandler)
  }

  drawImage() {
    let images: Array<IImage> = [];
    let image:any = new Image();
    ImageCompressService.filesArrayToCompressedImageSource([this.imageFile]).then(observableImages => {
        observableImages.subscribe((image) => {
          images.push(image);
        }, (error) => {
          console.log("Error while converting");
        }, () => {
          image.src = images[0].compressedImage.imageDataUrl;
          image.onload = () => {
            this.cropper.setImage(image);
          };
        });
      });
  }

  saveImage(event) {
    event.stopPropagation();
    this.result = this.data.image;
    this.close();
  }
}
