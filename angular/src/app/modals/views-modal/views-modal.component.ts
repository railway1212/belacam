import {Component, Input, OnInit} from '@angular/core';
import {Thought, User} from "../../app.model";
import {BelaApiService} from "../../core/bela-api.service";

declare var jQuery:any;

@Component({
  selector: '[app-views-modal]',
  templateUrl: './views-modal.component.html',
  styleUrls: ['./views-modal.component.css']
})
export class ViewsModalComponent implements OnInit {

  @Input() thot: Thought=null;
  @Input() user: User=null;
  csrf="";

  constructor(private appService:BelaApiService) { }

  ngOnInit() {
    this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
  }

  follow(user){
    if(user && user.id) {
      this.appService.follow(user, this.csrf).then(() => {
        user.is_following = true;
      }).catch(err => {
      })
    }
  }
  unfollow(user){
    if(user && user.id) {
      this.appService.unfollow(user, this.csrf).then(() => {
        user.is_following = false;
      }).catch(err => {
      })
    }
  }
}
