import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewsModalComponent } from './views-modal.component';

describe('ViewsModalComponent', () => {
  let component: ViewsModalComponent;
  let fixture: ComponentFixture<ViewsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
