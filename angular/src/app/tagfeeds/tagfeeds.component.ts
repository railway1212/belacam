import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { User, Thought } from '../app.model';
import { BelaApiService } from '../core/bela-api.service';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import { Constants } from "../constants";
import { Lightbox } from "../lightbox/lightbox.service";
import { AuthService } from "../core/auth.service";
import { NextObserver, Observer } from "rxjs/Observer";
import { Subscription } from "rxjs/Subscription";
import { EventService } from "../core/event.service";
import { BelaEvent, BelaEventType, EvLoadedAlbum, EvRequestAlbum } from "../core/event.types";
import { Subject } from "rxjs/Subject";

declare var jQuery:any;

@Component({
  selector: 'app-tagfeeds',
  templateUrl: './tagfeeds.component.html',
  styleUrls: ['./tagfeeds.component.css'],
  providers: []
})

export class TagfeedsComponent implements OnInit, OnDestroy {
  thoughts=[];
  albumList=[];
  user =new User();
  tagname = '';
  tagfeeds_api_url = '/api/tagfeeds/';
  sorting_algorithm = 'hot';
  _requestID = "tagfeeds";

  eventSubscription: Subscription;
  pendingAPI: {callback: Subject<any>, subscription: Subscription};

  public loading = true;
  public select_most_liked = false;

  constructor(private appService: BelaApiService,
              private authService: AuthService,
              private eventService: EventService,
              private route:ActivatedRoute,
              private location:Location,
              private _lightbox: Lightbox) { }

  ngOnInit() {
    this.user = this.authService.user;

    this.eventSubscription = this.eventService.subscribe({
      next: (event: BelaEvent) => {
        if (event.type === BelaEventType.DELETED_POST) {
          this.delete(event.value);
        } else if(event.type == BelaEventType.UPDATE_USER) {
          this.user = event.value;
        } else if(event.type === BelaEventType.REQUEST_NEXT_ALBUM &&
          event.value.requestID == this._requestID) {
          this._requestAlbum(event.value);
        }

      }
    });

    this.route.params
      .subscribe(params => {
        this.tagname = params['tagname'];
        this.tagfeeds_api_url = `/api/tagfeeds/${this.tagname}/`;
        jQuery('#mobileinfo').removeClass("slidein").addClass("slideout");
        jQuery('#search').removeClass("open");
        jQuery('.modal').modal('hide'); // Hide any opened modal.
        this._lightbox.close_all();

        this.thoughts = [];

        this.loading = true;
        this.loadPhotos(this.sorting_algorithm, false).subscribe(() => {
          this.loading = false;
        });

        jQuery('html, body').animate({scrollTop:0}, '400');
      });
  }

  ngOnDestroy () {
    this.eventSubscription.unsubscribe();
  }

  onFeedScroll(){
    let bottom_reached = this.appService.isBottomReached();
    if(this.tagfeeds_api_url != null && bottom_reached && !this.loading){
      this.loading = true;
      this.loadPhotos(this.sorting_algorithm, true)
        .subscribe(() => {
          this.loading = false;
        },err => {
          this.loading = false;
        });
    }
  }
  loadPhotos(param, isAppending = false) {
    if(!isAppending) {
      this.tagfeeds_api_url = `/api/tagfeeds/${this.tagname}/`;
      this.thoughts = [];

      if(this.pendingAPI) {
        this.pendingAPI.subscription.unsubscribe();
        this.pendingAPI.callback.complete();
      }
    }

    let sorting_algorithm = null;

    if(!isAppending) {
       this.sorting_algorithm = sorting_algorithm = param;
    }

    const callback = new Subject();
    const subscription = this.appService.getPhotoList(this.tagfeeds_api_url, sorting_algorithm)
      .subscribe(listResp =>{
        this.tagfeeds_api_url = listResp.next;
        if(isAppending) {
          for(let result of listResp.results) {
            this.thoughts.push(result);
          }
        } else {
          this.thoughts = listResp.results;
          jQuery('html, body').animate({scrollTop:0}, '400');
        }
        this.buildAlbum();

        callback.next();
        callback.complete();
      });

    this.pendingAPI = {
      callback: callback,
      subscription: subscription
    };

    return callback;
  }

  sort_by_hot() {
    this.select_most_liked = false;
    this.loading = true;
    this.loadPhotos('hot').subscribe(() => {
      this.loading = false;
    })
  }

  sort_by_new() {
    this.select_most_liked = false;
    this.loading = true;
    this.loadPhotos('new').subscribe(() => {
      this.loading = false;
    });
  }

  sort_by_likes(param = 'today') {
    this.select_most_liked = true;
    this.loadPhotos('time_' + param).subscribe(() => {
      this.loading = false;
    });
  }

  delete(thot) {
    let idx = this.thoughts.indexOf(thot);
    if(idx > -1) {
      this.thoughts.splice(idx, 1);
    }
    this.buildAlbum();
  }

  openThotBox(index) {
    if(index >= 0) {
      this._lightbox.openPost(this.albumList, index, {
        disableKeyboardNav: true,
        wrapAround: false,
        allowRequestAlbum: true,
        requestID: this._requestID
      });
    }
  }

  buildAlbum() {
    this.albumList = [];
    for(let j=0; j<this.thoughts.length; j++){
      let _album = {
        src: this.thoughts[j].image,
        caption: this.thoughts[j].content,
        thumb: this.thoughts[j].image,
        thot:this.thoughts[j]
      };
      this.albumList.push(_album);
    }
  }
  _requestAlbum(request: EvRequestAlbum) {
    this.loading = true;
    if(this.tagfeeds_api_url == null) {
      let event: BelaEvent = new BelaEvent();
      event.type = BelaEventType.NO_NEXT_ALBUM;
      event.value = null;

      this.eventService.fireEvent(event);
    } else {
      this.loadPhotos(this.sorting_algorithm, true).subscribe(() => {
        let evValue: EvLoadedAlbum = new EvLoadedAlbum();
        evValue.album = this.albumList;
        evValue.newImageIndex = request.currentImageIndex + 1;

        let event: BelaEvent = new BelaEvent();
        event.type = BelaEventType.LOADED_NEXT_ALBUM;
        event.value = evValue;

        this.loading = false;

        this.eventService.fireEvent(event);
      }, err => {
        this.loading = false;
      });
    }

  }
}


