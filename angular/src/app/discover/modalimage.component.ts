import { Component } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { User, Thought } from '../app.model';

export interface ImageModel {
  thot: User;
  preview: Thought;
}

@Component({
  selector: 'image-modal',
  templateUrl: './modalimage.component.html'
})
export class ImageModalComponent extends DialogComponent<ImageModel, null> implements ImageModel {
  thot: User;
  preview: Thought;
  constructor(dialogService: DialogService) {
    super(dialogService);
  }
}