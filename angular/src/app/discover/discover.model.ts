import { User, Thought, Tag } from '../app.model';

export class TagClass{
	id:number;
	tag:string;
	preview:Thought[];
}


export class People{
	person: User;
	preview: Thought[];
}

export class DiscoverResult{

	trending:Tag[];
	suggested:User[];
	earners:User[];
}