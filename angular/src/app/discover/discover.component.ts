import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import {User, Thought, ListResult, Tag} from '../app.model';
import { TagClass } from './discover.model';
import { BelaApiService } from '../core/bela-api.service';
import { DialogService } from "ng2-bootstrap-modal";
import { Lightbox } from '../lightbox/lightbox.service';
import { Http } from '@angular/http';
import { Constants } from "../constants";
import { AuthService } from "../core/auth.service";
import { EventService } from "../core/event.service";
import { Subscription } from "rxjs/Subscription";
import { IAlbum } from "../lightbox/lightbox-event.service";
import { BelaEvent, BelaEventType, EvLoadedAlbum, EvRequestAlbum } from "../core/event.types";
import { Subject } from "rxjs/Subject";

declare var jQuery:any;
@Component({
  selector: 'app-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.css'],
  providers: []
})
export class DiscoverComponent implements OnInit, OnDestroy {
  user = new User();

  cover_pic_url = Constants.default_cover_pic;
  csrf="";

  subscriptions: Subscription[] = [];
  public tab_name = "photo";


  constructor(private appService:BelaApiService,
              private authService: AuthService,
              private eventService: EventService) {
  }

  ngOnInit() {
    this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();

    this.setRequestingUser(this.authService.user);

    this.subscriptions.push(
      this.eventService.subscribe({
        next: (event: BelaEvent) => {
          if(event.type == BelaEventType.UPDATE_USER) {
            this.setRequestingUser(event.value);
          }
        }
      })
    );

    this.cover_pic_url = this.user.user1 && this.user.user1.cover_pic ? this.user.user1.cover_pic : Constants.default_cover_pic;
  }

  ngOnDestroy() {
    for(let subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }
  setRequestingUser(user: User) {
    this.user = user;
  }

  follow(user){
    if(user && user.id) {
      this.appService.follow(user, this.csrf).then(() => {
        user.is_following = true;
      }).catch(err => {
      })
    }
  }

  unfollow(user){
    if(user && user.id) {
      this.appService.unfollow(user, this.csrf).then(() => {
        user.is_following = false;
      }).catch(err => {
      })
    }
  }

  change_tab(tab_name) {
    this.tab_name = tab_name;
  }
}

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit, OnDestroy {
  @Input() user = new User();
  @Input() hidden: boolean = false;

  public loading = true;

  subscription: Subscription;
  people_api_url = '/api/people/';
  people=[];

  csrf="";

  constructor(private appService:BelaApiService,
              private dialogService:DialogService,
              private _lightbox: Lightbox,
              private eventService: EventService) {
  }

  ngOnInit() {
    this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
    this.subscription = this.eventService.subscribe({
      next: (event) => {
        if (event.type === BelaEventType.DELETED_POST) {
          this.delete(event.value);
        }
      }
    });

    this.loadPeoples();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  delete(thot: Thought) {
    for(let user of this.people) {
      if(user.id === thot.user.id) {
        for(let i = 0; i < user.album.length; i ++) {
          let album: IAlbum = user.album[i];

          if(album.thot.id === thot.id) {
            user.album.splice(i, 1);
            return;
          }
        }
      }
    }
  }

  onPeopleScroll(){
    let bottom_reached = this.appService.isBottomReached();
    if(this.people_api_url != null && bottom_reached && !this.loading && !this.hidden){
      this.loadPeoples();
    }
  }

  loadPeoples() {
    this.loading = true;
    return this.appService.getList(this.people_api_url)
      .subscribe((listResp) => {
        this.people_api_url = listResp.next;
        for(let result of listResp.results) {
          // this.people.push(result);
          let index = this.people.push(result)-1;
          for(let j=0; j<this.people[index].preview.length; j++){
            let _album = {
              src: this.people[index].preview[j].image,
              caption: this.people[index].preview[j].content,
              thumb: this.people[index].preview[j].image,
              thot:this.people[index].preview[j]
            };
            if(this.people[index].album == undefined)
              this.people[index].album = [_album];
            else
              this.people[index].album.push(_album);
          }
        }
        this.loading = false;
      }, error => {

      });
  }

  follow(user){
    if(user && user.id) {
      this.appService.follow(user, this.csrf).then(() => {
        user.is_following = true;
      }).catch(err => {
      })
    }
  }
  unfollow(user){
    if(user && user.id) {
      this.appService.unfollow(user, this.csrf).then(() => {
        user.is_following = false;
      }).catch(err => {
      })
    }
  }

  openPostBox(album:any[], index: number): void {
    // open lightbox
    this._lightbox.openPost(album, index, {disableKeyboardNav: true, wrapAround: true});
  }
}

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
})
export class TagsComponent implements OnInit, OnDestroy {
  @Input() user = new User();
  @Input() hidden: boolean = false;

  _requestID = 'tags';

  tags=[];
  tagApiUrl = '/api/tags/';
  tagInLightbox: Tag;
  tagfeedsApiUrl: string;
  subscription: Subscription;

  public loading = true;

  constructor(private belaApiService: BelaApiService,
              private _lightbox: Lightbox,
              private eventService: EventService) {
  }

  ngOnInit() {
    this.loadTags();

    this.subscription = this.eventService.subscribe({
      next: (event: BelaEvent) => {
        let request = event.value;

        if(
          event.type === BelaEventType.REQUEST_NEXT_ALBUM &&
          event.value.requestID == this._requestID
        ) {
          if(this.tagInLightbox.preview_next) {

            this.loadTagPreview(this.tagInLightbox).subscribe((album: IAlbum[]) => {

              let evValue: EvLoadedAlbum = new EvLoadedAlbum();

              evValue.album = album;
              evValue.newImageIndex = request.currentImageIndex + 1;

              let event: BelaEvent = new BelaEvent();

              event.type = BelaEventType.LOADED_NEXT_ALBUM;
              event.value = evValue;

              this.eventService.fireEvent(event);

            }, err => {

              let event: BelaEvent = new BelaEvent();

              event.type = BelaEventType.NO_NEXT_ALBUM;
              event.value = null;

              this.eventService.fireEvent(event);

            });

          } else {

            let event: BelaEvent = new BelaEvent();
            event.type = BelaEventType.NO_NEXT_ALBUM;
            event.value = null;

            this.eventService.fireEvent(event);

          }
        }
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  openPostBox(tag, index: number): void {
    // open lightbox
    this.tagInLightbox = tag;

    this._lightbox.openPost(tag.album, index, {
      disableKeyboardNav: true,
      allowRequestAlbum: true,
      requestID: this._requestID
    });
  }

  loadTags() {
    this.loading = true;
    return this.belaApiService.getList(this.tagApiUrl)
      .subscribe(listResp =>{
        this.tagApiUrl = listResp.next;
        for(let result of listResp.results) {
          let index = this.tags.push(result) - 1;

          for(let j=0; j<this.tags[index].preview.length; j++){
            let _album = this.buildAlbumFromPreview(this.tags[index].preview[j]);
            if(this.tags[index].album == undefined)
              this.tags[index].album = [_album];
            else
              this.tags[index].album.push(_album);
          }

          if(this.tags[index].preview.length >= 4) {
            this.tags[index].preview_next = 2;
            this.tags[index].preview_page_size = 4;
          }
        }
        this.loading = false;
      }, error => {

      });
  }

  loadTagPreview(tag) {
    let subject = new Subject();

    this.tagfeedsApiUrl = `/api/tagposts/${tag.tag}/`;

    this.belaApiService.getList(
      this.tagfeedsApiUrl,
      {page_size: tag.preview_page_size, page: tag.preview_next}
      )
      .subscribe((data: ListResult) => {
        if(data.next) {
          tag.preview_next += 1;
        } else {
          tag.preview_next = null;
        }

        for(let result of data.results) {
          let _album = this.buildAlbumFromPreview(result);
          tag.preview.push(result)

          if(tag.album == undefined)
            tag.album = [_album];
          else
            tag.album.push(_album);
        }

        subject.next(tag.album);
        subject.complete();
      }, err => {
        subject.error(err);
        tag.preview_next = null;
      });

    return subject;
  }

  buildAlbumFromPreview(preview) {
    return {
      src: preview.image,
        caption: preview.content,
      thumb: preview.image,
      thot: preview
    };
  }

  onTagScroll(){
    let bottom_reached = this.belaApiService.isBottomReached();
    if(this.tagApiUrl != null && bottom_reached && !this.loading && !this.hidden){
      this.loadTags();
    }
  }
}

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.css']
})
export class PhotoComponent implements OnInit, OnDestroy {
  @Input() user = new User();
  @Input() hidden: boolean = false;

  _requestID: string = 'photo';

  bellas:Thought[] = [];
  albumList = [];
  photo_api_url = '/api/feed/list/';
  requesting_user_username ="";
  csrf="";
  sorting_algorithm = 'hot';
  subscription: Subscription;
  pendingAPI: {callback: Subject<any>, subscription: Subscription};

  public select_most_liked = false;
  public loading = true;

  constructor(private appService:BelaApiService,
              private http:Http,
              private eventService: EventService,
              private _lightbox: Lightbox) {
  }

  ngOnInit() {
    this.requesting_user_username = jQuery('input[name="requesting_user_username"]').val();
    this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();

    this.loadPhotos(this.sorting_algorithm, false).subscribe(() => {
      this.loading = false;
    });

    this.subscription = this.eventService.subscribe({
      next: (event: BelaEvent) => {
        if(event.type === BelaEventType.REQUEST_NEXT_ALBUM &&
          event.value.requestID == this._requestID) {
          this._requestAlbum(event.value);
        }
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onPhotoScroll(){
    let bottom_reached = this.appService.isBottomReached();

    if(this.photo_api_url != null && bottom_reached && !this.loading && !this.hidden){
      this.loading = true;
      this.loadPhotos(this.sorting_algorithm, true)
        .subscribe(() => {
          this.loading = false;
        },err => {
          this.loading = false;
        });
    }

  }

  _requestAlbum(request: EvRequestAlbum) {
    this.loading = true;
    if(this.photo_api_url == null) {
      let event: BelaEvent = new BelaEvent();
      event.type = BelaEventType.NO_NEXT_ALBUM;
      event.value = null;

      this.eventService.fireEvent(event);
    } else {
      this.loadPhotos(this.sorting_algorithm, true).subscribe(() => {
        let evValue: EvLoadedAlbum = new EvLoadedAlbum();
        evValue.album = this.albumList;
        evValue.newImageIndex = request.currentImageIndex + 1;

        let event: BelaEvent = new BelaEvent();
        event.type = BelaEventType.LOADED_NEXT_ALBUM;
        event.value = evValue;

        this.loading = false;

        this.eventService.fireEvent(event);
      }, err => {
        this.loading = false;
      });
    }

  }

  loadPhotos(param, isAppending = false) {
    if(!isAppending) {
      this.photo_api_url = '/api/feed/list/';
      this.bellas = [];

      if(this.pendingAPI) {
        this.pendingAPI.subscription.unsubscribe();
        this.pendingAPI.callback.complete();
      }
    }

    let sorting_algorithm = null;

    if(!isAppending) {
       this.sorting_algorithm = sorting_algorithm = param;
    }

    const callback = new Subject();
    const subscription = this.appService.getPhotoList(this.photo_api_url, sorting_algorithm)
      .subscribe(listResp =>{
        this.photo_api_url = listResp.next;
        if(isAppending) {
          for(let result of listResp.results) {
            this.bellas.push(result);
          }
        } else {
          this.bellas = listResp.results;
          jQuery('html, body').animate({scrollTop:0}, '400');
        }
        this._buildAlbum();

        callback.next();
        callback.complete();
      });

    this.pendingAPI = {
      callback: callback,
      subscription: subscription
    };

    return callback;
  }

  delete(thot) {
    let idx = this.bellas.indexOf(thot);
    if(idx > -1) {
      this.bellas.splice(idx, 1);
    }
    this._buildAlbum();
  }

  sort_by_hot() {
    this.select_most_liked = false;
    this.loading = true;
    this.loadPhotos('hot').subscribe(() => {
      this.loading = false;
    })
  }

  sort_by_new() {
    this.select_most_liked = false;
    this.loading = true;
    this.loadPhotos('new').subscribe(() => {
      this.loading = false;
    });
  }

  sort_by_likes(param = 'today') {
    this.select_most_liked = true;
    this.loadPhotos('time_' + param).subscribe(() => {
      this.loading = false;
    });
  }

  openPostBox(index) {
    if(index >= 0) {
      this._lightbox.openPost(this.albumList, index, {
        disableKeyboardNav: true,
        allowRequestAlbum: true,
        requestID: this._requestID
      });
    }
  }

  _buildAlbum() {
    this.albumList = [];
    for(let j=0; j<this.bellas.length; j++){
      let _album = {
        src: this.bellas[j].image,
        caption: this.bellas[j].content,
        thumb: this.bellas[j].image,
        thot:this.bellas[j]
      };
      this.albumList.push(_album);
    }
  }
}
