import {Observable} from "rxjs/Observable";
import {Subscriber} from "rxjs/Subscriber";

export class Constants {
  public static root_dir='/api/';
  public static default_cover_pic='https://s3.amazonaws.com/bellassets/assets/images/c-pic.jpg';
  public static default_cover_color_pic='https://s3.amazonaws.com/bellassets/assets/images/c-pic_l3jvT7U.jpg';
  public static default_remote_asset_url_base='https://s3.amazonaws.com/bellassets';
  public static timeService = new Observable<Date>((observer: Subscriber<Date>) => {
    observer.next(new Date());
    setInterval(() => observer.next(new Date()), 600000);
  });
}
