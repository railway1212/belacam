import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {BelaApiService} from '../core/bela-api.service';
import {AuthService} from "../core/auth.service";
import { TranslateService } from '@ngx-translate/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";

declare var jQuery: any;
declare var userLanguage:string;

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
  providers: []

})
export class SettingsComponent implements OnInit, OnDestroy {

  username = "";
  email = "";
  language = "";
  language_list = [];
  send_weekly_email = false;
  verified = null;

  password_current = "";
  password_new = "";
  password_new_confirm = "";

  alert_message = "";
  message_type = "";
  csrf_token = "";
  loading = false;

  constructor(private appService: BelaApiService,
              private authService: AuthService,
              private router: Router,
              private dialogService: DialogService,
              private translate: TranslateService) {
  }

  ngOnInit() {
    this.loading = true;
    this.csrf_token = jQuery('input[name="csrfmiddlewaretoken"]').val();

    this.appService.getSettingDetails()
      .then((settingsData: any) => {
        this.username = settingsData.username;
        this.email = settingsData.email;
        this.language = settingsData.language;
        this.language_list = settingsData.language_list;
        this.send_weekly_email = settingsData.send_weekly_email;
        this.verified = settingsData.verified;
        this.loading = false;
      })
      .catch((err) => {
        if (err.status == 404) {
          this.router.navigate(['404']);
        } else {
          this.router.navigate(['']);
        }
      });
  }

  ngAfterViewInit() {
    this.setupValidation();
  }

  setupValidation() {
    jQuery("#account_form").validate({
      rules: {
        username: "required",
        email: {
            required: true,
            email: true
        },
        language: "required",
      }
    });
    jQuery("#password_form").validate({
      rules: {
        password_current: "required",
        password_new: "required",
        password_new_confirm: {
          required: true,
          equalTo: "#id_password_new"
        }
      }
    });
  }

  saveAccount() {
    if (jQuery("#account_form").valid()) {
      this.appService.updateAccount(this.username, this.email, this.language, this.send_weekly_email, this.csrf_token).then((res: any) => {
        if (res["status"] == "success") {
          this.username = res["username"];
          this.email = res["email"];
          this.language = res["language"];
          this.send_weekly_email = res["send_weekly_email"];
          this.verified = res["verified"];
          this.alert_message = res["message"];
          this.message_type = "alert-info";

          jQuery("html, body").animate({ scrollTop: 0 }, "slow");
          if(userLanguage != this.language) {
            this.translate.resetLang(userLanguage);
            this.translate.use(this.language);
            userLanguage = this.language;
          }
        } else {
          this.alert_message = res["message"];
          this.message_type = "alert-danger";
          jQuery("html, body").animate({ scrollTop: 0 }, "slow");
        }
      });
    }
  }

  resend_verify_email() {
    this.appService.resend_verify_email().then((res: any) => {
      if (res["status"] == "success") {
        this.alert_message = res["message"];
        this.message_type = "alert-info";
        jQuery("html, body").animate({ scrollTop: 0 }, "slow");
      } else {
        this.alert_message = res["message"];
        this.message_type = "alert-danger";
        jQuery("html, body").animate({ scrollTop: 0 }, "slow");
      }
    });
  }

  updatePassword() {
    if (jQuery("#password_form").valid()) {

      this.appService.updatePassword(this.password_current, this.password_new, this.password_new_confirm, this.csrf_token).then((res: any) => {
        if (res["status"] == "success") {
          this.password_current = "";
          this.password_new = "";
          this.password_new_confirm = "";
          this.alert_message = res["message"];
          this.message_type = "alert-info";
          jQuery("html, body").animate({ scrollTop: 0 }, "slow");
        } else {
          this.alert_message = res["message"];
          this.message_type = "alert-danger";
          jQuery("html, body").animate({ scrollTop: 0 }, "slow");
        }
      });
    }
  }

  deleteAccount() {
    this.appService.deleteAccount().then((res: any) => {
      if (res["status"] == "success") {
        this.alert_message = res["message"];
        this.message_type = "alert-info";
        jQuery("html, body").animate({ scrollTop: 0 }, "slow");
        this.authService.logout();
        setTimeout(()=>{
          this.router.navigate(['']);
      },4000);
      } else {
        this.alert_message = res["message"];
        this.message_type = "alert-danger";
        jQuery("html, body").animate({ scrollTop: 0 }, "slow");
      }
    });
  }

  showConfirm() {
      let confirm_modal = this.dialogService.addDialog(ConfirmComponent, {
          title:'Delete Account',
          message:'Are you sure to want to delete your account?'})
          .subscribe((isConfirmed)=>{
              //We get dialog result
              if(isConfirmed) {
                  this.deleteAccount();
              }
              else {
              }
          });
  }

  ngOnDestroy() {
  }

}

export interface ConfirmModel {
  title:string;
  message:string;
}
@Component({
    selector: 'confirm',
    template: `<div class="modal-dialog">
                <div class="modal-content">
                   <div class="modal-header">
                     <button type="button" class="close" (click)="close()" >&times;</button>
                     <h4 class="modal-title">{{title || 'Confirm'}}</h4>
                   </div>
                   <div class="modal-body">
                     <p>{{message || 'Are you sure?'}}</p>
                   </div>
                   <div class="modal-footer">
                     <button type="button" class="btn btn-primary" (click)="confirm()">OK</button>
                     <button type="button" class="btn btn-default" (click)="close()" >Cancel</button>
                   </div>
                 </div>
              </div>`
})
export class ConfirmComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel {
  title: string;
  message: string;
  constructor(dialogService: DialogService) {
    super(dialogService);
  }
  confirm() {
    // we set dialog result as true on click on confirm button,
    // then we can get dialog result from caller code
    this.result = true;
    this.close();
  }
}
