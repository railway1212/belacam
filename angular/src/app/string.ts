interface String {
  reverse(): string;
}

String.prototype.reverse = function(this: string) {
  return this.split('').reverse().join('');
};
