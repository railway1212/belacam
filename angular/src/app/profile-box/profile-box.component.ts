import {Component, Input, OnInit} from '@angular/core';
import {Lightbox} from "../lightbox/lightbox.service";
import {User} from "../app.model";
import {Constants} from "../constants";

@Component({
  selector: 'app-profile-box',
  templateUrl: './profile-box.component.html',
  styleUrls: ['./profile-box.component.css']
})
export class ProfileBoxComponent implements OnInit {

  @Input('user') user: User = new User();


  constructor(private _lightbox: Lightbox) { }

  ngOnInit() {

  }
  cover_pic_url():string {
    return this.user.user1.cover_pic ? this.user.user1.cover_pic : Constants.default_cover_pic;
  }
  openCoverLightbox() {
    let options = {
      disableScrolling: true,
      fitImageInViewPort: true
    };

    let _album = {
      src: this.cover_pic_url(),
      thumb: this.cover_pic_url(),
      caption: "",
      thot: null
    };

    this._lightbox.openAlbum([_album], 0, options)
  }
}
