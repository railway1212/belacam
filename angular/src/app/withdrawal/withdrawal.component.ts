import {Component, OnInit, OnDestroy, ViewChild, ElementRef} from '@angular/core';
import {User, WithdrawFeeSetting} from "../app.model";
import {Subscription} from "rxjs/Subscription";
import {BelaApiService} from "../core/bela-api.service";
import {EventService} from "../core/event.service";
import {BelaEvent, BelaEventType} from "../core/event.types";
import {AuthService} from "../core/auth.service";

@Component({
  templateUrl: './withdrawal.component.html',
  styleUrls: ['./withdrawal.component.css']
})
export class WithdrawalComponent implements OnInit, OnDestroy {

  @ViewChild('withdrawForm') withdrawForm: ElementRef;

  user = new User();
  withdrawFeeSettings: WithdrawFeeSetting[] = [];
  withdrawAddress = "";
  withdrawAmount = 0;

  withdrawFee: any;
  withdrawTotal: any;

  delayTimer: any;

  loading = false;
  subscriptions: Subscription[] = [];

  constructor (private belaApiService: BelaApiService,
               private authService: AuthService,
               private eventService: EventService) { }
  ngOnInit() {
    this.user = this.authService.user;
    this.belaApiService.getWithdrawFeeSettings()
      .then((res) => {
        this.withdrawFeeSettings = res;

        this.calculateWithdrawFee();
      })
      .catch(err => {})

    this.subscriptions.push(this.eventService.subscribe({
      next: (event: BelaEvent) => {
        if(event.type == BelaEventType.LIKE_POST) {
          this.user = event.value;
          localStorage.setItem('user', JSON.stringify(event.value));
        } else if(event.type == BelaEventType.UPDATE_USER) {
          this.user = event.value;
        }
      }
    }));
  }

  ngOnDestroy() {
    for(let subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  withdraw() {
    if(this.user.user1) {
      if(this.user.user1.belacoin < this.withdrawAmount) {
        alert("Withdrawal amount must be smaller than current account capacity!");
      } else if(this.withdrawAmount > 0 && this.withdrawAddress != "") {
        this.loading = true;
        this.belaApiService.createWithdrawRequest(this.withdrawAddress, this.withdrawAmount)
          .then(resp =>{
            alert("A withdrawal confirmation message will arrive in your email inbox in the next 5 minutes. Please click the link there to confirm your withdrawal!");
            this.user.user1.belacoin = resp['balance'];

            this.loading = false;

            this.resetForm();
          })
          .catch(err => {
            alert('Server error. Please try again later.');
            this.loading = false;
          });
      }
    }
  }

  resetForm() {
    this.withdrawForm.nativeElement.reset();
    this.withdrawAddress = "";
    this.withdrawAmount = 0;
  }

  assignAddress(event: any) {
    this.withdrawAddress = event.target.value;
  }

  assignAmount(event: any) {
    this.withdrawAmount = parseFloat(event.target.value);

    if(this.delayTimer) {
      clearTimeout(this.delayTimer)
    }

    this.delayTimer = setTimeout(() => {
      this.calculateWithdrawFee();
    }, 100);
  }

  calculateWithdrawFee() {
    for(let setting of this.withdrawFeeSettings) {
      let rangeFrom = setting.range_from;
      let rangeTo = setting.range_to === null ? Number.MAX_VALUE : setting.range_to;

      if(rangeFrom <= this.withdrawAmount && this.withdrawAmount <=rangeTo) {
        this.withdrawFee = (this.withdrawAmount * setting.percentage / 100).toFixed(2);
        this.withdrawTotal = (this.withdrawAmount * (100 - setting.percentage) / 100).toFixed(2);
        break;
      }
    }
  }
}
