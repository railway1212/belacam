import {
  ChangeDetectorRef,
  Component, ElementRef, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output, TemplateRef,
  ViewChild, ViewContainerRef
} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {Observable} from "rxjs/Observable";
import {Key} from "./models";
import {
  isIndexActive, resolveNextIndex,
  validateArrowKeys, validateNonCharKeyCode
} from "./typeahead.util";
import {SearchService} from "../search/search.service";
import {NgModel} from "@angular/forms";

@Component({
  selector: '[typeahead]',
  styles: [
    `
  .results {
    position: absolute;
  }
  .typeahead-backdrop {
    bottom: 0;
    left: 0;
    position: fixed;
    right: 0;
    top: 0;
    z-index: 1;
  }
  .list-group-item {
    position: relative;
    z-index: 2;
  }
  `
  ],
  template: `<ng-template #suggestionsTplRef>
  <section class="typeahead-list list-group results" *ngIf="showSuggestions">
    <div class="typeahead-backdrop" (click)="hideSuggestions()"></div>
    <div class="list-group-item"
      *ngFor="let result of results; let i = index;"
      [class.active]="markIsActive(i, result)"
      (click)="handleSelectSuggestion(result)"
      (mouseenter)="handleMouseEnterSuggestion(i)">
      <ng-container *ngIf="result.type=='user'; else tagtemp">
        @{{result.username}}<span class="pullright">(\${{result.earnings|number:'1.2-2'}} earned)</span>
      </ng-container>
      <ng-template #tagtemp>
        #{{result.name}}<span class="pullright">({{result.num_items}} posts) </span>
      </ng-template>
      <ng-template
        [ngTemplateOutlet]="taItemTpl"
        [ngTemplateOutletContext]="{ $implicit: {result: result, index: i} }"
      ></ng-template>
    </div>
  </section>
  </ng-template>`,
  providers: [ SearchService, NgModel ]
})
export class TypeaheadComponent implements OnInit, OnDestroy {
  showSuggestions = false;
  results: any[];

  @Input() taItemTpl: TemplateRef<any>;

  @Output() taSelected = new EventEmitter<string>();

  @ViewChild('suggestionsTplRef') suggestionsTplRef: TemplateRef<any>;

  private suggestionIndex = 0;
  private subscriptions: Subscription[];
  private activeResult: string;

  constructor(
    private element: ElementRef,
    private viewContainer: ViewContainerRef,
    private cdr: ChangeDetectorRef,
    private searchService: SearchService,
    private ngModel: NgModel
  ) { }

  @HostListener('keydown', ['$event'])
  handleEsc(event: KeyboardEvent) {
    if (event.keyCode === Key.Escape) {
      this.hideSuggestions();
      event.preventDefault();
    }
  }

  ngOnInit() {
    const onkeyDown$ = this.onElementKeyDown();
    this.subscriptions = [
      this.filterEnterEvent(onkeyDown$),
      this.listenAndSuggest(),
      this.navigateWithArrows(onkeyDown$)
    ];
    this.renderTemplate();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
    this.subscriptions.length = 0;
  }

  renderTemplate() {
    this.viewContainer.createEmbeddedView(this.suggestionsTplRef);
    this.cdr.markForCheck();
  }

  onElementKeyDown() {
    return Observable.fromEvent(this.element.nativeElement, 'keydown').share();
  }

  filterEnterEvent(elementObs: Observable<{}>) {
    return elementObs
      .filter((e: KeyboardEvent) => e.keyCode === Key.Enter)
      .subscribe((event: Event) => {
        if(this.showSuggestions) {
          event.preventDefault();
          this.handleSelectSuggestion(this.activeResult);
        }
      });
  }

  listenAndSuggest() {
    return Observable.fromEvent(this.element.nativeElement, 'keyup')
      .filter((e: any) => validateNonCharKeyCode(e.keyCode))
      .map((e: any) => {

        let selectionStart = this.element.nativeElement.selectionStart;
        let regex = /[@#](\w*)$/;
        let matches = e.target.value.substr(0, selectionStart).match(regex);

        this.hideSuggestions();
        e.matches = matches;
        if(e.matches) {
          return e.matches[0];
        } else {
          return null;
        }
      })
      .debounceTime(300)
      .concat()
      .distinctUntilChanged()
      .filter((query: string) => query && query.length > 0)
      .switchMap((query: string) => this.suggest(query))
      .subscribe((results: string[]) => {
        this.results = results;
        this.showSuggestions = true;
        this.suggestionIndex = 0;
        this.cdr.markForCheck();
      });
  }

  navigateWithArrows(elementObs: Observable<{}>) {
    return elementObs
      .filter((e: any) => validateArrowKeys(e.keyCode))
      .subscribe((e: any) => {
        let keyCode = e.keyCode;

        // Disable navigation if suggestion was hidden.
        if(!this.showSuggestions) {
          return;
        }

        e.preventDefault();
        this.suggestionIndex = resolveNextIndex(
          this.suggestionIndex,
          keyCode === Key.ArrowDown
        );
        this.showSuggestions = true;
        this.cdr.markForCheck();

      });
  }

  suggest(query: string) {
    return this.request(query);
  }

  /**
   * peforms a jsonp/http request to search with query and params
   * @param query the query to search from the remote source
   */
  request(query: string) {
    return Observable.from(this.searchService.search(query));
  }

  markIsActive(index: number, result: string) {
    const isActive = isIndexActive(index, this.suggestionIndex);
    if (isActive) {
      this.activeResult = result;
    }
    return isActive;
  }

  handleSelectSuggestion(suggestion: any) {
    let elCommentText = this.element.nativeElement;
    let selectionStart = elCommentText.selectionStart;
    let regex = /([@#])(\w*)$/;

    let subStrFront = elCommentText.value.substr(0, selectionStart).replace(regex, (match, p1, p2) => {
      if(p1 === '@') {
        return p1 + suggestion.username;
      } else {
        return p1 + suggestion.name;
      }
    });
    let subStrBack = elCommentText.value.substr(selectionStart);

    elCommentText.value = subStrFront + subStrBack;
    elCommentText.selectionStart = elCommentText.selectionEnd = subStrFront.length;
    elCommentText.focus();

    this.ngModel.update.emit(elCommentText.value);

    this.hideSuggestions();
    this.taSelected.emit(suggestion);
  }

  handleMouseEnterSuggestion(index: number) {
    this.suggestionIndex = index;
  }

  hideSuggestions() {
    this.showSuggestions = false;
  }

  hasItemTemplate() {
    return this.taItemTpl !== undefined;
  }

  getHostElement() {
    return this.element;
  }
}
