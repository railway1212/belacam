import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import { User, Thought } from '../app.model';
import { BelaApiService } from '../core/bela-api.service';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import { AuthService } from "../core/auth.service";
import { Subscription } from "rxjs/Subscription";
import { Lightbox } from "../lightbox/lightbox.service";
import { IAlbum } from "../lightbox/lightbox-event.service";
import { BelaEvent, BelaEventType } from "../core/event.types";
import {EventService} from "../core/event.service";

declare var jQuery:any;

@Component({
  selector: 'app-tagfeeds',
  templateUrl: './timesince.component.html',
  styleUrls: ['./timesince.component.css'],
  providers: []
})

export class TimesinceComponent implements OnInit, OnDestroy {
  thoughts=[];
  album: IAlbum[] = [];
  requesting_user = new User();
  subscriptions: Subscription[] = [];
  posting_api_url = '/api/post/';
  post_id=1;

  constructor(private appService: BelaApiService,
              private eventService: EventService,
              private authService: AuthService,
              private route: ActivatedRoute,
              private _lightbox: Lightbox,
              private location: Location) { }

  ngOnInit() {

    this.requesting_user = this.authService.user;
    this.subscriptions.push(this.eventService.subscribe({
      next: (event: BelaEvent) => {
        if(event.type == BelaEventType.UPDATE_USER) {
          this.requesting_user = event.value;
        }
      },
    }));
    this.route.params
      .subscribe(params => {
        this.post_id = params['post_id'];
        this.posting_api_url = '/api/post/'+this.post_id + '/';
        this.thoughts = [];
        this.appService.getList(this.posting_api_url).subscribe(listResp =>{
          this.posting_api_url = listResp.next;
          this.thoughts = listResp.results;
          this._buildAlbum();
        });
        jQuery('html, body').animate({scrollTop:0}, '400');
      });
  }

  ngOnDestroy() {
    for(let subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  openThotBox(index) {
    if(index >= 0) {
      this._lightbox.openPost(this.album, index, {
        disableKeyboardNav: true,
        wrapAround: false,
        allowRequestAlbum: true
      });
    }
  }

  _buildAlbum() {
    this.album = [];
    for(let j=0; j<this.thoughts.length; j++){
      let _album = {
        src: this.thoughts[j].image,
        caption: this.thoughts[j].content,
        thumb: this.thoughts[j].image,
        thot:this.thoughts[j]
      };
      this.album.push(_album);
    }
  }
}


