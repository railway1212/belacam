import { BelacamPage } from './app.po';

describe('belacam App', () => {
  let page: BelacamPage;

  beforeEach(() => {
    page = new BelacamPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
