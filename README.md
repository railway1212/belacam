# redesign

## Getting started

If you need to set up your local development environment for the first time, read the [installation guide](https://github.com/TheAmbiaFund/redesign/blob/master/INSTALL.md).


## Setup summary

Make sure you are using a virtual environment of some sort (e.g. `virtualenv` or
`pyenv`).

```
pip install -r requirements.txt
./manage.py migrate
./manage.py loaddata sites
./manage.py runserver
```


### Run background worker on local

1. Install Redis and start it
2. Activate virtualenv and run celery worker. Make sure it runs in background

    `celery worker -A belacam -l info`

3. (optional) daemonizing celeryd process

    ```
    chmod +x conf/configuration/*
    chmod +x conf/init/*
    cp conf/init/* /etc/init.d/
    cp conf/configuration/* /etc/default/
    ```

    Now celery worker can be started/stopped using

    `sudo /etc/init.d/celeryd start` and `sudo /etc/init.d/celeryd stop`


### Django translate files

```
python manage.py makemessages
python manage.py compilemessages
```
Loading test with Locust
locust -f belacam/locustfile.py --host=http://localhost:8000
