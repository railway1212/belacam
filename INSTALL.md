# install instructions

The purpose of this guide is to help developers get their localhost environment configured properly so that they can contribute to this repo.

## before you begin

  * Text in `code quotes` is meant to be executed in your terminal in the **bash** shell unless otherwise stated.
  * Files and folders within commands may need to be replaced with the entire path to that file or folder!
  * If your installation experience differs from the guide, *please* keep track of those differences so we can improve the guide over time.

## without further ado

  1. Download [belacamenv](https://s3.amazonaws.com/bellassets/belacamenv.zip).
  2. `unzip belacamenv.zip`
  3. Make sure python's [virtualenv](https://pypi.org/project/virtualenv/) is working correctly on your system.  If you are on a mac and don't have it set up yet, I **highly recommend** the installation process [below](#install-virtualenv-on-a-mac).
  4. `virtualenv --python=path/to/appropriate/python belacamenv` where you use the path to the python installation that you installed virtualenv with.
  5. ALL occurrences within *belcamenv* of the string */Users/tylermarx/Desktop/belacamenv* should now be changed to */path/to/your/local/belacamenv*.  If this didn't happen automatically, your virtualenv might be incorrectly configured, and you may need to look into it.
  6. `source belacamenv/bin/activate`.  This will set up a virtual environment for you to work in.  **Every time you want to work on this project in the future, you must open a bash shell and execute this command.**

Note that running `python` in your terminal should now run the python installation inside *belacamenv* if your virtual environment is set up correctly.  If it is not set up correctly, don't try to work around the issue.  You really need to have your virtual environment configured properly.

  7. At this point you need to set up your database.  We use sqlite with spatialite.  If you are on a mac however, this probably won't work, so you'll have to [use postgres instead](#set-up-postgres).
  8. Now you need to edit some environment variables, as explained [below](#edit-some-environment-variables).
  9. `git clone https://github.com/TheAmbiaFund/redesign.git`
  10. Get Angular running (see [below](#get-angular-going)).
  11. `cd redesign`
  12. `pip install -r requirements.txt`.  Remember that you should be using the belacam virtual environment.
  13. `python manage.py makemigrations`.  This command may not be necessary, but the following ones are.
  14. `python manage.py migrate`.  If you run into errors here, your database may not be set up correctly.
  15. `python manage.py runserver`.  This runs the server daemon.
  16. If everything worked, the website will fully function at [localhost:8000](http://localhost:8000).
  17. You may need to [create some users](#creating-users-on-the-website).


## all done

You are done and ready to develop!  You *do not need to read* the extra installation tidbits below unless you were linked to them from above.  Have fun.

You can optionally reserve the domain name *belacamg.com*  to point to *localhost* (for what purpose I do not know).  To do this, we need to edit the file */etc/hosts*.

  1. `sudo nano /etc/hosts`
  2. add the line `127.0.0.1        belacamg.com` (no, the "g" is **not** a typo)

Then you can access the website at [belacamg.com:8000](http://belacamg.com:8000).  Yes, you still need the *8000*.  No, you cannot put the *8000* in */etc/hosts*.


## edit some environment variables

The file `redesign/.env` holds some environment variables for your virtual environment.  Make the following edits to the file:

  * change `EMAIL_BACKEND` variable to have value `'django.core.mail.backends.console.EmailBackend'`.

If in addition you are using Postgres instead of Spatialite, add the following lines to the file

  * `DEV_DB_TYPE="POSTGRES"`
  * `PG_USER="your postgres username here"` where your postgres username was chosen when you installed postgres.  On a mac, the default username is the name of your mac user account (the name of your home folder).
  * `PG_PASSWORD="your postgres password here"` .  If you didn't create a password for your postgres user account, then it is a blank string `""`.


## install virtualenv on a mac

You may prefer a package manager as I do, but the [python mac installer](https://www.python.org/downloads/mac-osx/) will configure things correctly for virtualenv and save you a huge headache.

  1. Download the installer for the newest version of python 2 and run it.
  2. `/System/Library/Frameworks/Python.framework/Versions/2.7/bin/python2.7 /usr/local/bin/pip install virtualenv` where the first path must point to the python you just installed and the second path must point to your [pip](https://pip.pypa.io/en/stable/installing/) installation.

## set up postgres

The easiest way to install on a mac is by using brew:

  * `brew install postgres`

If you aren't on a mac, see the [postgres wiki](https://wiki.postgresql.org/wiki/Detailed_installation_guides) or the [postgres documentation](https://www.postgresql.org/docs/10/static/installation.html) on installation.

Once installed, follow the postgres documentation for a few pages starting [here](https://www.postgresql.org/docs/10/static/tutorial-createdb.html) to learn how to create databases and access them via `psql`.

Now it is time to seed the database.

  1. Obtain the database seed file `postgres_seed` (it may be in the repo, or you may need to ask another dev for the file if the one in the repo is outdated).
  2. `dropdb belacam_dev` to delete the database if it already exists.
  3. `createdb belacam_dev` to create the database to store all the user accounts, posts, etc.
  4.  Install [pgAdmin](https://www.pgadmin.org/download/).
  5. Open the pgAdmin application.
  6. In the "Servers" dropdown on the left, choose "local".  In the "Databases" dropdown, choose "belacam_dev".
  7. In the "Tools" menu at the top, choose "Restore...".
  8. For the "Filename", input the database seed file you downloaded, and for the "Role name", input your postgres username.
  9. Hit the "Restore" button.

You might get a repeated error like "relation "public.thoughts_geoplace" does not exist" that can be ignored.  Other errors could be a real problem.


## get Angular going

Part of belacam runs on [Angular](https://angular.io).  Open a separate terminal window/tab for this, since we will launch a daemon that automatically rebuilds the application on file changes.

  1. `cd redesign/angular`.  This is the root of the angular project.
  2. `npm install` to install all node dependencies.
  3. `npm install -g @angular/cli` to install the angular CLI.  The `-g` option causes it to be installed globally, and this is recommended.
  4. `ng build --watch`.  This command automatically rebuilds the angular part of the project whenever it sees file changes.  Without the `--watch` option, you would have to run `ng build` every time you want to update.

Most of your Angular work will be done in files in the folder `redesign/angular/src/app`.  CSS changes can be made in the file `redesign/belacam/static/assets/css/customs.css`.


## creating users on the website

You will probably need to create some users in order to test the website!

  1. `python manage.py createsuperuser` should create an admin.
  2. Login to admin at ​[localhost:8000/wahoo285](http://localhost:8000/wahoo285/).
  3. Once in the admin panel, go to "App settings", add a new app setting, and click "save".
  3. To create a user, add a new "User" and add a "User Profile" (what tracks their Bela balance, etc.) assigned to that user

## committing to GitHub

Below is the general recommended flow of adding your changes to the GitHub repo.  If you are not familiar with git, try reading a [tutorial](https://product.hubspot.com/blog/git-and-github-tutorial-for-beginners).  You can run `git status` at any time to see what branch you are on, what changes are ready ("staged") to be committed, etc.

  1. `cd path/to/directory/whose/changes/you/want/to/send/to/github`
  2. `git add .` adds the current directory to the list of changed things.
  3. `git commit -m "briefly explain the changes you made here"`
  4. `git pull` to pull the latest version of the repo and give you a chance to merge your code into it.
  5. `git push` to finally push your changes to GitHub.

Or even better, put your changes on a separate branch and make a pull request.  This encourages peer-review and gives a chance for somebody to catch your mistake before your code breaks something.
