webpackJsonp(["referral.module"],{

/***/ "./src/app/referral/referral-apply/referral-apply.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/referral/referral-apply/referral-apply.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n\n\n  <p class=\"reftext\" translate>Belacam referral links pay generously. Therefore, we require referral links to be manually requested, and we hand-pick who to approve.</p>\n\n\n  <div class=\"referredusers\" *ngIf=\"!requesting_user.id || !requesting_user.verified\">\n    <h3 translate>Notice</h3>\n    <p class=\"reftext\" translate>Please confirm your email before applying for a Belacam referral link. Once you confirm your email, this message will disappear, and you will be able to apply for a referral link.</p>\n  </div>\n  <br>\n\n\n  <div class=\"referredusers\" *ngIf=\"requesting_user.id && requesting_user.verified\">\n    <form (ngSubmit)=\"submit()\">\n\n      <h3 translate>Reminder to Play Fair</h3>\n      <p class=\"reftext\" translate>As per the <a  [routerLink]=\"['/terms']\">terms and conditions of Belacam</a>, you agree to not rig, cheat, and game the referral system through fake sign-ups or other deceptive methods. Doing so can result in the loss of Bela that you obtained through those methods.</p>\n\n\n      <h3 translate>How will you promote Belacam with your referral link? If you're promoting it on a website or social media accounts, please link us to those so we can preview them.</h3>\n      <textarea class=\"applytextarea\" name=\"message\" rows=\"5\" maxlength=\"2048\"\n            \n                [(ngModel)]=\"applyText\"\n                translate>\n      </textarea>\n\n      <button type=\"submit\" name=\"subscribe\" class=\"long_submitbutton\" translate> Submit </button>\n    </form>\n\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/referral/referral-apply/referral-apply.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReferralApplyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_event_types__ = __webpack_require__("./src/app/core/event.types.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_event_service__ = __webpack_require__("./src/app/core/event.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ReferralApplyComponent = (function () {
    function ReferralApplyComponent(authService, eventService, belaAPIService, router) {
        this.authService = authService;
        this.eventService = eventService;
        this.belaAPIService = belaAPIService;
        this.router = router;
        this.requesting_user = new __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]();
    }
    ReferralApplyComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.requesting_user = this.authService.user;
        this.eventService.subscribe({
            next: function (event) {
                if (event.type == __WEBPACK_IMPORTED_MODULE_4__core_event_types__["b" /* BelaEventType */].UPDATE_USER) {
                    _this.requesting_user = event.value;
                }
            }
        });
    };
    ReferralApplyComponent.prototype.submit = function () {
        var _this = this;
        console.log("on submit");
        this.belaAPIService.createReferralApplication(this.applyText)
            .then(function (res) {
            if (res.status == 'fail') {
                if (res.error['user_id'] && res.error['user_id'][0].indexOf('unique') !== -1) {
                    alert('You already have sent application');
                    _this.router.navigate(['/feed']);
                }
                else {
                    alert(res.message);
                }
            }
            else {
                alert('Your application was successfully sent');
                _this.router.navigate(['/feed']);
            }
        })
            .catch(function (err) {
            alert('Server error. Please try again later');
        });
    };
    return ReferralApplyComponent;
}());
ReferralApplyComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-referral-apply',
        template: __webpack_require__("./src/app/referral/referral-apply/referral-apply.component.html"),
        styles: [__webpack_require__("./src/app/referral/referral-apply/referral-apply.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__core_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_auth_service__["a" /* AuthService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_6__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_event_service__["a" /* EventService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__angular_router__["d" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_router__["d" /* Router */]) === "function" && _d || Object])
], ReferralApplyComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=referral-apply.component.js.map

/***/ }),

/***/ "./src/app/referral/referral-dashboard/referral-dashboard.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/referral/referral-dashboard/referral-dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n\n  <span *ngIf=\"referrer.id; else noReferrer\">\n    <h3 class=\"text-center\">Your Belacam Referral Link</h3>\n    <h3 class=\"reflink\">{{referrer.link}}</h3>\n    <br>\n\n    <div class=\"row refstatrow\">\n\n      <div class=\"refstat refstat1\"><h2>{{referrerStatistics.num_users}}</h2>\n        <ng-template #justone>\n          {{'User'|translate}}\n        </ng-template>\n        <ng-container *ngIf=\"referrerStatistics.num_users>1; else justone\">\n          {{'Users'|translate}}\n        </ng-container> Referred\n      </div>\n\n      <div class=\"refstat\"><h2>{{referrerStatistics.belas_income}}</h2> Bela Earned</div>\n      <div class=\"refstat refstat3\"><h2>${{referrerStatistics.dollar_income | number : '1.2-2' }}</h2> Dollar Value</div>\n\n    </div>\n  </span>\n  <ng-template #noReferrer>\n\n    <span *ngIf=\"referrerApplication; else noApplication\">\n      <h3 class=\"text-center\">Your Belacam Referral Application</h3>\n      <h3 class=\"reflink\">{{referrerApplication.statusText}}</h3>\n      <h4 class=\"text-center\" *ngIf=\"referrerApplication.status == 'DENIED'\">\n        <a href=\"https://belacam.zendesk.com/hc/en-us/articles/360008405992\" translate>\n          Why was I deferred, and when can I reapply?\n        </a>\n      </h4>\n\n    </span>\n\n    <ng-template #noApplication>\n\n      <p class=\"center reftext\">Before you can apply for a referral link, you must complete the following requirements.</p>\n      <ul class=\"referredusers\">\n\n\n        <li><i class=\"fas\"\n               [ngClass]=\"{'fa-times text-red': !requesting_user.id || !requesting_user.verified,\n               'fa-check text-green': requesting_user.id && requesting_user.verified}\"></i> Confirm your Email</li>\n\n\n        <li><i class=\"fas\"\n               [ngClass]=\"{'fa-times text-red': !requesting_user.id || requesting_user.earnings < 1,\n               'fa-check text-green': requesting_user.id && requesting_user.earnings >= 1}\"></i> Lifetime Earnings of $1 or more</li>\n\n        <!-- A total of 50, so 3 deposits of 20 Bela each would change this to a check -->\n        <li><i class=\"fas\"\n               [ngClass]=\"{'fa-times text-red': !requesting_user.id || requesting_user.total_deposit < 50,\n               'fa-check text-green': requesting_user.id && requesting_user.total_deposit >= 50}\"></i> Deposit 50 Bela or more <a [routerLink]=\"['/deposit']\" href=\"#\">to your account</a></li>\n\n        <!-- This should only show if the 3 requirements above are met -->\n        <h3 class=\"text-center\"\n            *ngIf=\"requesting_user.id\n                  && requesting_user.verified\n                  && requesting_user.earnings >= 1\n                  && requesting_user.total_deposit >= 50\">\n          <a [routerLink]=\"'/referral/apply'\">Click here to apply for a referral link</a>\n        </h3>\n      </ul>\n\n    </ng-template>\n\n  </ng-template>\n  <p class=\"reftext\">We are paying you for each person that you refer to Belacam. The more people you refer, the more you earn per referral. Share your Belacam referral link with others, and when they sign up through that link, we will credit Bela to your account.</p>\n\n  <ul class=\"referredusers\">\n    <li class=\"reftitle\"># of Users <span>Payout per Referral</span></li>\n    <li *ngFor=\"let setting of referralSettings\">\n      <ng-container *ngIf=\"setting.range_to\">\n        {{setting.range_from}} - {{setting.range_to}} <span>{{setting.payout}} Bela</span>\n      </ng-container>\n      <ng-container *ngIf=\"!setting.range_to\">\n        {{setting.range_from}}+ <span>{{setting.payout}} Bela</span>\n      </ng-container>\n\n    </li>\n  </ul>\n\n\n  <ul *ngIf=\"referrer.id\" class=\"referredusers\">\n    <li class=\"reftitle\">Referred User <span>Earnings for User</span></li>\n    <p class=\"removeifusers\">Once you refer some users, they will show up here.</p>\n    <li *ngFor=\"let user of referrer.users\">\n      @{{user.username}}\n      <span>{{user.belas}} Bela</span>\n    </li>\n  </ul>\n</div>\n"

/***/ }),

/***/ "./src/app/referral/referral-dashboard/referral-dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReferralDashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_event_types__ = __webpack_require__("./src/app/core/event.types.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_event_service__ = __webpack_require__("./src/app/core/event.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ReferralDashboardComponent = (function () {
    function ReferralDashboardComponent(appService, authService, eventService) {
        this.appService = appService;
        this.authService = authService;
        this.eventService = eventService;
        this.requesting_user = new __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]();
        this.referrer = new __WEBPACK_IMPORTED_MODULE_1__app_model__["c" /* Referrer */]();
        this.referrerStatistics = new __WEBPACK_IMPORTED_MODULE_1__app_model__["e" /* ReferrerStatistics */]();
        this.referralSettings = [];
        this.subscriptions = [];
    }
    ReferralDashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.requesting_user = this.authService.user;
        this.subscriptions.push(this.eventService.subscribe({
            next: function (event) {
                if (event.type == __WEBPACK_IMPORTED_MODULE_4__core_event_types__["b" /* BelaEventType */].UPDATE_USER) {
                    _this.requesting_user = event.value;
                }
            }
        }));
        if (this.authService.isLoggedIn()) {
            this.appService.getReferrer().then(function (obj) {
                if (obj.status == 'success') {
                    _this.referrer = obj.result['referrer'];
                }
            }).catch(function (err) {
                if (err.status === 404) {
                    _this.appService.getReferrerApplication()
                        .then(function (obj) {
                        if (obj.status == 'success') {
                            _this.referrerApplication = __WEBPACK_IMPORTED_MODULE_1__app_model__["d" /* ReferrerApplication */].fromJson(obj.result['application']);
                        }
                    });
                }
            });
            this.appService.getReferrerStatistics().then(function (obj) {
                _this.referrerStatistics = obj;
            });
        }
        this.appService.getReferralSettings().then(function (obj) {
            _this.referralSettings = obj;
        });
    };
    ReferralDashboardComponent.prototype.ngOnDestroy = function () {
        for (var _i = 0, _a = this.subscriptions; _i < _a.length; _i++) {
            var subscription = _a[_i];
            subscription.unsubscribe();
        }
    };
    return ReferralDashboardComponent;
}());
ReferralDashboardComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-referral-dashboard',
        template: __webpack_require__("./src/app/referral/referral-dashboard/referral-dashboard.component.html"),
        styles: [__webpack_require__("./src/app/referral/referral-dashboard/referral-dashboard.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__core_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_auth_service__["a" /* AuthService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_event_service__["a" /* EventService */]) === "function" && _c || Object])
], ReferralDashboardComponent);

var _a, _b, _c;
//# sourceMappingURL=referral-dashboard.component.js.map

/***/ }),

/***/ "./src/app/referral/referral.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferralModule", function() { return ReferralModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__referral_dashboard_referral_dashboard_component__ = __webpack_require__("./src/app/referral/referral-dashboard/referral-dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__referral_apply_referral_apply_component__ = __webpack_require__("./src/app/referral/referral-apply/referral-apply.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__referral_routing__ = __webpack_require__("./src/app/referral/referral.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__ = __webpack_require__("./src/app/shared/shared.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ReferralModule = (function () {
    function ReferralModule() {
    }
    return ReferralModule;
}());
ReferralModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__["a" /* SharedModule */],
            __WEBPACK_IMPORTED_MODULE_4__referral_routing__["a" /* ReferralModuleRouting */]
        ],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__referral_dashboard_referral_dashboard_component__["a" /* ReferralDashboardComponent */],
            __WEBPACK_IMPORTED_MODULE_3__referral_apply_referral_apply_component__["a" /* ReferralApplyComponent */]
        ]
    })
], ReferralModule);

//# sourceMappingURL=referral.module.js.map

/***/ }),

/***/ "./src/app/referral/referral.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReferralModuleRouting; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__referral_dashboard_referral_dashboard_component__ = __webpack_require__("./src/app/referral/referral-dashboard/referral-dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__referral_apply_referral_apply_component__ = __webpack_require__("./src/app/referral/referral-apply/referral-apply.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    { path: 'dashboard', component: __WEBPACK_IMPORTED_MODULE_1__referral_dashboard_referral_dashboard_component__["a" /* ReferralDashboardComponent */] },
    { path: 'apply', component: __WEBPACK_IMPORTED_MODULE_3__referral_apply_referral_apply_component__["a" /* ReferralApplyComponent */] }
];
var ReferralModuleRouting = (function () {
    function ReferralModuleRouting() {
    }
    return ReferralModuleRouting;
}());
ReferralModuleRouting = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["NgModule"])({
        imports: [__WEBPACK_IMPORTED_MODULE_0__angular_router__["e" /* RouterModule */].forChild(routes)],
        exports: [__WEBPACK_IMPORTED_MODULE_0__angular_router__["e" /* RouterModule */]]
    })
], ReferralModuleRouting);

//# sourceMappingURL=referral.routing.js.map

/***/ })

});
//# sourceMappingURL=referral.module.5ba1a2ab4a3d5f2cdcd4.chunk.js.map