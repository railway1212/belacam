webpackJsonp(["main"],{

/***/ "./src/$$_gendir lazy recursive":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"app/referral/referral.module": [
		"./src/app/referral/referral.module.ts",
		"referral.module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_gendir lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/about/about.component.html":
/***/ (function(module, exports) {

module.exports = "<script src=\"https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.js\"></script>\n\n<div class=\"user-profile-contain\">\n\n\n\n  <div class=\"mid-con\">\n    <div class=\"container\">\n      <div class=\"row\">\n\n        <div class=\"col-sm-3 col-lg-3 col-md-3 user-profile-left\">\n        </div>\n\n        <div class=\"col-sm-6 col-lg-6 col-md-6 middle-section\">\n\n          <div class=\"discover-box\">\n            <div class=\"panel with-nav-tabs \">\n\n              <h1 translate>Snap, Post, Paid. It’s that easy.</h1>\n              <h3 translate>On Belacam, a picture of your food can pay for your lunch, and your regular vacation photos can help fund the vacation itself.</h3>\n              <p translate>\n              Belacam is a photo-sharing site like Instagram but with a twist. Each ‘like’ automatically sends a small amount of money from one user to another. Belacam works like any other social media site; users comment, post photos, follow others, and browse feeds, all while earning real money for posting good content. \n\n              </p>\n\n              <p translate>Money is transferred using a cryptotoken called BELA. Once you earn money, you can withdraw it or use it to like other users’ posts. Belacam is a way for everyday users and quality content creators to receive monetary rewards for their hard work and creativity. \n\n              </p>\n\n              <h3 translate>Our Story</h3>\n\n              <p translate>When we set out to make our minimum viable product in early 2017, we needed to know whether or not users would give real money just to like a picture. The answer was a resounding <strong>YES!</strong>\n              </p>\n\n              <p translate>On April 14th, 2017, we launched the open Beta for Belacam at 2:30am from a University of Virginia dorm room with a 2 person team. Within 30 minutes, 250 registered users and more visitors sent our servers crashing to the ground. We allowed 3,500 users to enter the platform last summer and we watched their behavior closely.  We found that photographers, artists, and travelers receive ‘likes’ at a higher rate than others. People who post clearly copyrighted content, memes, or low-quality content tend to receive nothing. Regular users who simply post quality photos of their daily lives certainly do see their fair share of earnings. Through our Beta testing in summer 2017, we learned that people are willing to pay money to give a like. Though the product was rough, it proved our idea. Now, we are ready to take that idea to the next level. </p>\n                \n<h3 translate>Belacam Full Web Launch</h3>\n                \n              <p translate>This is the third version of Belacam, and we are finally ready to expand to the masses. Belacam has been totally overhauled from its 2017 version. We have introduced sidebar content, US Dollar balance estimates, a new discover page, smarter searching, easier registration, location tagging, support for an increasing number of languages, a referral system, photo cropping, swift posting, and beautiful styling. New additions to Belacam also reward users who give likes, making them more discoverable and increasing the chance that they get likes in return. Next features on our agenda include expanded photo editing, stories, and most importantly, an Android and iOS application.</p>\n\n\n              <p translate>While we understand that a mobile app is our true bridge to mass adoption, this full web launch period is incredibly important. As we expand our user-base in this period, it will allow our team to pursue further rounds of funding to get the mobile application ready for release as well as gain invaluable insight into what users need from Belacam. The quicker Belacam’s web version gains traction, the quicker and cleaner the release of the Belacam mobile app will be.</p>\n\n              <p translate>Our dedicated team is ready to make Belacam a viable Instagram competitor. With your help and advice to perfect our product, we know that we can achieve our goals.\n\n              </p>\n\n              <p translate>Thank you for being here, and welcome to Belacam.\n </p>\n\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/about/about.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AboutComponent = (function () {
    function AboutComponent() {
        this.stuff = [1, 2, 3, 4, 5];
    }
    return AboutComponent;
}());
AboutComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("./src/app/about/about.component.html")
    }),
    __metadata("design:paramtypes", [])
], AboutComponent);

//# sourceMappingURL=about.component.js.map

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet>\n\t<div class=\"loading\"></div>\n</router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__("./node_modules/@ngx-translate/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_event_service__ = __webpack_require__("./src/app/core/event.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_event_types__ = __webpack_require__("./src/app/core/event.types.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_loader_service__ = __webpack_require__("./src/app/core/loader.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AppComponent = (function () {
    function AppComponent(loaderService, router, renderer, eventService, translate) {
        // this language will be used as a fallback when a translation isn't found in the current language
        // translate.setDefaultLang('en');
        this.loaderService = loaderService;
        this.router = router;
        this.renderer = renderer;
        this.eventService = eventService;
        // the lang to use, if the lang isn't available, it will use the current loader to get them
        if (userLanguage == "") {
            translate.use("en");
        }
        else {
            translate.use(userLanguage);
        }
        router.events.subscribe(function (val) {
            if (val instanceof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* NavigationStart */]) {
                renderer.removeClass(document.body, 'modal-open');
                renderer.removeStyle(document.body, 'top');
            }
            if (val instanceof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* NavigationEnd */]) {
                window.scroll(0, 0);
            }
        });
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.eventService.subscribe({
            next: function (event) {
                if (event.type == __WEBPACK_IMPORTED_MODULE_5__core_event_types__["b" /* BelaEventType */].ADDED_POST) {
                    if (_this.needNavigateOnPost()) {
                        _this.router.navigate(['/feed']);
                    }
                }
            }
        });
    };
    AppComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    AppComponent.prototype.needNavigateOnPost = function () {
        return this.router.url !== '/feed' && this.router.url.indexOf('user/') === -1;
    };
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__("./src/app/app.component.html"),
        styles: [__webpack_require__("./src/app/app.component.css")],
        providers: [__WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */], __WEBPACK_IMPORTED_MODULE_6__core_loader_service__["a" /* LoaderService */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_6__core_loader_service__["a" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_loader_service__["a" /* LoaderService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer2"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer2"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_event_service__["a" /* EventService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */]) === "function" && _e || Object])
], AppComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "./src/app/app.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Comment */
/* unused harmony export Tag */
/* unused harmony export UserProfile */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return User; });
/* unused harmony export Follow */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return Thought; });
/* unused harmony export ExThought */
/* unused harmony export ThoughtListResult */
/* unused harmony export ListResult */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return LikeBelaResult; });
/* unused harmony export GeoLocation */
/* unused harmony export GeoPlace */
/* unused harmony export UserReferrer */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return Referrer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return ReferrerStatistics; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return ReferrerApplication; });
/* unused harmony export ReferralSetting */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GrowthStatistics; });
/* unused harmony export S3ResourceFields */
/* unused harmony export S3ResourceData */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return S3Resource; });
/* unused harmony export WithdrawFeeSetting */
/* unused harmony export BelaResponse */
/* unused harmony export RewardTask */
/* unused harmony export BelaNotification */
/* unused harmony export BelaNotificationResp */
/* unused harmony export PaginatedBelaNotificationResp */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Comment = (function () {
    function Comment() {
    }
    return Comment;
}());

var Tag = (function () {
    function Tag() {
    }
    return Tag;
}());

var UserProfile = (function () {
    function UserProfile() {
    }
    return UserProfile;
}());

var User = (function () {
    function User() {
        this.user1 = new UserProfile();
        this.is_following = false;
        this.earnings = 0;
        this.album = [];
        this.preview = [];
        this.earning_24hrs = 0.0;
        this.likes_24hrs = 0.0;
        this.is_superuser = false;
        this.verified = false;
    }
    return User;
}());

var Follow = (function () {
    function Follow() {
    }
    return Follow;
}());

var Thought = (function () {
    function Thought() {
        this.user = new User();
        this.likes = [];
        this.views = [];
        this.comments = [];
        this.displayed_comments = [];
        this.show_all_comment = false;
        this.likes_api_url = "";
    }
    Thought.updateDisplayedComments = function (thot, bShowAll) {
        if (bShowAll) {
            thot.displayed_comments = thot.comments.slice();
        }
        else {
            thot.displayed_comments = thot.comments.slice(-2);
        }
        thot.show_all_comment = bShowAll;
    };
    return Thought;
}());

var ExThought = (function (_super) {
    __extends(ExThought, _super);
    function ExThought() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return ExThought;
}(Thought));

var ThoughtListResult = (function () {
    function ThoughtListResult() {
    }
    return ThoughtListResult;
}());

var ListResult = (function () {
    function ListResult() {
    }
    return ListResult;
}());

var LikeBelaResult = (function () {
    function LikeBelaResult() {
    }
    return LikeBelaResult;
}());

LikeBelaResult.E_SAME_USER = 'BL_SAME_USER';
LikeBelaResult.E_INSUFFICIENT_COIN = 'BL_INSUFFICIENT_COIN';
var GeoLocation = (function () {
    function GeoLocation() {
    }
    return GeoLocation;
}());

var GeoPlace = (function () {
    function GeoPlace() {
    }
    return GeoPlace;
}());

var UserReferrer = (function () {
    function UserReferrer() {
    }
    return UserReferrer;
}());

var Referrer = (function () {
    function Referrer() {
        this.users = [];
    }
    return Referrer;
}());

var ReferrerStatistics = (function () {
    function ReferrerStatistics() {
    }
    return ReferrerStatistics;
}());

var ReferrerApplication = (function () {
    function ReferrerApplication() {
    }
    ReferrerApplication.fromJson = function (json) {
        var obj = new ReferrerApplication();
        Object.assign(obj, json);
        return obj;
    };
    Object.defineProperty(ReferrerApplication.prototype, "statusText", {
        get: function () {
            var map = {
                'PENDING': 'Pending',
                'APPROVED': 'Approved',
                'DENIED': 'Deferred'
            };
            if (this.status && map[this.status]) {
                return map[this.status];
            }
            else {
                return this.status;
            }
        },
        enumerable: true,
        configurable: true
    });
    return ReferrerApplication;
}());

var ReferralSetting = (function () {
    function ReferralSetting() {
    }
    return ReferralSetting;
}());

var GrowthStatistics = (function () {
    function GrowthStatistics() {
        this.total_users = 0;
        this.new_users_in_1 = 0;
        this.new_users_in_7 = 0;
    }
    return GrowthStatistics;
}());

var S3ResourceFields = (function () {
    function S3ResourceFields() {
    }
    S3ResourceFields.fromJson = function (json) {
        if (!json) {
            return null;
        }
        var obj = new S3ResourceFields();
        var propMaps = [
            ['policy', 'policy'],
            ['accessKeyId', 'AWSAccessKeyId'],
            ['contentType', 'Content-Type'],
            ['key', 'key'],
            ['signature', 'signature']
        ];
        for (var _i = 0, propMaps_1 = propMaps; _i < propMaps_1.length; _i++) {
            var map = propMaps_1[_i];
            if (json[map[1]] !== undefined) {
                obj[map[0]] = json[map[1]];
            }
        }
        return obj;
    };
    return S3ResourceFields;
}());

var S3ResourceData = (function () {
    function S3ResourceData() {
    }
    S3ResourceData.fromJson = function (json) {
        if (!json) {
            return null;
        }
        var obj = new S3ResourceData();
        obj.url = json.url;
        obj.fields = S3ResourceFields.fromJson(json.fields);
        return obj;
    };
    return S3ResourceData;
}());

var S3Resource = (function () {
    function S3Resource() {
    }
    S3Resource.fromJson = function (json) {
        var obj = new S3Resource();
        obj.url = json.url;
        obj.data = S3ResourceData.fromJson(json.data);
        return obj;
    };
    return S3Resource;
}());

var WithdrawFeeSetting = (function () {
    function WithdrawFeeSetting() {
    }
    return WithdrawFeeSetting;
}());

var BelaResponse = (function () {
    function BelaResponse() {
    }
    return BelaResponse;
}());

var RewardTask = (function () {
    function RewardTask() {
    }
    return RewardTask;
}());

var BelaNotification = (function () {
    function BelaNotification() {
    }
    return BelaNotification;
}());

var BelaNotificationResp = (function () {
    function BelaNotificationResp() {
    }
    return BelaNotificationResp;
}());

var PaginatedBelaNotificationResp = (function () {
    function PaginatedBelaNotificationResp() {
    }
    return PaginatedBelaNotificationResp;
}());

//# sourceMappingURL=app.model.js.map

/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng_recaptcha__ = __webpack_require__("./node_modules/ng-recaptcha/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng_recaptcha___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng_recaptcha__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng_recaptcha_forms__ = __webpack_require__("./node_modules/ng-recaptcha/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng_recaptcha_forms___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng_recaptcha_forms__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_infinite_scroll__ = __webpack_require__("./node_modules/ngx-infinite-scroll/modules/ngx-infinite-scroll.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__notifications_notifications_component__ = __webpack_require__("./src/app/notifications/notifications.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__core_header_header_component__ = __webpack_require__("./src/app/core/header/header.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__user_user_component__ = __webpack_require__("./src/app/user/user.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__app_routing__ = __webpack_require__("./src/app/app.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__discover_discover_component__ = __webpack_require__("./src/app/discover/discover.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__discover_modalimage_component__ = __webpack_require__("./src/app/discover/modalimage.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__tagfeeds_tagfeeds_component__ = __webpack_require__("./src/app/tagfeeds/tagfeeds.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__trending_trending_component__ = __webpack_require__("./src/app/trending/trending.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__comment_comment_component__ = __webpack_require__("./src/app/comment/comment.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__timesince_timesince_component__ = __webpack_require__("./src/app/timesince/timesince.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_ngx_loading__ = __webpack_require__("./node_modules/ngx-loading/ngx-loading/ngx-loading.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_ng_spin_kit__ = __webpack_require__("./node_modules/ng-spin-kit/dist/spinners.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20_ng2_bootstrap_modal__ = __webpack_require__("./node_modules/ng2-bootstrap-modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20_ng2_bootstrap_modal___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_20_ng2_bootstrap_modal__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__homefeed_homefeed_component__ = __webpack_require__("./src/app/homefeed/homefeed.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__search_search_component__ = __webpack_require__("./src/app/search/search.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__posting_form_posting_form_component__ = __webpack_require__("./src/app/posting-form/posting-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__about_about_component__ = __webpack_require__("./src/app/about/about.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__terms_terms_component__ = __webpack_require__("./src/app/terms/terms.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__directory_directory_component__ = __webpack_require__("./src/app/directory/directory.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__privacy_privacy_component__ = __webpack_require__("./src/app/privacy/privacy.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__deposit_deposit_component__ = __webpack_require__("./src/app/deposit/deposit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__withdrawal_withdrawal_component__ = __webpack_require__("./src/app/withdrawal/withdrawal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__comment_comment_form_component__ = __webpack_require__("./src/app/comment/comment-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__typeahead_typeahead_component__ = __webpack_require__("./src/app/typeahead/typeahead.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__not_found_not_found_component__ = __webpack_require__("./src/app/not-found/not-found.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__notifications_notification_page_component__ = __webpack_require__("./src/app/notifications/notification-page.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__deposit_withdrawal_form_deposit_withdrawal_form_component__ = __webpack_require__("./src/app/deposit-withdrawal-form/deposit-withdrawal-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__profile_box_profile_box_component__ = __webpack_require__("./src/app/profile-box/profile-box.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__modals_likes_modal_likes_modal_component__ = __webpack_require__("./src/app/modals/likes-modal/likes-modal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__core_main_layout_main_layout_component__ = __webpack_require__("./src/app/core/main-layout/main-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__core_sidebar_layout_sidebar_layout_component__ = __webpack_require__("./src/app/core/sidebar-layout/sidebar-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__modals_views_modal_views_modal_component__ = __webpack_require__("./src/app/modals/views-modal/views-modal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__modals_likes_modal_wrapper_likes_modal_wrapper_component__ = __webpack_require__("./src/app/modals/likes-modal-wrapper/likes-modal-wrapper.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__modals_views_modal_wrapper_views_modal_wrapper_component__ = __webpack_require__("./src/app/modals/views-modal-wrapper/views-modal-wrapper.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__core_core_module__ = __webpack_require__("./src/app/core/core.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__lightbox_lightbox_overlay_component__ = __webpack_require__("./src/app/lightbox/lightbox-overlay.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__insufficient_bela_insufficient_bela_component__ = __webpack_require__("./src/app/insufficient-bela/insufficient-bela.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__lightbox_lightbox_component__ = __webpack_require__("./src/app/lightbox/lightbox.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__lightbox_album_lightbox_component__ = __webpack_require__("./src/app/lightbox/album-lightbox.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__post_box_post_box_component__ = __webpack_require__("./src/app/post-box/post-box.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__top_search_top_search_component__ = __webpack_require__("./src/app/top-search/top-search.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__shared_shared_module__ = __webpack_require__("./src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__confirmed_confirmed_component__ = __webpack_require__("./src/app/confirmed/confirmed.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__help_help_component__ = __webpack_require__("./src/app/help/help.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__trade_trade_component__ = __webpack_require__("./src/app/trade/trade.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__earn_free_bela_earn_free_bela_component__ = __webpack_require__("./src/app/earn-free-bela/earn-free-bela.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__verify_account_verify_account_component__ = __webpack_require__("./src/app/verify-account/verify-account.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__leaderboards_leaderboards_component__ = __webpack_require__("./src/app/leaderboards/leaderboards.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__landing_page_landing_page_component__ = __webpack_require__("./src/app/landing-page/landing-page.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__password_reset_page_password_reset_page_component__ = __webpack_require__("./src/app/password-reset-page/password-reset-page.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__settings_settings_component__ = __webpack_require__("./src/app/settings/settings.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






























































var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_8__notifications_notifications_component__["a" /* NotificationsComponent */],
            __WEBPACK_IMPORTED_MODULE_9__core_header_header_component__["a" /* HeaderComponent */],
            __WEBPACK_IMPORTED_MODULE_10__user_user_component__["a" /* UserComponent */],
            __WEBPACK_IMPORTED_MODULE_12__discover_discover_component__["a" /* DiscoverComponent */],
            __WEBPACK_IMPORTED_MODULE_12__discover_discover_component__["c" /* PhotoComponent */],
            __WEBPACK_IMPORTED_MODULE_12__discover_discover_component__["d" /* TagsComponent */],
            __WEBPACK_IMPORTED_MODULE_12__discover_discover_component__["b" /* PeopleComponent */],
            __WEBPACK_IMPORTED_MODULE_14__tagfeeds_tagfeeds_component__["a" /* TagfeedsComponent */],
            __WEBPACK_IMPORTED_MODULE_15__trending_trending_component__["c" /* TrendingComponent */],
            __WEBPACK_IMPORTED_MODULE_15__trending_trending_component__["b" /* SuggestedComponent */],
            __WEBPACK_IMPORTED_MODULE_15__trending_trending_component__["a" /* GrowthComponent */],
            __WEBPACK_IMPORTED_MODULE_16__comment_comment_component__["a" /* CommentComponent */],
            __WEBPACK_IMPORTED_MODULE_16__comment_comment_component__["b" /* DynamicHtmlComponent */],
            __WEBPACK_IMPORTED_MODULE_17__timesince_timesince_component__["a" /* TimesinceComponent */],
            __WEBPACK_IMPORTED_MODULE_13__discover_modalimage_component__["a" /* ImageModalComponent */],
            __WEBPACK_IMPORTED_MODULE_21__homefeed_homefeed_component__["a" /* HomefeedComponent */],
            __WEBPACK_IMPORTED_MODULE_22__search_search_component__["b" /* SearchComponent */],
            __WEBPACK_IMPORTED_MODULE_23__posting_form_posting_form_component__["b" /* PostingFormComponent */],
            __WEBPACK_IMPORTED_MODULE_23__posting_form_posting_form_component__["a" /* HeaderPostingFormComponent */],
            __WEBPACK_IMPORTED_MODULE_22__search_search_component__["a" /* MobileSearchComponent */],
            __WEBPACK_IMPORTED_MODULE_24__about_about_component__["a" /* AboutComponent */],
            __WEBPACK_IMPORTED_MODULE_27__privacy_privacy_component__["a" /* PrivacyComponent */],
            __WEBPACK_IMPORTED_MODULE_28__deposit_deposit_component__["a" /* DepositComponent */],
            __WEBPACK_IMPORTED_MODULE_29__withdrawal_withdrawal_component__["a" /* WithdrawalComponent */],
            __WEBPACK_IMPORTED_MODULE_25__terms_terms_component__["a" /* TermsComponent */],
            __WEBPACK_IMPORTED_MODULE_26__directory_directory_component__["a" /* DirectoryComponent */],
            __WEBPACK_IMPORTED_MODULE_30__comment_comment_form_component__["a" /* CommentFormComponent */],
            __WEBPACK_IMPORTED_MODULE_31__typeahead_typeahead_component__["a" /* TypeaheadComponent */],
            __WEBPACK_IMPORTED_MODULE_32__not_found_not_found_component__["a" /* NotFoundComponent */],
            __WEBPACK_IMPORTED_MODULE_33__notifications_notification_page_component__["a" /* NotificationPageComponent */],
            __WEBPACK_IMPORTED_MODULE_34__deposit_withdrawal_form_deposit_withdrawal_form_component__["a" /* DepositWithdrawalFormComponent */],
            __WEBPACK_IMPORTED_MODULE_36__profile_box_profile_box_component__["a" /* ProfileBoxComponent */],
            __WEBPACK_IMPORTED_MODULE_37__modals_likes_modal_likes_modal_component__["a" /* LikesModalComponent */],
            __WEBPACK_IMPORTED_MODULE_38__core_main_layout_main_layout_component__["a" /* MainLayoutComponent */],
            __WEBPACK_IMPORTED_MODULE_39__core_sidebar_layout_sidebar_layout_component__["a" /* SidebarLayoutComponent */],
            __WEBPACK_IMPORTED_MODULE_40__modals_views_modal_views_modal_component__["a" /* ViewsModalComponent */],
            __WEBPACK_IMPORTED_MODULE_41__modals_likes_modal_wrapper_likes_modal_wrapper_component__["a" /* LikesModalWrapperComponent */],
            __WEBPACK_IMPORTED_MODULE_42__modals_views_modal_wrapper_views_modal_wrapper_component__["a" /* ViewsModalWrapperComponent */],
            __WEBPACK_IMPORTED_MODULE_46__lightbox_lightbox_component__["a" /* LightboxComponent */],
            __WEBPACK_IMPORTED_MODULE_44__lightbox_lightbox_overlay_component__["a" /* LightboxOverlayComponent */],
            __WEBPACK_IMPORTED_MODULE_47__lightbox_album_lightbox_component__["a" /* AlbumLightboxComponent */],
            __WEBPACK_IMPORTED_MODULE_45__insufficient_bela_insufficient_bela_component__["a" /* InsufficientBelaComponent */],
            __WEBPACK_IMPORTED_MODULE_48__post_box_post_box_component__["a" /* PostBoxComponent */],
            __WEBPACK_IMPORTED_MODULE_49__top_search_top_search_component__["a" /* TopSearchComponent */],
            __WEBPACK_IMPORTED_MODULE_51__confirmed_confirmed_component__["a" /* ConfirmedComponent */],
            __WEBPACK_IMPORTED_MODULE_52__help_help_component__["a" /* HelpComponent */],
            __WEBPACK_IMPORTED_MODULE_53__trade_trade_component__["a" /* TradeComponent */],
            __WEBPACK_IMPORTED_MODULE_54__earn_free_bela_earn_free_bela_component__["a" /* EarnFreeBelaComponent */],
            __WEBPACK_IMPORTED_MODULE_55__verify_account_verify_account_component__["a" /* VerifyAccountComponent */],
            __WEBPACK_IMPORTED_MODULE_56__leaderboards_leaderboards_component__["a" /* LeaderboardsComponent */],
            __WEBPACK_IMPORTED_MODULE_57__landing_page_landing_page_component__["a" /* LandingPageComponent */],
            __WEBPACK_IMPORTED_MODULE_58__password_reset_page_password_reset_page_component__["b" /* PasswordResetPageComponent */],
            __WEBPACK_IMPORTED_MODULE_58__password_reset_page_password_reset_page_component__["a" /* PasswordResetConfirmPageComponent */],
            __WEBPACK_IMPORTED_MODULE_59__settings_settings_component__["b" /* SettingsComponent */],
            __WEBPACK_IMPORTED_MODULE_59__settings_settings_component__["a" /* ConfirmComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["e" /* RouterModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["d" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_11__app_routing__["a" /* AppModuleRouting */],
            __WEBPACK_IMPORTED_MODULE_18_ngx_loading__["a" /* LoadingModule */],
            __WEBPACK_IMPORTED_MODULE_6_ngx_infinite_scroll__["a" /* InfiniteScrollModule */],
            __WEBPACK_IMPORTED_MODULE_19_ng_spin_kit__["a" /* NgSpinKitModule */],
            __WEBPACK_IMPORTED_MODULE_20_ng2_bootstrap_modal__["BootstrapModalModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["e" /* JsonpModule */],
            __WEBPACK_IMPORTED_MODULE_35__angular_common_http__["a" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_43__core_core_module__["a" /* CoreModule */],
            __WEBPACK_IMPORTED_MODULE_50__shared_shared_module__["a" /* SharedModule */],
            __WEBPACK_IMPORTED_MODULE_4_ng_recaptcha__["RecaptchaModule"].forRoot(),
            __WEBPACK_IMPORTED_MODULE_5_ng_recaptcha_forms__["RecaptchaFormsModule"],
        ],
        exports: [__WEBPACK_IMPORTED_MODULE_16__comment_comment_component__["a" /* CommentComponent */]],
        schemas: [
            __WEBPACK_IMPORTED_MODULE_1__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_31__typeahead_typeahead_component__["a" /* TypeaheadComponent */],
            __WEBPACK_IMPORTED_MODULE_37__modals_likes_modal_likes_modal_component__["a" /* LikesModalComponent */],
            __WEBPACK_IMPORTED_MODULE_44__lightbox_lightbox_overlay_component__["a" /* LightboxOverlayComponent */],
            __WEBPACK_IMPORTED_MODULE_46__lightbox_lightbox_component__["a" /* LightboxComponent */],
            __WEBPACK_IMPORTED_MODULE_47__lightbox_album_lightbox_component__["a" /* AlbumLightboxComponent */],
            __WEBPACK_IMPORTED_MODULE_59__settings_settings_component__["a" /* ConfirmComponent */]
        ],
        providers: [
            {
                provide: __WEBPACK_IMPORTED_MODULE_4_ng_recaptcha__["RECAPTCHA_SETTINGS"],
                useValue: { siteKey: "6LfCvVUUAAAAAFLF7zPTJ0iaWF_kSrGySSvSd7uW" },
            },
        ],
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "./src/app/app.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModuleRouting; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_user_component__ = __webpack_require__("./src/app/user/user.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__discover_discover_component__ = __webpack_require__("./src/app/discover/discover.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tagfeeds_tagfeeds_component__ = __webpack_require__("./src/app/tagfeeds/tagfeeds.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__timesince_timesince_component__ = __webpack_require__("./src/app/timesince/timesince.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__homefeed_homefeed_component__ = __webpack_require__("./src/app/homefeed/homefeed.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__about_about_component__ = __webpack_require__("./src/app/about/about.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__terms_terms_component__ = __webpack_require__("./src/app/terms/terms.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__directory_directory_component__ = __webpack_require__("./src/app/directory/directory.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__privacy_privacy_component__ = __webpack_require__("./src/app/privacy/privacy.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__deposit_deposit_component__ = __webpack_require__("./src/app/deposit/deposit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__withdrawal_withdrawal_component__ = __webpack_require__("./src/app/withdrawal/withdrawal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__not_found_not_found_component__ = __webpack_require__("./src/app/not-found/not-found.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__notifications_notification_page_component__ = __webpack_require__("./src/app/notifications/notification-page.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__top_search_top_search_component__ = __webpack_require__("./src/app/top-search/top-search.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__core_main_layout_main_layout_component__ = __webpack_require__("./src/app/core/main-layout/main-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__core_sidebar_layout_sidebar_layout_component__ = __webpack_require__("./src/app/core/sidebar-layout/sidebar-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__confirmed_confirmed_component__ = __webpack_require__("./src/app/confirmed/confirmed.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__help_help_component__ = __webpack_require__("./src/app/help/help.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__trade_trade_component__ = __webpack_require__("./src/app/trade/trade.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__verify_account_verify_account_component__ = __webpack_require__("./src/app/verify-account/verify-account.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__leaderboards_leaderboards_component__ = __webpack_require__("./src/app/leaderboards/leaderboards.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__landing_page_landing_page_component__ = __webpack_require__("./src/app/landing-page/landing-page.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__settings_settings_component__ = __webpack_require__("./src/app/settings/settings.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__password_reset_page_password_reset_page_component__ = __webpack_require__("./src/app/password-reset-page/password-reset-page.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


























var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_23__landing_page_landing_page_component__["a" /* LandingPageComponent */] },
    { path: '', component: __WEBPACK_IMPORTED_MODULE_16__core_main_layout_main_layout_component__["a" /* MainLayoutComponent */], children: [
            { path: '', component: __WEBPACK_IMPORTED_MODULE_17__core_sidebar_layout_sidebar_layout_component__["a" /* SidebarLayoutComponent */], children: [
                    { path: 'feed', component: __WEBPACK_IMPORTED_MODULE_6__homefeed_homefeed_component__["a" /* HomefeedComponent */] },
                    { path: 'verify', component: __WEBPACK_IMPORTED_MODULE_21__verify_account_verify_account_component__["a" /* VerifyAccountComponent */] },
                    { path: 'confirmed', component: __WEBPACK_IMPORTED_MODULE_18__confirmed_confirmed_component__["a" /* ConfirmedComponent */] },
                    { path: 'deposit', component: __WEBPACK_IMPORTED_MODULE_11__deposit_deposit_component__["a" /* DepositComponent */] },
                    { path: 'discover', component: __WEBPACK_IMPORTED_MODULE_3__discover_discover_component__["a" /* DiscoverComponent */] },
                    { path: 'discover/:tagname', component: __WEBPACK_IMPORTED_MODULE_4__tagfeeds_tagfeeds_component__["a" /* TagfeedsComponent */] },
                    { path: 'feed', component: __WEBPACK_IMPORTED_MODULE_6__homefeed_homefeed_component__["a" /* HomefeedComponent */] },
                    { path: 'help', component: __WEBPACK_IMPORTED_MODULE_19__help_help_component__["a" /* HelpComponent */] },
                    { path: 'leaderboards', component: __WEBPACK_IMPORTED_MODULE_22__leaderboards_leaderboards_component__["a" /* LeaderboardsComponent */] },
                    { path: 'notifications', component: __WEBPACK_IMPORTED_MODULE_14__notifications_notification_page_component__["a" /* NotificationPageComponent */] },
                    { path: 'post/:username/:post_id', component: __WEBPACK_IMPORTED_MODULE_5__timesince_timesince_component__["a" /* TimesinceComponent */] },
                    { path: 'referral', loadChildren: 'app/referral/referral.module#ReferralModule' },
                    { path: 'top_search', component: __WEBPACK_IMPORTED_MODULE_15__top_search_top_search_component__["a" /* TopSearchComponent */] },
                    { path: 'top_search/:tagname', component: __WEBPACK_IMPORTED_MODULE_15__top_search_top_search_component__["a" /* TopSearchComponent */] },
                    { path: 'trade', component: __WEBPACK_IMPORTED_MODULE_20__trade_trade_component__["a" /* TradeComponent */] },
                    { path: 'withdrawal', component: __WEBPACK_IMPORTED_MODULE_12__withdrawal_withdrawal_component__["a" /* WithdrawalComponent */] },
                ]
            },
            { path: 'password_reset', component: __WEBPACK_IMPORTED_MODULE_25__password_reset_page_password_reset_page_component__["b" /* PasswordResetPageComponent */] },
            { path: 'reset/:uidb64/:token', component: __WEBPACK_IMPORTED_MODULE_25__password_reset_page_password_reset_page_component__["a" /* PasswordResetConfirmPageComponent */] },
            { path: 'settings', component: __WEBPACK_IMPORTED_MODULE_24__settings_settings_component__["b" /* SettingsComponent */] },
            { path: 'directory', component: __WEBPACK_IMPORTED_MODULE_9__directory_directory_component__["a" /* DirectoryComponent */] },
            { path: 'privacy', component: __WEBPACK_IMPORTED_MODULE_10__privacy_privacy_component__["a" /* PrivacyComponent */] },
            { path: 'about', component: __WEBPACK_IMPORTED_MODULE_7__about_about_component__["a" /* AboutComponent */] },
            { path: 'terms', component: __WEBPACK_IMPORTED_MODULE_8__terms_terms_component__["a" /* TermsComponent */] },
            { path: 'user/:username', component: __WEBPACK_IMPORTED_MODULE_2__user_user_component__["a" /* UserComponent */] },
        ] },
    { path: '404', component: __WEBPACK_IMPORTED_MODULE_13__not_found_not_found_component__["a" /* NotFoundComponent */] },
    { path: '**', redirectTo: '/404' }
];
var AppModuleRouting = (function () {
    function AppModuleRouting() {
    }
    return AppModuleRouting;
}());
AppModuleRouting = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["e" /* RouterModule */].forRoot(routes)],
        exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["e" /* RouterModule */]]
    })
], AppModuleRouting);

//# sourceMappingURL=app.routing.js.map

/***/ }),

/***/ "./src/app/comment/comment-form.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/comment/comment-form.component.html":
/***/ (function(module, exports) {

module.exports = "<span class=\"circlephoto commentphoto\" *ngIf=\"requesting_user\" [innerHTML]=\"requesting_user.avatar\"></span>\n<form class=\"commentform\" *ngIf=\"requesting_user\">\n  <textarea class=\"comment autoExpand\"\n            name=\"comment\"\n            #commentText\n            placeholder=\"{{'Add a comment...'|translate}}\"\n            [(ngModel)]=\"comment_text\"\n            typeahead\n            ></textarea>\n\n  <div>\n    <ngx-loading [show]=\"loading\"></ngx-loading>\n  </div>\n  <label class=\"commentsubmit\"  (click)=\"addCommentToPost(thot);false\" translate>Post\n  </label>\n</form>\n"

/***/ }),

/***/ "./src/app/comment/comment-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommentFormComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__search_search_service__ = __webpack_require__("./src/app/search/search.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__typeahead_typeahead_component__ = __webpack_require__("./src/app/typeahead/typeahead.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CommentFormComponent = (function () {
    function CommentFormComponent(appService, searchService) {
        this.appService = appService;
        this.searchService = searchService;
        this.thot = new __WEBPACK_IMPORTED_MODULE_1__app_model__["g" /* Thought */]();
        this.success = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.requesting_user = new __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]();
        this.comment_text = "";
        this.loading = false;
    }
    CommentFormComponent.prototype.ngOnInit = function () {
        this.requesting_user = JSON.parse(localStorage.getItem('user'));
    };
    CommentFormComponent.prototype.addCommentToPost = function () {
        var _this = this;
        this.loading = true;
        if (this.comment_text.trim() != "") {
            this.appService.createComment(this.thot.id, this.comment_text.trim()).then(function (commentResp) {
                _this.loading = false;
                _this.success.emit(commentResp);
                // this.thot.displayed_comments.unshift(commentResp);
                _this.comment_text = "";
            });
        }
        else {
            this.loading = false;
        }
    };
    return CommentFormComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], CommentFormComponent.prototype, "thot", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
], CommentFormComponent.prototype, "success", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('commentText'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__typeahead_typeahead_component__["a" /* TypeaheadComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__typeahead_typeahead_component__["a" /* TypeaheadComponent */]) === "function" && _b || Object)
], CommentFormComponent.prototype, "commentText", void 0);
CommentFormComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-comment-form',
        template: __webpack_require__("./src/app/comment/comment-form.component.html"),
        styles: [__webpack_require__("./src/app/comment/comment-form.component.css")],
        providers: [__WEBPACK_IMPORTED_MODULE_3__search_search_service__["a" /* SearchService */]]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__search_search_service__["a" /* SearchService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__search_search_service__["a" /* SearchService */]) === "function" && _d || Object])
], CommentFormComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=comment-form.component.js.map

/***/ }),

/***/ "./src/app/comment/comment.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/comment/comment.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"thot.comments.length>2 && !thot.show_all_comment\" class=\"viewcoment-box\">\n  <a href=\"#\" (click)=\"viewAllComments();false;\" translate>View all comments  +</a>\n</div>\n\n<div *ngFor=\"let comment of thot.displayed_comments\" class=\"reply-box reply-box2\">\n\n  <a href=\"#\" [routerLink]=\"['/user/', comment.author.username]\">\n    <div class=\"reply-box-left circlephoto\">\n      <span [innerHTML]=\"comment.author.avatar\"></span>\n    </div>\n  </a>\n\n  <div class=\"reply-box-right\" *ngIf=\"!comment_in_edit || comment.id != comment_in_edit.id; else replyEditBox\">\n    <h4 class=\"commenttext\">\n      <a class=\"commentuserlink\" href=\"#\" [routerLink]=\"['/user/', comment.author.username]\">\n        {{ comment.author.username }}\n      </a>\n      <span class=\"commentspan\">\n        <app-dynamic-html [innerHTML]=\"comment.text|tagify\"> </app-dynamic-html>\n      </span>\n    </h4>\n    <h5>{{timeService | async | messageTime: comment.created_at}}</h5>\n  </div>\n\n  <ng-template #replyEditBox>\n    <div class=\"reply-edit-box\">\n      <textarea class=\"autoExpand comment\" title=\"Comment\" [(ngModel)]=\"comment_text\" typeahead rows=\"1\">\n      </textarea>\n    </div>\n  </ng-template>\n\n  <div class=\"reply-box-actions\"\n       [ngClass]=\"{'comment-edit-mode': comment_in_edit && comment.id == comment_in_edit.id }\"\n       [hidden]=\"!requesting_user || thot.user.id !== requesting_user.id\">\n    <a href=\"#\" class=\"btn btn-reply-action\" (click)=\"enterEditMode(comment); false\"\n       *ngIf=\"!comment_in_edit || comment.id != comment_in_edit.id; else actionInEdit\">\n      <i class=\"glyphicon glyphicon-pencil\"></i>\n    </a>\n\n    <ng-template #actionInEdit>\n      <a href=\"#\" class=\"btn btn-reply-action\" (click)=\"finishEdit(comment); false\">\n        <i class=\"glyphicon glyphicon-ok\"></i>\n      </a>\n      <a href=\"#\" class=\"btn btn-reply-action\" (click)=\"cancelEdit(comment); false\">\n        <i class=\"glyphicon glyphicon-remove\"></i>\n      </a>\n    </ng-template>\n\n    <a href=\"#\" class=\"btn btn-reply-action\" (click)=\"deleteComment(comment); false\">\n      <i class=\"glyphicon glyphicon-trash\"></i>\n    </a>\n  </div>\n</div>\n\n<app-comment-form [thot]=\"thot\" *ngIf=\"!hideForm\" (success)=\"onAddCommentToPost($event)\">\n</app-comment-form>\n"

/***/ }),

/***/ "./src/app/comment/comment.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommentComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return DynamicHtmlComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__constants__ = __webpack_require__("./src/app/constants.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CommentComponent = (function () {
    function CommentComponent(appService) {
        this.appService = appService;
        this.thot = new __WEBPACK_IMPORTED_MODULE_2__app_model__["g" /* Thought */]();
        this.hideForm = false;
        this.loading = false;
        this.requesting_user = new __WEBPACK_IMPORTED_MODULE_2__app_model__["h" /* User */]();
        this.comment_text = "";
        this.comment_in_edit = null;
        this.timeService = __WEBPACK_IMPORTED_MODULE_4__constants__["a" /* Constants */].timeService;
        this.comment_in_edit = null;
    }
    CommentComponent.prototype.ngOnInit = function () {
        this.thot.displayed_comments = this.thot.comments.slice(-2);
        this.requesting_user = JSON.parse(localStorage.getItem('user'));
    };
    CommentComponent.prototype.onAddCommentToPost = function (comment) {
        this.thot.displayed_comments.push(comment);
        this.thot.comments.push(comment);
    };
    CommentComponent.prototype.viewAllComments = function () {
        __WEBPACK_IMPORTED_MODULE_2__app_model__["g" /* Thought */].updateDisplayedComments(this.thot, true);
    };
    CommentComponent.prototype.hideComments = function () {
        __WEBPACK_IMPORTED_MODULE_2__app_model__["g" /* Thought */].updateDisplayedComments(this.thot, false);
        this.comment_in_edit = null;
    };
    CommentComponent.prototype.enterEditMode = function (comment) {
        this.comment_in_edit = comment;
        this.comment_text = comment.text;
    };
    CommentComponent.prototype.finishEdit = function (comment) {
        var _this = this;
        this.appService.updateComment(comment, { text: this.comment_text }).then(function () {
            _this.comment_in_edit.text = _this.comment_text;
            _this.comment_in_edit = null;
        }).catch(function () {
        });
    };
    CommentComponent.prototype.cancelEdit = function (comment) {
        this.comment_in_edit = null;
    };
    CommentComponent.prototype.deleteComment = function (comment) {
        var _this = this;
        this.appService.deleteComment(comment).then(function () {
            _this.comment_in_edit = null;
            // TODO: remove comment from displayed_comments list as well as thot comments
            var idxFrom = _this.thot.comments.indexOf(comment);
            if (idxFrom != -1) {
                _this.thot.comments.splice(idxFrom, 1);
            }
            __WEBPACK_IMPORTED_MODULE_2__app_model__["g" /* Thought */].updateDisplayedComments(_this.thot, _this.thot.show_all_comment);
        }).catch(function () {
        });
        this.comment_in_edit = null;
    };
    return CommentComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], CommentComponent.prototype, "thot", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], CommentComponent.prototype, "hideForm", void 0);
CommentComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-comment',
        template: __webpack_require__("./src/app/comment/comment.component.html"),
        styles: [__webpack_require__("./src/app/comment/comment.component.css")],
        providers: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _a || Object])
], CommentComponent);

var DynamicHtmlComponent = (function () {
    function DynamicHtmlComponent(router, el, renderer) {
        this.router = router;
        this.el = el;
        this.renderer = renderer;
        this.clickListeners = [];
    }
    DynamicHtmlComponent.prototype.ngOnInit = function () {
    };
    DynamicHtmlComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        var anchorNodes = this.el.nativeElement.querySelectorAll('a.ng-link');
        var anchors = Array.from(anchorNodes);
        anchors.forEach(function (anchor) {
            var listener = _this.renderer.listen(anchor, 'click', function (e) {
                var href;
                e.preventDefault();
                if (e.srcElement) {
                    href = e.srcElement.getAttribute('href');
                }
                else {
                    href = e.target.getAttribute('href');
                }
                _this.router.navigateByUrl(href);
            });
            _this.clickListeners.push(listener);
        });
    };
    return DynamicHtmlComponent;
}());
DynamicHtmlComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-dynamic-html',
        template: '<ng-content></ng-content>'
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["d" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["d" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer2"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer2"]) === "function" && _d || Object])
], DynamicHtmlComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=comment.component.js.map

/***/ }),

/***/ "./src/app/confirmed/confirmed.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/confirmed/confirmed.component.html":
/***/ (function(module, exports) {

module.exports = "<i class=\"fas fa-check confirmedicon\"></i>\n\n<h1 class=\"text-center\" translate>\n  Your account is claimed — Now share Belacam with friends!\n</h1>\n\n<p class=\"text-center\" translate>On July 20th, we will email you to give you $1 in Bela and let you finish making your account. Tell your friends about Belacam so they can get paid too!</p>\n\n<br>\n<h2 class=\"text-center\" translate>While you Wait</h2>\n<p class=\"text-center\" translate>Thousands of people just like you are waiting for Belacam to launch. Here are some things to do while you wait:</p>\n<ol class=\"confirmedol\">\n<li translate><a href=\"https://www.coinexchange.io/market/BELA/BTC\">Buy some Bela</a> — Bela is the cryptocurrency that powers Belacam. You're gifted $1 of Bela on July 20th, but if you want more, you'll have to buy it or earn it.</li>\n<li translate><a href=\"https://t.me/letslivebela\"> Join the Bela Telegram Chat</a> — Talk with other Belacam future users.</li>\n<li translate><a [routerLink]=\"['/about']\"> Read more about Belacam</a></li>\n<li translate><a routerLink=\"/discover\"> Explore Belacam</a> — Nearly 200 Early-Access Testers have filled the site in a live setting with great content for you to explore.</li>\n\n\n</ol>\n"

/***/ }),

/***/ "./src/app/confirmed/confirmed.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfirmedComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ConfirmedComponent = (function () {
    function ConfirmedComponent() {
    }
    ConfirmedComponent.prototype.ngOnInit = function () {
    };
    return ConfirmedComponent;
}());
ConfirmedComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-confirmed',
        template: __webpack_require__("./src/app/confirmed/confirmed.component.html"),
        styles: [__webpack_require__("./src/app/confirmed/confirmed.component.css")]
    }),
    __metadata("design:paramtypes", [])
], ConfirmedComponent);

//# sourceMappingURL=confirmed.component.js.map

/***/ }),

/***/ "./src/app/constants.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Constants; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");

var Constants = (function () {
    function Constants() {
    }
    return Constants;
}());

Constants.root_dir = '/api/';
Constants.default_cover_pic = 'https://s3.amazonaws.com/bellassets/assets/images/c-pic.jpg';
Constants.default_remote_asset_url_base = 'https://s3.amazonaws.com/bellassets';
Constants.timeService = new __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__["a" /* Observable */](function (observer) {
    observer.next(new Date());
    setInterval(function () { return observer.next(new Date()); }, 600000);
});
//# sourceMappingURL=constants.js.map

/***/ }),

/***/ "./src/app/core/auth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/_esm5/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/toPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__event_types__ = __webpack_require__("./src/app/core/event.types.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__event_service__ = __webpack_require__("./src/app/core/event.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AuthService = (function () {
    function AuthService(http, eventService) {
        this.http = http;
        this.eventService = eventService;
        this.isReady = false;
        this.user = new __WEBPACK_IMPORTED_MODULE_3__app_model__["h" /* User */]();
        this.eventEmitter = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["a" /* Subject */]();
        this.loadUserDetail();
    }
    AuthService.prototype.isServiceReady = function () {
        return this.isReady;
    };
    AuthService.prototype.isLoggedIn = function () {
        var user_id = jQuery('input[name="requesting_user_id"]').val();
        return !!user_id;
    };
    AuthService.prototype.getUser = function () {
        return this.user;
    };
    AuthService.prototype.updateUser = function (userData) {
        localStorage.setItem('user', JSON.stringify(userData));
        this.user = userData;
        var event = new __WEBPACK_IMPORTED_MODULE_4__event_types__["a" /* BelaEvent */]();
        event.type = __WEBPACK_IMPORTED_MODULE_4__event_types__["b" /* BelaEventType */].UPDATE_USER;
        event.value = this.user;
        this.eventService.fireEvent(event);
    };
    AuthService.prototype.getUserId = function () {
        return jQuery('input[name="requesting_user_id"]').val();
    };
    AuthService.prototype.getUserName = function () {
        return jQuery('input[name="requesting_user_username"]').val();
    };
    AuthService.prototype.loadUserDetail = function () {
        var _this = this;
        var userId = jQuery('input[name="requesting_user_id"]').val();
        var userName = jQuery('input[name="requesting_user_username"]').val();
        if (userName) {
            var userDataUrl = '/api/user/' + userName + '/';
            return this.http.get(userDataUrl)
                .toPromise()
                .then(function (response) { return response.json(); })
                .then(function (res) {
                _this.updateUser(res);
                _this.isReady = true;
            })
                .catch((function (err) {
            }));
        }
        else {
            var event = new __WEBPACK_IMPORTED_MODULE_4__event_types__["a" /* BelaEvent */]();
            event.type = __WEBPACK_IMPORTED_MODULE_4__event_types__["b" /* BelaEventType */].NO_AUTH;
            this.isReady = true;
            this.eventService.fireEvent(event);
            localStorage.removeItem('user');
        }
    };
    AuthService.prototype.subscribe = function (observer) {
        return this.eventEmitter.subscribe(observer);
    };
    AuthService.prototype.logout = function () {
        jQuery('input[name="requesting_user_id"]').val('');
        jQuery('input[name="requesting_user_username"]').val('');
        var event = new __WEBPACK_IMPORTED_MODULE_4__event_types__["a" /* BelaEvent */]();
        event.type = __WEBPACK_IMPORTED_MODULE_4__event_types__["b" /* BelaEventType */].NO_AUTH;
        this.isReady = true;
        delete this.user;
        this.user = new __WEBPACK_IMPORTED_MODULE_3__app_model__["h" /* User */]();
        this.eventEmitter.next(event);
        localStorage.removeItem('user');
    };
    return AuthService;
}());
AuthService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_http__["c" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_6__event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__event_service__["a" /* EventService */]) === "function" && _b || Object])
], AuthService);

var _a, _b;
//# sourceMappingURL=auth.service.js.map

/***/ }),

/***/ "./src/app/core/awss3-api/awss3-api.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AWSS3ApiService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AWSS3ApiService = (function () {
    function AWSS3ApiService(http) {
        this.http = http;
    }
    AWSS3ApiService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    AWSS3ApiService.prototype.uploadFileToS3 = function (_resource, _file) {
        return this.uploadBlobToS3(_resource, _file, _file.name);
    };
    AWSS3ApiService.prototype.uploadBlobToS3 = function (_resource, data, filename) {
        var url = _resource.data.url;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        headers.append("x-amz-acl", "public-read");
        var formData = new FormData();
        formData.append('AWSAccessKeyId', _resource.data.fields.accessKeyId);
        formData.append('Content-Type', _resource.data.fields.contentType);
        formData.append('key', _resource.data.fields.key);
        formData.append('policy', _resource.data.fields.policy);
        formData.append('signature', _resource.data.fields.signature);
        formData.append('file', data, filename);
        return this.http.post(url, formData, {
            headers: headers
        }).toPromise()
            .then(function (res) {
            if (res.status == 204) {
                return true;
            }
        })
            .catch(this.handleError);
    };
    return AWSS3ApiService;
}());
AWSS3ApiService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], AWSS3ApiService);

var _a;
//# sourceMappingURL=awss3-api.service.js.map

/***/ }),

/***/ "./src/app/core/bela-api.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BelaApiService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/toPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__event_service__ = __webpack_require__("./src/app/core/event.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var vrouter;
function handleErrorPromise(error) {
    handleError(error);
    return Promise.reject(error.message || error);
}
function handleError(error) {
    if (error.status === 403) {
        vrouter.navigate(['']);
    }
}
var BelaApiService = (function () {
    function BelaApiService(http, router, eventService, authService) {
        this.http = http;
        this.router = router;
        this.eventService = eventService;
        this.authService = authService;
        this.responseData = { success: false };
        vrouter = router;
    }
    BelaApiService.prototype.signin = function (username, password) {
        var signinUrl = '/api/sign-in/';
        var form = new FormData();
        form.append('username', username);
        form.append('password', password);
        return this.http.post(signinUrl, form)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.signup = function (email, username, password, captchaResponse) {
        var signupUrl = '/api/sign-up/';
        var form = new FormData();
        form.append('email', email);
        form.append('username', username);
        form.append('password', password);
        form.append('password_confirm', password);
        form.append('g-recaptcha-response', captchaResponse);
        return this.http.post(signupUrl, form)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.reset_password = function (email) {
        var resetPasswordUrl = '/api/reset-password';
        var form = new FormData();
        form.append('email', email);
        return this.http.post(resetPasswordUrl, form)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.check_reset_password_link = function (uidb64, token) {
        var resetPasswordLinkUrl = '/api/check-reset-password-link';
        var form = new FormData();
        form.append('uidb64', uidb64);
        form.append('token', token);
        return this.http.post(resetPasswordLinkUrl, form)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.reset_password_confirm = function (password_new, password_new_confirm, uidb64, token) {
        var resetPasswordConfirmUrl = '/api/reset-password-confirm';
        var form = new FormData();
        form.append('new_password1', password_new);
        form.append('new_password2', password_new_confirm);
        form.append('uidb64', uidb64);
        form.append('token', token);
        return this.http.post(resetPasswordConfirmUrl, form)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.logout = function () {
        var signinUrl = '/api/logout/';
        return this.http.post(signinUrl, {})
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.getSettingDetails = function () {
        var settingDetailsUrl = '/api/setting_details/';
        return this.http.post(settingDetailsUrl, {})
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.updateAccount = function (username, email, language, send_weekly_email, csrf_token) {
        var updateAccountUrl = '/api/settings/update_account';
        var form = new FormData();
        form.append('username', username);
        form.append('email', email);
        form.append('language', language);
        form.append('send_weekly_email', send_weekly_email.toString());
        form.append('csrfmiddlewaretoken', csrf_token);
        return this.http.post(updateAccountUrl, form)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.resend_verify_email = function () {
        var resendVerifyEmailUrl = '/api/settings/resend_email';
        return this.http.post(resendVerifyEmailUrl, {})
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.updatePassword = function (password_current, password_new, password_new_confirm, csrf_token) {
        var updatePasswordUrl = '/api/settings/update_password';
        var form = new FormData();
        form.append('password_current', password_current);
        form.append('password_new', password_new);
        form.append('password_new_confirm', password_new_confirm);
        form.append('csrfmiddlewaretoken', csrf_token);
        return this.http.post(updatePasswordUrl, form)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.deleteAccount = function () {
        var deleteAccountUrl = '/api/settings/deleteAccount';
        return this.http.post(deleteAccountUrl, {})
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.getUserDetail = function (username) {
        var userDataUrl = '/api/user/' + username + '/';
        return this.http.get(userDataUrl)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.updateUsername = function (user, username) {
        var nameSaveUrl = '/api/usernames/' + user.id + '/';
        return this.http.patch(nameSaveUrl, JSON.stringify({ username: username }), new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* RequestOptions */]({ headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/json' }) }))
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.updateUserProfile = function (user, data) {
        var url = "/api/user/" + user.id + "/profile/";
        return this.http.patch(url, JSON.stringify(data), new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* RequestOptions */]({ headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/json' }) }))
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.updateProfileCoverPicture = function (picUrl) {
        var _this = this;
        var url = "/api/account/cover_pic/";
        var form = new FormData();
        form.append('cover_pic', picUrl);
        return this.http.patch(url, form)
            .toPromise()
            .then(function (response) { return response.json(); })
            .then(function (response) { return _this._handleNotifications(response); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.updateProfileAvatar = function (picUrl) {
        var _this = this;
        var url = "/api/account/profile_pic/";
        var form = new FormData();
        form.append('profile_pic', picUrl);
        return this.http.post(url, form)
            .toPromise()
            .then(function (response) { return response.json(); })
            .then(function (response) { return _this._handleNotifications(response); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.createComment = function (post_id, text) {
        var addCommentUrl = "/api/post/" + post_id + "/comment/";
        return this.http.post(addCommentUrl, JSON.stringify({ "text": text }), new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* RequestOptions */]({ headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/json' }) }))
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.follow = function (user, csrf) {
        var followUrl = '/api/ajaxFollow/';
        return this.http.post(followUrl, "want_to_follow=" + user.id, new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* RequestOptions */]({ headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' }) }))
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.unfollow = function (user, csrf) {
        var unfollowUrl = '/api/ajaxUnfollow/';
        return this.http.post(unfollowUrl, "want_to_unfollow=" + user.id, new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* RequestOptions */]({ headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' }) }))
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.like = function (post) {
        var _this = this;
        var likeUrl = '/api/like/';
        return this.http.post(likeUrl, "pk=" + post.id, new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* RequestOptions */]({ headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' }) }))
            .toPromise()
            .then(function (response) { return response.json(); })
            .then(function (response) { return _this._handleNotifications(response); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.getList = function (apiUrl, params) {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["a" /* Observable */].create(function (observer) {
            _this.http.get(apiUrl, { params: params })
                .subscribe(function (data) {
                observer.next(data.json());
                observer.complete();
            }, function (err) {
                handleError(err);
                observer.error(err);
            });
        });
    };
    BelaApiService.prototype.getTopSearchList = function (apiUrl, search_term, search_in) {
        if (search_term === void 0) { search_term = ""; }
        if (search_in === void 0) { search_in = 'all'; }
        return this.http.get(apiUrl, { params: { term: search_term, in: search_in } })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.getPhotoList = function (apiUrl, param) {
        if (param === void 0) { param = 'hot'; }
        var params = param ? { sort_by: param } : {};
        return this.getList(apiUrl, params);
    };
    BelaApiService.prototype.deleteFeed = function (pk) {
        var deleteUrl = '/api/delete/';
        return this.http.post(deleteUrl, "pk=" + pk, new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* RequestOptions */]({ headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' }) }))
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.deleteAdminFeed = function (pk) {
        var deleteUrl = '/api/adminDelete/';
        return this.http.post(deleteUrl, "pk=" + pk, new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* RequestOptions */]({ headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' }) }))
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.report = function (pk) {
        var reportUrl = '/api/report/';
        return this.http.post(reportUrl, "pk=" + pk, new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* RequestOptions */]({ headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' }) }))
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.blockUser = function (pk) {
        var reportUrl = '/api/block/';
        return this.http.post(reportUrl, "pk=" + pk, new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* RequestOptions */]({ headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' }) }))
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.isBottomReached = function () {
        var bottom_reached = false;
        var windowHeight = "innerHeight" in window ? window.innerHeight
            : document.documentElement.offsetHeight;
        var body = document.body, html = document.documentElement;
        var docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
        var windowBottom = windowHeight + window.pageYOffset;
        if (docHeight - windowBottom < 1500) {
            bottom_reached = true;
        }
        return bottom_reached;
    };
    BelaApiService.prototype.createFeed = function (username, data) {
        var _this = this;
        var url = '/api/users/' + username + '/';
        return this.postData(url, data, null)
            .then(function (response) { return _this._handleNotifications(response); });
    };
    BelaApiService.prototype.postData = function (url, data, headers) {
        var form = new FormData();
        for (var key in data) {
            form.append(key, data[key]);
        }
        return this.http.post(url, form, { headers: headers })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.postWithImage = function (url, postData, imageParamKey, imageData, imageName, imageType) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        var formData = new FormData();
        formData.append(imageParamKey, this.dataURItoBlob(imageData, imageType), imageName);
        // For multiple files
        // for (let i = 0; i < files.length; i++) {
        //     formData.append(`files[]`, files[i], files[i].name);
        // }
        if (postData !== "" && postData !== undefined && postData !== null) {
            for (var property in postData) {
                if (postData.hasOwnProperty(property)) {
                    formData.append(property, postData[property]);
                }
            }
        }
        return new Promise(function (resolve, reject) {
            _this.http.post('/api/' + url, formData, {
                headers: headers
            }).subscribe(function (res) {
                _this.responseData = res.json();
                resolve(_this.responseData);
            }, function (error) {
                reject(error);
            });
        });
    };
    BelaApiService.prototype.postWithFile = function (url, postData, files) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        var formData = new FormData();
        formData.append('image', files[0], files[0].name);
        // For multiple files
        // for (let i = 0; i < files.length; i++) {
        //     formData.append(`files[]`, files[i], files[i].name);
        // }
        if (postData !== "" && postData !== undefined && postData !== null) {
            for (var property in postData) {
                if (postData.hasOwnProperty(property)) {
                    formData.append(property, postData[property]);
                }
            }
        }
        return new Promise(function (resolve, reject) {
            _this.http.post('/api/' + url, formData, {
                headers: headers
            }).subscribe(function (res) {
                _this.responseData = res.json();
                resolve(_this.responseData);
            }, function (error) {
                reject(error);
            });
        });
    };
    BelaApiService.prototype.dataURItoBlob = function (dataURI, type) {
        var binary = atob(dataURI.split(',')[1]);
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], { type: type });
    };
    BelaApiService.prototype.updateCaption = function (pk, caption) {
        var updateCaptionUrl = '/api/update_caption/';
        return this.http.post(updateCaptionUrl, JSON.stringify({
            "pk": pk,
            "caption": caption
        }), new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* RequestOptions */]({ headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/json' }) }))
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.updateLocation = function (pk, location) {
        var updateLocationUrl = '/api/update_location/';
        return this.http.post(updateLocationUrl, JSON.stringify({
            "pk": pk,
            "location": location
        }), new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* RequestOptions */]({ headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/json' }) }))
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.updateComment = function (comment, newData) {
        var url = "/api/comment/" + comment.id + "/";
        return this.http.patch(url, JSON.stringify(newData), new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* RequestOptions */]({ headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/json' }) }))
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.deleteComment = function (comment) {
        var url = "/api/comment/" + comment.id + "/";
        return this.http.delete(url, new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* RequestOptions */]({ headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/json' }) }))
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.searchPlace = function (params) {
        var url = "/api/places/";
        return this.http.get(url, new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* RequestOptions */]({
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/json' }),
            params: params
        }))
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.getDiscover = function () {
        var discoverDataUrl = '/api/discover/';
        return this.http.get(discoverDataUrl)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.getReferrer = function () {
        var referrerUrl = '/api/referrer/';
        return this.http.get(referrerUrl)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.getReferrerStatistics = function () {
        var url = '/api/referrer/statistics/';
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.getReferralSettings = function () {
        var url = '/api/referral/settings/';
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.getGrowthStatistics = function () {
        var url = '/api/growth/';
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.getImagePostLink = function (fileName, fileType) {
        var url = '/api/get_s3_post';
        var options = {
            params: {
                'file-name': fileName,
                'file-type': fileType
            }
        };
        return this.http.get(url, options)
            .toPromise()
            .then(function (response) { return __WEBPACK_IMPORTED_MODULE_3__app_model__["f" /* S3Resource */].fromJson(response.json()); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.getReferrerApplication = function (status) {
        var url = '/api/referrer/application/';
        var params = {
            "status": status
        };
        return this.http.get(url, { params: params })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.createReferralApplication = function (content) {
        var url = '/api/referrer/application/';
        var form = new FormData();
        form.append('content', content);
        return this.http.post(url, form)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.createWithdrawRequest = function (address, amount) {
        var url = '/api/withdraw/';
        return this.http.post(url, { address: address, amount: amount })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.requestDepositAddress = function () {
        var url = '/api/request_deposit_address/';
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.getUnreadNotifications = function () {
        var notificationsUrl = '/api/notifications/live/';
        return this.http.get(notificationsUrl)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.markAllRead = function () {
        var markAllReadUrl = '/api/mark_all_read';
        return this.http.get(markAllReadUrl)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.markItRead = function (notification) {
        var markItReadUrl = '/api/mark_it_read/' + notification.id + "/";
        return this.http.get(markItReadUrl)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.getNotificatons = function (url, params) {
        return this.http.get(url, new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* RequestOptions */]({
            params: params
        }))
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.getWithdrawFeeSettings = function () {
        var url = '/api/withdraw/settings/fee/';
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.requestPhoneVerification = function (phoneNumber) {
        var url = '/api/account/verify_phone/';
        return this.http.post(url, { phone_number: phoneNumber })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.verifySecurityCode = function (phoneNumber, securityCode) {
        var url = '/api/account/verify_phone_confirm/';
        return this.http.post(url, { phone_number: phoneNumber, code: securityCode })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype.getRewardTasks = function () {
        var url = '/api/reward/';
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(handleErrorPromise);
    };
    BelaApiService.prototype._handleNotifications = function (resp) {
        if (resp.notifications) {
            for (var _i = 0, _a = resp.notifications; _i < _a.length; _i++) {
                var notification = _a[_i];
                if (notification.type === 'complete_reward_task') {
                    var belacoin = notification.belacoin;
                    var user = this.authService.getUser();
                    if (user && user.id) {
                        user.user1.belacoin = belacoin;
                        this.authService.updateUser(user);
                    }
                }
            }
        }
        return resp;
    };
    return BelaApiService;
}());
BelaApiService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_7__angular_router__["d" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__angular_router__["d" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5__event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__event_service__["a" /* EventService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_6__auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__auth_service__["a" /* AuthService */]) === "function" && _d || Object])
], BelaApiService);

var _a, _b, _c, _d;
//# sourceMappingURL=bela-api.service.js.map

/***/ }),

/***/ "./src/app/core/core.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CoreModule; });
/* unused harmony export xsrfFactory */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__event_service__ = __webpack_require__("./src/app/core/event.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lightbox_lightbox_event_service__ = __webpack_require__("./src/app/lightbox/lightbox-event.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__lightbox_lightbox_config_service__ = __webpack_require__("./src/app/lightbox/lightbox-config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__lightbox_lightbox_service__ = __webpack_require__("./src/app/lightbox/lightbox.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__filter_sub4_pipe__ = __webpack_require__("./src/app/core/filter-sub4.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__message_time_pipe__ = __webpack_require__("./src/app/core/message-time.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__tagify_pipe__ = __webpack_require__("./src/app/core/tagify.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__translate_cut_pipe__ = __webpack_require__("./src/app/core/translate-cut.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__biesbjerg_ngx_translate_po_http_loader__ = __webpack_require__("./node_modules/@biesbjerg/ngx-translate-po-http-loader/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__biesbjerg_ngx_translate_po_http_loader___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13__biesbjerg_ngx_translate_po_http_loader__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ngx_translate_core__ = __webpack_require__("./node_modules/@ngx-translate/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__constants__ = __webpack_require__("./src/app/constants.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__awss3_api_awss3_api_service__ = __webpack_require__("./src/app/core/awss3-api/awss3-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_ng2_bootstrap_modal__ = __webpack_require__("./node_modules/ng2-bootstrap-modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_ng2_bootstrap_modal___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_17_ng2_bootstrap_modal__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__modals_avatar_edit_modal_avatar_edit_modal_component__ = __webpack_require__("./src/app/modals/avatar-edit-modal/avatar-edit-modal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_ng2_img_cropper__ = __webpack_require__("./node_modules/ng2-img-cropper/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__modals_cover_edit_modal_cover_edit_modal_component__ = __webpack_require__("./src/app/modals/cover-edit-modal/cover-edit-modal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__web_socket_service__ = __webpack_require__("./src/app/core/web-socket.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};























function createTranslateLoader(http) {
    if (appDebug.toLowerCase() == "true") {
        return new __WEBPACK_IMPORTED_MODULE_13__biesbjerg_ngx_translate_po_http_loader__["TranslatePoHttpLoader"](http, '/static/assets/locale', '/LC_MESSAGES/js.po');
    }
    else {
        return new __WEBPACK_IMPORTED_MODULE_13__biesbjerg_ngx_translate_po_http_loader__["TranslatePoHttpLoader"](http, __WEBPACK_IMPORTED_MODULE_15__constants__["a" /* Constants */].default_remote_asset_url_base + '/assets/locale', '/LC_MESSAGES/js.po');
    }
}
var CoreModule = (function () {
    function CoreModule(webService) {
    }
    return CoreModule;
}());
CoreModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_17_ng2_bootstrap_modal__["BootstrapModalModule"],
            __WEBPACK_IMPORTED_MODULE_14__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                loader: {
                    provide: __WEBPACK_IMPORTED_MODULE_14__ngx_translate_core__["a" /* TranslateLoader */],
                    useFactory: createTranslateLoader,
                    deps: [__WEBPACK_IMPORTED_MODULE_12__angular_http__["c" /* Http */]]
                }
            }),
            __WEBPACK_IMPORTED_MODULE_19_ng2_img_cropper__["d" /* ImageCropperModule */]
        ],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_8__filter_sub4_pipe__["a" /* FilterSub4Pipe */],
            __WEBPACK_IMPORTED_MODULE_9__message_time_pipe__["a" /* MessageTimePipe */],
            __WEBPACK_IMPORTED_MODULE_10__tagify_pipe__["a" /* TagifyPipe */],
            __WEBPACK_IMPORTED_MODULE_11__translate_cut_pipe__["a" /* TranslateCut */],
            __WEBPACK_IMPORTED_MODULE_18__modals_avatar_edit_modal_avatar_edit_modal_component__["a" /* AvatarEditModalComponent */],
            __WEBPACK_IMPORTED_MODULE_20__modals_cover_edit_modal_cover_edit_modal_component__["a" /* CoverEditModalComponent */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_8__filter_sub4_pipe__["a" /* FilterSub4Pipe */],
            __WEBPACK_IMPORTED_MODULE_9__message_time_pipe__["a" /* MessageTimePipe */],
            __WEBPACK_IMPORTED_MODULE_10__tagify_pipe__["a" /* TagifyPipe */],
            __WEBPACK_IMPORTED_MODULE_11__translate_cut_pipe__["a" /* TranslateCut */],
            __WEBPACK_IMPORTED_MODULE_19_ng2_img_cropper__["d" /* ImageCropperModule */]
        ],
        providers: [
            {
                provide: __WEBPACK_IMPORTED_MODULE_12__angular_http__["h" /* XSRFStrategy */],
                useFactory: xsrfFactory
            },
            __WEBPACK_IMPORTED_MODULE_6__lightbox_lightbox_service__["a" /* Lightbox */],
            __WEBPACK_IMPORTED_MODULE_5__lightbox_lightbox_config_service__["a" /* LightboxConfig */],
            __WEBPACK_IMPORTED_MODULE_4__lightbox_lightbox_event_service__["b" /* LightboxEvent */],
            __WEBPACK_IMPORTED_MODULE_4__lightbox_lightbox_event_service__["c" /* LightboxWindowRef */],
            __WEBPACK_IMPORTED_MODULE_7__auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_3__event_service__["a" /* EventService */],
            __WEBPACK_IMPORTED_MODULE_2__bela_api_service__["a" /* BelaApiService */],
            __WEBPACK_IMPORTED_MODULE_16__awss3_api_awss3_api_service__["a" /* AWSS3ApiService */],
            __WEBPACK_IMPORTED_MODULE_21__web_socket_service__["b" /* WebSocketService */]
        ],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_18__modals_avatar_edit_modal_avatar_edit_modal_component__["a" /* AvatarEditModalComponent */],
            __WEBPACK_IMPORTED_MODULE_20__modals_cover_edit_modal_cover_edit_modal_component__["a" /* CoverEditModalComponent */]
        ]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_21__web_socket_service__["b" /* WebSocketService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_21__web_socket_service__["b" /* WebSocketService */]) === "function" && _a || Object])
], CoreModule);

function xsrfFactory() {
    return new __WEBPACK_IMPORTED_MODULE_12__angular_http__["a" /* CookieXSRFStrategy */]('csrftoken', 'X-CSRFToken');
}
var _a;
//# sourceMappingURL=core.module.js.map

/***/ }),

/***/ "./src/app/core/event.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/_esm5/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/toPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EventService = (function () {
    function EventService() {
        this.eventEmitter = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["a" /* Subject */]();
    }
    EventService.prototype.subscribe = function (observer) {
        return this.eventEmitter.subscribe(observer);
    };
    EventService.prototype.fireEvent = function (event) {
        this.eventEmitter.next(event);
    };
    return EventService;
}());
EventService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], EventService);

//# sourceMappingURL=event.service.js.map

/***/ }),

/***/ "./src/app/core/event.types.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return EvRequestAlbum; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return EvLoadedAlbum; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BelaEvent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return BelaEventType; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var EvValue = (function () {
    function EvValue() {
    }
    return EvValue;
}());
var EvRequestAlbum = (function (_super) {
    __extends(EvRequestAlbum, _super);
    function EvRequestAlbum() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return EvRequestAlbum;
}(EvValue));

EvRequestAlbum.DIRECTION_NEXT = 0;
EvRequestAlbum.DIRECTION_PREV = 0;
var EvLoadedAlbum = (function (_super) {
    __extends(EvLoadedAlbum, _super);
    function EvLoadedAlbum() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return EvLoadedAlbum;
}(EvValue));

var BelaEvent = (function () {
    function BelaEvent() {
    }
    return BelaEvent;
}());

var BelaEventType;
(function (BelaEventType) {
    BelaEventType[BelaEventType["ADDED_POST"] = 0] = "ADDED_POST";
    BelaEventType[BelaEventType["DELETED_POST"] = 1] = "DELETED_POST";
    BelaEventType[BelaEventType["LIKE_POST"] = 2] = "LIKE_POST";
    BelaEventType[BelaEventType["REQUEST_NEXT_ALBUM"] = 3] = "REQUEST_NEXT_ALBUM";
    BelaEventType[BelaEventType["LOADED_NEXT_ALBUM"] = 4] = "LOADED_NEXT_ALBUM";
    BelaEventType[BelaEventType["NO_NEXT_ALBUM"] = 5] = "NO_NEXT_ALBUM";
    BelaEventType[BelaEventType["UPDATE_USER"] = 6] = "UPDATE_USER";
    BelaEventType[BelaEventType["NO_AUTH"] = 7] = "NO_AUTH";
})(BelaEventType || (BelaEventType = {}));
//# sourceMappingURL=event.types.js.map

/***/ }),

/***/ "./src/app/core/filter-sub4.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterSub4Pipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FilterSub4Pipe = (function () {
    function FilterSub4Pipe() {
    }
    FilterSub4Pipe.prototype.transform = function (obj, args) {
        if (Array.isArray(obj)) {
            return obj.slice(0, 4);
        }
        else {
            return obj;
        }
    };
    return FilterSub4Pipe;
}());
FilterSub4Pipe = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
        name: 'filterSub4'
    })
], FilterSub4Pipe);

//# sourceMappingURL=filter-sub4.pipe.js.map

/***/ }),

/***/ "./src/app/core/header/header.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/core/header/header.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"header-box\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-sm-3 col-lg-3 col-md-3 logo\">\n        <a [routerLink]=\"['/feed']\" href=\"#\"> <img\n          src=\"https://s3.amazonaws.com/bellassets/orangeicon.png\"/></a>\n\n\n        <div class=\"feedanddiscover\">\n          <a onclick=\"toTop()\" routerLink=\"/feed\" routerLinkActive=\"activelink\" href=\"#\" class=\"feedlink inactivelink\">\n            <h2 translate>Feed</h2>\n          </a>\n\n\n\n\n          <a onclick=\"toTop()\" routerLink=\"/discover\" routerLinkActive=\"activelink\" href=\"#\" class=\"discoverlink inactivelink\">\n            <h2 translate>Explore</h2>\n          </a>\n        </div>\n\n\n      </div>\n\n\n      <my-search></my-search>\n\n\n      <div class=\"col-sm-3 col-lg-3 col-md-3 header-right\">\n\n        <span *ngIf=\"(requesting_user.id ===  undefined) && !loading\">\n          <a id=\"desktoplogin\" class=\"gotologin\" routerLink=\"\" href=\"#\" translate>Login or Sign Up</a>\n        </span>\n\n        <ul *ngIf=\"(requesting_user.id !==  undefined) && !loading \">\n\n          <a routerLink=\"/trade\" routerLinkActive=\"activelink\" href=\"#\" class=\"inactivelink tradelink\">\n            <h2 translate>Trade</h2>\n          </a>\n\n\n          <app-notifications [unread_count]=\"unread_count\" [notification_list]=\"notification_list\">\n          </app-notifications>\n\n\n          <a id=\"dropdownMenuLink\"\n             class=\"dropdown-toggle\"\n             data-toggle=\"dropdown\"\n             aria-haspopup=\"true\"\n             aria-expanded=\"false\">\n            <li class=\"headerusername\">\n\n\n              <span [innerHTML]=\"requesting_user.avatar\"></span>\n\n\n              <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i>\n\n            </li>\n\n          </a>\n\n\n          <div class=\"dropdown-menu account-menu\" aria-labelledby=\"dropdownMenuLink\">\n            <div class=\"dropdown-caret\">\n              <div class=\"caret-outer\"></div>\n              <div class=\"caret-inner\"></div>\n            </div>\n            <a class=\"dropdown-item\" href=\"#\"\n               [routerLink]=\"['/user/', requesting_user.username]\" translate>Profile</a>\n            <a class=\"dropdown-item\" href=\"#\" routerLink=\"/settings\" translate>Settings</a>\n            <a class=\"dropdown-item\"\n               target=\"_blank\"\n               href=\"https://belacam.zendesk.com/hc/en-us\"\n               translate>Support</a>\n            <div class=\"divider\"></div>\n            <a class=\"dropdown-item logoutdropdown\" id=\"logoutdropdown\" href=\"#\" (click)=\"logout();false\" translate>Log Out</a>\n\n          </div>\n\n        </ul>\n\n      </div>\n    </div>\n\n  </div>\n\n</div>\n\n\n\n<div class=\"showonmobile mobileheader\">\n  <div class=\"row\">\n    <a routerLink=\"/feed\" routerLinkActive=\"activelink\" href=\"#\" onclick=\"toTop()\" class=\"feedlink inactivelink\" >\n      <h2 translate>Feed</h2>\n    </a>\n\n\n    <a routerLink=\"/discover\" routerLinkActive=\"activelink\" onclick=\"toTop()\" href=\"#\" class=\"discoverlink inactivelink\">\n      <h2 translate>Explore</h2>\n    </a>\n\n\n    <a routerLink=\"/trade\" routerLinkActive=\"activelink\" href=\"#\" class=\"tradelink inactivelink\" >\n      <h2 translate>Trade</h2>\n    </a>\n\n\n  </div>\n</div>\n\n<div id=\"mobileinfo\" class=\"row showonmobile\">\n  <div class=\"row mobilerowholder\">\n\n    <div id=\"mobilesearch\">\n      <mobile-search [trending]=\"trending\"></mobile-search>\n    </div>\n\n    <div class=\"user-profile-box\" id=\"mobileimageupload\">\n\n      <app-header-posting-form [requesting_user]=\"requesting_user\"></app-header-posting-form>\n\n    </div>\n\n    <div id=\"mobilenotifications\">\n      <div class=\"dropdown show mobilenoticationholder\">\n        <div class=\"notification-bar2\">\n          <span\n            [routerLink]=\"['/notifications']\"\n            onclick=\"mobileNotifications()\"\n            class=\"notification-menu-header\"\n            [title]=\"'Notifications'|translate\" translate>\n            Notifications\n          </span>\n          <a href=\"#\" onclick=\"mobileNotifications()\" (click)=\"markAllRead();false\"\n             class=\"mark-all-as-read\" translate>Mark all as read</a>\n        </div>\n\n        <span class=\"mobilenotifs\" *ngFor=\"let notification of notification_list\">\n\n          <a [ngClass]=\"{'unread': notification.unread }\" class=\"dropdown-item notifline\" href=\"#\" onclick=\"mobileNotifications()\">\n\n            <span class=\"circlephoto notifavatar\" [innerHTML]=\"notification.actor_thumbnail\" (click)=\"handleClickAvatar($event, notification);false\"></span>\n            <div class=\"notifytext\" (click)=\"goToAction(notification);false\">{{ notification.actor }}{{ notification.verb }}</div>\n\n          </a>\n        </span>\n      </div>\n\n    </div>\n\n    <div id=\"mobileprofile\">\n      <div *ngIf=\"(requesting_user.id !==  undefined) && !loading\" class=\"white-box myprofilewhitebox\">\n        <h4 class=\"account\" translate>My Profile</h4>\n        <a onclick=\"mobileProfile()\" class=\"mobileusername\" href=\"#\" [routerLink]=\"['/user/', requesting_user.username]\">\n          <span class=\"circlephoto\" [innerHTML]=\"requesting_user.avatar\"></span>\n          <span>  {{ requesting_user.username }}</span>\n        </a>\n\n        <a class=\"mobileusername\" href=\"#\" routerLink=\"/settings\">\n          <img src=\"https://s3.amazonaws.com/bellassets/assets/images/settings.png\"/>\n          <span translate>Settings</span>\n        </a>\n        <a class=\"mobileusername\" href=\"https://belacam.zendesk.com/hc/en-us\" target=\"_blank\">\n          <img src=\"https://s3.amazonaws.com/bellassets/assets/images/support.png\"/>\n          <span translate>Support</span>\n        </a>\n        <a class=\"mobileusername logoutdropdown\" href=\"#\" (click)=\"logout();false\">\n          <img src=\"https://s3.amazonaws.com/bellassets/assets/images/logout.png\"/>\n          <span translate>Log Out</span>\n        </a>\n\n\n      </div>\n\n      <!-- Account Section on Right Sidebar -->\n\n      <div *ngIf=\"(requesting_user.id !==  undefined) && !loading\" class=\"white-box\">\n        <h4 class=\"account\" translate>Each <img src=\"https://s3.amazonaws.com/bellassets/assets/images/redheart.png\">  = {{ requesting_user.bella_per_like|number:'1.2-2' }} <img src=\"https://s3.amazonaws.com/bellassets/assets/images/orangebela.png\">\n        </h4>\n\n\n\n        <div class=\"trendingbox\" *ngIf=\"requesting_user.user1\">\n          <div class=\"balances\">\n\n\n            <h3 class=\"bigbelabalance\"> {{ requesting_user.user1.belacoin }} <img src=\"https://s3.amazonaws.com/bellassets/assets/images/orangebela.png\"></h3>\n\n            <h5 class=\"usdbalance\"> ${{ requesting_user.user1.account_balance|number:'1.2-2' }}</h5>\n\n\n            <div class=\"clearfix\"></div>\n\n\n\n            <a href=\"#\" onclick=\"closeMobileProfile()\" [routerLink]=\"['/deposit']\" class=\"depowith\" translate>Deposit</a>\n            <span><a href=\"#\" onclick=\"closeMobileProfile()\" [routerLink]=\"['/withdrawal']\" class=\"depowith\" translate>Withdraw</a></span>\n\n\n          </div>\n\n\n        </div>\n\n\n      </div>\n      <app-suggested [suggested]=\"suggested\" [earners]=\"earners\"></app-suggested>\n\n\n    </div>\n\n  </div>\n\n\n</div>\n\n<div class=\"mobilemenu showonmobile\">\n\n  <a [routerLink]=\"['/feed']\" class=\"fifths\">\n    <img src=\"https://s3.amazonaws.com/bellassets/orangeicon.png\"/>\n  </a>\n  <a class=\"fifths\" id=\"mobile_search\" onclick=\"mobileSearch()\">\n    <img class=\"mobilemenuimgwidth\" src=\"https://s3.amazonaws.com/bellassets/assets/images/searchicon.png\"/>\n  </a>\n  <div class=\"fifths\" *ngIf=\"(requesting_user.id ===  undefined) && !loading\">\n    <a id=\"mobilelogin\" class=\"gotologin\" routerLink=\"\" href=\"#\" translate>Login</a>\n  </div>\n  <a *ngIf=\"(requesting_user.id !==  undefined) && !loading\"\n     class=\"fifths\" id=\"mobile_image\"\n     onclick=\"uploadMobileImage()\">\n    <img class=\"mobilemenuimgwidth\" src=\"https://s3.amazonaws.com/bellassets/assets/images/cameraicon.png\"/>\n  </a>\n  <a id=\"mobile_notif\" onclick=\"mobileNotifications()\" class=\"fifths\">\n    <img class=\"mobilemenuimgwidth\"\n         src=\"https://s3.amazonaws.com/bellassets/assets/images/notificationicon.png\"/>\n    <span class=\"notificationspanmobile\" *ngIf=\"unread_count > 0\"><p>{{ unread_count }}</p></span>\n  </a>\n  <a class=\"fifths\" id=\"mobile_profile\" onclick=\"mobileProfile()\">\n    <img class=\"mobilemenuimgwidth\"\n         src=\"https://s3.amazonaws.com/bellassets/assets/images/moreicon.png\"/>\n  </a>\n\n</div>\n"

/***/ }),

/***/ "./src/app/core/header/header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__search_search_service__ = __webpack_require__("./src/app/search/search.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__event_types__ = __webpack_require__("./src/app/core/event.types.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__web_socket_service__ = __webpack_require__("./src/app/core/web-socket.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__event_service__ = __webpack_require__("./src/app/core/event.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HeaderComponent = (function () {
    function HeaderComponent(authService, eventService, webSocketService, apiService, router) {
        this.authService = authService;
        this.eventService = eventService;
        this.webSocketService = webSocketService;
        this.apiService = apiService;
        this.router = router;
        this.unread_count = 0;
        this.notification_list = [];
        this.requesting_user_id = "";
        this.requesting_user_username = "";
        this.requesting_user = new __WEBPACK_IMPORTED_MODULE_2__app_model__["h" /* User */]();
        this.trending = [];
        this.suggested = [];
        this.earners = [];
        this.growth = new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* GrowthStatistics */]();
        this.loading = true;
        this.subscriptions = [];
    }
    HeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.requesting_user_id = this.authService.getUserId();
        this.requesting_user_username = this.authService.getUserName();
        this.apiService.getDiscover().then(function (discoverResp) {
            _this.trending = discoverResp.trending;
            _this.suggested = discoverResp.suggested;
            _this.earners = discoverResp.earners;
        });
        this.requesting_user = this.authService.user;
        this.loading = !this.authService.isServiceReady();
        this.subscriptions.push(this.eventService.subscribe({
            next: function (event) {
                if (event.type == __WEBPACK_IMPORTED_MODULE_6__event_types__["b" /* BelaEventType */].UPDATE_USER) {
                    _this.requesting_user = event.value;
                    _this.loading = false;
                }
            }
        }));
        this.subscriptions.push(this.webSocketService.subscribe({
            next: function (event) {
                if (event.type === __WEBPACK_IMPORTED_MODULE_7__web_socket_service__["a" /* WebSocketEvent */].EVENT_NEW_NOTIFICATION) {
                    _this.getUnreadNotifications();
                }
            }
        }));
        this.getUnreadNotifications();
    };
    HeaderComponent.prototype.ngOnDestroy = function () {
        for (var _i = 0, _a = this.subscriptions; _i < _a.length; _i++) {
            var subscription = _a[_i];
            subscription.unsubscribe();
        }
    };
    HeaderComponent.prototype.getUnreadNotifications = function () {
        var _this = this;
        this.apiService.getUnreadNotifications().then(function (result) {
            _this.unread_count = result.unread_count;
            _this.notification_list = result.notifications_list;
        });
    };
    HeaderComponent.prototype.markAllRead = function () {
        var _this = this;
        this.apiService.markAllRead().then(function (result) {
            _this.getUnreadNotifications();
        });
    };
    HeaderComponent.prototype.goToAction = function (notification) {
        var _this = this;
        this.apiService.markItRead(notification).then(function (result) {
            if (notification.unread == true) {
                notification.unread = false;
                _this.unread_count = _this.unread_count - 1;
            }
        });
        if (notification.action == "comment" || notification.action == "bella")
            this.router.navigate(['/post', notification.username, notification.action_id]);
        else if (notification.action == "user") {
            this.router.navigate(['/user', notification.username]);
        }
    };
    HeaderComponent.prototype.handleClickAvatar = function (event, notification) {
        this.router.navigate(['user/', notification.actor]);
    };
    HeaderComponent.prototype.logout = function () {
        var _this = this;
        this.apiService.logout().then(function (result) {
            if (result["status"] == "success") {
                _this.authService.logout();
                _this.router.navigate(['']);
            }
            else {
                alert("Logout failed!");
            }
        });
    };
    return HeaderComponent;
}());
HeaderComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-header',
        template: __webpack_require__("./src/app/core/header/header.component.html"),
        styles: [__webpack_require__("./src/app/core/header/header.component.css")],
        providers: [__WEBPACK_IMPORTED_MODULE_3__search_search_service__["a" /* SearchService */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5__auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__auth_service__["a" /* AuthService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_8__event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__event_service__["a" /* EventService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_7__web_socket_service__["b" /* WebSocketService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__web_socket_service__["b" /* WebSocketService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__bela_api_service__["a" /* BelaApiService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["d" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["d" /* Router */]) === "function" && _e || Object])
], HeaderComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=header.component.js.map

/***/ }),

/***/ "./src/app/core/loader.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoaderService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_BehaviorSubject__ = __webpack_require__("./node_modules/rxjs/_esm5/BehaviorSubject.js");

var LoaderService = (function () {
    function LoaderService() {
        this.status = new __WEBPACK_IMPORTED_MODULE_0_rxjs_BehaviorSubject__["BehaviorSubject"](false);
    }
    LoaderService.prototype.display = function (value) {
        this.status.next(value);
    };
    return LoaderService;
}());

//# sourceMappingURL=loader.service.js.map

/***/ }),

/***/ "./src/app/core/main-layout/main-layout.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/core/main-layout/main-layout.component.html":
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<router-outlet>\n\n</router-outlet>\n"

/***/ }),

/***/ "./src/app/core/main-layout/main-layout.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainLayoutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MainLayoutComponent = (function () {
    function MainLayoutComponent() {
    }
    MainLayoutComponent.prototype.ngOnInit = function () {
    };
    return MainLayoutComponent;
}());
MainLayoutComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-main-layout',
        template: __webpack_require__("./src/app/core/main-layout/main-layout.component.html"),
        styles: [__webpack_require__("./src/app/core/main-layout/main-layout.component.css")]
    }),
    __metadata("design:paramtypes", [])
], MainLayoutComponent);

//# sourceMappingURL=main-layout.component.js.map

/***/ }),

/***/ "./src/app/core/message-time.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessageTimePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var MessageTimePipe = (function () {
    function MessageTimePipe() {
        this.value = new Date();
    }
    MessageTimePipe.prototype.transform = function (obj, args) {
        var result = '';
        var from = obj;
        var to;
        if (args) {
            from = args;
            to = obj;
        }
        else {
            from = obj;
            to = new Date();
        }
        if (from instanceof Date) {
            this.value = from;
        }
        else {
            this.value = new Date(from);
        }
        var now = to.getTime();
        var delta = (now - this.value.getTime()) / 1000;
        if (delta < 10) {
            result = 'just now';
        }
        else if (delta < 60) {
            result = Math.floor(delta) + ' seconds ago';
        }
        else if (delta < 3600) {
            result = Math.floor(delta / 60) + ' minutes ago';
        }
        else if (delta < 86400) {
            result = Math.floor(delta / 3600) + ' hours ago';
        }
        else {
            result = Math.floor(delta / 86400) + ' days ago';
        }
        return result;
    };
    return MessageTimePipe;
}());
MessageTimePipe = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
        name: 'messageTime'
    })
], MessageTimePipe);

//# sourceMappingURL=message-time.pipe.js.map

/***/ }),

/***/ "./src/app/core/sidebar-layout/sidebar-layout.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/core/sidebar-layout/sidebar-layout.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"user-profile-contain\">\n\n  <div class=\"mid-con\">\n    <div class=\"container\">\n\n\n      <div class=\"row\">\n        <div class=\"col-sm-3 col-lg-3 col-md-3 user-profile-left\">\n          <app-profile-box *ngIf=\"!!requesting_user.id\" [user]=\"requesting_user\"></app-profile-box>\n          <app-trending [trending]=\"trending\"></app-trending>\n          <app-growth [growth]=\"growth\"></app-growth>\n        </div>\n\n        <div class=\"col-sm-6 col-lg-6 col-md-6 middle-section\" >\n          <router-outlet>\n          </router-outlet>\n        </div>\n        <div class=\"col-sm-3 col-lg-3 col-md-3 user-profile-left\">\n\n          <app-deposit-withdrawal-form *ngIf=\"requesting_user.id !==  undefined \"></app-deposit-withdrawal-form>\n          <app-suggested [suggested]=\"suggested\" [earners]=\"earners\"></app-suggested>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"footer-top-margin\">\n</div>\n"

/***/ }),

/***/ "./src/app/core/sidebar-layout/sidebar-layout.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SidebarLayoutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__event_types__ = __webpack_require__("./src/app/core/event.types.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__event_service__ = __webpack_require__("./src/app/core/event.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SidebarLayoutComponent = (function () {
    function SidebarLayoutComponent(authService, appService, eventService) {
        this.authService = authService;
        this.appService = appService;
        this.eventService = eventService;
        this.requesting_user = new __WEBPACK_IMPORTED_MODULE_2__app_model__["h" /* User */]();
        this.trending = [];
        this.suggested = [];
        this.earners = [];
        this.growth = new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* GrowthStatistics */]();
        this.subscriptions = [];
    }
    SidebarLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.requesting_user = this.authService.user;
        this.subscriptions.push(this.eventService.subscribe({
            next: function (event) {
                if (event.type == __WEBPACK_IMPORTED_MODULE_4__event_types__["b" /* BelaEventType */].UPDATE_USER) {
                    _this.requesting_user = event.value;
                }
            }
        }));
        this.appService.getDiscover().then(function (discoverResp) {
            _this.trending = discoverResp.trending;
            _this.suggested = discoverResp.suggested;
            _this.earners = discoverResp.earners;
            window.scroll(0, 0);
        });
        this.appService.getGrowthStatistics().then(function (growthResp) {
            _this.growth = growthResp;
        });
    };
    SidebarLayoutComponent.prototype.ngOnDestroy = function () {
        for (var _i = 0, _a = this.subscriptions; _i < _a.length; _i++) {
            var subscription = _a[_i];
            subscription.unsubscribe();
        }
    };
    return SidebarLayoutComponent;
}());
SidebarLayoutComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-sidebar-layout',
        template: __webpack_require__("./src/app/core/sidebar-layout/sidebar-layout.component.html"),
        styles: [__webpack_require__("./src/app/core/sidebar-layout/sidebar-layout.component.css")],
        providers: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* AuthService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__bela_api_service__["a" /* BelaApiService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5__event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__event_service__["a" /* EventService */]) === "function" && _c || Object])
], SidebarLayoutComponent);

var _a, _b, _c;
//# sourceMappingURL=sidebar-layout.component.js.map

/***/ }),

/***/ "./src/app/core/tagify.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TagifyPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash__ = __webpack_require__("./node_modules/lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__string__ = __webpack_require__("./src/app/string.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__string___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__string__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_xregexp__ = __webpack_require__("./node_modules/xregexp/lib/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_xregexp___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_xregexp__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TagifyPipe = (function () {
    function TagifyPipe(_sanitizer) {
        this._sanitizer = _sanitizer;
        this.value = '';
    }
    TagifyPipe.prototype.convert_http = function (match, p1, p2, p3, offset, string) {
        var s = p2;
        s = s.replace('https://', '').replace('http://', '');
        return p1 + '<a target="_blank" href="http://' + s + '">' + p2 + '</a>' + p3;
    };
    TagifyPipe.prototype.append_reverse = function (match, p1, offset, string) {
        return (' <a class="ng-link" href="/discover/' + p1.reverse() + '">#' + p1.reverse() + '</a>').reverse();
    };
    TagifyPipe.prototype.transform = function (obj, args) {
        if (!obj) {
            obj = "";
        }
        var result = __WEBPACK_IMPORTED_MODULE_2_lodash__["escape"](" " + obj); // add space character in the front to match such case : "#stunning
        var pat1 = __WEBPACK_IMPORTED_MODULE_4_xregexp__("([\\p{L}0-9*-_]+)#(?!&)", 'g');
        var pat2 = __WEBPACK_IMPORTED_MODULE_4_xregexp__("@([\\p{L}0-9*-_]+)", 'g');
        var pat3; // web url pattern
        pat3 = /(^|[ ,.!?()"\\\n-]|&quot;|&#39;)((?:https?:\/\/)?(?:[\w-]+\.)+(?:[a-z]{2,6}\.?)[\/\w.?=-]*\/?(?:\?[\w.=*&]+)?)($|[ ,.!?()"\\\n-]|&quot;|&#39;)/g;
        result = __WEBPACK_IMPORTED_MODULE_4_xregexp__["replace"](result.reverse(), pat1, this.append_reverse).reverse();
        result = __WEBPACK_IMPORTED_MODULE_4_xregexp__["replace"](result, pat2, ' <a class="ng-link" href="/user/$1">@$1</a>');
        result = result.replace(pat3, this.convert_http);
        return this._sanitizer.sanitize(__WEBPACK_IMPORTED_MODULE_0__angular_core__["SecurityContext"].HTML, result);
    };
    return TagifyPipe;
}());
TagifyPipe = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
        name: 'tagify'
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */]) === "function" && _a || Object])
], TagifyPipe);

var _a;
//# sourceMappingURL=tagify.pipe.js.map

/***/ }),

/***/ "./src/app/core/translate-cut.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TranslateCut; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var TranslateCut = (function () {
    function TranslateCut() {
    }
    TranslateCut.prototype.transform = function (value, index) {
        var cutIndex = Number(index);
        return value.split('|')[cutIndex];
    };
    return TranslateCut;
}());
TranslateCut = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'translateCut' })
], TranslateCut);

//# sourceMappingURL=translate-cut.pipe.js.map

/***/ }),

/***/ "./src/app/core/web-socket.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return WebSocketService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WebSocketEvent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular2_websocket_angular2_websocket__ = __webpack_require__("./node_modules/angular2-websocket/angular2-websocket.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular2_websocket_angular2_websocket___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angular2_websocket_angular2_websocket__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var WebSocketService = (function () {
    function WebSocketService() {
        var _this = this;
        this.socket = null;
        this.eventEmitter = new __WEBPACK_IMPORTED_MODULE_2_rxjs__["Subject"]();
        var wsProtocol = 'ws://';
        if (location.protocol.indexOf('https') !== -1) {
            wsProtocol = 'wss://';
        }
        console.log(wsProtocol + location.host);
        this.socket = new __WEBPACK_IMPORTED_MODULE_1_angular2_websocket_angular2_websocket__["$WebSocket"](wsProtocol + location.host);
        this.socket.onMessage(function (msg) {
            console.log("New message arrived");
            console.log(msg.data);
            _this.fireEvent(JSON.parse(msg.data));
        }, { autoApply: false });
    }
    WebSocketService.prototype.subscribe = function (observer) {
        return this.eventEmitter.subscribe(observer);
    };
    WebSocketService.prototype.fireEvent = function (event) {
        this.eventEmitter.next(event);
    };
    return WebSocketService;
}());
WebSocketService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], WebSocketService);

var WebSocketEvent = (function () {
    function WebSocketEvent() {
    }
    return WebSocketEvent;
}());

WebSocketEvent.EVENT_NEW_NOTIFICATION = 'NEW_NOTIFICATION';
//# sourceMappingURL=web-socket.service.js.map

/***/ }),

/***/ "./src/app/deposit-withdrawal-form/deposit-withdrawal-form.component.css":
/***/ (function(module, exports) {

module.exports = "\n.withdraw{\n\n    margin-top: -10px !important;\n}\n\n.withdraw .midsectioninfo {\n  background-color: #EEE;\n    height: auto;\n}\n\n.withdraw .midsectioninfo .withdebit {\n  border-right: none;\n}\n\n.midsectioninfo h2 {\n  font-size: 80px;\n  margin-top: 40px;\n  display: block;\n  color: #888;\n\n}\n\n.submitfooter {\n  width: 100%;\n  border-top: 2px solid #CCC;\n  position: relative;\n  padding-top: 10px;\n  padding-bottom: 10px;\n  background: #73CA65;\n}\n\n.submitfooter:hover{\n    cursor: pointer;\n    background: #73B765;\n}\n\n.withdrawsubmit {\n  background: None !important;\n  font-size: 24px;\n  font-family: 'ProximaNovaRegular', sans-serif;\n  color: white;\n  width: 100%;\n}\n\n.withdraw input::-webkit-input-placeholder {\n  color: #CCC;\n}\n\n.withdraw input::-moz-placeholder {\n  color: #CCC;\n}\n\n.withdraw input:-ms-input-placeholder {\n  color: #CCC;\n}\n\n.withdraw input:-moz-placeholder {\n  color: #CCC;\n}\n\n.withdraw div {\n  display: inline-block;\n}\n\n.withdraw h5 {\n  color: #888;\n  font-size: 24px;\n  padding-left: 10%;\n  padding-right: 10%;\n\n}\n\n.addressdiv {\n  width: 60%;\n  margin-right: 10px;\n  color: #888;\n  vertical-align: top;\n}\n\n.amountdiv {\n  width: 20%;\n  vertical-align: top;\n\n}\n\n.withdraw {\n  color: #888;\n  margin-top: 40px;\n\n}\n\n.withdrawbox h4 {\n  width: 100%;\n  color: #FFF;\n  font-size: 28px;\n  text-align: right;\n  font-family: 'ProximaNovaRegular', sans-serif;\n  background-color: #F87433;\n  padding-right: 20px;\n}\n\n#address {\n  font-size: 24px;\n  display: inline-block;\n  text-align: center;\n  padding-left: 20px;\n  padding-right: 20px;\n  padding-bottom: 10px;\n  padding-top: 10px;\n  width: 100%;\n  background-color: #F9F9F9;\n  border-radius: 5px;\n  color: #888;\n  border: 1px solid #D5D5D5;\n}\n\n#amount {\n\n  font-size: 24px;\n  display: inline-block;\n  text-align: center;\n  padding-left: 20px;\n  padding-right: 20px;\n  padding-bottom: 10px;\n  padding-top: 10px;\n  width: 100%;\n  background-color: #F9F9F9;\n  border-radius: 5px;\n  color: #888;\n  border: 1px solid #D5D5D5;\n}\n\n.midsectioninfo div:hover {\n  background-color: #EEE;\n\n}\n\n.midsectioninfo div {\n  display: inline-block;\n  width: 50%;\n\n}\n\n.midsectioninfo p {\n  font-size: 18px;\n  color: #888;\n  margin-top: 10px;\n}\n\n.midsectioninfo i {\n  font-size: 120px;\n  margin-top: 40px;\n  display: block;\n  color: #888;\n}\n\n.withdebit {\n  border-right: 2px solid #CCCCCC;\n\n}\n\n.withbtc {\n  margin-left: -4px;\n\n}\n\n.midsectioninfo {\n  white-space: nowrap;\n  width: 100%;\n\n  border-top: 2px solid #CCCCCC;\n  text-align: center;\n}\n\n.depowith:hover {\n  cursor: pointer;\n}\n\n.depositbox, .withdrawbox {\n  text-align: center;\n\n\n}\n\n.depositbox .followerbox, .withdrawbox .followerbox {\n  border: none !important;\n    padding-bottom: 0px !important;\n    height: auto !important;\n\n}\n\n.depositbox h4 {\n  width: 100%;\n  color: #FFF;\n  font-size: 28px;\n  text-align: left;\n  font-family: 'ProximaNovaRegular', sans-serif;\n  background-color: #64A9EB;\n}\n\n.depositinstructions {\n  color: #888;\n  font-size: 24px;\n  padding-left: 10%;\n  padding-right: 10%;\n\n}\n\n.depositbox h3 {\n  margin-top: 20px;\n  margin-bottom: 20px;\n  font-size: 24px;\n  display: inline-block;\n  text-align: center;\n  padding-left: 25px;\n  padding-right: 25px;\n  padding-bottom: 15px;\n  padding-top: 15px;\n  background-color: #F9F9F9;\n  border-radius: 5px;\n  color: #64A9EB;\n\n  border: 1px solid #D5D5D5;\n}\n"

/***/ }),

/***/ "./src/app/deposit-withdrawal-form/deposit-withdrawal-form.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"white-box\" *ngIf=\"user.user1\">\n  <h4 class=\"account\" translate>Each <img src=\"https://s3.amazonaws.com/bellassets/assets/images/redheart.png\">  = {{ user.bella_per_like|number:'1.2-2' }} <img src=\"https://s3.amazonaws.com/bellassets/assets/images/orangebela.png\">\n  </h4>\n  <div class=\"trendingbox\">\n    <div class=\"balances\">\n\n      <h3 class=\"bigbelabalance\"> {{ user.user1.belacoin }} <img src=\"https://s3.amazonaws.com/bellassets/assets/images/orangebela.png\"></h3>\n\n      <h5 class=\"usdbalance\"> ${{ user.user1.account_balance|number:'1.2-2' }}</h5>\n\n\n\n\n\n      <div class=\"clearfix\"></div>\n      <div class=\"clearfix\">\n        <span>\n          <!--<a class=\"depowith\" data-toggle=\"modal\" data-target=\".depositbox\" translate>Deposit</a>-->\n          <a class=\"depowith\"  [routerLink]=\"['/deposit']\" translate>Deposit</a>\n        </span>\n\n        <span class=\"pull-right\">\n          <a class=\"depowith\" [routerLink]=\"['/withdrawal']\" translate>Withdraw</a>\n          <!--<a class=\"depowith\" data-toggle=\"modal\" data-target=\".withdrawbox\" translate>Withdraw</a>-->\n        </span>\n\n        <div class=\"modal fade bs-example-modal-lg depositbox\" tabindex=\"-1\" role=\"dialog\"\n             aria-labelledby=\"myLargeModalLabel\">\n\n          <div class=\"modal-dialog modal-lg\">\n            <div class=\"modal-content\">\n\n              <div class=\"white-box followerbox\">\n                <h4 translate>Deposit Bela</h4>\n                <h3 translate>Deposits are currently closed</h3>\n                <p class=\"depositinstructions\" translate>To deposit Bela to your account, send Bela from an exchange or wallet to your deposit address listed above. </p>\n                <br>\n                <div class=\"midsectioninfo\">\n\n\n                  <a href=\"https://mercatox.com/?referrer=308520\" target=\"_blank\"\n                     alt=\"Bela on Mercatox\">\n                    <div class=\"withbtc\">\n                      <i class=\"fab fa-bitcoin\"></i>\n                      <p translate>Buy with Bitcoin</p>\n                    </div>\n                  </a>\n\n\n                </div>\n\n              </div>\n            </div>\n          </div>\n        </div>\n\n        <div class=\"modal fade bs-example-modal-lg withdrawbox\" tabindex=\"-1\" role=\"dialog\"\n             aria-labelledby=\"myLargeModalLabel\">\n\n          <div class=\"modal-dialog modal-lg\">\n            <div class=\"modal-content\">\n\n              <div class=\"white-box followerbox\">\n\n                <h4 translate>Withdraw Bela</h4>\n\n\n\n                <form class=\"withdraw\">\n\n                  <div class=\"addressdiv\">\n                    <h5 translate>Address</h5>\n                    <input #withdrawalAddress\n                           placeholder=\"0xffb44851dbdc7dd2a679bf669fdae90d47d0ecac\"\n                           id=\"address\" type=\"text\" name=\"withdraw\" value=\"\"\n                           (change)=\"assignAddress($event)\"\n                           required>\n                  </div>\n\n                  <div class=\"amountdiv\">\n                    <h5>Bela</h5>\n                    <input #withdrawalAmount placeholder=\"5\" id=\"amount\" type=\"number\"\n                           name=\"amount\"\n                           min=\".5\" value=\"\" (change)=\"assignAmount($event)\" required>\n                  </div>\n\n\n                  <br><br>\n                  <p class=\"depositinstructions\" translate>Enter a Bela address to receive\n                    your funds and choose how much you would like to withdraw. </p>\n\n                  <div class=\"midsectioninfo\">\n                    <div class=\"withdebit\">\n                      <h2> {{ user.user1.belacoin }} </h2>\n                      <p>BELA</p>\n                    </div>\n\n                    <div class=\"withbtc\">\n                      <h2>${{ user.user1.account_balance|number:'1.2-2' }}</h2>\n                      <p translate>USD Value</p>\n                    </div>\n\n                  </div>\n\n\n                  <div class=\"submitfooter\">\n                    <input (click)=\"withdrawal();false\" class=\"withdrawsubmit\"\n                           type=\"submit\" value=\"Submit\">\n                    <div>\n                      <ngx-loading [show]=\"loading\"></ngx-loading>\n                    </div>\n                  </div>\n\n\n                </form>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n  </div>\n\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/deposit-withdrawal-form/deposit-withdrawal-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DepositWithdrawalFormComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_event_service__ = __webpack_require__("./src/app/core/event.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_event_types__ = __webpack_require__("./src/app/core/event.types.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DepositWithdrawalFormComponent = (function () {
    function DepositWithdrawalFormComponent(appService, authService, eventService) {
        this.appService = appService;
        this.authService = authService;
        this.eventService = eventService;
        this.user = new __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]();
        this.withdrawal_address = "";
        this.withdrawal_amount = 0;
        this.loading = false;
        this.subscriptions = [];
    }
    DepositWithdrawalFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = this.authService.user;
        this.subscriptions.push(this.eventService.subscribe({
            next: function (event) {
                if (event.type == __WEBPACK_IMPORTED_MODULE_5__core_event_types__["b" /* BelaEventType */].LIKE_POST) {
                    _this.user = event.value;
                    localStorage.setItem('user', JSON.stringify(event.value));
                }
                else if (event.type == __WEBPACK_IMPORTED_MODULE_5__core_event_types__["b" /* BelaEventType */].UPDATE_USER) {
                    _this.user = event.value;
                }
            }
        }));
    };
    DepositWithdrawalFormComponent.prototype.ngOnDestroy = function () {
        for (var _i = 0, _a = this.subscriptions; _i < _a.length; _i++) {
            var subscription = _a[_i];
            subscription.unsubscribe();
        }
    };
    DepositWithdrawalFormComponent.prototype.withdrawal = function () {
        var _this = this;
        if (this.user.user1) {
            if (this.user.user1.belacoin < this.withdrawal_amount) {
                alert("Withdrawal amount must be smaller than current account capacity!");
            }
            else if (this.withdrawal_amount > 0 && this.withdrawal_address != "") {
                this.loading = true;
                this.appService.createWithdrawRequest(this.withdrawal_address, this.withdrawal_amount)
                    .then(function (resp) {
                    _this.user.user1.belacoin = resp['balance'];
                    _this.authService.updateUser(_this.user);
                    alert("A withdrawal confirmation message will arrive in your email inbox in the next 5 minutes. Please click the link there to confirm your withdrawal!");
                    _this.loading = false;
                });
            }
        }
    };
    DepositWithdrawalFormComponent.prototype.assignAddress = function (event) {
        this.withdrawal_address = event.target.value;
    };
    DepositWithdrawalFormComponent.prototype.assignAmount = function (event) {
        this.withdrawal_amount = parseFloat(event.target.value);
    };
    return DepositWithdrawalFormComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('withdrawalAddress'),
    __metadata("design:type", Object)
], DepositWithdrawalFormComponent.prototype, "withdrawalAddress", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('withdrawalAmount'),
    __metadata("design:type", Object)
], DepositWithdrawalFormComponent.prototype, "withdrawalAmount", void 0);
DepositWithdrawalFormComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-deposit-withdrawal-form',
        template: __webpack_require__("./src/app/deposit-withdrawal-form/deposit-withdrawal-form.component.html"),
        styles: [__webpack_require__("./src/app/deposit-withdrawal-form/deposit-withdrawal-form.component.css")],
        providers: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__core_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_auth_service__["a" /* AuthService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_event_service__["a" /* EventService */]) === "function" && _c || Object])
], DepositWithdrawalFormComponent);

var _a, _b, _c;
//# sourceMappingURL=deposit-withdrawal-form.component.js.map

/***/ }),

/***/ "./src/app/deposit/deposit.component.css":
/***/ (function(module, exports) {

module.exports = ".midsectioninfo h2 {\n  font-size: 80px;\n  margin-top: 40px;\n  display: block;\n  color: #888;\n\n}\n\n.midsectioninfo div:hover {\n  background-color: #EEE;\n\n}\n\n.midsectioninfo div {\n  display: block;\n    margin-left: -30px;\n    margin-right: -30px;\n    margin-top: -2px;\n    margin-bottom: -30px;\n    padding: 10px;\n\n}\n\n.midsectioninfo a:hover {\ntext-decoration: none;\n}\n\n.midsectioninfo p {\n  font-size: 24px;\n  color: #888;\n  margin-top: 10px;\n}\n\n.midsectioninfo i {\n  font-size: 120px;\n  margin-top: 40px;\n  display: block;\n  color: #f87433;\n}\n\n.withdebit {\n  border-right: 2px solid #CCCCCC;\n\n}\n\n.withbtc {\n  margin-left: -4px;\n\n}\n\n.midsectioninfo {\n  white-space: nowrap;\n  width: 100%;\n\n  border-top: 2px solid #CCCCCC;\n  text-align: center;\n}\n\n.depowith:hover {\n  cursor: pointer;\n}\n\n.depositbox, .withdrawbox {\n  text-align: center;\n}\n\n.depositbox .followerbox, .withdrawbox .followerbox {\n  border: none !important;\n  padding-bottom: 0px !important;\n  height: auto !important;\n\n}\n\n.depositbox h4 {\n  width: 100%;\n  color: #FFF;\n  font-size: 28px;\n  text-align: left;\n  font-family: 'ProximaNovaRegular', sans-serif;\n  background-color: #64A9EB;\n}\n\n.depositinstructions {\n  color: #888;\n  font-size: 24px;\n  padding-left: 10%;\n  padding-right: 10%;\n\n}\n\n.depositbox h3 {\n  margin-top: 20px;\n  margin-bottom: 20px;\n  font-size: 24px;\n  display: block;\n  text-align: center;\n  padding-left: 15px;\n  padding-right: 15px;\n  padding-bottom: 15px;\n  padding-top: 15px;\n  background-color: #F9F9F9;\n  border-radius: 5px;\n  color: #64A9EB;\n\n  border: 1px solid #D5D5D5;\n}\n\n.depositbox button.depositaddress {\n  margin-top: 20px;\n  margin-bottom: 20px;\n  font-size: 24px;\n  display: block;\n  text-align: center;\n  padding-left: 15px;\n  padding-right: 15px;\n  padding-bottom: 15px;\n  padding-top: 15px;\n  background-color: #F9F9F9;\n  border-radius: 5px;\n  color: #64A9EB;\n  width: 100%;\n  border: 1px solid #D5D5D5;\n  cursor: pointer;\n}\n\n.depositbox a.depositaddress:hover {\n  text-decoration: none;\n}\n"

/***/ }),

/***/ "./src/app/deposit/deposit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"depositbox\">\n  <div class=\"white-box\">\n    <h4 translate>Deposit Bela</h4>\n    <h3 class=\"depositaddress\" translate *ngIf=\"deposit_address.startsWith('0xbe1a')\">{{ deposit_address }}</h3>\n    <button class=\"depositaddress\" translate (click)=\"request_deposit_address()\" *ngIf=\"!deposit_address.startsWith('0xbe1a')\" [disabled]=\"processing\">Click for deposit address</button>\n    <p class=\"depositinstructions\" translate>To deposit Bela to your account, send Bela from an exchange or wallet to your deposit address listed above. </p>\n    <br>\n    <div class=\"midsectioninfo\">\n\n\n      <a href=\"https://mercatox.com/?referrer=308520\" target=\"_blank\"\n         alt=\"Bela on Mercatox\">\n        <div class=\"withbtc\">\n          <i class=\"fab fa-bitcoin\"></i>\n          <p translate>Buy Bela with Bitcoin</p>\n        </div>\n      </a>\n\n\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/deposit/deposit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DepositComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DepositComponent = (function () {
    function DepositComponent(appService) {
        this.appService = appService;
        this.user = new __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]();
        this.deposit_address = "";
        this.processing = false;
    }
    DepositComponent.prototype.ngOnInit = function () {
        this.user = JSON.parse(localStorage.getItem('user'));
        if (this.user.user1.belacoin_address) {
            this.deposit_address = this.user.user1.belacoin_address;
        }
    };
    DepositComponent.prototype.request_deposit_address = function () {
        var _this = this;
        this.processing = true;
        this.appService.requestDepositAddress().then(function (resp) {
            if (resp.status == "error") {
                alert(resp.content);
            }
            else {
                _this.deposit_address = _this.user.user1.belacoin_address = resp.content;
                localStorage.setItem('user', JSON.stringify(_this.user));
            }
            _this.processing = false;
        }).catch(function (err) {
            console.error(err);
            alert("Server Error!");
            _this.processing = false;
        });
    };
    return DepositComponent;
}());
DepositComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("./src/app/deposit/deposit.component.html"),
        styles: [__webpack_require__("./src/app/deposit/deposit.component.css")],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _a || Object])
], DepositComponent);

var _a;
//# sourceMappingURL=deposit.component.js.map

/***/ }),

/***/ "./src/app/directory/directory.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DirectoryComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DirectoryComponent = (function () {
    function DirectoryComponent() {
        this.stuff = [1, 2, 3, 4, 5];
    }
    return DirectoryComponent;
}());
DirectoryComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: "\n    <h1>Search Demo</h1>\n    <p>Search after each keystroke</p>\n  "
    }),
    __metadata("design:paramtypes", [])
], DirectoryComponent);

//# sourceMappingURL=directory.component.js.map

/***/ }),

/***/ "./src/app/discover/discover.component.css":
/***/ (function(module, exports) {

module.exports = ".content3 h3 {\n  color: #000;\n\n}\n\n.content3 a:hover {\n\n  text-decoration: none;\n}\n\n"

/***/ }),

/***/ "./src/app/discover/discover.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"discover-box\">\n  <div class=\"panel with-nav-tabs\">\n\n\n    <div class=\"panel-heading\">\n\n      <ul class=\"nav nav-tabs\">\n        <li class=\"discoverheader hidden\">\n          <a href=\"#\" data-toggle=\"tab\" translate>Discover</a>\n        </li>\n\n        <li class=\"active\">\n          <a href=\"#photosdefault\" data-toggle=\"tab\" (click)=\"change_tab('photo')\">\n           <i class=\"fas fa-camera-retro lightningbolt\"></i>\n            <span class=\"hideonphones\" translate>&nbsp;&nbsp;&nbsp;Photos</span>\n          </a>\n        </li>\n        <li>\n          <a href=\"#peopledefault\" data-toggle=\"tab\" (click)=\"change_tab('people')\">\n           <i class=\"fas fa-users lightningbolt\"></i>\n            <span class=\"hideonphones\" translate>&nbsp;&nbsp;&nbsp;People</span>\n          </a>\n        </li>\n        <li>\n          <a href=\"#tagsdefault\" data-toggle=\"tab\" (click)=\"change_tab('tag')\">\n            <i class=\"fas fa-hashtag lightningbolt\"></i>\n            <span class=\"hideonphones\" translate>&nbsp;&nbsp;&nbsp;Tags</span></a></li>\n\n      </ul>\n    </div>\n\n\n    <div class=\"panel-body\">\n\n      <div class=\"tab-content\">\n\n\n        <div class=\"tab-pane fade in active endless_page_template\" id=\"photosdefault\" style=\"float: left; width: 100%;\">\n          <app-photos [user]=\"user\" [hidden]=\"tab_name!='photo'\"></app-photos>\n        </div>\n\n        <div class=\"tab-pane fade endless_page_template\" style=\"float:left;width:100%;\"\n             id=\"peopledefault\">\n          <app-people [user]=\"user\" [hidden]=\"tab_name!='people'\"></app-people>\n        </div>\n\n        <div class=\"tab-pane fade endless_page_template\" id=\"tagsdefault\">\n          <app-tags [user]=\"user\" [hidden]=\"tab_name!='tag'\"></app-tags>\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/discover/discover.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DiscoverComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PeopleComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return TagsComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return PhotoComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap_modal__ = __webpack_require__("./node_modules/ng2-bootstrap-modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap_modal___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap_modal__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lightbox_lightbox_service__ = __webpack_require__("./src/app/lightbox/lightbox.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__constants__ = __webpack_require__("./src/app/constants.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__core_event_service__ = __webpack_require__("./src/app/core/event.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__core_event_types__ = __webpack_require__("./src/app/core/event.types.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_Subject__ = __webpack_require__("./node_modules/rxjs/_esm5/Subject.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var DiscoverComponent = (function () {
    function DiscoverComponent(appService, authService, eventService) {
        this.appService = appService;
        this.authService = authService;
        this.eventService = eventService;
        this.user = new __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]();
        this.cover_pic_url = __WEBPACK_IMPORTED_MODULE_6__constants__["a" /* Constants */].default_cover_pic;
        this.csrf = "";
        this.subscriptions = [];
        this.tab_name = "photo";
    }
    DiscoverComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
        this.setRequestingUser(this.authService.user);
        this.subscriptions.push(this.eventService.subscribe({
            next: function (event) {
                if (event.type == __WEBPACK_IMPORTED_MODULE_9__core_event_types__["b" /* BelaEventType */].UPDATE_USER) {
                    _this.setRequestingUser(event.value);
                }
            }
        }));
        this.cover_pic_url = this.user.user1 && this.user.user1.cover_pic ? this.user.user1.cover_pic : __WEBPACK_IMPORTED_MODULE_6__constants__["a" /* Constants */].default_cover_pic;
    };
    DiscoverComponent.prototype.ngOnDestroy = function () {
        for (var _i = 0, _a = this.subscriptions; _i < _a.length; _i++) {
            var subscription = _a[_i];
            subscription.unsubscribe();
        }
    };
    DiscoverComponent.prototype.setRequestingUser = function (user) {
        this.user = user;
    };
    DiscoverComponent.prototype.follow = function (user) {
        if (user && user.id) {
            this.appService.follow(user, this.csrf).then(function () {
                user.is_following = true;
            }).catch(function (err) {
            });
        }
    };
    DiscoverComponent.prototype.unfollow = function (user) {
        if (user && user.id) {
            this.appService.unfollow(user, this.csrf).then(function () {
                user.is_following = false;
            }).catch(function (err) {
            });
        }
    };
    DiscoverComponent.prototype.change_tab = function (tab_name) {
        this.tab_name = tab_name;
    };
    return DiscoverComponent;
}());
DiscoverComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-discover',
        template: __webpack_require__("./src/app/discover/discover.component.html"),
        styles: [__webpack_require__("./src/app/discover/discover.component.css")],
        providers: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_7__core_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_auth_service__["a" /* AuthService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_8__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__core_event_service__["a" /* EventService */]) === "function" && _c || Object])
], DiscoverComponent);

var PeopleComponent = (function () {
    function PeopleComponent(appService, dialogService, _lightbox, eventService) {
        this.appService = appService;
        this.dialogService = dialogService;
        this._lightbox = _lightbox;
        this.eventService = eventService;
        this.user = new __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]();
        this.hidden = false;
        this.loading = true;
        this.people_api_url = '/api/people/';
        this.people = [];
        this.csrf = "";
    }
    PeopleComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
        this.subscription = this.eventService.subscribe({
            next: function (event) {
                if (event.type === __WEBPACK_IMPORTED_MODULE_9__core_event_types__["b" /* BelaEventType */].DELETED_POST) {
                    _this.delete(event.value);
                }
            }
        });
        this.loadPeoples();
    };
    PeopleComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    PeopleComponent.prototype.delete = function (thot) {
        for (var _i = 0, _a = this.people; _i < _a.length; _i++) {
            var user = _a[_i];
            if (user.id === thot.user.id) {
                for (var i = 0; i < user.album.length; i++) {
                    var album = user.album[i];
                    if (album.thot.id === thot.id) {
                        user.album.splice(i, 1);
                        return;
                    }
                }
            }
        }
    };
    PeopleComponent.prototype.onPeopleScroll = function () {
        var bottom_reached = this.appService.isBottomReached();
        if (this.people_api_url != null && bottom_reached && !this.loading && !this.hidden) {
            this.loadPeoples();
        }
    };
    PeopleComponent.prototype.loadPeoples = function () {
        var _this = this;
        this.loading = true;
        return this.appService.getList(this.people_api_url)
            .subscribe(function (listResp) {
            _this.people_api_url = listResp.next;
            for (var _i = 0, _a = listResp.results; _i < _a.length; _i++) {
                var result = _a[_i];
                // this.people.push(result);
                var index = _this.people.push(result) - 1;
                for (var j = 0; j < _this.people[index].preview.length; j++) {
                    var _album = {
                        src: _this.people[index].preview[j].image,
                        caption: _this.people[index].preview[j].content,
                        thumb: _this.people[index].preview[j].image,
                        thot: _this.people[index].preview[j]
                    };
                    if (_this.people[index].album == undefined)
                        _this.people[index].album = [_album];
                    else
                        _this.people[index].album.push(_album);
                }
            }
            _this.loading = false;
        }, function (error) {
        });
    };
    PeopleComponent.prototype.follow = function (user) {
        if (user && user.id) {
            this.appService.follow(user, this.csrf).then(function () {
                user.is_following = true;
            }).catch(function (err) {
            });
        }
    };
    PeopleComponent.prototype.unfollow = function (user) {
        if (user && user.id) {
            this.appService.unfollow(user, this.csrf).then(function () {
                user.is_following = false;
            }).catch(function (err) {
            });
        }
    };
    PeopleComponent.prototype.openPostBox = function (album, index) {
        // open lightbox
        this._lightbox.openPost(album, index, { disableKeyboardNav: true, wrapAround: true });
    };
    return PeopleComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], PeopleComponent.prototype, "user", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], PeopleComponent.prototype, "hidden", void 0);
PeopleComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-people',
        template: __webpack_require__("./src/app/discover/people.component.html"),
        styles: [__webpack_require__("./src/app/discover/people.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap_modal__["DialogService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap_modal__["DialogService"]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_4__lightbox_lightbox_service__["a" /* Lightbox */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__lightbox_lightbox_service__["a" /* Lightbox */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_8__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__core_event_service__["a" /* EventService */]) === "function" && _g || Object])
], PeopleComponent);

var TagsComponent = (function () {
    function TagsComponent(belaApiService, _lightbox, eventService) {
        this.belaApiService = belaApiService;
        this._lightbox = _lightbox;
        this.eventService = eventService;
        this.user = new __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]();
        this.hidden = false;
        this._requestID = 'tags';
        this.tags = [];
        this.tagApiUrl = '/api/tags/';
        this.loading = true;
    }
    TagsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadTags();
        this.subscription = this.eventService.subscribe({
            next: function (event) {
                var request = event.value;
                if (event.type === __WEBPACK_IMPORTED_MODULE_9__core_event_types__["b" /* BelaEventType */].REQUEST_NEXT_ALBUM &&
                    event.value.requestID == _this._requestID) {
                    if (_this.tagInLightbox.preview_next) {
                        _this.loadTagPreview(_this.tagInLightbox).subscribe(function (album) {
                            var evValue = new __WEBPACK_IMPORTED_MODULE_9__core_event_types__["c" /* EvLoadedAlbum */]();
                            evValue.album = album;
                            evValue.newImageIndex = request.currentImageIndex + 1;
                            var event = new __WEBPACK_IMPORTED_MODULE_9__core_event_types__["a" /* BelaEvent */]();
                            event.type = __WEBPACK_IMPORTED_MODULE_9__core_event_types__["b" /* BelaEventType */].LOADED_NEXT_ALBUM;
                            event.value = evValue;
                            _this.eventService.fireEvent(event);
                        }, function (err) {
                            var event = new __WEBPACK_IMPORTED_MODULE_9__core_event_types__["a" /* BelaEvent */]();
                            event.type = __WEBPACK_IMPORTED_MODULE_9__core_event_types__["b" /* BelaEventType */].NO_NEXT_ALBUM;
                            event.value = null;
                            _this.eventService.fireEvent(event);
                        });
                    }
                    else {
                        var event_1 = new __WEBPACK_IMPORTED_MODULE_9__core_event_types__["a" /* BelaEvent */]();
                        event_1.type = __WEBPACK_IMPORTED_MODULE_9__core_event_types__["b" /* BelaEventType */].NO_NEXT_ALBUM;
                        event_1.value = null;
                        _this.eventService.fireEvent(event_1);
                    }
                }
            }
        });
    };
    TagsComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    TagsComponent.prototype.openPostBox = function (tag, index) {
        // open lightbox
        this.tagInLightbox = tag;
        this._lightbox.openPost(tag.album, index, {
            disableKeyboardNav: true,
            allowRequestAlbum: true,
            requestID: this._requestID
        });
    };
    TagsComponent.prototype.loadTags = function () {
        var _this = this;
        this.loading = true;
        return this.belaApiService.getList(this.tagApiUrl)
            .subscribe(function (listResp) {
            _this.tagApiUrl = listResp.next;
            for (var _i = 0, _a = listResp.results; _i < _a.length; _i++) {
                var result = _a[_i];
                var index = _this.tags.push(result) - 1;
                for (var j = 0; j < _this.tags[index].preview.length; j++) {
                    var _album = _this.buildAlbumFromPreview(_this.tags[index].preview[j]);
                    if (_this.tags[index].album == undefined)
                        _this.tags[index].album = [_album];
                    else
                        _this.tags[index].album.push(_album);
                }
                if (_this.tags[index].preview.length >= 4) {
                    _this.tags[index].preview_next = 2;
                    _this.tags[index].preview_page_size = 4;
                }
            }
            _this.loading = false;
        }, function (error) {
        });
    };
    TagsComponent.prototype.loadTagPreview = function (tag) {
        var _this = this;
        var subject = new __WEBPACK_IMPORTED_MODULE_10_rxjs_Subject__["b" /* Subject */]();
        this.tagfeedsApiUrl = "/api/tagposts/" + tag.tag + "/";
        this.belaApiService.getList(this.tagfeedsApiUrl, { page_size: tag.preview_page_size, page: tag.preview_next })
            .subscribe(function (data) {
            if (data.next) {
                tag.preview_next += 1;
            }
            else {
                tag.preview_next = null;
            }
            for (var _i = 0, _a = data.results; _i < _a.length; _i++) {
                var result = _a[_i];
                var _album = _this.buildAlbumFromPreview(result);
                tag.preview.push(result);
                if (tag.album == undefined)
                    tag.album = [_album];
                else
                    tag.album.push(_album);
            }
            subject.next(tag.album);
            subject.complete();
        }, function (err) {
            subject.error(err);
            tag.preview_next = null;
        });
        return subject;
    };
    TagsComponent.prototype.buildAlbumFromPreview = function (preview) {
        return {
            src: preview.image,
            caption: preview.content,
            thumb: preview.image,
            thot: preview
        };
    };
    TagsComponent.prototype.onTagScroll = function () {
        var bottom_reached = this.belaApiService.isBottomReached();
        if (this.tagApiUrl != null && bottom_reached && !this.loading && !this.hidden) {
            this.loadTags();
        }
    };
    return TagsComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], TagsComponent.prototype, "user", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], TagsComponent.prototype, "hidden", void 0);
TagsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-tags',
        template: __webpack_require__("./src/app/discover/tags.component.html"),
    }),
    __metadata("design:paramtypes", [typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_4__lightbox_lightbox_service__["a" /* Lightbox */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__lightbox_lightbox_service__["a" /* Lightbox */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_8__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__core_event_service__["a" /* EventService */]) === "function" && _k || Object])
], TagsComponent);

var PhotoComponent = (function () {
    function PhotoComponent(appService, http, eventService, _lightbox) {
        this.appService = appService;
        this.http = http;
        this.eventService = eventService;
        this._lightbox = _lightbox;
        this.user = new __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]();
        this.hidden = false;
        this._requestID = 'photo';
        this.bellas = [];
        this.albumList = [];
        this.photo_api_url = '/api/feed/list/';
        this.requesting_user_username = "";
        this.csrf = "";
        this.sorting_algorithm = 'hot';
        this.select_most_liked = false;
        this.loading = true;
    }
    PhotoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.requesting_user_username = jQuery('input[name="requesting_user_username"]').val();
        this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
        this.loadPhotos(this.sorting_algorithm, false).subscribe(function () {
            _this.loading = false;
        });
        this.subscription = this.eventService.subscribe({
            next: function (event) {
                if (event.type === __WEBPACK_IMPORTED_MODULE_9__core_event_types__["b" /* BelaEventType */].REQUEST_NEXT_ALBUM &&
                    event.value.requestID == _this._requestID) {
                    _this._requestAlbum(event.value);
                }
            }
        });
    };
    PhotoComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    PhotoComponent.prototype.onPhotoScroll = function () {
        var _this = this;
        var bottom_reached = this.appService.isBottomReached();
        if (this.photo_api_url != null && bottom_reached && !this.loading && !this.hidden) {
            this.loading = true;
            this.loadPhotos(this.sorting_algorithm, true)
                .subscribe(function () {
                _this.loading = false;
            }, function (err) {
                _this.loading = false;
            });
        }
    };
    PhotoComponent.prototype._requestAlbum = function (request) {
        var _this = this;
        this.loading = true;
        if (this.photo_api_url == null) {
            var event = new __WEBPACK_IMPORTED_MODULE_9__core_event_types__["a" /* BelaEvent */]();
            event.type = __WEBPACK_IMPORTED_MODULE_9__core_event_types__["b" /* BelaEventType */].NO_NEXT_ALBUM;
            event.value = null;
            this.eventService.fireEvent(event);
        }
        else {
            this.loadPhotos(this.sorting_algorithm, true).subscribe(function () {
                var evValue = new __WEBPACK_IMPORTED_MODULE_9__core_event_types__["c" /* EvLoadedAlbum */]();
                evValue.album = _this.albumList;
                evValue.newImageIndex = request.currentImageIndex + 1;
                var event = new __WEBPACK_IMPORTED_MODULE_9__core_event_types__["a" /* BelaEvent */]();
                event.type = __WEBPACK_IMPORTED_MODULE_9__core_event_types__["b" /* BelaEventType */].LOADED_NEXT_ALBUM;
                event.value = evValue;
                _this.loading = false;
                _this.eventService.fireEvent(event);
            }, function (err) {
                _this.loading = false;
            });
        }
    };
    PhotoComponent.prototype.loadPhotos = function (param, isAppending) {
        var _this = this;
        if (isAppending === void 0) { isAppending = false; }
        if (!isAppending) {
            this.photo_api_url = '/api/feed/list/';
            this.bellas = [];
            if (this.pendingAPI) {
                this.pendingAPI.subscription.unsubscribe();
                this.pendingAPI.callback.complete();
            }
        }
        var sorting_algorithm = null;
        if (!isAppending) {
            this.sorting_algorithm = sorting_algorithm = param;
        }
        var callback = new __WEBPACK_IMPORTED_MODULE_10_rxjs_Subject__["b" /* Subject */]();
        var subscription = this.appService.getPhotoList(this.photo_api_url, sorting_algorithm)
            .subscribe(function (listResp) {
            _this.photo_api_url = listResp.next;
            if (isAppending) {
                for (var _i = 0, _a = listResp.results; _i < _a.length; _i++) {
                    var result = _a[_i];
                    _this.bellas.push(result);
                }
            }
            else {
                _this.bellas = listResp.results;
                window.scroll(0, 0);
            }
            _this._buildAlbum();
            callback.next();
            callback.complete();
        });
        this.pendingAPI = {
            callback: callback,
            subscription: subscription
        };
        return callback;
    };
    PhotoComponent.prototype.delete = function (thot) {
        var idx = this.bellas.indexOf(thot);
        if (idx > -1) {
            this.bellas.splice(idx, 1);
        }
        this._buildAlbum();
    };
    PhotoComponent.prototype.sort_by_hot = function () {
        var _this = this;
        this.select_most_liked = false;
        this.loading = true;
        this.loadPhotos('hot').subscribe(function () {
            _this.loading = false;
        });
    };
    PhotoComponent.prototype.sort_by_new = function () {
        var _this = this;
        this.select_most_liked = false;
        this.loading = true;
        this.loadPhotos('new').subscribe(function () {
            _this.loading = false;
        });
    };
    PhotoComponent.prototype.sort_by_likes = function (param) {
        var _this = this;
        if (param === void 0) { param = 'today'; }
        this.select_most_liked = true;
        this.loadPhotos('time_' + param).subscribe(function () {
            _this.loading = false;
        });
    };
    PhotoComponent.prototype.openPostBox = function (index) {
        if (index >= 0) {
            this._lightbox.openPost(this.albumList, index, {
                disableKeyboardNav: true,
                allowRequestAlbum: true,
                requestID: this._requestID
            });
        }
    };
    PhotoComponent.prototype._buildAlbum = function () {
        this.albumList = [];
        for (var j = 0; j < this.bellas.length; j++) {
            var _album = {
                src: this.bellas[j].image,
                caption: this.bellas[j].content,
                thumb: this.bellas[j].image,
                thot: this.bellas[j]
            };
            this.albumList.push(_album);
        }
    };
    return PhotoComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], PhotoComponent.prototype, "user", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], PhotoComponent.prototype, "hidden", void 0);
PhotoComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-photos',
        template: __webpack_require__("./src/app/discover/photos.component.html"),
        styles: [__webpack_require__("./src/app/discover/photos.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_5__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_http__["c" /* Http */]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_8__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__core_event_service__["a" /* EventService */]) === "function" && _o || Object, typeof (_p = typeof __WEBPACK_IMPORTED_MODULE_4__lightbox_lightbox_service__["a" /* Lightbox */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__lightbox_lightbox_service__["a" /* Lightbox */]) === "function" && _p || Object])
], PhotoComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p;
//# sourceMappingURL=discover.component.js.map

/***/ }),

/***/ "./src/app/discover/modalimage.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"modal bs-example-modal-lg imagemodal\">\n  <div class=\"modal-dialog \">\n    <div class=\"modal-content \">\n\n      <div class=\"white-box\">\n        <div class=\"user-post\">\n          <div class=\"user-post-left\">\n            <span class=\"avatar-wrapper\" [innerHTML]=\"thot.avatar\"></span>\n            <h4>@{{thot.username }}<span>{{ preview.created_at|messageTime }} </span></h4>\n          </div>\n\n          <div class=\"user-post-right\">\n\n\n            <div class=\"dropdown show hidden\">\n              <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n\n\n                <li class=\"noti\"><img src=\"https://s3.amazonaws.com/bellassets/assets/images/notification-icon.png\"/></li>\n              </a>\n\n              <div class=\"dropdown-menu report-menu\" aria-labelledby=\"dropdownMenuLink\">\n                <div class=\"dropdown-caret\">\n                  <div class=\"caret-outer\"></div>\n                  <div class=\"caret-inner\"></div>\n                </div>\n                TEST THIS OUT\n              </div>\n\n            </div>\n\n            <a href=\"#\"><img src=\"https://s3.amazonaws.com/bellassets/assets/images/doted-icon.png\"/></a>\n          </div>\n\n        </div>\n        <p>{{ preview.content }}</p>\n        <img [src]=\"thot.preview[0].image\"/>\n        <ul class=\"like-box\">\n          <li>\n            <a class=\"\" href=\"#\">\n              <i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i>&nbsp;&nbsp;{{ preview.total_likes_count }}  {{'Likes'|translate}}\n            </a>\n          </li>\n\n          <li class=\"postearnings\">\n            <label data-toggle=\"tooltip\" [title]=\"'Estimated Post Earnings'|translate\">\n              <a href=\"#\"><i class=\"fa fa-dollar\" aria-hidden=\"true\"></i> {{ preview.earnings|number:'1.2-2' }}</a>\n            </label>\n          </li>\n        </ul>\n\n\n\n\n        <div class=\"comment-boxes\">\n\n\n          <div class=\"reply-box\">\n            <div class=\"reply-box-left\">\n              <img src=\"https://s3.amazonaws.com/bellassets/assets/images/follow-img-10.png\"/>\n            </div>\n            <div class=\"reply-box-right\">\n              <h4>@rickyherish</h4>\n              <h5>2  mins ago</h5>\n              <p translate>Awesome shot! Love the tie dye shirt, it really makes the photo</p>\n\n            </div>\n          </div>\n          <div class=\"reply-box reply-box2\">\n            <div class=\"reply-box-left\">\n              <img src=\"https://s3.amazonaws.com/bellassets/assets/images/jems-img3.png\"/>\n            </div>\n            <div class=\"reply-box-right\">\n              <h4>@billyfitch</h4>\n              <h5>2  mins ago</h5>\n              <p>Is this in Westend Park, Chicago? I remember playing in those fountains when I was a kid!</p>\n\n            </div>\n          </div>\n          <div class=\"viewcoment-box\">\n            <a href=\"#\">View more comments  +</a>\n          </div>\n\n          <img src=\"https://s3.amazonaws.com/bellassets/assets/images/jems-img.png\"/>\n\n          <form>\n            <textarea type=\"text\" [placeholder]=\"'Add a comment...'|translate\"></textarea>\n          </form>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/discover/modalimage.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap_modal__ = __webpack_require__("./node_modules/ng2-bootstrap-modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap_modal___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap_modal__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ImageModalComponent = (function (_super) {
    __extends(ImageModalComponent, _super);
    function ImageModalComponent(dialogService) {
        return _super.call(this, dialogService) || this;
    }
    return ImageModalComponent;
}(__WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap_modal__["DialogComponent"]));
ImageModalComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'image-modal',
        template: __webpack_require__("./src/app/discover/modalimage.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap_modal__["DialogService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap_modal__["DialogService"]) === "function" && _a || Object])
], ImageModalComponent);

var _a;
//# sourceMappingURL=modalimage.component.js.map

/***/ }),

/***/ "./src/app/discover/people.component.css":
/***/ (function(module, exports) {

module.exports = ".discoverpeopleli{\n\n  width: 32% !important;\n  position: relative !important;\n  overflow: hidden;\n  background-position: center center;\n  background-size: cover;\n\n}\n\n.discoverpeopleli:after {\n  content: \"\" !important;\n  display: block !important;\n  padding-bottom: 100% !important;\n}\n\n.fade.in {\n  opacity: 1;\n}\n\n.modal.in .modal-dialog {\n  -webkit-transform: translate(0, 0);\n  transform: translate(0, 0);\n}\n\n.modal-backdrop .fade .in {\n  opacity: 0.5 !important;\n}\n\n.modal-backdrop.fade {\n  opacity: 0.5 !important;\n}\n"

/***/ }),

/***/ "./src/app/discover/people.component.html":
/***/ (function(module, exports) {

module.exports = "<div infiniteScroll\n     [infiniteScrollDistance]=\"0.4\"\n     [infiniteScrollThrottle]=\"1000\"\n     (scrolled)=\"onPeopleScroll()\">\n  <div *ngFor=\"let user of people\" class=\"discover-small white-box\">\n\n    <div class=\"user-name table\">\n      <div class=\"user-flower clearfix\">\n\n        <a href=\"#\" [routerLink]=\"['/user/', user.username]\" >\n          <div class=\"user-post-left\">\n            <span class=\"circlephoto avatar-wrapper\" [innerHTML]=\"user.avatar\"></span>\n            <h4 >{{ user.username }}<span class=\"block\">{{'Earnings'|translate}}: ${{user.earnings|number:'1.2-2'}}</span></h4>\n          </div>\n        </a>\n\n      </div>\n\n      <ng-container>\n        <div *ngIf=\"!user.is_following\" class=\"follow notfollowing\">\n          <a href=\"#\" (click)=\"follow(user);false\" translate>Follow</a>\n        </div>\n\n\n        <div *ngIf=\"user.is_following\" class=\"follow following\">\n          <a href=\"#\" (click)=\"unfollow(user);false\" translate>Following</a>\n        </div>\n      </ng-container>\n\n    </div>\n\n    <div class=\"row threetags\">\n      <div *ngFor=\"let preview of user.album; let i=index\" class=\"threetagslink\">\n\n        <a data-toggle=\"modal\" data-target=\"#peopleimage1-{{user.username}}\" (click)=\"openPostBox(user.album, i);false\">\n          <img class=\"threetagsimage\" src=\"{{preview.src}}\" />\n        </a>\n\n      </div>\n\n    </div>\n\n  </div>\n  <sk-three-bounce *ngIf=\"loading == true\"></sk-three-bounce>\n</div>\n"

/***/ }),

/***/ "./src/app/discover/photos.component.css":
/***/ (function(module, exports) {

module.exports = ".lightboxmodal .comment-boxes{\n\n  overflow: scroll;\n  height: 200px;\n  margin-bottom: 20px;\n  margin-top: -20px;\n}\n\n.inlinecomment{\n  margin-top: -10px;\n  margin-left: -25px;\n  padding-left: 25px;\n  margin-bottom: -10px;\n  line-height: 25px !important;\n  height: 55px;\n  background: none;\n  width: calc(100% - 75px);\n}\n\n.commentsubmit{\n  background: #FFF;\n  color: #F87433;\n  border: 2px solid #F87433;\n  padding: 5px 10px 5px 10px;\n  float: right;\n  border-radius: 5px;\n  vertical-align: top;\n}\n"

/***/ }),

/***/ "./src/app/discover/photos.component.html":
/***/ (function(module, exports) {

module.exports = "<div infiniteScroll\n     [infiniteScrollDistance]=\"0.4\"\n     [infiniteScrollThrottle]=\"1000\"\n     (scrolled)=\"onPhotoScroll()\" style=\"float:left;width:100%;\">\n  <div class=\"row sortrow\">\n    <div class=\"dropdown\">\n      <button onclick=\"dropdownSort()\" class=\"dropbtn\">\n      <span>\n        <i class=\"fa fa-fire\" aria-hidden=\"true\"></i>\n        &nbsp;Hot&nbsp;\n      </span>\n        <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i></button>\n      <div id=\"dropdownsort\" class=\"dropdown-content dropdown-content-sort\">\n        <a href=\"#\" onClick=\"select_sort_btn(this);false\" (click)=\"sort_by_hot();false\" class=\"hide\" translate><i class=\"fa fa-fire\" aria-hidden=\"true\"></i> &nbsp;Hot&nbsp;</a>\n        <a href=\"#\" onClick=\"select_sort_btn(this);false\" (click)=\"sort_by_new();false\" translate> <i class=\"fa fa-hourglass\"></i> &nbsp;New&nbsp;</a>\n        <a href=\"#\" onClick=\"select_sort_btn(this);false\" (click)=\"sort_by_likes();false\" translate><i class=\"fa fa-heart\"></i> &nbsp;Most Liked&nbsp;</a>\n      </div>\n    </div>\n\n\n    <div class=\"dropdown\" *ngIf=\"select_most_liked\">\n      <button onclick=\"dropdownTime()\" class=\"dropbtn\">\n        <i class=\"fa fa-clock\" aria-hidden=\"true\"></i>\n        <span>\n        &nbsp;Today&nbsp;\n        </span>\n        <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i></button>\n      <div id=\"dropdowntime\" class=\"dropdown-content dropdown-content-time\">\n        <a href=\"#\" onClick=\"select_sort_btn(this);false\" (click)=\"sort_by_likes('today');false\" class=\"hide\" translate>Today</a>\n        <a href=\"#\" onClick=\"select_sort_btn(this);false\" (click)=\"sort_by_likes('week');false\" translate>This Week</a>\n        <a href=\"#\" onClick=\"select_sort_btn(this);false\" (click)=\"sort_by_likes('month');false\" translate>This Month</a>\n        <a href=\"#\" onClick=\"select_sort_btn(this);false\" (click)=\"sort_by_likes('all');false\" translate>All Time</a>\n      </div>\n    </div>\n  </div>\n\n  <app-post-box *ngFor=\"let thot of bellas;index as i\" [thot]=\"thot\" [user]=\"user\" [index]=\"i\" (delete)=\"delete($event)\" (openThotBox)=\"openPostBox($event)\"></app-post-box>\n  <sk-three-bounce *ngIf=\"loading == true\"></sk-three-bounce>\n</div>\n"

/***/ }),

/***/ "./src/app/discover/tags.component.html":
/***/ (function(module, exports) {

module.exports = "<div infiniteScroll\n     [infiniteScrollDistance]=\"0.4\"\n     [infiniteScrollThrottle]=\"1000\"\n     (scrolled)=\"onTagScroll()\" class=\"d-tags-container\">\n\n  <div *ngFor=\"let tag of tags\" class=\"discover-smmal white-box\">\n\n\n    <div class=\"col-sm-12 col-lg-12 col-md-12 user-post-left\">\n\n      <div class=\"tagsheader\">\n        <a [routerLink]=\"['/discover/', tag.tag]\">\n          <i class=\"fas fa-hashtag lightningbolt\"></i>\n          <h4 class=\"trendingtags\"><span class=\"bold\">#{{ tag.tag }}</span>\n            <span class=\"block\"> {{tag.posting_count}}\n                      <ng-template #posts>\n                       {{'Post'|translate}}\n                      </ng-template>\n                      <ng-container *ngIf=\"tag.posting_count!=1; else posts\">\n                        {{'Posts'|translate}}\n                      </ng-container>\n                    </span>\n          </h4>\n        </a>\n        <a href=\"#\" [routerLink]=\"['/discover/',tag.tag]\" class=\"viewall\">View All</a>\n      </div>\n\n\n\n    </div>\n\n    <a class=\"maintaglink\" href=\"#\" (click)=\"openPostBox(tag, 0);false\">\n      <img class=\"maintagimage\" [src]=\"tag.preview[0].image\"/>\n    </a>\n\n    <div class=\"row threetags\">\n      <a class=\"threetagslink\" href=\"#\" *ngFor=\"let preview of tag.preview.slice(1,4) ; let i=index;\"\n         (click)=\"openPostBox(tag, i+1);false\">\n        <img class=\"threetagsimage\" [src]=\"preview.image\"/>\n      </a>\n    </div>\n\n\n  </div>\n\n  <sk-three-bounce *ngIf=\"loading == true\"></sk-three-bounce>\n</div>\n"

/***/ }),

/***/ "./src/app/earn-free-bela/earn-free-bela.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/earn-free-bela/earn-free-bela.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"white-box\">\n  <h4 translate>Earn Free Bela</h4>\n  <div class=\"trendingbox trendingbox-padding\">\n    <p class=\"sidebar-verify center\" *ngIf=\"!requesting_user.user1.phone_number\" translate>\n      <a [routerLink]=\"'/verify/'\">Click here to verify your identity and earn free Bela</a>\n    </p>\n    <p *ngFor=\"let task of tasks\">\n      <i\n        [ngClass]=\"{\n         'fas fa-check-circle text-green': task.is_complete,\n         'far fa-circle': !task.is_complete\n         }\"></i>\n      {{task.description}}\n      <span class=\"task-reward\"> ({{task.reward}} Bela)</span></p>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/earn-free-bela/earn-free-bela.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EarnFreeBelaComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_event_types__ = __webpack_require__("./src/app/core/event.types.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_event_service__ = __webpack_require__("./src/app/core/event.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var EarnFreeBelaComponent = (function () {
    function EarnFreeBelaComponent(authService, eventService, belaApiService) {
        this.authService = authService;
        this.eventService = eventService;
        this.belaApiService = belaApiService;
        this.requesting_user = new __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]();
        this.subscriptions = [];
    }
    EarnFreeBelaComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.requesting_user = this.authService.user;
        this.assignTasks();
        this.subscriptions.push(this.eventService.subscribe({
            next: function (event) {
                if (event.type == __WEBPACK_IMPORTED_MODULE_3__core_event_types__["b" /* BelaEventType */].UPDATE_USER) {
                    _this.requesting_user = event.value;
                    _this.assignTasks();
                }
            }
        }));
    };
    EarnFreeBelaComponent.prototype.ngOnDestroy = function () {
        for (var _i = 0, _a = this.subscriptions; _i < _a.length; _i++) {
            var subscription = _a[_i];
            subscription.unsubscribe();
        }
    };
    EarnFreeBelaComponent.prototype.assignTasks = function () {
        var _this = this;
        if (!this.requesting_user.user1.phone_number) {
            this.tasks = [];
        }
        else {
            this.belaApiService.getRewardTasks().then(function (res) {
                _this.tasks = res.results;
            });
        }
    };
    return EarnFreeBelaComponent;
}());
EarnFreeBelaComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-earn-free-bela',
        template: __webpack_require__("./src/app/earn-free-bela/earn-free-bela.component.html"),
        styles: [__webpack_require__("./src/app/earn-free-bela/earn-free-bela.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__core_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_auth_service__["a" /* AuthService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_event_service__["a" /* EventService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _c || Object])
], EarnFreeBelaComponent);

var _a, _b, _c;
//# sourceMappingURL=earn-free-bela.component.js.map

/***/ }),

/***/ "./src/app/help/help.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/help/help.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  help works!\n</p>\n"

/***/ }),

/***/ "./src/app/help/help.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HelpComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HelpComponent = (function () {
    function HelpComponent() {
    }
    HelpComponent.prototype.ngOnInit = function () {
    };
    return HelpComponent;
}());
HelpComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-help',
        template: __webpack_require__("./src/app/help/help.component.html"),
        styles: [__webpack_require__("./src/app/help/help.component.css")]
    }),
    __metadata("design:paramtypes", [])
], HelpComponent);

//# sourceMappingURL=help.component.js.map

/***/ }),

/***/ "./src/app/homefeed/homefeed.component.css":
/***/ (function(module, exports) {

module.exports = ".content3 h3 {\n  color: #000;\n\n}\n\n.content3 a:hover {\n\n  text-decoration: none;\n}\n"

/***/ }),

/***/ "./src/app/homefeed/homefeed.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"(requesting_user.id !==  undefined) && !loading\" class=\"user-profile-box hideonphones\">\n  <div class=\"panel with-nav-tabs\">\n\n    <app-posting-form  [requesting_user]=\"requesting_user\" [thots]=\"thoughts\"></app-posting-form>\n  </div>\n</div>\n\n\n<div class=\"notfollowinganyone col-sm-12 col-lg-12 col-md-12\"\n     *ngIf=\"thoughts?.length < 1 && !loading\">\n\n  <img height=\"200px\" width=\"200px\" src=\"https://s3.amazonaws.com/bellassets/orangeicon.png\"/>\n  <h2 translate>Welcome to Belacam!</h2>\n\n\n  <h4 translate>Belacam is a social media site where social interactions carry real economic value. When you post a photo and someone gives it a 'like', you receive a micro-transaction of the Bela token. <br><br>\n      \n      When you 'like' someone else's post, you're giving them a micro-tip as well. That means about $.025 - $.10 worth of Bela (depending on Bela's market price) is given from your account to theirs. \n    \n    \n    </h4>\n\n  <hr>\n  <h3><strong translate>What is Bela?</strong>  </h3>\n<h4 translate>Bela is the cryptocurrency that powers Belacam. When you get or give a like, you're either receiving or giving Bela. Bela has real-world value and fluctuates as people trade it on markets and exchanges. <br><br> You can cash out Bela that you've earned at any time by withdrawing it to an exchange, selling it for Bitcoin, and then selling that Bitcoin for your country's native currency.</h4>\n\n\n\n\n\n  <a [routerLink]=\"['/discover']\" href=\"#\"><h1 translate>Let's get started!</h1></a></div>\n\n\n<div class=\"discover-box\">\n  <div class=\"panel with-nav-tabs\">\n\n    <div class=\"panel-body\">\n\n      <div class=\"tab-content\">\n\n\n        <div class=\"fade in active endless_page_template\" infiniteScroll\n             [infiniteScrollDistance]=\"0.4\"\n             [infiniteScrollThrottle]=\"1000\"\n             (scrolled)=\"onFeedScroll()\" style=\"float:left;width:100%;\">\n\n          <app-post-box *ngFor=\"let thot of thoughts; index as i\"\n                        [user]=\"requesting_user\"\n                        [thot]=\"thot\"\n                        [index]=\"i\"\n                        (delete)=\"delete($event)\"\n                        (openThotBox)=\"openThotBox($event)\">\n          </app-post-box>\n\n          <sk-three-bounce *ngIf=\"loading == true\"></sk-three-bounce>\n\n\n        </div>\n\n\n      </div>\n\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/homefeed/homefeed.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomefeedComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__constants__ = __webpack_require__("./src/app/constants.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lightbox_lightbox_service__ = __webpack_require__("./src/app/lightbox/lightbox.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_event_service__ = __webpack_require__("./src/app/core/event.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_event_types__ = __webpack_require__("./src/app/core/event.types.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var HomefeedComponent = (function () {
    function HomefeedComponent(appService, _lightbox, authService, eventService) {
        this.appService = appService;
        this._lightbox = _lightbox;
        this.authService = authService;
        this.eventService = eventService;
        this.thoughts = [];
        this.album = [];
        this.requesting_user = new __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]();
        this.homefeeds_api_url = '/api/homefeeds/';
        this.cover_pic_url = __WEBPACK_IMPORTED_MODULE_3__constants__["a" /* Constants */].default_cover_pic;
        this.subscriptions = [];
        this.loading = true;
    }
    HomefeedComponent.prototype.ngOnInit = function () {
        //this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
        var _this = this;
        this.setRequestingUser(this.authService.user);
        this.subscriptions.push(this.eventService.subscribe({
            next: function (val) {
                if (val.type === __WEBPACK_IMPORTED_MODULE_7__core_event_types__["b" /* BelaEventType */].ADDED_POST) {
                    _this.thoughts.unshift(val.value);
                    _this._buildAlbum();
                }
                else if (val.type === __WEBPACK_IMPORTED_MODULE_7__core_event_types__["b" /* BelaEventType */].DELETED_POST) {
                    _this.delete(val.value);
                }
                else if (val.type === __WEBPACK_IMPORTED_MODULE_7__core_event_types__["b" /* BelaEventType */].REQUEST_NEXT_ALBUM) {
                    _this._requestAlbum(val.value);
                }
                else if (val.type === __WEBPACK_IMPORTED_MODULE_7__core_event_types__["b" /* BelaEventType */].UPDATE_USER) {
                    _this.setRequestingUser(val.value);
                }
            }
        }));
        this.loading = true;
        this._loadNextPage().subscribe(function () {
            _this.loading = false;
            window.scroll(0, 0);
        });
    };
    HomefeedComponent.prototype.setRequestingUser = function (user) {
        this.cover_pic_url =
            user.user1 && user.user1.cover_pic ? user.user1.cover_pic : __WEBPACK_IMPORTED_MODULE_3__constants__["a" /* Constants */].default_cover_pic;
        this.requesting_user = user;
    };
    HomefeedComponent.prototype.ngOnDestroy = function () {
        for (var _i = 0, _a = this.subscriptions; _i < _a.length; _i++) {
            var subscription = _a[_i];
            subscription.unsubscribe();
        }
    };
    HomefeedComponent.prototype.onFeedScroll = function () {
        var _this = this;
        var bottom_reached = this.appService.isBottomReached();
        if (this.homefeeds_api_url != null && bottom_reached && !this.loading) {
            this.loading = true;
            this._loadNextPage().subscribe(function () {
                _this.loading = false;
            });
        }
    };
    HomefeedComponent.prototype.delete = function (thot) {
        var idx = this.thoughts.indexOf(thot);
        if (idx > -1) {
            this.thoughts.splice(idx, 1);
        }
        this._buildAlbum();
    };
    HomefeedComponent.prototype.openThotBox = function (index) {
        if (index >= 0) {
            this._lightbox.openPost(this.album, index, {
                disableKeyboardNav: true,
                wrapAround: false,
                allowRequestAlbum: true
            });
        }
    };
    HomefeedComponent.prototype._buildAlbum = function () {
        this.album = [];
        for (var j = 0; j < this.thoughts.length; j++) {
            var _album = {
                src: this.thoughts[j].image,
                caption: this.thoughts[j].content,
                thumb: this.thoughts[j].image,
                thot: this.thoughts[j]
            };
            this.album.push(_album);
        }
    };
    HomefeedComponent.prototype._loadNextPage = function () {
        var _this = this;
        return this.appService.getList(this.homefeeds_api_url)
            .map(function (listResp) {
            _this.homefeeds_api_url = listResp.next;
            for (var i in listResp.results) {
                _this.thoughts.push(listResp.results[i]);
            }
            _this._buildAlbum();
        });
    };
    HomefeedComponent.prototype._requestAlbum = function (request) {
        var _this = this;
        if (this.homefeeds_api_url != null && request.direction === __WEBPACK_IMPORTED_MODULE_7__core_event_types__["d" /* EvRequestAlbum */].DIRECTION_NEXT) {
            this._loadNextPage().subscribe(function () {
                var evValue = new __WEBPACK_IMPORTED_MODULE_7__core_event_types__["c" /* EvLoadedAlbum */]();
                evValue.album = _this.album;
                evValue.newImageIndex = request.currentImageIndex + 1;
                _this.eventService.fireEvent({
                    type: __WEBPACK_IMPORTED_MODULE_7__core_event_types__["b" /* BelaEventType */].LOADED_NEXT_ALBUM,
                    value: evValue
                });
            });
        }
        else {
            this.eventService.fireEvent({
                type: __WEBPACK_IMPORTED_MODULE_7__core_event_types__["b" /* BelaEventType */].NO_NEXT_ALBUM,
                value: null
            });
        }
    };
    return HomefeedComponent;
}());
HomefeedComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-homefeed',
        template: __webpack_require__("./src/app/homefeed/homefeed.component.html"),
        styles: [__webpack_require__("./src/app/homefeed/homefeed.component.css")],
        providers: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__lightbox_lightbox_service__["a" /* Lightbox */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__lightbox_lightbox_service__["a" /* Lightbox */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* AuthService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_6__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_event_service__["a" /* EventService */]) === "function" && _d || Object])
], HomefeedComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=homefeed.component.js.map

/***/ }),

/***/ "./src/app/insufficient-bela/insufficient-bela.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"outofbela\" [hidden]=\"!visible\">\n  <a class=\"close\" (click)=\"close()\">\n    <i class=\"fas fa-times\" aria-label=\"Close message\"></i>\n  </a>\n\n  <h3>Not enough Bela!</h3>\n\n  <h4 class=\"account\" translate>Each <img src=\"https://s3.amazonaws.com/bellassets/assets/images/redheart.png\"> gives {{user.bella_per_like}} <img src=\"https://s3.amazonaws.com/bellassets/assets/images/orangebela.png\">\n  </h4>\n\n  <p>In Belacam, likes give money. When you give a like, you give Bela from your account to someone else. And for every like you get, that's another real person giving you some Bela as well.</p>\n\n  <a class=\"btn-submit-post\" [routerLink]=\"['/deposit']\" translate>Deposit Bela</a>\n  <a class=\"btn-submit-post\" [routerLink]=\"['/referral/dashboard']\" href=\"#\" translate>Earn Bela</a>\n\n</div>\n"

/***/ }),

/***/ "./src/app/insufficient-bela/insufficient-bela.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InsufficientBelaComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_model__ = __webpack_require__("./src/app/app.model.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InsufficientBelaComponent = (function () {
    function InsufficientBelaComponent() {
        this.user = new __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]();
        this.visible = false;
    }
    InsufficientBelaComponent.prototype.ngOnInit = function () {
    };
    InsufficientBelaComponent.prototype.close = function () {
        this.visible = false;
    };
    InsufficientBelaComponent.prototype.show = function () {
        this.visible = true;
    };
    return InsufficientBelaComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]) === "function" && _a || Object)
], InsufficientBelaComponent.prototype, "user", void 0);
InsufficientBelaComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-insufficient-bela',
        template: __webpack_require__("./src/app/insufficient-bela/insufficient-bela.component.html"),
        styleUrls: []
    }),
    __metadata("design:paramtypes", [])
], InsufficientBelaComponent);

var _a;
//# sourceMappingURL=insufficient-bela.component.js.map

/***/ }),

/***/ "./src/app/landing-page/landing-page.component.css":
/***/ (function(module, exports) {

module.exports = "  .show {\n    display: block;\n  }\n\n  .hide {\n    display: none;\n  }\n\n  .homepageheader {\n    display: block;\n    border: none;\n    height: 120px;\n    background-color: Transparent;\n    padding: 20px 40px 20px 40px;\n  }\n\n  .logo_link:hover {\n    text-decoration: none;\n  }\n\n  .logo {\n    display: none;\n    width: 50px;\n    vertical-align: top;\n  }\n\n  .web_name {\n    display: inline-block;\n    color: #FFF;\n    vertical-align: top;\n    margin: 0 auto;\n    line-height: 50px;\n    padding: 0px 10px 0px 10px;\n    font-weight: 600;\n    font-size: 35px;\n  }\n\n  .explore_belacam {\n    display: inline-block;\n    color: #FFF;\n    vertical-align: top;\n    margin: 0 auto;\n\n    height: 50px;\n    padding: 0px 10px 0px 10px;\n    font-weight: 600;\n\n  }\n\n  .explore_belacam h2 {\n    line-height: 50px;\n\n    margin: 0 auto;\n    font-size: 20px;\n    font-weight: 900;\n  }\n\n  .explore_belacam:hover, .reset_password:hover {\n    color: #FFF;\n  }\n\n  #login {\n    float: right;\n  }\n\n  .logininput {\n    margin-top: 3px;\n    border-radius: 5px;\n    display: inline-block !important;\n    border: 1px solid #c0c0c0;\n    color: #000000;\n    margin-right: 10px;\n    padding: 10px;\n    width: 150px;\n    font-size: 16px;\n    font-family: 'proximanovalight';\n  }\n\n  .submitbutton {\n    font-size: 18px;\n    line-height: 30px;\n    margin-top: 3px !important;\n  }\n\n  .reset_password {\n    display: block;\n    clear: both;\n    float: right;\n    color: #FFF;\n    margin-right: 88px;\n  }\n\n  .hook_em_in {\n    clear: both;\n    display: block;\n    width: 100%;\n    text-align: center;\n    color: #FFF;\n  }\n\n  .hook_title {\n    font-size: 45px;\n    font-weight: 600;\n    max-width: 100vw;\n    padding-bottom: 30px;\n    margin: 0 auto;\n\n  }\n\n  .howitworks {\n\n    display: -webkit-box;\n\n    display: -ms-flexbox;\n\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n  }\n\n  .howitworks {\n    color: #FFF;\n    font-size: 35px;\n    text-align: center;\n  }\n\n  .howitworks i {\n\n    text-align: center;\n\n    width: 100px;\n    height: 100px;\n    border-radius: 100px;\n    line-height: 100px;\n\n    display: block;\n    margin: 0 auto;\n    font-size: 70px;\n    margin-bottom: 10px;\n  }\n\n  .howitworks div {\n    font-weight: 600;\n    font-size: 30px;\n  }\n\n  .signuprow {\n\n    border-radius: 10px;\n    background-color: #FDFDFD;\n    display: block;\n    width: 80%;\n    max-width: 900px;\n    margin: 0 auto;\n    margin-top: 40px;\n  }\n\n  .signup_form {\n    width: 100%;\n    text-align: center;\n\n  }\n\n  .signup_form h2 {\n    font-weight: 600;\n    color: #F87433;\n\n  }\n\n  .signup_form input {\n    border: 1px solid Green;\n  }\n\n  .signup_form div.input-div {\n    display: inline-block;\n    position: relative;\n    vertical-align: top;\n  }\n\n  ul.errorlist {\n    color: red !important;\n  }\n\n  ul.errorlist li {\n    font-weight: 700;\n  }\n\n  .revealpassword {\n    position: absolute;\n    background: #FFF;\n    color: #F87433;\n    border: 2px solid #F87433;\n    padding: 5px 10px 5px 10px;\n    border-radius: 5px;\n    vertical-align: top;\n    margin-top: 5px;\n    right: 12px;\n  }\n\n  .revealpassword:hover {\n    background: #F87433;\n    color: #FFF;\n  }\n\n  .signup_form input[type=\"text\"] {\n    border-radius: 5px;\n    border: 1px solid #c0c0c0;\n    color: #000000;\n    margin-bottom: 7px;\n    padding: 10px;\n    margin-left: 5px;\n    margin-right: 5px;\n    font-size: 16px;\n    font-family: 'proximanovalight';\n  }\n\n  .signup_form input[type=\"password\"] {\n    border-radius: 5px;\n    border: 1px solid #c0c0c0;\n    color: #000000;\n    margin-bottom: 7px;\n    padding: 10px;\n    font-size: 16px;\n    font-family: 'proximanovalight';\n\n    margin-left: 5px;\n    margin-right: 5px;\n  }\n\n  #signup_submit {\n    border: 2px solid #F87433;\n    display: block;\n    margin: 0 auto;\n    clear: both;\n    float: none;\n  }\n\n  .accept_the_terms {\n    margin-top: 10px;\n    font-size: 14px;\n    color: #999;\n  }\n\n  .Form-label-tick {\n    width: 100%;\n    margin-bottom: 20px;\n    margin-top: 0px;\n    text-align: center;\n  }\n\n  .Form-label-checkbox:checked + .Form-label-text::before {\n    background-color: lightgreen;\n    border: 1px solid #CCC !important;\n    font-family: 'Font Awesome\\ 5 Free';\n    content: \"\\f058\";\n  }\n\n  .Form-label-checkbox + .Form-label-text::before {\n    border-radius: 5px;\n    font-size: 18px !important;\n    line-height: 1.6;\n    background-clip: border-box;\n  }\n\n  .hidethisw {\n    color: inherit !important;\n    font-weight: inherit !important;\n  }\n\n  .footerrow {\n    display: block;\n    width: 100%;\n    text-align: center;\n    clear: both;\n    padding-top: 10px;\n    margin-bottom: 50px;\n  }\n\n  .footerrow a {\n    color: #FFF;\n  }\n\n  .footerrow li {\n    display: inline-block;\n    padding-top: 20px;\n    padding: 5px;\n  }\n\n  .signup-box {\n    padding-top: 0px;\n\n  }\n\n  .infopart {\n    background-size: cover !important;\n    height: 100%;\n    /*background: #7B6F6C; */\n\n  }\n\n  .infopart h1 {\n    font-size: 100px;\n    font-weight: 600;\n  }\n\n  .loginpart {\n    float: right;\n    background-color: #FAFCFF;\n    height: 100%;\n    padding: 0px !important;\n  }\n\n  .infofont {\n    font-size: 22px !important;\n  }\n\n  .insta-form {\n    width: 97%;\n    position: absolute;\n    top: 50%;\n    -webkit-transform: translateY(-50%);\n            transform: translateY(-50%);\n    background-color: #FAFCFF;\n  }\n\n  .insta-form h4 {\n    padding-left: 20px;\n    padding-right: 20px;\n    font-size: 30px;\n    color: #F97B45;\n    font-weight: 600;\n  }\n\n  .tab-pane {\n    margin-top: 0px !important;\n\n  }\n\n  .orange {\n    color: #F97B45;\n  }\n\n  .signup-box {\n    position: relative;\n  }\n\n  .fholder {\n    width: calc(100% - 70px);\n    padding-right: 10px;\n    position: absolute;\n    top: 40%;\n    -webkit-transform: translateY(-40%);\n            transform: translateY(-40%);\n  }\n\n  .fholder hr {\n    margin-bottom: 60px;\n\n  }\n\n  .footerhelper {\n    text-align: center;\n    padding: 20px;\n    min-height: 100%;\n    margin: 0 auto -20px;\n  }\n\n  #mc_embed_signup {\n    padding: 10px;\n    background: #fff;\n    clear: left;\n    font: 14px Helvetica, Arial, sans-serif;\n\n    text-align: center;\n\n  }\n\n  #mc_embed_signup form {\n    padding: 0px !important;\n  }\n\n  #mc_embed_signup_scroll {\n    height: 50px;\n\n  }\n\n  .preemail {\n    width: 100% !important;\n\n    height: 50px !important;\n    font-size: 18px !important;\n    margin-top: 20px !important;\n    margin-bottom: 40px !important;\n  }\n\n  .loginrow {\n\n    margin: 0 auto;\n    text-align: center;\n    padding: 10px;\n    padding-bottom: 0px;\n    margin-bottom: -10px;\n\n  }\n\n  .loginrow a {\n    font-size: 20px;\n    border-radius: 5px;\n    color: #F87433;\n  }\n\n  .loginrow a:hover {\n    text-decoration: underline;\n  }\n\n  .exit_login {\n    display: none;\n  }\n\n  .explore_belacam_mobile {\n    display: none;\n  }\n\n  .mobile_log_in {\n    display: none;\n  }\n\n  @media only screen and (max-width: 830px) {\n    .explore_belacam h2 {\n      font-size: 16px;\n\n    }\n\n  }\n\n  @media only screen and (min-width: 800px) {\n    #login {\n      display: inline-block !important;\n    }\n  }\n\n  @media only screen and (max-width: 799px) {\n    .logo {\n      display: inline-block;\n    }\n\n    .web_name {\n      display: none;\n    }\n\n    .mobile_log_in {\n      display: inline-block;\n      float: right;\n    }\n\n    .mobile_log_in:hover {\n      cursor: pointer;\n    }\n\n    #login a:hover {\n      cursor: pointer;\n    }\n\n    .signup_form input {\n      width: 150px;\n    }\n\n    .explore_belacam_mobile {\n      display: inline-block;\n      float: right;\n    }\n\n    .explore_belacam_desktop {\n      display: none;\n    }\n\n    #login {\n      z-index: 999;\n      display: none;\n      width: 100%;\n      position: absolute;\n      text-align: center;\n      background-color: #FFF;\n\n      top: 0px;\n      left: 0px;\n      bottom: 0px;\n      right: 0px;\n\n    }\n\n    .exit_login {\n      display: block;\n      position: absolute;\n      float: right;\n      top: 15px;\n      right: 20px;\n      font-size: 50px;\n      color: #F87433;\n\n    }\n\n    #login h1 {\n      margin-top: 100px;\n      color: #F87433;\n    }\n\n    .logininput {\n      width: 80%;\n\n      display: block !important;\n      margin: 0 auto;\n      margin-top: 10px;\n      margin-bottom: 10px;\n    }\n\n    .reset_password {\n      clear: both;\n      width: 100%;\n      display: block;\n      margin-right: -15px !important;\n\n    }\n\n    #login .submitbutton {\n\n      float: none;\n    }\n  }\n\n  @media only screen and (max-width: 650px) {\n    .signup_form div.input-div {\n      display: block;\n      position: relative;\n    }\n\n    .signup_form input {\n      display: block;\n      width: 80%;\n      margin: 0 auto !important;\n      margin-top: 10px !important;\n      margin-bottom: 10px !important;\n    }\n\n    .revealpassword {\n      float: right;\n      margin-right: 40px;\n    }\n\n  }\n\n  @media only screen and (max-width: 500px) {\n\n    .howitworks div {\n      font-size: 18px;\n    }\n\n    .real_background {\n      bottom: 0 !important;\n    }\n\n    .revealpassword {\n      float: right;\n      margin-right: 30px;\n    }\n  }\n\n  @media only screen and (max-width: 360px) {\n    .howitworks i {\n      font-size: 50px;\n    }\n\n    .revealpassword {\n      margin-right: 20px;\n    }\n\n    .explore_belacam h2 {\n      width: 50px;\n      line-height: 16px;\n      margin-right: 20px;\n      margin-top: 10px;\n      text-align: center;\n    }\n  }\n\n  .orange_background {\n    position: relative;\n    padding-bottom: 200px;\n    margin-bottom: -200px;\n  }\n\n  .real_background {\n\n    position: absolute;\n    top: 0;\n    bottom: 0;\n    right: 0;\n    left: 0;\n\n    background-image: -webkit-gradient(linear, left top, left bottom, from(#ff9d2f), to(#ff6126));\n\n    background-image: linear-gradient(#ff9d2f, #ff6126);\n    -webkit-transform: skewY(6deg);\n            transform: skewY(6deg);\n    -webkit-transform-origin: top right;\n            transform-origin: top right;\n    z-index: -999;\n  }\n\n  .about_belacam {\n    max-width: 1100px;\n    background-color: #FFF;\n    border-radius: 25px;\n    margin: 0 auto;\n    padding: 30px;\n    margin-bottom: 50px;\n  }\n\n  .about_belacam a:hover {\n    cursor: pointer;\n  }\n\n  .about_belacam .submitbutton {\n    float: none;\n    display: block;\n    margin: 0 auto;\n    text-align: center;\n    max-width: 250px;\n  }\n\n  .about_belacam .submitbutton:hover {\n    text-decoration: none !important;\n  }\n\n  .about_belacam img {\n    height: 300px;\n    width: 300px;\n    margin: 0 auto;\n    display: block;\n  }\n\n  .about_belacam h1, .about_belacam h2, .about_belacam h3 {\n    text-align: center;\n    color: #F87433;\n    font-weight: 900;\n  }\n\n  .about_belacam p {\n    font-size: 22px;\n    font-weight: 200;\n    line-height: 30px;\n    margin-top: 15px;\n    margin-bottom: 15px;\n  }\n\n  .left_align {\n    text-align: left !important;\n  }\n\n"

/***/ }),

/***/ "./src/app/landing-page/landing-page.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"alert fade in alert-danger\" *ngIf=\"alert_message\">\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n    {{ alert_message }}\n</div>\n<div class=\"orange_background\">\n  <div class=\"real_background\"></div>\n  <div class=\"homepageheader\">\n    <a class=\"logo_link\" href=\"#\" [routerLink]=\"['/discover']\">\n      <img class=\"logo\" src=\"https://s3.amazonaws.com/bellassets/orangeicon.png\" alt=\"Belacam Logo\"/>\n\n      <h1 class=\"web_name\">Belacam</h1>\n    </a>\n    <a class=\"submitbutton mobile_log_in\" (click)=\"showLogin()\">Log In</a>\n    <a href=\"#\" [routerLink]=\"['/discover']\" class=\"explore_belacam explore_belacam_mobile\"><h2>Browse Belacam</h2></a>\n\n\n    <form id=\"login\" class=\"hide\" role=\"form\" (ngSubmit)=\"signin($event);false\">\n      <a href=\"#\" [routerLink]=\"['/discover']\" class=\"explore_belacam explore_belacam_desktop\"><h2>Browse Belacam</h2></a>\n\n      <a (click)=\"showLogin()\"><i class=\"fas fa-times exit_login\"></i></a>\n      <h1 class=\"showonmobile\">Login</h1>\n      <input class=\"logininput\" type=\"text\" placeholder=\"Username\" name=\"signin_username\" [(ngModel)]=\"signin_username\">\n\n      <input class=\"logininput\" type=\"password\" placeholder=\"Password\" name=\"signin_password\"  [(ngModel)]=\"signin_password\">\n\n      <input class=\"submitbutton\" type=\"submit\" value=\"Log in\" name=\"login-form\">\n\n\n      <a class=\"reset_password\" href=\"#\" [routerLink]=\"['/password_reset']\">Reset Password</a>\n    </form>\n  </div>\n\n\n  <div class=\"hook_em_in\">\n    <div>\n      <h1 class=\"hook_title\">A Social Media Economy</h1>\n    </div>\n\n    <div class=\"howitworks\">\n      <div class=\"col-xs-4 col-sm-3 col-lg-3 col-md-3\"><i class=\"fas fa-users\"></i>Build a Following</div>\n      <div class=\"col-xs-4 col-sm-3 col-lg-3 col-md-3\"><i class=\"fas fa-camera-retro\"></i>Share Photos</div>\n      <div class=\"col-xs-4 col-sm-3 col-lg-3 col-md-3\"><i class=\"fas fa-hand-holding-heart\"></i>Earn $.05 per Like</div>\n    </div>\n  </div>\n\n\n  <div class=\"row signuprow\">\n\n\n    <form class=\"signup_form\" role=\"form\" method=\"post\">\n      <h2>Start Getting Paid</h2>\n      <div style=\"margin-bottom: 10px\">\n        <div class=\"input-div\">\n          <input id=\"signup_email\" name=\"signup_email\" placeholder=\"Email\" required=\"required\" tabindex=\"1\" type=\"text\" aria-required=\"true\" aria-invalid=\"false\" [(ngModel)]=\"signup_email\">\n        </div>\n        <div class=\"input-div\">\n          <input id=\"signup_username\" name=\"signup_username\" maxlength=\"100\" placeholder=\"Username\" required=\"required\" tabindex=\"2\" type=\"text\" aria-required=\"true\" aria-invalid=\"false\" [(ngModel)]=\"signup_username\">\n        </div>\n        <div class=\"input-div\">\n          <button class=\"revealpassword\">Show</button>\n          <input id=\"signup_password\" name=\"signup_password\" placeholder=\"Password\" required=\"required\" tabindex=\"3\" type=\"password\" aria-required=\"true\" aria-invalid=\"false\" [(ngModel)]=\"signup_password\">\n        </div>\n      </div>\n      <re-captcha size=\"invisible\" #captchaRef=\"reCaptcha\" (resolved)=\"$event && signup($event)\"></re-captcha>\n\n      <input class=\"submitbutton\" id=\"signup_submit\" type=\"submit\" value=\"Sign Up\"  (click)=\"$event.preventDefault();is_signup_form_valid() && captchaRef.execute();\">\n      <p class=\"accept_the_terms\">By clicking \"Sign Up,\" you agree to the <a [routerLink]=\"['/terms']\">Terms and Conditions</a> of\n        Belacam.</p>\n\n\n    </form>\n\n  </div>\n\n  <ul class=\"footerrow\">\n    <li><a [routerLink]=\"['/about']\">About</a></li>\n    <li><a href=\"https://belacam.zendesk.com/hc/en-us\">Support</a></li>\n    <li><a target=\"_blank\" href=\"http://Livebela.com\">Bela</a></li>\n    <li><a [routerLink]=\"['/privacy']\">Privacy</a></li>\n    <li><a [routerLink]=\"['/terms']\">Terms</a></li>\n  </ul>\n\n</div>\n\n\n<div class=\"about_belacam\">\n  <img src=\"https://s3.amazonaws.com/bellassets/tyler.png\" alt=\"Belacam Logo\"/>\n\n  <h1>Snap, Post, Paid. It's that easy.</h1>\n\n\n  <p>Belacam is a social media site where you get paid for creating and sharing content that other people enjoy. Each\n    'like' carries a $.05 - $.10 tip, so each 'like' you get means someone else enjoyed your photo. When you give\n    someone else a like, you're tipping them for sharing their content too. It's an economy based around our social\n    interactions.</p>\n\n\n  <p>Whether motivated by goodwill, honest enjoyment, or a few economic benefits that we provide to people who give\n    likes, over $5,000 has been tipped around the site to content creators. That's over 100,000 likes in our first 3\n    weeks of operation. </p>\n\n  <p>We're leading the rise of micro-entrepreneurs all around the world. With Belacam, anyone can earn a few extra\n    dollars a day by sharing their own original photos. It makes you wonder: why would you ever post your photos for\n    free on sites like Instagram again?</p>\n\n  <p>The best part about Belacam is that original content and photos are the ones that earn the most. There are no ads\n    or corporate-paid promotions because no one wants to give those tips. Instead, high-quality content rises to the\n    top, creating a cleaner, more refined social media site. It's the Belacam social media economy.</p>\n\n\n  <h3 class=\"left_align\">I'll see you on Belacam, <br><br>Tyler Marx</h3><h4>CEO of Belacam</h4>\n\n  <a class=\"submitbutton\" onClick=\"toTop()\">\n    Create an Account\n  </a>\n</div>\n"

/***/ }),

/***/ "./src/app/landing-page/landing-page.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LandingPageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LandingPageComponent = (function () {
    function LandingPageComponent(belaAPIService, router, authService) {
        this.belaAPIService = belaAPIService;
        this.router = router;
        this.authService = authService;
        this.signin_username = "";
        this.signin_password = "";
        this.signup_email = "";
        this.signup_username = "";
        this.signup_password = "";
        this.alert_message = "";
        var userName = jQuery('input[name="requesting_user_username"]').val();
        if (userName) {
            this.router.navigate(['/feed']);
        }
    }
    LandingPageComponent.prototype.ngOnInit = function () {
    };
    LandingPageComponent.prototype.ngOnDestroy = function () {
    };
    LandingPageComponent.prototype.ngAfterViewInit = function () {
        jQuery(".signup_form").validate({
            rules: {
                signup_email: {
                    required: true,
                    email: true
                },
                signup_username: "required",
                signup_password: "required",
            }
        });
    };
    LandingPageComponent.prototype.showLogin = function () {
        jQuery("#login").toggleClass("show");
    };
    LandingPageComponent.prototype.signin = function (event) {
        var _this = this;
        event.stopPropagation();
        if (this.signin_username.trim() != "" && this.signin_password.trim() != "") {
            this.alert_message = "";
            this.belaAPIService.signin(this.signin_username.trim(), this.signin_password.trim()).then(function (res) {
                if (res["status"] == "success") {
                    jQuery('input[name="requesting_user_id"]').val(res["user_id"]);
                    jQuery('input[name="requesting_user_username"]').val(res["user_name"]);
                    jQuery('input[name="csrfmiddlewaretoken"]').val(res["csrf_token"]);
                    _this.authService.loadUserDetail();
                    _this.router.navigate(['/feed']);
                }
                else {
                    _this.alert_message = res["message"];
                    jQuery("html, body").animate({ scrollTop: 0 }, "slow");
                }
            });
        }
        else {
            alert("Username and Password is required!");
        }
    };
    LandingPageComponent.prototype.is_signup_form_valid = function () {
        return jQuery(".signup_form").valid();
    };
    LandingPageComponent.prototype.signup = function (captchaResponse) {
        var _this = this;
        grecaptcha.reset();
        if (jQuery(".signup_form").valid()) {
            this.alert_message = "";
            this.belaAPIService.signup(this.signup_email.trim(), this.signup_username.trim(), this.signup_password.trim(), captchaResponse).then(function (res) {
                if (res["status"] == "success") {
                    jQuery('input[name="requesting_user_id"]').val(res["user_id"]);
                    jQuery('input[name="requesting_user_username"]').val(res["user_name"]);
                    jQuery('input[name="csrfmiddlewaretoken"]').val(res["csrf_token"]);
                    _this.authService.loadUserDetail();
                    _this.router.navigate(['/feed']);
                }
                else {
                    _this.alert_message = res["message"];
                    jQuery("html, body").animate({ scrollTop: 0 }, "slow");
                }
            });
        }
    };
    return LandingPageComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('captchaRef'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], LandingPageComponent.prototype, "captchaRef", void 0);
LandingPageComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-landing-page',
        template: __webpack_require__("./src/app/landing-page/landing-page.component.html"),
        styles: [__webpack_require__("./src/app/landing-page/landing-page.component.css")],
        providers: []
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__core_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_auth_service__["a" /* AuthService */]) === "function" && _d || Object])
], LandingPageComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=landing-page.component.js.map

/***/ }),

/***/ "./src/app/leaderboards/leaderboards.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/leaderboards/leaderboards.component.html":
/***/ (function(module, exports) {

module.exports = "<h2 class=\"center\">Weekly Leaderboards</h2>\n<br>\n<br>\n\n\n<h4 class=\"center\">Leaderboard bonuses are recorded and paid out at the end of each week. </h4>\n    \n  <div class=\"leaderboard_bonuses_wrapper\">  \n<div class=\"row leaderboard_bonuses\">\n    <div class=\"col-xs-4 col-sm-4 bonus_amount\"><h3>100 <img src=\"https://s3.amazonaws.com/bellassets/assets/images/orangebela.png\"/></h3></div>\n    \n    \n    <div class=\"col-xs-8 col-sm-8 gold\">Gold Winners</div></div>\n    \n<div class=\"row leaderboard_bonuses\">\n    <div class=\"col-xs-4 col-sm-4 bonus_amount\"><h3>50 <img src=\"https://s3.amazonaws.com/bellassets/assets/images/orangebela.png\"/></h3></div>\n    \n    \n    <div class=\"col-xs-8 col-sm-8 silver\">Silver Winners</div></div>\n    \n<div class=\"row leaderboard_bonuses\">\n    <div class=\"col-xs-4 col-sm-4 bonus_amount\"><h3>25 <img src=\"https://s3.amazonaws.com/bellassets/assets/images/orangebela.png\"/></h3></div>\n    \n    \n    <div class=\"col-xs-8 col-sm-8 bronze\">Bronze Winners</div></div>\n</div>\n<br>\n<br>\n\n\n<div class=\"white-box leaderboard-white-box\">\n\n    \n    \n    <h3 class=\"center\">Most Likes Given</h3>\n<h4 class=\"center no-border\">August 20th - August 26th</h4>\n<ul class=\"leaderboard\">\n<li class=\"header-li\">User <span class=\"pull-right\">Likes Given</span></li>\n<li class=\"gold\">\n1) <a href=\"/b/user/tyler\">Tyler</a> <span class=\"pull-right\">1,684</span>\n</li>\n    \n<li class=\"gold\">\n2) Tanner <span class=\"pull-right\">1,511</span>\n</li>\n    \n<li class=\"silver\">\n3) William_ikiabo <span class=\"pull-right\">1,005</span>\n</li>\n    \n<li class=\"silver\">\n4) luktov7777 <span class=\"pull-right\">850</span>\n</li>\n\n<li class=\"silver\">\n5) 2icken <span class=\"pull-right\">786</span>\n</li>\n    \n<li class=\"bronze\">\n6) Vinateee <span class=\"pull-right\">758</span>\n</li>\n    \n<li class=\"bronze\">\n7) quikflip757 <span class=\"pull-right\">605</span>\n</li>\n\n<li class=\"bronze\">\n8) serrgl <span class=\"pull-right\">569</span>\n</li>\n    \n<li class=\"bronze\">\n9) Gdigitax <span class=\"pull-right\">564</span>\n</li>\n    \n<li class=\"bronze\">\n10) greent <span class=\"pull-right\">564</span>\n</li>\n\n</ul>\n</div>\n\n<div class=\"white-box leaderboard-white-box\">\n<h3 class=\"center\">Most Likes Given to Different People</h3>\n<h4 class=\"center no-border\">August 20th - August 26th</h4>\n<ul class=\"leaderboard\">\n<li class=\"header-li\">User <span class=\"pull-right\">People Given Likes</span></li>\n<li class=\"gold\">\n1) <a href=\"/b/user/tyler\">Tyler</a> <span class=\"pull-right\">568</span>\n</li>\n    \n<li class=\"gold\">\n2) Tanner <span class=\"pull-right\">548</span>\n</li>\n    \n<li class=\"silver\">\n3) William_ikiabo <span class=\"pull-right\">384</span>\n</li>\n    \n<li class=\"silver\">\n4) luktov7777 <span class=\"pull-right\">379</span>\n</li>\n\n<li class=\"silver\">\n5) 2icken <span class=\"pull-right\">362</span>\n</li>\n    \n<li class=\"bronze\">\n6) Vinateee <span class=\"pull-right\">320</span>\n</li>\n    \n<li class=\"bronze\">\n7) quikflip757 <span class=\"pull-right\">301</span>\n</li>\n\n<li class=\"bronze\">\n8) serrgl <span class=\"pull-right\">296</span>\n</li>\n    \n<li class=\"bronze\">\n9) Gdigitax <span class=\"pull-right\">292</span>\n</li>\n    \n<li class=\"bronze\">\n10) greent <span class=\"pull-right\">279</span>\n</li>\n\n</ul>\n</div>"

/***/ }),

/***/ "./src/app/leaderboards/leaderboards.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeaderboardsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LeaderboardsComponent = (function () {
    function LeaderboardsComponent() {
    }
    LeaderboardsComponent.prototype.ngOnInit = function () {
    };
    return LeaderboardsComponent;
}());
LeaderboardsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-leaderboards',
        template: __webpack_require__("./src/app/leaderboards/leaderboards.component.html"),
        styles: [__webpack_require__("./src/app/leaderboards/leaderboards.component.css")]
    }),
    __metadata("design:paramtypes", [])
], LeaderboardsComponent);

//# sourceMappingURL=leaderboards.component.js.map

/***/ }),

/***/ "./src/app/lightbox/album-lightbox.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"lb-outerContainer transition\" #outerContainer>\n  <div class=\"lb-container\" #container>\n    <img class=\"lb-image\" [src]=\"album[currentImageIndex].src\" class=\"lb-image animation fadeIn\" [hidden]=\"ui.showReloader\" #image>\n    <div class=\"lb-nav\" [hidden]=\"!ui.showArrowNav\" #navArrow>\n      <a class=\"lb-prev\" [hidden]=\"!ui.showLeftArrow\" (click)=\"prevImage()\" #leftArrow></a>\n      <a class=\"lb-next\"[hidden]=\"!ui.showRightArrow\" (click)=\"nextImage()\" #rightArrow></a>\n    </div>\n    <div class=\"lb-loader\" [hidden]=\"!ui.showReloader\" (click)=\"close($event)\">\n      <a class=\"lb-cancel\"></a>\n    </div>\n  </div>\n</div>\n<div class=\"lb-dataContainer\" [hidden]=\"ui.showReloader\" #dataContainer>\n  <div class=\"lb-data\">\n    <div class=\"lb-details\">\n      <span class=\"lb-caption animation fadeIn\" [hidden]=\"!ui.showCaption\" #caption>{{ album[currentImageIndex].caption }}</span>\n      <span class=\"lb-number animation fadeIn\" [hidden]=\"!ui.showPageNumber\" #number>{{ content.pageNumber }}</span>\n    </div>\n    <div class=\"lb-closeContainer\">\n      <a class=\"lb-close\" (click)=\"close($event)\"></a>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/lightbox/album-lightbox.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlbumLightboxComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__ = __webpack_require__("./src/app/lightbox/lightbox-event.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var AlbumLightboxComponent = (function () {
    function AlbumLightboxComponent(_elemRef, _rendererRef, _lightboxEvent, _lightboxElem, _lightboxWindowRef, _documentRef) {
        var _this = this;
        this._elemRef = _elemRef;
        this._rendererRef = _rendererRef;
        this._lightboxEvent = _lightboxEvent;
        this._lightboxElem = _lightboxElem;
        this._lightboxWindowRef = _lightboxWindowRef;
        this._documentRef = _documentRef;
        // initialize data
        this.options = this.options || {};
        this.album = this.album || [];
        this.currentImageIndex = this.currentImageIndex || null;
        this._windowRef = this._lightboxWindowRef.nativeWindow;
        // control the interactive of the directive
        this.ui = {
            // control the appear of the reloader
            // false: image has loaded completely and ready to be shown
            // true: image is still loading
            showReloader: true,
            // control the appear of the nav arrow
            // the arrowNav is the parent of both left and right arrow
            // in some cases, the parent shows but the child does not show
            showLeftArrow: false,
            showRightArrow: false,
            showArrowNav: false,
            // control whether to show the
            // page number or not
            showPageNumber: false,
            showCaption: false,
            classList: 'lightbox animation fadeIn album-lightbox'
        };
        this.content = {
            pageNumber: ''
        };
        this._event = {};
        this._lightboxElem = this._elemRef;
        this._event.subscription = this._lightboxEvent.lightboxEvent$
            .subscribe(function (event) { return _this._onReceivedEvent(event); });
    }
    AlbumLightboxComponent.prototype.ngAfterViewInit = function () {
        // need to init css value here, after the view ready
        // actually these values are always 0
        this._cssValue = {
            containerTopPadding: Math.round(this._getCssStyleValue(this._containerElem, 'padding-top')),
            containerRightPadding: Math.round(this._getCssStyleValue(this._containerElem, 'padding-right')),
            containerBottomPadding: Math.round(this._getCssStyleValue(this._containerElem, 'padding-bottom')),
            containerLeftPadding: Math.round(this._getCssStyleValue(this._containerElem, 'padding-left')),
            imageBorderWidthTop: Math.round(this._getCssStyleValue(this._imageElem, 'border-top-width')),
            imageBorderWidthBottom: Math.round(this._getCssStyleValue(this._imageElem, 'border-bottom-width')),
            imageBorderWidthLeft: Math.round(this._getCssStyleValue(this._imageElem, 'border-left-width')),
            imageBorderWidthRight: Math.round(this._getCssStyleValue(this._imageElem, 'border-right-width'))
        };
        if (this._validateInputData()) {
            this._prepareComponent();
            this._registerImageLoadingEvent();
        }
    };
    AlbumLightboxComponent.prototype.ngOnDestroy = function () {
        if (!this.options.disableKeyboardNav) {
            // unbind keyboard event
            this._disableKeyboardNav();
        }
        this._event.subscription.unsubscribe();
    };
    AlbumLightboxComponent.prototype.close = function ($event) {
        $event.stopPropagation();
        if ($event.target.classList.contains('lightbox') ||
            $event.target.classList.contains('lb-loader') ||
            $event.target.classList.contains('lb-close')) {
            this._lightboxEvent.broadcastLightboxEvent({ id: __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["a" /* LIGHTBOX_EVENT */].CLOSE, data: null });
        }
    };
    AlbumLightboxComponent.prototype.nextImage = function () {
        if (this.album.length === 1) {
            return;
        }
        else if (this.currentImageIndex === this.album.length - 1) {
            this._changeImage(0);
        }
        else {
            this._changeImage(this.currentImageIndex + 1);
        }
    };
    AlbumLightboxComponent.prototype.prevImage = function () {
        if (this.album.length === 1) {
            return;
        }
        else if (this.currentImageIndex === 0 && this.album.length > 1) {
            this._changeImage(this.album.length - 1);
        }
        else {
            this._changeImage(this.currentImageIndex - 1);
        }
    };
    AlbumLightboxComponent.prototype._validateInputData = function () {
        if (this.album &&
            this.album instanceof Array &&
            this.album.length > 0) {
            for (var i = 0; i < this.album.length; i++) {
                // check whether each _nside
                // album has src data or not
                if (this.album[i].src) {
                    continue;
                }
                throw new Error('One of the album data does not have source data');
            }
        }
        else {
            throw new Error('No album data or album data is not correct in type');
        }
        // to prevent data understand as string
        // convert it to number
        if (isNaN(this.currentImageIndex)) {
            throw new Error('Current image index is not a number');
        }
        else {
            this.currentImageIndex = Number(this.currentImageIndex);
        }
        return true;
    };
    AlbumLightboxComponent.prototype._registerImageLoadingEvent = function () {
        var _this = this;
        var preloader = new Image();
        preloader.onload = function () {
            _this._onLoadImageSuccess();
        };
        preloader.src = this.album[this.currentImageIndex].src;
    };
    /**
     * Fire when the image is loaded
     */
    AlbumLightboxComponent.prototype._onLoadImageSuccess = function () {
        if (!this.options.disableKeyboardNav) {
            // unbind keyboard event during transition
            this._disableKeyboardNav();
        }
        var imageHeight;
        var imageWidth;
        var maxImageHeight;
        var maxImageWidth;
        var windowHeight;
        var windowWidth;
        var naturalImageWidth;
        var naturalImageHeight;
        // set default width and height of image to be its natural
        imageWidth = naturalImageWidth = this._imageElem.nativeElement.naturalWidth;
        imageHeight = naturalImageHeight = this._imageElem.nativeElement.naturalHeight;
        if (this.options.fitImageInViewPort) {
            windowWidth = this._windowRef.innerWidth;
            windowHeight = this._windowRef.innerHeight;
            maxImageWidth = windowWidth - this._cssValue.containerLeftPadding -
                this._cssValue.containerRightPadding - this._cssValue.imageBorderWidthLeft -
                this._cssValue.imageBorderWidthRight - 20;
            maxImageHeight = windowHeight - this._cssValue.containerTopPadding -
                this._cssValue.containerTopPadding - this._cssValue.imageBorderWidthTop -
                this._cssValue.imageBorderWidthBottom - 120;
            // if (naturalImageWidth > maxImageWidth || naturalImageHeight > maxImageHeight) {
            if ((naturalImageWidth / maxImageWidth) > (naturalImageHeight / maxImageHeight)) {
                imageWidth = maxImageWidth;
                imageHeight = Math.round(naturalImageHeight / (naturalImageWidth / imageWidth));
            }
            else {
                imageHeight = maxImageHeight;
                imageWidth = Math.round(naturalImageWidth / (naturalImageHeight / imageHeight));
            }
            // }
            this._rendererRef.setElementStyle(this._imageElem.nativeElement, 'width', imageWidth + "px");
            this._rendererRef.setElementStyle(this._imageElem.nativeElement, 'height', imageHeight + "px");
        }
        else if (this.options.fullScreen) {
            windowWidth = this._windowRef.innerWidth;
            windowHeight = this._windowRef.innerHeight;
            maxImageWidth = windowWidth - this._cssValue.containerLeftPadding -
                this._cssValue.containerRightPadding - this._cssValue.imageBorderWidthLeft -
                this._cssValue.imageBorderWidthRight;
            maxImageHeight = windowHeight - this._cssValue.containerTopPadding -
                this._cssValue.containerTopPadding - this._cssValue.imageBorderWidthTop -
                this._cssValue.imageBorderWidthBottom;
            // if (naturalImageWidth > maxImageWidth || naturalImageHeight > maxImageHeight) {
            if ((naturalImageWidth / maxImageWidth) > (naturalImageHeight / maxImageHeight)) {
                imageWidth = maxImageWidth;
                imageHeight = Math.round(naturalImageHeight / (naturalImageWidth / imageWidth));
                var top_position = (windowHeight - imageHeight) / 2;
                this._rendererRef.setElementStyle(this._lightboxElem.nativeElement, 'top', top_position + "px");
            }
            else {
                imageHeight = maxImageHeight;
                imageWidth = Math.round(naturalImageWidth / (naturalImageHeight / imageHeight));
            }
            // }
            this._rendererRef.setElementStyle(this._imageElem.nativeElement, 'width', imageWidth + "px");
            this._rendererRef.setElementStyle(this._imageElem.nativeElement, 'height', imageHeight + "px");
        }
        this._sizeContainer(imageWidth, imageHeight);
    };
    AlbumLightboxComponent.prototype._sizeContainer = function (imageWidth, imageHeight) {
        var oldWidth = this._outerContainerElem.nativeElement.offsetWidth;
        var oldHeight = this._outerContainerElem.nativeElement.offsetHeight;
        var newWidth = imageWidth + this._cssValue.containerRightPadding + this._cssValue.containerLeftPadding +
            this._cssValue.imageBorderWidthLeft + this._cssValue.imageBorderWidthRight;
        var newHeight = imageHeight + this._cssValue.containerTopPadding + this._cssValue.containerBottomPadding +
            this._cssValue.imageBorderWidthTop + this._cssValue.imageBorderWidthBottom;
        if (oldWidth !== newWidth || oldHeight !== newHeight) {
            this._rendererRef.setElementStyle(this._outerContainerElem.nativeElement, 'width', newWidth + "px");
            this._rendererRef.setElementStyle(this._outerContainerElem.nativeElement, 'height', newHeight + "px");
            // bind resize event to outer container
            // this._event.transitions = [];
            // ['transitionend', 'webkitTransitionEnd', 'oTransitionEnd', 'MSTransitionEnd'].forEach(eventName => {
            //   this._event.transitions.push(
            //     this._rendererRef.listen(this._outerContainerElem.nativeElement, eventName, (event: any) => {
            //       if (event.target === event.currentTarget) {
            //         this._postResize(newWidth, newHeight);
            //       }
            //     })
            //   );
            // });
            this._postResize(newWidth, newHeight);
        }
        else {
            this._postResize(newWidth, newHeight);
        }
    };
    AlbumLightboxComponent.prototype._postResize = function (newWidth, newHeight) {
        // unbind resize event
        if (Array.isArray(this._event.transitions)) {
            this._event.transitions.forEach(function (eventHandler) {
                eventHandler();
            });
            this._event.transitions = [];
        }
        this._rendererRef.setElementStyle(this._dataContainerElem.nativeElement, 'width', newWidth + "px");
        this._showImage();
    };
    AlbumLightboxComponent.prototype._showImage = function () {
        this.ui.showReloader = false;
        this._updateNav();
        this._updateDetails();
        if (!this.options.disableKeyboardNav) {
            this._enableKeyboardNav();
        }
    };
    AlbumLightboxComponent.prototype._prepareComponent = function () {
        this._addOptionCss();
        // add css3 animation
        this._addCssAnimation();
        // position the image according to user's option
        this._positionLightBox();
    };
    AlbumLightboxComponent.prototype._addOptionCss = function () {
        if (this.options.circleImage) {
            this._rendererRef.setElementStyle(this._outerContainerElem.nativeElement, 'border-radius', '50%');
            this._rendererRef.setElementStyle(this._outerContainerElem.nativeElement, 'overflow', 'hidden');
            this._rendererRef.setElementStyle(this._imageElem.nativeElement, 'border-radius', '50%');
        }
    };
    AlbumLightboxComponent.prototype._positionLightBox = function () {
        // @see https://stackoverflow.com/questions/3464876/javascript-get-window-x-y-position-for-scroll
        var top = (this._windowRef.pageYOffset || this._documentRef.documentElement.scrollTop) +
            this.options.positionFromTop;
        var left = this._windowRef.pageXOffset || this._documentRef.documentElement.scrollLeft;
        this._rendererRef.setElementStyle(this._lightboxElem.nativeElement, 'top', top + "px");
        this._rendererRef.setElementStyle(this._lightboxElem.nativeElement, 'left', left + "px");
        this._rendererRef.setElementStyle(this._lightboxElem.nativeElement, 'display', 'block');
        // disable scrolling of the page while open
        if (this.options.disableScrolling) {
            this._rendererRef.setElementClass(this._documentRef.body, 'lb-disable-scrolling', true);
        }
    };
    /**
     * addCssAnimation add css3 classes for animate lightbox
     */
    AlbumLightboxComponent.prototype._addCssAnimation = function () {
        var resizeDuration = this.options.resizeDuration;
        var fadeDuration = this.options.fadeDuration;
        this._rendererRef.setElementStyle(this._lightboxElem.nativeElement, '-webkit-animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._lightboxElem.nativeElement, 'animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._outerContainerElem.nativeElement, '-webkit-transition-duration', resizeDuration + "s");
        this._rendererRef.setElementStyle(this._outerContainerElem.nativeElement, 'transition-duration', resizeDuration + "s");
        this._rendererRef.setElementStyle(this._dataContainerElem.nativeElement, '-webkit-animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._dataContainerElem.nativeElement, 'animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._imageElem.nativeElement, '-webkit-animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._imageElem.nativeElement, 'animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._captionElem.nativeElement, '-webkit-animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._captionElem.nativeElement, 'animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._numberElem.nativeElement, '-webkit-animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._numberElem.nativeElement, 'animation-duration', fadeDuration + "s");
    };
    AlbumLightboxComponent.prototype._end = function () {
        var _this = this;
        this.ui.classList = 'lightbox animation fadeOut album-lightbox';
        if (this.options.disableScrolling) {
            this._rendererRef.setElementClass(this._documentRef.body, 'lb-disable-scrolling', false);
        }
        setTimeout(function () {
            _this.cmpRef.destroy();
        }, this.options.fadeDuration * 1000);
    };
    AlbumLightboxComponent.prototype._updateDetails = function () {
        // update the caption
        if (typeof this.album[this.currentImageIndex].caption !== 'undefined' &&
            this.album[this.currentImageIndex].caption !== '') {
            this.ui.showCaption = true;
        }
        // update the page number if user choose to do so
        // does not perform numbering the page if the
        // array length in album <= 1
        if (this.album.length > 1 && this.options.showImageNumberLabel) {
            this.ui.showPageNumber = true;
            this.content.pageNumber = this._albumLabel();
        }
    };
    AlbumLightboxComponent.prototype._albumLabel = function () {
        // due to {this.currentImageIndex} is set from 0 to {this.album.length} - 1
        return "Image " + Number(this.currentImageIndex + 1) + " of " + this.album.length;
    };
    AlbumLightboxComponent.prototype._changeImage = function (newIndex) {
        this.currentImageIndex = newIndex;
        this._hideImage();
        this._registerImageLoadingEvent();
        this._lightboxEvent.broadcastLightboxEvent({ id: __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["a" /* LIGHTBOX_EVENT */].CHANGE_PAGE, data: newIndex });
    };
    AlbumLightboxComponent.prototype._hideImage = function () {
        this.ui.showReloader = true;
        this.ui.showArrowNav = false;
        this.ui.showLeftArrow = false;
        this.ui.showRightArrow = false;
        this.ui.showPageNumber = false;
        this.ui.showCaption = false;
    };
    AlbumLightboxComponent.prototype._updateNav = function () {
        var alwaysShowNav = false;
        // check to see the browser support touch event
        try {
            this._documentRef.createEvent('TouchEvent');
            alwaysShowNav = !!(this.options.alwaysShowNavOnTouchDevices);
        }
        catch (e) {
            // noop
        }
        // initially show the arrow nav
        // which is the parent of both left and right nav
        this._showArrowNav();
        if (this.album.length > 1) {
            if (this.options.wrapAround) {
                if (alwaysShowNav) {
                    // alternatives this.$lightbox.find('.lb-prev, .lb-next').css('opacity', '1');
                    this._rendererRef.setElementStyle(this._leftArrowElem.nativeElement, 'opacity', '1');
                    this._rendererRef.setElementStyle(this._rightArrowElem.nativeElement, 'opacity', '1');
                }
                // alternatives this.$lightbox.find('.lb-prev, .lb-next').show();
                this._showLeftArrowNav();
                this._showRightArrowNav();
            }
            else {
                if (this.currentImageIndex > 0) {
                    // alternatives this.$lightbox.find('.lb-prev').show();
                    this._showLeftArrowNav();
                    if (alwaysShowNav) {
                        // alternatives this.$lightbox.find('.lb-prev').css('opacity', '1');
                        this._rendererRef.setElementStyle(this._leftArrowElem.nativeElement, 'opacity', '1');
                    }
                }
                if (this.currentImageIndex < this.album.length - 1) {
                    // alternatives this.$lightbox.find('.lb-next').show();
                    this._showRightArrowNav();
                    if (alwaysShowNav) {
                        // alternatives this.$lightbox.find('.lb-next').css('opacity', '1');
                        this._rendererRef.setElementStyle(this._rightArrowElem.nativeElement, 'opacity', '1');
                    }
                }
            }
        }
    };
    AlbumLightboxComponent.prototype._showLeftArrowNav = function () {
        this.ui.showLeftArrow = true;
    };
    AlbumLightboxComponent.prototype._showRightArrowNav = function () {
        this.ui.showRightArrow = true;
    };
    AlbumLightboxComponent.prototype._showArrowNav = function () {
        this.ui.showArrowNav = (this.album.length !== 1);
    };
    AlbumLightboxComponent.prototype._enableKeyboardNav = function () {
        var _this = this;
        this._event.keyup = this._rendererRef.listenGlobal('document', 'keyup', function (event) {
            _this._keyboardAction(event);
        });
    };
    AlbumLightboxComponent.prototype._disableKeyboardNav = function () {
        if (this._event.keyup) {
            this._event.keyup();
        }
    };
    AlbumLightboxComponent.prototype._keyboardAction = function ($event) {
        var KEYCODE_ESC = 27;
        var KEYCODE_LEFTARROW = 37;
        var KEYCODE_RIGHTARROW = 39;
        var keycode = $event.keyCode;
        var key = String.fromCharCode(keycode).toLowerCase();
        if (keycode === KEYCODE_ESC || key.match(/x|o|c/)) {
            this._lightboxEvent.broadcastLightboxEvent({ id: __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["a" /* LIGHTBOX_EVENT */].CLOSE, data: null });
        }
        else if (key === 'p' || keycode === KEYCODE_LEFTARROW) {
            if (this.currentImageIndex !== 0) {
                this._changeImage(this.currentImageIndex - 1);
            }
            else if (this.options.wrapAround && this.album.length > 1) {
                this._changeImage(this.album.length - 1);
            }
        }
        else if (key === 'n' || keycode === KEYCODE_RIGHTARROW) {
            if (this.currentImageIndex !== this.album.length - 1) {
                this._changeImage(this.currentImageIndex + 1);
            }
            else if (this.options.wrapAround && this.album.length > 1) {
                this._changeImage(0);
            }
        }
    };
    AlbumLightboxComponent.prototype._getCssStyleValue = function (elem, propertyName) {
        return parseFloat(this._windowRef
            .getComputedStyle(elem.nativeElement, null)
            .getPropertyValue(propertyName));
    };
    AlbumLightboxComponent.prototype._onReceivedEvent = function (event) {
        switch (event.id) {
            case __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["a" /* LIGHTBOX_EVENT */].CLOSE:
                this._end();
                break;
            default:
                break;
        }
    };
    return AlbumLightboxComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], AlbumLightboxComponent.prototype, "album", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Number)
], AlbumLightboxComponent.prototype, "currentImageIndex", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], AlbumLightboxComponent.prototype, "options", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], AlbumLightboxComponent.prototype, "cmpRef", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('outerContainer'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], AlbumLightboxComponent.prototype, "_outerContainerElem", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('container'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object)
], AlbumLightboxComponent.prototype, "_containerElem", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('leftArrow'),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _c || Object)
], AlbumLightboxComponent.prototype, "_leftArrowElem", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('rightArrow'),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _d || Object)
], AlbumLightboxComponent.prototype, "_rightArrowElem", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('navArrow'),
    __metadata("design:type", typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _e || Object)
], AlbumLightboxComponent.prototype, "_navArrowElem", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('dataContainer'),
    __metadata("design:type", typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _f || Object)
], AlbumLightboxComponent.prototype, "_dataContainerElem", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('image'),
    __metadata("design:type", typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _g || Object)
], AlbumLightboxComponent.prototype, "_imageElem", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('caption'),
    __metadata("design:type", typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _h || Object)
], AlbumLightboxComponent.prototype, "_captionElem", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('number'),
    __metadata("design:type", typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _j || Object)
], AlbumLightboxComponent.prototype, "_numberElem", void 0);
AlbumLightboxComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("./src/app/lightbox/album-lightbox.component.html"),
        selector: '[lb-album]',
        host: {
            '(click)': 'close($event)',
            '[class]': 'ui.classList'
        }
    }),
    __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["b" /* DOCUMENT */])),
    __metadata("design:paramtypes", [typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["b" /* LightboxEvent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["b" /* LightboxEvent */]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _o || Object, typeof (_p = typeof __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["c" /* LightboxWindowRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["c" /* LightboxWindowRef */]) === "function" && _p || Object, Object])
], AlbumLightboxComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p;
//# sourceMappingURL=album-lightbox.component.js.map

/***/ }),

/***/ "./src/app/lightbox/lightbox-config.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LightboxConfig; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LightboxConfig = (function () {
    function LightboxConfig() {
        this.fadeDuration = 0.7;
        this.resizeDuration = 0.5;
        this.fitImageInViewPort = true;
        this.positionFromTop = 0;
        this.positionFromBottom = 0;
        this.showImageNumberLabel = false;
        this.alwaysShowNavOnTouchDevices = false;
        this.wrapAround = false;
        this.disableKeyboardNav = false;
        this.allowRequestAlbum = false;
        this.requestID = null;
    }
    return LightboxConfig;
}());
LightboxConfig = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], LightboxConfig);

//# sourceMappingURL=lightbox-config.service.js.map

/***/ }),

/***/ "./src/app/lightbox/lightbox-event.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LIGHTBOX_EVENT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return LightboxEvent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return LightboxWindowRef; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__("./node_modules/rxjs/_esm5/Subject.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LIGHTBOX_EVENT;
(function (LIGHTBOX_EVENT) {
    LIGHTBOX_EVENT[LIGHTBOX_EVENT["CHANGE_PAGE"] = 1] = "CHANGE_PAGE";
    LIGHTBOX_EVENT[LIGHTBOX_EVENT["CLOSE"] = 2] = "CLOSE";
    LIGHTBOX_EVENT[LIGHTBOX_EVENT["OPEN"] = 3] = "OPEN";
})(LIGHTBOX_EVENT || (LIGHTBOX_EVENT = {}));
;
var LightboxEvent = (function () {
    function LightboxEvent() {
        this._lightboxEventSource = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["b" /* Subject */]();
        this.lightboxEvent$ = this._lightboxEventSource.asObservable();
    }
    LightboxEvent.prototype.broadcastLightboxEvent = function (event) {
        this._lightboxEventSource.next(event);
    };
    return LightboxEvent;
}());
LightboxEvent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], LightboxEvent);

function getWindow() {
    return window;
}
var LightboxWindowRef = (function () {
    function LightboxWindowRef() {
    }
    Object.defineProperty(LightboxWindowRef.prototype, "nativeWindow", {
        get: function () {
            return getWindow();
        },
        enumerable: true,
        configurable: true
    });
    return LightboxWindowRef;
}());
LightboxWindowRef = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], LightboxWindowRef);

//# sourceMappingURL=lightbox-event.service.js.map

/***/ }),

/***/ "./src/app/lightbox/lightbox-overlay.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LightboxOverlayComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__ = __webpack_require__("./src/app/lightbox/lightbox-event.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var LightboxOverlayComponent = (function () {
    function LightboxOverlayComponent(_elemRef, _rendererRef, _lightboxEvent, _documentRef) {
        var _this = this;
        this._elemRef = _elemRef;
        this._rendererRef = _rendererRef;
        this._lightboxEvent = _lightboxEvent;
        this._documentRef = _documentRef;
        this._classList = 'lightboxOverlay animation fadeInOverlay';
        this._subscription = this._lightboxEvent.lightboxEvent$.subscribe(function (event) { return _this._onReceivedEvent(event); });
    }
    LightboxOverlayComponent.prototype.ngAfterViewInit = function () {
        var fadeDuration = this.options.fadeDuration;
        this._rendererRef.setElementStyle(this._elemRef.nativeElement, '-webkit-animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._elemRef.nativeElement, '-animation-duration', fadeDuration + "s");
        this._sizeOverlay();
    };
    LightboxOverlayComponent.prototype.close = function () {
        // broadcast to itself and all others subscriber including the components
        this._lightboxEvent.broadcastLightboxEvent({ id: __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["a" /* LIGHTBOX_EVENT */].CLOSE, data: null });
    };
    LightboxOverlayComponent.prototype.onResize = function () {
        this._sizeOverlay();
    };
    LightboxOverlayComponent.prototype.ngOnDestroy = function () {
        this._subscription.unsubscribe();
    };
    LightboxOverlayComponent.prototype._sizeOverlay = function () {
        var width = this._getOverlayWidth();
        var height = this._getOverlayHeight();
        this._rendererRef.setElementStyle(this._elemRef.nativeElement, 'width', width + "px");
        this._rendererRef.setElementStyle(this._elemRef.nativeElement, 'height', height + "px");
    };
    LightboxOverlayComponent.prototype._onReceivedEvent = function (event) {
        switch (event.id) {
            case __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["a" /* LIGHTBOX_EVENT */].CLOSE:
                this._end();
                break;
            default:
                break;
        }
    };
    LightboxOverlayComponent.prototype._end = function () {
        // queue self destruction after the animation has finished
        // FIXME: not sure if there is any way better than this
        this.cmpRef.destroy();
        this._classList = 'lightboxOverlay animation fadeOutOverlay';
    };
    LightboxOverlayComponent.prototype._getOverlayWidth = function () {
        return Math.max(this._documentRef.body.scrollWidth, this._documentRef.body.offsetWidth, this._documentRef.documentElement.clientWidth, this._documentRef.documentElement.scrollWidth, this._documentRef.documentElement.offsetWidth);
    };
    LightboxOverlayComponent.prototype._getOverlayHeight = function () {
        return Math.max(this._documentRef.body.scrollHeight, this._documentRef.body.offsetHeight, this._documentRef.documentElement.clientHeight, this._documentRef.documentElement.scrollHeight, this._documentRef.documentElement.offsetHeight);
    };
    return LightboxOverlayComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], LightboxOverlayComponent.prototype, "options", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], LightboxOverlayComponent.prototype, "cmpRef", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('window:resize'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], LightboxOverlayComponent.prototype, "onResize", null);
LightboxOverlayComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: '[lb-overlay]',
        template: '',
        host: {
            '(click)': 'close()',
            '[class]': '_classList'
        }
    }),
    __param(3, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["b" /* DOCUMENT */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["b" /* LightboxEvent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["b" /* LightboxEvent */]) === "function" && _c || Object, Object])
], LightboxOverlayComponent);

var _a, _b, _c;
//# sourceMappingURL=lightbox-overlay.component.js.map

/***/ }),

/***/ "./src/app/lightbox/lightbox.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"post-lightbox\" [hidden]=\"curPageID != 0\">\n  <div class=\"lb-outerDiv\" #outerDiv>\n    <div *ngIf=\"album.length > 0\" class=\"lb-outerContainer transition\" style=\"height: auto\" #outerContainer>\n\n      <div class=\"lb-container\" #container>\n        <div class=\"white-box\">\n\n\n\n          <div class=\"lb-image-wrapper\">\n            <app-insufficient-bela #insufficientMessage [user]=\"requesting_user\" *ngIf=\"requesting_user\">\n            </app-insufficient-bela>\n\n            <img class=\"lb-image animation fadeIn\" [src]=\"album[currentImageIndex].src\" [hidden]=\"ui.showReloader\"\n                 #image/>\n            <div class=\"lb-nav\" [hidden]=\"!ui.showArrowNav\" #navArrow>\n              <a class=\"lb-prev\" [hidden]=\"!ui.showLeftArrow\" (click)=\"prevImage()\" #leftArrow></a>\n              <a class=\"lb-next\" [hidden]=\"!ui.showRightArrow\" (click)=\"nextImage()\" #rightArrow></a>\n            </div>\n            <div class=\"lb-loader\" [hidden]=\"!ui.showReloader\" (click)=\"close($event)\">\n              <a class=\"lb-cancel\"></a>\n            </div>\n          </div>\n          <div class=\"user-post\">\n\n            <div class=\"user-post-left\">\n              <a [routerLink]=\"['/user/', currentThot.user.username]\"\n                 alt=\"Belacam User\"\n                 class=\"circlephoto user-avatar\"\n                 (click)=\"handleAvatarClick($event)\">\n                <span class=\"avatar-wrapper\" [innerHTML]=\"currentThot.user.avatar\"></span>\n              </a>\n\n\n              <h4 class=\"post-user-info\">\n                <!-- Links username to user's profile page -->\n                <a [routerLink] = \"['/user/', currentThot.user.username]\" alt=\"Belacam User\" (click)=\"handleAvatarClick($event)\">\n                  {{ currentThot.user.username }}\n                </a>\n                <span class=\"postlocation\" *ngIf=\"currentThot.location && !currentThot.editableLocation\">\n                  <i class=\"fas fa-map-marker-alt\"></i> {{currentThot.location}}\n                </span>\n                <span class=\"posttime\">{{ currentThot.created_at|messageTime }} </span>\n              </h4>\n            </div>\n\n\n            <div class=\"user-post-right\">\n              <div class=\"dropdown\">\n                <a class=\"dropdown-toggle image-btn\" id=\"dropdownMenuLink3\" href=\"#\" role=\"button\" data-toggle=\"dropdown\"\n                   aria-haspopup=\"true\" aria-expanded=\"false\">\n                  <img src=\"https://s3.amazonaws.com/bellassets/assets/images/doted-icon.png\"/>\n                </a>\n                <div class=\"dropdown-menu deletereport-menu\" aria-labelledby=\"dropdownMenuLink3\">\n                  <div class=\"dropdown-caret\">\n                    <div class=\"caret-outer\"></div>\n                    <div class=\"caret-inner\"></div>\n                  </div>\n                  <a class=\"\" href=\"#\" *ngIf=\"requesting_user && requesting_user.id && currentThot.user.id != requesting_user.id\"\n                     (click)=\"follow(currentThot.user);false\" translate>\n                    Follow\n                  </a>\n\n                  <a class=\"dropdown-item\" target=\"_blank\"\n                     [attr.href]=\"['https://twitter.com/home?status=https://www.belacam.com/n/post/' +  currentThot.user.username +'/' + currentThot.id]\"\n                     translate>Share on Twitter</a>\n\n                  <ng-container *ngIf=\"requesting_user\">\n                    <a class=\"dropdown-item\"\n                       href=\"#\"\n                       *ngIf=\"requesting_user.id && (currentThot.user.id==requesting_user.id)\"\n                       (click)=\"editable(true);false\" translate>Edit</a>\n\n                    <a class=\"dropdown-item\"\n                       href=\"#\"\n                       *ngIf=\"requesting_user.id && (currentThot.user.id==requesting_user.id)\"\n                       (click)=\"delete();false\" translate>Delete</a>\n\n                    <a class=\"dropdown-item\"\n                       href=\"#\"\n                       *ngIf=\"requesting_user.is_superuser\"\n                       (click)=\"deleteAdmin();false\" translate>Admin Delete</a>\n\n                    <a class=\"dropdown-item\"\n                       href=\"#\"\n                       *ngIf=\"requesting_user.id && (currentThot.user.id!=requesting_user.id)\"\n                       (click)=\"report();false\" translate>Report</a>\n\n                    <a class=\"dropdown-item\"\n                       href=\"#\"\n                       *ngIf=\"requesting_user.id && (currentThot.user.id!=requesting_user.id)\"\n                       (click)=\"block_user();false\" translate>Block User</a>\n                  </ng-container>\n\n                </div>\n              </div>\n            </div>\n\n\n\n            <p class=\"lightboxcaption\" *ngIf=\"!currentThot.editable\">\n              <app-dynamic-html [innerHTML]=\"currentThot.content|tagify\"></app-dynamic-html>\n            </p>\n\n            <div *ngIf=\"currentThot.editable\" style=\"float: left; width: 100%; margin-bottom: 15px; margin-left: 5px; margin-right: 5px;\">\n              <textarea class=\"caption-edit-box\" [(ngModel)]=\"currentThot.tempContent\"></textarea>\n              <input class=\"savebutton\" (click)=\"update();false\" type=\"button\" value=\"Save\">\n              <input class=\"cancelbutton\" (click)=\"editable(false);false\" type=\"button\" value=\"Cancel\">\n            </div>\n\n            <ul class=\"like-box like-box-lightbox\">\n              <li class=\"heart\"\n                  [class.unlikedheart]=\"!currentThot.is_liked\"\n                  [class.likedheart]=\"currentThot.is_liked\" >\n                <a class=\"\" href=\"#\" (click)=\"like(currentThot);false\">\n\n                  <img class=\"grayheart\"\n                       src=\"https://s3.amazonaws.com/bellassets/assets/images/grayheart.png\"\n                       [alt]=\"'Give some Bela'|translate\" />\n\n                  <img class=\"redheart\"\n                       src=\"https://s3.amazonaws.com/bellassets/assets/images/redheart.png\"\n                       [alt]=\"'Give some Bela'|translate\" />\n\n\n                </a>\n                <a (click)=\"openLikes()\">\n                  {{ currentThot.total_likes_count }}\n                </a>\n\n\n                  <span class=\"viewscount\" >\n                  <i class=\"far fa-eye\"></i><b> {{currentThot.views.length}}</b>\n                </span>\n              </li>\n\n\n              <li class=\"postearnings\">\n                <img src=\"https://s3.amazonaws.com/bellassets/assets/images/orangebela.png\">\n                {{ album[currentImageIndex].thot.belas|number:'1.2-2' }}<br>\n                <i class=\"fas fa-dollar-sign\"></i>\n                {{ album[currentImageIndex].thot.earnings|number:'1.2-2' }}\n              </li>\n            </ul>\n\n            <div class=\"comment-boxes comment-box\">\n              <app-comment [thot]=\"currentThot\" [hideForm]=\"true\"></app-comment>\n            </div>\n\n            <div class=\"comment-boxes add-comment-box\">\n              <app-comment-form [thot]=\"currentThot\"\n                                (success)=\"addCommentToAlbumPost($event)\"></app-comment-form>\n            </div>\n\n          </div>\n\n        </div>\n      </div>\n\n      <div class=\"lb-loader\" [hidden]=\"!ui.showAlbumRequestLoader\">\n        <a class=\"lb-cancel\"></a>\n      </div>\n    </div>\n\n\n    <div *ngIf=\"album.length > 0\" class=\"lb-dataContainer\" [hidden]=\"ui.showReloader\" #dataContainer>\n      <div class=\"lb-data\">\n        <div class=\"lb-details\">\n                <span class=\"lb-caption animationBelacam Growth fadeIn\" [hidden]=\"true\"\n                      #caption>{{ currentThot.caption }}</span>\n          <span class=\"lb-number animation fadeIn\" [hidden]=\"true\" #number>{{ content.pageNumber }}</span>\n        </div>\n        <div class=\"lb-closeContainer\">\n          <a class=\"lb-close\" (click)=\"close($event)\"></a>\n        </div>\n      </div>\n    </div>\n  </div>\n\n</div>\n\n<div app-likes-modal [thot]=\"currentThot\" [user]=\"requesting_user\" *ngIf=\"curPageID == 1\" style=\"height: 0;\">\n</div>\n\n<div app-views-modal [thot]=\"currentThot\" [user]=\"requesting_user\" *ngIf=\"curPageID == 2\" style=\"height: 0;\">\n</div>\n\n"

/***/ }),

/***/ "./src/app/lightbox/lightbox.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LightboxComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__ = __webpack_require__("./src/app/lightbox/lightbox-event.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_event_service__ = __webpack_require__("./src/app/core/event.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__insufficient_bela_insufficient_bela_component__ = __webpack_require__("./src/app/insufficient-bela/insufficient-bela.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__core_event_types__ = __webpack_require__("./src/app/core/event.types.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};









var LightboxComponent = (function () {
    function LightboxComponent(_elemRef, _rendererRef, _lightboxEvent, _lightboxElem, _lightboxWindowRef, _documentRef, appService, eventService, authService) {
        this._elemRef = _elemRef;
        this._rendererRef = _rendererRef;
        this._lightboxEvent = _lightboxEvent;
        this._lightboxElem = _lightboxElem;
        this._lightboxWindowRef = _lightboxWindowRef;
        this._documentRef = _documentRef;
        this.appService = appService;
        this.eventService = eventService;
        this.authService = authService;
        this.curPageID = 0;
        this.csrf = "";
        this.requesting_user = new __WEBPACK_IMPORTED_MODULE_4__app_model__["h" /* User */]();
        // initialize data
        this.options = this.options || {};
        this.album = this.album || [];
        this.currentImageIndex = this.currentImageIndex || null;
        this._windowRef = this._lightboxWindowRef.nativeWindow;
        // control the interactive of the directive
        this.ui = {
            // control the appear of the reloader
            // false: image has loaded completely and ready to be shown
            // true: image is still loading
            showReloader: true,
            showAlbumRequestLoader: false,
            // control the appear of the nav arrow
            // the arrowNav is the parent of both left and right arrow
            // in some cases, the parent shows but the child does not show
            showLeftArrow: false,
            showRightArrow: false,
            showArrowNav: false,
            // control whether to show the
            // page number or not
            showPageNumber: false,
            showCaption: false,
            classList: 'lightbox animation fadeIn'
        };
        this.content = {
            pageNumber: ''
        };
        this._event = {
            subscriptions: []
        };
        this._lightboxElem = this._elemRef;
    }
    LightboxComponent.prototype.ngAfterViewInit = function () {
        // need to init css value here, after the view ready
        // actually these values are always 0
        this._cssValue = {
            containerTopPadding: Math.round(this._getCssStyleValue(this._containerElem, 'padding-top')),
            containerRightPadding: Math.round(this._getCssStyleValue(this._containerElem, 'padding-right')),
            containerBottomPadding: Math.round(this._getCssStyleValue(this._containerElem, 'padding-bottom')),
            containerLeftPadding: Math.round(this._getCssStyleValue(this._containerElem, 'padding-left')),
            imageBorderWidthTop: Math.round(this._getCssStyleValue(this._imageElem, 'border-top-width')),
            imageBorderWidthBottom: Math.round(this._getCssStyleValue(this._imageElem, 'border-bottom-width')),
            imageBorderWidthLeft: Math.round(this._getCssStyleValue(this._imageElem, 'border-left-width')),
            imageBorderWidthRight: Math.round(this._getCssStyleValue(this._imageElem, 'border-right-width'))
        };
        if (this._validateInputData()) {
            this._prepareComponent();
            this._registerImageLoadingEvent();
        }
        jQuery('#dropdownMenuLink3').dropdown();
    };
    LightboxComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
        this.requesting_user = JSON.parse(localStorage.getItem('user'));
        this._event.subscriptions.push(this._lightboxEvent.lightboxEvent$
            .subscribe(function (event) { return _this._onReceivedEvent(event); }));
        this._event.subscriptions.push(this.eventService.subscribe({
            next: function (event) {
                if (event.type === __WEBPACK_IMPORTED_MODULE_8__core_event_types__["b" /* BelaEventType */].LOADED_NEXT_ALBUM) {
                    var evValue = event.value;
                    _this.album = evValue.album;
                    _this.ui.showAlbumRequestLoader = false;
                    _this._changeImage(evValue.newImageIndex);
                }
                else if (event.type === __WEBPACK_IMPORTED_MODULE_8__core_event_types__["b" /* BelaEventType */].NO_NEXT_ALBUM) {
                    _this.ui.showAlbumRequestLoader = false;
                }
            }
        }));
    };
    LightboxComponent.prototype.ngOnDestroy = function () {
        if (!this.options.disableKeyboardNav) {
            // unbind keyboard event
            this._disableKeyboardNav();
        }
        for (var _i = 0, _a = this._event.subscriptions; _i < _a.length; _i++) {
            var subscription = _a[_i];
            subscription.unsubscribe();
        }
    };
    LightboxComponent.prototype.handleAvatarClick = function ($event) {
        $event.stopPropagation();
        this._lightboxEvent.broadcastLightboxEvent({ id: __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["a" /* LIGHTBOX_EVENT */].CLOSE, data: null });
    };
    LightboxComponent.prototype.close = function ($event) {
        $event.stopPropagation();
        if (($event.target.classList.contains('lightbox') && this.isHome()) ||
            $event.target.classList.contains('lb-loader') ||
            $event.target.classList.contains('lb-outerDiv') ||
            $event.target.classList.contains('lb-close')) {
            this._close();
        }
        else if (($event.target.classList.contains('lightbox') && !this.isHome())) {
            this.openHome();
        }
    };
    LightboxComponent.prototype.nextImage = function () {
        if (this.currentImageIndex !== this.album.length - 1) {
            this._changeImage(this.currentImageIndex + 1);
        }
        else if (this.options.wrapAround && this.album.length > 1) {
            this._changeImage(0);
        }
        else if (this.options.allowRequestAlbum && !this.ui.showAlbumRequestLoader) {
            var evValue = new __WEBPACK_IMPORTED_MODULE_8__core_event_types__["d" /* EvRequestAlbum */]();
            evValue.album = this.album;
            evValue.currentImageIndex = this.currentImageIndex;
            evValue.direction = __WEBPACK_IMPORTED_MODULE_8__core_event_types__["d" /* EvRequestAlbum */].DIRECTION_NEXT;
            evValue.requestID = this.options.requestID;
            this.eventService.fireEvent({
                type: __WEBPACK_IMPORTED_MODULE_8__core_event_types__["b" /* BelaEventType */].REQUEST_NEXT_ALBUM,
                value: evValue
            });
            this.ui.showAlbumRequestLoader = true;
        }
    };
    LightboxComponent.prototype.prevImage = function () {
        if (this.currentImageIndex !== 0) {
            this._changeImage(this.currentImageIndex - 1);
        }
        else if (this.options.wrapAround && this.album.length > 1) {
            this._changeImage(this.album.length - 1);
        }
        else if (this.options.allowRequestAlbum && !this.ui.showAlbumRequestLoader) {
            var evValue = new __WEBPACK_IMPORTED_MODULE_8__core_event_types__["d" /* EvRequestAlbum */]();
            evValue.album = this.album;
            evValue.currentImageIndex = this.currentImageIndex;
            evValue.direction = __WEBPACK_IMPORTED_MODULE_8__core_event_types__["d" /* EvRequestAlbum */].DIRECTION_PREV;
            evValue.requestID = this.options.requestID;
            this.eventService.fireEvent({
                type: __WEBPACK_IMPORTED_MODULE_8__core_event_types__["b" /* BelaEventType */].REQUEST_NEXT_ALBUM,
                value: evValue
            });
            this.ui.showAlbumRequestLoader = true;
        }
    };
    LightboxComponent.prototype.like = function (post) {
        var _this = this;
        if (!post.is_liking && !post.is_liked && this.requesting_user) {
            post.is_liking = true;
            post.is_liked = true;
            this.appService.like(post).then(function (res) {
                if (res.error === __WEBPACK_IMPORTED_MODULE_4__app_model__["b" /* LikeBelaResult */].E_INSUFFICIENT_COIN) {
                    _this._insufficientMessage.show();
                }
                else {
                    var result = res.result;
                    if (result.bela) {
                        post.likes.push(result.bela);
                        post.belas = result.total_bela;
                        post.earnings = result.total_earning;
                        post.is_liked = result.liked;
                        post.is_liking = false;
                        post.total_likes_count = result.total_likes;
                    }
                    if (result.user) {
                        _this.requesting_user.user1 = result.user;
                        _this.authService.updateUser(_this.requesting_user);
                        _this.eventService.fireEvent({
                            type: __WEBPACK_IMPORTED_MODULE_8__core_event_types__["b" /* BelaEventType */].LIKE_POST,
                            value: _this.requesting_user
                        });
                    }
                }
            }).catch(function () {
                post.is_liked = false;
                post.is_liking = false;
            });
        }
    };
    LightboxComponent.prototype.removeAlbumAtPos = function (idx) {
        this.eventService.fireEvent({
            type: __WEBPACK_IMPORTED_MODULE_8__core_event_types__["b" /* BelaEventType */].DELETED_POST,
            value: this.album[idx].thot
        });
        this.album.splice(this.currentImageIndex, 1);
        if (this.album.length < 1) {
            this._lightboxEvent.broadcastLightboxEvent({ id: __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["a" /* LIGHTBOX_EVENT */].CLOSE, data: null });
        }
        else {
            this.prevImage();
        }
    };
    LightboxComponent.prototype.report = function () {
        var _this = this;
        jQuery('#dropdownMenuLink3').dropdown('toggle');
        this.appService.report(this.currentThot.id).then(function (result) {
            _this.removeAlbumAtPos(_this.currentImageIndex);
        });
    };
    LightboxComponent.prototype.delete = function () {
        var _this = this;
        jQuery('#dropdownMenuLink3').dropdown('toggle');
        this.appService.deleteFeed(this.currentThot.id).then(function () {
            _this.removeAlbumAtPos(_this.currentImageIndex);
        });
    };
    LightboxComponent.prototype.deleteAdmin = function (thot, i) {
        var _this = this;
        jQuery('#dropdownMenuLink3').dropdown('toggle');
        this.appService.deleteAdminFeed(this.currentThot.id).then(function () {
            _this.removeAlbumAtPos(_this.currentImageIndex);
        }).catch(function () {
            alert("Can't delete the post. Try again later");
        });
    };
    LightboxComponent.prototype.block_user = function () {
        jQuery('#dropdownMenuLink3').dropdown('toggle');
        this.appService.blockUser(this.currentThot.user.id).then(function (listResp) {
            window.location.reload();
        });
    };
    LightboxComponent.prototype.editable = function (state) {
        jQuery('#dropdownMenuLink3').dropdown('toggle');
        this.currentThot.editable = state;
        if (state) {
            this.currentThot.tempContent = this.currentThot.content;
        }
        else {
            this.currentThot.tempContent = '';
        }
    };
    LightboxComponent.prototype.update = function () {
        var _this = this;
        this.appService.updateCaption(this.currentThot.id, this.currentThot.tempContent).then(function (result) {
            if (result['status'] == "success") {
                _this.currentThot.content = result['result'];
                _this.currentThot.editable = false;
            }
            else {
                alert("Something went wrong. Please try again later!");
            }
        });
    };
    LightboxComponent.prototype.addCommentToAlbumPost = function (comment) {
        this.currentThot.comments.push(comment);
        this.currentThot.displayed_comments.push(comment);
    };
    LightboxComponent.prototype.follow = function (user) {
        if (user && user.id) {
            this.appService.follow(user, this.csrf).then(function (result) {
                user.is_following = true;
            }).catch(function (err) {
            });
        }
    };
    LightboxComponent.prototype.unfollow = function (user) {
        this.appService.unfollow(user, this.csrf).then(function (result) {
            user.is_following = false;
        }).catch(function (err) { });
    };
    LightboxComponent.prototype.isHome = function () {
        return this.curPageID === 0;
    };
    LightboxComponent.prototype.openHome = function () {
        this.curPageID = 0;
    };
    LightboxComponent.prototype.openLikes = function () {
        if (this.currentThot.total_likes_count > 0) {
            this.curPageID = 1;
        }
    };
    LightboxComponent.prototype.openViews = function () {
        if (this.requesting_user) {
            this.curPageID = 2;
        }
    };
    LightboxComponent.prototype._validateInputData = function () {
        if (this.album && this.album.length > 0) {
            for (var i = 0; i < this.album.length; i++) {
                // check whether each _nside
                // album has src data or not
                if (this.album[i].src) {
                    continue;
                }
                throw new Error('One of the album data does not have source data');
            }
        }
        else {
            throw new Error('No album data or album data is not correct in type');
        }
        // to prevent data understand as string
        // convert it to number
        if (isNaN(this.currentImageIndex)) {
            throw new Error('Current image index is not a number');
        }
        else {
            this.currentImageIndex = Number(this.currentImageIndex);
        }
        return true;
    };
    LightboxComponent.prototype._registerImageLoadingEvent = function () {
        var _this = this;
        var preloader = new Image();
        preloader.onload = preloader.onerror = function () {
            _this._onLoadImageSuccess();
        };
        preloader.src = this.album[this.currentImageIndex].src;
    };
    /**
     * Fire when the image is loaded
     */
    LightboxComponent.prototype._onLoadImageSuccess = function () {
        if (!this.options.disableKeyboardNav) {
            // unbind keyboard event during transition
            this._disableKeyboardNav();
        }
        var imageHeight;
        var imageWidth;
        var maxImageHeight;
        var maxImageWidth;
        var windowHeight;
        var windowWidth;
        var naturalImageWidth;
        var naturalImageHeight;
        // set default width and height of image to be its natural
        imageWidth = naturalImageWidth = this._imageElem.nativeElement.naturalWidth;
        imageHeight = naturalImageHeight = this._imageElem.nativeElement.naturalHeight;
        if (this.options.fitImageInViewPort) {
            windowWidth = this._windowRef.innerWidth;
            windowHeight = this._windowRef.innerHeight;
            maxImageWidth = windowWidth - this._cssValue.containerLeftPadding -
                this._cssValue.containerRightPadding - this._cssValue.imageBorderWidthLeft -
                this._cssValue.imageBorderWidthRight - 20;
            maxImageHeight = windowHeight - this._cssValue.containerTopPadding -
                this._cssValue.containerTopPadding - this._cssValue.imageBorderWidthTop -
                this._cssValue.imageBorderWidthBottom - 120;
            if (naturalImageWidth > maxImageWidth || naturalImageHeight > maxImageHeight) {
                if ((naturalImageWidth / maxImageWidth) > (naturalImageHeight / maxImageHeight)) {
                    imageWidth = maxImageWidth;
                    imageHeight = Math.round(naturalImageHeight / (naturalImageWidth / imageWidth));
                }
                else {
                    imageHeight = maxImageHeight;
                    imageWidth = Math.round(naturalImageWidth / (naturalImageHeight / imageHeight));
                }
            }
            this._rendererRef.setElementStyle(this._imageElem.nativeElement, 'width', imageWidth + "px");
            this._rendererRef.setElementStyle(this._imageElem.nativeElement, 'height', imageHeight + "px");
        }
        this._sizeContainer(imageWidth, imageHeight);
        this._sizeNav(imageWidth, imageHeight);
        this._sizeOuterDiv();
    };
    LightboxComponent.prototype._sizeContainer = function (imageWidth, imageHeight) {
        var oldWidth = this._outerContainerElem.nativeElement.offsetWidth;
        var oldHeight = this._outerContainerElem.nativeElement.offsetHeight;
        var newWidth = imageWidth + this._cssValue.containerRightPadding + this._cssValue.containerLeftPadding +
            this._cssValue.imageBorderWidthLeft + this._cssValue.imageBorderWidthRight;
        var newHeight = imageHeight + this._cssValue.containerTopPadding
            + this._cssValue.containerBottomPadding + this._cssValue.imageBorderWidthTop
            + this._cssValue.imageBorderWidthBottom;
        if (oldWidth !== newWidth /* || oldHeight !== newHeight */) {
            this._rendererRef.setElementStyle(this._outerContainerElem.nativeElement, 'max-width', "935px");
            // this._rendererRef.setElementStyle(this._outerContainerElem.nativeElement, 'width', `${newWidth}px`);
            // this._rendererRef.setElementStyle(this._outerContainerElem.nativeElement, 'height', `${newHeight}px`);
            // bind resize event to outer container
            // this._event.transitions = [];
            // ['transitionend', 'webkitTransitionEnd', 'oTransitionEnd', 'MSTransitionEnd'].forEach(eventName => {
            //   this._event.transitions.push(
            //     this._rendererRef.listen(this._outerContainerElem.nativeElement, eventName, (event: any) => {
            //       if (event.target === event.currentTarget) {
            //         this._postResize(newWidth, newHeight);
            //       }
            //     })
            //   );
            // });
            this._postResize(newWidth, newHeight);
        }
        else {
            this._postResize(newWidth, newHeight);
        }
    };
    LightboxComponent.prototype._sizeNav = function (imageWidth, imageHeight) {
        this._rendererRef.setElementStyle(this._navArrowElem.nativeElement, 'height', imageHeight + "px");
    };
    LightboxComponent.prototype._sizeOuterDiv = function () {
        var containerHeight = this._outerContainerElem.nativeElement.offsetHeight;
        var windowHeight = this._windowRef.innerHeight;
        if (containerHeight > windowHeight) {
            this._rendererRef.setElementStyle(this._outerDivElem.nativeElement, 'height', 'auto');
        }
        else {
            this._rendererRef.setElementStyle(this._outerDivElem.nativeElement, 'height', '100%');
        }
    };
    LightboxComponent.prototype._postResize = function (newWidth, newHeight) {
        // unbind resize event
        if (Array.isArray(this._event.transitions)) {
            this._event.transitions.forEach(function (eventHandler) {
                eventHandler();
            });
            this._event.transitions = [];
        }
        this._rendererRef.setElementStyle(this._dataContainerElem.nativeElement, 'width', newWidth + "px");
        this._showImage();
    };
    LightboxComponent.prototype._showImage = function () {
        this.ui.showReloader = false;
        this._updateNav();
        this._updateDetails();
        if (!this.options.disableKeyboardNav) {
            this._enableKeyboardNav();
        }
    };
    LightboxComponent.prototype._prepareComponent = function () {
        // add css3 animation
        this._addCssAnimation();
        // position the image according to user's option
        this._positionLightBox();
    };
    LightboxComponent.prototype._positionLightBox = function () {
        var top = this.options.positionFromTop;
        var bottom = this.options.positionFromBottom;
        var left = 0;
        this._rendererRef.setElementStyle(this._lightboxElem.nativeElement, 'top', top + "px");
        this._rendererRef.setElementStyle(this._lightboxElem.nativeElement, 'bottom', bottom + "px");
        this._rendererRef.setElementStyle(this._lightboxElem.nativeElement, 'left', left + "px");
        this._rendererRef.setElementStyle(this._lightboxElem.nativeElement, 'display', 'block');
    };
    /**
     * addCssAnimation add css3 classes for animate lightbox
     */
    LightboxComponent.prototype._addCssAnimation = function () {
        var resizeDuration = this.options.resizeDuration;
        var fadeDuration = this.options.fadeDuration;
        this._rendererRef.setElementStyle(this._lightboxElem.nativeElement, '-webkit-animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._lightboxElem.nativeElement, '-animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._outerContainerElem.nativeElement, '-webkit-transition-duration', resizeDuration + "s");
        this._rendererRef.setElementStyle(this._outerContainerElem.nativeElement, '-transition-duration', resizeDuration + "s");
        this._rendererRef.setElementStyle(this._dataContainerElem.nativeElement, '-webkit-animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._dataContainerElem.nativeElement, '-animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._imageElem.nativeElement, '-webkit-animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._imageElem.nativeElement, '-animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._captionElem.nativeElement, '-webkit-animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._captionElem.nativeElement, '-animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._numberElem.nativeElement, '-webkit-animation-duration', fadeDuration + "s");
        this._rendererRef.setElementStyle(this._numberElem.nativeElement, '-animation-duration', fadeDuration + "s");
    };
    LightboxComponent.prototype._end = function () {
        var _this = this;
        this.ui.classList = 'lightbox animation fadeOut';
        setTimeout(function () {
            _this.cmpRef.destroy();
        }, this.options.fadeDuration * 1000);
    };
    LightboxComponent.prototype._updateDetails = function () {
        // update the caption
        if (typeof this.album[this.currentImageIndex].caption !== 'undefined' &&
            this.album[this.currentImageIndex].caption !== '') {
            this.ui.showCaption = true;
        }
        // update the page number if user choose to do so
        // does not perform numbering the page if the
        // array length in album <= 1
        if (this.album.length > 1 && this.options.showImageNumberLabel) {
            this.ui.showPageNumber = true;
            this.content.pageNumber = this._albumLabel();
        }
    };
    LightboxComponent.prototype._albumLabel = function () {
        // due to {this.currentImageIndex} is set from 0 to {this.album.length} - 1
        return "Image " + Number(this.currentImageIndex + 1) + " of " + this.album.length;
    };
    LightboxComponent.prototype._changeImage = function (newIndex) {
        this.currentImageIndex = newIndex;
        this._hideImage();
        this._registerImageLoadingEvent();
        this._lightboxEvent.broadcastLightboxEvent({ id: __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["a" /* LIGHTBOX_EVENT */].CHANGE_PAGE, data: newIndex });
    };
    LightboxComponent.prototype._hideImage = function () {
        this.ui.showReloader = true;
        this.ui.showArrowNav = false;
        this.ui.showLeftArrow = false;
        this.ui.showRightArrow = false;
        this.ui.showPageNumber = false;
        this.ui.showCaption = false;
    };
    LightboxComponent.prototype._updateNav = function () {
        var alwaysShowNav = false;
        // check to see the browser support touch event
        try {
            this._documentRef.createEvent('TouchEvent');
            alwaysShowNav = !!(this.options.alwaysShowNavOnTouchDevices);
        }
        catch (e) {
            // noop
        }
        // initially show the arrow nav
        // which is the parent of both left and right nav
        this._showArrowNav();
        if (this.album.length > 1) {
            if (this.options.wrapAround) {
                if (alwaysShowNav) {
                    // alternatives this.$lightbox.find('.lb-prev, .lb-next').css('opacity', '1');
                    this._rendererRef.setElementStyle(this._leftArrowElem.nativeElement, 'opacity', '1');
                    this._rendererRef.setElementStyle(this._rightArrowElem.nativeElement, 'opacity', '1');
                }
                // alternatives this.$lightbox.find('.lb-prev, .lb-next').show();
                this._showLeftArrowNav();
                this._showRightArrowNav();
            }
            else {
                if (this.currentImageIndex > 0) {
                    // alternatives this.$lightbox.find('.lb-prev').show();
                    this._showLeftArrowNav();
                    if (alwaysShowNav) {
                        // alternatives this.$lightbox.find('.lb-prev').css('opacity', '1');
                        this._rendererRef.setElementStyle(this._leftArrowElem.nativeElement, 'opacity', '1');
                    }
                }
                if ((this.currentImageIndex < this.album.length - 1) || (this.currentImageIndex >= this.album.length - 1 && this.options.allowRequestAlbum)) {
                    // alternatives this.$lightbox.find('.lb-next').show();
                    this._showRightArrowNav();
                    if (alwaysShowNav) {
                        // alternatives this.$lightbox.find('.lb-next').css('opacity', '1');
                        this._rendererRef.setElementStyle(this._rightArrowElem.nativeElement, 'opacity', '1');
                    }
                }
            }
        }
    };
    LightboxComponent.prototype._showLeftArrowNav = function () {
        this.ui.showLeftArrow = true;
    };
    LightboxComponent.prototype._showRightArrowNav = function () {
        this.ui.showRightArrow = true;
    };
    LightboxComponent.prototype._showArrowNav = function () {
        this.ui.showArrowNav = (this.album.length !== 1);
    };
    LightboxComponent.prototype._enableKeyboardNav = function () {
        var _this = this;
        this._event.keyup = this._rendererRef.listenGlobal('document', 'keyup', function (event) {
            _this._keyboardAction(event);
        });
    };
    LightboxComponent.prototype._disableKeyboardNav = function () {
        if (this._event.keyup) {
            this._event.keyup();
        }
    };
    LightboxComponent.prototype._keyboardAction = function ($event) {
        var KEYCODE_ESC = 27;
        var KEYCODE_LEFTARROW = 37;
        var KEYCODE_RIGHTARROW = 39;
        var keycode = $event.keyCode;
        var key = String.fromCharCode(keycode).toLowerCase();
        if (keycode === KEYCODE_ESC || key.match(/x|o|c/)) {
            this._lightboxEvent.broadcastLightboxEvent({ id: __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["a" /* LIGHTBOX_EVENT */].CLOSE, data: null });
        }
        else if (key === 'p' || keycode === KEYCODE_LEFTARROW) {
            this.prevImage();
        }
        else if (key === 'n' || keycode === KEYCODE_RIGHTARROW) {
            this.nextImage();
        }
    };
    LightboxComponent.prototype._getCssStyleValue = function (elem, propertyName) {
        return parseFloat(this._windowRef
            .getComputedStyle(elem.nativeElement, null)
            .getPropertyValue(propertyName));
    };
    LightboxComponent.prototype._onReceivedEvent = function (event) {
        switch (event.id) {
            case __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["a" /* LIGHTBOX_EVENT */].CLOSE:
                this._end();
                break;
            default:
                break;
        }
    };
    LightboxComponent.prototype._close = function () {
        this._lightboxEvent.broadcastLightboxEvent({ id: __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["a" /* LIGHTBOX_EVENT */].CLOSE, data: null });
    };
    Object.defineProperty(LightboxComponent.prototype, "currentThot", {
        get: function () {
            if (this.album && this.currentImageIndex !== null) {
                return this.album[this.currentImageIndex].thot;
            }
            return null;
        },
        enumerable: true,
        configurable: true
    });
    return LightboxComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], LightboxComponent.prototype, "album", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Number)
], LightboxComponent.prototype, "currentImageIndex", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], LightboxComponent.prototype, "options", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], LightboxComponent.prototype, "cmpRef", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('outerDiv'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], LightboxComponent.prototype, "_outerDivElem", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('outerContainer'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object)
], LightboxComponent.prototype, "_outerContainerElem", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('container'),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _c || Object)
], LightboxComponent.prototype, "_containerElem", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('leftArrow'),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _d || Object)
], LightboxComponent.prototype, "_leftArrowElem", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('rightArrow'),
    __metadata("design:type", typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _e || Object)
], LightboxComponent.prototype, "_rightArrowElem", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('navArrow'),
    __metadata("design:type", typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _f || Object)
], LightboxComponent.prototype, "_navArrowElem", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('dataContainer'),
    __metadata("design:type", typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _g || Object)
], LightboxComponent.prototype, "_dataContainerElem", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('image'),
    __metadata("design:type", typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _h || Object)
], LightboxComponent.prototype, "_imageElem", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('caption'),
    __metadata("design:type", typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _j || Object)
], LightboxComponent.prototype, "_captionElem", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('number'),
    __metadata("design:type", typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _k || Object)
], LightboxComponent.prototype, "_numberElem", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('insufficientMessage'),
    __metadata("design:type", typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_7__insufficient_bela_insufficient_bela_component__["a" /* InsufficientBelaComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__insufficient_bela_insufficient_bela_component__["a" /* InsufficientBelaComponent */]) === "function" && _l || Object)
], LightboxComponent.prototype, "_insufficientMessage", void 0);
LightboxComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("./src/app/lightbox/lightbox.component.html"),
        selector: '[lb-content]',
        host: {
            '(click)': 'close($event)',
            '[class]': 'ui.classList'
        },
        styleUrls: [],
        providers: [__WEBPACK_IMPORTED_MODULE_3__core_bela_api_service__["a" /* BelaApiService */]]
    }),
    __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["b" /* DOCUMENT */])),
    __metadata("design:paramtypes", [typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"]) === "function" && _o || Object, typeof (_p = typeof __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["b" /* LightboxEvent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["b" /* LightboxEvent */]) === "function" && _p || Object, typeof (_q = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _q || Object, typeof (_r = typeof __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["c" /* LightboxWindowRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__lightbox_event_service__["c" /* LightboxWindowRef */]) === "function" && _r || Object, Object, typeof (_s = typeof __WEBPACK_IMPORTED_MODULE_3__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _s || Object, typeof (_t = typeof __WEBPACK_IMPORTED_MODULE_5__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_event_service__["a" /* EventService */]) === "function" && _t || Object, typeof (_u = typeof __WEBPACK_IMPORTED_MODULE_6__core_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_auth_service__["a" /* AuthService */]) === "function" && _u || Object])
], LightboxComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u;
//# sourceMappingURL=lightbox.component.js.map

/***/ }),

/***/ "./src/app/lightbox/lightbox.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Lightbox; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__lightbox_component__ = __webpack_require__("./src/app/lightbox/lightbox.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__album_lightbox_component__ = __webpack_require__("./src/app/lightbox/album-lightbox.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lightbox_config_service__ = __webpack_require__("./src/app/lightbox/lightbox-config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__lightbox_event_service__ = __webpack_require__("./src/app/lightbox/lightbox-event.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__lightbox_overlay_component__ = __webpack_require__("./src/app/lightbox/lightbox-overlay.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};







var Lightbox = (function () {
    function Lightbox(_componentFactoryResolver, _injector, _applicationRef, _lightboxConfig, _lightboxEvent, _documentRef) {
        this._componentFactoryResolver = _componentFactoryResolver;
        this._injector = _injector;
        this._applicationRef = _applicationRef;
        this._lightboxConfig = _lightboxConfig;
        this._lightboxEvent = _lightboxEvent;
        this._documentRef = _documentRef;
    }
    Lightbox.prototype.close_all = function () {
        this._lightboxEvent.broadcastLightboxEvent({ id: __WEBPACK_IMPORTED_MODULE_5__lightbox_event_service__["a" /* LIGHTBOX_EVENT */].CLOSE, data: null });
    };
    Lightbox.prototype.openPost = function (album, curIndex, options) {
        if (curIndex === void 0) { curIndex = 0; }
        if (options === void 0) { options = {}; }
        this._open(album, curIndex, options, __WEBPACK_IMPORTED_MODULE_2__lightbox_component__["a" /* LightboxComponent */]);
    };
    Lightbox.prototype.openAlbum = function (album, curIndex, options) {
        if (curIndex === void 0) { curIndex = 0; }
        if (options === void 0) { options = {}; }
        this._open(album, curIndex, options, __WEBPACK_IMPORTED_MODULE_3__album_lightbox_component__["a" /* AlbumLightboxComponent */]);
    };
    Lightbox.prototype._open = function (album, curIndex, options, componentClass) {
        var _this = this;
        if (curIndex === void 0) { curIndex = 0; }
        if (options === void 0) { options = {}; }
        var overlayComponentRef = this._createComponent(__WEBPACK_IMPORTED_MODULE_6__lightbox_overlay_component__["a" /* LightboxOverlayComponent */]);
        var componentRef = this._createComponent(componentClass);
        var newOptions = {};
        // broadcast open event
        this._lightboxEvent.broadcastLightboxEvent(__WEBPACK_IMPORTED_MODULE_5__lightbox_event_service__["a" /* LIGHTBOX_EVENT */].OPEN);
        Object.assign(newOptions, this._lightboxConfig, options);
        // attach input to lightbox
        componentRef.instance.album = album;
        componentRef.instance.currentImageIndex = curIndex;
        componentRef.instance.options = newOptions;
        componentRef.instance.cmpRef = componentRef;
        // attach input to overlay
        overlayComponentRef.instance.options = newOptions;
        overlayComponentRef.instance.cmpRef = overlayComponentRef;
        // FIXME: not sure why last event is broadcasted (which is CLOSED) and make
        // lightbox can not be opened the second time.
        // Need to timeout so that the OPEN event is set before component is initialized
        setTimeout(function () {
            _this._applicationRef.attachView(overlayComponentRef.hostView);
            _this._applicationRef.attachView(componentRef.hostView);
            overlayComponentRef.onDestroy(function () {
                var body = _this._documentRef.querySelector('body');
                body.classList.remove("modal-open");
                body.style.top = 0;
                window.scrollTo(0, _this.scrollTop);
                _this._applicationRef.detachView(overlayComponentRef.hostView);
            });
            componentRef.onDestroy(function () {
                _this._applicationRef.detachView(componentRef.hostView);
            });
            var body = _this._documentRef.querySelector('body');
            _this.scrollTop = window.pageYOffset;
            body.classList.add('modal-open');
            body.style.top = -_this.scrollTop + "px";
            body.appendChild(overlayComponentRef.location.nativeElement);
            body.appendChild(componentRef.location.nativeElement);
        });
    };
    Lightbox.prototype._createComponent = function (ComponentClass) {
        var factory = this._componentFactoryResolver.resolveComponentFactory(ComponentClass);
        var component = factory.create(this._injector);
        return component;
    };
    return Lightbox;
}());
Lightbox = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["b" /* DOCUMENT */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ComponentFactoryResolver"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ComponentFactoryResolver"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injector"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injector"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ApplicationRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ApplicationRef"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__lightbox_config_service__["a" /* LightboxConfig */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__lightbox_config_service__["a" /* LightboxConfig */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__lightbox_event_service__["b" /* LightboxEvent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__lightbox_event_service__["b" /* LightboxEvent */]) === "function" && _e || Object, Object])
], Lightbox);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=lightbox.service.js.map

/***/ }),

/***/ "./src/app/modals/avatar-edit-modal/avatar-edit-modal.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/modals/avatar-edit-modal/avatar-edit-modal.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-dialog modal-m image-edit-modal\">\n  <div class=\"modal-content\">\n\n    <div class=\"modal-header\">\n      <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n      <h5 class=\"modal-title\">{{ this.title }}</h5>\n    </div>\n    <div class=\"modal-body\">\n      <div class=\"row\">\n        <div class=\"imageeditdiv col-md-8\" #imageEditDiv>\n          <img-cropper id=\"previewimage\" #cropper [image]=\"data\" [settings]=\"cropperSettings\"></img-cropper>\n        </div>\n        <div class=\"resultimage rounded col-md-4\" *ngIf=\"data.image\" >\n          <h5 class=\"preview_label\">Preview</h5>\n          <img [src]=\"data.image\" [width]=\"cropperSettings.croppedWidth\">\n        </div>\n      </div>\n\n    </div>\n    <div class=\"modal-footer\">\n      <button type=\"button\" class=\"btn btn-primary\" (click)=\"saveImage($event);\">Save changes</button>\n      <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/modals/avatar-edit-modal/avatar-edit-modal.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AvatarEditModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_img_cropper__ = __webpack_require__("./node_modules/ng2-img-cropper/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap_modal__ = __webpack_require__("./node_modules/ng2-bootstrap-modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap_modal___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap_modal__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AvatarEditModalComponent = (function (_super) {
    __extends(AvatarEditModalComponent, _super);
    function AvatarEditModalComponent(dialogService) {
        var _this = _super.call(this, dialogService) || this;
        _this.imageFile = null;
        _this.type = 'circle';
        _this.keepAspect = false;
        _this.title = 'Edit Image';
        _this.csrf = "";
        return _this;
    }
    AvatarEditModalComponent.prototype.ngOnInit = function () {
        this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
        this.cropperSettings = new __WEBPACK_IMPORTED_MODULE_1_ng2_img_cropper__["b" /* CropperSettings */]();
        this.cropperSettings.canvasWidth = 200;
        this.cropperSettings.canvasHeight = 200;
        this.cropperSettings.croppedWidth = 1000;
        this.cropperSettings.minWidth = 200;
        this.cropperSettings.minHeight = 200;
        this.cropperSettings.noFileInput = true;
        this.cropperSettings.preserveSize = true;
        this.cropperSettings.keepAspect = this.keepAspect;
        this.cropperSettings.dynamicSizing = true;
        this.cropperSettings.cropperClass = "previewimage_canvas";
        this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,0,0,1)';
        this.cropperSettings.touchRadius = 20;
        this.cropperSettings.rounded = this.type == "circle";
        this.data = {};
        this.drawImage();
    };
    AvatarEditModalComponent.prototype.ngAfterViewInit = function () {
        this.resizeCroppingCanvas();
    };
    AvatarEditModalComponent.prototype.resizeCroppingCanvas = function () {
        var _this = this;
        this.resizeHandler = function () {
            _this.cropper.cropper.resizeCanvas(jQuery(_this.imageEditDiv.nativeElement).width(), 400, true);
        };
        this.resizeHandler();
        jQuery(window).resize(this.resizeHandler);
    };
    AvatarEditModalComponent.prototype.ngOnDestroy = function () {
        _super.prototype.ngOnDestroy.call(this);
        jQuery(window).off('resize', this.resizeHandler);
    };
    AvatarEditModalComponent.prototype.drawImage = function () {
        var _this = this;
        var image = new Image();
        var file = this.imageFile;
        var Reader = new FileReader();
        Reader.onloadend = function (loadEvent) {
            image.src = loadEvent.target.result;
            image.onload = function () {
                _this.cropper.setImage(image);
            };
        };
        Reader.readAsDataURL(file);
    };
    AvatarEditModalComponent.prototype.saveImage = function (event) {
        event.stopPropagation();
        this.result = this.data.image;
        this.close();
        // let postData = {
        //   csrfmiddlewaretoken:this.csrf
        // };
        // let file = this.imageFile;
        // if(file) {
        //   let fileName = file.name;
        //   let fileType = file.type;
        //   let data = this.data.image;
        //
        //   this.belaAPIService.getImagePostLink(fileName, fileType)
        //     .then((res: S3Resource) => {
        //       postData['image_path'] = res.url.replace(res.data.url, '');
        //       return this.s3APIService.uploadBlobToS3(res, data, fileName)
        //     })
        //     .then(res =>{
        //
        //       window.location.reload();
        //     });
        //   // this.callback_url
        //   // this.appService.postWithImage(, postData, 'avatar', this.data.image, files[0].name, files[0].type).then(result => {
        //   //
        //   //
        //   // });
        // } else {
        //   alert("file tag is empty!")
        // }
    };
    return AvatarEditModalComponent;
}(__WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap_modal__["DialogComponent"]));
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('cropper', undefined),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_img_cropper__["c" /* ImageCropperComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_img_cropper__["c" /* ImageCropperComponent */]) === "function" && _a || Object)
], AvatarEditModalComponent.prototype, "cropper", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('imageEditDiv'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object)
], AvatarEditModalComponent.prototype, "imageEditDiv", void 0);
AvatarEditModalComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-avatar-edit-modal',
        template: __webpack_require__("./src/app/modals/avatar-edit-modal/avatar-edit-modal.component.html"),
        styles: [__webpack_require__("./src/app/modals/avatar-edit-modal/avatar-edit-modal.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap_modal__["DialogService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap_modal__["DialogService"]) === "function" && _c || Object])
], AvatarEditModalComponent);

var _a, _b, _c;
//# sourceMappingURL=avatar-edit-modal.component.js.map

/***/ }),

/***/ "./src/app/modals/cover-edit-modal/cover-edit-modal.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/modals/cover-edit-modal/cover-edit-modal.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-dialog modal-m image-edit-modal\">\n  <div class=\"modal-content\">\n\n    <div class=\"modal-header\">\n      <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n      <h5 class=\"modal-title\">{{ this.title }}</h5>\n    </div>\n    <div class=\"modal-body\">\n      <div class=\"row\">\n        <div class=\"imageeditdiv col-md-12\" #imageEditDiv>\n          <img-cropper id=\"previewimage\" #cropper [image]=\"data\" [settings]=\"cropperSettings\"></img-cropper>\n        </div>\n      </div>\n    </div>\n    <div class=\"modal-footer\">\n      <button type=\"button\" class=\"btn btn-primary\" (click)=\"saveImage($event);\">Save changes</button>\n      <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/modals/cover-edit-modal/cover-edit-modal.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CoverEditModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_img_cropper__ = __webpack_require__("./node_modules/ng2-img-cropper/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap_modal__ = __webpack_require__("./node_modules/ng2-bootstrap-modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap_modal___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap_modal__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CoverEditModalComponent = (function (_super) {
    __extends(CoverEditModalComponent, _super);
    function CoverEditModalComponent(appService, dialogService) {
        var _this = _super.call(this, dialogService) || this;
        _this.appService = appService;
        _this.imageFile = null;
        _this.type = 'rect';
        _this.keepAspect = false;
        _this.title = 'Edit Image';
        _this.csrf = "";
        return _this;
    }
    CoverEditModalComponent.prototype.ngOnInit = function () {
        this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
        this.cropperSettings = new __WEBPACK_IMPORTED_MODULE_1_ng2_img_cropper__["b" /* CropperSettings */]();
        this.cropperSettings.canvasWidth = 200;
        this.cropperSettings.canvasHeight = 200;
        this.cropperSettings.croppedWidth = 1140;
        this.cropperSettings.width = 325;
        this.cropperSettings.height = 100;
        this.cropperSettings.noFileInput = true;
        this.cropperSettings.preserveSize = true;
        this.cropperSettings.keepAspect = true;
        this.cropperSettings.dynamicSizing = true;
        this.cropperSettings.cropperClass = "previewimage_canvas";
        this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,0,0,1)';
        this.cropperSettings.touchRadius = 20;
        this.cropperSettings.rounded = this.type == "circle";
        this.data = {};
        this.drawImage();
    };
    CoverEditModalComponent.prototype.ngAfterViewInit = function () {
        this.resizeCropperCanvas();
    };
    CoverEditModalComponent.prototype.ngOnDestroy = function () {
        _super.prototype.ngOnDestroy.call(this);
        jQuery(window).off('resize', this.resizeHandler);
    };
    CoverEditModalComponent.prototype.resizeCropperCanvas = function () {
        var _this = this;
        this.resizeHandler = function () {
            _this.cropper.cropper.resizeCanvas(jQuery(_this.imageEditDiv.nativeElement).width(), 400, true);
        };
        this.resizeHandler();
        jQuery(window).resize(this.resizeHandler);
    };
    CoverEditModalComponent.prototype.drawImage = function () {
        var _this = this;
        var image = new Image();
        var file = this.imageFile;
        var Reader = new FileReader();
        Reader.onloadend = function (loadEvent) {
            image.src = loadEvent.target.result;
            image.onload = function () {
                _this.cropper.setImage(image);
            };
        };
        Reader.readAsDataURL(file);
    };
    CoverEditModalComponent.prototype.saveImage = function (event) {
        event.stopPropagation();
        this.result = this.data.image;
        this.close();
    };
    return CoverEditModalComponent;
}(__WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap_modal__["DialogComponent"]));
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('cropper', undefined),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_img_cropper__["c" /* ImageCropperComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_img_cropper__["c" /* ImageCropperComponent */]) === "function" && _a || Object)
], CoverEditModalComponent.prototype, "cropper", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('imageEditDiv'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object)
], CoverEditModalComponent.prototype, "imageEditDiv", void 0);
CoverEditModalComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-cover-edit-modal',
        template: __webpack_require__("./src/app/modals/cover-edit-modal/cover-edit-modal.component.html"),
        styles: [__webpack_require__("./src/app/modals/cover-edit-modal/cover-edit-modal.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap_modal__["DialogService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap_modal__["DialogService"]) === "function" && _d || Object])
], CoverEditModalComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=cover-edit-modal.component.js.map

/***/ }),

/***/ "./src/app/modals/likes-modal-wrapper/likes-modal-wrapper.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/modals/likes-modal-wrapper/likes-modal-wrapper.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"modal fade\"\n     *ngIf=\"thot.total_likes_count\"\n     [attr.id]=\"id\"\n     tabindex=\"-1\"\n     role=\"dialog\"\n     aria-labelledby=\"myLargeModalLabel\"\n     app-likes-modal\n     [thot]=\"thot\" [user]=\"user\"\n>\n</div>\n"

/***/ }),

/***/ "./src/app/modals/likes-modal-wrapper/likes-modal-wrapper.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LikesModalWrapperComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_model__ = __webpack_require__("./src/app/app.model.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LikesModalWrapperComponent = (function () {
    function LikesModalWrapperComponent() {
        this.thot = null;
        this.id = 'likebox';
        this.user = null;
    }
    LikesModalWrapperComponent.prototype.ngOnInit = function () {
    };
    return LikesModalWrapperComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__app_model__["g" /* Thought */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__app_model__["g" /* Thought */]) === "function" && _a || Object)
], LikesModalWrapperComponent.prototype, "thot", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], LikesModalWrapperComponent.prototype, "id", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]) === "function" && _b || Object)
], LikesModalWrapperComponent.prototype, "user", void 0);
LikesModalWrapperComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-likes-modal-wrapper',
        template: __webpack_require__("./src/app/modals/likes-modal-wrapper/likes-modal-wrapper.component.html"),
        styles: [__webpack_require__("./src/app/modals/likes-modal-wrapper/likes-modal-wrapper.component.css")]
    }),
    __metadata("design:paramtypes", [])
], LikesModalWrapperComponent);

var _a, _b;
//# sourceMappingURL=likes-modal-wrapper.component.js.map

/***/ }),

/***/ "./src/app/modals/likes-modal/likes-modal.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/modals/likes-modal/likes-modal.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-dialog modal-m\">\n  <div class=\"modal-content\">\n\n    <div class=\"white-box followerbox\" infiniteScroll\n                       [infiniteScrollDistance]=\"3\"\n                       [infiniteScrollThrottle]=\"1000\"\n                       (scrolled)=\"onLikeScroll()\"\n                       [scrollWindow]=\"false\">\n\n      <h4 class=\"followerboxheader\">Likes ({{ thot.total_likes_count }})</h4>\n\n      <ul class=\"followerscroll\">\n        <li *ngFor=\"let like of thot.likes\">\n\n          <a [routerLink]=\"['/user/', like.username]\">\n            <span class=\"circlephoto\" [innerHTML]=\"like.avatar\"></span> <span class=\"modalusername\">{{like.username}}</span>\n          </a>\n\n          <a  href=\"#\"\n              (click)=\"follow(like);false\"\n              *ngIf=\"(!user || like.id != user.id) && !like.is_following\"\n              class=\"followbtn\" translate>Follow</a>\n\n          <a *ngIf=\"(!user || like.id != user.id) && like.is_following\"\n             href=\"#\" (click)=\"unfollow(like);false\"\n             class=\"followingbtn\" translate>Following</a>\n        </li>\n      </ul>\n      <sk-three-bounce *ngIf=\"like_loading == true\"></sk-three-bounce>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/modals/likes-modal/likes-modal.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LikesModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_model__ = __webpack_require__("./src/app/app.model.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LikesModalComponent = (function () {
    function LikesModalComponent(appService) {
        this.appService = appService;
        this.thot = null;
        this.user = null;
        this.csrf = "";
        this.like_loading = false;
        this.likes_api_url = '/api/likes/';
    }
    LikesModalComponent.prototype.ngOnInit = function () {
        this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
        if (this.thot.total_likes_count > 20 && this.thot.likes.length <= 20) {
            this.thot.likes_api_url = this.likes_api_url + this.thot.id + '/?page=2';
        }
        else if (this.thot.total_likes_count <= 20) {
            this.thot.likes_api_url = null;
        }
    };
    LikesModalComponent.prototype.follow = function (user) {
        if (user && user.id) {
            this.appService.follow(user, this.csrf).then(function () {
                user.is_following = true;
            }).catch(function (err) {
            });
        }
    };
    LikesModalComponent.prototype.unfollow = function (user) {
        if (user && user.id) {
            this.appService.unfollow(user, this.csrf).then(function () {
                user.is_following = false;
            }).catch(function (err) {
            });
        }
    };
    LikesModalComponent.prototype.onLikeScroll = function () {
        var _this = this;
        if (this.thot.likes_api_url != null && !this.like_loading) {
            this.like_loading = true;
            this.appService.getList(this.thot.likes_api_url).subscribe(function (likeListResp) {
                for (var _i = 0, _a = likeListResp.results; _i < _a.length; _i++) {
                    var result = _a[_i];
                    _this.thot.likes.push(result);
                }
                _this.thot.likes_api_url = likeListResp.next;
                _this.like_loading = false;
            });
        }
    };
    return LikesModalComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__app_model__["g" /* Thought */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__app_model__["g" /* Thought */]) === "function" && _a || Object)
], LikesModalComponent.prototype, "thot", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__app_model__["h" /* User */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__app_model__["h" /* User */]) === "function" && _b || Object)
], LikesModalComponent.prototype, "user", void 0);
LikesModalComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: '[app-likes-modal]',
        template: __webpack_require__("./src/app/modals/likes-modal/likes-modal.component.html"),
        styles: [__webpack_require__("./src/app/modals/likes-modal/likes-modal.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _c || Object])
], LikesModalComponent);

var _a, _b, _c;
//# sourceMappingURL=likes-modal.component.js.map

/***/ }),

/***/ "./src/app/modals/views-modal-wrapper/views-modal-wrapper.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/modals/views-modal-wrapper/views-modal-wrapper.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"modal fade\"\n     *ngIf=\"thot.views.length\"\n     [attr.id]=\"id\"\n     tabindex=\"-1\"\n     role=\"dialog\"\n     aria-labelledby=\"myLargeModalLabel\"\n     app-views-modal\n     [thot]=\"thot\"\n     [user]=\"user\"\n>\n</div>\n"

/***/ }),

/***/ "./src/app/modals/views-modal-wrapper/views-modal-wrapper.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewsModalWrapperComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_model__ = __webpack_require__("./src/app/app.model.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ViewsModalWrapperComponent = (function () {
    function ViewsModalWrapperComponent() {
        this.thot = null;
        this.id = 'viewbox';
        this.user = null;
    }
    ViewsModalWrapperComponent.prototype.ngOnInit = function () {
    };
    return ViewsModalWrapperComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__app_model__["g" /* Thought */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__app_model__["g" /* Thought */]) === "function" && _a || Object)
], ViewsModalWrapperComponent.prototype, "thot", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], ViewsModalWrapperComponent.prototype, "id", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]) === "function" && _b || Object)
], ViewsModalWrapperComponent.prototype, "user", void 0);
ViewsModalWrapperComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-views-modal-wrapper',
        template: __webpack_require__("./src/app/modals/views-modal-wrapper/views-modal-wrapper.component.html"),
        styles: [__webpack_require__("./src/app/modals/views-modal-wrapper/views-modal-wrapper.component.css")]
    }),
    __metadata("design:paramtypes", [])
], ViewsModalWrapperComponent);

var _a, _b;
//# sourceMappingURL=views-modal-wrapper.component.js.map

/***/ }),

/***/ "./src/app/modals/views-modal/views-modal.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/modals/views-modal/views-modal.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-dialog modal-m\">\n  <div class=\"modal-content\">\n\n    <div class=\"white-box followerbox\">\n\n      <h4 class=\"followerboxheader\">Views ({{ thot.views.length }})</h4>\n\n      <ul class=\"followerscroll\">\n        <li *ngFor=\"let view of thot.views\">\n\n          <a [routerLink]=\"['/user/', view.username]\">\n            <span class=\"circlephoto\" [innerHTML]=\"view.avatar\"></span> @{{view.username}}\n          </a>\n\n          <a  href=\"#\"\n              (click)=\"follow(view);false\"\n              *ngIf=\"(!user || view.id != user.id) && !view.is_following\"\n              class=\"followbtn\" translate>Follow</a>\n\n          <a *ngIf=\"(!user || view.id != user.id) && view.is_following\"\n             href=\"#\" (click)=\"unfollow(view);false\"\n             class=\"followingbtn\" translate>Following</a>\n        </li>\n      </ul>\n\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/modals/views-modal/views-modal.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewsModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ViewsModalComponent = (function () {
    function ViewsModalComponent(appService) {
        this.appService = appService;
        this.thot = null;
        this.user = null;
        this.csrf = "";
    }
    ViewsModalComponent.prototype.ngOnInit = function () {
        this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
    };
    ViewsModalComponent.prototype.follow = function (user) {
        if (user && user.id) {
            this.appService.follow(user, this.csrf).then(function () {
                user.is_following = true;
            }).catch(function (err) {
            });
        }
    };
    ViewsModalComponent.prototype.unfollow = function (user) {
        if (user && user.id) {
            this.appService.unfollow(user, this.csrf).then(function () {
                user.is_following = false;
            }).catch(function (err) {
            });
        }
    };
    return ViewsModalComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__app_model__["g" /* Thought */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__app_model__["g" /* Thought */]) === "function" && _a || Object)
], ViewsModalComponent.prototype, "thot", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]) === "function" && _b || Object)
], ViewsModalComponent.prototype, "user", void 0);
ViewsModalComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: '[app-views-modal]',
        template: __webpack_require__("./src/app/modals/views-modal/views-modal.component.html"),
        styles: [__webpack_require__("./src/app/modals/views-modal/views-modal.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _c || Object])
], ViewsModalComponent);

var _a, _b, _c;
//# sourceMappingURL=views-modal.component.js.map

/***/ }),

/***/ "./src/app/not-found/not-found.component.css":
/***/ (function(module, exports) {

module.exports = ".background {\n  background: -webkit-gradient(linear, left top, left bottom, from(#7fc4ff), color-stop(50%, #bfe2ff), to(#fafafa)) no-repeat;\n  background: linear-gradient(to bottom, #7fc4ff 0%, #bfe2ff 50%, #fafafa 100%) no-repeat;\n  height: 100vh;\n  position: fixed;\n}\n\n.main-wrapper {\n  font-size: 25px;\n  margin-bottom: 30px;\n}\n\n.left-section {\n  float: left;\n  width: 60%;\n  padding: 25px;\n}\n\n.right-section {\n  float: left;\n  width: 40%;\n  padding: 25px;\n}\n\n.container {\n  padding-right: 15px;\n  padding-left: 15px;\n  margin-right: auto;\n  margin-left: auto;\n}\n\n.container::after {\n  content: ' ';\n  display: block;\n  clear: both;\n}\n\n.notice-important {\n  font-size: 1.7em;\n}\n\n.notice-wrapper {\n  margin-top: calc(50vh - 100px);\n}\n\n.balloon-wrapper {\n  margin-top: calc(50vh - 350px);\n}\n\n.showonmobile {\n  display: none;\n}\n\n@media (max-width: 768px) {\n  .main-wrapper {\n    font-size: 17px;\n  }\n  .hideonmobile {\n    display: none;\n  }\n  .showonmobile {\n    display: block;\n  }\n  .showonmobile .balloon-wrapper {\n    width: 40%;\n    margin: auto;\n    margin-top: 50px;\n  }\n  .balloon-wrapper img{\n    width: 100%;\n  }\n  .left-section {\n    width: 100%;\n    padding: 0;\n  }\n  .notice-wrapper {\n    margin-top: 20px;\n    padding: 20px;\n  }\n}\n\n@media (min-width: 768px) {\n  .container {\n    width: 750px;\n  }\n  .balloon-wrapper img{\n    max-width: 300px;\n  }\n}\n\n@media (min-width: 992px) {\n  .container {\n    width: 970px;\n  }\n}\n\n@media (min-width: 1200px) {\n  .container {\n    width: 1170px;\n  }\n}\n"

/***/ }),

/***/ "./src/app/not-found/not-found.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"background\"></div>\n<div class=\"main-wrapper\">\n  <div class=\"container\">\n    <div class=\"showonmobile\">\n      <div class=\"balloon-wrapper\">\n        <img src=\"https://s3.amazonaws.com/bellassets/assets/images/balloon.png\">\n      </div>\n    </div>\n    <div class=\"left-section\">\n      <div class=\"notice-wrapper\">\n        <div class=\"notice-important\" translate>Oops...</div>\n        <p translate>\n          We didn't find the page you are looking for.\n        </p>\n        <p>\n          {{'Please click |here| to go back to home page.'|translate|translateCut: 0}}\n          <a [routerLink]=\"['']\">{{'Please click |here| to go back to home page.'|translate|translateCut:1}}</a>\n          {{'Please click |here| to go back to home page.'|translate|translateCut:2}}\n        </p>\n      </div>\n    </div>\n    <div class=\"right-section hideonmobile\">\n      <div class=\"balloon-wrapper\">\n        <img src=\"https://s3.amazonaws.com/bellassets/assets/images/balloon.png\">\n      </div>\n    </div>\n  </div>\n\n\n</div>\n"

/***/ }),

/***/ "./src/app/not-found/not-found.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotFoundComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NotFoundComponent = (function () {
    function NotFoundComponent(_location) {
        this._location = _location;
    }
    NotFoundComponent.prototype.ngOnInit = function () {
    };
    NotFoundComponent.prototype.goback = function () {
        this._location.back();
    };
    return NotFoundComponent;
}());
NotFoundComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-not-found',
        template: __webpack_require__("./src/app/not-found/not-found.component.html"),
        styles: [__webpack_require__("./src/app/not-found/not-found.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common__["Location"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common__["Location"]) === "function" && _a || Object])
], NotFoundComponent);

var _a;
//# sourceMappingURL=not-found.component.js.map

/***/ }),

/***/ "./src/app/notifications/notification-page.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/notifications/notification-page.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"notification-list white-box\" infiniteScroll\n     [infiniteScrollDistance]=\"0.4\"\n     [infiniteScrollThrottle]=\"1000\"\n     (scrolled)=\"onNotificationScroll()\">\n\n\n  <h3>Notifications</h3>\n  <div class=\"center\">\n    <button class=\"btn btn-primary\" *ngIf=\"new_notifications\" (click)=\"loadNewNotifications()\">\n      View {{new_notifications}} new notifications\n    </button>\n  </div>\n\n  <span *ngFor=\"let notification of notification_list\">\n    <a [ngClass]=\"{'unread': notification.unread}\"\n       class=\"notification-list-item\"\n       href=\"#\" (click)=\"goToAction(notification);false\">\n\n      <span class=\"circlephoto notify-avatar\"\n            (click)=\"handleClickAvatar($event, notification);false\"\n            [innerHTML]=\"notification.actor_thumbnail\">\n      </span>\n      <div class=\"notification-item-right vertical-list\">\n        <div class=\"vertical-list-item\">\n          <span class=\"notify-text\">{{notification.actor}}{{notification.verb}}</span>\n        </div>\n        <div class=\"vertical-list-item\">\n          <div class=\"notify-actions\">\n            <!--<button *ngIf=\"notification.actor_user && !notification.actor_user.is_following\"-->\n            <!--class=\"btn-follow\" (click)=\"followUser($event, notification.actor_user);false\" translate>-->\n            <!--Follow-->\n            <!--</button>-->\n          </div>\n          <span class=\"notify-time\">\n            {{ notification.timestamp |messageTime }}\n          </span>\n        </div>\n      </div>\n\n    </a>\n  </span>\n  <sk-three-bounce *ngIf=\"loading == true\"></sk-three-bounce>\n</div>\n"

/***/ }),

/***/ "./src/app/notifications/notification-page.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationPageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_web_socket_service__ = __webpack_require__("./src/app/core/web-socket.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var NotificationPageComponent = (function () {
    function NotificationPageComponent(apiService, authService, webSocketSerivce, route, router) {
        this.apiService = apiService;
        this.authService = authService;
        this.webSocketSerivce = webSocketSerivce;
        this.route = route;
        this.router = router;
        this.csrf = "";
        this.requesting_user = new __WEBPACK_IMPORTED_MODULE_3__app_model__["h" /* User */]();
        this.subscriptions = [];
        this.all_notifications_api_url = '/api/notifications/';
        this.next_page_url = '';
        this.new_notifications = 0;
        this.notification_list = [];
        this.loading = true;
    }
    NotificationPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
        this.requesting_user = this.authService.user;
        if (this.authService.isLoggedIn()) {
            this.loadNewNotifications();
            this.subscriptions.push(this.webSocketSerivce.subscribe({
                next: function (event) {
                    if (event.type === __WEBPACK_IMPORTED_MODULE_5__core_web_socket_service__["a" /* WebSocketEvent */].EVENT_NEW_NOTIFICATION) {
                        _this.new_notifications++;
                    }
                }
            }));
        }
    };
    NotificationPageComponent.prototype.ngOnDestroy = function () {
        for (var _i = 0, _a = this.subscriptions; _i < _a.length; _i++) {
            var subscription = _a[_i];
            subscription.unsubscribe();
        }
    };
    NotificationPageComponent.prototype.handleClickAvatar = function (event, notification) {
        this.router.navigate(['user/', notification.actor]);
    };
    NotificationPageComponent.prototype.loadNewNotifications = function () {
        var _this = this;
        this.loading = true;
        this.apiService.getNotificatons(this.all_notifications_api_url, { from: new Date().toISOString() })
            .then(function (listResp) {
            _this.next_page_url = listResp.next;
            _this.new_notifications = 0;
            _this.notification_list = listResp.results;
            _this.loading = false;
        });
    };
    NotificationPageComponent.prototype.goToAction = function (notification) {
        var _this = this;
        this.apiService.markItRead(notification).then(function (result) {
            if (notification.unread == true) {
                notification.unread = false;
                _this.unread_count = _this.unread_count - 1;
            }
        });
        if (notification.action == "comment" || notification.action == "bella")
            this.router.navigate(['/post', notification.username, notification.action_id]);
        else if (notification.action == "user") {
            this.router.navigate(['/user', notification.username]);
        }
    };
    NotificationPageComponent.prototype.followUser = function (event, user) {
        event.preventDefault();
        event.stopPropagation();
        this.apiService.follow(user, this.csrf).then(function (listResp) {
            window.location.reload();
        });
    };
    NotificationPageComponent.prototype.onNotificationScroll = function () {
        var _this = this;
        var bottom_reached = this.apiService.isBottomReached();
        if (this.next_page_url != null && bottom_reached && !this.loading) {
            this.loading = true;
            this.apiService.getNotificatons(this.next_page_url, {}).then(function (listResp) {
                _this.next_page_url = listResp.next;
                for (var _i = 0, _a = listResp.results; _i < _a.length; _i++) {
                    var result = _a[_i];
                    _this.notification_list.push(result);
                }
                _this.loading = false;
            });
        }
    };
    return NotificationPageComponent;
}());
NotificationPageComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-notification-page',
        template: __webpack_require__("./src/app/notifications/notification-page.component.html"),
        styles: [__webpack_require__("./src/app/notifications/notification-page.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__core_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_auth_service__["a" /* AuthService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5__core_web_socket_service__["b" /* WebSocketService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_web_socket_service__["b" /* WebSocketService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* Router */]) === "function" && _e || Object])
], NotificationPageComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=notification-page.component.js.map

/***/ }),

/***/ "./src/app/notifications/notifications.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/notifications/notifications.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"d-inline-block\">\n  <div class=\"dropdown show\" style=\"position: relative;\">\n\n    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n      <li class=\"noti\">\n        <img src=\"https://s3.amazonaws.com/bellassets/assets/images/notification-icon.png\"/>\n        <span class=\"notificationspan\" *ngIf=\"unread_count > 0\"  >{{unread_count}}</span>\n      </li>\n    </a>\n\n    <div class=\"dropdown-menu notification-menu\" aria-labelledby=\"dropdownMenuLink\">\n      <div class=\"dropdown-caret\">\n        <div class=\"caret-outer\"></div>\n        <div class=\"caret-inner\"></div>\n      </div>\n      <div class=\"notification-bar clearfix\">\n        <span [routerLink]=\"['/notifications']\" class=\"notification-menu-header\" title=\"Notifications\" translate>\n          Notifications\n        </span>\n        <a href=\"#\" (click)=\"markAllRead();false\" class=\"mark-all-as-read\" translate>Mark all as read</a>\n      </div>\n\n      <div class=\"notification-scroll\">\n        <span  *ngFor=\"let notification of notification_list\">\n            <a class=\"dropdown-item notification-list-item clearfix\" [ngClass]=\"{'unread': notification.unread}\" href=\"#\">\n              <span class=\"circlephoto notifavatar\"\n                    (click)=\"handleClickAvatar($event, notification);false\"\n                    [innerHTML]=\"notification.actor_thumbnail\"></span>\n\n              <div class=\"notification-item-right vertical-list\" (click)=\"goToAction(notification);false\">\n                <span class=\"notify-text vertical-list-item\"><span class=\"bold\">{{notification.actor}}</span>{{notification.verb}}</span>\n\n                <div class=\"vertical-list-item clearfix\">\n                  <!--<button *ngIf=\"notification.actor_user && !notification.actor_user.is_following\"-->\n                          <!--class=\"btn-follow\" (click)=\"followUser($event, notification.actor_user);false\" translate>-->\n                    <!--Follow-->\n                  <!--</button>-->\n                </div>\n              </div>\n\n            </a>\n        </span>\n      </div>\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/notifications/notifications.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NotificationsComponent = (function () {
    function NotificationsComponent(apiService, router) {
        this.apiService = apiService;
        this.router = router;
        this.unread_count = 0;
        this.notification_list = [];
        this.csrf = "";
    }
    NotificationsComponent.prototype.ngOnInit = function () {
        this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
    };
    NotificationsComponent.prototype.markAllRead = function () {
        var _this = this;
        this.apiService.markAllRead().then(function (result) {
            _this.apiService.getUnreadNotifications().then(function (result) {
                _this.unread_count = result.unread_count;
                _this.notification_list = result.notifications_list;
            });
        });
    };
    NotificationsComponent.prototype.goToAction = function (notification) {
        var _this = this;
        this.apiService.markItRead(notification).then(function (result) {
            if (notification.unread == true) {
                notification.unread = false;
                _this.unread_count = _this.unread_count - 1;
            }
        });
        if (notification.action == "comment" || notification.action == "bella")
            this.router.navigate(['/post', notification.username, notification.action_id]);
        else if (notification.action == "user") {
            this.router.navigate(['/user', notification.username]);
        }
    };
    NotificationsComponent.prototype.handleClickAvatar = function (event, notification) {
        this.router.navigate(['user/', notification.actor]);
    };
    NotificationsComponent.prototype.followUser = function (event, user) {
        event.preventDefault();
        event.stopPropagation();
        this.apiService.follow(user, this.csrf).then(function (listResp) {
            window.location.reload();
        });
    };
    return NotificationsComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], NotificationsComponent.prototype, "unread_count", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], NotificationsComponent.prototype, "notification_list", void 0);
NotificationsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-notifications',
        template: __webpack_require__("./src/app/notifications/notifications.component.html"),
        styles: [__webpack_require__("./src/app/notifications/notifications.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* Router */]) === "function" && _b || Object])
], NotificationsComponent);

var _a, _b;
//# sourceMappingURL=notifications.component.js.map

/***/ }),

/***/ "./src/app/password-reset-page/password-reset-confirm-page.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"user-profile-contain\">\n  <div class=\"mid-con\">\n    <div class=\"container\">\n\n      <div class=\"row\">\n        <div class=\"col-sm-2 col-lg-2 col-md-2\">\n\n        </div>\n        <div class=\"col-sm-8 col-lg-8 col-md-8 content-body\" *ngIf=\"status=='loading'\">\n          <legend>Loading...</legend>\n        </div>\n        <div class=\"col-sm-8 col-lg-8 col-md-8 content-body\" *ngIf=\"status=='success'\">\n          <legend>Change password</legend>\n\n          <form method=\"post\" id=\"reset_password_confirm_form\">\n            <div class=\"form-group\">\n              <label for=\"id_new_password\">New Pasword:</label>\n              <input id=\"id_new_password\" class=\"form-control\" maxlength=\"254\" name=\"new_password\" type=\"password\"\n                     [(ngModel)]=\"password_new\">\n            </div>\n            <div class=\"form-group\">\n              <label for=\"id_new_password_confirm\">New Pasword Confirmation:</label>\n              <input id=\"id_new_password_confirm\" class=\"form-control\" maxlength=\"254\" name=\"new_password_confirm\"\n                     type=\"password\" [(ngModel)]=\"password_new_confirm\">\n            </div>\n            <div class=\"form-group\">\n              <button class=\"btn btn-primary\" type=\"submit\"\n                      (click)=\"$event.preventDefault();reset_password_confirm_form();\">\n                Change password\n              </button>\n            </div>\n          </form>\n        </div>\n        <div class=\"col-sm-8 col-lg-8 col-md-8 content-body\" *ngIf=\"status=='incorrect'\">\n          <p>\n            The password reset link was invalid, possibly because it has already been used. Please request a new\n            password reset.\n          </p>\n        </div>\n        <div class=\"col-sm-8 col-lg-8 col-md-8 content-body\" *ngIf=\"status=='done'\">\n          <p>\n            Your password has been set. You may go ahead and <a href=\"#\" [routerLink]=\"['']\">sign in</a> now.\n          </p></div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/password-reset-page/password-reset-page.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/password-reset-page/password-reset-page.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"user-profile-contain\">\n  <div class=\"alert fade in {{ message_type }}\" *ngIf=\"alert_message\">\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n    {{ alert_message }}\n  </div>\n  <div class=\"mid-con\">\n    <div class=\"container\">\n\n      <div class=\"row\">\n        <div class=\"col-sm-2 col-lg-2 col-md-2\">\n\n        </div>\n        <div class=\"col-sm-8 col-lg-8 col-md-8 content-body\">\n          <legend>Forgot password</legend>\n\n          <form method=\"post\" id=\"reset_password_form\">\n            <div class=\"form-group\">\n              <label for=\"id_email\">Email:</label>\n              <input id=\"id_email\" class=\"form-control\" maxlength=\"254\" name=\"email\" type=\"email\" [(ngModel)]=\"email\">\n            </div>\n            <div class=\"form-group\">\n              <button class=\"btn btn-primary\" type=\"submit\" (click)=\"$event.preventDefault();reset_password_form();\">Submit</button>\n            </div>\n          </form>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/password-reset-page/password-reset-page.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PasswordResetPageComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordResetConfirmPageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PasswordResetPageComponent = (function () {
    function PasswordResetPageComponent(belaAPIService, router, authService) {
        this.belaAPIService = belaAPIService;
        this.router = router;
        this.authService = authService;
        this.email = "";
        this.alert_message = "";
        this.message_type = "";
        var userName = jQuery('input[name="requesting_user_username"]').val();
        if (userName) {
            this.router.navigate(['/feed']);
        }
    }
    PasswordResetPageComponent.prototype.ngOnInit = function () {
        jQuery("#reset_password_form").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                }
            }
        });
    };
    PasswordResetPageComponent.prototype.ngOnDestroy = function () {
    };
    PasswordResetPageComponent.prototype.reset_password_form = function () {
        var _this = this;
        if (jQuery("#reset_password_form").valid()) {
            this.belaAPIService.reset_password(this.email).then(function (res) {
                if (res["status"] == "success") {
                    _this.alert_message = res["message"];
                    _this.message_type = "alert-info";
                    jQuery("html, body").animate({ scrollTop: 0 }, "slow");
                }
                else {
                    _this.alert_message = res["message"];
                    _this.message_type = "alert-danger";
                    jQuery("html, body").animate({ scrollTop: 0 }, "slow");
                }
            });
        }
        else {
        }
    };
    return PasswordResetPageComponent;
}());
PasswordResetPageComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-password-reset-page',
        template: __webpack_require__("./src/app/password-reset-page/password-reset-page.component.html"),
        styles: [__webpack_require__("./src/app/password-reset-page/password-reset-page.component.css")],
        providers: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__core_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_auth_service__["a" /* AuthService */]) === "function" && _c || Object])
], PasswordResetPageComponent);

var PasswordResetConfirmPageComponent = (function () {
    function PasswordResetConfirmPageComponent(belaAPIService, router, route, authService) {
        this.belaAPIService = belaAPIService;
        this.router = router;
        this.route = route;
        this.authService = authService;
        this.password_new = "";
        this.password_new_confirm = "";
        this.status = "loading";
        this.uidb64 = "";
        this.token = "";
        var userName = jQuery('input[name="requesting_user_username"]').val();
        if (userName) {
            this.router.navigate(['/feed']);
        }
    }
    PasswordResetConfirmPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.belaAPIService.check_reset_password_link(params['uidb64'], params['token']).then(function (res) {
                _this.uidb64 = params['uidb64'];
                _this.token = params['token'];
                _this.status = res["status"];
                jQuery("html, body").animate({ scrollTop: 0 }, "slow");
            });
        });
    };
    PasswordResetConfirmPageComponent.prototype.ngOnDestroy = function () {
    };
    PasswordResetConfirmPageComponent.prototype.reset_password_confirm_form = function () {
        var _this = this;
        jQuery("#reset_password_confirm_form").validate({
            rules: {
                new_password: "required",
                new_password_confirm: {
                    required: true,
                    equalTo: "#id_new_password"
                }
            }
        });
        if (jQuery("#reset_password_confirm_form").valid()) {
            this.belaAPIService.reset_password_confirm(this.password_new, this.password_new_confirm, this.uidb64, this.token).then(function (res) {
                _this.status = res["status"];
                jQuery("html, body").animate({ scrollTop: 0 }, "slow");
            });
        }
    };
    return PasswordResetConfirmPageComponent;
}());
PasswordResetConfirmPageComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-password-reset-confirm-page',
        template: __webpack_require__("./src/app/password-reset-page/password-reset-confirm-page.component.html"),
        styles: [__webpack_require__("./src/app/password-reset-page/password-reset-page.component.css")],
        providers: []
    }),
    __metadata("design:paramtypes", [typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* Router */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_3__core_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_auth_service__["a" /* AuthService */]) === "function" && _g || Object])
], PasswordResetConfirmPageComponent);

var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=password-reset-page.component.js.map

/***/ }),

/***/ "./src/app/post-box/post-box.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/post-box/post-box.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"white-box whiteboxpost\">\n\n  <div class=\"user-post\">\n\n    <div class=\"user-post-left\">\n      <a href=\"#\" [routerLink]=\"['/user/', thot.user.username]\" class=\"post-avatar circlephoto\" >\n        <span class=\"avatar-wrapper\" [innerHTML]=\"thot.user.avatar\"></span>\n      </a>\n\n      <div class=\"post-content\">\n        <h4 class=\"post-user-info\">\n          <a class=\"postusername\" [routerLink]=\"['/user/', thot.user.username]\">\n            <b>{{ thot.user.username }}</b>\n          </a>\n          <a class=\"postlocation\" *ngIf=\"thot.location && !thot.editableLocation\">\n            <i class=\"fas fa-map-marker-alt\"></i> {{thot.location}}\n          </a>\n          <a class=\"h5 posttime\" href=\"#\" [routerLink]=\"['/post/', thot.user.username, thot.id]\">\n            {{ thot.created_at|messageTime }}\n          </a>\n        </h4>\n      </div>\n\n    </div>\n\n    <div class=\"user-post-right\">\n      <a class=\"dropdown-toggle image-btn\"  data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n        <img src=\"https://s3.amazonaws.com/bellassets/assets/images/doted-icon.png\"/>\n      </a>\n      <div class=\"dropdown-menu deletereport-menu\">\n        <div class=\"dropdown-caret\">\n          <div class=\"caret-outer\"></div>\n          <div class=\"caret-inner\"></div>\n        </div>\n\n        <!-- Follow button section start -->\n        <ng-container *ngIf=\"user.id && (thot.user.id!=user.id)\">\n          <a class=\"\" href=\"#\" *ngIf=\"!thot.user.is_following\" (click)=\"followUser();false\" translate>Follow</a>\n          <a class=\"\" href=\"#\" *ngIf=\"thot.user.is_following\" (click)=\"unfollowUser();false\" translate>Unfollow</a>\n        </ng-container>\n        <!-- Follow button section end -->\n\n\n        <a onclick=\"copyLink()\"><li id=\"repoFolder\" [attr.value]=\"['https://www.belacam.com/b/post/' + thot.user.username + '/' + thot.id]\">Copy Link</li></a>\n\n        <a class=\"\" target=\"_blank\" [attr.href]=\"['https://twitter.com/home?status=https://www.belacam.com/n/post/' +  thot.user.username +'/' + thot.id]\" translate>Share on Twitter</a>\n\n        <a class=\"\" href=\"#\" *ngIf=\"user.id && (thot.user.id==user.id)\" (click)=\"editable(true);false\" translate>Edit Caption</a>\n        <a class=\"\" href=\"#\" *ngIf=\"user.id && (thot.user.id==user.id)\" (click)=\"editableLocation(true);false\" translate>Edit Location</a>\n        <a class=\"\" href=\"#\" *ngIf=\"user.id && (thot.user.id==user.id)\" (click)=\"delete();false\" translate>Delete</a>\n\n        <a class=\"\" href=\"#\" *ngIf=\"user.is_superuser\" (click)=\"deleteAdmin();false\" translate>Admin Delete</a>\n\n        <a class=\"\" href=\"#\" *ngIf=\"user.id && (thot.user.id!=user.id)\" (click)=\"report();false\" translate>Report</a>\n        <a class=\"\" href=\"#\" *ngIf=\"user.id && (thot.user.id!=user.id)\" (click)=\"blockUser();false\" translate>Block User</a>\n      </div>\n    </div>\n\n\n    <div *ngIf=\"thot.editableLocation\" style=\"float: left; width: 100%; margin-bottom: 15px;\">\n      <textarea class=\"caption-edit-box\" [(ngModel)]=\"thot.tempLocation\"></textarea>\n      <input class=\"savebutton\" (click)=\"updateLocation();false\" type=\"button\" value=\"Save\">\n      <input class=\"cancelbutton\" (click)=\"editableLocation(false);false\" type=\"button\" value=\"Cancel\">\n    </div>\n\n  </div>\n\n  <div class=\"post-img-container\">\n    <app-insufficient-bela #insufficientMessage [user]=\"user\"></app-insufficient-bela>\n\n    <a href=\"#\" (click)=\"openThotBox();false\">\n      <img class=\"post-img\" [src]=\"thot.image\"/>\n      <span class=\"full-screen-btn\">\n        <i class=\"fa fa-expand\" aria-hidden=\"true\"></i>\n      </span>\n    </a>\n  </div>\n\n  <ul class=\"like-box\">\n\n    <span *ngIf=\"user.id === undefined\" onclick=\"loginFirst()\">\n\n      <li class=\"heart\" [class.unlikedheart]=\"!thot.is_liked\" [class.likedheart]=\"thot.is_liked\">\n        <a class=\"\" href=\"#\" (click)=\"like();false\">\n\n          <img class=\"grayheart\"\n               src=\"https://s3.amazonaws.com/bellassets/assets/images/grayheart.png\"\n               [alt]=\"'Give some Bela'|translate\"/>\n\n          <img class=\"redheart\"\n               src=\"https://s3.amazonaws.com/bellassets/assets/images/redheart.png\"\n               [alt]=\"'Give some Bela'|translate\"/>\n\n        </a>\n        <a href=\"#\" data-toggle=\"modal\" [attr.data-target]=\"'#likesbox' + thot.id\">\n          {{ thot.total_likes_count }}\n        </a>\n\n        <span class=\"viewscount\">\n          <i class=\"far fa-eye\"></i><b> {{thot.views.length}}</b>\n        </span>\n      </li>\n\n    </span>\n\n\n    <span *ngIf=\"user.id !== undefined\">\n\n      <li class=\"heart\" [class.unlikedheart]=\"!thot.is_liked\" [class.likedheart]=\"thot.is_liked\">\n        <a class=\"\" href=\"#\" (click)=\"like();false\">\n\n          <img class=\"grayheart\"\n               src=\"https://s3.amazonaws.com/bellassets/assets/images/grayheart.png\"\n               [alt]=\"'Give some Bela'|translate\"/>\n\n          <img class=\"redheart\"\n               src=\"https://s3.amazonaws.com/bellassets/assets/images/redheart.png\"\n               [alt]=\"'Give some Bela'|translate\"/>\n\n\n        </a>\n        <a href=\"#\" data-toggle=\"modal\" [attr.data-target]=\"'#likesbox' + thot.id\">\n          {{ thot.total_likes_count }}\n        </a>\n\n        <span class=\"viewscount\">\n          <i class=\"far fa-eye\"></i><b> {{thot.views.length}}</b>\n        </span>\n      </li>\n\n    </span>\n\n    <li class=\"postearnings\">\n      <img src=\"https://s3.amazonaws.com/bellassets/assets/images/orangebela.png\"> {{ thot.belas |number:'1.2-2' }}<br>\n      <i class=\"fas fa-dollar-sign\"></i> {{ thot.earnings|number:'1.2-2' }}\n    </li>\n\n    <app-likes-modal-wrapper [thot]=\"thot\" [id]=\"'likesbox' + thot.id\" [user]=\"user\"></app-likes-modal-wrapper>\n\n  </ul>\n\n  <p class=\"caption\" *ngIf=\"!thot.editable\" style=\"word-wrap: break-word;\" ><a class=\"captionusername\" href=\"#\" [routerLink]=\"['/user/', thot.user.username]\">{{ thot.user.username }}</a>\n    <app-dynamic-html class=\"captioncontent\" [innerHTML]=\"thot.content|tagify\"></app-dynamic-html>\n  </p>\n\n  <div *ngIf=\"thot.editable\" style=\"float: left; width: 100%; margin-bottom: 15px;\">\n    <textarea class=\"caption-edit-box\" [(ngModel)]=\"thot.tempContent\"></textarea>\n    <input class=\"savebutton\" (click)=\"update();false\" type=\"button\" value=\"Save\">\n    <input class=\"cancelbutton\" (click)=\"editable(false);false\" type=\"button\" value=\"Cancel\">\n  </div>\n\n  <div class=\"comment-boxes\">\n\n    <app-comment [thot]=\"thot\"></app-comment>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/post-box/post-box.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PostBoxComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_event_service__ = __webpack_require__("./src/app/core/event.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__insufficient_bela_insufficient_bela_component__ = __webpack_require__("./src/app/insufficient-bela/insufficient-bela.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_event_types__ = __webpack_require__("./src/app/core/event.types.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PostBoxComponent = (function () {
    function PostBoxComponent(apiService, eventService, authService) {
        this.apiService = apiService;
        this.eventService = eventService;
        this.authService = authService;
        this.deleteEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.openThotBoxEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.csrf = "";
    }
    PostBoxComponent.prototype.ngOnInit = function () {
        this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
    };
    PostBoxComponent.prototype.like = function () {
        var _this = this;
        var post = this.thot;
        if (!post.is_liking && !post.is_liked && post.user.id !== this.user.id) {
            post.is_liking = true;
            post.is_liked = true;
            this.apiService.like(post).then(function (res) {
                if (res.error === __WEBPACK_IMPORTED_MODULE_1__app_model__["b" /* LikeBelaResult */].E_INSUFFICIENT_COIN) {
                    _this._insufficientMessage.show();
                }
                else {
                    var result = res.result;
                    if (result.bela) {
                        post.likes.push(result.bela);
                        post.belas = result.total_bela;
                        post.earnings = result.total_earning;
                        post.is_liked = result.liked;
                        post.is_liking = false;
                        post.total_likes_count = result.total_likes;
                    }
                    if (result.user) {
                        _this.user.user1 = result.user;
                        _this.authService.updateUser(_this.user);
                        _this.eventService.fireEvent({
                            type: __WEBPACK_IMPORTED_MODULE_6__core_event_types__["b" /* BelaEventType */].LIKE_POST,
                            value: _this.user
                        });
                    }
                }
            }).catch(function () {
                post.is_liked = false;
                post.is_liking = false;
            });
        }
    };
    PostBoxComponent.prototype.delete = function () {
        var _this = this;
        this.apiService.deleteFeed(this.thot.id).then(function () {
            _this.deleteEvent.emit(_this.thot);
        });
    };
    PostBoxComponent.prototype.deleteAdmin = function () {
        var _this = this;
        this.apiService.deleteAdminFeed(this.thot.id).then(function () {
            _this.deleteEvent.emit(_this.thot);
        }).catch(function () {
            alert("Can't delete the post. Try again later");
        });
    };
    PostBoxComponent.prototype.report = function () {
        var _this = this;
        this.apiService.report(this.thot.id).then(function () {
            _this.deleteEvent.emit(_this.thot);
        });
    };
    PostBoxComponent.prototype.blockUser = function () {
        this.apiService.blockUser(this.thot.user.id).then(function (listResp) {
            window.location.reload();
        });
    };
    PostBoxComponent.prototype.followUser = function () {
        this.apiService.follow(this.thot.user, this.csrf).then(function (listResp) {
            window.location.reload();
        });
    };
    PostBoxComponent.prototype.unfollowUser = function () {
        this.apiService.unfollow(this.thot.user, this.csrf).then(function (listResp) {
            window.location.reload();
        });
    };
    PostBoxComponent.prototype.editable = function (state) {
        this.thot.editable = state;
        if (state) {
            this.thot.tempContent = this.thot.content;
        }
        else {
            this.thot.tempContent = '';
        }
    };
    PostBoxComponent.prototype.editableLocation = function (state) {
        this.thot.editableLocation = state;
        if (state) {
            this.thot.tempLocation = this.thot.location;
        }
        else {
            this.thot.tempLocation = '';
        }
    };
    PostBoxComponent.prototype.update = function () {
        var _this = this;
        this.apiService.updateCaption(this.thot.id, this.thot.tempContent).then(function (result) {
            if (result['status'] == "success") {
                _this.thot.content = result['result'];
                _this.thot.editable = false;
            }
            else {
                alert("Something went wrong. Please try again later!");
            }
        });
    };
    PostBoxComponent.prototype.updateLocation = function () {
        var _this = this;
        this.apiService.updateLocation(this.thot.id, this.thot.tempLocation).then(function (result) {
            if (result['status'] == "success") {
                _this.thot.location = result['result'];
                _this.thot.editableLocation = false;
            }
            else {
                alert("Something went wrong. Please try again later!");
            }
        });
    };
    PostBoxComponent.prototype.openThotBox = function () {
        this.openThotBoxEvent.emit(this.index);
    };
    Object.defineProperty(PostBoxComponent.prototype, "mapLink", {
        get: function () {
            var place = this.thot.location;
            if (place) {
                return null;
            }
            else {
                return null;
            }
        },
        enumerable: true,
        configurable: true
    });
    return PostBoxComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__app_model__["g" /* Thought */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__app_model__["g" /* Thought */]) === "function" && _a || Object)
], PostBoxComponent.prototype, "thot", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]) === "function" && _b || Object)
], PostBoxComponent.prototype, "user", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Number)
], PostBoxComponent.prototype, "index", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('delete'),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _c || Object)
], PostBoxComponent.prototype, "deleteEvent", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('openThotBox'),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _d || Object)
], PostBoxComponent.prototype, "openThotBoxEvent", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('insufficientMessage'),
    __metadata("design:type", typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__insufficient_bela_insufficient_bela_component__["a" /* InsufficientBelaComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__insufficient_bela_insufficient_bela_component__["a" /* InsufficientBelaComponent */]) === "function" && _e || Object)
], PostBoxComponent.prototype, "_insufficientMessage", void 0);
PostBoxComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-post-box',
        template: __webpack_require__("./src/app/post-box/post-box.component.html"),
        styles: [__webpack_require__("./src/app/post-box/post-box.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_4__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_2__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_event_service__["a" /* EventService */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_3__core_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_auth_service__["a" /* AuthService */]) === "function" && _h || Object])
], PostBoxComponent);

var _a, _b, _c, _d, _e, _f, _g, _h;
//# sourceMappingURL=post-box.component.js.map

/***/ }),

/***/ "./src/app/posting-form/header-posting-form.component.html":
/***/ (function(module, exports) {

module.exports = "<form enctype=\"multipart/form-data\" (ngSubmit)=\"postThought($event);false\" class=\"post-form dropzone\" method=\"post\" id=\"header_post_form\" action=\"\">\n  <div class=\"panel-body\">\n    <div class=\" fade in active\" id=\"tab2default\" >\n      <div class=\"fild\">\n        <span class=\"uploadavatar\"> <span [innerHTML]=\"requesting_user.avatar\"></span></span>\n        <textarea\n          typeahead\n          col=\"80\"\n          #headerContent\n          rows=\"2\"\n          name=\"content\"\n          class=\"tag autoExpand\"\n          [placeholder]=\"'Upload and caption your photo.'|translate\"\n        ></textarea>\n\n\n        <div class=\"previewdiv\">\n          <div class=\"row imagetools\" *ngIf=\"showHeaderPreview && !showHeaderGifPreview\">\n            <a (click)=\"setCrop();false\"><i class=\"fas fa-crop\" [ngClass]=\"{'fa-selected': enableCrop}\"></i></a>\n            <a (click)=\"rotate();false\"><i class=\"fas fa-circle-notch selected\"></i></a>\n            <a (click)=\"toggleFilterPreview(); isPreviewApplied()\" *ngIf=\"!enableCrop\"><i class=\"fas fa-palette\"></i></a>\n          </div>\n\n          <a id=\"removeimageHeader\"\n             *ngIf=\"showHeaderPreview\"\n             (click)=\"clearHeaderPreview();false\"\n             class=\"removeimage\">\n            <img src=\"https://s3.amazonaws.com/bellassets/assets/images/remove.png\" [alt]=\"'Remove Belacam Photo'|translate\" />\n          </a>\n          <img id=\"headerpreviewgifimage\" *ngIf=\"showHeaderPreview && (showHeaderGifPreview || !enableCrop)\" [src]=\"headerPreviewImg\" [alt]=\"'Belacam User Uploaded Media'|translate\"/>\n          <img-cropper id=\"headerpreviewimage\" [style.display]=\"showHeaderPreview && !showHeaderGifPreview && enableCrop ? 'block' : 'none' \" #cropper [image]=\"data\" [settings]=\"cropperSettings\" [alt]=\"'Belacam User Uploaded Media'|translate\"></img-cropper>\n        </div>\n        <!-- START FILTER -->\n        <div class=\"filterPreview\" *ngIf=\"headerEnableFilter && (showHeaderPreview && !enableCrop)\">\n            <div class=\"filterImagePreview\" (click)=\"setOriginal()\">\n            <img id=\"headerpreviewgifimage\" *ngIf=\"showHeaderPreview && (showHeaderGifPreview || !enableCrop)\" [src]=\"headerFilterImgPreview\" [alt]=\"'Belacam User Uploaded Media'|translate\"/>\n            <h4 class=\"filterDesc\">Original</h4>\n            </div>\n            <div class=\"filterImagePreview\" (click)=\"setSaturated()\">\n            <img  id=\"headerpreviewgifimage\" *ngIf=\"showHeaderPreview && (showHeaderGifPreview || !enableCrop)\" [src]=\"headerSaturatedPreview\" [alt]=\"'Belacam User Uploaded Media'|translate\"/>\n            <h4 class=\"filterDesc\" >Saturated</h4>\n            </div>\n            <div class=\"filterImagePreview\" (click)=\"setGrayscale()\">\n            <img class=\"imageBW\" id=\"headerpreviewgifimage\" *ngIf=\"showHeaderPreview && (showHeaderGifPreview || !enableCrop)\" [src]=\"headerFilterImgPreview\" [alt]=\"'Belacam User Uploaded Media'|translate\"/>\n            <h4 class=\"filterDesc\">Grayscale</h4>\n            </div>\n          </div>\n        <!-- END FILTER -->\n        <div class=\"geo-picker\" *ngIf=\"isPickerVisible\">\n          <p class=\"geo-picker-btn\">\n            <i class=\"fas fa-map-marker-alt\"></i>\n            <span class=\"\">{{isLoadingLocation ? \"Getting Location...\" : (curLocation && curLocation.location ? curLocation.location: \"Can not find your location.\")}}</span>\n          </p>\n\n        </div>\n      </div>\n      <div>\n        <ngx-loading [show]=\"loading\"></ngx-loading>\n      </div>\n    </div>\n  </div>\n  <ul class=\"icon-box\">\n    <li>\n      <label data-toggle=\"tooltip\" [title]=\"'Post your photo'|translate\" class=\"btn-upload\" id=\"uploadlabelHeader\" for=\"file-input-31\">\n        <span>\n          <a class=\"btn icon-btn\">\n            <i class=\"fas fa-camera\"></i>\n            <input type=\"file\" (change)=\"headerThoughtPicChange($event)\" name=\"image\" #headerThoughtFile class=\"hidden\" id=\"file-input-31\" accept=\"image/gif, image/jpeg, image/png\">\n          </a>\n        </span>\n      </label>\n    </li>\n    <li>\n      <label data-toggle=\"tooltip\" [title]=\"'Add location'|translate\" class=\"btn-location\">\n        <a class=\"btn icon-btn\" (click)=\"toogleGeoPicker()\" *ngIf=\"!isPickerVisible\">\n          <i class=\"fas fa-map-marker-alt\"></i>\n        </a>\n\n        <a class=\"btn icon-btn active\"\n           (click)=\"toogleGeoPicker()\"\n           *ngIf=\"isPickerVisible\">\n          <i class=\"fas fa-sync-alt\"></i>\n        </a>\n\n      </label>\n    </li>\n    <li> <button class=\"btn-submit-post\" type=\"submit\" translate>Post</button> </li>\n\n  </ul>\n</form>\n"

/***/ }),

/***/ "./src/app/posting-form/posting-form.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/posting-form/posting-form.component.html":
/***/ (function(module, exports) {

module.exports = "<form enctype=\"multipart/form-data\" (ngSubmit)=\"postThought($event);false\" class=\"post-form dropzone\" method=\"post\" id=\"post_form\" action=\"\">\n  <div class=\"panel-body\" >\n    <div class=\" fade in active\" id=\"tab2default\" >\n      <div class=\"fild\">\n        <span class=\"uploadavatar\"> <span [innerHTML]=\"requesting_user.avatar\"></span></span>\n        <textarea\n          typeahead\n          col=\"80\"\n          #content\n          rows=\"2\"\n          onfocus=\"inputFocus()\"\n          onfocusout=\"inputFocusOut()\"\n          id=\"postingtextarea\"\n          name=\"content\"\n          class=\"tag autoExpand\"\n          [placeholder]=\"'Upload and caption your photo.'|translate\"\n        ></textarea>\n\n        <div class=\"previewdiv\">\n\n            <div class=\"row imagetools\" *ngIf=\"showPreview && !showGifPreview\">\n              <a (click)=\"setCrop();false\"><i class=\"fas fa-crop\" [ngClass]=\"{'fa-selected': enableCrop}\"></i></a>\n              <a (click)=\"rotate();false\"><i class=\"fas fa-sync-alt selected\"></i></a>\n              <a (click)=\"toggleFilterPreview(); isPreviewApplied()\" *ngIf=\"!enableCrop\"><i class=\"fas fa-palette\"></i></a>\n             \n            </div>\n\n          <a *ngIf=\"showPreview || showVideoPreview\" (click)=\"clearPreview();false\" id=\"removeimage\" class=\"removeimage\">\n            <img src=\"https://s3.amazonaws.com/bellassets/assets/images/remove.png\" [alt]=\"'Remove Belacam Photo'|translate\" />\n          </a>\n          \n          <!-- VIDEO PREVIEW -->\n          <video controls *ngIf=\"showVideoPreview\" id=\"previewvideo\" width=\"520\"\n          height=\"400\">\n              <source [src]=\"videoPreview\" type=\"video/mp4\">\n              \n            </video>\n          <!-- END VIDEO PREVIEW -->\n\n          <img id=\"previewgifimage\" *ngIf=\"showPreview && (showGifPreview || !enableCrop)\" [src]=\"previewImg\" [alt]=\"'Belacam User Uploaded Media'|translate\"/>\n\n          <img-cropper id=\"previewimage\" [style.display]=\"showPreview && !showGifPreview && enableCrop ? 'block' : 'none' \" #cropper [image]=\"data\" [settings]=\"cropperSettings\" [alt]=\"'Belacam User Uploaded Media'|translate\"></img-cropper>\n       \n        </div>\n         <!-- FILTER PREVIEW -->\n        <div class=\"filterPreview\" *ngIf=\"enableFilter && (showPreview && !enableCrop)\">\n            <div class=\"filterImagePreview\" (click)=\"setOriginal()\">\n            <img id=\"previewgifimage\" *ngIf=\"showPreview && (showGifPreview || !enableCrop)\" [src]=\"filterImgPreview\" [alt]=\"'Belacam User Uploaded Media'|translate\"/>\n            <h4 class=\"filterDesc\">Original</h4>\n            </div>\n            <div class=\"filterImagePreview\" (click)=\"setSaturated()\">\n            <img  id=\"previewgifimage\" *ngIf=\"showPreview && (showGifPreview || !enableCrop)\" [src]=\"saturatedPreview\" [alt]=\"'Belacam User Uploaded Media'|translate\"/>\n            <h4 class=\"filterDesc\" >Saturated</h4>\n            </div>\n            <div class=\"filterImagePreview\" (click)=\"setGrayscale()\">\n            <img class=\"imageBW\" id=\"previewgifimage\" *ngIf=\"showPreview && (showGifPreview || !enableCrop)\" [src]=\"filterImgPreview\" [alt]=\"'Belacam User Uploaded Media'|translate\"/>\n            <h4 class=\"filterDesc\">Grayscale</h4>\n            </div>\n          </div>\n        <!-- END FILTER PREVIEW -->\n\n\n        <div class=\"geo-picker\" *ngIf=\"isPickerVisible\">\n          <p class=\"geo-picker-btn\">\n            <i class=\"fas fa-map-marker-alt\"></i>\n            <span class=\"\">{{isLoadingLocation ? \"Getting Location...\" : (curLocation && curLocation.location ? curLocation.location: \"Can not find your location.\")}}</span>\n          </p>\n\n        </div>\n      </div>\n      <div>\n        <ngx-loading [show]=\"loading\"></ngx-loading>\n      </div>\n    </div>\n  </div>\n  <ul class=\"icon-box\">\n    <li>\n      <label data-toggle=\"tooltip\" [title]=\"'Post your photo'|translate\" class=\"btn-upload\" id=\"uploadlabel\" for=\"file-input-3\">\n        <a class=\"btn icon-btn transitiontime\" id=\"posting-form-camera\">\n          <i class=\"fas fa-camera\"></i>\n          <input type=\"file\" (change)=\"thoughtPicChange($event)\" name=\"image\" #thoughtFile class=\"hidden\" id=\"file-input-3\" accept=\"image/gif, image/jpeg, image/png\">\n        </a>\n      </label>\n    </li>\n    <li>\n      <label data-toggle=\"tooltip\" [title]=\"'Post your video'|translate\" class=\"btn-upload\" id=\"uploadlabel\">\n        <a class=\"btn icon-btn transitiontime\" id=\"posting-form-camera\">\n            <i class=\"fas fa-video\"></i>\n            <input type=\"file\" (change)=\"thoughtVidUpload($event)\" name=\"video\" #thoughtFile class=\"hidden\" id=\"file-input-3\" accept=\".mp4\">\n        </a>\n      </label>\n    </li>\n    <li>\n      <label data-toggle=\"tooltip\" [title]=\"'Add location'|translate\" class=\"btn-location\">\n        <a class=\"btn icon-btn transitiontime\" id=\"posting-form-location\" (click)=\"toogleGeoPicker()\" *ngIf=\"!isPickerVisible\">\n          <i class=\"fas fa-map-marker-alt\"></i>\n        </a>\n\n        <a class=\"btn icon-btn active\"\n           (click)=\"toogleGeoPicker()\"\n           *ngIf=\"isPickerVisible\">\n          <i class=\"fas fa-map-marker-alt\"></i>\n        </a>\n\n      </label>\n    </li>\n    <li class=\"pull-right\">\n      <button type=\"submit\" class=\"btn-submit-post\" translate>Post</button>\n    </li>\n  </ul>\n</form>\n"

/***/ }),

/***/ "./src/app/posting-form/posting-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PostingFormComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderPostingFormComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__typeahead_typeahead_component__ = __webpack_require__("./src/app/typeahead/typeahead.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_img_cropper__ = __webpack_require__("./node_modules/ng2-img-cropper/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_event_service__ = __webpack_require__("./src/app/core/event.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_awss3_api_awss3_api_service__ = __webpack_require__("./src/app/core/awss3-api/awss3-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__core_event_types__ = __webpack_require__("./src/app/core/event.types.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var PostingFormComponent = (function () {
    function PostingFormComponent(belaAPIService, s3APIService, eventService) {
        this.belaAPIService = belaAPIService;
        this.s3APIService = s3APIService;
        this.eventService = eventService;
        this.requesting_user = new __WEBPACK_IMPORTED_MODULE_2__app_model__["h" /* User */]();
        this.loading = false;
        this.showPreview = false;
        this.showGifPreview = false;
        this.csrf = "";
        this.isLoadingLocation = false;
        this.isSearchingQuery = false;
        this.locations = {
            topSearches: [],
            searchResult: []
        };
        this.delayTimer = null;
        this.curLocation = null;
        this.isPickerVisible = false;
        this.enableCrop = false;
        //FILTER VARIABLES
        this.enableFilter = false;
        this.appliedPreview = 0;
        this.showVideoPreview = false;
        this.cropperSettings = new __WEBPACK_IMPORTED_MODULE_5_ng2_img_cropper__["b" /* CropperSettings */]();
        this.cropperSettings.canvasWidth = 200;
        this.cropperSettings.canvasHeight = 200;
        this.cropperSettings.croppedWidth = 1000;
        this.cropperSettings.minWidth = 200;
        this.cropperSettings.minHeight = 200;
        this.cropperSettings.noFileInput = true;
        this.cropperSettings.preserveSize = true;
        this.cropperSettings.keepAspect = false;
        this.cropperSettings.dynamicSizing = true;
        this.cropperSettings.cropperClass = "previewimage_canvas";
        this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,0,0,1)';
        this.cropperSettings.touchRadius = 50;
        this.data = {};
    }
    PostingFormComponent.prototype.ngOnInit = function () {
        this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
    };
    PostingFormComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.cropper.cropper.resizeCanvas(jQuery('#post_form .fild').width(), 400, true);
        jQuery(window).resize(function () {
            _this.cropper.cropper.resizeCanvas(jQuery('#post_form .fild').width(), 400, true);
        });
    };
    PostingFormComponent.prototype.clearPreview = function () {
        this.showPreview = false;
        this.showGifPreview = false;
        this.showVideoPreview = false;
        this.videoPreview = 0;
        this.appliedPreview = 0;
        this.toggleFilterPreview();
        this.cropper.reset();
        this.thoughtFile.nativeElement.value = "";
    };
    // Geo Picker
    PostingFormComponent.prototype.toogleGeoPicker = function () {
        var _this = this;
        this.isPickerVisible = !this.isPickerVisible;
        if (!this.curLocation && this.isPickerVisible) {
            navigator.geolocation.getCurrentPosition(function (position) {
                // TODO: get nearest location from server
                _this.searchNearest({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                });
            }, function (err) {
                _this.searchNearest({});
            });
        }
    };
    PostingFormComponent.prototype.onChangeKeyword = function () {
        var _this = this;
        this.belaAPIService.searchPlace({ keyword: this.curKeyword }).then(function (res) {
            if (_this.delayTimer) {
                clearTimeout(_this.delayTimer);
            }
            _this.delayTimer = setTimeout(function () {
                _this.searchPlaces(_this.curKeyword);
            }, 200);
        });
    };
    PostingFormComponent.prototype.searchNearest = function (params) {
        var _this = this;
        this.isLoadingLocation = true;
        this.belaAPIService.searchPlace(params).then(function (res) {
            _this.isLoadingLocation = false;
            _this.locations.topSearches = res;
            _this.curLocation = _this.locations.topSearches[0];
        });
    };
    PostingFormComponent.prototype.searchPlaces = function (keyword) {
        var _this = this;
        var params = {};
        if (keyword) {
            params = { keyword: keyword };
        }
        else {
            params = {};
        }
        this.isSearchingQuery = true;
        this.belaAPIService.searchPlace(params).then(function (res) {
            _this.isSearchingQuery = false;
            _this.locations.searchResult = res;
        });
    };
    PostingFormComponent.prototype.selectPlace = function (place) {
        var placeIdx = this.locations.topSearches.indexOf(place);
        if (placeIdx !== -1) {
            this.locations.topSearches.splice(placeIdx, 1);
        }
        this.locations.topSearches.unshift(place);
        this.locations.topSearches = this.locations.topSearches.slice(0, 5);
        this.curLocation = place;
    };
    PostingFormComponent.prototype.postThought = function (event) {
        var _this = this;
        this.loading = true;
        event.stopPropagation();
        var postData = {
            content: this.content.getHostElement().nativeElement.value,
            csrfmiddlewaretoken: this.csrf
        };
        var files = this.thoughtFile.nativeElement.files;
        if (this.isPickerVisible) {
            postData['location'] = this.curLocation.location;
        }
        if (files.length > 0) {
            // this.appService.postWithFile('users/'+this.requesting_user.username+'/', postData, files).then(result => {
            var response = void 0;
            var fileData_1;
            var fileType = void 0;
            var fileName_1;
            fileName_1 = files[0].name;
            fileType = files[0].type;
            if (this.showGifPreview) {
                fileData_1 = files[0];
            }
            else {
                if (this.enableCrop) {
                    fileData_1 = this.belaAPIService.dataURItoBlob(this.data.image, fileType);
                }
                else {
                    fileData_1 = this.belaAPIService.dataURItoBlob(this.previewImg, fileType);
                }
            }
            response = this.belaAPIService.getImagePostLink(fileName_1, fileType)
                .then(function (res) {
                postData['image_path'] = res.url.replace(res.data.url, '');
                return _this.s3APIService.uploadBlobToS3(res, fileData_1, fileName_1);
            })
                .then(function (res) {
                console.log(res);
                return _this.belaAPIService.createFeed(_this.requesting_user.username, postData);
            }).catch(function (err) {
                console.error(err);
            });
            response.then(function (res) {
                _this.loading = false;
                if (res.status === 'success') {
                    event.target.reset();
                    _this.showPreview = false;
                    _this.showGifPreview = false;
                    _this.isPickerVisible = false;
                    _this.eventService.fireEvent({
                        type: __WEBPACK_IMPORTED_MODULE_8__core_event_types__["b" /* BelaEventType */].ADDED_POST,
                        value: res.result['thot']
                    });
                }
            });
        }
        else {
            this.loading = false;
            jQuery('#uploadlabel').tooltip("show");
            setTimeout(function () { jQuery('#uploadlabel').tooltip('hide'); }, 4000);
        }
    };
    /**
     * Video Upload Method
     * @param $event
     */
    PostingFormComponent.prototype.thoughtVidUpload = function ($event) {
        this.enableCrop = false;
        var file = $event.target.files[0];
        this.videoPreview = file.type == "video/mp4";
        var Reader = new FileReader();
        var that = this;
        Reader.onloadend = function (loadEvent) {
            that.showVideoPreview = true;
            that.videoPreview = loadEvent.target.result;
        };
        Reader.readAsDataURL(file);
    };
    PostingFormComponent.prototype.thoughtPicChange = function ($event) {
        this.enableCrop = false;
        var file = $event.target.files[0];
        this.showGifPreview = file.type == "image/gif";
        this.videoPreview = file.type == "video/mp4";
        var Reader = new FileReader();
        var that = this;
        Reader.onloadend = function (loadEvent) {
            that.showPreview = true;
            that.previewImg = loadEvent.target.result;
            that.filterImgPreview = loadEvent.target.result;
            that.saturatedPreview = loadEvent.target.result;
            that.videoPreview = loadEvent.target.result;
        };
        Reader.readAsDataURL(file);
    };
    PostingFormComponent.prototype.setCrop = function () {
        this.enableCrop = !this.enableCrop;
        if (this.enableCrop) {
            var image = new Image();
            image.src = this.previewImg;
            this.cropper.cropper.resizeCanvas(jQuery('#post_form .fild').width(), 400, true);
            this.cropper.setImage(image, new __WEBPACK_IMPORTED_MODULE_5_ng2_img_cropper__["a" /* Bounds */](image.width / 4, image.height / 4, image.width / 2, image.height / 2));
        }
        else {
            this.previewImg = this.cropper.image.original.src;
        }
    };
    PostingFormComponent.prototype.rotate = function () {
        if (!this.showGifPreview) {
            var image = new Image();
            image.src = this.previewImg;
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');
            canvas.width = image.height;
            canvas.height = image.width;
            ctx.rotate(90 * Math.PI / 180);
            ctx.drawImage(image, 0, -image.height);
            this.previewImg = image.src = canvas.toDataURL('image/png');
            //TO ROTATE FILTER PREVIEWS
            var image2 = new Image();
            image2.src = this.filterImgPreview;
            var canvas2 = document.createElement('canvas');
            var ctx2 = canvas2.getContext('2d');
            canvas2.width = image2.height;
            canvas2.height = image2.width;
            ctx2.rotate(90 * Math.PI / 180);
            ctx2.drawImage(image2, 0, -image2.height);
            this.filterImgPreview = image2.src = canvas2.toDataURL('image/png');
            this.saturatedPreview = this.filterImgPreview;
            this.filterImgPreview = this.filterImgPreview;
            if (this.enableCrop) {
                this.cropper.setImage(image, new __WEBPACK_IMPORTED_MODULE_5_ng2_img_cropper__["a" /* Bounds */](image.height / 4, image.width / 4, image.height / 2, image.width / 2));
            }
        }
    };
    /*
        START FILTER METHODS
    */
    PostingFormComponent.prototype.toggleFilterPreview = function () {
        this.enableFilter = !this.enableFilter;
    };
    PostingFormComponent.prototype.isPreviewApplied = function () {
        this.appliedPreview = this.appliedPreview + 1;
        this.saturatedPreviewApply();
    };
    PostingFormComponent.prototype.resetApplied = function () {
        this.appliedPreview = 0;
    };
    PostingFormComponent.prototype.saturatedPreviewApply = function () {
        if (this.appliedPreview == 1) {
            var image = new Image();
            image.src = this.saturatedPreview;
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');
            canvas.width = image.width;
            canvas.height = image.height;
            ctx.drawImage(image, 0, 0);
            image.style.display = "none";
            var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
            var data = imageData.data;
            for (var i = 0; i < data.length; i += 4) {
                var r = data[i] / 1.5;
                var g = data[i + 1] / 1.5;
                var b = data[i + 1] / 1.5;
                data[i] = data[i] + r; // red
                data[i + 1] = data[i + 1] + g; // green
                data[i + 2] = data[i + 2] + b; // blue
            }
            ctx.putImageData(imageData, 0, 0);
            this.saturatedPreview = image.src = canvas.toDataURL('image/jpeg', 0.5);
            this.appliedPreview = this.appliedPreview + 1;
        }
    };
    PostingFormComponent.prototype.setOriginal = function () {
        this.previewImg = this.filterImgPreview;
    };
    PostingFormComponent.prototype.setSaturated = function () {
        var image = new Image();
        image.src = this.filterImgPreview;
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        canvas.width = image.width;
        canvas.height = image.height;
        ctx.drawImage(image, 0, 0);
        image.style.display = "none";
        var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        var data = imageData.data;
        for (var i = 0; i < data.length; i += 4) {
            var r = data[i] / 1.5;
            var g = data[i + 1] / 1.5;
            var b = data[i + 1] / 1.5;
            data[i] = data[i] + r; // red
            data[i + 1] = data[i + 1] + g; // green
            data[i + 2] = data[i + 2] + b; // blue
        }
        ctx.putImageData(imageData, 0, 0);
        this.previewImg = image.src = canvas.toDataURL('image/jpeg', 0.5);
    };
    PostingFormComponent.prototype.setGrayscale = function () {
        var image = new Image();
        image.src = this.filterImgPreview;
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        canvas.width = image.width;
        canvas.height = image.height;
        ctx.drawImage(image, 0, 0);
        image.style.display = "none";
        var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        var data = imageData.data;
        for (var i = 0; i < data.length; i += 4) {
            var avg = (data[i] + data[i + 1] + data[i + 2]) / 3;
            data[i] = avg; // red
            data[i + 1] = avg; // green
            data[i + 2] = avg; // blue
        }
        ctx.putImageData(imageData, 0, 0);
        this.previewImg = image.src = canvas.toDataURL('image/jpeg', 1);
    };
    return PostingFormComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('thoughtFile'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], PostingFormComponent.prototype, "thoughtFile", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('content'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__typeahead_typeahead_component__["a" /* TypeaheadComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__typeahead_typeahead_component__["a" /* TypeaheadComponent */]) === "function" && _b || Object)
], PostingFormComponent.prototype, "content", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('cropper', undefined),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5_ng2_img_cropper__["c" /* ImageCropperComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_ng2_img_cropper__["c" /* ImageCropperComponent */]) === "function" && _c || Object)
], PostingFormComponent.prototype, "cropper", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], PostingFormComponent.prototype, "requesting_user", void 0);
PostingFormComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-posting-form',
        template: __webpack_require__("./src/app/posting-form/posting-form.component.html"),
        styles: [__webpack_require__("./src/app/posting-form/posting-form.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_7__core_awss3_api_awss3_api_service__["a" /* AWSS3ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_awss3_api_awss3_api_service__["a" /* AWSS3ApiService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_6__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_event_service__["a" /* EventService */]) === "function" && _f || Object])
], PostingFormComponent);

var HeaderPostingFormComponent = (function () {
    function HeaderPostingFormComponent(belaAPIService, s3APIService, router, eventService) {
        this.belaAPIService = belaAPIService;
        this.s3APIService = s3APIService;
        this.router = router;
        this.eventService = eventService;
        this.requesting_user = new __WEBPACK_IMPORTED_MODULE_2__app_model__["h" /* User */]();
        this.loading = false;
        this.showHeaderPreview = false;
        this.showHeaderGifPreview = false;
        this.csrf = "";
        this.enableCrop = false;
        this.curLocation = null;
        this.isPickerVisible = false;
        this.isLoadingLocation = false;
        this.locations = {
            topSearches: [],
            searchResult: []
        };
        //FILTER VARIABLES
        this.headerEnableFilter = false;
        this.headerAppliedPreview = 0;
        this.cropperSettings = new __WEBPACK_IMPORTED_MODULE_5_ng2_img_cropper__["b" /* CropperSettings */]();
        this.cropperSettings.canvasWidth = 200;
        this.cropperSettings.canvasHeight = 200;
        this.cropperSettings.croppedWidth = 1000;
        this.cropperSettings.minWidth = 200;
        this.cropperSettings.minHeight = 200;
        this.cropperSettings.noFileInput = true;
        this.cropperSettings.preserveSize = true;
        this.cropperSettings.keepAspect = false;
        this.cropperSettings.dynamicSizing = true;
        this.cropperSettings.cropperClass = "previewimage_canvas";
        this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,0,0,1)';
        this.cropperSettings.touchRadius = 50;
        this.data = {};
    }
    HeaderPostingFormComponent.prototype.ngOnInit = function () {
        this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
    };
    HeaderPostingFormComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        var elImagePreview = jQuery('#header_post_form .fild');
        this.cropper.cropper.resizeCanvas(elImagePreview.width(), 300, true);
        jQuery(window).resize(function () {
            _this.cropper.cropper.resizeCanvas(elImagePreview.width(), 300, true);
        });
    };
    // Geo Picker
    HeaderPostingFormComponent.prototype.toogleGeoPicker = function () {
        var _this = this;
        this.isPickerVisible = !this.isPickerVisible;
        if (!this.curLocation && this.isPickerVisible) {
            navigator.geolocation.getCurrentPosition(function (position) {
                // TODO: get nearest location from server
                _this.searchNearest({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                });
            }, function (err) {
                _this.searchNearest({});
            });
        }
    };
    HeaderPostingFormComponent.prototype.searchNearest = function (params) {
        var _this = this;
        this.isLoadingLocation = true;
        this.belaAPIService.searchPlace(params).then(function (res) {
            _this.isLoadingLocation = false;
            _this.locations.topSearches = res;
            _this.curLocation = _this.locations.topSearches[0];
        });
    };
    HeaderPostingFormComponent.prototype.clearHeaderPreview = function () {
        this.showHeaderPreview = false;
        this.showHeaderGifPreview = false;
        this.headerAppliedPreview = 0;
        this.cropper.reset();
        this.headerThoughtFile.nativeElement.value = "";
    };
    HeaderPostingFormComponent.prototype.headerThoughtPicChange = function ($event) {
        this.enableCrop = false;
        var file = $event.target.files[0];
        this.showHeaderGifPreview = file.type == "image/gif";
        var Reader = new FileReader();
        var that = this;
        Reader.onloadend = function (loadEvent) {
            that.showHeaderPreview = true;
            that.headerPreviewImg = loadEvent.target.result;
            that.headerFilterImgPreview = loadEvent.target.result;
            that.headerSaturatedPreview = loadEvent.target.result;
        };
        Reader.readAsDataURL(file);
    };
    HeaderPostingFormComponent.prototype.postThought = function (event) {
        var _this = this;
        this.loading = true;
        event.stopPropagation();
        var postData = {
            content: this.headerContent.getHostElement().nativeElement.value,
            csrfmiddlewaretoken: this.csrf
        };
        var files = this.headerThoughtFile.nativeElement.files;
        if (this.isPickerVisible) {
            postData['location'] = this.curLocation.location;
        }
        if (files.length > 0) {
            var response = void 0;
            var fileData_2, fileType = void 0, fileName_2;
            fileName_2 = files[0].name;
            fileType = files[0].type;
            if (this.showHeaderGifPreview) {
                fileData_2 = files[0];
            }
            else {
                if (this.enableCrop) {
                    fileData_2 = this.belaAPIService.dataURItoBlob(this.data.image, fileType);
                }
                else {
                    fileData_2 = this.belaAPIService.dataURItoBlob(this.headerPreviewImg, fileType);
                }
            }
            response = this.belaAPIService.getImagePostLink(fileName_2, fileType)
                .then(function (res) {
                postData['image_path'] = res.url.replace(res.data.url, '');
                return _this.s3APIService.uploadBlobToS3(res, fileData_2, fileName_2);
            })
                .then(function (res) {
                return _this.belaAPIService.createFeed(_this.requesting_user.username, postData);
            }).catch(function (err) {
                console.error(err);
            });
            response.then(function (res) {
                _this.loading = false;
                if (res.status === 'success') {
                    //this.initiateFeed();
                    event.target.reset();
                    _this.showHeaderPreview = false;
                    _this.showHeaderGifPreview = false;
                    _this.eventService.fireEvent({
                        type: __WEBPACK_IMPORTED_MODULE_8__core_event_types__["b" /* BelaEventType */].ADDED_POST,
                        value: res.result['thot']
                    });
                    window.uploadMobileImage();
                    jQuery('html, body').animate({
                        scrollTop: jQuery(".middle-section .white-box").first().offset().top - 80
                    }, 1000);
                }
                else {
                    //Handle error in form
                }
            });
        }
        else {
            this.loading = false;
            jQuery('#uploadlabelHeader').tooltip("show");
            setTimeout(function () { jQuery('#uploadlabelHeader').tooltip('hide'); }, 4000);
        }
    };
    HeaderPostingFormComponent.prototype.setCrop = function () {
        this.enableCrop = !this.enableCrop;
        if (this.enableCrop) {
            var image = new Image();
            image.src = this.headerPreviewImg;
            this.cropper.cropper.resizeCanvas(jQuery('#header_post_form .fild').width(), 300, true);
            this.cropper.setImage(image, new __WEBPACK_IMPORTED_MODULE_5_ng2_img_cropper__["a" /* Bounds */](image.width / 4, image.height / 4, image.width / 2, image.height / 2));
        }
        else {
            this.headerPreviewImg = this.cropper.image.original.src;
        }
    };
    HeaderPostingFormComponent.prototype.rotate = function () {
        if (!this.showHeaderGifPreview) {
            var image = new Image();
            image.src = this.headerPreviewImg;
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');
            canvas.width = image.height;
            canvas.height = image.width;
            ctx.rotate(90 * Math.PI / 180);
            ctx.drawImage(image, 0, -image.height);
            this.headerPreviewImg = image.src = canvas.toDataURL('image/png');
            //TO ROTATE FILTER PREVIEWS
            var image2 = new Image();
            image2.src = this.headerFilterImgPreview;
            var canvas2 = document.createElement('canvas');
            var ctx2 = canvas2.getContext('2d');
            canvas2.width = image2.height;
            canvas2.height = image2.width;
            ctx2.rotate(90 * Math.PI / 180);
            ctx2.drawImage(image2, 0, -image2.height);
            this.headerFilterImgPreview = image2.src = canvas2.toDataURL('image/png');
            this.headerSaturatedPreview = this.headerFilterImgPreview;
            this.headerFilterImgPreview = this.headerFilterImgPreview;
            if (this.enableCrop) {
                this.cropper.setImage(image, new __WEBPACK_IMPORTED_MODULE_5_ng2_img_cropper__["a" /* Bounds */](image.height / 4, image.width / 4, image.height / 2, image.width / 2));
            }
        }
    };
    /*
       START FILTER METHODS
   */
    HeaderPostingFormComponent.prototype.toggleFilterPreview = function () {
        this.headerEnableFilter = !this.headerEnableFilter;
    };
    HeaderPostingFormComponent.prototype.isPreviewApplied = function () {
        this.headerAppliedPreview = this.headerAppliedPreview + 1;
        this.saturatedPreviewApply();
    };
    HeaderPostingFormComponent.prototype.resetApplied = function () {
        this.headerAppliedPreview = 0;
    };
    HeaderPostingFormComponent.prototype.saturatedPreviewApply = function () {
        if (this.headerAppliedPreview == 1) {
            var image = new Image();
            image.src = this.headerSaturatedPreview;
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');
            canvas.width = image.width;
            canvas.height = image.height;
            ctx.drawImage(image, 0, 0);
            image.style.display = "none";
            var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
            var data = imageData.data;
            for (var i = 0; i < data.length; i += 4) {
                var r = data[i] / 1.5;
                var g = data[i + 1] / 1.5;
                var b = data[i + 1] / 1.5;
                data[i] = data[i] + r; // red
                data[i + 1] = data[i + 1] + g; // green
                data[i + 2] = data[i + 2] + b; // blue
            }
            ctx.putImageData(imageData, 0, 0);
            this.headerSaturatedPreview = image.src = canvas.toDataURL('image/jpeg', 0.5);
            this.headerAppliedPreview = this.headerAppliedPreview + 1;
        }
    };
    HeaderPostingFormComponent.prototype.setOriginal = function () {
        this.headerPreviewImg = this.headerFilterImgPreview;
    };
    HeaderPostingFormComponent.prototype.setSaturated = function () {
        var image = new Image();
        image.src = this.headerFilterImgPreview;
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        canvas.width = image.width;
        canvas.height = image.height;
        ctx.drawImage(image, 0, 0);
        image.style.display = "none";
        var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        var data = imageData.data;
        for (var i = 0; i < data.length; i += 4) {
            var r = data[i] / 1.5;
            var g = data[i + 1] / 1.5;
            var b = data[i + 1] / 1.5;
            data[i] = data[i] + r; // red
            data[i + 1] = data[i + 1] + g; // green
            data[i + 2] = data[i + 2] + b; // blue
        }
        ctx.putImageData(imageData, 0, 0);
        this.headerPreviewImg = image.src = canvas.toDataURL('image/jpeg', 0.5);
    };
    HeaderPostingFormComponent.prototype.setGrayscale = function () {
        var image = new Image();
        image.src = this.headerFilterImgPreview;
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        canvas.width = image.width;
        canvas.height = image.height;
        ctx.drawImage(image, 0, 0);
        image.style.display = "none";
        var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        var data = imageData.data;
        for (var i = 0; i < data.length; i += 4) {
            var avg = (data[i] + data[i + 1] + data[i + 2]) / 3;
            data[i] = avg; // red
            data[i + 1] = avg; // green
            data[i + 2] = avg; // blue
        }
        ctx.putImageData(imageData, 0, 0);
        this.headerPreviewImg = image.src = canvas.toDataURL('image/jpeg', 1);
    };
    return HeaderPostingFormComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('headerThoughtFile'),
    __metadata("design:type", typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _g || Object)
], HeaderPostingFormComponent.prototype, "headerThoughtFile", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('headerContent'),
    __metadata("design:type", typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_3__typeahead_typeahead_component__["a" /* TypeaheadComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__typeahead_typeahead_component__["a" /* TypeaheadComponent */]) === "function" && _h || Object)
], HeaderPostingFormComponent.prototype, "headerContent", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('cropper', undefined),
    __metadata("design:type", typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_5_ng2_img_cropper__["c" /* ImageCropperComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_ng2_img_cropper__["c" /* ImageCropperComponent */]) === "function" && _j || Object)
], HeaderPostingFormComponent.prototype, "cropper", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], HeaderPostingFormComponent.prototype, "requesting_user", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], HeaderPostingFormComponent.prototype, "thots", void 0);
HeaderPostingFormComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-header-posting-form',
        template: __webpack_require__("./src/app/posting-form/header-posting-form.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_7__core_awss3_api_awss3_api_service__["a" /* AWSS3ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_awss3_api_awss3_api_service__["a" /* AWSS3ApiService */]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["d" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["d" /* Router */]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_6__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_event_service__["a" /* EventService */]) === "function" && _o || Object])
], HeaderPostingFormComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o;
//# sourceMappingURL=posting-form.component.js.map

/***/ }),

/***/ "./src/app/privacy/privacy.component.html":
/***/ (function(module, exports) {

module.exports = "<script src=\"https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.js\"></script>\n\n<div class=\"user-profile-contain\">\n  <div class=\"mid-con\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <div class=\"col-sm-3 col-lg-3 col-md-3 user-profile-left\">\n        </div>\n\n        <div class=\"col-sm-6 col-lg-6 col-md-6 middle-section\">\n\n          <div class=\"discover-box\">\n            <div class=\"panel with-nav-tabs \">\n\n              <h1 translate>Belacam v. 3.0 Early Access Privacy Policy</h1>\n\n              <h3 translate>This Privacy Policy is effective for July 20th, 2018.</h3>\n\n              <p translate>Belacam is a platform entering uncharted waters. As social media expands to accommodate monetization, new frontiers of data security and privacy arise. You can rest assured that your privacy is of utmost importance to us.</p>\n\n              <p translate>It is Belacam, Inc's policy to respect your privacy regarding any information we may collect while operating our website. Accordingly, we have developed this privacy policy in order for you to understand how we collect, use, communicate, disclose and otherwise make use of personal information. We have outlined our privacy policy below.</p>\n\n              <p translate>We will collect personal information by lawful and fair means and, where appropriate, with or without the knowledge or consent of the individual concerned. This collection may or may not employ automated scripts and data scraping programs to gather information.</p>\n\n              <p translate>Before or at the time of collecting personal information, we may or may not identify the purposes for which information is being collected.</p>\n\n              <p translate>We will collect and use personal information solely for fulfilling those purposes specified by us (user experience optimization, etc.) and for other ancillary purposes. Personal data should be relevant to the purposes for which it is to be used.</p>\n\n              <p translate>We will protect personal information by using reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.</p>\n\n              <p translate>We will make readily available to customers information about our policies and practices relating to the management of personal information, including principally updates to the Privacy Policy.</p>\n\n              <p translate>We will only retain personal information for as long as necessary for the fulfillment of those purposes.</p>\n\n              <p translate>We are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained. Belacam, Inc may change this privacy policy from time to time at Belacam, Inc's sole discretion.</p>\n\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/privacy/privacy.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrivacyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PrivacyComponent = (function () {
    function PrivacyComponent() {
        this.stuff = [1, 2, 3, 4, 5];
    }
    return PrivacyComponent;
}());
PrivacyComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("./src/app/privacy/privacy.component.html")
    }),
    __metadata("design:paramtypes", [])
], PrivacyComponent);

//# sourceMappingURL=privacy.component.js.map

/***/ }),

/***/ "./src/app/profile-box/profile-box.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/profile-box/profile-box.component.html":
/***/ (function(module, exports) {

module.exports = "\n<a class=\"profile-box-a\" href=\"#\" [routerLink]=\"['/user/', user.username]\">\n  <div class=\"white-box\">\n    <div>\n      <img id=\"cover_image\" [src]=\"cover_pic_url()\" class=\"img-responsive\">\n    </div>\n\n\n    <div class=\"profilepicbox\">\n\n      <span [innerHTML]=\"user.avatar\"></span>\n\n      <div class=\"content3\">\n        <h3>{{ user.username }}</h3>\n\n\n        <ul>\n          <li class=\"col-sm-4\"><strong>{{user.thought_count}}</strong><br>\n\n            <ng-template #posts>\n              {{'Post'|translate}}\n            </ng-template>\n            <ng-container *ngIf=\"user.thought_count>1; else posts\">\n              {{'Posts'|translate}}\n            </ng-container>\n          </li>\n\n          <li class=\"col-sm-8\"><strong>${{user.earnings|number:'1.2-2'}}</strong><br>\n            {{'Lifetime Earnings'|translate}}\n          </li>\n        </ul>\n\n      </div>\n    </div>\n\n  </div>\n</a>\n\n\n"

/***/ }),

/***/ "./src/app/profile-box/profile-box.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileBoxComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__lightbox_lightbox_service__ = __webpack_require__("./src/app/lightbox/lightbox.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__constants__ = __webpack_require__("./src/app/constants.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProfileBoxComponent = (function () {
    function ProfileBoxComponent(_lightbox) {
        this._lightbox = _lightbox;
        this.user = new __WEBPACK_IMPORTED_MODULE_2__app_model__["h" /* User */]();
    }
    ProfileBoxComponent.prototype.ngOnInit = function () {
    };
    ProfileBoxComponent.prototype.cover_pic_url = function () {
        return this.user.user1.cover_pic ? this.user.user1.cover_pic : __WEBPACK_IMPORTED_MODULE_3__constants__["a" /* Constants */].default_cover_pic;
    };
    ProfileBoxComponent.prototype.openCoverLightbox = function () {
        var options = {
            disableScrolling: true,
            fitImageInViewPort: true
        };
        var _album = {
            src: this.cover_pic_url(),
            thumb: this.cover_pic_url(),
            caption: "",
            thot: null
        };
        this._lightbox.openAlbum([_album], 0, options);
    };
    return ProfileBoxComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('user'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__app_model__["h" /* User */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__app_model__["h" /* User */]) === "function" && _a || Object)
], ProfileBoxComponent.prototype, "user", void 0);
ProfileBoxComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-profile-box',
        template: __webpack_require__("./src/app/profile-box/profile-box.component.html"),
        styles: [__webpack_require__("./src/app/profile-box/profile-box.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__lightbox_lightbox_service__["a" /* Lightbox */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__lightbox_lightbox_service__["a" /* Lightbox */]) === "function" && _b || Object])
], ProfileBoxComponent);

var _a, _b;
//# sourceMappingURL=profile-box.component.js.map

/***/ }),

/***/ "./src/app/search/mobile-search.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-sm-6 col-lg-6 col-md-6 search-form-box show\">\n  <form name=\"msearch\" id=\"msearch\" #searchForm>\n\n    <input id=\"mobilesearch1\" type=\"text\" (keyup)=\"search($event.target.value)\" (keyup.enter)=\"search($event.target.value)\" placeholder=\" Search users or tags\" #mterm autocomplete=\"off\" autocapitalize=\"none\" />\n    <input type=\"submit\" value=\"\" (click)=\"goToTopSearch()\" onclick=\"mobileSearch()\">\n    <ul class=\"msearchresults\">\n      <ng-container *ngFor=\"let item of items\">\n        <a href=\"#\" *ngIf=\"item.type=='user'\" [routerLink]=\"['/user/', item.username]\" onclick=\"mobileSearch()\">\n          <li>\n            <span class=\"item-user-info\"><span class=\"item-fullname\">@{{item.username}}</span> </span>\n            <div class=\"item-user-earning\">${{item.earnings|number:'1.2-2'}} earned</div>\n            <div class=\"item-social-context\">\n              {{item.social_context}}\n            </div>\n          </li>\n        </a>\n      </ng-container>\n      <ng-container *ngFor=\"let item of items\">\n        <a href=\"#\" *ngIf=\"item.type=='tag'\" [routerLink]=\"['/discover/', item.name]\" onclick=\"mobileSearch()\">\n          <li>#{{item.name}}<span class=\"pullright\">({{item.num_items}} {{'posts'|translate}})</span>\n          </li>\n        </a>\n      </ng-container>\n\n    </ul>\n\n  </form>\n  <app-trending [trending]=\"trending\"></app-trending>\n  <app-growth [growth]=\"growth\"></app-growth>\n</div>\n"

/***/ }),

/***/ "./src/app/search/search.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-sm-6 col-lg-6 col-md-6 search-form-box\">\n\n  <form name=\"search\" id=\"search\" class=\"dropdown\" title=\"Search a user or tag\" #searchForm\n        [ngClass]=\"{'open': showSuggestions}\">\n\n    <input\n      id=\"desktopsearch\"\n      type=\"text\"\n      (keyup)=\"search($event.target.value)\"\n      (keyup.enter)=\"search($event.target.value)\"\n      [placeholder]=\"'Search users or tags'|translate\"\n      #term\n      autocomplete=\"off\" />\n    <input type=\"submit\" value=\"\" (click)=\"goToTopSearch()\">\n    <div class=\"searchresults-wrapper dropdown-menu\">\n      <div class=\"search-backdrop\" (click)=\"hideSuggestions()\"></div>\n      <div class=\"dropdown-caret\">\n        <div class=\"caret-outer\"></div>\n        <div class=\"caret-inner\"></div>\n      </div>\n      <ul class=\"searchresults\">\n        <ng-container *ngFor=\"let item of items\">\n          <a href=\"#\" *ngIf=\"item.type=='user'\" [routerLink]=\"['/user/', item.username]\" (click)=\"handleSelectItem()\">\n            <li class=\"clearfix\">\n              <span class=\"item-user-info\"><span class=\"item-fullname\">@{{item.username}}</span> </span>\n              <span class=\"pullright\">(${{item.earnings|number:'1.2-2'}} earned)</span>\n              <div class=\"item-social-context\">\n                {{item.social_context}}\n              </div>\n            </li>\n          </a>\n        </ng-container>\n        <ng-container *ngFor=\"let item of items\">\n          <a href=\"#\" *ngIf=\"item.type=='tag'\" [routerLink]=\"['/discover/', item.name]\" (click)=\"handleSelectItem()\">\n            <li class=\"clearfix\">#{{item.name}}<span class=\"pullright\">({{item.num_items}} {{'posts'|translate}}) </span></li>\n          </a>\n        </ng-container>\n      </ul>\n\n    </div>\n\n  </form>\n\n</div>\n"

/***/ }),

/***/ "./src/app/search/search.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return SearchComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MobileSearchComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__ = __webpack_require__("./node_modules/rxjs/_esm5/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_of__ = __webpack_require__("./node_modules/rxjs/_esm5/add/observable/of.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_catch__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_debounceTime__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/distinctUntilChanged.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__search_service__ = __webpack_require__("./src/app/search/search.service.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// Observable class extensions

// Observable operators




var SearchComponent = (function () {
    function SearchComponent(searchService, cdr, renderer, router) {
        this.searchService = searchService;
        this.cdr = cdr;
        this.renderer = renderer;
        this.router = router;
        this.items = [];
        this.showSuggestions = false;
        this.searchTerms = new __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__["b" /* Subject */]();
        this.term = "";
    }
    SearchComponent.prototype.search = function (term) {
        var blank = "";
        var hash = "#";
        console.log(this.searchForm);
        term = term.trim();
        if (term != blank || term != hash) {
            this.searchTerms.next(term);
            this.term = term;
        }
    };
    SearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.searchTerms
            .debounceTime(300) // wait 300ms after each keystroke before considering the term
            .distinctUntilChanged() // ignore if next search term is same as previous
            .switchMap(function (term) {
            return term // switch to new observable each time the term changes
                ? _this.searchService.search(term)
                    .catch(function (err) { return []; })
                : __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["a" /* Observable */].of([]);
        })
            .subscribe(function (results) {
            _this.items = results;
            _this._addSocialContext();
            if (_this.items.length > 0) {
                _this.showSuggestions = true;
            }
            else {
                _this.hideSuggestions();
            }
            _this.cdr.markForCheck();
        });
        jQuery(this.searchForm.nativeElement).tooltip({
            trigger: 'manual',
            placement: 'bottom'
        });
    };
    SearchComponent.prototype._addSocialContext = function () {
        for (var _i = 0, _a = this.items; _i < _a.length; _i++) {
            var item = _a[_i];
            var contextArr = [];
            if (item.is_followee) {
                contextArr.push('Following');
            }
            if (item.is_follower) {
                contextArr.push('Follows you');
            }
            item['social_context'] = contextArr.join(', ');
        }
    };
    SearchComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
        jQuery(this.searchForm.nativeElement).tooltip('destroy');
    };
    SearchComponent.prototype.hideSuggestions = function () {
        this.showSuggestions = false;
    };
    SearchComponent.prototype.handleSelectItem = function () {
        this.hideSuggestions();
    };
    SearchComponent.prototype.goToTopSearch = function () {
        var _this = this;
        if (this.term) {
            this.router.navigate(['/top_search/', this.term]);
        }
        else {
            jQuery(this.searchForm.nativeElement).tooltip('show');
            setTimeout(function () {
                jQuery(_this.searchForm.nativeElement).tooltip('hide');
            }, 2000);
        }
    };
    return SearchComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('searchForm'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], SearchComponent.prototype, "searchForm", void 0);
SearchComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        styles: [
            "\n      .search-backdrop {\n        bottom: 0;\n        left: 0;\n        position: fixed;\n        right: 0;\n        top: 0;\n        z-index: 0;\n      }\n    "
        ],
        selector: 'my-search',
        template: __webpack_require__("./src/app/search/search.component.html"),
        providers: [__WEBPACK_IMPORTED_MODULE_10__search_service__["a" /* SearchService */]]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_10__search_service__["a" /* SearchService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_10__search_service__["a" /* SearchService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer2"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer2"]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__angular_router__["d" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_router__["d" /* Router */]) === "function" && _e || Object])
], SearchComponent);

var MobileSearchComponent = (function (_super) {
    __extends(MobileSearchComponent, _super);
    function MobileSearchComponent(searchService, cdr, renderer, router, appService) {
        var _this = _super.call(this, searchService, cdr, renderer, router) || this;
        _this.appService = appService;
        _this.trending = [];
        _this.growth = new __WEBPACK_IMPORTED_MODULE_1__app_model__["a" /* GrowthStatistics */]();
        _this.appService.getGrowthStatistics().then(function (res) {
            _this.growth = res;
        });
        return _this;
    }
    return MobileSearchComponent;
}(SearchComponent));
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], MobileSearchComponent.prototype, "trending", void 0);
MobileSearchComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'mobile-search',
        template: __webpack_require__("./src/app/search/mobile-search.component.html"),
        providers: [__WEBPACK_IMPORTED_MODULE_10__search_service__["a" /* SearchService */]]
    }),
    __metadata("design:paramtypes", [typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_10__search_service__["a" /* SearchService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_10__search_service__["a" /* SearchService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer2"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer2"]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_5__angular_router__["d" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_router__["d" /* Router */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _k || Object])
], MobileSearchComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;
//# sourceMappingURL=search.component.js.map

/***/ }),

/***/ "./src/app/search/search.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// #docregion



var SearchService = (function () {
    function SearchService(http) {
        this.http = http;
    }
    SearchService.prototype.search = function (term) {
        var wikiUrl = "";
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["g" /* URLSearchParams */]();
        params.set('format', 'json');
        if (term.charAt(0) == "#") {
            term = term.slice(1);
            // #docregion search-parameters
            params.set('in', 'tag');
        }
        else if (term.charAt(0) == "@") {
            term = term.slice(1);
            // #docregion search-parameters
            params.set('in', 'user');
        }
        wikiUrl = '/api/search/' + term + '/';
        return this.http
            .get(wikiUrl, { params: params })
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    return SearchService;
}());
SearchService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], SearchService);

var _a;
//# sourceMappingURL=search.service.js.map

/***/ }),

/***/ "./src/app/settings/settings.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/settings/settings.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"user-profile-contain settings-page\">\n  <div class=\"alert fade in {{ message_type }}\" *ngIf=\"alert_message\">\n    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n    {{ alert_message }}\n  </div>\n\n  <div class=\"mid-con\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <div class=\"col-md-3\">\n          <ul class=\"nav nav-pills nav-stacked\">\n            <li>Settings</li>\n            <li class=\"active\"><a data-toggle=\"pill\" href=\"#settings_account\" translate>Account</a></li>\n            <li class=\"\"><a data-toggle=\"pill\" href=\"#settings_password\" translate>Password</a></li>\n            <li class=\"\"><a data-toggle=\"pill\" href=\"#settings_delete\" translate>Delete</a></li>\n          </ul>\n        </div>\n        <div class=\"col-md-9\">\n          <div class=\"tab-content\">\n            <div id=\"settings_account\" class=\"tab-pane fade active in\">\n\n              <div class=\"row\">\n                <div class=\"col-md-7\">\n                  <form id=\"account_form\">\n                    <legend translate>Account</legend>\n                    <p class=\"text-warning\" *ngIf=\"!verified\" translate>\n                        Your email address wasn't verified yet. If you have not received a verification email yet, you can resend verification mail.\n                        <br>\n                        <button class=\"btn btn-primary\" id=\"resendBtn\" (click)=\"resend_verify_email()\" translate>Resend Verification Email</button>\n                    </p>\n                    <div class=\"form-group\">\n                      <label class=\"control-label\" for=\"id_username\" translate>Username</label>\n                      <div class=\"\">\n                        <input class=\"form-control\" id=\"id_username\" name=\"username\" type=\"text\" [(ngModel)]=\"username\">\n                      </div>\n                    </div>\n                    <div class=\"form-group\">\n                      <label class=\"control-label\" for=\"id_email\" translate>Email</label>\n                      <div class=\"\">\n                        <input class=\"form-control\" id=\"id_email\" name=\"email\" type=\"email\"\n                                [(ngModel)]=\"email\">\n                      </div>\n                    </div>\n                    <div class=\"form-group\">\n                      <label class=\"control-label\" for=\"id_language\" translate>Language</label>\n                      <div class=\"\">\n                        <select class=\"form-control\" id=\"id_language\" name=\"language\"[(ngModel)]=\"language\"  required>\n                          <option *ngFor=\"let lang of language_list\" [value]=\"lang.value\">{{ lang.label }}</option>\n                        </select>\n                      </div>\n                    </div>\n                    <div class=\"form-group\">\n                      <div class=\"\">\n                        <div class=\"checkbox\">\n                          <label>\n                            <input id=\"id_send_weekly_email\" name=\"send_weekly_email\" type=\"checkbox\" [(ngModel)]=\"send_weekly_email\"> <span translate>Send weekly email?</span>\n                          </label>\n                        </div>\n                      </div>\n                    </div>\n                    <button class=\"btn btn-primary\" type=\"submit\" (click)=\"$event.preventDefault();saveAccount();\" translate>Save</button>\n                  </form>\n                </div>\n              </div>\n            </div>\n            <div id=\"settings_password\" class=\"tab-pane fade\">\n              <div class=\"row\">\n                <div class=\"col-md-7\">\n                  <form id=\"password_form\">\n                    <legend translate>Change password</legend>\n                    <fieldset>\n                      <div class=\"form-group\">\n                        <label class=\"control-label\" for=\"id_password_current\" translate>Current Password</label>\n                        <div class=\"\">\n                          <input class=\"form-control\" id=\"id_password_current\" name=\"password_current\" type=\"password\" [(ngModel)]=\"password_current\">\n                        </div>\n                      </div>\n                      <div class=\"form-group\">\n                        <label class=\"control-label\" for=\"id_password_new\" translate>New Password</label>\n                        <div class=\"\">\n                          <input class=\"form-control\" id=\"id_password_new\" name=\"password_new\" type=\"password\" [(ngModel)]=\"password_new\">\n                        </div>\n                      </div>\n                      <div class=\"form-group\">\n                        <label class=\"control-label\" for=\"id_password_new_confirm\" translate>New Password (again)</label>\n                        <div class=\"\">\n                          <input class=\" form-control\" id=\"id_password_new_confirm\" name=\"password_new_confirm\" type=\"password\" [(ngModel)]=\"password_new_confirm\">\n                        </div>\n                      </div>\n                      <button type=\"submit\" class=\"btn btn-primary\" (click)=\"$event.preventDefault();updatePassword();\" translate>Save</button>\n                    </fieldset>\n                  </form>\n                </div>\n              </div>\n            </div>\n            <div id=\"settings_delete\" class=\"tab-pane fade\">\n              <h1 translate>Delete Account</h1>\n              <p translate>If you go ahead and delete your account, your information will be <b>expunged within 48 hours</b>.</p>\n              <form>\n                <button type=\"submit\" class=\"btn btn-danger\" (click)=\"$event.preventDefault();showConfirm();\" translate>Delete My Account</button>\n              </form>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/settings/settings.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return SettingsComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfirmComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__("./node_modules/@ngx-translate/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_bootstrap_modal__ = __webpack_require__("./node_modules/ng2-bootstrap-modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_bootstrap_modal___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng2_bootstrap_modal__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SettingsComponent = (function () {
    function SettingsComponent(appService, authService, router, dialogService, translate) {
        this.appService = appService;
        this.authService = authService;
        this.router = router;
        this.dialogService = dialogService;
        this.translate = translate;
        this.username = "";
        this.email = "";
        this.language = "";
        this.language_list = [];
        this.send_weekly_email = false;
        this.verified = null;
        this.password_current = "";
        this.password_new = "";
        this.password_new_confirm = "";
        this.alert_message = "";
        this.message_type = "";
        this.csrf_token = "";
    }
    SettingsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.csrf_token = jQuery('input[name="csrfmiddlewaretoken"]').val();
        this.appService.getSettingDetails()
            .then(function (settingsData) {
            _this.username = settingsData.username;
            _this.email = settingsData.email;
            _this.language = settingsData.language;
            _this.language_list = settingsData.language_list;
            _this.send_weekly_email = settingsData.send_weekly_email;
            _this.verified = settingsData.verified;
        })
            .catch(function (err) {
            if (err.status == 404) {
                _this.router.navigate(['404']);
            }
            else {
                _this.router.navigate(['']);
            }
        });
    };
    SettingsComponent.prototype.ngAfterViewInit = function () {
        this.setupValidation();
    };
    SettingsComponent.prototype.setupValidation = function () {
        jQuery("#account_form").validate({
            rules: {
                username: "required",
                email: {
                    required: true,
                    email: true
                },
                language: "required",
            }
        });
        jQuery("#password_form").validate({
            rules: {
                password_current: "required",
                password_new: "required",
                password_new_confirm: {
                    required: true,
                    equalTo: "#id_password_new"
                }
            }
        });
    };
    SettingsComponent.prototype.saveAccount = function () {
        var _this = this;
        if (jQuery("#account_form").valid()) {
            this.appService.updateAccount(this.username, this.email, this.language, this.send_weekly_email, this.csrf_token).then(function (res) {
                if (res["status"] == "success") {
                    _this.username = res["username"];
                    _this.email = res["email"];
                    _this.language = res["language"];
                    _this.send_weekly_email = res["send_weekly_email"];
                    _this.verified = res["verified"];
                    _this.alert_message = res["message"];
                    _this.message_type = "alert-info";
                    jQuery("html, body").animate({ scrollTop: 0 }, "slow");
                    if (userLanguage != _this.language) {
                        _this.translate.resetLang(userLanguage);
                        _this.translate.use(_this.language);
                        userLanguage = _this.language;
                    }
                }
                else {
                    _this.alert_message = res["message"];
                    _this.message_type = "alert-danger";
                    jQuery("html, body").animate({ scrollTop: 0 }, "slow");
                }
            });
        }
    };
    SettingsComponent.prototype.resend_verify_email = function () {
        var _this = this;
        this.appService.resend_verify_email().then(function (res) {
            if (res["status"] == "success") {
                _this.alert_message = res["message"];
                _this.message_type = "alert-info";
                jQuery("html, body").animate({ scrollTop: 0 }, "slow");
            }
            else {
                _this.alert_message = res["message"];
                _this.message_type = "alert-danger";
                jQuery("html, body").animate({ scrollTop: 0 }, "slow");
            }
        });
    };
    SettingsComponent.prototype.updatePassword = function () {
        var _this = this;
        if (jQuery("#password_form").valid()) {
            this.appService.updatePassword(this.password_current, this.password_new, this.password_new_confirm, this.csrf_token).then(function (res) {
                if (res["status"] == "success") {
                    _this.password_current = "";
                    _this.password_new = "";
                    _this.password_new_confirm = "";
                    _this.alert_message = res["message"];
                    _this.message_type = "alert-info";
                    jQuery("html, body").animate({ scrollTop: 0 }, "slow");
                }
                else {
                    _this.alert_message = res["message"];
                    _this.message_type = "alert-danger";
                    jQuery("html, body").animate({ scrollTop: 0 }, "slow");
                }
            });
        }
    };
    SettingsComponent.prototype.deleteAccount = function () {
        var _this = this;
        this.appService.deleteAccount().then(function (res) {
            if (res["status"] == "success") {
                _this.alert_message = res["message"];
                _this.message_type = "alert-info";
                jQuery("html, body").animate({ scrollTop: 0 }, "slow");
                _this.authService.logout();
                setTimeout(function () {
                    _this.router.navigate(['']);
                }, 4000);
            }
            else {
                _this.alert_message = res["message"];
                _this.message_type = "alert-danger";
                jQuery("html, body").animate({ scrollTop: 0 }, "slow");
            }
        });
    };
    SettingsComponent.prototype.showConfirm = function () {
        var _this = this;
        var confirm_modal = this.dialogService.addDialog(ConfirmComponent, {
            title: 'Delete Account',
            message: 'Are you sure to want to delete your account?'
        })
            .subscribe(function (isConfirmed) {
            //We get dialog result
            if (isConfirmed) {
                _this.deleteAccount();
            }
            else {
            }
        });
    };
    SettingsComponent.prototype.ngOnDestroy = function () {
    };
    return SettingsComponent;
}());
SettingsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-settings',
        template: __webpack_require__("./src/app/settings/settings.component.html"),
        styles: [__webpack_require__("./src/app/settings/settings.component.css")],
        providers: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__core_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_auth_service__["a" /* AuthService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5_ng2_bootstrap_modal__["DialogService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_ng2_bootstrap_modal__["DialogService"]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */]) === "function" && _e || Object])
], SettingsComponent);


var ConfirmComponent = (function (_super) {
    __extends(ConfirmComponent, _super);
    function ConfirmComponent(dialogService) {
        return _super.call(this, dialogService) || this;
    }
    ConfirmComponent.prototype.confirm = function () {
        // we set dialog result as true on click on confirm button,
        // then we can get dialog result from caller code
        this.result = true;
        this.close();
    };
    return ConfirmComponent;
}(__WEBPACK_IMPORTED_MODULE_5_ng2_bootstrap_modal__["DialogComponent"]));
ConfirmComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'confirm',
        template: "<div class=\"modal-dialog\">\n                <div class=\"modal-content\">\n                   <div class=\"modal-header\">\n                     <button type=\"button\" class=\"close\" (click)=\"close()\" >&times;</button>\n                     <h4 class=\"modal-title\">{{title || 'Confirm'}}</h4>\n                   </div>\n                   <div class=\"modal-body\">\n                     <p>{{message || 'Are you sure?'}}</p>\n                   </div>\n                   <div class=\"modal-footer\">\n                     <button type=\"button\" class=\"btn btn-primary\" (click)=\"confirm()\">OK</button>\n                     <button type=\"button\" class=\"btn btn-default\" (click)=\"close()\" >Cancel</button>\n                   </div>\n                 </div>\n              </div>"
    }),
    __metadata("design:paramtypes", [typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5_ng2_bootstrap_modal__["DialogService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_ng2_bootstrap_modal__["DialogService"]) === "function" && _f || Object])
], ConfirmComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=settings.component.js.map

/***/ }),

/***/ "./src/app/shared/shared.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__("./node_modules/@ngx-translate/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SharedModule = (function () {
    function SharedModule() {
    }
    return SharedModule;
}());
SharedModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"]
        ],
        declarations: [],
        exports: [__WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["b" /* TranslateModule */], __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"]]
    })
], SharedModule);

//# sourceMappingURL=shared.module.js.map

/***/ }),

/***/ "./src/app/string.ts":
/***/ (function(module, exports) {

String.prototype.reverse = function () {
    return this.split('').reverse().join('');
};
//# sourceMappingURL=string.js.map

/***/ }),

/***/ "./src/app/tagfeeds/tagfeeds.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/tagfeeds/tagfeeds.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"discover-box\">\n  <div class=\"panel with-nav-tabs \">\n\n    <div class=\"panel-body\">\n\n      <div class=\"tab-content\">\n\n\n        <div class=\" fade in active endless_page_template\" infiniteScroll\n             [infiniteScrollDistance]=\"0.4\"\n             [infiniteScrollThrottle]=\"1000\"\n             (scrolled)=\"onFeedScroll()\"\n             style=\"float:left;width:100%;\">\n\n          <div class=\"row sortrow\">\n\n\n            <div class=\"dropdown\">\n              <button onclick=\"dropdownSort()\" class=\"dropbtn\">\n                <span>\n                  <i class=\"fa fa-fire\" aria-hidden=\"true\"></i>\n                  &nbsp;Hot&nbsp;\n                </span>\n                <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i></button>\n              <div id=\"dropdownsort\" class=\"dropdown-content dropdown-content-sort\">\n                <a href=\"#\" onClick=\"select_sort_btn(this);false\" (click)=\"sort_by_hot();false\" class=\"hide\" translate><i class=\"fa fa-fire\" aria-hidden=\"true\"></i> &nbsp;Hot&nbsp;</a>\n                <a href=\"#\" onClick=\"select_sort_btn(this);false\" (click)=\"sort_by_new();false\" translate> <i class=\"fa fa-hourglass\"></i> &nbsp;New&nbsp;</a>\n                <a href=\"#\" onClick=\"select_sort_btn(this);false\" (click)=\"sort_by_likes();false\" translate><i class=\"fa fa-heart\"></i> &nbsp;Most Liked&nbsp;</a>\n              </div>\n            </div>\n\n\n            <div class=\"dropdown\" *ngIf=\"select_most_liked\">\n              <button onclick=\"dropdownTime()\" class=\"dropbtn\">\n                <i class=\"fa fa-clock\" aria-hidden=\"true\"></i>\n                <span>\n                  &nbsp;Today&nbsp;\n                </span>\n                <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i></button>\n              <div id=\"dropdowntime\" class=\"dropdown-content dropdown-content-time\">\n                <a href=\"#\" onClick=\"select_sort_btn(this);false\" (click)=\"sort_by_likes('today');false\" class=\"hide\" translate>Today</a>\n                <a href=\"#\" onClick=\"select_sort_btn(this);false\" (click)=\"sort_by_likes('week');false\" translate>This Week</a>\n                <a href=\"#\" onClick=\"select_sort_btn(this);false\" (click)=\"sort_by_likes('month');false\" translate>This Month</a>\n                <a href=\"#\" onClick=\"select_sort_btn(this);false\" (click)=\"sort_by_likes('all');false\" translate>All Time</a>\n              </div>\n            </div>\n          </div>\n\n          <app-post-box *ngFor=\"let thot of thoughts; index as i\" [thot]=\"thot\" [user]=\"user\" [index]=\"i\" (delete)=\"delete($event)\" (openThotBox)=\"openThotBox($event)\"></app-post-box>\n\n          <sk-three-bounce *ngIf=\"loading == true\"></sk-three-bounce>\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/tagfeeds/tagfeeds.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TagfeedsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__lightbox_lightbox_service__ = __webpack_require__("./src/app/lightbox/lightbox.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_event_service__ = __webpack_require__("./src/app/core/event.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__core_event_types__ = __webpack_require__("./src/app/core/event.types.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_Subject__ = __webpack_require__("./node_modules/rxjs/_esm5/Subject.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var TagfeedsComponent = (function () {
    function TagfeedsComponent(appService, authService, eventService, route, location, _lightbox) {
        this.appService = appService;
        this.authService = authService;
        this.eventService = eventService;
        this.route = route;
        this.location = location;
        this._lightbox = _lightbox;
        this.thoughts = [];
        this.albumList = [];
        this.user = new __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]();
        this.tagname = '';
        this.tagfeeds_api_url = '/api/tagfeeds/';
        this.sorting_algorithm = 'hot';
        this._requestID = "tagfeeds";
        this.loading = true;
        this.select_most_liked = false;
    }
    TagfeedsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = this.authService.user;
        this.eventSubscription = this.eventService.subscribe({
            next: function (event) {
                if (event.type === __WEBPACK_IMPORTED_MODULE_8__core_event_types__["b" /* BelaEventType */].DELETED_POST) {
                    _this.delete(event.value);
                }
                else if (event.type == __WEBPACK_IMPORTED_MODULE_8__core_event_types__["b" /* BelaEventType */].UPDATE_USER) {
                    _this.user = event.value;
                }
                else if (event.type === __WEBPACK_IMPORTED_MODULE_8__core_event_types__["b" /* BelaEventType */].REQUEST_NEXT_ALBUM &&
                    event.value.requestID == _this._requestID) {
                    _this._requestAlbum(event.value);
                }
            }
        });
        this.route.params
            .subscribe(function (params) {
            _this.tagname = params['tagname'];
            _this.tagfeeds_api_url = "/api/tagfeeds/" + _this.tagname + "/";
            jQuery('#mobileinfo').removeClass("slidein").addClass("slideout");
            jQuery('#search').removeClass("open");
            jQuery('.modal').modal('hide'); // Hide any opened modal.
            _this._lightbox.close_all();
            _this.thoughts = [];
            _this.loading = true;
            _this.loadPhotos(_this.sorting_algorithm, false).subscribe(function () {
                _this.loading = false;
            });
            window.scroll(0, 0);
        });
    };
    TagfeedsComponent.prototype.ngOnDestroy = function () {
        this.eventSubscription.unsubscribe();
    };
    TagfeedsComponent.prototype.onFeedScroll = function () {
        var _this = this;
        var bottom_reached = this.appService.isBottomReached();
        if (this.tagfeeds_api_url != null && bottom_reached && !this.loading) {
            this.loading = true;
            this.loadPhotos(this.sorting_algorithm, true)
                .subscribe(function () {
                _this.loading = false;
            }, function (err) {
                _this.loading = false;
            });
        }
    };
    TagfeedsComponent.prototype.loadPhotos = function (param, isAppending) {
        var _this = this;
        if (isAppending === void 0) { isAppending = false; }
        if (!isAppending) {
            this.tagfeeds_api_url = "/api/tagfeeds/" + this.tagname + "/";
            this.thoughts = [];
            if (this.pendingAPI) {
                this.pendingAPI.subscription.unsubscribe();
                this.pendingAPI.callback.complete();
            }
        }
        var sorting_algorithm = null;
        if (!isAppending) {
            this.sorting_algorithm = sorting_algorithm = param;
        }
        var callback = new __WEBPACK_IMPORTED_MODULE_9_rxjs_Subject__["b" /* Subject */]();
        var subscription = this.appService.getPhotoList(this.tagfeeds_api_url, sorting_algorithm)
            .subscribe(function (listResp) {
            _this.tagfeeds_api_url = listResp.next;
            if (isAppending) {
                for (var _i = 0, _a = listResp.results; _i < _a.length; _i++) {
                    var result = _a[_i];
                    _this.thoughts.push(result);
                }
            }
            else {
                _this.thoughts = listResp.results;
                window.scroll(0, 0);
            }
            _this.buildAlbum();
            callback.next();
            callback.complete();
        });
        this.pendingAPI = {
            callback: callback,
            subscription: subscription
        };
        return callback;
    };
    TagfeedsComponent.prototype.sort_by_hot = function () {
        var _this = this;
        this.select_most_liked = false;
        this.loading = true;
        this.loadPhotos('hot').subscribe(function () {
            _this.loading = false;
        });
    };
    TagfeedsComponent.prototype.sort_by_new = function () {
        var _this = this;
        this.select_most_liked = false;
        this.loading = true;
        this.loadPhotos('new').subscribe(function () {
            _this.loading = false;
        });
    };
    TagfeedsComponent.prototype.sort_by_likes = function (param) {
        var _this = this;
        if (param === void 0) { param = 'today'; }
        this.select_most_liked = true;
        this.loadPhotos('time_' + param).subscribe(function () {
            _this.loading = false;
        });
    };
    TagfeedsComponent.prototype.delete = function (thot) {
        var idx = this.thoughts.indexOf(thot);
        if (idx > -1) {
            this.thoughts.splice(idx, 1);
        }
        this.buildAlbum();
    };
    TagfeedsComponent.prototype.openThotBox = function (index) {
        if (index >= 0) {
            this._lightbox.openPost(this.albumList, index, {
                disableKeyboardNav: true,
                wrapAround: false,
                allowRequestAlbum: true,
                requestID: this._requestID
            });
        }
    };
    TagfeedsComponent.prototype.buildAlbum = function () {
        this.albumList = [];
        for (var j = 0; j < this.thoughts.length; j++) {
            var _album = {
                src: this.thoughts[j].image,
                caption: this.thoughts[j].content,
                thumb: this.thoughts[j].image,
                thot: this.thoughts[j]
            };
            this.albumList.push(_album);
        }
    };
    TagfeedsComponent.prototype._requestAlbum = function (request) {
        var _this = this;
        this.loading = true;
        if (this.tagfeeds_api_url == null) {
            var event = new __WEBPACK_IMPORTED_MODULE_8__core_event_types__["a" /* BelaEvent */]();
            event.type = __WEBPACK_IMPORTED_MODULE_8__core_event_types__["b" /* BelaEventType */].NO_NEXT_ALBUM;
            event.value = null;
            this.eventService.fireEvent(event);
        }
        else {
            this.loadPhotos(this.sorting_algorithm, true).subscribe(function () {
                var evValue = new __WEBPACK_IMPORTED_MODULE_8__core_event_types__["c" /* EvLoadedAlbum */]();
                evValue.album = _this.albumList;
                evValue.newImageIndex = request.currentImageIndex + 1;
                var event = new __WEBPACK_IMPORTED_MODULE_8__core_event_types__["a" /* BelaEvent */]();
                event.type = __WEBPACK_IMPORTED_MODULE_8__core_event_types__["b" /* BelaEventType */].LOADED_NEXT_ALBUM;
                event.value = evValue;
                _this.loading = false;
                _this.eventService.fireEvent(event);
            }, function (err) {
                _this.loading = false;
            });
        }
    };
    return TagfeedsComponent;
}());
TagfeedsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-tagfeeds',
        template: __webpack_require__("./src/app/tagfeeds/tagfeeds.component.html"),
        styles: [__webpack_require__("./src/app/tagfeeds/tagfeeds.component.css")],
        providers: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_6__core_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_auth_service__["a" /* AuthService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_7__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_event_service__["a" /* EventService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__angular_common__["Location"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_common__["Location"]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__lightbox_lightbox_service__["a" /* Lightbox */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__lightbox_lightbox_service__["a" /* Lightbox */]) === "function" && _f || Object])
], TagfeedsComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=tagfeeds.component.js.map

/***/ }),

/***/ "./src/app/terms/terms.component.html":
/***/ (function(module, exports) {

module.exports = "<script src=\"https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.js\"></script>\n\n<div class=\"user-profile-contain\">\n\n  <div class=\"mid-con\">\n    <div class=\"container\">\n\n      <div class=\"row\">\n\n        <div class=\"col-sm-3 col-lg-3 col-md-3 user-profile-left\">\n\n        </div>\n\n        <div class=\"col-sm-6 col-lg-6 col-md-6 middle-section\">\n\n\n          <div class=\"discover-box\">\n            <div class=\"panel with-nav-tabs \">\n\n              <h1 translate>Terms and Conditions</h1>\n\n              <h3 translate>Belacam Terms and Conditions of Use v. 3.0 <br>These Terms of Use are effective on 07/20/18 for Belacam\n\n              </h3>\n\n              <p translate>By engaging with and utilizing the Belacam website or any applications (including future mobile applications) created by Belacam (together, the \"Platform\"), you agree to be bound by these terms of use (\"Terms of Use\"). The Platform is operated and owned by Belacam, Inc (\"Belacam\"). These Terms of Use affect your legal rights and obligations. If you do not agree to be bound by all of these Terms of Use, do not engage with the Platform. There will be times after the full Belacam launch when a new device or feature is enabled that has its own terms and conditions that represents an additional contract between the user and Belacam aside from the normal Terms of Use. \n</p>\n              <h2 translate>Basic Terms</h2>\n\n              <h3 translate>Eligibility and Accounts</h3>\n\n              <p translate>You must be at least 13 years old to use the Platform. You agree that you will not manipulate, exchange, or license your account rights, username, or follower-base. This excludes individuals or organizations that are explicitly permitted to create accounts for their clients. Aside from this, you agree that you will not create an account for any individual other than yourself or impersonate another individual via an account. You maintain that all information provided to Belacam upon account creation and at all times will be accurate and current. You agree to refrain from falsifying information in an attempt to obscure the identity of your account. You are responsible for privacy and security of your password. Do not tell others your login credentials. Belacam is not liable for any damages related to or stemming from a compensation in account security. You are prohibited from using use domain names or web URLs in/as your username without prior confirmed written consent from Belacam. You must not use an automated device (e.g. bot, script, etc.) or other unauthorized means to create accounts with the Platform. If you the individual, a human being who matches your given identification, does not create the account, it is considered spam. Belacam administration may reclaim any username for any reason without explanation.</p>\n\n              <h3 translate>Belacam User Conduct</h3>\n              <p translate>You are solely responsible for your conduct on the Platform, as well as any “Content” your account produces. “Content” includes photos, other images or graphics, text/comments/messages, links, information, sounds, original works of art, GIFs and video clips, and any other content or materials that you can publicize via the Platform. You agree that you will not mine, phish, solicit or otherwise obtain/use the login credentials and other account information of other Belacam users. You must not degrade, harass, stalk, threaten, blackmail, spam (submit/conduct unwanted interaction or content) or intimidate people or other entities through any means granted within the Platform including without limitation comments, messages, captions, and other shareable content. You are prohibited from using the Platform for any illegal goals or intentions. You agree to comply with all policy and regulations (for example, federal, provincial, local) applicable to your use of the Platform and your Content, including but not limited to, copyright laws, prostitution, internet piracy, narcotics transmission, excessive gore, nudity (for now), organized crime, terrorism, hate campaigns, etc. Belacam reserves the right to remove any Content from the Platform for any reason, without prior notice. Content removed from Belacam may continue to be stored by Belacam. Belacam will not be liable for any modification, suspension, or discontinuation of the Platform, or the loss of any Content. You also acknowledge that the internet applications may be subject to breaches of security and that the submission of Content or other information may not be secure. You must not attempt to change, modify, adapt, attack, hack, spam or otherwise alter the Platform by use of means including but not limited to DDOS attack, transmission of spyware, malware, worms, viruses, and all other forms of malicious activity available to disrupt the stability of the Platform. If you do participate in any of the above operations or any other kind of hacking/injecting, you will be prosecuted to the full extent under the law. You are prohibited from restricting another user’s enjoyment of the Platform. This includes encouraging other users to violate the Belacam Terms Conditions of Use. You agree to refrain from changing or founding another website so as to impersonate Belacam. You agree to first consult Belacam support for any questions or concerns pertaining to your account. Violation of these Terms of Use can promote limitation, suspension, or termination of your Belacam account by our administration team and loss of Bela that your account earned while committing acts in violation of our terms. You understand that Belacam, Inc will not be liable for any Content posted and you agree to use Belacam at your own risk and discretion. Any creation of legal risk for Belacam, Inc such as a violation of any clause of the Terms of Use or any other legal controversy may result in termination of an account. If your account on the Platform is suspended or terminated, all rights granted to you in these Terms of Use become void. You are no longer a user of Belacam and are therefore no longer subject to any of its Terms of Use or Rights.</p>\n                <p translate>For completing certain tasks, some accounts may be rewarded with Bela credited to their account. For example, some accounts may receive free Bela upon sign up or when they refer other users. However, if you try to game these systems by creating or referring fake accounts — often characterized as multiple new accounts created by an individual or a team solely for the purpose of taking advantage of our free Bela programs — or gaming the system in any other way then you may lose any and all Bela in your account. To avoid this, please do not create multiple Belacam accounts. If you do create multiple accounts for the purpose of posting content in good-faith, keeping those accounts updated, unique, and active is a good way to make it clear that you are not gaming the system.\n                \n                </p>\n\n              <h3 translate>General Conditions</h3>\n\n              <p translate>We reserve the right to alter, suspend, or terminate the Platform or your account within the Platform at any time without notice or liability to the user. If your Belacam account is terminated or you deactivate your account, all content and interactions including photos, Bela transactions, followers and those you follow, comments, messages, and other data will no longer be visible within your account. In addition, other users will not be able to access this content by searching your username or otherwise engaging with your old account through tags or tracing other interactions. Belacam administrators reserve the right to change these Terms of Use from time to time according to need. You agree that we may notify you of an update to the Terms of Use, often through a message within the Platform. If we do not require a user review of the terms of use, your interaction with the platform after the effective date of the update confirms your compliance to the alterations of the policy. It is important that you thoroughly review these Terms of Use and any Updated Terms before interacting with the Platform. Belacam administrators are permitted to monitor or become involved in disputes between you and other users, acting in the best interest for the community of the Belacam Platform. There will be links from the Platform’s posted content, as well as messages and other interactions relating to third-party web sites or features created by businesses, organizations, and individuals. You agree that Belacam does not condone nor takes liability for any third-party services or features. You understand that your interactions and relationship with third parties found on Belacam are strictly between you and the third parties and do not include Belacam in any way. We have no responsibility for the results of these interactions. You agree that the platform is not responsible for all data charges you will incur through use of the Platform. Belacam strictly prohibits caching, phishing, scraping or otherwise accessing any content on the Platform via automated means, including but not limited to, user profiles and photos. The only exception for this clause will be data extraction as the result of analytics methods or technologies used by a search engine with Belacam's express consent.</p>\n              <p translate>\n               Belacam will have occasions when the Platform is not online or has bugs. Main reasons for this include breakdown of telecommunications equipment, site maintenance, or addition of new features. You agree that you will be patient with the system as it will typically be back online within a reasonable amount of time. You agree that Belacam is not responsible for Content posted on the Platform. We will not be liable for any use of Content by users inside or outside of the platform. This includes screenshots and other references to Content produced. You acknowledge and agree that your relationship with Belacam is not any kind of special relationship other than that that the public typically maintains with Belacam.</p>\n                <p translate>When withdrawing Bela, please allow the Belacam team 48 hours to verify and process your withdrawal. We often manually verify withdrawals to make sure that there are no errors or withdrawal hacks that could drain users’ funds, and this added security takes some extra time. Only supply us with an ERC20 Bela compatible address. Once we process and send your Bela from your withdrawal, we cannot reverse the transaction, so we rely on you to provide accurate information regarding your withdrawal address.</p>\n\n              <p translate>You hereby comply that Belacam may place advertising and promotions on the Service or on, about, or in conjunction with your Content and data garnered by Belacam data extraction. The extent and methodology of Belacam advertisements may or may not change without notice to users. </p>\n\n              <h3 translate>Disclaimers</h3>\n\n              <p translate>You agree that the risk and liability of using Belacam rests on the user alone. In coordination with the General Terms, you agree that your conduct on the Platform is fully legal in the jurisdiction in which you utilize Belacam. Belacam, Inc. does not claim that the Platform will be perfect or bug-free. Belacam also does not maintain that the service will be free from illegal and dangerous devices such as spy-ware or viruses. However, the administration does ensure that defects and harmful devices will be resolved/removed within full capacity of the team. Belacam, Inc. expressly disclaims all responsibility for any damages resulting from content or interactions within the system. These damages, applicable to both organizations and individuals include but are not limited to legal consequences, loss of reputation, and actual damages.This includes claims relating to broken Bela transactions (mistyped address, etc.), forgotten or stolen login credentials, and other unauthorized third party conduct on the Platform.\n              </p>\n\n              <h3 translate>Indemnification</h3>\n\n              <p translate>You or your representative third-party agree to indemnify and hold Belacam, Inc safe and free-of-harm from any expenses, losses or damages. This includes without limitation, all legal fees and attorney rates from damages affiliated with breach of account security, content posted by your account, interactions with other users or connected to any facet of the platform.\n\n              </p>\n\n              <h2 translate>Arbitration and Time Limitations</h2>\n\n              <p>\n                <strong translate>By agreeing to the Terms and Conditions of Belacam, you waive the right to filing or participating in a class-action lawsuit against Belacam, Inc, Live Bela LLC, or The Ambia Fund LLC. You agree to settle all legal disputes via individual arbitration between Belacam, Inc and the user. You may bring claims only on your own behalf. You will not participate in any class-action arbitration claims. You also agree not to participate in claims brought in a private attorney general or representative capacity, or consolidated claims involving another person's account, if Belacam is a party to the proceeding. All claims must be brought to Belacam, Inc within ninety (90) days of the original incident or conflict. After this time has passed, all individual arbitration claims are permanently barred and void.\n</strong> </p>\n                \n                <h3 translate>DMCA Takedown Notice Policy</h3>\n                <h2>DMCA</h2>\n                <p translate>Belacam has adopted the following policy regarding copyright infringement in accordance with the Digital Millennium Copyright Act (\"http://http://lcweb.loc.gov/copyright/legislation/dmca.pdf\"). Contact information for the Designated Agent to receive the DMCA takedown notice of Claimed Infringement (\"Designated Agent\") is listed at the end of this policy.</p>\n                <h2 translate>PROCEDURE FOR REPORTING COPYRIGHT INFRINGEMENT:</h2>\n                <p translate>If you have firm belief that content accessible through the Belacam platform infringes a copyright, please send a notice of copyright infringement containing all the following information to the Designated Agent listed at the end of this section:</p>\n                <p translate>1. An electronic or physical signature of the owner of alleged copyrighted content or a person authorized to act on behalf of the owner (“claiming party”) of the copyright that has been allegedly infringed;\n</p>\n         <p translate>2. A location within the Belacam platform where the copyright infringement is present (a URL of the content and usernames of any/all users who shared copyrighted content) as well as a description of the copyrighted content—we must be able to find the content;\n</p>      \n                 <p translate>3. Contact details of the claiming party filing the takedown notice (name, email address, telephone number, and physical mailing address;\n</p>       \n<p translate>4. A statement confirming that the claiming party has “good faith” that they have not authorized the use of the copyrighted material; </p>\n                <p translate>5. A statement made under penalty of perjury that all supplied details regarding the alleged copyrighted content, authorization of any claiming 3rd party to act on behalf of owner of copyrighted content, contact details of the claiming party, and any other above information is accurate.</p>\n                <p translate>If we receive a takedown notice in accordance with the above requirements, Belacam will attempt to notify the user(s) that uploaded content infringing upon the copyright that we will remove the content in question. Any of these users are allowed to deliver a counter-notice to Belacam (see procedure below). Habitual offenders that upload copyrighted content on a repeated basis will be disabled and swiftly removed.</p>\n    \n                <h3 translate>PROCEDURE TO DELIVER COUNTER-NOTICE:\n</h3>\n                <p translate>Any user that is confident that they have the right to use the removed content or that the content removed is not infringing on a copyright owned by copyright owner, the copyright owner’s agent, or pursuant to the law, the user may send a counter-notice comprised of the information below to the Designated Agent listed at the end of the section:</p>\n                <p translate>1. A physical or electronic signature of the user; <br>\n2. A location within the Belacam platform where the content existed before it was removed (a URL of the content) as well as a description of the removed content—we must be able to find the content; <br>\n3. A statement claiming that the user has “good faith” that the content was removed due to an inaccurate understanding of the content’s identity or other mistake on the part of the Belacam team or claiming party; <br>\n4. Contact details of the user filing the counter-notice (name, email address, telephone number, and physical mailing address); <br>\n5. A statement consenting to the jurisdiction of the Federal Court for the district in which the user resides, or if the user resides outside of the United States, for the laws and regulations of the State of Delaware; <br>\n6. A statement accepting service of process from the claiming party of the alleged infringement.</p>\n                <p translate>If a counter-notice is received by the Designated Agent, Belacam will forward a copy of the counter-notice to the claiming party informing that Belacam may restore the content in question 10 days after the counter-notice has been received. The claiming party then must notify Belacam within those 10 days that the claiming party has filed further legal action relating to the copyrighted content in question. If Belacam does not receive the notification of legal action within that 10 day period, we may restore the content to Belacam the platform.\n</p>\n                <p translate>Please contact the Designated Agent for Belacam at mail@ambiafund.com or at: <br>\nBelacam, Inc. <br>\n1038 Wertland St. Ste. A <br>\nCharlottesville, VA 22903\n</p>\n                <h3 translate>These Terms of Use are drafted in accordance with the laws of the State of Delaware. </h3>\n\n              <p translate>The effective date of these Terms of Use is February 20th, 2018. The Terms of Use are subject to substantial change after the Gamma Early Access testing phase, and users may not be notified when changes are made.</p>\n\n            </div>\n          </div>\n\n        </div>\n\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/terms/terms.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TermsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TermsComponent = (function () {
    function TermsComponent() {
        this.stuff = [1, 2, 3, 4, 5];
    }
    return TermsComponent;
}());
TermsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("./src/app/terms/terms.component.html"),
        providers: []
    }),
    __metadata("design:paramtypes", [])
], TermsComponent);

//# sourceMappingURL=terms.component.js.map

/***/ }),

/***/ "./src/app/timesince/timesince.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/timesince/timesince.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"discover-box\">\n  <div class=\"panel with-nav-tabs\">\n\n    <div class=\"panel-body\">\n\n      <div class=\"tab-content\">\n\n        <div class=\" fade in active endless_page_template\">\n          <app-post-box\n            *ngFor=\"let thot of thoughts; index as i \"\n            [thot]=\"thot\"\n            [user]=\"requesting_user\"\n            [index]=\"i\"\n            (openThotBox)=\"openThotBox($event)\"\n          ></app-post-box>\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/timesince/timesince.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimesinceComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__lightbox_lightbox_service__ = __webpack_require__("./src/app/lightbox/lightbox.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_event_types__ = __webpack_require__("./src/app/core/event.types.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__core_event_service__ = __webpack_require__("./src/app/core/event.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var TimesinceComponent = (function () {
    function TimesinceComponent(appService, eventService, authService, route, _lightbox, location) {
        this.appService = appService;
        this.eventService = eventService;
        this.authService = authService;
        this.route = route;
        this._lightbox = _lightbox;
        this.location = location;
        this.thoughts = [];
        this.album = [];
        this.requesting_user = new __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]();
        this.subscriptions = [];
        this.posting_api_url = '/api/post/';
        this.post_id = 1;
    }
    TimesinceComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.requesting_user = this.authService.user;
        this.subscriptions.push(this.eventService.subscribe({
            next: function (event) {
                if (event.type == __WEBPACK_IMPORTED_MODULE_7__core_event_types__["b" /* BelaEventType */].UPDATE_USER) {
                    _this.requesting_user = event.value;
                }
            },
        }));
        this.route.params
            .subscribe(function (params) {
            _this.post_id = params['post_id'];
            _this.posting_api_url = '/api/post/' + _this.post_id + '/';
            _this.thoughts = [];
            _this.appService.getList(_this.posting_api_url).subscribe(function (listResp) {
                _this.posting_api_url = listResp.next;
                _this.thoughts = listResp.results;
                _this._buildAlbum();
            });
            window.scroll(0, 0);
        });
    };
    TimesinceComponent.prototype.ngOnDestroy = function () {
        for (var _i = 0, _a = this.subscriptions; _i < _a.length; _i++) {
            var subscription = _a[_i];
            subscription.unsubscribe();
        }
    };
    TimesinceComponent.prototype.openThotBox = function (index) {
        if (index >= 0) {
            this._lightbox.openPost(this.album, index, {
                disableKeyboardNav: true,
                wrapAround: false,
                allowRequestAlbum: true
            });
        }
    };
    TimesinceComponent.prototype._buildAlbum = function () {
        this.album = [];
        for (var j = 0; j < this.thoughts.length; j++) {
            var _album = {
                src: this.thoughts[j].image,
                caption: this.thoughts[j].content,
                thumb: this.thoughts[j].image,
                thot: this.thoughts[j]
            };
            this.album.push(_album);
        }
    };
    return TimesinceComponent;
}());
TimesinceComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-tagfeeds',
        template: __webpack_require__("./src/app/timesince/timesince.component.html"),
        styles: [__webpack_require__("./src/app/timesince/timesince.component.css")],
        providers: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_8__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__core_event_service__["a" /* EventService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* AuthService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__lightbox_lightbox_service__["a" /* Lightbox */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__lightbox_lightbox_service__["a" /* Lightbox */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_4__angular_common__["Location"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_common__["Location"]) === "function" && _f || Object])
], TimesinceComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=timesince.component.js.map

/***/ }),

/***/ "./src/app/top-search/top-search.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/top-search/top-search.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"top-search-box\">\n  <div class=\"panel with-nav-tabs\">\n\n    <div class=\"panel-body\">\n\n      <div class=\"tab-content\">\n\n        <div class=\"white-box\">\n          <h3>Users</h3>\n          <ng-container *ngFor=\"let item of items\">\n            <a href=\"#\" *ngIf=\"item.type=='relevant_user'\" [routerLink]=\"['/user/', item.username]\">\n              <li class=\"clearfix\">\n                <span class=\"item-user-info\"><span class=\"item-fullname\">{{ item.full_name }}</span> <span\n                    class=\"item-username\">@{{ item.username }}</span></span>\n                <span class=\"pullright\">(${{ item.earnings|number:'1.2-2' }} earned)</span>\n              </li>\n            </a>\n          </ng-container>\n        </div>\n\n        <div class=\"white-box\">\n          <h3>Hashtags</h3>\n          <ng-container *ngFor=\"let item of items\">\n            <a href=\"#\" *ngIf=\"item.type=='relevant_tag'\" [routerLink]=\"['/discover/', item.name]\">\n              <li class=\"clearfix\">#{{ item.name }}<span\n                  class=\"pullright\">({{ item.num_items }} {{ 'posts'|translate }}) </span></li>\n            </a>\n          </ng-container>\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/top-search/top-search.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TopSearchComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TopSearchComponent = (function () {
    function TopSearchComponent(appService, route) {
        this.appService = appService;
        this.route = route;
        this.items = [];
        this.top_search_api_url = '/api/searchtop/';
        this.tagname = "";
    }
    TopSearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params
            .subscribe(function (params) {
            _this.tagname = params['tagname'];
            var search_in = "all";
            if (_this.tagname == "" || _this.tagname === undefined) {
                search_in = "all";
                _this.tagname = "";
            }
            else if (_this.tagname.charAt(0) == "#") {
                _this.tagname = _this.tagname.slice(1);
                // #docregion search-parameters
                search_in = 'tag';
            }
            else if (_this.tagname.charAt(0) == "@") {
                _this.tagname = _this.tagname.slice(1);
                // #docregion search-parameters
                search_in = 'user';
            }
            _this.appService.getTopSearchList(_this.top_search_api_url, _this.tagname, search_in).then(function (listResp) {
                _this.items = listResp;
            });
        });
    };
    return TopSearchComponent;
}());
TopSearchComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-top-search',
        template: __webpack_require__("./src/app/top-search/top-search.component.html"),
        styles: [__webpack_require__("./src/app/top-search/top-search.component.css")],
        providers: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _b || Object])
], TopSearchComponent);

var _a, _b;
//# sourceMappingURL=top-search.component.js.map

/***/ }),

/***/ "./src/app/trade/trade.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/trade/trade.component.html":
/***/ (function(module, exports) {

module.exports = "<a href=\"https://mercatox.com/?referrer=308520\" class=\"gotologin trade_bela_btn\"  target=\"_blank\"><h3 class=\"center\" translate>Trade Bela</h3></a>\n<p class=\"reftext\" translate>Bela is a high-risk / high-reward cryptocurrency traded by thousands of people. It's also one of the few cryptos that has a real, successful product already available for use: Belacam.\n</p>\n\n<a href=\"https://mercatox.com/?referrer=308520\" target=\"_blank\"><img class=\"trade_bela_image\" src=\"https://www.tradingview.com/x/B2dS133o\" /></a>\n\n<p class=\"reftext\" translate>As more people use Belacam, they'll continuously need Bela to like photos. With about 40 million Bela in existence, there is a limited amount of Bela that can be split among all Belacam users. As more people use Belacam and buy Bela, Bela gets more scarce.\n</p>\n\n<p class=\"reftext\" translate>If you're up for trading Bela, click the button below to start.\n</p>\n\n<a href=\"https://mercatox.com/?referrer=308520\" class=\"savebutton referral-btn trade_bela_btn\"  target=\"_blank\"><h3 class=\"center\" translate>Click Here to Trade Bela</h3></a>\n<br><br>\n<h3 class=\"center\">ADVANCED: Earning Interest on Bela</h3>\n<p class=\"reftext\" translate>Once you own Bela, you can earn more Bela per month through a process called <em>staking</em>. By locking away your Bela for 30 days, you are provided with more Bela  at the end of that staking period. This is to reward users that hold Bela for a long-term period of time, which decreases the real supply and helps increase Bela's price.\n</p>\n\n<img src=\"https://www.livebela.com/assets/img/staking.png\" alt=\"Stake ERC20 Bela\" />\n\n<a href=\"https://medium.com/livebela/how-to-stake-bela-in-mew-and-mist-wallets-957e1749b028\" class=\"savebutton referral-btn trade_bela_btn\"  target=\"_blank\"><h3 class=\"center\" translate>How to Stake Bela</h3></a>\n"

/***/ }),

/***/ "./src/app/trade/trade.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TradeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TradeComponent = (function () {
    function TradeComponent() {
    }
    TradeComponent.prototype.ngOnInit = function () {
    };
    return TradeComponent;
}());
TradeComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-trade',
        template: __webpack_require__("./src/app/trade/trade.component.html"),
        styles: [__webpack_require__("./src/app/trade/trade.component.css")]
    }),
    __metadata("design:paramtypes", [])
], TradeComponent);

//# sourceMappingURL=trade.component.js.map

/***/ }),

/***/ "./src/app/trending/growth.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"white-box totalusers\">\n  <h4 translate>Belacam Growth</h4>\n\n  <p><span>{{growth.total_users|number}}</span> <ng-container translate>Total Belacam Users</ng-container></p>\n  <p><span>{{growth.new_users_in_7|number}}</span> <ng-container translate>New Users in past 7 Days</ng-container></p>\n  <p><span>{{growth.new_users_in_1|number}}</span> <ng-container translate>New Users in past 24 Hours</ng-container></p>\n</div>\n"

/***/ }),

/***/ "./src/app/trending/suggested.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"white-box\">\n  <h4 translate>Earn Bela</h4>\n  <div class=\"trendingbox trendingbox-padding\">\n    <form class=\"feedbackform\">\n      <p translate>\n        We'll pay you for each new user you refer to Belacam. Have anyone in mind?</p>\n      <a onclick=\"closeMobileProfile()\" class=\"savebutton referral-btn\" href=\"#\" [routerLink]=\"['/referral/dashboard']\" translate>Referral Dashboard</a>\n    </form>\n  </div>\n</div>\n\n<div class=\"white-box\">\n  <h4 translate>Top Likers</h4>\n  <div class=\"trendingbox\">\n\n    <div class=\"panel-title\">\n      <a href=\"#\" *ngFor=\"let thot of suggested\" [routerLink]=\"['/user/', thot.username]\">\n        <div  class=\"suggestedperson\">\n\n\n          <span class=\"circlephoto\" [innerHTML]=\"thot.avatar\"></span>\n          <div class=\"nameanduser\">\n            <p class=\"name\">{{ thot.username }}</p>\n            <p class=\"count1200\">{{thot.likes_24hrs}} BELA</p>\n          </div>\n\n          <span class=\"pull-right\">{{thot.likes_24hrs}} BELA</span>\n\n        </div>\n      </a>\n    </div>\n\n  </div>\n</div>\n\n<div class=\"white-box\">\n  <h4 translate>Top Earners</h4>\n\n  <div class=\"trendingbox\">\n\n    <div class=\"panel-title\">\n      <a href=\"#\" *ngFor=\"let earner of earners\" [routerLink]=\"['/user/', earner.username]\">\n        <div class=\"suggestedperson\">\n\n          <span class=\"circlephoto\" [innerHTML]=\"earner.avatar\"></span>\n          <div class=\"nameanduser\">\n            <p class=\"name\">{{ earner.username }}</p>\n            <p class=\"count1200\">${{earner.earnings|number:'1.2-2'}}</p>\n          </div>\n          <span class=\"pull-right\">${{earner.earnings|number:'1.2-2'}}</span>\n\n        </div>\n      </a>\n    </div>\n\n  </div>\n</div>\n\n<div class=\"white-box totalusers\">\n  <h4 translate>Notices</h4>\n\n  <div class=\"trendingbox\">\n    <p>Chat with other Belacam users and supporters in our Telegram.</p><a class=\"telegram_link\" href=\"https://t.me/letslivebela\" target=\"_blank\"><i class=\"fab fa-telegram-plane\"></i></a>\n  </div>\n</div>\n\n<app-earn-free-bela></app-earn-free-bela>\n\n<ul class=\"rightsidelinks\">\n  <li><a onclick=\"closeMobileProfile()\" [routerLink]=\"['/about']\" href=\"#\" translate>About</a></li>\n  <li><a href=\"https://belacam.zendesk.com/hc/en-us\" target=\"_blank\" translate>Support</a></li>\n  <li><a onclick=\"closeMobileProfile()\" [routerLink]=\"['/privacy']\" href=\"#\" translate>Privacy</a></li>\n  <li><a onclick=\"closeMobileProfile()\" [routerLink]=\"['/terms']\" href=\"#\" translate>Terms</a></li>\n  <li><a onclick=\"closeMobileProfile()\" [routerLink]=\"['/deposit']\" href=\"#\" translate>Deposit</a></li>\n  <li><a onclick=\"closeMobileProfile()\" [routerLink]=\"['/withdrawal']\" href=\"#\" translate>Withdraw</a></li>\n</ul>\n\n\n"

/***/ }),

/***/ "./src/app/trending/trending.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/trending/trending.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"white-box\">\n  <h4 translate>Trending</h4>\n  <div class=\"trendingbox\">\n    <ul>\n\n      <a *ngFor=\"let tag of trending\" class=\"trendinglink\" [routerLink]=\"['/discover/', tag.name]\">\n\n        <li>\n          <span class=\"lightningbolt\">\n            <i class=\"fas fa-hashtag lightningbolt\"></i>\n          </span>\n          <div class=\"trendinginfo\">\n            <h5><span class=\"hashtag\">#</span>{{ tag.name }}</h5>\n            <p>{{ tag.num_items}} {{'posts'|translate}}</p>\n\n          </div>\n        </li>\n      </a>\n    </ul>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/trending/trending.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return TrendingComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return SuggestedComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GrowthComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_model__ = __webpack_require__("./src/app/app.model.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TrendingComponent = (function () {
    function TrendingComponent() {
        this.trending = [];
    }
    TrendingComponent.prototype.ngOnInit = function () {
    };
    return TrendingComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], TrendingComponent.prototype, "trending", void 0);
TrendingComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-trending',
        template: __webpack_require__("./src/app/trending/trending.component.html"),
        styles: [__webpack_require__("./src/app/trending/trending.component.css")]
    })
], TrendingComponent);

var SuggestedComponent = (function () {
    function SuggestedComponent() {
        this.suggested = [];
        this.earners = [];
    }
    SuggestedComponent.prototype.ngOnInit = function () {
    };
    return SuggestedComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], SuggestedComponent.prototype, "suggested", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], SuggestedComponent.prototype, "earners", void 0);
SuggestedComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-suggested',
        template: __webpack_require__("./src/app/trending/suggested.component.html")
    })
], SuggestedComponent);

var GrowthComponent = (function () {
    function GrowthComponent() {
        this.growth = new __WEBPACK_IMPORTED_MODULE_1__app_model__["a" /* GrowthStatistics */]();
    }
    GrowthComponent.prototype.ngOnInit = function () {
    };
    return GrowthComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__app_model__["a" /* GrowthStatistics */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__app_model__["a" /* GrowthStatistics */]) === "function" && _a || Object)
], GrowthComponent.prototype, "growth", void 0);
GrowthComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-growth',
        template: __webpack_require__("./src/app/trending/growth.component.html")
    })
], GrowthComponent);

var _a;
//# sourceMappingURL=trending.component.js.map

/***/ }),

/***/ "./src/app/typeahead/models.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Key; });
var Key;
(function (Key) {
    Key[Key["Backspace"] = 8] = "Backspace";
    Key[Key["Tab"] = 9] = "Tab";
    Key[Key["Enter"] = 13] = "Enter";
    Key[Key["Shift"] = 16] = "Shift";
    Key[Key["Escape"] = 27] = "Escape";
    Key[Key["ArrowLeft"] = 37] = "ArrowLeft";
    Key[Key["ArrowRight"] = 39] = "ArrowRight";
    Key[Key["ArrowUp"] = 38] = "ArrowUp";
    Key[Key["ArrowDown"] = 40] = "ArrowDown";
    // http://unixpapa.com/js/key.html
    Key[Key["MacCommandLeft"] = 91] = "MacCommandLeft";
    Key[Key["MacCommandRight"] = 93] = "MacCommandRight";
    Key[Key["MacCommandFirefox"] = 224] = "MacCommandFirefox";
})(Key || (Key = {}));
//# sourceMappingURL=models.js.map

/***/ }),

/***/ "./src/app/typeahead/typeahead.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TypeaheadComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models__ = __webpack_require__("./src/app/typeahead/models.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__typeahead_util__ = __webpack_require__("./src/app/typeahead/typeahead.util.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__search_search_service__ = __webpack_require__("./src/app/search/search.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TypeaheadComponent = (function () {
    function TypeaheadComponent(element, viewContainer, cdr, searchService, ngModel) {
        this.element = element;
        this.viewContainer = viewContainer;
        this.cdr = cdr;
        this.searchService = searchService;
        this.ngModel = ngModel;
        this.showSuggestions = false;
        this.taSelected = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.suggestionIndex = 0;
    }
    TypeaheadComponent.prototype.handleEsc = function (event) {
        if (event.keyCode === __WEBPACK_IMPORTED_MODULE_2__models__["a" /* Key */].Escape) {
            this.hideSuggestions();
            event.preventDefault();
        }
    };
    TypeaheadComponent.prototype.ngOnInit = function () {
        var onkeyDown$ = this.onElementKeyDown();
        this.subscriptions = [
            this.filterEnterEvent(onkeyDown$),
            this.listenAndSuggest(),
            this.navigateWithArrows(onkeyDown$)
        ];
        this.renderTemplate();
    };
    TypeaheadComponent.prototype.ngOnDestroy = function () {
        this.subscriptions.forEach(function (sub) { return sub.unsubscribe(); });
        this.subscriptions.length = 0;
    };
    TypeaheadComponent.prototype.renderTemplate = function () {
        this.viewContainer.createEmbeddedView(this.suggestionsTplRef);
        this.cdr.markForCheck();
    };
    TypeaheadComponent.prototype.onElementKeyDown = function () {
        return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["a" /* Observable */].fromEvent(this.element.nativeElement, 'keydown').share();
    };
    TypeaheadComponent.prototype.filterEnterEvent = function (elementObs) {
        var _this = this;
        return elementObs
            .filter(function (e) { return e.keyCode === __WEBPACK_IMPORTED_MODULE_2__models__["a" /* Key */].Enter; })
            .subscribe(function (event) {
            if (_this.showSuggestions) {
                event.preventDefault();
                _this.handleSelectSuggestion(_this.activeResult);
            }
        });
    };
    TypeaheadComponent.prototype.listenAndSuggest = function () {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["a" /* Observable */].fromEvent(this.element.nativeElement, 'keyup')
            .filter(function (e) { return Object(__WEBPACK_IMPORTED_MODULE_3__typeahead_util__["d" /* validateNonCharKeyCode */])(e.keyCode); })
            .map(function (e) {
            var selectionStart = _this.element.nativeElement.selectionStart;
            var regex = /[@#](\w*)$/;
            var matches = e.target.value.substr(0, selectionStart).match(regex);
            _this.hideSuggestions();
            e.matches = matches;
            if (e.matches) {
                return e.matches[0];
            }
            else {
                return null;
            }
        })
            .debounceTime(300)
            .concat()
            .distinctUntilChanged()
            .filter(function (query) { return query && query.length > 0; })
            .switchMap(function (query) { return _this.suggest(query); })
            .subscribe(function (results) {
            _this.results = results;
            _this.showSuggestions = true;
            _this.suggestionIndex = 0;
            _this.cdr.markForCheck();
        });
    };
    TypeaheadComponent.prototype.navigateWithArrows = function (elementObs) {
        var _this = this;
        return elementObs
            .filter(function (e) { return Object(__WEBPACK_IMPORTED_MODULE_3__typeahead_util__["c" /* validateArrowKeys */])(e.keyCode); })
            .subscribe(function (e) {
            var keyCode = e.keyCode;
            // Disable navigation if suggestion was hidden.
            if (!_this.showSuggestions) {
                return;
            }
            e.preventDefault();
            _this.suggestionIndex = Object(__WEBPACK_IMPORTED_MODULE_3__typeahead_util__["b" /* resolveNextIndex */])(_this.suggestionIndex, keyCode === __WEBPACK_IMPORTED_MODULE_2__models__["a" /* Key */].ArrowDown);
            _this.showSuggestions = true;
            _this.cdr.markForCheck();
        });
    };
    TypeaheadComponent.prototype.suggest = function (query) {
        return this.request(query);
    };
    /**
     * peforms a jsonp/http request to search with query and params
     * @param query the query to search from the remote source
     */
    TypeaheadComponent.prototype.request = function (query) {
        return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["a" /* Observable */].from(this.searchService.search(query));
    };
    TypeaheadComponent.prototype.markIsActive = function (index, result) {
        var isActive = Object(__WEBPACK_IMPORTED_MODULE_3__typeahead_util__["a" /* isIndexActive */])(index, this.suggestionIndex);
        if (isActive) {
            this.activeResult = result;
        }
        return isActive;
    };
    TypeaheadComponent.prototype.handleSelectSuggestion = function (suggestion) {
        var elCommentText = this.element.nativeElement;
        var selectionStart = elCommentText.selectionStart;
        var regex = /([@#])(\w*)$/;
        var subStrFront = elCommentText.value.substr(0, selectionStart).replace(regex, function (match, p1, p2) {
            if (p1 === '@') {
                return p1 + suggestion.username;
            }
            else {
                return p1 + suggestion.name;
            }
        });
        var subStrBack = elCommentText.value.substr(selectionStart);
        elCommentText.value = subStrFront + subStrBack;
        elCommentText.selectionStart = elCommentText.selectionEnd = subStrFront.length;
        elCommentText.focus();
        this.ngModel.update.emit(elCommentText.value);
        this.hideSuggestions();
        this.taSelected.emit(suggestion);
    };
    TypeaheadComponent.prototype.handleMouseEnterSuggestion = function (index) {
        this.suggestionIndex = index;
    };
    TypeaheadComponent.prototype.hideSuggestions = function () {
        this.showSuggestions = false;
    };
    TypeaheadComponent.prototype.hasItemTemplate = function () {
        return this.taItemTpl !== undefined;
    };
    TypeaheadComponent.prototype.getHostElement = function () {
        return this.element;
    };
    return TypeaheadComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"]) === "function" && _a || Object)
], TypeaheadComponent.prototype, "taItemTpl", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], TypeaheadComponent.prototype, "taSelected", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('suggestionsTplRef'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"]) === "function" && _b || Object)
], TypeaheadComponent.prototype, "suggestionsTplRef", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('keydown', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], TypeaheadComponent.prototype, "handleEsc", null);
TypeaheadComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: '[typeahead]',
        styles: [
            "\n  .results {\n    position: absolute;\n  }\n  .typeahead-backdrop {\n    bottom: 0;\n    left: 0;\n    position: fixed;\n    right: 0;\n    top: 0;\n    z-index: 1;\n  }\n  .list-group-item {\n    position: relative;\n    z-index: 2;\n  }\n  "
        ],
        template: "<ng-template #suggestionsTplRef>\n  <section class=\"typeahead-list list-group results\" *ngIf=\"showSuggestions\">\n    <div class=\"typeahead-backdrop\" (click)=\"hideSuggestions()\"></div>\n    <div class=\"list-group-item\"\n      *ngFor=\"let result of results; let i = index;\"\n      [class.active]=\"markIsActive(i, result)\"\n      (click)=\"handleSelectSuggestion(result)\"\n      (mouseenter)=\"handleMouseEnterSuggestion(i)\">\n      <ng-container *ngIf=\"result.type=='user'; else tagtemp\">\n        @{{result.username}}<span class=\"pullright\">(${{result.earnings|number:'1.2-2'}} earned)</span>\n      </ng-container>\n      <ng-template #tagtemp>\n        #{{result.name}}<span class=\"pullright\">({{result.num_items}} posts) </span>\n      </ng-template>\n      <ng-template\n        [ngTemplateOutlet]=\"taItemTpl\"\n        [ngTemplateOutletContext]=\"{ $implicit: {result: result, index: i} }\"\n      ></ng-template>\n    </div>\n  </section>\n  </ng-template>",
        providers: [__WEBPACK_IMPORTED_MODULE_4__search_search_service__["a" /* SearchService */], __WEBPACK_IMPORTED_MODULE_5__angular_forms__["NgModel"]]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_4__search_search_service__["a" /* SearchService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__search_search_service__["a" /* SearchService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_5__angular_forms__["NgModel"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_forms__["NgModel"]) === "function" && _g || Object])
], TypeaheadComponent);

var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=typeahead.component.js.map

/***/ }),

/***/ "./src/app/typeahead/typeahead.util.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["d"] = validateNonCharKeyCode;
/* harmony export (immutable) */ __webpack_exports__["c"] = validateArrowKeys;
/* harmony export (immutable) */ __webpack_exports__["a"] = isIndexActive;
/* unused harmony export createParamsForQuery */
/* unused harmony export resolveApiMethod */
/* harmony export (immutable) */ __webpack_exports__["b"] = resolveNextIndex;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models__ = __webpack_require__("./src/app/typeahead/models.ts");
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};


function validateNonCharKeyCode(keyCode) {
    return [
        __WEBPACK_IMPORTED_MODULE_1__models__["a" /* Key */].Enter,
        __WEBPACK_IMPORTED_MODULE_1__models__["a" /* Key */].Tab,
        __WEBPACK_IMPORTED_MODULE_1__models__["a" /* Key */].Shift,
        __WEBPACK_IMPORTED_MODULE_1__models__["a" /* Key */].ArrowLeft,
        __WEBPACK_IMPORTED_MODULE_1__models__["a" /* Key */].ArrowUp,
        __WEBPACK_IMPORTED_MODULE_1__models__["a" /* Key */].ArrowRight,
        __WEBPACK_IMPORTED_MODULE_1__models__["a" /* Key */].ArrowDown,
        __WEBPACK_IMPORTED_MODULE_1__models__["a" /* Key */].MacCommandLeft,
        __WEBPACK_IMPORTED_MODULE_1__models__["a" /* Key */].MacCommandRight,
        __WEBPACK_IMPORTED_MODULE_1__models__["a" /* Key */].MacCommandFirefox
    ].every(function (codeKey) { return codeKey !== keyCode; });
}
function validateArrowKeys(keyCode) {
    return keyCode === __WEBPACK_IMPORTED_MODULE_1__models__["a" /* Key */].ArrowDown || keyCode === __WEBPACK_IMPORTED_MODULE_1__models__["a" /* Key */].ArrowUp;
}
function isIndexActive(index, currentIndex) {
    return index === currentIndex;
}
function createParamsForQuery(query, queryParamKey, customParams) {
    if (queryParamKey === void 0) { queryParamKey = 'q'; }
    if (customParams === void 0) { customParams = {}; }
    var searchParams = __assign((_a = {}, _a[queryParamKey] = query, _a), customParams);
    // tslint:disable-next-line
    var setParam = function (acc, param) { return acc.set(param, searchParams[param]); };
    var params = Object.keys(searchParams).reduce(setParam, new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpParams */]());
    return params;
    var _a;
}
function resolveApiMethod(method) {
    if (method === void 0) { method = ''; }
    var isMethodValid = [
        'get',
        'post',
        'put',
        'delete',
        'patch',
        'request'
    ].some(function (methodName) { return method === methodName; });
    var apiMethod = isMethodValid ? method : 'get';
    return apiMethod;
}
function resolveNextIndex(currentIndex, stepUp) {
    var step = stepUp ? 1 : -1;
    var topLimit = 9;
    var bottomLimit = 0;
    var currentResultIndex = currentIndex + step;
    var resultIndex = currentResultIndex;
    if (currentResultIndex === topLimit + 1) {
        resultIndex = bottomLimit;
    }
    if (currentResultIndex === bottomLimit - 1) {
        resultIndex = topLimit;
    }
    return resultIndex;
}
//# sourceMappingURL=typeahead.util.js.map

/***/ }),

/***/ "./src/app/user/user.component.css":
/***/ (function(module, exports) {

module.exports = ".usernameinput{\n  color: #444;\n  border: 1px solid #CCC;\n  border-radius: 4px;\n  display: block;\n  width: 100%;\n  padding: 5px;\n  font-size: 16px;\n  margin-bottom: 10px;\n}\n\n/*.tootlbar-icon {\n  padding: 0 14px;\n}\n\n.tootlbar-spacer {\n  flex: 1 1 auto;\n}*/\n\n/* Absolute Center Spinner */\n\n/*.loading {\n  position: fixed;\n  z-index: 999;\n  height: 2em;\n  width: 2em;\n  overflow: show;\n  margin: auto;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n}*/\n\n/* Transparent Overlay */\n\n/*.loading:before {\n  content: '';\n  display: block;\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background-color: rgba(0,0,0,0.3);\n}*/\n\n/* :not(:required) hides these rules from IE9 and below */\n\n/*.loading:not(:required) {*/\n\n/* hide \"loading...\" text */\n\n/* font: 0/0 a;\n  color: transparent;\n  text-shadow: none;\n  background-color: transparent;\n  border: 0;\n}\n\n.loading:not(:required):after {\n  content: '';\n  display: block;\n  font-size: 10px;\n  width: 1em;\n  height: 1em;\n  margin-top: -0.5em;\n  -webkit-animation: spinner 1500ms infinite linear;\n  -moz-animation: spinner 1500ms infinite linear;\n  -ms-animation: spinner 1500ms infinite linear;\n  -o-animation: spinner 1500ms infinite linear;\n  animation: spinner 1500ms infinite linear;\n  border-radius: 0.5em;\n  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;\n  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;\n}*/\n\n/* Animation */\n\n/*@-webkit-keyframes spinner {\n  0% {\n    -webkit-transform: rotate(0deg);\n    -moz-transform: rotate(0deg);\n    -ms-transform: rotate(0deg);\n    -o-transform: rotate(0deg);\n    transform: rotate(0deg);\n  }\n  100% {\n    -webkit-transform: rotate(360deg);\n    -moz-transform: rotate(360deg);\n    -ms-transform: rotate(360deg);\n    -o-transform: rotate(360deg);\n    transform: rotate(360deg);\n  }\n}\n@-moz-keyframes spinner {\n  0% {\n    -webkit-transform: rotate(0deg);\n    -moz-transform: rotate(0deg);\n    -ms-transform: rotate(0deg);\n    -o-transform: rotate(0deg);\n    transform: rotate(0deg);\n  }\n  100% {\n    -webkit-transform: rotate(360deg);\n    -moz-transform: rotate(360deg);\n    -ms-transform: rotate(360deg);\n    -o-transform: rotate(360deg);\n    transform: rotate(360deg);\n  }\n}\n@-o-keyframes spinner {\n  0% {\n    -webkit-transform: rotate(0deg);\n    -moz-transform: rotate(0deg);\n    -ms-transform: rotate(0deg);\n    -o-transform: rotate(0deg);\n    transform: rotate(0deg);\n  }\n  100% {\n    -webkit-transform: rotate(360deg);\n    -moz-transform: rotate(360deg);\n    -ms-transform: rotate(360deg);\n    -o-transform: rotate(360deg);\n    transform: rotate(360deg);\n  }\n}\n@keyframes spinner {\n  0% {\n    -webkit-transform: rotate(0deg);\n    -moz-transform: rotate(0deg);\n    -ms-transform: rotate(0deg);\n    -o-transform: rotate(0deg);\n    transform: rotate(0deg);\n  }\n  100% {\n    -webkit-transform: rotate(360deg);\n    -moz-transform: rotate(360deg);\n    -ms-transform: rotate(360deg);\n    -o-transform: rotate(360deg);\n    transform: rotate(360deg);\n  }\n}*/\n\n/*.loading {\n    background-color: blue;\n    background-image: url(\"http://loadinggif.com/images/image-selection/3.gif\");\n    background-size: 25px;\n    background-position: center;\n    background-repeat: no-repeat;\n}*/\n"

/***/ }),

/***/ "./src/app/user/user.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <div *ngIf=\"showLoader\" class=\"loading\"></div> -->\n<div class=\"user-profile-contain\">\n  <div class=\"banner-sec\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <div class=\"col-sm-12 col-lg-12 col-md-12\">\n\n\n          <div class=\"coverphotoholder\">\n            <a href=\"#\" class=\"editcoverphotolink\"(click)=\"selectCoverPic();false\">\n              <span id=\"edit_cover_icon\" class=\"editcoverphoto\" *ngIf=\"edit_clicked\" translate>\n                Change\n              </span>\n            </a>\n            <input type=\"file\" #coverfileInput (change)=\"coverFileChangeEvent($event)\" name=\"cover_pic\" style=\"position:absolute;top:-1000px;\" />\n            <a href=\"#\" (click)=\"openCoverLightbox(coverImage.src);false\">\n              <img id=\"cover_image\" [src]=\"cover_pic_url\" class=\"img-responsive\" #coverImage>\n            </a>\n          </div>\n\n\n        </div>\n      </div>\n\n\n      <div class=\"row\">\n\n        <div class=\"col-md-12\">\n\n\n          <div class=\"profile-sec\">\n            <ul class=\"upper\">\n              <li>\n                <div class=\"profile-pic\">\n                  <form id=\"edit_profile_pic\">\n                    <input type=\"file\" #profilefileInput (change)=\"profileFileChangeEvent($event)\" name=\"profile_pic\"\n                           style=\"width:0; height: 0; overflow: hidden;\" accept=\"image/gif, image/jpeg, image/png\" />\n                  </form>\n\n                  <div class=\"profilepicturecircle\" *ngIf=\"requesting_user.id ==  user.id\">\n                    <span [innerHTML]=\"user.avatar\"></span>\n\n                    <a href=\"#\" (click)=\"selectProfilePic();false\">\n                        <span id=\"edit_pic_icon\" class=\"profilepicedit\" *ngIf=\"edit_clicked\" translate>\n                          Change\n                        </span>\n                    </a>\n                  </div>\n\n                  <div class=\"profilepicturecircle\" *ngIf=\"requesting_user.id !=  user.id\">\n                    <a class=\"btn-avatar-lightbox\" href=\"#\" (click)=\"openAvatarLightbox();false\">\n                      <span [innerHTML]=\"user.avatar\"></span>\n                    </a>\n\n                  </div>\n\n                  <div class=\"content2\">\n                    <form name=\"edit_username\" id=\"edit_username\" *ngIf=\"edit_username_clicked\">\n                      <br>\n                      <input class=\"usernameinput\" name=\"username\" [placeholder]=\"'Enter your name'|translate\"\n                             [(ngModel)]=\"user.username\">\n                      <input class=\"savebutton\" (click)=\"saveName($event);\" type=\"button\" value=\"Save\">\n                    </form>\n                    <h2 class=\"profile-username\">\n                      {{ user.username }}\n                      <a href=\"#\" (click)=\"edit_username_clicked = !edit_username_clicked;false\">\n                        <span id=\"edit_username_icon\" *ngIf=\"false\">\n                          <i class=\"usernameedit fa fa-pencil\" aria-hidden=\"true\"></i>\n                        </span>\n                      </a>\n                    </h2>\n                  </div>\n                </div>\n              </li>\n\n\n            </ul>\n            <div class=\"showonmobile mobilebio\">\n\n\n              <form class=\"clearfix\" name=\"edit_bio\" *ngIf=\"edit_bio_clicked\">\n\n                <textarea class=\"biotextarea\"\n                          name=\"bio\"\n                          [placeholder]=\"'Enter your bio'|translate\"\n                          [(ngModel)]=\"bio_in_edit\" rows=\"3\">{{ bio_in_edit }}</textarea>\n                <input class=\"savebutton\" (click)=\"saveBio($event);\" type=\"button\" value=\"Save\">\n                <input class=\"cancelbutton\" (click)=\"cancelBioEdit($event);\" type=\"button\" value=\"Cancel\">\n\n              </form>\n\n              <span class=\"commentspan\">\n                <app-dynamic-html class=\"user_bio\" [innerHTML]=\"user.user1.bio|tagify\"></app-dynamic-html>\n              </span>\n\n              <a href=\"#\" *ngIf=\"edit_clicked && !edit_bio_clicked\"\n                 (click)=\"startBioEdit($event)\">\n                <span><i class=\"bioedit fas fa-pencil-alt\" aria-hidden=\"true\"></i></span>\n              </a>\n\n            </div>\n\n\n            <a href=\"#\" class=\"mobileseeall mfs1 mobilefollowerstat\" data-toggle=\"modal\" data-target=\".followersbox2\">\n              <h4 class=\"\"><strong>{{ followers_count }}</strong><br>\n                <ng-container translate>Followers</ng-container>\n              </h4>\n\n            </a><a href=\"#\" class=\"mobileseeall mfs2 mobilefollowerstat\" data-toggle=\"modal\" data-target=\".followingbox2\">\n            <h4 class=\"\"><strong>{{ followings_count }}</strong><br>\n              <ng-container translate>Following</ng-container>\n            </h4>\n          </a>\n\n\n            <ul class=\"userstatmobilerow\">\n\n              <li class=\"userstatmobile poststatmobile\">\n                <h3>{{ user.thought_count }}</h3>\n\n                <h4>\n                  <ng-template #posts>\n                    {{'Post'|translate}}\n                  </ng-template>\n                  <ng-container *ngIf=\"user.thought_count>1; else posts\">\n                    {{'Posts'|translate}}\n                  </ng-container>\n                </h4>\n\n              </li>\n\n\n              <li class=\"userstatmobile\">\n                <h3>${{ user.earnings|number:'1.2-2' }}</h3>\n                <h4 translate>Earnings</h4>\n              </li>\n              <li *ngIf=\"!loading && (requesting_user.id ==  user.id)\">\n                <a href=\"#\" class=\"edit-profile\" (click)=\"edit_clicked = !edit_clicked;false\">\n\n                  <span *ngIf=\"!edit_clicked\"> {{'Edit Profile'|translate}}</span>\n                  <span *ngIf=\"edit_clicked\"> {{'Save'|translate}}</span>\n\n                </a>\n              </li>\n              <ng-container *ngIf=\"!loading && (requesting_user.id !=  user.id)\">\n                <li *ngIf=\"!!requesting_user.id && !user.is_following\">\n                  <a href=\"#\" class=\"edit-profile\" (click)=\"follow(user);false\">\n                    <i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i> {{'Follow'|translate}}\n                  </a>\n                </li>\n                <li *ngIf=\"!!requesting_user.id && user.is_following\">\n                  <a href=\"#\" class=\"edit-profile\" (click)=\"unfollow(user);false\">\n                    <i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i> {{'Unfollow'|translate}}</a>\n                </li>\n              </ng-container>\n\n            </ul>\n          </div>\n\n\n        </div>\n\n      </div>\n\n\n    </div>\n  </div>\n\n\n  <div class=\"mid-con\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <div class=\"col-sm-3 col-lg-3 col-md-3 showonmobile\">\n          <div class=\"modal fade bs-example-modal-lg followersbox2\" tabindex=\"-1\" role=\"dialog\"\n               aria-labelledby=\"myLargeModalLabel\">\n\n            <div class=\"modal-dialog modal-lg\">\n              <div class=\"modal-content\">\n\n                <div class=\"white-box followerbox\" infiniteScroll\n                      [infiniteScrollDistance]=\"3\"\n                      [infiniteScrollThrottle]=\"1000\"\n                      (scrolled)=\"onFollowerScroll()\"\n                      [scrollWindow]=\"false\">\n\n                  <h4 class=\"followerboxheader\">{{'Followers'|translate}} ({{ followers_count }})</h4>\n\n                  <ul class=\"followerscroll\">\n                    <li *ngFor=\"let follower of followers\">\n                      <a [routerLink]=\"['/user/', follower.username]\">\n                        <span class=\"circlephoto\" [innerHTML]=\"follower.avatar\"></span><span class=\"modalusername\">{{ follower.full_name }}</span>\n                      </a>\n                      <ng-container *ngIf=\"follower.id != requesting_user.id\">\n                        <a href=\"#\" (click)=\"follow(follower);false\"\n                           *ngIf=\"!follower.is_following\" class=\"followbtn\">\n                          {{'Follow'|translate}}\n                        </a>\n                        <a class=\"followingbtn\"\n                           *ngIf=\"(follower.id != requesting_user.id) && follower.is_following\" href=\"#\"\n                           (click)=\"unfollow(follower);false\">\n                          {{'Following'|translate}}\n                        </a>\n                      </ng-container>\n                    </li>\n                  </ul>\n                  <sk-three-bounce *ngIf=\"follow_loading == true\"></sk-three-bounce>\n\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"modal fade bs-example-modal-lg followingbox2\" tabindex=\"-1\" role=\"dialog\"\n               aria-labelledby=\"myLargeModalLabel\">\n\n            <div class=\"modal-dialog modal-lg\">\n              <div class=\"modal-content\">\n\n                <div class=\"white-box followerbox\" infiniteScroll\n                      [infiniteScrollDistance]=\"3\"\n                      [infiniteScrollThrottle]=\"1000\"\n                      (scrolled)=\"onFollowingScroll()\"\n                      [scrollWindow]=\"false\">>\n\n                  <h4 class=\"followerboxheader\">Following ({{ followings_count }})</h4>\n\n                  <ul class=\"followerscroll\">\n                    <li *ngFor=\"let following of followings\">\n                      <a\n                        [routerLink]=\"['/user/', following.username]\"><span\n                        class=\"circlephoto\"\n                        [innerHTML]=\"following.avatar\"></span><span class=\"modalusername\">{{ following.full_name }}</span>\n                      </a>\n                      <a href=\"#\" *ngIf=\"(following.id != requesting_user.id) && !following.is_following\" class=\"followbtn\"\n                         (click)=\"follow(following);false\" translate>Follow</a>\n\n                      <a *ngIf=\"(following.id != requesting_user.id) && following.is_following\"\n                         href=\"#\"\n                         (click)=\"unfollow(following);false\"\n                         class=\"followingbtn\" translate>Following</a>\n                    </li>\n                  </ul>\n                  <sk-three-bounce *ngIf=\"follow_loading == true\"></sk-three-bounce>\n\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n\n        <div class=\"col-sm-3 col-lg-3 col-md-3 user-profile-left\">\n          <div class=\"white-box\">\n            <h4 translate>About Me</h4>\n            <div class=\"about-box\">\n              <form class=\"clearfix\" name=\"edit_bio\" *ngIf=\"edit_bio_clicked\">\n                <br>\n                <textarea class=\"biotextarea\" name=\"bio\" [placeholder]=\"'Enter your bio'|translate\"\n                          [(ngModel)]=\"bio_in_edit\" rows=\"3\">{{ user.user1.bio }}</textarea>\n                <input class=\"savebutton\" (click)=\"saveBio($event);\" type=\"button\" value=\"Save\">\n                <input class=\"cancelbutton\" (click)=\"cancelBioEdit($event);\" type=\"button\" value=\"Cancel\">\n              </form>\n              <span class=\"commentspan\">\n                <app-dynamic-html class=\"user_bio\" [innerHTML]=\"user.user1.bio|tagify\"></app-dynamic-html>\n              </span>\n\n              <a href=\"#\" *ngIf=\"edit_clicked && !edit_bio_clicked\"\n                 (click)=\"startBioEdit($event)\">\n                  <span>\n                    <i class=\"bioedit fas fa-pencil-alt\" aria-hidden=\"true\"></i>\n                  </span>\n              </a>\n            </div>\n          </div>\n        </div>\n\n        <div class=\"col-sm-6 col-lg-6 col-md-6 middle-section\" infiniteScroll [infiniteScrollDistance]=\"0.4\"\n             [infiniteScrollThrottle]=\"1000\"\n             (scrolled)=\"onScroll()\" style=\"float:left;\">\n\n          <div class=\"user-profile-box hideonphones\" *ngIf=\"requesting_user.id ==  user.id\">\n            <div class=\"hideonmobile panel with-nav-tabs\">\n              <div class=\"panel-heading\">\n                <ul class=\"uploadanimagebar nav nav-tabs\">\n\n                </ul>\n              </div>\n\n              <app-posting-form [requesting_user]=\"requesting_user\" [thots]=\"thoughts\"></app-posting-form>\n            </div>\n          </div>\n\n          <div class=\"container-fluid feed-view-switch\">\n            <div class=\"row\">\n              <a class=\"col-xs-6 text-center btn-switch\" [ngClass]=\"{'active': viewMode=='list'}\" href=\"javascript:void(0)\"\n                 (click)=\"switchViewMode('list')\">\n                <i class=\"glyphicon glyphicon-th-list\"></i>\n              </a>\n              <a class=\"col-xs-6 text-center btn-switch\" [ngClass]=\"{'active': viewMode=='thumbnail'}\" href=\"javascript:void(0)\"\n                 (click)=\"switchViewMode('thumbnail'); false;\">\n                <i class=\"glyphicon glyphicon-th\"></i>\n              </a>\n            </div>\n          </div>\n\n          <ng-container *ngIf=\"viewMode == 'list'\">\n            <app-post-box *ngFor=\"let thot of thoughts; index as i\" [user]=\"requesting_user\" [thot]=\"thot\" [index]=\"i\" (delete)=\"delete($event)\" (openThotBox)=\"openThotBox($event)\"></app-post-box>\n          </ng-container>\n\n          <ng-container *ngIf=\"viewMode == 'thumbnail'\">\n\n            <div class=\"white-box clear-fix\" *ngIf=\"thoughts.length > 0\">\n\n              <div class=\"col-xs-6 col-md-4 thumbnail-list-item\" *ngFor=\"let thot of thoughts;index as i\">\n\n                <div class=\"thumbnail-img-container\">\n                  <a href=\"#\" (click)=\"openThotBox(i);false\">\n                    <img class=\"thumbnail-img\" [src]=\"thot.image\"/>\n                    <span class=\"full-screen-btn\">\n                      <i class=\"fa fa-expand\" aria-hidden=\"true\"></i>\n                    </span>\n                  </a>\n                </div>\n              </div>\n            </div>\n\n          </ng-container>\n\n          <sk-three-bounce *ngIf=\"loading == true\"></sk-three-bounce>\n\n        </div>\n\n        <div class=\"col-sm-3 col-lg-3 col-md-3 user-profile-left\">\n          <div class=\"white-box\">\n            <h4 class=\"text-center\">\n              <a class=\"header-link\" data-toggle=\"modal\" data-target=\".followersbox\">\n                {{'Followers'|translate}} ({{ followers_count }})\n              </a>\n            </h4>\n            <div class=\"follow-box\">\n              <ul>\n                <li *ngFor=\"let follower of followers.slice(0,15)\">\n                  <a [routerLink]=\"['/user/', follower.username]\">\n                    <span class=\"circlephoto\" [innerHTML]=\"follower.avatar\"></span>\n                  </a>\n                </li>\n              </ul>\n            </div>\n            <h5 *ngIf=\"followers_count!=0\">\n              <a data-toggle=\"modal\" data-target=\".followersbox\" translate>See All</a>\n            </h5>\n\n            <div class=\"modal fade bs-example-modal-lg followersbox\" tabindex=\"-1\" role=\"dialog\"\n                 aria-labelledby=\"myLargeModalLabel\">\n\n              <div class=\"modal-dialog modal-lg\">\n                <div class=\"modal-content\">\n\n                  <div class=\"white-box followerbox\" infiniteScroll\n                      [infiniteScrollDistance]=\"3\"\n                      [infiniteScrollThrottle]=\"1000\"\n                      (scrolled)=\"onFollowerScroll()\"\n                      [scrollWindow]=\"false\">\n\n                    <h4 class=\"followerboxheader\">{{'Followers'|translate}} ({{ followers_count }})</h4>\n\n                    <ul class=\"followerscroll\">\n                      <li *ngFor=\"let follower of followers\">\n                        <a [routerLink]=\"['/user/', follower.username]\">\n                          <span class=\"circlephoto\" [innerHTML]=\"follower.avatar\"></span><span class=\"modalusername\">{{ follower.full_name }}</span>\n\n\n                        </a>\n                        <ng-container *ngIf=\"follower.id != requesting_user.id\">\n                          <a *ngIf=\"!follower.is_following\"\n                             href=\"#\"\n                             (click)=\"follow(follower);false\"\n                             class=\"followbtn\" translate>Follow</a>\n\n                          <a *ngIf=\"follower.is_following\"\n                             href=\"#\"\n                             (click)=\"unfollow(follower);false\"\n                             class=\"followingbtn\" translate>Following</a>\n                        </ng-container>\n\n                      </li>\n                    </ul>\n                    <sk-three-bounce *ngIf=\"follow_loading == true\"></sk-three-bounce>\n\n                  </div>\n                </div>\n              </div>\n            </div>\n\n\n          </div>\n          <div class=\"white-box\">\n            <h4 class=\"text-center\">\n              <a class=\"header-link\" data-toggle=\"modal\" data-target=\".followingbox\">\n                {{'Following'|translate}} ({{ followings_count }})\n              </a>\n            </h4>\n            <div class=\"follow-box\">\n              <ul>\n                <li *ngFor=\"let following of followings.slice(0,15)\">\n                  <a [routerLink]=\"['/user/', following.username]\">\n                    <span class=\"circlephoto\" [innerHTML]=\"following.avatar\"></span>\n                  </a>\n                </li>\n\n              </ul>\n            </div>\n            <h5 *ngIf=\"followings_count!=0\">\n              <a data-toggle=\"modal\" data-target=\".followingbox\" translate>See All</a>\n            </h5>\n\n            <div class=\"modal fade bs-example-modal-lg followingbox\" tabindex=\"-1\" role=\"dialog\"\n                 aria-labelledby=\"myLargeModalLabel\">\n\n              <div class=\"modal-dialog modal-lg\">\n                <div class=\"modal-content\">\n\n                  <div class=\"white-box followerbox\" infiniteScroll\n                      [infiniteScrollDistance]=\"3\"\n                      [infiniteScrollThrottle]=\"1000\"\n                      (scrolled)=\"onFollowingScroll()\"\n                      [scrollWindow]=\"false\">>\n\n                    <h4 class=\"followerboxheader\">Following ({{ followings_count }})</h4>\n\n                    <ul class=\"followerscroll\">\n                      <li *ngFor=\"let following of followings\"><a\n                        [routerLink]=\"['/user/', following.username]\"><span\n                        class=\"circlephoto\"\n                        [innerHTML]=\"following.avatar\"></span><span class=\"modalusername\">{{ following.full_name }}</span>\n                      </a>\n                        <ng-container *ngIf=\"(following.id != requesting_user.id)\">\n                          <a href=\"#\"  *ngIf=\"!following.is_following\"\n                             class=\"followbtn\"\n                             (click)=\"follow(following);false\" translate>Follow</a>\n\n                          <a *ngIf=\"following.is_following\"\n                             href=\"#\"\n                             class=\"followingbtn\"\n                             (click)=\"unfollow(following);false\" translate>Following</a>\n                        </ng-container>\n\n                      </li>\n                    </ul>\n                    <sk-three-bounce *ngIf=\"follow_loading == true\"></sk-three-bounce>\n\n                  </div>\n                </div>\n              </div>\n            </div>\n\n          </div>\n        </div>\n\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/user/user.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__lightbox_lightbox_service__ = __webpack_require__("./src/app/lightbox/lightbox.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_switchMap__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/switchMap.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__constants__ = __webpack_require__("./src/app/constants.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__core_event_service__ = __webpack_require__("./src/app/core/event.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__core_auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__core_event_types__ = __webpack_require__("./src/app/core/event.types.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__core_loader_service__ = __webpack_require__("./src/app/core/loader.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__core_awss3_api_awss3_api_service__ = __webpack_require__("./src/app/core/awss3-api/awss3-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_ng2_bootstrap_modal__ = __webpack_require__("./node_modules/ng2-bootstrap-modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_ng2_bootstrap_modal___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_ng2_bootstrap_modal__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__modals_avatar_edit_modal_avatar_edit_modal_component__ = __webpack_require__("./src/app/modals/avatar-edit-modal/avatar-edit-modal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__modals_cover_edit_modal_cover_edit_modal_component__ = __webpack_require__("./src/app/modals/cover-edit-modal/cover-edit-modal.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var UserComponent = (function () {
    function UserComponent(belaApiService, awsApiService, authService, eventService, ngZone, loaderService, route, router, location, _lightbox, dialogService) {
        this.belaApiService = belaApiService;
        this.awsApiService = awsApiService;
        this.authService = authService;
        this.eventService = eventService;
        this.ngZone = ngZone;
        this.loaderService = loaderService;
        this.route = route;
        this.router = router;
        this.location = location;
        this._lightbox = _lightbox;
        this.dialogService = dialogService;
        this.title = 'app';
        this.username = 'amitjaiswal';
        this.total_feeds = 0;
        this.thoughts = [];
        this.albumList = [];
        this.user = new __WEBPACK_IMPORTED_MODULE_2__app_model__["h" /* User */]();
        this.requesting_user = new __WEBPACK_IMPORTED_MODULE_2__app_model__["h" /* User */]();
        this.requesting_user_id = 1;
        this.requesting_user_username = '';
        this.cover_pic_url = __WEBPACK_IMPORTED_MODULE_7__constants__["a" /* Constants */].default_cover_pic;
        this.csrf = "";
        this.followers = [];
        this.followings = [];
        this.followers_count = 0;
        this.followings_count = 0;
        this.feed_api_url = '/api/self/';
        this.followers_api_url = '/api/followers/';
        this.followings_api_url = '/api/followings/';
        this.followers_api_base_url = '/api/followers/';
        this.followings_api_base_url = '/api/followings/';
        this.subscriptions = [];
        this.viewMode = 'list';
        this.edit_clicked = false;
        this.edit_bio_clicked = false;
        this.edit_username_clicked = false;
        this.edit_bio_success = true;
        this.edit_username_success = true;
        this.requestID = 'user_page';
        this.loading = true;
        this.follow_loading = true;
    }
    UserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.requesting_user_id = this.authService.getUserId();
        this.requesting_user_username = this.authService.getUserName();
        this.csrf = jQuery('input[name="csrfmiddlewaretoken"]').val();
        this.requesting_user = this.authService.user;
        this.subscriptions.push(this.eventService.subscribe({
            next: function (event) {
                if (event.type === __WEBPACK_IMPORTED_MODULE_10__core_event_types__["b" /* BelaEventType */].DELETED_POST) {
                    _this.delete(event.value);
                }
                else if (event.type === __WEBPACK_IMPORTED_MODULE_10__core_event_types__["b" /* BelaEventType */].ADDED_POST) {
                    _this.add(event.value);
                }
                else if (event.type === __WEBPACK_IMPORTED_MODULE_10__core_event_types__["b" /* BelaEventType */].REQUEST_NEXT_ALBUM) {
                    var request_1 = event.value;
                    if (request_1.requestID === _this.requestID) {
                        if (_this.existNextFeed()) {
                            _this.loadNextFeed().subscribe(function (next) {
                                var evValue = new __WEBPACK_IMPORTED_MODULE_10__core_event_types__["c" /* EvLoadedAlbum */]();
                                evValue.album = _this.albumList;
                                evValue.newImageIndex = request_1.currentImageIndex + 1;
                                var event = new __WEBPACK_IMPORTED_MODULE_10__core_event_types__["a" /* BelaEvent */]();
                                event.type = __WEBPACK_IMPORTED_MODULE_10__core_event_types__["b" /* BelaEventType */].LOADED_NEXT_ALBUM;
                                event.value = evValue;
                                _this.eventService.fireEvent(event);
                            }, function (err) {
                                var event = new __WEBPACK_IMPORTED_MODULE_10__core_event_types__["a" /* BelaEvent */]();
                                event.type = __WEBPACK_IMPORTED_MODULE_10__core_event_types__["b" /* BelaEventType */].NO_NEXT_ALBUM;
                                event.value = null;
                                _this.eventService.fireEvent(event);
                            });
                        }
                        else {
                            var event_1 = new __WEBPACK_IMPORTED_MODULE_10__core_event_types__["a" /* BelaEvent */]();
                            event_1.type = __WEBPACK_IMPORTED_MODULE_10__core_event_types__["b" /* BelaEventType */].NO_NEXT_ALBUM;
                            _this.eventService.fireEvent(event_1);
                        }
                    }
                }
                else if (event.type == __WEBPACK_IMPORTED_MODULE_10__core_event_types__["b" /* BelaEventType */].UPDATE_USER) {
                    _this.requesting_user = event.value;
                }
            }
        }));
        this.route.params
            .subscribe(function (params) {
            _this.loading = true;
            _this.username = params['username'];
            if (!_this.username || _this.username == "undefined") {
                _this.router.navigate(['/discover']);
            }
            else {
                _this.feed_api_url = '/api/self/' + _this.username + '/';
                _this.followers_api_url = _this.followers_api_base_url + _this.username + '/';
                _this.followings_api_url = _this.followings_api_base_url + _this.username + '/';
                jQuery('#mobileinfo').removeClass("slidein").addClass("slideout");
                jQuery('#search').removeClass('open');
                jQuery('.modal').modal('hide'); // Hide any opened modal.
                _this._lightbox.close_all();
                _this.followers = [];
                _this.followings = [];
                _this.total_feeds = 0;
                _this.belaApiService.getUserDetail(_this.username)
                    .then(function (userData) {
                    _this.user = userData;
                    _this.cover_pic_url =
                        userData.user1 && userData.user1.cover_pic ? userData.user1.cover_pic : __WEBPACK_IMPORTED_MODULE_7__constants__["a" /* Constants */].default_cover_pic;
                    window.scroll(0, 0);
                })
                    .catch(function (err) {
                    if (err.status == 404) {
                        // User was not found
                        _this.router.navigate(['404']);
                    }
                });
                _this.initiateFeed();
                _this.initiateEditStatus();
                _this.initiateFollowerFollowing();
            }
            window.scroll(0, 0);
        });
        window.scroll(0, 0);
    };
    UserComponent.prototype.ngOnDestroy = function () {
        for (var _i = 0, _a = this.subscriptions; _i < _a.length; _i++) {
            var subscription = _a[_i];
            subscription.unsubscribe();
        }
    };
    UserComponent.prototype.initiateEditStatus = function () {
        this.edit_clicked = false;
        this.edit_bio_clicked = false;
        this.edit_username_clicked = false;
        this.edit_bio_success = true;
        this.edit_username_success = true;
    };
    UserComponent.prototype.initiateFeed = function () {
        var _this = this;
        this.loading = true;
        this.thoughts = [];
        this.belaApiService.getList(this.feed_api_url).subscribe(function (thoughtListResp) {
            _this.total_feeds = thoughtListResp.count;
            _this.thoughts = thoughtListResp.results;
            _this.feed_api_url = thoughtListResp.next;
            window.scroll(0, 0);
            _this.loading = false;
            _this.buildAlbum();
        });
        window.scroll(0, 0);
    };
    UserComponent.prototype.initiateFollowerFollowing = function () {
        var _this = this;
        this.follow_loading = true;
        this.followers = [];
        this.followings = [];
        this.followers_count = 0;
        this.followings_count = 0;
        this.belaApiService.getList(this.followers_api_url).subscribe(function (followListResp) {
            _this.followers_count = followListResp.count;
            _this.followers = followListResp.results;
            _this.followers_api_url = followListResp.next;
            _this.follow_loading = false;
        });
        this.belaApiService.getList(this.followings_api_url).subscribe(function (followListResp) {
            _this.followings_count = followListResp.count;
            _this.followings = followListResp.results;
            _this.followings_api_url = followListResp.next;
            _this.follow_loading = false;
        });
    };
    UserComponent.prototype.onFollowerScroll = function () {
        var _this = this;
        if (this.followers_api_url != null && !this.follow_loading) {
            this.follow_loading = true;
            this.belaApiService.getList(this.followers_api_url).subscribe(function (followListResp) {
                _this.followers_count = followListResp.count;
                for (var _i = 0, _a = followListResp.results; _i < _a.length; _i++) {
                    var result = _a[_i];
                    _this.followers.push(result);
                }
                _this.followers_api_url = followListResp.next;
                _this.follow_loading = false;
            });
        }
    };
    UserComponent.prototype.onFollowingScroll = function () {
        var _this = this;
        if (this.followings_api_url != null && !this.follow_loading) {
            this.follow_loading = true;
            this.belaApiService.getList(this.followings_api_url).subscribe(function (followListResp) {
                _this.followings_count = followListResp.count;
                for (var _i = 0, _a = followListResp.results; _i < _a.length; _i++) {
                    var result = _a[_i];
                    _this.followings.push(result);
                }
                _this.followings_api_url = followListResp.next;
                _this.follow_loading = false;
            });
        }
    };
    UserComponent.prototype.onScroll = function () {
        var bottom_reached = this.belaApiService.isBottomReached();
        if (this.existNextFeed() && bottom_reached && !this.loading) {
            this.loadNextFeed().subscribe();
        }
    };
    UserComponent.prototype.existNextFeed = function () {
        return this.feed_api_url != null;
    };
    UserComponent.prototype.loadNextFeed = function () {
        var _this = this;
        this.loading = true;
        return this.belaApiService.getList(this.feed_api_url).do(function (listResp) {
            _this.feed_api_url = listResp.next;
            for (var _i = 0, _a = listResp.results; _i < _a.length; _i++) {
                var result = _a[_i];
                _this.thoughts.push(result);
            }
            console.log("Loaded ok");
            _this.loading = false;
            _this.buildAlbum();
        });
    };
    UserComponent.prototype.saveBio = function (event) {
        var _this = this;
        event.stopPropagation();
        this.belaApiService.updateUserProfile(this.requesting_user, { bio: this.bio_in_edit })
            .then(function (userProfileResp) {
            _this.user.user1 = userProfileResp;
            _this.edit_bio_clicked = false;
            _this.edit_bio_success = true;
        });
    };
    UserComponent.prototype.startBioEdit = function (event) {
        event.stopPropagation();
        this.bio_in_edit = this.user.user1.bio;
        this.edit_bio_clicked = !this.edit_bio_clicked;
        return false;
    };
    UserComponent.prototype.cancelBioEdit = function (event) {
        event.stopPropagation();
        this.edit_bio_clicked = !this.edit_bio_clicked;
    };
    UserComponent.prototype.saveName = function (event) {
        var _this = this;
        event.stopPropagation();
        this.belaApiService.updateUsername(this.requesting_user, this.user.username)
            .then(function (userResp) {
            _this.user.username = userResp.username;
            _this.edit_username_clicked = false;
            _this.edit_username_success = true;
        });
    };
    UserComponent.prototype.selectProfilePic = function () {
        this.profilefileInput.nativeElement.value = "";
        this.profilefileInput.nativeElement.click();
    };
    UserComponent.prototype.selectCoverPic = function () {
        this.coverfileInput.nativeElement.value = "";
        this.coverfileInput.nativeElement.click();
    };
    UserComponent.prototype.coverFileChangeEvent = function ($event) {
        var _this = this;
        var file = $event.target.files[0];
        var fileName = file.name;
        var fileType = file.type;
        var picUrl;
        this.dialogService.addDialog(__WEBPACK_IMPORTED_MODULE_15__modals_cover_edit_modal_cover_edit_modal_component__["a" /* CoverEditModalComponent */], {
            imageFile: file
        }).subscribe(function (result) {
            if (result) {
                var resizedData_1 = _this.belaApiService.dataURItoBlob(result, fileType);
                _this.belaApiService.getImagePostLink(fileName, fileType)
                    .then(function (res) {
                    picUrl = res.url.replace(res.data.url, '');
                    return _this.awsApiService.uploadBlobToS3(res, resizedData_1, fileName);
                })
                    .then(function (res) {
                    return _this.belaApiService.updateProfileCoverPicture(picUrl);
                })
                    .then(function (res) {
                    if (res.status === "success") {
                        _this.cover_pic_url = res.result['cover_url'];
                    }
                })
                    .catch(function (err) {
                    console.log(err);
                    alert("We got error while uploading photo. Please try again later.");
                });
            }
        });
    };
    UserComponent.prototype.profileFileChangeEvent = function ($event) {
        var _this = this;
        var file = $event.target.files[0];
        var fileName = file.name;
        var fileType = file.type;
        var picUrl;
        this.dialogService.addDialog(__WEBPACK_IMPORTED_MODULE_14__modals_avatar_edit_modal_avatar_edit_modal_component__["a" /* AvatarEditModalComponent */], {
            imageFile: file
        }).subscribe(function (result) {
            console.log('Dialog closed');
            console.log(result);
            if (result) {
                var resizedData_2 = _this.belaApiService.dataURItoBlob(result, fileType);
                _this.belaApiService.getImagePostLink(fileName, fileType)
                    .then(function (res) {
                    picUrl = res.url.replace(res.data.url, '');
                    return _this.awsApiService.uploadBlobToS3(res, resizedData_2, fileName);
                })
                    .then(function (res) {
                    return _this.belaApiService.updateProfileAvatar(picUrl);
                })
                    .then(function (res) {
                    if (res.status === 'success') {
                        _this.user.avatar_url = res.result['avatar_url'];
                        _this.user.avatar = res.result['avatar'];
                        _this.authService.updateUser(_this.user);
                        _this.initiateFeed();
                    }
                })
                    .catch(function (err) {
                    console.log(err);
                    alert("We got error while uploading photo. Please try again later.");
                });
            }
        });
    };
    UserComponent.prototype.openCoverLightbox = function (imgUrl) {
        var options = {
            disableScrolling: true,
            fitImageInViewPort: true
        };
        var _album = {
            src: imgUrl,
            thumb: imgUrl,
            caption: "",
            thot: null
        };
        this._lightbox.openAlbum([_album], 0, options);
    };
    UserComponent.prototype.openAvatarLightbox = function () {
        var _album = {
            src: this.user.avatar_url,
            thumb: this.user.avatar_url,
            caption: "",
            thot: null
        };
        var options = {
            disableScrolling: true,
            circleImage: true,
            fitImageInViewPort: true
        };
        this._lightbox.openAlbum([_album], 0, options);
    };
    UserComponent.prototype.openThotBox = function (index) {
        if (index >= 0) {
            this._lightbox.openPost(this.albumList, index, {
                disableKeyboardNav: true,
                requestID: this.requestID,
                allowRequestAlbum: true
            });
        }
    };
    UserComponent.prototype.follow = function (user) {
        var _this = this;
        if (user && user.id) {
            this.belaApiService.follow(user, this.csrf).then(function (result) {
                user.is_following = true;
                _this.belaApiService.getList(_this.followers_api_base_url + _this.username + '/').subscribe(function (followListResp) {
                    _this.followers_count = followListResp.count;
                    _this.followers = followListResp.results;
                    _this.followers_api_url = followListResp.next;
                    _this.follow_loading = false;
                });
                _this.belaApiService.getList(_this.followings_api_base_url + _this.username + '/').subscribe(function (followListResp) {
                    _this.followings_count = followListResp.count;
                    _this.followings = followListResp.results;
                    _this.followings_api_url = followListResp.next;
                    _this.follow_loading = false;
                });
            }).catch(function () {
            });
        }
    };
    UserComponent.prototype.unfollow = function (user) {
        var _this = this;
        if (user && user.id) {
            this.belaApiService.unfollow(user, this.csrf).then(function (result) {
                user.is_following = false;
                _this.belaApiService.getList(_this.followers_api_base_url + _this.username + '/').subscribe(function (followListResp) {
                    _this.followers_count = followListResp.count;
                    _this.followers = followListResp.results;
                    _this.followers_api_url = followListResp.next;
                    _this.follow_loading = false;
                });
                _this.belaApiService.getList(_this.followings_api_base_url + _this.username + '/').subscribe(function (followListResp) {
                    _this.followings_count = followListResp.count;
                    _this.followings = followListResp.results;
                    _this.followings_api_url = followListResp.next;
                    _this.follow_loading = false;
                });
            }).catch(function () {
            });
        }
    };
    UserComponent.prototype.switchViewMode = function (mode) {
        var _this = this;
        if (this.viewMode !== mode) {
            this.viewMode = mode;
            if (mode == "thumbnail") {
                setTimeout(function () {
                    _this.onScroll();
                }, 1500);
            }
        }
    };
    UserComponent.prototype.add = function (thot) {
        this.thoughts.unshift(thot);
    };
    UserComponent.prototype.delete = function (thot) {
        var idx = this.thoughts.indexOf(thot);
        if (idx > -1) {
            this.thoughts.splice(idx, 1);
        }
    };
    UserComponent.prototype.buildAlbum = function () {
        this.albumList = [];
        for (var j = 0; j < this.thoughts.length; j++) {
            var _album = {
                src: this.thoughts[j].image,
                caption: this.thoughts[j].content,
                thumb: this.thoughts[j].image,
                thot: this.thoughts[j]
            };
            this.albumList.push(_album);
        }
    };
    return UserComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('profilefileInput'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], UserComponent.prototype, "profilefileInput", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('coverfileInput'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object)
], UserComponent.prototype, "coverfileInput", void 0);
UserComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-user',
        template: __webpack_require__("./src/app/user/user.component.html"),
        styles: [__webpack_require__("./src/app/user/user.component.css")],
        providers: [__WEBPACK_IMPORTED_MODULE_11__core_loader_service__["a" /* LoaderService */]]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_12__core_awss3_api_awss3_api_service__["a" /* AWSS3ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_12__core_awss3_api_awss3_api_service__["a" /* AWSS3ApiService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_9__core_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_9__core_auth_service__["a" /* AuthService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_8__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__core_event_service__["a" /* EventService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_11__core_loader_service__["a" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_11__core_loader_service__["a" /* LoaderService */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["d" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["d" /* Router */]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_4__angular_common__["Location"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_common__["Location"]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_5__lightbox_lightbox_service__["a" /* Lightbox */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__lightbox_lightbox_service__["a" /* Lightbox */]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_13_ng2_bootstrap_modal__["DialogService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_13_ng2_bootstrap_modal__["DialogService"]) === "function" && _o || Object])
], UserComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o;
//# sourceMappingURL=user.component.js.map

/***/ }),

/***/ "./src/app/verify-account/verify-account.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/verify-account/verify-account.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row phone-verification-container\">\n  <p class=\"phone-verification-instruction lead center\">\n    Please enter your phone number to receive a verification code via text.\n  </p>\n  <div class=\"phone-number-container center\">\n    <label translate>Phone Number</label>\n    <div class=\"form-group form-inline\">\n      <input\n        placeholder=\"1-123-456-7890\"\n        type=\"text\" name=\"phone_number\" value=\"\"\n        (input)=\"assignPhoneNumber($event)\"\n        class=\"form-control\"\n        required>\n      <button class=\"btn btn-primary\" (click)=\"requestVerification()\">Send code</button>\n    </div>\n       <label translate>Please be sure to include your country code and local area code.</label>\n  </div>\n\n  <div class=\"security-code-container center\" *ngIf=\"isCodeSent\">\n    <label>Security code</label>\n    <div class=\"form-group form-inline\">\n      <input placeholder=\"Security code\"\n             type=\"text\" name=\"security_code\" value=\"\"\n             (input)=\"assignSecurityCode($event)\"\n             class=\"form-control\"\n             required>\n      <button class=\"btn btn-primary\" (click)=\"verifySecurityCode()\">Verify</button>\n    </div>\n  </div>\n\n  <br>\n  <p class=\"phone-verification-instruction center\">\n    Standard SMS rates apply. We understand that some users have reservations about providing us with their phone number. Phone identification is option. If you don't verify your account, all features, except the Earn Free Bela tasks, will still be accessible.\n\n\n  </p>\n\n</div>\n"

/***/ }),

/***/ "./src/app/verify-account/verify-account.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VerifyAccountComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_event_types__ = __webpack_require__("./src/app/core/event.types.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_event_service__ = __webpack_require__("./src/app/core/event.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var VerifyAccountComponent = (function () {
    function VerifyAccountComponent(belaApiService, eventService, authService, router) {
        this.belaApiService = belaApiService;
        this.eventService = eventService;
        this.authService = authService;
        this.router = router;
        this.user = new __WEBPACK_IMPORTED_MODULE_2__app_model__["h" /* User */]();
        this.isCodeSent = false;
        this.subscriptions = [];
    }
    VerifyAccountComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = this.authService.user;
        this.subscriptions.push(this.eventService.subscribe({
            next: function (event) {
                if (event.type == __WEBPACK_IMPORTED_MODULE_4__core_event_types__["b" /* BelaEventType */].UPDATE_USER) {
                    _this.user = event.value;
                }
            }
        }));
    };
    VerifyAccountComponent.prototype.ngOnDestroy = function () {
        for (var _i = 0, _a = this.subscriptions; _i < _a.length; _i++) {
            var subscription = _a[_i];
            subscription.unsubscribe();
        }
    };
    VerifyAccountComponent.prototype.assignPhoneNumber = function (event) {
        this.phoneNumber = event.target.value;
    };
    VerifyAccountComponent.prototype.assignSecurityCode = function (event) {
        this.securityCode = event.target.value;
    };
    VerifyAccountComponent.prototype.requestVerification = function () {
        var _this = this;
        this.belaApiService.requestPhoneVerification(this.phoneNumber)
            .then(function (response) {
            if (response.status === 'success') {
                _this.isCodeSent = true;
            }
            else {
                alert(response.message);
            }
        })
            .catch(function (err) {
            alert('Server error. Please try again later.');
        });
    };
    VerifyAccountComponent.prototype.verifySecurityCode = function () {
        var _this = this;
        this.belaApiService.verifySecurityCode(this.phoneNumber, this.securityCode)
            .then(function (response) {
            if (response.status === 'success') {
                alert('Your phone number was successfully verified');
                _this.user.user1.phone_number = _this.phoneNumber;
                _this.authService.updateUser(_this.user);
                _this.router.navigate(['/']);
            }
            else {
                alert(response.message);
            }
        })
            .catch(function (err) {
            alert('Server error. Please try again later.');
        });
    };
    return VerifyAccountComponent;
}());
VerifyAccountComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-verify-account',
        template: __webpack_require__("./src/app/verify-account/verify-account.component.html"),
        styles: [__webpack_require__("./src/app/verify-account/verify-account.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_event_service__["a" /* EventService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__core_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_auth_service__["a" /* AuthService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_6__angular_router__["d" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_router__["d" /* Router */]) === "function" && _d || Object])
], VerifyAccountComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=verify-account.component.js.map

/***/ }),

/***/ "./src/app/withdrawal/withdrawal.component.css":
/***/ (function(module, exports) {

module.exports = "\n.withdraw{\n\n  margin-top: -10px !important;\n}\n\n.withdraw .midsectioninfo {\n  background-color: #EEE;\n  height: auto;\n}\n\n.withdraw .midsectioninfo .withdebit {\n  border-right: none;\n}\n\n.midsectioninfo h2 {\n  font-size: 80px;\n  margin-top: 40px;\n  display: block;\n  color: #888;\n\n}\n\n.submitfooter {\n  width: 100%;\n    border-radius: 5px;\n  position: relative;\n    margin-top: 10px;\n  padding-top: 10px;\n  padding-bottom: 10px;\n  background: #73CA65;\n}\n\n.submitfooter:hover{\n  cursor: pointer;\n  background: #73B765;\n}\n\n.withdrawsubmit {\n  background: None !important;\n  font-size: 24px;\n  font-family: 'ProximaNovaRegular', sans-serif;\n  color: white;\n  width: 100%;\n}\n\n.withdraw input::-webkit-input-placeholder {\n  color: #CCC;\n}\n\n.withdraw input::-moz-placeholder {\n  color: #CCC;\n}\n\n.withdraw input:-ms-input-placeholder {\n  color: #CCC;\n}\n\n.withdraw input:-moz-placeholder {\n  color: #CCC;\n}\n\n.withdraw h5 {\n  color: #888;\n  font-size: 24px;\n  padding-left: 10%;\n  padding-right: 10%;\n\n}\n\n.addressdiv {\ndisplay: block;\n  margin-right: 10px;\n  color: #888;\n  vertical-align: top;\n}\n\n.amountdiv {\ndisplay: block;\n  vertical-align: top;\n\n}\n\n.withdraw {\n  color: #888;\n  margin-top: 40px;\n\n}\n\n.withdrawbox h4 {\n  width: 100%;\n  color: #FFF;\n  font-size: 28px;\n  text-align: center;\n  font-family: 'ProximaNovaRegular', sans-serif;\n  background-color: #F87433;\n  padding-right: 20px;\n}\n\n#address {\n  font-size: 24px;\n  display: inline-block;\n  text-align: center;\n  padding-left: 20px;\n  padding-right: 20px;\n  padding-bottom: 10px;\n  padding-top: 10px;\n  width: 100%;\n  background-color: #F9F9F9;\n  border-radius: 5px;\n  color: #888;\n  border: 1px solid #D5D5D5;\n}\n\n#amount {\n\n  font-size: 24px;\n  display: inline-block;\n  text-align: center;\n  padding-left: 20px;\n  padding-right: 20px;\n  padding-bottom: 10px;\n  padding-top: 10px;\n  width: 100%;\n  background-color: #F9F9F9;\n  border-radius: 5px;\n  color: #888;\n  border: 1px solid #D5D5D5;\n}\n\n.midsectioninfo div:hover {\n  background-color: #EEE;\n\n}\n\n.midsectioninfo div {\n  display: inline-block;\n  width: 50%;\n\n}\n\n.midsectioninfo p {\n  font-size: 18px;\n  color: #888;\n  margin-top: 10px;\n}\n\n.midsectioninfo i {\n  font-size: 120px;\n  margin-top: 40px;\n  display: block;\n  color: #888;\n}\n\n.withdebit {\n  border-right: 2px solid #CCCCCC;\n\n}\n\n.withbtc {\n  margin-left: -4px;\n\n}\n\n.midsectioninfo {\n  white-space: nowrap;\n  width: 100%;\n\n  border-top: 2px solid #CCCCCC;\n  text-align: center;\n}\n\n.depowith:hover {\n  cursor: pointer;\n}\n\n.depositbox, .withdrawbox {\n  text-align: center;\n\n\n}\n\n.depositbox .followerbox, .withdrawbox .followerbox {\n  border: none !important;\n  padding-bottom: 0px !important;\n  height: auto !important;\n\n}\n\n.depositbox h4 {\n  width: 100%;\n  color: #FFF;\n  font-size: 28px;\n  text-align: center;\n  font-family: 'ProximaNovaRegular', sans-serif;\n  background-color: #64A9EB;\n}\n\n.depositinstructions {\n  color: #888;\n  font-size: 24px;\n  padding-left: 10%;\n  padding-right: 10%;\n\n}\n\n.depositbox h3 {\n  margin-top: 20px;\n  margin-bottom: 20px;\n  font-size: 24px;\n  display: inline-block;\n  text-align: center;\n  padding-left: 25px;\n  padding-right: 25px;\n  padding-bottom: 15px;\n  padding-top: 15px;\n  background-color: #F9F9F9;\n  border-radius: 5px;\n  color: #64A9EB;\n\n  border: 1px solid #D5D5D5;\n}\n\n@media only screen and (max-width: 1200px) {\n\n.midsectioninfo h2 {\nfont-size: 40px;\n    }\n}\n\n@media only screen and (max-width: 400px) {\n\n.midsectioninfo h2 {\nfont-size: 26px;\n    }\n}"

/***/ }),

/***/ "./src/app/withdrawal/withdrawal.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"withdrawbox\">\n  <div class=\"white-box\">\n\n    <h4 translate>Withdraw Bela</h4>\n\n    <sk-three-bounce *ngIf=\"loading\"></sk-three-bounce>\n\n    <form class=\"withdraw\" [hidden]=\"loading\" #withdrawForm>\n\n      <h5 translate>Your Balance: {{ user.user1.belacoin }} Bela </h5>\n\n\n      <p class=\"depositinstructions\" translate>Enter a Bela address to receive\n        your funds and choose how much you would like to withdraw. </p>\n\n      <div class=\"addressdiv\">\n        <h5 translate>Address</h5>\n        <input\n          placeholder=\"0xffb44851dbdc7dd2a679bf669fdae90d47d0ecac\"\n          id=\"address\" type=\"text\" name=\"withdraw\" value=\"\"\n          (change)=\"assignAddress($event)\"\n          required>\n      </div>\n\n      <div class=\"amountdiv\">\n        <h5>Bela</h5>\n        <input placeholder=\"5\" id=\"amount\" type=\"number\"\n               name=\"amount\"\n               min=\".5\" value=\"\" (input)=\"assignAmount($event)\" required>\n      </div>\n\n      <div class=\"feeandtotal\" *ngIf=\"withdrawAmount != 0\">\n        <h5 class=\"fee\">Fee: <span>{{withdrawFee}} Bela</span></h5>\n        <h5 class=\"total\">Total: <span>{{withdrawTotal}} Bela</span></h5>\n\n      </div>\n\n      <ul class=\"feeschedule\">\n        <li class=\"feetitle\">Withdrawal Amount <span>Fee</span></li>\n        <li *ngFor=\"let setting of withdrawFeeSettings\">\n          <ng-container *ngIf=\"setting.range_to != null\">\n            {{setting.range_from}} - {{setting.range_to}} Bela\n          </ng-container>\n          <ng-container *ngIf=\"setting.range_to == null\">\n            {{setting.range_from}}+ Bela\n          </ng-container>\n          <span> {{setting.percentage}}%</span>\n        </li>\n      </ul>\n\n\n\n      <div class=\"submitfooter\">\n        <input (click)=\"withdraw();false\" class=\"withdrawsubmit\"\n               type=\"submit\" value=\"Submit\">\n        <div>\n          <ngx-loading [show]=\"\"></ngx-loading>\n        </div>\n      </div>\n\n    </form>\n  </div>\n</div>\n\n<p class=\"helpfultext\">Need help cashing out your Bela? <a href=\"https://belacam.zendesk.com/hc/en-us\">Click here to visit the Belacam support and help portal.</a></p>\n"

/***/ }),

/***/ "./src/app/withdrawal/withdrawal.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WithdrawalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_model__ = __webpack_require__("./src/app/app.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__ = __webpack_require__("./src/app/core/bela-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_event_service__ = __webpack_require__("./src/app/core/event.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_event_types__ = __webpack_require__("./src/app/core/event.types.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_auth_service__ = __webpack_require__("./src/app/core/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var WithdrawalComponent = (function () {
    function WithdrawalComponent(belaApiService, authService, eventService) {
        this.belaApiService = belaApiService;
        this.authService = authService;
        this.eventService = eventService;
        this.user = new __WEBPACK_IMPORTED_MODULE_1__app_model__["h" /* User */]();
        this.withdrawFeeSettings = [];
        this.withdrawAddress = "";
        this.withdrawAmount = 0;
        this.loading = false;
        this.subscriptions = [];
    }
    WithdrawalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = this.authService.user;
        this.belaApiService.getWithdrawFeeSettings()
            .then(function (res) {
            _this.withdrawFeeSettings = res;
            _this.calculateWithdrawFee();
        })
            .catch(function (err) { });
        this.subscriptions.push(this.eventService.subscribe({
            next: function (event) {
                if (event.type == __WEBPACK_IMPORTED_MODULE_4__core_event_types__["b" /* BelaEventType */].LIKE_POST) {
                    _this.user = event.value;
                    localStorage.setItem('user', JSON.stringify(event.value));
                }
                else if (event.type == __WEBPACK_IMPORTED_MODULE_4__core_event_types__["b" /* BelaEventType */].UPDATE_USER) {
                    _this.user = event.value;
                }
            }
        }));
    };
    WithdrawalComponent.prototype.ngOnDestroy = function () {
        for (var _i = 0, _a = this.subscriptions; _i < _a.length; _i++) {
            var subscription = _a[_i];
            subscription.unsubscribe();
        }
    };
    WithdrawalComponent.prototype.withdraw = function () {
        var _this = this;
        if (this.user.user1) {
            if (this.user.user1.belacoin < this.withdrawAmount) {
                alert("Withdrawal amount must be smaller than current account capacity!");
            }
            else if (this.withdrawAmount > 0 && this.withdrawAddress != "") {
                this.loading = true;
                this.belaApiService.createWithdrawRequest(this.withdrawAddress, this.withdrawAmount)
                    .then(function (resp) {
                    alert("A withdrawal confirmation message will arrive in your email inbox in the next 5 minutes. Please click the link there to confirm your withdrawal!");
                    _this.user.user1.belacoin = resp['balance'];
                    _this.loading = false;
                    _this.resetForm();
                })
                    .catch(function (err) {
                    alert('Server error. Please try again later.');
                    _this.loading = false;
                });
            }
        }
    };
    WithdrawalComponent.prototype.resetForm = function () {
        this.withdrawForm.nativeElement.reset();
        this.withdrawAddress = "";
        this.withdrawAmount = 0;
    };
    WithdrawalComponent.prototype.assignAddress = function (event) {
        this.withdrawAddress = event.target.value;
    };
    WithdrawalComponent.prototype.assignAmount = function (event) {
        var _this = this;
        this.withdrawAmount = parseFloat(event.target.value);
        if (this.delayTimer) {
            clearTimeout(this.delayTimer);
        }
        this.delayTimer = setTimeout(function () {
            _this.calculateWithdrawFee();
        }, 100);
    };
    WithdrawalComponent.prototype.calculateWithdrawFee = function () {
        for (var _i = 0, _a = this.withdrawFeeSettings; _i < _a.length; _i++) {
            var setting = _a[_i];
            var rangeFrom = setting.range_from;
            var rangeTo = setting.range_to === null ? Number.MAX_VALUE : setting.range_to;
            if (rangeFrom <= this.withdrawAmount && this.withdrawAmount <= rangeTo) {
                this.withdrawFee = (this.withdrawAmount * setting.percentage / 100).toFixed(2);
                this.withdrawTotal = (this.withdrawAmount * (100 - setting.percentage) / 100).toFixed(2);
                break;
            }
        }
    };
    return WithdrawalComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('withdrawForm'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], WithdrawalComponent.prototype, "withdrawForm", void 0);
WithdrawalComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("./src/app/withdrawal/withdrawal.component.html"),
        styles: [__webpack_require__("./src/app/withdrawal/withdrawal.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_bela_api_service__["a" /* BelaApiService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_auth_service__["a" /* AuthService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__core_event_service__["a" /* EventService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_event_service__["a" /* EventService */]) === "function" && _d || Object])
], WithdrawalComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=withdrawal.component.js.map

/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ }),

/***/ 1:
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[0]);
//# sourceMappingURL=main.aa4ea8b952e6a4abc869.bundle.js.map